#!/bin/bash

device=$1

echo -n -e "\033]0;Pushing $device via fastlane at $PWD\007"

if [ "$device" = "ios" ]; then
    cd ios
    fastlane beta
elif [ "$device" = "android" ]; then
    cd android
    bash bundleRelease.sh
    echo jeremyf21 | sudo -S fastlane beta
    echo ""
    echo "Look good? HIT ENTER to push to Playstore!"
    read;
    echo jeremyf21 | sudo -S fastlane deploy
fi
