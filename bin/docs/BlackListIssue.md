# Issue: Metro Bundler (node -v for v14+) not running. 

## Resolution: Locate the blacklist.js (inside node_module folder) file 
<img src="https://i.ibb.co/J5jTWQy/Location-of-Blacklistjs.png" alt="Location-of-Blacklistjs" border="0">


and update sharedBlackList array with this

```
var sharedBlacklist = [
  /node_modules[\/\\]react[\/\\]dist[\/\\].*/,
  /website\/node_modules\/.*/,
  /heapCapture\/bundle\.js/,
  /.*\/__tests__\/.*/
];
```
Screenshot of error:
## 
<img src="https://i.ibb.co/fDL38fp/image.png" width="300" height="500"/>

