#   Firebase Pod Issue

##  Issue

```
/node_modules/react-native-firebase/ios/RNFirebase/firestore/RNFirebaseFirestore.m:392:27: Property 'timestampsInSnapshotsEnabled' not found on object of type 'FIRFirestoreSettings *'
```

## Reproduction

-   Use wrong version

## Solution

-   In Podfile, set Firebase codeblock to match the following:

```
  pod 'RNFirebase', :path => '../node_modules/react-native-firebase/ios'
  pod 'Firebase/Core', '6.26.0'
  pod 'Firebase/Analytics', '6.26.0'
  pod 'Firebase/Auth', '6.26.0'
  pod 'Firebase/Messaging', '6.26.0'
  pod 'Firebase/Functions', '6.26.0'
  pod 'Firebase/Firestore', '6.26.0'
  pod 'Firebase/Storage', '6.26.0'
```

-   run `cd ios && pod install`