// v5
import "react-native-gesture-handler";
import { AppRegistry, YellowBox, UIManager, Platform } from "react-native";
import App from "./app/root/App";
import TrackPlayer from "react-native-track-player";
import { TrackPlayerServices } from "root/TrackPlayerServices";

// fix for android :D
global.Symbol = require("core-js/es6/symbol");
require("core-js/fn/symbol/iterator");
require("core-js/fn/map");
require("core-js/fn/set");
require("core-js/fn/array/find");

YellowBox.ignoreWarnings([""]);

AppRegistry.registerComponent("PrimericaTemplate", () => App);
TrackPlayer.registerPlaybackService(() => TrackPlayerServices);

// android animations
if (Platform.OS === "android")
  UIManager.setLayoutAnimationEnabledExperimental &&
    UIManager.setLayoutAnimationEnabledExperimental(true);
