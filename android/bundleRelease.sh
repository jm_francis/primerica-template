#!/bin/bash
echo ""
echo "Bundling release build and extracting apk."
echo ""
memoryStr="org.gradle.jvmargs=-Xmx4096m -XX:MaxPermSize=4096m -XX:+HeapDumpOnOutOfMemoryError"
sed -i '' "s/$memoryStr//g" gradle.properties
echo "$memoryStr" >> gradle.properties
# ANCHOR: bundle for release
./gradlew bundleRelease -x bundleReleaseJsAndAssets
# ANCHOR: extract .apk file from .aab
rm -rf app/build/outputs/apk/release/app.zip
rm -rf app/build/outputs/apk/release/app.apks
rm -rf app/build/outputs/apk/release/app
bundletool build-apks --bundle=app/build/outputs/bundle/release/app.aab --output=app/build/outputs/apk/release/app.apks --mode=universal
mv app/build/outputs/apk/release/app.apks app/build/outputs/apk/release/app.zip
cd app/build/outputs/apk/release/
unzip app.zip -d app
cd ../../../../..
mv app/build/outputs/apk/release/app/universal.apk app/build/outputs/apk/release/
