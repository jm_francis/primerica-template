#!/bin/bash
appName="${PWD##*/}"
echo -n -e "\033]0;$appName\007"
echo ""
echo "Starting Android bundle script in $PWD"
## TMP? ##
# echo "Deleting android production build files..."
# rm -rf android/app/src/main/assets/index.android.bundle
# rm -rf android/app/build
# mkdir android/app/build
echo "Bundling files..."
react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
if [[ -d "android/app/build/generated/res/react" ]]; then
    echo "Removing extra app/src/main/res files..."
    cd android/app/src/main/res
    rm -rf drawable-hdpi
    rm -rf drawable-mdpi
    rm -rf drawable-xhdpi
    rm -rf drawable-xxhdpi
    rm -rf drawable-xxxhdpi
    rm -rf raw
    cd ../../../../../
fi
echo "Android JavaScript bundle complete!"
