// module.exports = {
//   presets: [
//     "module:metro-react-native-babel-preset",
//     // "module:react-native-dotenv",
//   ],
//   env: {
//     production: {},
//   },
//   plugins: [
//     [
//       "@babel/plugin-proposal-decorators",
//       {
//         legacy: true,
//       },
//     ],
//     ["@babel/plugin-proposal-optional-catch-binding"],
//     [
//       "module-resolver",
//       {
//                 root: ["./app"],
//                 extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json"],
//                 alias: {
//                   tsaiutils: "./app/utilities",
//                 },
//       },
//     ],
//   ],
// };

module.exports = {
  presets: [
    "module:metro-react-native-babel-preset",
    // "module:react-native-dotenv",
  ],
  env: {
    production: {},
  },
  plugins: [
    [
      "module-resolver",
      {
        root: ["."],
        extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json"],
        alias: {
          assets: ["./assets"],
          utilities: ["./app/utilities"],
          components: "./app/components",
          animation: ["./app/modules/animation"],
          backend: ["./app/backend"],
          "constant-deprecated": ["./app/constant-deprecated"],
          pages: ["./app/pages"],
        },
      },
    ],
  ],
};
