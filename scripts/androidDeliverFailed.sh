#!/bin/bash

cd ../../scripts

. ./init.sh reverse --source-only

deliverFailuresFile="$originPath/scripts/androidDeliverFailures.txt"
echo "$updateAppName" >> "$deliverFailuresFile"
open "$deliverFailuresFile"

echo "❗️❗️❗️ Failed to deliver $updateAppName to Playstore."
sendTextMessage "18043866934" "Failed to deliver $updateAppName to Playstore."
