#!/bin/bash

# ANCHOR: Imports
. ./jsUpdate.sh --source-only
. ./massUpdate.sh --source-only
. ./cloneSelf.sh --source-only
. ./_updateLogo.sh --source-only
. ./_simulate_app.sh --source-only
. ./_update_android_files.sh --source-only
. ./init.sh --source-only # sets dir to root of project (ex: apps/PrimericaTemplate)

command=$1

clear
if [ -z "$command" ]; then
    echo "Please use the command like this:"
    # TODO: massUpdateSelf (just for self)?
    echo "./MASTER <cloneSelf|recover|jsUpdate|massUpdate|deliver_ios|deliver_android|deliver_all|updateLogo|simulateApp> [SOME_TEAM]"
    exit 1
fi

updateAppName="$2"
if [ -z "$updateAppName" ] && [ "$command" != "massUpdate" ] && [ "$command" != "deliver_all" ]; then
    print "($command) Enter name of app: "
    read updateAppName
    if [ ! -d "../$updateAppName" ] && [ "$command" != "cloneSelf" ]; then
        echo "$updateAppName directory not found."
        exit 0
    fi
    updateAppNameLowercase=$(lowercase $updateAppName)
fi
if [ ! -z "$updateAppName" ]; then
    cd ..
    updatePath="$PWD/$updateAppName"
fi

if [ "$command" != "massUpdate" ] && [ "$command" != "deliver_all" ]; then
    initUpdateAppVariables
fi
# Variables now in place: (exception: massUpdate)
#   originAppName, originAppNameLowercase, originPath, originPackageVersion, originApplicationId, originAndroidPackage, originAndroidVersion, originAndroidVersionCode
#   updateAppName, updateAppNameLowercase, updatePath, updatePackageVersion, updateApplicationId, updateAndroidPackage, updateAndroidVersion, updateAndroidVersionCode

if [ "$command" = "jsUpdate" ]; then
    jsUpdate
elif [ "$command" = "cloneSelf" ]; then
    cloneSelf
elif [ "$command" = "recover" ]; then
    cloneSelf recover
elif [ "$command" = "massUpdate" ]; then
    massUpdate
elif [ "$command" = "deliver_ios" ]; then
    deliver ios
elif [ "$command" = "deliver_android" ]; then
    deliver android
elif [ "$command" = "deliver_all" ]; then
    deliver_all "$2" "$3"
elif [ "$command" = "updateLogo" ]; then
    updateLogo
elif [ "$command" = "simulateApp" ]; then
    simulateApp
else
    echo "\"$command\" is not a valid command."
    exit 1
fi
