updateAppName="TestApp"
firebaseProjectId="test-project-apptakeoff"

function generateFirebaseApp() {
    if [ "$1" == "web" ]; then
        _output=$(firebase apps:create $1 "$updateAppName" --project "$firebaseProjectId")
    elif [ "$1" == "ios" ]; then
        # echo "Please type in com.apptakeoff.$updateAppName and HIT ENTER TWICE"
        _output=$(yes com.apptakeoff.$updateAppName | firebase apps:create $1 "$updateAppName" --project "$firebaseProjectId")
        echo $_output
    fi
    _startsWith="firebase apps:"
    _result=$(echo $_output | grep -o "${_startsWith}.*")
    echo $_result
    $_result
}

generateFirebaseApp ios
