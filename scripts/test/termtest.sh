#!/bin/bash

function terminal() { # terminal [path to start at] "<some command>"
  path="$PWD"
  command="$1"
  if [ ! -z "$2" ]; then
    path="$1"
    command="$2"
  fi
  osascript <<END
  tell application "Terminal"
    do script "cd \"$path\";$command;"
  end tell
END
}

terminal "$PWD" "echo test && echo test this and that"
