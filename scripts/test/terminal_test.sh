#!/bin/bash

updateAppName="WYD"
updatePath="/Volumes/App Takeoff [A]/Dropbox/Primerica/apps/WYD"

cd ..

. ./init.sh --source-only

terminal "$updatePath" "caffeinate bash bundle_android.sh && cd android && bash bundleRelease.sh && echo jeremyf21 | sudo -S fastlane beta"

# terminal "$updatePath" "bash bundle_ios.sh && cd ios && fastlane sigh && FASTLANE_ITUNES_TRANSPORTER_PATH='fastlane_workaround' fastlane beta" # removed certify_me
