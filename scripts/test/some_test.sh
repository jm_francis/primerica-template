#!/bin/bash

originAppName="PrimericaTemplate"
updateAppName="XTremeNation"

_path="/Volumes/App Takeoff [A]/Dropbox/Primerica/apps"
originPath="${_path}/$originAppName"
updatePath="${_path}/$updateAppName"

function readJSON() { # readJSON <key> <file>
  echo $(cat "$2" | underscore extract "$1")
}

somevar=$(readJSON "skip_update_ios_files" "config.json")

echo "$somevar"

if $somevar ; then
    echo "some var is true"
else
    echo "some var is false"
fi

