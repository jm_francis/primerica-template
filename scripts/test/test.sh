#!/bin/bash

updateAppName="TestApp"

# echo "Enter firebase project ID:"
# read firebaseProjectId

firebaseProjectId="test-project-apptakeoff"

function generateFirebaseApp() {
    if [ "$1" == "web" ]; then
        _output=$(firebase apps:create $1 "$updateAppName" --project "$firebaseProjectId")
    elif [ "$1" == "ios" ]; then
        # _output=$(firebase apps:create $1 "$updateAppName" --project "$firebaseProjectId")
        firebase apps:create $1 "$updateAppName" --project "$firebaseProjectId"
    fi
    _startsWith="firebase apps:"
    _result=$(echo $_output | grep -o "${_startsWith}.*")
    echo $_result
    $_result
}

echo "#!/bin/bash/expect -f" > expect.exp
echo "set timeout 4" >> expect.exp
echo "spawn firebase apps:create ios \"$updateAppName\" --project \"$firebaseProjectId\""
echo "expect \"? Please specify your iOS app bundle ID: () \"" >> expect.exp
echo "send \"com.apptakeoff.$updateAppName\r\"" >> expect.exp
echo "expect \"? Please specify your iOS app App Store ID: () \"" >> expect.exp
echo "send \"\r\"" >> expect.exp
expect ./expect.exp

# expect
# spawn generateFirebaseApp ios
# expect ""

# firebase apps:create ios "$updateAppName" "com.apptakeoff.$updateAppName" --project "$firebaseProjectId"


# firebase apps:create ios "$updateAppName" --project "$firebaseProjectId"
