#!/bin/bash

# PWD: SomeApp/ios/fastlane/
cd ../../scripts

. ./init.sh reverse --source-only

# ensures iosFastlaneFailure.sh has had time to update the failures.txt if needed
sleep 0.25

_failuresFile="$originPath/scripts/failures.txt"
contactPhoneNumber="18043866934"

if [[ "$(cat "$_failuresFile")" == *"$updateAppName"* ]]; then
    echo " X X X X "
    # echo "Attempted to run iosFastlaneCompleted but noticed $updateAppName in failures.txt and therefore cancelled."
    echo "❗️ Something may have gone wrong... (noticed $updateAppName in failures.txt)"
    echo " X X X X "
    # setWindowColor "#7e821f"
    # exit 0;
    # echo "WARNING: $updateAppName is listed in the failures.txt"
else
    echo -en "\033]0; ✅ ✅ ✅ Successfully exported $updateAppName ($1) \a"
    # sendTextMessage "$contactPhoneNumber" "Successfully exported $updateAppName"
fi

# setWindowColor "#18351a"

echo "Just pushed $updateAppName with fastlane (ios)."

nextAppName="none"
is_next="false"
while read team; do
    if [ "$team" = "end" ]; then
        nextAppName="none"
        echo "Reached the end."
        break;
    fi
    if [ "$is_next" = "true" ] && [[ "$(cat "$_failuresFile")" == *"$team"* ]]; then
        echo "❗️ Skipping $team because they are listed in the failures.txt ❗️"
        open "$_failuresFile"
        continue
    fi
    if [ "$is_next" = "true" ]; then
        nextAppName="$team"
        break
    fi
    if [ "$team" = "$updateAppName" ]; then
        is_next="true"
    fi
done <"$updatePath/scripts/massUpdate.txt"

if [ "$nextAppName" = "none" ]; then
    echo "No next app found. Stopping script."
    sendTextMessage "$contactPhoneNumber" "All apps updated."
    echo "($PWD)"
    exit 0
fi

echo " - - - - - - - "
echo "(iosFastlaneCompleted.sh)"
echo "Updating: $nextAppName in new terminal... ($PWD)"
echo " - - - - - - - "

terminal "$originPath/../$nextAppName/scripts" "bash massUpdateSelf.sh ios"
