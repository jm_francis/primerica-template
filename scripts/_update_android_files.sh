#!/bin/bash

function _update_android_files() {
    print "Updating android files... ($updateAppName)"

    # MainApplication.java, MainActivity.java
    updateUniqueFile "$originAndroidPackagePath/MainActivity.java" "$updateAndroidPackagePath/MainActivity.java"
    updateUniqueFile "$originAndroidPackagePath/MainApplication.java" "$updateAndroidPackagePath/MainApplication.java"

    # app/build.gradle
    updateUniqueFile "$originPath/android/app/build.gradle" "$updatePath/android/app/build.gradle"

    # AndroidManifest.xml
    updateUniqueFile "$originPath/android/app/src/main/AndroidManifest.xml" "$updatePath/android/app/src/main/AndroidManifest.xml"

    # settings.gradle
    updateUniqueFile "$originPath/android/settings.gradle" "$updatePath/android/settings.gradle"

    # res/
    updateUniqueFile "$originPath/android/app/src/main/res/values/colors.xml" "$updatePath/android/app/src/main/res/values/colors.xml"
    updateUniqueFile "$originPath/android/app/src/main/res/values/styles.xml" "$updatePath/android/app/src/main/res/values/styles.xml"
}
