#!/bin/bash
#
function install_node_modules() { # install_node_modules <team>
    goback="$PWD"
    cd "$originPath/../$1"
    if [ -d "node_modules" ] && [ "$(extract_package_json_version node_modules/npm_i_version.json)" = "$originPackageVersion" ]; then
        echo "Skipping node_modules install for ${1}, already up to date."
    else
        echo "Installing node_modules for ${1}... (this may take a minute)"
        mv node_modules node_modules.delete
        _rsync_output=$(rsync -avr "$originPath/node_modules" ./)
        echo ""
        echo "{ \"version\": ${originPackageVersion} }" > "./node_modules/npm_i_version.json"
        rm -rf node_modules.delete &
        cd "$goback"
    fi
}

function massUpdate() {

    _someoneFailed=0
    while read team<&3; do
        if [ ! -d "$originPath/../$team" ] && [ "$team" != "end" ]; then
            echo "Uh oh! $team may have been typed incorrectly. No directory found with that name."
            _someoneFailed=1
        fi
    done 3< "$originPath/scripts/massUpdate.txt"
    if [ $_someoneFailed = 1 ]; then
        exit 1
    fi

    skip_js_update=$(readConfig skip_js_update)
    skip_node_modules=$(readConfig skip_node_modules)

    cd "$originPath/scripts"

    clear
    echo ""
    echo "This script will do the following:"
    if [ $skip_node_modules = true ]; then
        echo "    🛑 node_modules will not be updated due to config.json"
    else
        echo "    ✅ Update node_modules if needed"
    fi
    if [ $skip_js_update = true ]; then
        echo "    🛑 JavaScript portion will be skipped due to config.json"
    else
        echo "    ✅ Update the JavaScript"
    fi
    echo "    ✅ Build android and or ios and push to both via fastlane (unless set differently in config.json)"
    echo ""
    echo "HIT ENTER to continue."
    read;
    clear

    _failures_output=$(cat "$originPath/scripts/failures.txt")
    if (( ${#_failures_output} > 2 )); then
        echo "❕ Looks like failures.txt is not empty. This may affect the script. Proceed with CAUTION!"
        echo "(HIT ENTER)"
        read
        clear
    fi

    clear
    _latestVersion=$(readJSON "version" "$originPath/package.json")
    echo "If you would like to update EVERY APP to the latest version ($_latestVersion), type YES. (or HIT ENTER to continue)"
    read _updateALL
    if [ "$_updateALL" = "YES" ]; then
        __goback="$PWD"
        cd "$originPath/.."
        for d in * ; do
            if [ ! -f "$d/package.json" ] || [[ "$d" == *"-bkp"* ]]; then
                continue;
            fi
            if [[ "$(cat "$originPath/scripts/massUpdate.txt")" == *"$d"* ]]; then
                # echo "$d is already in massUpdate.txt, skipping."
                continue;
            fi
            if [[ "$(readJSON status $d/CONFIG.json)" == *"inactive"* ]]; then
                echo "$d is set as inactive. Skipping."
                continue;
            fi
            if [[ "$(readJSON freezeVersion $d/CONFIG.json)" == *"true"* ]]; then
                echo "❗️ $d has a frozen version. Skipping. (please deal with this guy later)"
                continue;
            fi
            if [ "$(readJSON version $d/package.json)" == "$_latestVersion" ]; then
                echo "$d is already on version $_latestVersion, skipping."
                continue;
            fi
            echo "$d" >> "$originPath/scripts/massUpdate.txt"
        done
        echo "" >> "$originPath/scripts/massUpdate.txt"
        open "$originPath/scripts/massUpdate.txt"
        cd "$__goback"
    fi

    echo "Would you like to update iOS? (y/n)"
    read update_ios;
    echo "Would you like to update Android? (y/n)"
    read update_android;
    clear

    # massUpdate.txt
    echo "All teams to be updated must be listed in massUpdate.txt"
    echo "     * ALSO: The last line of that file must be blank."
    echo "     * Type open to open."
    echo "     * Or HIT ENTER to continue."
    echo ""
    read shouldOpenTxt;
    if [ "$shouldOpenTxt" = "open" ]; then
        open "$originPath/scripts/massUpdate.txt"
    fi
    clear

    # IOS VERSIONING #
    if [ "$update_ios" = "y" ]; then
        echo "Would you like to automatically update every iOS version? (y/n)"
        read updateIosVersions;
        if [ "$updateIosVersions" = "y" ]; then
            while read team<&3; do
                if [ "$team" = "end" ]; then
                    echo "Reached the end."
                    break;
                fi

                cd "$originPath/../$team/ios"

                # TMP START
                cp "$originPath/ios/versionHandler.sh" ./
                cp "$originPath/ios/calculator.js" ./
                updateUniqueFile "$originPath/ios/fastlane/Fastfile" "fastlane/Fastfile" $team
                # TMP END

                liveVersion=$(bash versionHandler.sh getLiveVersion) # be cautious with this command
                currentVersion=$(bash versionHandler.sh getVersion)
                nextVersion=$(bash versionHandler.sh getNextVersion $liveVersion)
                # currentVersion=$(agvtool mvers -terse1)
                # nextVersion=$(echo "$liveVersion + 0.1" | bc | python -c "print round(float(raw_input()),2)")

                if [ "$liveVersion" = "0" ]; then
                    echo "$team has not been submitted to AppStore before. Skipping..."
                    continue
                elif [ "$currentVersion" = "$nextVersion" ]; then
                    echo "$team is already set to the correct next version ($currentVersion). Skipping..."
                    continue
                fi
                agvtool new-marketing-version $nextVersion
                # NOTE: needed?
                open "$originPath/../$team/ios/$team.xcworkspace"
                sleep 6
                killall Xcode
                # osascript -e 'tell application "XCode" to set miniaturized of every window to true'
                echo ""
                echo "Set $team iOS version from $currentVersion to $nextVersion"
                echo ""
            done 3< "$originPath/scripts/massUpdate.txt"
        fi
        clear
    fi

    # ANDROID VERSIONING #
    if [ "$update_android" = "y" ]; then
        echo "Would you like to automatically update all of the app/build.gradle versions? (y/n)"
        read updateAllAndroidVersions;
        if [ "$updateAllAndroidVersions" = "y" ]; then
            while read team<&3; do
                # echo "Auto updating $team"
                if [ "$team" = "end" ]; then
                    echo "Reached the end."
                    break;
                fi
                if [ ! -d "$originPath/../$team" ]; then
                    echo "Skipping \"$team\" - no directory found."
                    continue;
                fi
                incrementAndroidVersion "$team"
            done 3< "$originPath/scripts/massUpdate.txt"
        fi
        clear
    fi

    ### MIGRATION SCRIPT ###
    echo "Would you like to run the migration script for each app? (y/n)"
    read runMigrationScripts
    if [ "$runMigrationScripts" = "y" ]; then
        _gobck="$PWD"
        cd "$originPath/../../firebase-manager"
        while read team<&3; do
        echo -n -e "\033]0;Migrating $team\007"
        bash ./MASTER.sh
        done 3< "$originPath/scripts/massUpdate.txt"
        cd "$_gbck"
    fi
    clear

    echo ""
    echo "HIT ENTER when you and ready to start the MASS UPDATE! :D"
    read;
    clear

    firstTeam="none"
    while read team; do
        if [ "$firstTeam" = "none" ]; then
            firstTeam="$team"
        fi
        if [ "$team" = "end" ]; then
            echo "Reached the end."
            break;
        fi
        echo "Updating JavaScript & node_modules for ${team}..."
        echo -en "\033]0; In progress: $team \a"
        cp "$originPath/scripts/massUpdate.txt" "$originPath/../$team/scripts/"
        updateAppName="$team"
        updateAppNameLowercase=$(lowercase $team)
        updatePath="$originPath/../$team"
        if $skip_node_modules && [ -d "$originPath/../$team/node_modules" ] ; then
            echo "Skipping node_modules... (config.json)"
        else
            install_node_modules "$team"
        fi
        if $skip_js_update ; then
            echo "Skipping jsUpdate... (config.json)"
        else
            jsUpdate "$team"
        fi
    done <"$originPath/scripts/massUpdate.txt"

    echo -en "\033]0; All done! \a"
    echo ""
    echo ""
    echo "All teams updated with latest JavaScript!"
    echo ""
    echo "Compiling and pushing via fastlane starting with ${firstTeam}..!"

    cd "$originPath/../$firstTeam"
    update="none"
    if [ "$update_ios" = "y" ]; then
        update="ios"
    fi
    if [ "$update_android" = "y" ]; then
        if [ "$update" = "ios" ]; then
            update="both"
        else
            update="android"
        fi
    fi

    terminalPath="${originPath}/../${firstTeam}/scripts"
    if [ "$update" = "ios" ] || [ "$update" = "both" ]; then
        terminal "$terminalPath" "caffeinate bash massUpdateSelf.sh ios"
    fi
    if [ "$update" = "android" ] || [ "$update" = "both" ]; then
        terminal "$terminalPath" "caffeinate bash massUpdateSelf.sh android"
    fi

    clear
    echo ""
    echo "Process has begun!"
    echo ""
    if [ "$update_android" = "y" ]; then
        echo "When you are ready to deploy every app to the Playstore, HIT ENTER!"
        read;
        cd "$originPath/scripts"
        ./MASTER.sh deliver_all android

        echo ""
        echo "All android apps have been deployed."
        echo ""
    fi

    if [ "$update_ios" = "y" ]; then
        echo "When you are ready to deploy every app to the AppStore, HIT ENTER!"
        read;
        cd "$originPath/scripts"
        ./MASTER.sh deliver_all ios

        echo ""
        echo "All iOS apps have been deployed."
        echo ""
    fi

}
