#!/bin/bash

# DESCRIPTION:
# Copies the .aab file into the apps exports/$app directory and publishes to git!
# The purpose of this is so people like Andrew Poole can access .aab files and upload them to the Playstore if needed.

goback="$PWD"

updateAppName="$1"

if [ -z "$updateAppName" ]; then
    echo "Please use the command like this: ./pushAndroidExport.sh <AppName>"
    exit 1
fi

echo "(exports) Upload android aab file for ${updateAppName}..."

if [ ! -d "../../exports/$updateAppName" ]; then
    mkdir ../../exports/$updateAppName
fi

cp ../../$updateAppName/android/app/build/outputs/bundle/release/app.aab ../../exports/$updateAppName/
cd ../../exports
bash update.sh
cd "$goback"
