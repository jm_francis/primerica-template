#!/bin/bash

cd ../..

echo "$PWD"

function lowercase() { # returns string as lowercase
  _lowercase_var=$(echo $1 | tr '[:upper:]' '[:lower:]')
  echo "$_lowercase_var"
}

originAppName="PrimericaTemplate"
originAppNameLowercase=$(lowercase $originAppName)

# replaces origin's app name (normal and lowercase) keywords with the update app's name (normal and lowercase)
function renewFileKeywords() { # renewFileKeywords <some_file>
#   if [[ "$1" == *"android"* ]]; then
#     if [[ "$1" == *".java"* ]] || [[ "$1" == *".xml"* ]]; then
#       sed -i '' "s/${originAndroidPackage}/${updateAndroidPackage}/g" "$1"
#     fi
#   fi

#   if [[ "$1" == *"app/build.gradle"* ]]; then
#     sed -i '' "s/${originApplicationId}/${updateApplicationId}/g" "$1"
#     sed -i '' "s/versionName \"${originAndroidVersion}\"/versionName \"${updateAndroidVersion}\"/g" "$1"
#     sed -i '' "s/versionCode ${originAndroidVersionCode}/versionCode ${updateAndroidVersionCode}/g" "$1"
#   fi

  sed -i '' "s/${originAppName}/${updateAppName}/g" "$1"
  sed -i '' "s/${originAppNameLowercase}/${updateAppNameLowercase}/g" "$1"
}

# update a file from PrimericaTemplate but maintain proper keywords
function updateUniqueFile() { # <origin_file> <file_to_update>
  cp "$1" "$2"
  renewFileKeywords "$2"
}

# TODO: implement even without versions being updated?
# while read team<&3; do
  # echo $team
  
  # cp PrimericaTemplate/scripts/iosFastlaneFailure.sh $team/scripts/
  # cp PrimericaTemplate/scripts/iosFastlaneCompleted.sh $team/scripts/
  # cp PrimericaTemplate/scripts/init.sh $team/scripts/

  # open "$team/ios/$team.xcworkspace"
  # osascript -e 'tell application "XCode" to set miniaturized of every window to true'
  # sleep 5
# done 3< "PrimericaTemplate/scripts/massUpdate.txt"

for d in * ; do
    if [ ! -f "$d/package.json" ] || [ "$d" = "PrimericaTemplate" ]; then
        continue;
    fi

    updateAppName="$d"
    updateAppNameLowercase=$(lowercase $updateAppName)

    echo "Deleting DerivedData for $d"
    rm -rf "$d/ios/DerivedData"
    
    ## UPDATE EVERY PROJECTS FASTLANE ##
    # echo "Updating fastlane for $updateAppName..."
    # cd "$updateAppName/ios"
    # bundle update fastlane
    # cd ../..

    ## LOGO ADJUSTMENT ##
    # echo "Setting logo for $updateAppName"
    # if [ -f "$updateAppName/ios/$updateAppName/Images.xcassets/AppIcon.appiconset/icon-1024@1x.png" ]; then
    #   cp "$updateAppName/ios/$updateAppName/Images.xcassets/AppIcon.appiconset/icon-1024@1x.png" "$updateAppName/assets/app_specific_images/logo.png"
    # else
    #   cp "$updateAppName/ios/$updateAppName/Images.xcassets/AppIcon.appiconset/ItunesArtwork@2x.png" "$updateAppName/assets/app_specific_images/logo.png"
    # fi
done

echo "Done!"
