#!/bin/bash

function updateInfoPlist() {
    _originPlist="$originPath/ios/$originAppName/Info.plist"
    _updatePlist="$updatePath/ios/$updateAppName/Info.plist"

    if [ ! -f "${_updatePlist}.original" ]; then
        cp "$_updatePlist" "${_updatePlist}.original"
    fi

    info_displayName=$(valueFromPlist "CFBundleDisplayName" "$_updatePlist")
    info_shortVersionString=$(valueFromPlist "CFBundleShortVersionString" "$_updatePlist")
    info_bundleVersion=$(valueFromPlist "CFBundleVersion" "$_updatePlist")

    cp "$_originPlist" "$_updatePlist"

    setPlistValue "CFBundleDisplayName" "$info_displayName" "$_updatePlist"
    setPlistValue "CFBundleShortVersionString" "$info_shortVersionString" "$_updatePlist"
    setPlistValue "CFBundleVersion" "$info_bundleVersion" "$_updatePlist"

    __goback="$PWD"
    cd "$updatePath/ios"
    agvtool new-marketing-version $(bash ./versionHandler.sh getVersion)
    cd "$__goback"
}

function _update_ios_files() {
    goback="$PWD"
    print "Updating ios files... ($updateAppName)"

    # TMP - deleting DerivedData to avoid conflicts
    rm -rf "$updatePath/ios/DerivedData" &

    # AppDelegate
    updateUniqueFile "$originPath/ios/$originAppName/AppDelegate.m" "$updatePath/ios/$updateAppName/AppDelegate.m"
    updateUniqueFile "$originPath/ios/$originAppName/AppDelegate.h" "$updatePath/ios/$updateAppName/AppDelegate.h"

    # Info.plist
    echo "Updating Info.plist"
    updateInfoPlist

    # Podfile
    updateUniqueFile "$originPath/ios/Podfile" "$updatePath/ios/Podfile"
    echo "Installing pods..."
    cd "$updatePath/ios"
    mv Pods Pods.delete
    rm -rf Pods.delete &
    pod install

    # Fastlane (the main fastlane files are updated elsewhere)
    rsync -avr "$originPath/ios/fastlane_workaround" "$updatePath/ios/"
    cp "$originPath/ios/AuthKey_SCNZ7X6V6S.p8" "$updatePath/ios/"

    # Snagging latest versionHandler.sh
    cp "$originPath/ios/versionHandler.sh" "$updatePath/ios/"
    cp "$originPath/ios/calculator.js" "$updatePath/ios/"

    cd "$goback"
}
