#!/bin/bash

# PWD: SomeApp/ios/fastlane/
cd ../../scripts

. ./init.sh reverse --source-only

deliverFailuresFile="$originPath/scripts/iosDeliverFailures.txt"

echo "$updateAppName" >> "$deliverFailuresFile"
open "$deliverFailuresFile"

sendTextMessage "18043866934" "Failed to deliver $updateAppName"
