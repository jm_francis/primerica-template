#!/bin/bash

cd ../../

echo "" > "PrimericaTemplate/scripts/massUpdate.txt"

for d in * ; do
    if [ ! -f "$d/package.json" ] || [ "$d" = "PrimericaTemplate" ]; then
        continue;
    fi
    if grep -q "$d" "PrimericaTemplate/scripts/completedUpdates.txt"; then
        continue;
    fi
    echo "$d" >> PrimericaTemplate/scripts/massUpdate.txt
done
