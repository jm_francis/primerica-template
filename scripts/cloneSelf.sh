#!/bin/bash

# TODO: auotmate/remove the kill it functions
# TODO: automate the push to Test Flight for approval

function cloneSelf() { # cloneSelf or cloneSelf recover

  recover=0
  if [ "$1" = "recover" ]; then
    recover=1
  fi

  clear

  echo -n -e "\033]0;Creating $updateAppName\007"

  # ANCHOR: Declare directory paths
  cd "$originPath"
  cd ../../resources/team_resources
  mkdir $updateAppName
  resourcePath="$PWD/$updateAppName"
  cd "$originPath"
  echo "resourcePath = $resourcePath"

  # ANCHOR: Opening prompts
  print "What would you like the display name to be?"
  read DISPLAY_NAME

if [ $recover = 0 ]; then
  print "Enter an Access Code:"
  read ACCESS_CODE
fi

  print "Please enter the android firebase app id:"
  echo "(Firebase > Settings > General > Apps > Android apps > \"App ID\")"
  read FIREBASE_APP_ID # kill it

  # firebase cloud functions
  if [ $recover = 0 ]; then
    print "Please select the correct firebase project. (cloud-functions)"
    cd "$originPath/../../cloud-functions"
    firebase use --add
    echo ""
    print "Now go into Firebase > Functions and click \"Get Started\". HIT ENTER when done."
    read;
    echo "Deploying firebase functions in new window..."
    terminal "$originPath/../../cloud-functions" "firebase use $updateAppName && sleep 10 && bash updateSingle.sh $updateAppName"
    # terminal "$originPath/../../cloud-functions" "firebase use $updateAppName && clear && echo 'Please manually enter this command: firebase deploy' && echo ''"
    echo ""
  fi

  # new
  # firebase apps:create web "$updateAppName" --project "$firebaseProjectId"
  # firebase apps:create ios "$updateAppName" --project "$firebaseProjectId"

if [ $recover = 0 ]; then
  print "Please drop the files below into the following directory."
else
  print "(recovery mode) Please make sure the following files are still in place..."
fi
  echo "   logo.png"
  echo "   logo-transparent.png"
  echo "   GoogleService-Info.plist" # kill it
  echo "   google-services.json" # kill it
  echo "HIT ENTER to open directory."
  read
  cd "$resourcePath"
  open .
  print "HIT ENTER when done."
  read
  if [ ! -f "logo-transparent.png" ] || [ ! -f "logo.png" ]; then
    echo "Either logo-transparent.png or logo.png is missing or named incorrectly. Please correct this and run the script again."
    exit 1
  fi
  if [ ! -f "GoogleService-Info.plist" ] || [ ! -f "google-services.json" ]; then
    echo "Either GoogleService-Info.plist or google-services.json is missing or named incorrectly. Please correct this and run the script again."
    exit 1
  fi
  cp logo-transparent.png APP-LOGO-250.png
  sips -Z 250 APP-LOGO-250.png
  # cp APP-LOGO-250.png xtreme-nation-250-grey.png
  cp logo.png playstore.png
  sips -Z 512 playstore.png
  print "Uploading logo to git..."
  cd ..
  git fetch --all
  git pull origin master
  git add .
  git commit -m "Logos for $updateAppName"
  git pull origin master
  git push origin master
  print "Now set the $originAppName android app icon to $updateAppName's logo. [HIT ENTER]"
  read
  open "$originPath/android" -a "Android Studio"
if [ $recover = 0 ]; then
  print "Now you need to paste in the Firebase Web SDK. [HIT ENTER]" # kill it (all of this)
  read
  cd "$resourcePath"
  cp "$originPath/../../admin_sites/$originAppName-FireAdmin/src/config/unique.js" "${resourcePath}/"
  code unique.js
  print "HIT ENTER when you are done."
  read
fi
  cd "$originPath"

  # TMP START
  echo "update name and path: $updateAppName $updatePath"
  echo "origin name and path: $originAppName $originPath"
  # TMP END

  print "VERY IMPORTANT CHECKLIST:"
  echo "    🚀 The Gcloud clone is complete."
  echo "    🚀 You have clicked \"Get Started\" in the \"App Distribution\" section of firebase."
  echo "    🚀 The admin account has the following email and password: admin@${updateAppNameLowercase}.com  ${updateAppNameLowercase}admin"
  echo "    🚀 $updateAppName in Appstore Connect is created with this EXACT bundle id: com.apptakeoff.$updateAppName"
  echo "    🚀 The firebase android package name (google-services.json) is EXACTLY: com.apptakeoff.$updateAppNameLowercase"
  # echo "    * Make sure you are on the correct git branch!"
  echo ""
  echo "If you are 100% ready, HIT ENTER to begin the clone!"
if [ $recover = 1 ]; then
  echo "(recover mode enabled)"
fi
  read

  # ANCHOR: Manually set/overwrite necessary variables
  updateApplicationId="com.apptakeoff.$updateAppNameLowercase"
  updateAndroidPackage="com.apptakeoff.$updateAppNameLowercase"
  updateAndroidVersion="3.0"
  updateAndroidVersionCode="1"

  # ANCHOR: CLONE (rsync)
  print "The clone has begun! (this may take a minute, please be patient)"
  cd "$originPath"
if [ $recover = 1 ]; then
  echo "Backing up broken $updateAppName..."
  if [ -d "$originPath/../$updateAppName-bkp" ]; then
    echo "$updateAppName-bkp already exists. Are you okay with continuing? (HIT ENTER) "
    read
  else
    mv "$updatePath" "$originPath/../$updateAppName-bkp"
  fi
fi
  _hidelog=$(rsync -avr --exclude='ios/DerivedData' --exclude='ios/build' --exclude='ios/Pods' --exclude='.git' "./" ../$updateAppName)
  print " *** Finished copying files. *** "

  # ANCHOR: Rename the app
  print "Running react-native-rename $updateAppName"
  cd "$updatePath"
  react-native-rename $updateAppName # is this ruining it?
  # workaround (if needed)
  if [ ! -f "$updatePath/android/app/src/main/java/com/apptakeoff/$updateAppNameLowercase/MainActivity.java" ]; then
    cd "$updatePath/android/app/src/main/java/com"
    mkdir apptakeoff
    cd apptakeoff
    mkdir "$updateAppNameLowercase"
    echo "$originAndroidPackagePath vs. $updateAndroidPackagePath"
    cp "$originAndroidPackagePath/MainActivity.java" "$updateAndroidPackagePath/"
    cp "$originAndroidPackagePath/MainApplication.java" "$updateAndroidPackagePath/"
  fi
  _update_android_files
  print "App rename succeeded!"

  # ANCHOR: iOS setup
  print "Installing pods..."
  cd "$updatePath/ios"
  pod install

  cd "$updatePath/ios"
  print "Creating $updateAppName-Bridging-Header ($PWD)"
  echo "" >>"$updateAppName-Bridging-Header.h"

  # ANCHOR: DISPLAY_NAME
  echo "Setting display names..."
  sed -i '' "s/$updateAppName/$DISPLAY_NAME/g" "$updatePath/android/app/src/main/res/values/strings.xml"
  sed -i '' "s/DISPLAY_NAME/$DISPLAY_NAME/g" "$updatePath/ios/$updateAppName/Info.plist"

  # ANCHOR: App logo setup
  print "Setting up app logo."
  cd "$updatePath"
  cp "$resourcePath/APP-LOGO-250.png" "$updatePath/assets/app_specific_images/"
  cp "$resourcePath/APP-LOGO-250.png" "$updatePath/ios/$updateAppName/Images.xcassets/xtreme-nation-250.imageset/"
  react-native set-icon --path "$resourcePath/logo.png" --platform ios
  cp "$resourcePath/logo.png" "$updatePath/assets/app_specific_images/"

  # ANCHOR: GoogleService-Info.plist & google-services.json
  print "Pasting in Google service files for firebase."
  cp "$resourcePath/GoogleService-Info.plist" "$updatePath/ios/"
  cp "$resourcePath/google-services.json" "$updatePath/android/app/"

  # ANCHOR: Fastlane
  print "Setting up fastlane files."
  cd "$updatePath/ios/fastlane"
  renewFileKeywords Fastfile
  renewFileKeywords Appfile
  cd "$updatePath/android/fastlane"
  renewFileKeywords Fastfile
  renewFileKeywords Appfile
  sed -i '' "s/FIREBASE_APP_ID/$FIREBASE_APP_ID/g" Fastfile

  # ANCHOR: Exporting & Shipping
  print "Exporting iOS in a separate terminal... (using iTunes Transporter workaround)"
  cd "$updatePath"
  if [ $recover = 1 ]; then
    echo ""
    echo "(recover mode)"
    echo "Time to export ios. Make sure the Xcode version is correct then HIT ENTER."
    read
  fi
  terminal "bash bundle_ios.sh && cd ios && fastlane sigh && open $updateAppName.xcworkspace && echo \\\"Please wait 60 seconds for Xcode to index...\\\" && sleep 60 && bash fastlaneBeta.sh external"

  print "Exporting android in a separate terminal..."
  if [ $recover = 1 ]; then
    echo ""
    echo "(recover mode)"
    echo "Time to export android. Make sure the android/app/build.gradle version is correct first then HIT ENTER."
    read
  fi
  terminal "bash bundle_android.sh && cd android && bash bundleRelease.sh && fastlane beta" # removed sudo

  # ANCHOR: Configuring Firebase
if [ $recover = 0 ]; then
  print "Configuring Firebase project..."
  cd "$originPath/../../firebase-manager/"
  FULL_APP_TITLE=$(sed 's/\([^[:blank:]]\)\([[:upper:]]\)/\1 \2/g' <<< "$updateAppName") # FULL_APP_TITLE used to be DISPLAY_NAME but did not satisfy auto update developer feature in app
  echo " * node configureApp.js $updateAppName $FULL_APP_TITLE $ACCESS_CODE"
  node configureApp.js "$updateAppName" "$FULL_APP_TITLE" "$ACCESS_CODE" # "$GOOGLE_DRIVE"
fi

  # ANCHOR: FireAdmin
if [ $recover = 0 ]; then
  print "Setting up FireAdmin panel... (this may take a minute)"
  cd "$updatePath/../../admin_sites"
  _hidelog=$(rsync -avr --exclude='.firebase' --exclude='.firebaserc' --exclude='firebase.json' --exclude='.git' "./$originAppName-FireAdmin/" "./$updateAppName-FireAdmin")
  cp "$resourcePath/unique.js" "$updateAppName-FireAdmin/src/config/"
  cd "$updateAppName-FireAdmin"
  firebase init
  print "Finishing FireAdmin setup..."
  cp "../$originAppName-FireAdmin/firebase.json" ./
  npm run build
  firebase deploy
fi

  print "Script finished for $updateAppName. Please double check the following:"
  echo ""
  echo "   🚀 In the firebase's Firestore config > variables, appTitle and appPassword are set correctly."
  echo "   🚀 The APNS key file is uploaded in Firebase (Settings > Cloud Messaging)"
  echo "   🚀 Any data from the cloned app is removed or reset: users, notifications, scoreboards, etc."
  echo "   🚀 Submit iOS build to TestFlight."
  echo "   🚀 Submit android build to Playstore."
  echo ""
  echo "If all looks good, pat yourself on the back!"
if [ $recover = 1 ]; then
  echo "(recovery mode) hopefully things are back to normal, but give it all a double check!"
fi

}
