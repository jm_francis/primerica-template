#!/bin/bash

cd ../../scripts

. ./init.sh reverse --source-only

sendTextMessage "18042779099" "-> Attempted to distribute $updateAppName to external TestFlight testers but may have failed. You may need to do this manually."
