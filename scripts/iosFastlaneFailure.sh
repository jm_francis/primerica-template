#!/bin/bash

# PWD: SomeApp/ios/fastlane/
cd ../../scripts

. ./init.sh reverse --source-only

# setWindowColor "#C70000"
echo -en "\033]0; 💥 💥 💥 Error updating $updateAppName ($1) \a"
sendTextMessage "18043866934" "Error updating $updateAppName"

echo "Failed to push $updateAppName with fastlane (ios)!"
echo "$(date +%F) - ${updateAppName}\n" >> "$originPath/scripts/failures.txt"
open "$originPath/scripts/failures.txt"

if [ -z "$updateAppName" ]; then #TMP
    echo "updateAppName is null..."
fi

# NOTE: Starting the export of the next app is handled by iosFastlaneCompleted.sh even when it fails.

# nextAppName="none"
# is_next="false"
# while read team; do
#     if [ "$is_next" = "true" ]; then
#         nextAppName="$team"
#         break
#     fi
#     if [ "$team" = "$updateAppName" ]; then
#         is_next="true"
#     fi
# done <"$updatePath/scripts/massUpdate.txt"

# if [ "$nextAppName" = "none" ]; then
#     echo "No next app found. Stopping script."
#     echo "($PWD)"
#     exit 0
# fi

# echo " - - - - - - - "
# echo "(iosFastlaneFailure.sh)"
# echo "Updating: $nextAppName in new terminal... ($PWD)"
# echo " - - - - - - - "

# terminal "$originPath/../$nextAppName/scripts" "bash massUpdateSelf.sh ios"
