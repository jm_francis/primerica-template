#!/bin/bash

function updateLogo() {
  goback="$PWD"

  clear
  print "Please paste in the new logo.png and logo-transparent.png into the resource directory for $updateAppName."
  echo "[ HIT ENTER ] to open this directory."
  read
  resourcePath="$originPath/../../resources/team_resources/$updateAppName"
  open "$resourcePath"
  print "When you are done just [ HIT ENTER ] to continue."
  read

  cd "$resourcePath"

  if [ ! -f logo-transparent.png ] || [ ! -f logo.png ]; then
    echo "Missing either logo-transparent.png or logo.png in $resourcePath"
    echo "TIP: check your spelling on those file names and try again!"
    exit 0
  fi

  cp logo-transparent.png APP-LOGO-250.png
  sips -Z 250 APP-LOGO-250.png
  cp logo.png playstore.png
  sips -Z 512 playstore.png

  cp "$resourcePath/APP-LOGO-250.png" "$updatePath/assets/app_specific_images/"
  cp "$resourcePath/APP-LOGO-250.png" "$updatePath/ios/$updateAppName/Images.xcassets/xtreme-nation-250.imageset/"
  cd "$updatePath"
  react-native set-icon --path "$resourcePath/logo.png" --platform ios

  # Netlify
  _netlifyDir="$originPath/../../distribution_landing_pages/Netlify-pages"
  netlifyPath="$_netlifyDir/t/$updateAppName"
  if [ -d "$netlifyPath" ]; then
    cp "$resourcePath/playstore.png" "$netlifyPath/logo.png"
    __goback="$PWD"
    cd "$_netlifyDir"
    bash publish.sh "Updating $updateAppName logo"
    cd "$__goback"
    echo "Netlify logo updated."
  else
    echo "❗️ WARNING: $netlifyPath was not found. Netlify (get-my.app) logo will not be updated."
    read;
  fi

  echo ""
  echo ""
  echo "✅ iOS logo should be ready now!"
  print "Would you like to update the android logo? (y/n)"
  read _updateAndroidLogo
  if [ "$_updateAndroidLogo" = "y" ]; then
    open "$updatePath/android" -a "Android Studio"
  fi
  clear

  echo "FINAL TODOs:"
  echo ""
  echo " 🚀  Text https://get-my.app/$updateAppNameLowercase to yourself and make sure the new logo shows."
  echo " 🚀  Export and push a new update for the iOS and Android projects and get them off to production!"
  echo " 🚀  Inform client about their logo update."

  # if [ -d "$netlifyPath" ]; then
  #   echo "Would you like to update the get-my.app (Netlify) logo? (y/n)"
  #   read _publishNetlify
  #   if [ "$_publishNetlify" = "y" ]; then
  #     cd "$netlifyPath/../../"
  #     bash publish.sh "Updated logo for $updateAppName"
  #   fi
  #   clear
  # fi
  
  # print "Would you like to"

  cd "$goback"
  echo ""
  echo "Have a wonderful day!"
  echo ""
}
