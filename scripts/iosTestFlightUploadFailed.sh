#!/bin/bash

cd ../../scripts

. ./init.sh reverse --source-only

echo -en "\033]0; ❗️❗️❗️ Error uploading $updateAppName to TestFlight \a"
sendTextMessage "18043866934" "Error uploading $updateAppName to TestFlight"
