#!/bin/bash

. ./_update_android_files.sh --source-only
. ./_update_ios_files.sh --source-only
. ./init.sh reverse --source-only

if [ -z "$1" ]; then
    echo "UH oh! Please use the command like this: ./massUpdateSelf.sh <ios/android/both>"
    exit 1
fi

# ANCHOR: Variables
update_ios="false"
update_android="false"
if [ "$1" = "both" ]; then
    update_android="true"
    update_ios="true"
elif [ "$1" = "ios" ]; then
    update_ios="true"
elif [ "$1" = "android" ]; then
    update_android="true"
fi

skip_ios_update_files=$(readConfig skip_update_ios_files)

is_next="false"
while read team; do
    if [ "$is_next" = "true" ]; then
        nextAppName="$team"
        break
    fi
    if [ "$team" = "$updateAppName" ]; then
        is_next="true"
    fi
done <"$originPath/scripts/massUpdate.txt"

# ANCHOR: Caffeinate
echo "Caffeinating in background..."
caffeinate -u -t 3200 &

# ANCHOR: Script
clear
echo -en "\033]0; Updating $updateAppName ($1) \a"
echo "Updating: ${updateAppName} ($1)"
echo "From: ${originAppName}"
echo "Up next: ${nextAppName}"
echo ""

cd "$updatePath"

# IOS
if [ "$update_ios" = "true" ]; then
    # open "$updatePath/ios/$updateAppName/Info.plist"
    # osascript -e 'tell application "XCode" to set miniaturized of every window to true'
    # sleep 4

    # delete DerivedData ?
    rm -rf "$updatePath/ios/DerivedData"

    # fastlane files
    print "Updating Fastlane files for iOS..."
    updateUniqueFile "$originPath/ios/fastlane/Fastfile" "$updatePath/ios/fastlane/Fastfile"
    cp "$originPath/ios/Gemfile" "$updatePath/ios/"
    cp "$originPath/ios/fastlane/Pluginfile" "$updatePath/ios/fastlane/"
    # NOTE: iTunes Transporter workaround
    rsync -avr "$originPath/ios/fastlane_workaround" "$updatePath/ios/"

    # ios specific files
    if $skip_ios_update_files ; then
        echo "Skipping _update_ios_files... (config.json)"
    else
        _update_ios_files
    fi

    # bundle & push
    print "Bundling and pushing ios with fastlane beta."
    cd "$updatePath"
    bash bundle_ios.sh
    cd ios

    # maybe this is the issue?
    # echo "Disabling old .ipa file..."
    # for i in *.ipa; do
    #     mv "$i" $(echo $i | sed "s/ipa/old/g")
    #     # rm -rf "$i"
    # done

    echo " * fastlane sigh"
    fastlane sigh
    FASTLANE_ITUNES_TRANSPORTER_PATH="$(pwd)/fastlane_workaround" caffeinate fastlane beta
fi

# ANDROID
if [ "$update_android" = "true" ]; then
    # fastlane files
    print "Updating Fastfile files for Android..."
    cd "$updatePath/android/fastlane"
    mkdir bkp
    cp Fastfile bkp/
    cp "$updatePath/android/fastlane" 
    cp "$originPath/android/fastlane/Pluginfile" "$updatePath/android/fastlane/"
    cp "$originPath/android/fastlane/extract_app_id.sh" "$updatePath/android/fastlane/"
    firebaseAppId=$(bash extract_app_id.sh)
    updateUniqueFile "$originPath/android/fastlane/Fastfile" "$updatePath/android/fastlane/Fastfile"
    sed -i '' "s/FIREBASE_APP_ID/${firebaseAppId}/g" "$updatePath/android/fastlane/Fastfile"

    # android specific files
    _update_android_files

    # bundle & push
    print "Bundling and pushing android with fastlane beta."
    cd "$updatePath"
    bash bundle_android.sh
    cd android
    caffeinate bash bundleRelease.sh
    caffeinate fastlane beta
    # echo jeremyf21 | sudo -S caffeinate fastlane beta
fi
