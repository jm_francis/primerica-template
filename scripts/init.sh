#!/bin/bash

# reverse is applied in massUpdateSelf.sh for when init.sh is being run from an app receiving an update from PrimericaTemplate
reverse=0
if [ "$1" = "reverse" ]; then
  reverse=1
fi

if [ "${PWD##*/}" = "scripts" ]; then
  cd .. # plants us right in the project directory (ex: apps/PrimericaTemplate)
fi

function sendTextMessage() {
  __goback="$PWD"
  cd "$originPath/../../nexmo-texting"
  bash sendMessage.sh "$1" "$2"
  cd "$__goback"
}

function lowercase() { # returns string as lowercase
  _lowercase_var=$(echo $1 | tr '[:upper:]' '[:lower:]')
  echo "$_lowercase_var"
}

function readJSON() { # readJSON <key> <file>
  if [[ $(cat "$2") != *"$1"* ]]; then
    echo ""
    return 0
  fi
  echo $(cat "$2" | underscore extract "$1")
}
function extract_package_json_version() { # extract_package_json_version path_to/package.json
  if [ -f "$1" ]; then
    readJSON "version" "$1"
  else
    echo ""
  fi
}
function readConfig() { # readConfig <key> # get value of any key in config.json
  readJSON "$1" "$originPath/scripts/config.json"
}

# ANCHOR: Android functions
function extract_application_id() { # extract_application_id <path_to/app/build.gradle>
  echo $(echo $(awk '/applicationId/{print $NF}' "$1") | sed 's/"//g')
}
function extract_android_package() { # extract_android_package <path_to/SomeFile.java>
  echo $(awk '/package /{print $NF}' "$1") | sed 's/ false);//g' | sed 's/;//g'
}
function extractAndroidVersionName() {                              # extractAndroidVersionName path_to/app/build.gradle
  echo $(grep "versionName" "$1" | awk '{print $2}' | sed 's/"//g') # ex: 1.0
}
function extractAndroidVersionCode() {                # extractAndroidVersionCode path_to/app/build.gradle
  echo $(grep "versionCode " "$1" | awk '{print $2}') # ex: 2
}

function initUpdateAppVariables() { # run this after setting updatePath, updateAppName, and updateAppNameLowercase
  # general
  updatePackageVersion=$(extract_package_json_version "$updatePath/package.json")
  # android
  updateApplicationId=$(extract_application_id "${updatePath}/android/app/build.gradle")
  updateAndroidPackagePath="$updatePath/android/app/src/main/java/com/apptakeoff/$updateAppNameLowercase"
  if [ ! -d "$updatePath/android/app/src/main/java/com/apptakeoff" ] && [ -d "$updatePath/android" ]; then
    updateAndroidPackagePath="$updatePath/android/app/src/main/java/com/$updateAppNameLowercase"
  fi
  updateAndroidPackage=$(extract_android_package "$updateAndroidPackagePath/MainApplication.java")
  updateAndroidVersion=$(extractAndroidVersionName "$updatePath/android/app/build.gradle")
  updateAndroidVersionCode=$(extractAndroidVersionCode "$updatePath/android/app/build.gradle")

  echo ""
  echo "Initialized and generated some of the following variables:"
  echo "${originAppName}, ${originApplicationId}, ${originAndroidPackage}, ${originAndroidVersion} (${originAndroidVersionCode})"
  echo "${updateAppName}, ${updateApplicationId}, ${updateAndroidPackage}, ${updateAndroidVersion} (${updateAndroidVersionCode})"
  echo ""
}

# ANCHOR: iOS start
function getIOSBundleID() { # Usage: getIOSBundleID [path_to_ios_folder]
  __goback="$PWD"
  if [ ! -z "$1" ]; then
    cd "$1"
  fi
  echo $(xcodebuild -showBuildSettings | grep PRODUCT_BUNDLE_IDENTIFIER) | sed "s/ //g" | sed "s/PRODUCT_BUNDLE_IDENTIFIER//g" | sed "s/=//g"
  cd "$__goback"
}
# ANCHOR: iOS end

# Origin App Variables
originPath="$PWD"
if [ $reverse = 1 ]; then
  updatePath="$PWD"
  updateAppName="${updatePath##*/}"
  updateAppNameLowercase=$(lowercase $updateAppName)
  originPath="${PWD}/../PrimericaTemplate"
  initUpdateAppVariables
fi
originAppName="${originPath##*/}"
originAppNameLowercase=$(lowercase $originAppName)

echo "origin path = $originPath"
echo "originAppName = $originAppName"

originPackageVersion=$(extract_package_json_version "$originPath/package.json")

originApplicationId=$(extract_application_id "${originPath}/android/app/build.gradle")

originAndroidPackagePath="$originPath/android/app/src/main/java/com/$originAppNameLowercase"
if [ -d "$originPath/android/app/src/main/java/com/apptakeoff" ]; then
  originAndroidPackagePath="$originPath/android/app/src/main/java/com/apptakeoff/$originAppNameLowercase"
fi

originAndroidPackage=$(extract_android_package "$originAndroidPackagePath/MainApplication.java")

originAndroidVersion=$(extractAndroidVersionName "$originPath/android/app/build.gradle")
originAndroidVersionCode=$(extractAndroidVersionCode "$originPath/android/app/build.gradle")

function print() { # print $1
  echo ""
  echo "$1"
}

# open new terminal window and run some command in it
function terminal() { # terminal [path to start at] "<some command>"
  path="$PWD"
  command="$1"
  if [ ! -z "$2" ]; then
    path="$1"
    command="$2"
  fi
  osascript <<END
  tell application "Terminal"
    do script "cd \"$path\";$command;"
  end tell
END
}
function setWindowColor() { # setWindowColor [valid terminal color]
  _color="#78FBA0BF"
  if [ ! -z "$1" ]; then
    _color="$1"
  fi
  osascript <<END
    tell application "Terminal"
      set current settings of window 1 to settings set "$_color"
    end tell
END
}

# replaces origin's app name (normal and lowercase) keywords with the update app's name (normal and lowercase)
function renewFileKeywords() { # renewFileKeywords <some_file> [altTeamName]
  _altTeamName="$2"

  if [[ "$1" == *"android"* ]]; then
    if [[ "$1" == *".java"* ]] || [[ "$1" == *".xml"* ]]; then
      sed -i '' "s/${originAndroidPackage}/${updateAndroidPackage}/g" "$1"
    fi
  fi

  if [[ "$1" == *"app/build.gradle"* ]]; then
    sed -i '' "s/${originApplicationId}/${updateApplicationId}/g" "$1"
    sed -i '' "s/versionName \"${originAndroidVersion}\"/versionName \"${updateAndroidVersion}\"/g" "$1"
    sed -i '' "s/versionCode ${originAndroidVersionCode}/versionCode ${updateAndroidVersionCode}/g" "$1"
  fi

  if [[ "$1" == *"Fastfile"* ]]; then
    if [[ "$1" == *"ios"* ]]; then
      _iosFolderPath=$(echo $1 | sed "s/\/fastlane\/Fastfile//g")
    elif [[ "$PWD" == *"ios"* ]]; then
      _iosFolderPath="$PWD"
    fi
    # echo "chosen path: $_iosFolderPath"
    if [ ! -z "$_iosFolderPath" ]; then
      iosBundleID=$(getIOSBundleID "$_iosFolderPath")
      # echo "Replacing com.apptakeoff.$originAppName with $iosBundleID in Fastfile"
      sed -i '' "s/com.apptakeoff.$originAppName/$iosBundleID/g" "$1"
    fi
  fi

  _teamName="${updateAppName}"
  if [ ! -z "$_altTeamName" ]; then
    _teamName="$_altTeamName"
  fi
  _teamNameLowercase=$(lowercase $_teamName)

  sed -i '' "s/${originAppName}/${_teamName}/g" "$1"
  sed -i '' "s/${originAppNameLowercase}/${_teamNameLowercase}/g" "$1"
}

# update a file from PrimericaTemplate but maintain proper keywords
function updateUniqueFile() { # <origin_file> <file_to_update> [altTeamName]
  cp "$1" "$2"
  renewFileKeywords "$2" "$3"
}

# returns path to $? with \'s for spaces and brackets
function nakedPath() { # nakedPath <some_path>
  return $(echo $(echo "$1" | sed 's/ /\\ /g') | sed 's/[][]/\\&/g')
}

# plist files
function valueFromPlist() { # valueFromPlist <key> <file_path>
  echo $(/usr/libexec/PlistBuddy -c "Print :$1" "$2")
}
function setPlistValue() { # setPlistValue <key> <new_value> <file_path>
  /usr/libexec/PlistBuddy -c "Set :$1 $2" "$3"
}

# increments android app/build.gradle versionName and versionCode for updateAppName or provided alt_team
function incrementAndroidVersion() { # incrementAndroidVersion [alt_team]
  _team_path="$updatePath"
  if [ ! -z "$1" ]; then
    _team_path="$originPath/../$1"
  fi
  _gradle_path="$_team_path/android/app/build.gradle"
  _team_androidVersionCode=$(extractAndroidVersionCode "$_team_path/android/app/build.gradle")
  _team_androidVersionName=$(extractAndroidVersionName "$_team_path/android/app/build.gradle")
  _nextVersionCode=$((${_team_androidVersionCode} + 1))
  _newVersionName="${originAndroidVersion}.${_nextVersionCode}"
  sed -i '' "s/versionCode $_team_androidVersionCode/versionCode $_nextVersionCode/g" "$_gradle_path"
  sed -i '' "s/versionName \"$_team_androidVersionName\"/versionName \"$_newVersionName\"/g" "$_gradle_path"
  if [ -z "$1" ]; then # updateApp
    setAndroidVersionVariables
  fi
  echo "Set $1 (android) versionName from $_team_androidVersionName to $_newVersionName"
}

# deliver/publish via fastlane to AppStore, TestFlight, or Playstore
function deliver() { # deliver <ios/android>
  goback="$PWD"
  cd "$updatePath"
  if [ "$1" = "ios" ]; then
    # ANCHOR: deliver ios
    cd ios
    _currentVersion=$(agvtool mvers -terse1)
    # _liveVersion=$(bash versionHandler.sh getLiveVersion) # be cautious with this command
    _deliverMessage="Delivering $updateAppName ($_currentVersion)"
    echo "$_deliverMessage"
    echo -en "\033]0; $_deliverMessage \a"
    if [ "$_currentVersion" = "1.0" ] || [ "$_currentVersion" = "1.1.1" ]; then
      # Distribute for Test Flight release
      if [ $(readJSON testFlightRelease "$updatePath/CONFIG.json") = true ]; then
        print "Distributing ${updateAppName} to external TestFlight testers..."
        fastlane testflight_external
        return 0;
      fi
      _liveVersion=$(bash versionHandler.sh getLiveVersion)
      if [ "$_liveVersion" != "1.0" ]; then
        print "⚠️⚠️⚠️ ${updateAppName}'s iOS version is 1.0, please manually submit to AppStore! (pushing TestFlight external for now)"
        fastlane testflight_external
        return 0;
      fi
    fi
    # TMP START
    # echo "Copying from $PWD"
    # cp "../../PrimericaTemplate/ios/fastlane/app_privacy_details.json" "./fastlane/"
    # TMP END
    if [ ! -z "$(ls fastlane/metadata)" ]; then
      echo "fastlane/metadata already exists and has contents :)"
    else
      echo "fastlane/metadata is empty, running \"fastlane deliver init\""
      rm -rf fastlane/metadata
      fastlane deliver init
    fi
    echo "New social feature updates on home screen and bug fixes all around." > "fastlane/metadata/en-US/release_notes.txt"
    # cp "$originPath/ios/fastlane/app_privacy_details.json" fastlane/
    echo "Submitting to Apple for review..."
    fastlane submit_review
  elif [ "$1" = "android" ]; then
    # ANCHOR: deliver android
    cd android
    echo "Submitting $updateAppName to Google Playstore..."
    # echo jeremyf21 | sudo -S fastlane deploy
    caffeinate fastlane deploy
  fi
  cd "$goback"
}

function deliver_all() { # deliver_all <ios|android> [altFile]
  if [ -z "$1" ]; then
    print "Please use the command like this: deliver_all <ios|android>"
    exit 1
  fi
  print "Delivering all apps listed in massUpdate.txt ($1)"

  _file="$originPath/scripts/massUpdate.txt"
  if [ ! -z "$2" ]; then
    _file="$originPath/scripts/$2"
  fi

  if [ ! -f "$_file" ]; then
    echo "Update list file not found at $_file"
    exit 0;
  fi

  # TMP START (?)
  while read team<&3; do
    if [ ! -d "$originPath/../$team" ]; then
      continue;
    fi
    cp "$originPath/../PrimericaTemplate/ios/fastlane/app_privacy_details.json" "$originPath/../$team/ios/fastlane/"
  done 3< "$_file"
  # TMP END

  while read team<&3; do
    if [ "$team" = "end" ]; then
      print "Reached the end."
      break;
    elif [ ! -d "$originPath/../$team" ]; then
      echo "\"$team\" directory not found. Skipping."
      continue;
    fi

    cd "$originPath/scripts"

    if [ "$1" = "ios" ]; then
      ./MASTER.sh deliver_ios "$team"
    elif [ "$1" = "android" ]; then
      ./MASTER.sh deliver_android "$team"
    fi
  done 3< "$_file"

  # Try failed android deliveries again if needed
  _androidDeliverFailuresFile="$originPath/scripts/androidDeliverFailures.txt"
  _android_failures_output=$(cat "$_androidDeliverFailuresFile")
  if [ "$1" = "android" ] && (( ${#_android_failures_output} > 2 )); then
    echo "It looks like some apps failed to publish (see scripts/androidDeliverFailures.txt)"
    echo "Do you want to try publishing those failed apps again? (y/n) "
    read try_again
    if [ "$try_again" = "y" ]; then
      while read team<&3; do
        cd "$originPath/scripts"
        ./MASTER.sh deliver_android "$team"
      done 3< "$_androidDeliverFailuresFile"
    fi
  fi

  print "Finished!"
}
