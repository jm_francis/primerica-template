#!/bin/bash

# ANCHOR: Imports
# . ./init.sh --source-only

function jsUpdate {

  cd "$originPath" # .../apps/PrimericaTemplate/

  if [ ! -d "$updatePath/app/constant-deprecated/" ]; then
    echo "Looks like app/constant-deprecated no longer is a thing. Please update me! (jsUpdate.sh)"
    echo "(or HIT ENTER to proceed anyway)"
    read;
  fi

  # ANCHOR: app directory
  echo " [ app ] "
  cd "$updatePath"
  mv app app.original
  cp -r "${originPath}/app" "${updatePath}/"
  cp "$updatePath/app.original/constant-deprecated/Config.js" "$updatePath/app/constant-deprecated/"

  # ANCHOR: assets
  echo " [ assets/images, assets/videos ] "
  cp -r "${originPath}/assets/images" "${updatePath}/assets/"
  cp -r "${originPath}/assets/videos" "${updatePath}/assets/"
  # cp -r "${originPath}/assets/icons" "${updatePath}/assets/"
  cp -r "${originPath}/assets/index.js" "${updatePath}/assets/"

  # ANCHOR: scripts
  echo " [ *.sh, ./scripts, ./bin ] "
  cd "$updatePath"
  find ../$originAppName -name '*.sh' -maxdepth 1 -exec cp {} ./ \;
  cp -r "${originPath}/scripts" "${updatePath}/"
  cp -r "${originPath}/bin" "${updatePath}/"

  # ANCHOR: other root files
  echo " [ index.js, package.json, tsconfig.json, jest.config.js, babel.config.js ] "
  updateUniqueFile "${originPath}/index.js" "${updatePath}/index.js"
  updateUniqueFile "${originPath}/package.json" "${updatePath}/package.json"
  cp "${originPath}/tsconfig.json" "${updatePath}/"
  cp "${originPath}/jest.config.js" "${updatePath}/"
  cp "${originPath}/babel.config.js" "${updatePath}/"

  # ANCHOR: files to remove
  function removeFile() {
    if [ -f "$1" ]; then
      rm "$1"
    fi
  }
  removeFile "${updatePath}/babel.config.json"

  # ANCHOR: bundleRelease.sh
  echo " [ android/bundleRelease.sh ] "
  cp "${originPath}/android/bundleRelease.sh" "${updatePath}/android/"

  # ANCHOR: backup
  print "Moving backup files..."
  if [ ! -d "../bkp_files" ]; then
    mkdir "../bkp_files"
  fi
  if [ ! -d "../bkp_files/${updateAppName}" ]; then
    mkdir "../bkp_files/${updateAppName}"
  fi
  if [ -d "../bkp_files/${updateAppName}/app.original" ]; then
    rm -rf "../bkp_files/${updateAppName}/app.original"
  fi
  mv app.original "../bkp_files/${updateAppName}/"

  # ANCHOR: Temporary workaround fixes
  cp "$originPath/scripts/workarounds/RCTUIImageViewAnimated.m" "$updatePath/node_modules/react-native/Libraries/Image/"

  print "* * * * * * * * * * * * * * * * * * * * *"
  print "JavaScript update from $originAppName to $updateAppName complete!"
  print "* * * * * * * * * * * * * * * * * * * * *"

}
