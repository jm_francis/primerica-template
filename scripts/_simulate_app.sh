#!/bin/bash

function _testapp() {
    cd "$originPath"
    cp ios/GoogleService-Info.plist ios/GoogleService-Info.plist.original
    rm ios/GoogleService-Info.plist.old
    mv ios/GoogleService-Info.plist ios/GoogleService-Info.plist.old
    cp "$updatePath/ios/GoogleService-Info.plist" ios/
    if [ ! -z "$uid" ]; then
        sed -i '' "s/TEST_UID = null/TEST_UID = \"$uid\"/g" "$originPath/app/backend/FirestoreHandler.js"
        echo "Set uid in FirestoreHandler.js to $uid"
    fi
}
function _revert() {
    mv ios/GoogleService-Info.plist ios/GoogleService-Info.plist.old
    cp ios/GoogleService-Info.plist.original ios/GoogleService-Info.plist
    if [ ! -z "$uid" ]; then
        sed -i '' "s/TEST_UID = \"$uid\"/TEST_UID = null/g" "$originPath/app/backend/FirestoreHandler.js"
    fi
}

function simulateApp() {
    goback="$PWD"
    cd "$originPath"

    uid="$2"

    _testapp
    terminal "npm start"
    terminal "react-native run-ios && cd scripts && bash _simulate_app.sh --revert $uid"
    echo "Now testing ${updateAppName}!"
    # _revert
    echo "Done!"

    cd "$goback"
}

if [ "$1" = "--revert" ]; then
    cd ..
    originPath="$PWD"
    uid="$2"
    _revert
fi
