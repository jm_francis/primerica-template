#!/bin/bash

# PWD: SomeApp/ios/fastlane/
cd ../../scripts

. ./init.sh reverse --source-only

contactPhoneNumber="18043866934"

# exportDir="$updatePath/../exports/$updateAppName/"
# if [ ! -d "$exportDir" ]; then
#     echo "Attempting to upload .aab to exports git..."
#     mkdir "$exportDir"
#     rsync -avr "$updatePath/android/app/build/ouputs/bundle/release/app.aab" "$exportDir/"
#     git add .
#     git commit -m "$updateAppName android aab upload"
#     git pull origin master
#     git push origin master
# fi

# setWindowColor
echo -en "\033]0; ✔️✔️✔️ Android update - $updateAppName ($1) \a"

echo "Just pushed $updateAppName with fastlane (android)."

if [ -z "$updateAppName" ]; then #TMP
    echo "updateAppName is null..."
fi

nextAppName="none"
is_next="false"
while read team; do
    if [ "$team" = "end" ]; then
        nextAppName="none"
        echo "Reached the end."
        break;
    fi
    if [ "$is_next" = "true" ]; then
        nextAppName="$team"
        break
    fi
    if [ "$team" = "$updateAppName" ]; then
        is_next="true"
    fi
done <"$updatePath/scripts/massUpdate.txt"

# if [ ! -d "$updatePath/../exports/$updateAppName" ]; then
#     __goback="$PWD"
#     cd "$updatePath/scripts"
#     echo "Attempting to run pushAndroidExport.sh from $PWD"
#     bash ./pushAndroidExport.sh $updateAppName
#     cd "$__goback"
# fi

if [ "$nextAppName" = "none" ]; then
    # echo "($PWD)"
    sendTextMessage "$contactPhoneNumber" "All android app updates complete."
    cd "$originPath/scripts"
    exit 0
fi

echo " - - - - - - - "
echo "(androidFastlaneCompleted.sh)"
echo "Updating: $nextAppName in new terminal... ($PWD)"
echo " - - - - - - - "

terminal "$originPath/../$nextAppName/scripts" "bash massUpdateSelf.sh android"
