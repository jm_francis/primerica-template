#!/bin/bash

APP_NAME=$1
uid=$2

ONLY_REVERT=0
ROOT="$PWD"
shouldSetUid=0

if [ -z "$APP_NAME" ]; then
    echo "Please use the command like this: ./test_app <APP_NAME> [uid]"
    echo "Use ./test_app --revert to only perform the revert."
    exit;
elif [ "$APP_NAME" = "--revert" ]; then
    ONLY_REVERT=1
fi

if [ -z "$uid" ]; then
    echo "No uid provided, leaving as is."
else
    shouldSetUid=1
fi

function testapp {
    cd "$PWD"
    appResources="../../resources/team_resources/$APP_NAME"
    if [ ! -d "$appResources" ]; then
        echo "No directory found at $appResources"
        exit 1
    fi
    cp ios/GoogleService-Info.plist ios/GoogleService-Info.plist.original
    rm ios/GoogleService-Info.plist.old
    mv ios/GoogleService-Info.plist ios/GoogleService-Info.plist.old
    cp "$appResources/GoogleService-Info.plist" ios/
    if [ $shouldSetUid = 1 ]; then
        sed -i '' "s/TEST_UID = null/TEST_UID = \"$uid\"/g" app/backend/FirestoreHandler.js
        echo "Set uid in FirestoreHandler.js to $uid"
    fi
}
function revert {
    cd "$PWD"
    mv ios/GoogleService-Info.plist ios/GoogleService-Info.plist.old
    cp ios/GoogleService-Info.plist.original ios/GoogleService-Info.plist
    if [ $shouldSetUid = 1 ]; then
        sed -i '' "s/TEST_UID = \"$uid\"/TEST_UID = null/g" app/backend/FirestoreHandler.js
    fi
}

if [ $ONLY_REVERT = 0 ]; then
    testapp
    echo "$APP_NAME ready to test!"
    echo "HIT ENTER when done."
    read;
fi

echo "Restoring original state..."
revert
if [ $ONLY_REVERT = 1 ]; then
    code "$ROOT/app/backend/FirestoreHandler.js"
fi
echo "Done! (remember: this command only supports iOS)"
