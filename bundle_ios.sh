#!/bin/bash

appName="${PWD##*/}"
echo -n -e "\033]0;$appName\007"

echo ""
echo "Starting iOS bundle script in $PWD"

react-native bundle --entry-file index.js --platform ios --dev false --bundle-output ios/main.jsbundle --assets-dest ./assets

echo "iOS JavaScript bundle complete!"
