#!/bin/bash

cd ios
if [ -d "fastlane/metadata" ]; then
    echo "fastlane/metadata exists, therefore not downloading new data."
else
    fastlane deliver init
fi

currentVersion=$(agvtool mvers -terse1)

cd fastlane/metadata/en-US
rm release_notes.txt
echo "Latest updates and features. (version $currentVersion)" >> release_notes.txt

cd ../../..
fastlane submit_review

echo ""
echo "Complete."
