const minimumArgs = 2;

if (process.argv.length < minimumArgs + 2) {
  // + 2 because node calculator.js
  console.log(
    "Incorrect use of command. Do this instead: node calculator.js <functionKey> <someValue>"
  );
  process.exit();
}

const commandKey = process.argv[2];
const value = process.argv[3];

if (commandKey === "extractLiveVersion") {
  // ANCHOR: extractLiveVersion
  let output = value;
  const _liveVersion = "liveVersion: ";
  output = output.substring(
    output.indexOf(_liveVersion) + _liveVersion.length,
    output.length
  );
  output = output.substring(0, output.indexOf(";"));

  console.log(output);
} else if (commandKey === "getNextVersion") {
  // ANCHOR: getNextVersion
  let nextVersion = value;
  if (nextVersion.split(".").length < 3) {
    let _result = (Number.parseFloat(nextVersion) + 0.1).toString();
    if (!_result.includes(".")) _result += ".0";
    if (_result.length > 3) _result = _result.substring(0, 3);
    // NOTE: this last if statement is no longer use versions like 8.1 and start using 8.1.1 in order to make fastlane and Apple friends again?
    if (_result.split(".").length < 3) _result += ".1";
    console.log(_result);
    return;
  }
  let nstr = (Number.parseInt(nextVersion.replace(/\./g, "")) + 1).toString();
  nextVersion = "";
  for (let i = 0; i < nstr.length; i++) {
    const num = Number.parseInt(nstr[i]);
    nextVersion += num + (nstr.length - 1 == i ? "" : ".");
  }
  while (nextVersion.split(".").length - 1 >= 3)
    nextVersion = nextVersion.replace(".", "");
  console.log(nextVersion);
} else {
  console.log(`Command key \"${commandKey}\" not found.`);
}
