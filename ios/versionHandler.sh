#!/bin/bash

function getLiveVersion() {
    versionOutput=$(fastlane live_version)
    node calculator.js extractLiveVersion "$versionOutput"
}
function getVersion {
    _v=$(agvtool mvers -terse1)
    echo "$_v"
}
function getNextVersion { # getNextVersion [alt_version]
    _v="$(getVersion)"
    if [ ! -z "$1" ]; then
        _v="$1"
    fi
    node calculator.js getNextVersion "$_v"
}

function setVersion { # setVersion <version_string>
    agvtool new-marketing-version $1
}
# @deprecated in favor of setVersion
function updateVersion { # updateVersion [custom_version]
    if [ -z "$1" ]; then
        agvtool new-marketing-version $(getNextVersion)
    else
        agvtool new-marketing-version $1
    fi
}

if [ "$1" = "getLiveVersion" ]; then
    getLiveVersion
elif [ "$1" = "getVersion" ]; then
    getVersion
elif [ "$1" = "getNextVersion" ]; then
    getNextVersion "$2"
elif [ "$1" = "updateVersion" ]; then
    updateVersion $2
else
    echo "Please use the command like this: bash versionHandler.sh <getLiveVersion|getVersion|getNextVersion|updateVersion> [custom_version]"
fi
