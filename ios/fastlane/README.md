fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios submit_review
```
fastlane ios submit_review
```
Submit latest build for AppStore review
### ios beta
```
fastlane ios beta
```
Push a new beta build to TestFlight
### ios testflight_external
```
fastlane ios testflight_external
```
Distribute build to external Test Flight testers.
### ios live_version
```
fastlane ios live_version
```
Get the latest live version from AppStore Connect

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
