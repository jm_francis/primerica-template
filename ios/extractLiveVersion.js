const argIndex = 2;

if (process.argv.length < argIndex + 1) {
  console.log(
    "Incorrect use of command. Do this instead: node extractLiveVersion.js <output>"
  );
  process.exit();
}

let output = process.argv[argIndex];

const _liveVersion = "liveVersion: ";
output = output.substring(
  output.indexOf(_liveVersion) + _liveVersion.length,
  output.length
);
output = output.substring(0, output.indexOf(";"));

console.log(output);
