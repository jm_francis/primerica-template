#!/bin/bash

FASTLANE_ITUNES_TRANSPORTER_PATH="$(pwd)/fastlane_workaround" caffeinate fastlane beta

if [ "$1" = "external" ]; then
    echo "Attempting to distribute to external testers..."
    fastlane testflight_external
fi
