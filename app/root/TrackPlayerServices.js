import TrackPlayer from "react-native-track-player";

const config = () => {
  TrackPlayer.updateOptions({
    stopWithApp: false,
    jumpInterval: 15,
    capabilities: [
      TrackPlayer.CAPABILITY_PLAY,
      TrackPlayer.CAPABILITY_PAUSE,
      TrackPlayer.CAPABILITY_STOP,
      //TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
      //TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
      TrackPlayer.CAPABILITY_SEEK_TO,
      TrackPlayer.CAPABILITY_JUMP_FORWARD,
      TrackPlayer.CAPABILITY_JUMP_BACKWARD
    ]
  });
};

const stateChangeSubs = [];
export const subscribe = (service, callback) => {
  if (service === "state") stateChangeSubs.push(callback);
};

const trackChanged = () => {
  // ...
};
const stateChanged = state => {
  for (s in stateChangeSubs) stateChangeSubs[s](state);
};

const play = () => {
  TrackPlayer.play();
};
const pause = () => {
  TrackPlayer.pause();
};
const stop = () => {
  TrackPlayer.destroy();
};
const seek = data => {
  TrackPlayer.seekTo(data.position);
};
const jumpForward = data => {
  TrackPlayer.getPosition().then(seconds => {
    TrackPlayer.seekTo(seconds + data.interval);
  });
};
const jumpBackward = data => {
  TrackPlayer.getPosition().then(seconds => {
    TrackPlayer.seekTo(seconds - data.interval);
  });
};

export const TrackPlayerServices = () => {
  return new Promise((resolve, reject) => {
    config();

    TrackPlayer.addEventListener("remote-play", play);
    TrackPlayer.addEventListener("remote-pause", pause);
    TrackPlayer.addEventListener("remote-seek", seek);
    TrackPlayer.addEventListener("remote-jump-forward", jumpForward);
    TrackPlayer.addEventListener("remote-jump-backward", jumpBackward);
    TrackPlayer.addEventListener("remote-stop", stop);

    TrackPlayer.addEventListener("playback-track-changed", trackChanged);
    TrackPlayer.addEventListener("playback-state", stateChanged);

    resolve(true);
  });
};
