import Backend from "backend/";
import K from "constant-deprecated";
import { APP_LOGO_TRANSPARENT } from "constant-deprecated/Images";
import React, { Component } from "react";
import {
  Dimensions,
  Image,
  KeyboardAvoidingView,
  TextInput, View
} from "react-native";
import { moderateScale, verticalScale } from "utilities/";
import Background from "./Background";


export default class AppPassword extends Component {
  state = {
    password: ""
  };

  async textChange(password) {
    this.setState({ password });

    await Backend.localHandler.setAppPassword(password);
    const testPassword = await Backend.localHandler.testAppPassword();
    if (testPassword) {
      this.props.navigation.navigate("Home");
    }
  }

  render() {
    return (
      <View style={Styles.container}>
        <Background />

        <Image source={APP_LOGO_TRANSPARENT} style={Styles.teamLogo} />

        <KeyboardAvoidingView>
          <TextInput
            value={this.state.password}
            onChangeText={this.textChange.bind(this)}
            style={Styles.input}
            placeholder="Enter Team Password"
            placeholderTextColor="rgb(235,235,235)"
            autoCapitalize={"characters"}
            autoCorrect={false}
            autoFocus={true}
            selectionColor="white"
          />
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const screenWidth = Dimensions.get("window").width;

const Styles = {
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  teamLogo: {
    position: "absolute",
    top: "15%",
    width: verticalScale(90),
    height: verticalScale(90)
  },
  input: {
    width: screenWidth * 0.9,
    color: "white",
    fontFamily: K.Fonts.medium,
    fontSize: moderateScale(35, 0.41),
    textAlign: "center",
    borderBottomWidth: 2,
    borderColor: "white"
  }
};
