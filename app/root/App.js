//@ts-check
import * as eva from "@eva-design/eva";
import { ApplicationProvider } from "@ui-kitten/components";
// import notificationService from "backend/NotificationHandler/NotificationService";
import { Toast, toastConfig } from "components/";
import AppNavigator from "navigation/AppNavigator";
import React, { Component } from "react";
import {
  Dimensions,
  StatusBar,
} from "react-native";
import { SafeAreaProvider, SafeAreaView } from "react-native-safe-area-context";
import SplashScreen from "react-native-splash-screen";
import { C, primrConfig, primrLight } from "utilities/";

// iPhone 12 Max? - 428 x 926
// iPhone 12 -

export default class App extends Component {
  componentDidMount() {
    // this.checkMessage();
    SplashScreen.hide();

    setTimeout(() => {
      console.log(
        "** " +
          Dimensions.get("window").width +
          " " +
          Dimensions.get("window").height
      );
    }, 2500);
  }

  // async checkMessage() {
  //   const enabled = await firebase.messaging().hasPermission();
  //   if (enabled) {
  //     // user has permissions
  //     // const fcmToken = await firebase.messaging().getToken();
  //     // console.log("fcmToken: ", fcmToken);
  //     this.setFCMToken();
  //     return new notificationService(null);
  //   } else {
  //     // user doesn't have permission
  //     return firebase
  //       .messaging()
  //       .requestPermission()
  //       .then(async (res) => {
  //         // const fcmToken = await firebase.messaging().getToken();
  //         // console.log("fcmToken: ", fcmToken);
  //         this.setFCMToken();
  //         return new notificationService(null);
  //       })
  //       .catch((error) => console.warn("err messaging: ", error));
  //   }
  // }

  render() {
    return (
      <ApplicationProvider
        {...eva}
        theme={{ ...primrLight }}
        customMapping={primrConfig}
      >
        <SafeAreaProvider>
          {/* <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 20}
            style={{
              flex: 1,
              backgroundColor: "blue",
              justifyContent: "flex-end",
            }}
          > */}
          <SafeAreaView style={{ flex: 1, backgroundColor: C.background01 }}>
            <AppNavigator />
            <StatusBar barStyle="light-content" />
            <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
          </SafeAreaView>
          {/* </KeyboardAvoidingView> */}
        </SafeAreaProvider>
      </ApplicationProvider>
    );
  }
}
