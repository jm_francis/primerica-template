//@ts-check
import Backend from "backend/";
import { Buttoon } from "components/";
import React, { Component } from "react";
import { Alert, Dimensions, Platform, View, Image } from "react-native";
import { checkMultiple, PERMISSIONS, RESULTS } from "react-native-permissions";
import { C } from "utilities/";
// import Background from "./Background";

// NOTE: Documentation
// Decides where to depending on if logged in or not

export default class BootingUp extends Component {
  // Backend inits > is logged in or needs to login

  state = {
    showBugsBunny: false,
  };

  componentDidMount() {
    Backend.init(this.isLoggedIn.bind(this), this.needsToRegister.bind(this));
    setTimeout(() => {
      this.setState({ showBugsBunny: true });
    }, 2000);
    // firestore()
    //   .collection("config")
    //   .doc("variables")
    //   .update({ version: version })
    //   .catch((err) => console.log("error fetching latest ver: ", err));
  }

  async hasAcceptedPermissions() {
    const permissions =
      Platform.OS === "ios"
        ? await checkMultiple([PERMISSIONS.IOS.CONTACTS])
        : await checkMultiple([
            PERMISSIONS.ANDROID.READ_CONTACTS,
            PERMISSIONS.ANDROID.WRITE_CONTACTS,
          ]);
    for (let p in permissions) {
      console.log("perm: " + permissions[p]);
      if (
        permissions[p] !== RESULTS.GRANTED &&
        permissions[p] !== RESULTS.BLOCKED
      )
        return false;
    }
    return true;
  }

  isLoggedIn() {
    Backend.firestoreHandler.config(async (acc) => {
      if (this.configRan === true) return;
      this.configRan = true;

      const testPassword = await Backend.localHandler.testAppPassword();
      if (testPassword) {
        // const permissionsDidOpen = await Backend.localHandler.getPermissionsDidOpen();
        if (await this.hasAcceptedPermissions()) {
          this.props.navigation.navigate("Home");
        } else {
          this.props.navigation.navigate("Permissions");
        }
        // else {
        // Backend.localHandler.setPermissionsDidOpen();
        // this.props.navigation.navigate("Permissions");
        // }

        Backend.firestoreHandler.account(
          ((account) => {
            if (!account) return;

            if (this.didLoadAccount) return;
            else this.didLoadAccount = true;

            if (
              (account.banned === true || account.kicked === true) &&
              account.admin !== true
            ) {
              this.props.navigation.navigate("BootingUp");
              Alert.alert(
                "It looks like you have been banned from the app by an admin. =(",
                "If you think this is incorrect please notify your team leader."
              );
              return;
            }
          }).bind(this)
        );
      } else {
        this.props.navigation.navigate("AppPassword");
      }
    });
  }

  needsToRegister() {
    this.props.navigation.navigate("Register");
  }

  render() {
    const { showBugsBunny } = this.state;
    return (
      <View style={Styles.container}>
        {showBugsBunny ? <$_BugsBunny {...this.props} /> : null}
      </View>
    );
  }
}

function $_BugsBunny(props) {
  const { navigation } = props;
  return (
    <>
      <Image
        source={{
          uri:
            "https://1.bp.blogspot.com/-GH0bj6Geg3Q/UUHNvdKT6lI/AAAAAAAAcio/6wwdRoJld6Q/s1600/3_Left-At-Albuquerque.jpg",
        }}
        resizeMode="contain"
        style={{
          borderRadius: 4,
          width: Dimensions.get("window").width * 0.7,
          height: Dimensions.get("window").width * 0.7,
        }}
      />
      <Buttoon
        icon={{ right: true, name: "arrow_right" }}
        style={{
          backgroundColor: C.awakenVolt,
          borderWidth: 0,
        }}
        onPress={() => {
          if (Backend.firestoreHandler._account) navigation.navigate("Home");
          else navigation.navigate("Register");
        }}
      >
        Go Back
      </Buttoon>
    </>
  );
}

const Styles = {
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: C.background01,
  },
};
