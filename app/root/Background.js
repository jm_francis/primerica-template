import React, { Component } from "react";
import { View, Image, Dimensions } from "react-native";

import { GA_DOME_IMAGE } from "constant-deprecated/Images";

export default class Background extends Component {
  render() {
    return (
      <View style={Styles.container}>
        <Image source={GA_DOME_IMAGE} style={Styles.image} />
      </View>
    );
  }
}

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

const Styles = {
  container: {
    backgroundColor: "black",
    position: "absolute",
    height: screenHeight,
    width: screenHeight,
    transform: [{ translateX: screenWidth / 2 - screenHeight / 2 }]
  },
  image: {
    height: screenHeight,
    width: screenHeight,
    opacity: 0.5
  }
};
