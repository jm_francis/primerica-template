// A Config not controlled by the user, but by the developer

// Memory Jogger Structure
// [{ screenType: "standard" | "message" }, items: [], contacts: []]
// items:
//  { itemType: "text", text: "" }
//  { itemType: "contacts_list" }
//  { itemType: "priority_organizer", priority: { id: "_" } }
//  { itemType: "video", video: "url" }

const MemoryJoggerConfig = {
  priorityTypes: {
    A: {
      id: "A",
      message: "🤝 These people are SUPPORTIVE of you.",
    },
    B: {
      id: "B",
      message: "👨‍👩‍👧 These people can BENEFIT from what we offer.",
    },
    C: {
      id: "C",
      message: "👨🏽‍🔧 These are people you would enjoy WORKING with.",
    },
  },
  groups: [
    {
      name: "Group A",
      requiredPriorities: ["A", "B", "C"],
    },
    {
      name: "Group B",
      requiredPriorities: ["A", "C"],
    },
    {
      name: "Group C",
      requiredPriorities: ["A", "B"],
    },
    {
      name: "Group D",
      requiredPriorities: ["B", "C"],
    },
    {
      name: "Group E",
      requiredPriorities: ["C"],
    },
    {
      name: "Group Z",
      requiredPriorities: ["B"],
    },
    {
      name: "Everyone Else",
      requiredPriorities: [],
    },
  ],
  screens: [
    // start
    {
      screenType: "message",
      message: "Let's\nBrainstorm!\n🤔",
    },
    {
      screenType: "standard",
      items: [
        {
          itemType: "text",
          text:
            "Imagine you are opening a restaurant or other business. Who are 3 people you want to open it with?",
        },
        { itemType: "contacts_list" },
      ],
      contacts: [],
    },
    {
      screenType: "standard",
      items: [
        { itemType: "text", text: "Who are your closest friends?" },
        { itemType: "contacts_list" },
      ],
      contacts: [],
    },
    {
      screenType: "message",
      message: "YOU ARE ON\nA ROLL.\nKEEP GOING!\n🚀🚀🚀",
    },
    {
      screenType: "standard",
      items: [
        {
          itemType: "text",
          text: "Who are your favorite family members or in-laws?",
        },
        { itemType: "contacts_list" },
      ],
      contacts: [],
    },
    {
      screenType: "standard",
      items: [
        { itemType: "text", text: "Favorite co-workers? (past or present)" },
        { itemType: "contacts_list" },
      ],
      contacts: [],
    },
    {
      screenType: "standard",
      items: [
        {
          itemType: "text",
          text:
            "Who are some cool people you know through your church, gym, or other activities?",
        },
        { itemType: "contacts_list" },
      ],
      contacts: [],
    },
    {
      screenType: "standard",
      items: [
        { itemType: "text", text: "Classmates? (past or present)" },
        { itemType: "contacts_list" },
      ],
      contacts: [],
    },
    {
      screenType: "message",
      message: "GREAT!!!\nNext Step.",
    },
    {
      screenType: "standard",
      items: [
        // { text: "Tap beside each person who is married or has kids, and if you invited them to a nice dinner they could afford it.", itemType: "text" },
        {
          itemType: "video",
          video: "MARRIED",
          emoji: "👨‍👩‍👧",
          // video: "https://vimeo.com/426338093/eac4a676f8"
        },
        {
          itemType: "priority_organizer",
          priority: { id: "B" },
        },
      ],
      contacts: [],
    },
    {
      screenType: "standard",
      items: [
        // { text: "Now tap beside each person who you would love to work with, regardless of whether or not they would be interested in this business. For example they’re motivated, trustworthy and fun.", itemType: "text" },
        {
          itemType: "video",
          video: "WORKWITH",
          emoji: "👨🏽‍🔧",
          // video: "https://vimeo.com/426342316/14ed9cc08a"
        },
        {
          itemType: "priority_organizer",
          priority: { id: "C" },
        },
      ],
    },
    {
      screenType: "standard",
      items: [
        // { text: "Next, tap beside each person who is supportive of you. If you need a favor, you could call them and they would definitely help you out.", itemType: "text" },
        {
          itemType: "video",
          video: "SUPPORTIVE",
          emoji: "🤝"
          // video: "https://vimeo.com/426338551/4fd61e9caf"
        },
        {
          itemType: "priority_organizer",
          priority: { id: "A" },
        },
      ],
    },
    {
      screenType: "message",
      message:
        "🔥 NICE WORK! 🔥\n\nNow let's share your list with your trainer!",
    },
    {
      screenType: "share-list",
    },
    {
      screenType: "message",
      message: "CONGRATULATIONS!\n\nSEE YOUR LIST!",
    },
  ], // end
};

export default MemoryJoggerConfig;
