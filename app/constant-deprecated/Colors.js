export default {
  darkGrey: "rgb(20, 20, 20)",
  black: "rgb(13, 13, 13)",
  vimeoBlue: "rgb(90, 209, 246)",
  soundCloudTint: "rgb(254, 85, 74)",
  soundCloudBackground: "rgb(20,20,20)",
  adminPink: "#ba1c93",
};
