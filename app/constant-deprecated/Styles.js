import { Dimensions, Platform } from "react-native";
import { scale, verticalScale, moderateScale } from "utilities";

import Fonts from "./Fonts";

const screenWidth = Dimensions.get("window").width;
const videoWidth = Platform.isPad
  ? screenWidth * 0.93 * 0.5
  : screenWidth * 0.93;
const videoHeight = videoWidth * 0.64;

const Styles = {
  customPage: {
    video: {
      // width: moderateScale(videoWidth,-0.28),
      // height: moderateScale(videoHeight,-0.28),
      width: videoWidth,
      height: videoHeight,
      borderRadius: 7
    }
  },
  title: {
    color: "white",
    fontFamily: Fonts.medium,
    fontSize: 22,
    textAlign: "center"
  }
};

export default Styles;
