const images = "../../assets/images";
const app_images = "../../assets/app_specific_images";
const navigator = images + "/navigator";

// ANCHOR ONFIG IMAGES
export const SNL = require(`${app_images}/SNL.png`); // Video Calls
export const APP_LOGO = require(`${app_images}/logo.png`);
export const APP_LOGO_TRANSPARENT = require(`${app_images}/APP-LOGO-250.png`);
export const NEW_PEOPLE_BACKGROUND = require(`${images}/mapncompass.jpeg`); // New? Tap here!
export const SCOREBOARD_BACKGROUND = require(`${app_images}/scoreboard-background.png`);

// ANCHOR PRESET LOGOS
export const SOCIAL_MEDIA = require(`${images}/presetLogos/social_media.png`);
export const PARTNERSHIP = require(`${images}/presetLogos/partnership.png`);
export const TOOLBOX = require(`${images}/presetLogos/toolbox.png`);
export const BUILDING = require(`${images}/presetLogos/building.png`);
export const RECRUITING = require(`${images}/presetLogos/recruiting.png`);
export const ROCKET = require(`${images}/presetLogos/rocket.jpg`);
export const BIG_EVENT = require(`${images}/presetLogos/big_event.png`);
export const MONEY_HOUSE = require(`${images}/presetLogos/money_house.png`);
export const PIE_CHART = require(`${images}/presetLogos/pie_chart.png`);

export const presetLogos = Object.freeze({
  none: null,
  social_media: SOCIAL_MEDIA,
  partnership: PARTNERSHIP,
  toolbox: TOOLBOX,
  social_media: SOCIAL_MEDIA,
  building: BUILDING,
  recruiting: RECRUITING,
  rocket: ROCKET,
  big_event: BIG_EVENT,
  money_house: MONEY_HOUSE,
  pie_chart: PIE_CHART
});

// ANCHOR TAB BAR ICONS
export const HOME_ICON = require(`${navigator}/list.png`);
export const TV_ICON = require(`${navigator}/television.png`);
export const VIDEO_CALLS_ICON = require(`${navigator}/camera.png`);
export const CALENDAR_ICON = require(`${navigator}/calendar.png`);
export const MORE_ICON = require(`${navigator}/more.png`);

export const GA_DOME_IMAGE = require(`${app_images}/background.png`);

// ANCHOR My Prospecting Icons
export const CLIPBOARD = require(`${images}/clipboard.png`);
export const GROUP_OF_PEOPLE = require(`${images}/group_of_people.png`);
export const PROFILE = require(`${images}/profile.png`);

// ANCHOR MEDIA
export const PLAY_BUTTON = require(`${images}/play_button.png`);
export const VIMEO_PLAY = require(`${images}/vimeosquareplay.png`);
export const PLAY = require(`${images}/play.png`);
export const LOADING = require(`${images}/loading.png`);

// ANCHOR BASIC
export const BACK_BUTTON = require(`${images}/back_button.png`);
export const LEFT_ARROW = require(`${images}/left-arrow.png`);
export const RIGHT_ARROW = require(`${images}/right-arrow.png`);
export const UP_ARROW = require(`${images}/uparrow.png`);
export const SHARE_BUTTON = require(`${images}/share.png`);
export const X_ICON = require(`${images}/x.png`);
export const PLUS_ICON = require(`${images}/plus.png`);
export const SMARTPHONE = require(`${images}/smartphone.png`);
export const EMAIL = require(`${images}/email.png`);
export const TEXT_MESSAGE = require(`${images}/textmessage.png`);
export const TRASH = require(`${images}/trash.png`);
export const EDIT_PERSON = require(`${images}/editperson.png`);
export const STAR = require(`${images}/star.png`);
export const CHECK = require(`${images}/check.png`);
export const LIST_BUILDER = require(`${images}/list_builder.png`);
export const MEMORY_JOGGER = require(`${images}/memoryjogger.png`);
export const CROWN = require(`${images}/crown.png`);
export const LOCK_ICON = require(`${images}/lock.png`);
export const MAIL = require(`${images}/mail.png`);
export const DOCUMENT = require(`${images}/document.png`);

// ANCHOR MEDALS
export const GOLD_MEDAL = require(`${images}/gold-medal.png`);
export const SILVER_MEDAL = require(`${images}/silver-medal.png`);
export const BRONZE_MEDAL = require(`${images}/bronze-medal.png`);
export const CRYING_FACE = require(`${images}/crying.png`);
export const SAD_FACE = require(`${images}/sad.png`);
export const MID_FACE = require(`${images}/indifferent.png`);
export const LIGHTNING = require(`${images}/lightning.png`);
