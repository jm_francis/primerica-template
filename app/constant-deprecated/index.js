// @ts-check
// import Images from "./Images";
// import Videos from "./Videos";
import Styles from "./Styles";
import Colors from './Colors'
import Fonts from "./Fonts";
import Config from "./Config";
import Keys from "./Keys";
import Variables from "./Variables"
import MemoryJoggerConfig from "./MemoryJoggerConfig";

/**
 * @deprecated use Khoi's updated utilities/
 */
const K = {
  // Images,
  // Videos,
  Colors,
  Styles,
  Fonts,
  Config,
  Keys,
  Variables,
  MemoryJoggerConfig,
};
export default K;

export {
  // Images,
  // Videos,
  /**
   * @deprecated
   */
  Colors,
  /**
   * @deprecated
   */
  Styles,
  /**
   * @deprecated
   */
  Fonts,
  Config,
  Keys,
  Variables,
  MemoryJoggerConfig,
};
