import { Platform } from "react-native";
const ios = Platform.OS === "ios";

const Fonts = {
  medium: ios ? "AvenirNext-Medium" : "Lato-Regular",
  bold: ios ? "AvenirNext-Bold" : "Lato-Black",
  typewriter: ios ? "AmericanTypewriter-Light" : "atwriter"
};

export default Fonts;
