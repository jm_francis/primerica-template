import Backend, { PosstSchema } from "backend/";
import FileNetwork from "backend/apis/FileNetwork";
import { Buttoon, IconPrimr, Txt } from "components/";
import R from "ramda";
import React, { useEffect, useState } from "react";
import { Controller, useFormContext } from "react-hook-form";
import { View } from "react-native";
import Image from "react-native-scalable-image";
import { C, DEVICE_WIDTH, fn, scale } from "utilities/";
import { ERR, FORM, SS } from "./PosstCreatorPage";
import { Videoh } from "./Videoh";

export function $_PoostMedia() {
  let _personali = React.useMemo(() => Backend.firestoreHandler._account, []);
  const { control, setValue } = useFormContext();
  //#region [SECTION ]  FORMING GLOBALLY
  const [media, setMedia] = React.useState<PosstSchema["medias"][0]>(null);
  useEffect(
    function formMedia() {
      setValue(FORM.MEDIA, media);
    },
    [media]
  );
  //#endregion
  //#region [SECTION ] MEDIA-PICKER
  const [progress, setProgress] = useState(0);
  const [uploadError, setUploadError] = useState("");

  // const [width, setWidth] = useState(300);
  // const [height, setHeight] = useState(300);
  async function pickMedia() {
    FileNetwork.selectAndUpload(
      {
        title: "posst_" + _personali.uid + fn.js.ID(3),
        type: "any",
      },
      (progress) => {
        console.log("progress: ", progress);
        setProgress(Math.round(progress));
      }
    ).then((result) => {
      setMedia({ uri: result.uri, type: result.type });
      console.log("link url: ", result.uri);
    });
    // .catch((e) => {
    //   setUploadError(JSON.stringify(e));
    // });
  }
  //#endregion
  function MediaPlayer() {
    if (media.type.includes("image")) {
      return (
        <Image
          resizeMode="contain"
          source={{
            uri: FileNetwork.getThumbnailFromURL(media.uri),
          }}
          width={DEVICE_WIDTH}
        />
      );
    }
    if (media.type.includes("video") && media.uri) {
      return (
        <Videoh uri={media.uri} />
        // <Videoh
        //   resizeMode="cover"
        //   video={{ uri: media.uri }}
        //   thumbnail={{ uri: FileNetwork.getThumbnailFromURL(media.uri) }}
        //   // videoHeight={height}
        // />
      );
    }
    return <></>;
  }

  try {
    // if (1 == 1) throw Error(ERR.UPLOADING_MEDIA);
    if (!R.isEmpty(uploadError)) throw Error(ERR.UPLOADING_MEDIA);

    return (
      <Controller
        control={control}
        render={({}) => {
          return (
            <SS.CtnrMedia
              disabled={!!media} //* disable onPress if `media` exists
              onPress={pickMedia}
              style={{ borderWidth: media ? 0 : 1 }}
            >
              <View
                style={{
                  alignItems: "center",
                }}
              >
                {media ? (
                  <MediaPlayer />
                ) : progress ? (
                  <SS.TxtUploading>
                    {"Uploading " + progress + "%"}
                  </SS.TxtUploading>
                ) : (
                  <View
                    style={{
                      alignItems: "center",
                    }}
                  >
                    <IconPrimr name="image" size={scale(30)} color={C.dim} />
                    <Txt.Indicator>
                      {"A powerful post needs an image or a video.\nAdd one?"}
                    </Txt.Indicator>
                  </View>
                )}
              </View>
            </SS.CtnrMedia>
          );
        }}
        name={FORM.MEDIA}
        rules={{ required: true }}
        defaultValue={{
          uri: "",
          type: "",
        }}
      />
    );
  } catch (error) {
    switch (error.message) {
      case ERR.UPLOADING_MEDIA:
        return (
          <SS.CtnrMedia
            style={{ justifyContent: "center", alignItems: "center" }}
          >
            <Txt.Indicator>Error Uploading Media</Txt.Indicator>
            <Buttoon
              size="tiny"
              appearance="outline"
              status="warning"
              onPress={() => {
                setUploadError("");
                setProgress(0);
              }}
            >
              Try again
            </Buttoon>
          </SS.CtnrMedia>
        );
        break;
      default:
        return (
          <SS.CtnrMedia
            style={{ justifyContent: "center", alignItems: "center" }}
          >
            <Txt.Indicator>Error Uploading Media</Txt.Indicator>
          </SS.CtnrMedia>
        );
        break;
    }
  }
}
