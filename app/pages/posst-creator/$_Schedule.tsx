import {
    dAccessory,
    IconPrimr,
    Kitten,


    Txt
} from "components/";
import React from "react";
import { LayoutAnimation, View } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { C } from "utilities/";
import { dateFormat, prettyTime } from "utilities/functions/calendar-functions";

export function $_Schedule(props: {
  onDateSelect: (date: Date) => void;
  disabled?: boolean;
}) {
  const { onDateSelect, disabled } = props;

  const selectedDate = React.useRef<Date>();
  const [_datePickerVisible, setDatePickerVisible] = React.useState(false);
  const [_timePickerVisible, setTimePickerVisible] = React.useState(false);
  const [_showSelectedDate, showSelectedDate] = React.useState(false);

  React.useEffect(
    function resetComponent() {
      if (disabled)
        showSelectedDate(false);
    },
    [disabled]
  );

  let _prettyDate = selectedDate.current && dateFormat(selectedDate.current);
  if (_prettyDate && _prettyDate.substring(0, 2).startsWith("0"))
    _prettyDate = _prettyDate.substring(1, _prettyDate.length);
  //
  return (
    <>
      <Kitten.ListItem
        title="Schedule"
        description="Tap to set"
        style={{ backgroundColor: C["transparent"] }}
        onPress={() => {
          setDatePickerVisible(true);
        }}
        accessoryLeft={(p: dAccessory) => (
          <IconPrimr name="schedule" color={C.text01} />
        )}
        accessoryRight={(p: dAccessory) => _showSelectedDate ? (
          <View
            style={{
              minHeight: 45,
              justifyContent: "center",
            }}
          >
            <Txt.S1
              style={{
                color: C.hazardYellow,
                textAlign: "right",
              }}
            >{`Schedule for \n${_prettyDate} @ ${prettyTime(
              selectedDate.current
            )}?`}</Txt.S1>
          </View>
        ) : null} />
      <DateTimePickerModal
        minimumDate={new Date()}
        isVisible={_datePickerVisible}
        onConfirm={(date) => {
          selectedDate.current = date;
          setDatePickerVisible(false);
          setTimeout(() => {
            setTimePickerVisible(true);
          }, 325);
        }}
        onCancel={() => setDatePickerVisible(false)} />
      <DateTimePickerModal
        date={new Date()}
        mode="time"
        isVisible={_timePickerVisible}
        onConfirm={(date) => {
          selectedDate.current.setHours(date.getHours());
          selectedDate.current.setMinutes(date.getMinutes());
          setTimePickerVisible(false);
          showSelectedDate(true);
          onDateSelect(selectedDate.current);
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        }}
        onCancel={() => setTimePickerVisible(false)} />
    </>
  );
}
