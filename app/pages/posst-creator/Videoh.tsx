//@ts-check
import Backend from "backend/";
import Video from "react-native-video";
// import { Video as OverrideVideo } from "override/Video";
import React from "react";
import {
  Dimensions,
  Image,
  Platform,
  TouchableOpacity,
  View,
} from "react-native";
import { AndroidVideo } from "./AndroidVideo";
import { IconPrimr } from "components/";
import { moderateScale } from "utilities/";
const { FileNetwork } = Backend;

interface PropTypes {
  uri: string;
}

export class Videoh extends React.Component<PropTypes> {
  state = {
    video: null,
    paused: false,
  };

  handleProgress(time) {}
  videoLoaded() {}
  playbackEnded() {
    this.close();
  }
  close() {
    // this.state.videoRef.dismissFullscreenPlayer();
    this.setState({ video: null });
  }
  async playVideo() {
    const { uri } = this.props;

    const VideoComponent = (_props) =>
      Platform.OS === "ios" ? (
        <Video {..._props} />
      ) : (
        <AndroidVideo {..._props} />
      );
    const video = (
      <VideoComponent
        resizeMode="cover"
        style={
          Platform.OS === "ios"
            ? {
                overflow: "hidden",
                position: "absolute",
                top: 0,
                width,
                height,
              }
            : {}
        }
        controls={true}
        ref={(ref) => {
          this.setState({ videoRef: ref });
          // this.videoRef = ref;
        }}
        ignoreSilentSwitch={"ignore"}
        paused={false}
        onLoad={this.videoLoaded.bind(this)}
        onProgress={this.handleProgress.bind(this)}
        onEnd={this.playbackEnded.bind(this)}
        onFullscreenPlayerWillDismiss={this.playbackEnded.bind(this)}
        onVideoFullscreenPlayerDidDismiss={this.playbackEnded.bind(this)}
        source={{ uri: FileNetwork.blurredBorderFromURL(uri) }}
        pictureInPicture={true}
        fullscreen={true}
        preventsDisplaySleepDuringVideoPlayback={true}
      />
    );

    this.setState({ video: null }, () => {
      this.setState({ video });
    });
  }

  render() {
    const { uri } = this.props;
    return (
      <>
        <TouchableOpacity
          disabled={this.state.video ? true : false}
          style={Styles.touchStyle}
          onPress={this.playVideo.bind(this)}
        >
          <View style={Styles.body}>
            <View style={Styles.image}>
              <Image
                source={{ uri: FileNetwork.getThumbnailFromURL(uri) }}
                style={Styles.image}
              />
            </View>
            {/* <View style={Styles.absolute}>
              <PlayButton />
            </View> */}
            <View style={Styles.absolute}>
              <IconPrimr
                name="play"
                size={moderateScale(32)}
                color="white"
                containerStyle={{
                  opacity: 0.8
                }} />
            </View>
          </View>
          {this.state.video}
        </TouchableOpacity>
      </>
    );
  }
}

const width = Dimensions.get("window").width;
const height = width * 0.7;

const Styles = {
  touchStyle: {
    alignSelf: "center",
  },
  body: {
    width,
    height,
    backgroundColor: "black",
    overflow: "hidden",
  },
  image: {
    width,
    height,
    justifyContent: "center",
    alignItems: "center",
  },
  absolute: {
    position: "absolute",
    width,
    height,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
};
