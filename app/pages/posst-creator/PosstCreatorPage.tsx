//#region [IMPORT]
import { Input } from "@ui-kitten/components";
import Backend, { MediaPageSchema, PosstSchema } from "backend/";
import {
  Buttoon,
  dAccessory,
  IconPrimr,
  Kitten,
  sstyled,
  Toasty,
  Txt,
} from "components/";
import R from "ramda";
import React, { useState } from "react";
import {
  Controller,
  FormProvider,
  useForm,
  useFormContext,
} from "react-hook-form";
import { Keyboard, ScrollView, View } from "react-native";
import { Modalize } from "react-native-modalize";
import { C, fn, spacing } from "utilities/";
import { extractLinkFromBody } from "utilities/functions/js-functions";
import { $_PoostMedia } from "./$_PoostMedia";
import { $_Schedule } from "./$_Schedule";
import { SH_PageSelect } from "./sh-page-select";
//#endregion

/**
 * ### A page to create a posst
 * -  Using `react-hook-form` for ease of use.
 *  - This hook returns a `rawForm` thru `getValues(_)`
 *  - See enum FORM for the form's keys
 *  - `rawForm` is then convert to `doneForm` with `PosstSchema`
 * and ready to upload to frbs
 *
 * ---
 * @example As is
 * ---
 * @version 1.1.21
 * @author nguyenkhooi
 */
export function PosstCreatorPage(props) {
  const { navigation } = props;
  const refTextPost = React.useRef<Input>();
  const creatorForm = useForm();
  const [height, setHeight] = useState(42);
  /**
   * Whether or not a url is found in the body
   */
  const [_activeURL, setActiveURL] = useState(false);
  const [_linkedPage, setLinkedPage] = useState<MediaPageSchema>(null);
  const [_scheduledDate, setScheduledDate] = useState<Date>(null);
  // const [poostsC0, poostLoading, poostError] = useCollection(
  //   firestore().collection(FPATH.POOSTS)
  // );
  const pageSelectRef = React.useRef<Modalize>();

  return (
    <FormProvider {...creatorForm}>
      <SS.Sctnr>
        <Kitten.TopNavigation
          // style={{ ...SS.CTNR_HEADER }}
          style={{ backgroundColor: C.background01 }}
          title={"Create a post"}
          alignment="center"
          accessoryLeft={() => (
            <IconPrimr
              name={"arrow_left"}
              size={20}
              color={C.text01}
              onPress={() => navigation.pop()}
            />
          )}
        />
        <SS.CtnrBuilder
          onPress={() => refTextPost.current.focus()}
          header={$_CreatorHeader}
          footer={(p) =>
            $_CreatorFooter({
              ...p,
              ...props,
              goToPage: _linkedPage && _linkedPage.id,
              isScheduled: !!_scheduledDate,
            })
          }
        >
          <Controller
            control={creatorForm.control}
            render={({ onChange, onBlur, value }) => (
              <>
                <Kitten.Input
                  status={value == "" && "warning"}
                  ref={refTextPost}
                  onBlur={() => {
                    onBlur();
                    Keyboard.dismiss();
                  }}
                  onChangeText={(value) => {
                    onChange(value);
                    const link = extractLinkFromBody(value);
                    setActiveURL(link && link.length > 0 ? true : false);
                  }}
                  value={value}
                  placeholder={"What's on your mind?"}
                  multiline={true}
                  style={{
                    padding: spacing(3),
                    backgroundColor: "transparent",
                  }}
                  textStyle={{ minHeight: 64, height: height }}
                  onContentSizeChange={(e) =>
                    setHeight(e.nativeEvent.contentSize.height)
                  }
                />
                {_activeURL ? (
                  <Txt.C1
                    style={{
                      alignSelf: "flex-end",
                      marginTop: -spacing(3),
                      marginRight: spacing(4),
                    }}
                  >
                    Link Found 🔗
                  </Txt.C1>
                ) : null}
                {/* <Txt.Indicator>
                {
                  "Tip: Including external link can turn your post into a hypeerlink  "
                }
              </Txt.Indicator> */}
              </>
            )}
            name={FORM.BODY}
            rules={{ required: true }}
            defaultValue=""
          />
          <$_PoostMedia />
          <Kitten.ListItem
            title="Link to page"
            description="Tap to select"
            style={{ backgroundColor: C["transparent"] }}
            onPress={() => {
              pageSelectRef.current.open();
            }}
            accessoryLeft={(p: dAccessory) => (
              <IconPrimr name="link" color={C.text01} />
            )}
            accessoryRight={(p: dAccessory) => (
              <Txt.S1
                style={{
                  color: C.infoBlue,
                }}
              >
                {_linkedPage?.name}
              </Txt.S1>
            )}
          />
          <Controller
            control={creatorForm.control}
            render={({ onChange, onBlur, value }) => (
              <>
                <$_Schedule
                  onDateSelect={(date) => {
                    onChange(date);
                    setScheduledDate(date);
                  }}
                />
                {/* <Txt.Indicator>
                {
                  "Tip: Including external link can turn your post into a hypeerlink  "
                }
              </Txt.Indicator> */}
              </>
            )}
            name={FORM.DATE}
            rules={{ required: true }}
            defaultValue={null}
          />
        </SS.CtnrBuilder>
      </SS.Sctnr>
      <SH_PageSelect
        ref={pageSelectRef}
        onSelect={(page: MediaPageSchema) => {
          setLinkedPage(page);
        }}
        onNone={() => setLinkedPage(null)}
      />
    </FormProvider>
  );
}

const $_CreatorFooter = (p: { goToPage?: string; isScheduled?: boolean }) => {
  // const { userProfile } = useAppContext();
  // const personali = React.useMemo(() => userProfile?.personali, [userProfile]);
  let _personali = React.useMemo(() => Backend.firestoreHandler._account, []);
  const { navigation: Navigation, goToPage, isScheduled } = p;
  const { getValues } = useFormContext();

  const [] = useState(null);

  return (
    _personali && (
      <View
        style={{
          padding: spacing(4),
          flexDirection: "row",
          alignSelf: "flex-end",
        }}
      >
        <Buttoon
          status="basic"
          appearance="ghost"
          size="medium"
          onPress={() => {
            Navigation.goBack();
          }}
        >
          {"Cancel"}
        </Buttoon>
        <Buttoon
          size="medium"
          progress={true}
          onPress={async (xong) => {
            let _rawForm = getValues();
            try {
              //#region [SECTION ] For Frontend testing

              // Alert.alert(
              //   "Result",
              //   JSON.stringify({
              //     body: _rawForm[FORM.BODY] || "",
              //     goToPage,
              //     scheduledAt: _rawForm[FORM.DATE],
              //   })
              // );

              //#endregion

              if (
                //* Cannot post with empty body or media
                R.isEmpty(_rawForm[FORM.BODY]) ||
                R.isEmpty(_rawForm[FORM.MEDIA])
                // R.isEmpty(_rawForm[FORM.DATE]) ||
              )
                throw Error("NOT_COMPLETED");
              const finalPosst: Partial<PosstSchema> = {
                body: _rawForm[FORM.BODY] || "",
                goToPage,
                scheduledAt: _rawForm[FORM.DATE],
                medias: _rawForm[FORM.MEDIA] ? [_rawForm[FORM.MEDIA]] : [],
              };
              await Backend.exploreHandler.createPosst(finalPosst);
              Navigation.goBack();
              if (!finalPosst.scheduledAt) {
                Toasty({ text1: "Post successful!" });
              } else {
                Toasty({
                  text1: "Your post has been scheduled",
                  text2: "Your post will be sent out at the specified time.",
                });
              }
            } catch (error) {
              switch (error.message) {
                case "NOT_COMPLETED":
                  xong();
                  return Toasty({
                    type: "error",
                    text1: "No, an empty post is not a thing",
                    text2: "Please add some content.",
                    visibilityTime: 4000,
                  });
                  break;
                default:
                  console.log(error);
                  return Toasty({
                    type: "error",
                    text1: "Something went wrong.",
                    text2: error,
                  });
                  break;
              }
            }
            xong();
          }}
        >
          {isScheduled ? "Schedule" : "Post"}
        </Buttoon>
      </View>
    )
  );
};

const $_CreatorHeader = () => {
  // const { userProfile } = useAppContext();
  // const _personali = React.useMemo(() => userProfile?.personali, [userProfile]);
  let _personali = React.useMemo(() => Backend.firestoreHandler._account, []);

  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "flex-start",
        padding: spacing(2),
      }}
    >
      <Kitten.Avatar
        source={{ uri: _personali?.profileImage || "" }}
        size="tiny"
        style={{
          alignSelf: "center",
          marginRight: spacing(3),
          backgroundColor: C.surface,
        }}
      />
      <View>
        <Txt.C1 style={{ fontWeight: "bold" }}>{_personali?.name}</Txt.C1>
        <Txt.C1>{fn.calendar.dateFormat(new Date())}</Txt.C1>
      </View>
    </View>
  );
};

export enum ERR {
  UPLOADING_MEDIA = "UPLOADING_MEDIA",
}

// _handleImage(result.uri, { userID: uid, bucket: "users", contentType });

export const SS = {
  Sctnr: sstyled(ScrollView)((p) => ({
    flex: 1,
    // width: 500,
    // backgroundColor: p.C.awakenVolt,
    // borderWidth: 2,
    // borderColor: p.C.dim,
    // paddingTop: p.vs(30),
  })),
  CtnrBuilder: sstyled(Kitten.Card)((p) => ({
    backgroundColor: p.C.background01,
  })),
  CtnrMedia: sstyled(Kitten.Card)((p) => ({
    // width: "100%",
    minHeight: 100,
    margin: spacing(3),
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    borderColor: p.C.dim,
    // backgroundColor: p.C.background01,
  })),
  TxtUploading: sstyled(Txt.Indicator)((p) => ({
    color: p.C.hazardYellow,
  })),
};

/**
 * ###  Naming system for form hook
 */
export enum FORM {
  BODY = "form-body",
  MEDIA = "form-media",
  DATE = "form-date",
}
