import { Input, List, ListItem } from "@ui-kitten/components";
import Backend from "backend/";
import { Buttoon } from "components";
import React from "react";
import { Modalize } from "react-native-modalize";
import {
  C,
  DEVICE_HEIGHT,
  dMediaPageItem as MediaPageSchema,
  spacing
} from "utilities/";

interface P {
  onSelect: (page: MediaPageSchema) => void;
  onNone: () => void;
}

export const SH_PageSelect = React.forwardRef<Modalize, P>((props: P, ref) => {
  const { onSelect, onNone } = props;

  const [pages, setPages] = React.useState<MediaPageSchema[]>([]);
  const [_searchValue, setSearchValue] = React.useState("");

  React.useEffect(function fetchPages() {
    Backend.firebasePages.pages((pages) => setPages(pages));
  }, []);

  const searchPages = pages.filter((pg: MediaPageSchema) =>
    pg?.name?.toLowerCase().includes(_searchValue.toLowerCase())
  );

  console.log(`PAGES: ${pages.length} vs SEARCH_PAGES: ${searchPages.length}`);

  return (
    <Modalize
      ref={ref}
      scrollViewProps={{
        keyboardShouldPersistTaps: "always",
      }}
      modalHeight={DEVICE_HEIGHT * 0.72}
      modalStyle={{
        backgroundColor: C.surface01,
        padding: spacing(3),
        // borderWidth: 1,
        // borderColor: C["color-primary-500"],
        borderBottomWidth: 0,
      }}
      HeaderComponent={() => (
        <Input
          autoFocus={true}
          placeholder="Search for a page..."
          value={_searchValue}
          onChangeText={(text) => setSearchValue(text)}
        />
      )}
    >
      <Buttoon
        appearance="outline"
        onPress={() => {
          onNone();
          ref.current.close();
        }}
      >
        None
      </Buttoon>
      <List
        style={{
          width: "100%",
        }}
        data={searchPages}
        renderItem={({ item }) => (
          <ListItem
            title={item.name}
            onPress={() => {
              onSelect(item);
              ref.current.close();
            }}
          />
        )}
      />
    </Modalize>
  );
});
