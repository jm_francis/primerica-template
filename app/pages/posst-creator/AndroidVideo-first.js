//@ts-check
import React from "react";
import { View, Modal as _Modal, Platform, Dimensions } from "react-native";
import Orientation from "react-native-orientation";
import VideoPlayer from "react-native-video-controls";
// import _Video from "react-native-video";

class Modal extends React.Component {
  render() {
    return (
      <>
        <_Modal style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "black",
            }}
          >
            {this.props.children}
          </View>
        </_Modal>
      </>
    );
  }
}

export class AndroidVideo extends React.Component {
  state = {
    duration: 0,
    paused: false,
    end: false,
    orientation: "LANDSCAPE",
  };

  componentDidMount() {
    Orientation.unlockAllOrientations();
    Orientation.addOrientationListener(this.onOritentationChanged.bind(this));
  }

  onOritentationChanged(orientation) {
    this.setState({ orientation });
  }

  onFullscreenPlayerWillPresent() {
    this.toggle();
  }
  onFullscreenPlayerWillDismiss() {
    const { onFullscreenPlayerWillDismiss } = this.props;
    if (onFullscreenPlayerWillDismiss) onFullscreenPlayerWillDismiss();
  }

  onEnd() {
    this.toggle();
  }
  toggle() {
    const { onEnd } = this.props;
    const end = !this.state.end;
    if (end === true) Orientation.lockToPortrait();
    this.setState({ end }, () => {
      end && onEnd && onEnd();
    });
  }

  onLoad(data) {}
  onProgress(time) {
    if (!time) return;

    const { onAutoComplete } = this.props;
    if (time.currentTime / time.seekableDuration > 0.9 && onAutoComplete) {
      this.onEnd();
    }
  }

  render() {
    const { orientation } = this.state;

    const screenWidth = Dimensions.get("window").width;
    const screenHeight = Dimensions.get("window").height;

    const videoStyle = {
      // width: orientation === "LANDSCAPE" ? screenHeight : screenWidth,
      height:
        orientation === "LANDSCAPE"
          ? screenWidth
          : screenHeight * (screenWidth / screenHeight),
    };

    return this.state.end === false ? (
      <Modal>
        <VideoPlayer
          {...this.props}
          toggleResizeModeOnFullscreen={true}
          onBack={this.onEnd.bind(this)}
          onEnterFullscreen={this.onFullscreenPlayerWillPresent.bind(this)}
          onExitFullscreen={this.onFullscreenPlayerWillDismiss.bind(this)}
          onEnd={this.onEnd.bind(this)}
          onPlay={this.onLoad.bind(this)}
          onProgress={this.onProgress.bind(this)}
          videoStyle={videoStyle}
        />
      </Modal>
    ) : null;
  }
}
