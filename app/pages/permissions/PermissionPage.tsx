import Backend from "backend/";
import {
  Buttoon,
  IconPrimr,
  ICON_NAME,
  Inpuut,
  sstyled,
  Toasty,
  Txt,
} from "components/";
import React, { useState } from "react";
import {
  Keyboard,
  LayoutAnimation,
  Platform,
  StyleSheet,
  View,
} from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";
import PushNotification from "react-native-push-notification";
import {
  PERMISSIONS,
  request,
  requestMultiple,
  requestNotifications,
  RESULTS,
} from "react-native-permissions";
import { C, IS_ANDROID, scale, spacing } from "utilities/";

interface dContent {
  title: string;
  text: string;
  icon: ICON_NAME;
  bg: string;
}

const _SCR_CONTENTS: dContent[] = [
  {
    title: "Welcome!",
    text:
      "\nWe need help setting up your account.\nLet's start with your name.",
    icon: null,
    bg: C.primary,
  },
  // {
  //   title: Platform.OS==="ios" ? "" : "Storage Permission",
  //   text: "We need your permission\nto access storage and handle media",
  //   icon: "folder",
  //   bg: C.hazardYellow,
  // },
  {
    title: "Contact Permission",
    text:
      "We need your permission to access contact to handle team communication",
    icon: "contacts",
    bg: C.awakenVolt,
  },
  {
    title: "Notification Permission",
    text: "We need your permission to show app notification",
    icon: "notification",
    bg: C.errorRed,
  },
];

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: spacing(5),
  },
  text: { color: C.text01, textAlign: "center" },
});

const SS = {
  CtnrSlide: sstyled(View)((p) => ({
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: spacing(5),
  })),
  Title: sstyled(Txt.H4)((p) => ({
    color: C.text01,
    textAlign: "center",
  })),
  Description: sstyled(Txt.S1)((p) => ({
    color: C.text01,
    textAlign: "center",
  })),
};

export function PermissionPage(props) {
  const { navigation } = props;
  const [_index, setIndex] = React.useState(0);
  const [_isContactAllowed, shouldAllowContact] = React.useState(false);
  const [_isStorageAllowed, shouldAllowStorage] = React.useState(false);
  const [_isNotificationAllowed, shouldAllowNotification] = React.useState(
    false
  );
  async function onAllowPress() {
    /**
     * Fundamental fn for handling permission status
     *
     * ---
     * @param givenStatus
     * @param keyword
     * ---
     * @version 0.11.22
     * @author nguyenkhooi
     */
    const isPermissionAllowed = (
      givenStatus,
      keyword: "contact" | "notification" | "storage" | string
    ) => {
      switch (givenStatus) {
        case RESULTS.UNAVAILABLE:
          console.warn(
            "This feature is not available (on this device / in this context)"
          );
          Toasty({
            text1: `Error getting ${keyword} permission`,
            text2: `Permission for ${keyword} is not available`,
            type: "error",
          });
          return false;
          break;
        case RESULTS.DENIED:
          console.warn(
            "The permission has not been requested / is denied but requestable"
          );
          Toasty({
            text1: `Permission for ${keyword} denied`,
            text2: "Please enable permission manually in phone settings",
            type: "error",
          });
          return false;
          break;
        case RESULTS.GRANTED:
          console.info(`Permission for ${keyword} is granted`);
          const firstLetter = keyword.substring(0, 1);
          Toasty({
            text1: `${keyword.replace(
              firstLetter,
              firstLetter.toUpperCase()
            )} permissions enabled.`,
          });
          return true;
          break;
        case RESULTS.BLOCKED:
          console.warn("The permission is denied and not requestable anymore");
          Toasty({
            text1: `Permission for ${keyword} is blocked`,
            text2: "Please try enabling permission manually in phone settings",
            type: "error",
          });
          return false;
          break;
        default:
          return false;
          break;
      }
    };
    //*----MAIN-SECTION --------------------------
    switch (_index) {
      default:
        return false;
        break;
      case 0:
        return false;
        break;

      //   break;
      case 1: //*----CONTACT-PERMISSION -------------------
        let keyword2 = "contact";
        try {
          if (IS_ANDROID) {
            const isReadContactAllowed = request(
              PERMISSIONS.ANDROID.READ_CONTACTS
            ).then((result) => {
              console.log("😀 Read Contact Permission result", result);
              return isPermissionAllowed(result, keyword2);
            });

            const isWriteContactAllowed = request(
              PERMISSIONS.ANDROID.WRITE_CONTACTS
            ).then((result) => {
              console.log("😀 Write Contact Permission result", result);
              return isPermissionAllowed(result, keyword2);
            });

            if (isReadContactAllowed && isWriteContactAllowed) {
              // Toasty({
              //   text1: `Permission for ${keyword2} is set`,
              //   text2: "Thank you",
              // });
              shouldAllowContact(true);
              return true;
            } else {
              // Toasty({
              //   type: "error",
              //   text1: `Permission for ${keyword2} denied.`,
              // });
              shouldAllowContact(false);
              return false;
            }
          } else {
            const permissions =
              Platform.OS === "ios"
                ? [PERMISSIONS.IOS.CONTACTS]
                : [
                    PERMISSIONS.ANDROID.READ_CONTACTS,
                    PERMISSIONS.ANDROID.WRITE_CONTACTS,
                  ];
            const isIOSContactAllowed = requestMultiple(permissions).then(
              (result) => {
                console.log("😀 IOS Contact Permission result", result);
                return isPermissionAllowed(result, keyword2);
              }
            );

            if (isIOSContactAllowed) {
              // Toasty({
              //   text1: `Permission for ${keyword2} is set`,
              //   text2: "Thank you",
              // });
              shouldAllowContact(true);
              return true;
            } else {
              // Toasty({
              //   type: "error",
              //   text1: `Permission for ${keyword2} denied.`,
              // });
              shouldAllowContact(false);
              return false;
            }

            // return (
            //   isIOSContactAllowed &&
            //   (Toasty({
            //     text1: `Permission for ${keyword2} is set`,
            //     text2: "Thank you",
            //   }),
            //   shouldAllowContact(true))
            // );
          }
        } catch (error) {
          Toasty({
            text1: `Error getting ${keyword2} permission`,
            text2: "Please enable permission manually in phone settings",
            type: "error",
          });
          return false;
        }
        break;
      case 2: //*----NOTIFICATION-PERMISSION -------------------
        let keyword3 = "notification";
        try {
          if (
            Backend.firestoreHandler._config.variables
              .enablePushNotifications === true
          ) {
            PushNotification.requestPermissions((r) => {
              console.log("pnresult: ", JSON.stringify(r));
            });
          } else {
            console.warn("sth is wrong");
          }

          // console.log("pnresult: " + JSON.stringify(result));
          // result = await PushNotification.checkPermissions();
          // const allowsPushNotifications =
          //   result.alert || result.badge || result.sound;
          // isPermissionAllowed(
          //   allowsPushNotifications ? RESULTS.GRANTED : RESULTS.DENIED,
          //   "notification"
          // );
          let allowsPushNotifications = true;
          if (allowsPushNotifications) {
            // Toasty({
            //   text1: `Permission for ${keyword3} is set`,
            //   text2: "Thank you",
            // });
            shouldAllowNotification(true);
            return true;
          } else {
            // Toasty({
            //   type: "error",
            //   text1: `Permission for ${keyword3} denied.`,
            // });
            shouldAllowNotification(false);
            return false;
          }
          // return requestNotifications(["alert", "sound", "badge"]).then(
          //   ({ status, settings }) => {
          //     return (
          //       isPermissionAllowed(status, keyword3) &&
          //       (Toasty({
          //         text1: `Permission for ${keyword3} is set`,
          //         text2: "Thank you",
          //       }),
          //       shouldAllowNotification(true))
          //     );
          //   }
          // );
        } catch (error) {
          Toasty({
            text1: `Error getting ${keyword3} permission`,
            text2: "Please enable permission manually in phone settings",
            type: "error",
          });
          return false;
        }
        break;
    }
  }
  const [name, setName] = useState("");
  const [nextButtonShown, showNextButton] = useState(false);
  const didSetName = React.useRef(false);

  Backend.firestoreHandler.account((_acc) => {
    if (_acc.name && _acc.name.length > 0 && !didSetName.current) {
      didSetName.current = true;
      setTimeout(() => {
        setName(_acc.name);
        showNextButton(true);
      }, 100);
    }
  });

  function saveName() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    Backend.firestoreHandler.updateAccount("", { name });
    showNextButton(true);
    Keyboard.dismiss();
  }

  const _renderItem = ({ item }: { item: dContent }) => {
    return (
      <SS.CtnrSlide style={{ backgroundColor: item.bg }}>
        {item.title ? <SS.Title>{item.title}</SS.Title> : null}
        {item.icon ? (
          <IconPrimr
            preset={"circular"}
            name={item.icon}
            size={scale(50)}
            // containerStyle={{ backgroundColor: "#ffffff80", borderRadius: 30 }}
            color={C.text01}
          />
        ) : null}
        {/* <Image source={item.image} style={styles.image} /> */}
        {item.text ? <SS.Description>{item.text}</SS.Description> : null}
        <View style={{ marginVertical: spacing(5) }}>
          {_index == 0 && (
            <Inpuut
              title="My name is"
              doneText="Done"
              autoCapitalize="words"
              // autoFocus={Platform.OS === "ios"}
              value={name}
              onChangeText={setName}
              onEndEditing={() => saveName()}
              onSavePress={() => saveName()}
              isUrl={false}
            />
          )}
          {_index == 1 && (
            <Buttoon
              onPress={onAllowPress}
              status="control"
              icon={_isContactAllowed ? { name: "check" } : null}
              appearance={_isContactAllowed ? "ghost" : "filled"}
            >
              {_isContactAllowed ? "Permission Set" : "Allow"}
            </Buttoon>
          )}
          {_index == 2 && (
            <Buttoon
              onPress={onAllowPress}
              status="control"
              icon={_isNotificationAllowed ? { name: "check" } : null}
              appearance={_isNotificationAllowed ? "ghost" : "filled"}
            >
              {_isNotificationAllowed ? "Permission Set" : "Allow"}
            </Buttoon>
          )}
        </View>
      </SS.CtnrSlide>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <AppIntroSlider
        scrollEnabled={false}
        keyboardShouldPersistTaps="always"
        keyExtractor={(item) => item.title}
        showNextButton={nextButtonShown}
        renderDoneButton={DoneButton}
        renderNextButton={NextButton}
        renderItem={_renderItem}
        data={_SCR_CONTENTS}
        onSlideChange={(index) => {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
          setIndex(index);
        }}
        onDone={() => navigation.navigate("Home")}
      />
    </View>
  );
}

function $_Action() {
  return (
    <Buttoon
      onPress={() => {}}
      appearance="ghost"
      icon={{ name: "chevron_right" }}
      status="basic"
      size="medium"
    >
      Nine-nine!
    </Buttoon>
  );
}

const DoneButton = () => {
  return (
    <IconPrimr
      preset={"circular"}
      name={"check"}
      size={scale(24)}
      containerStyle={{ backgroundColor: "#ffffff80" }}
      color={C.text01}
    />
  );
};

const NextButton = () => {
  return (
    <IconPrimr
      preset={"circular"}
      name={"arrow_right"}
      size={scale(24)}
      containerStyle={{ backgroundColor: "#ffffff80" }}
      color={C.text01}
    />
  );
};
