import { Input } from "@ui-kitten/components";
import Backend from "backend/";
import { Buttoon, Kitten, Toasty, Txt } from "components/";
import React from "react";
import { Linking, ScrollView, View } from "react-native";
import { Modalize } from "react-native-modalize";
import { C, DEVICE_HEIGHT, DEVICE_WIDTH, IPSCR, spacing } from "utilities/";
import { C_TeamItem } from "./c-team-item";
type dTeam = {
  name: string;
  position: number;
  password?: string | number;
  zoom?: string;
  mediaItem: {
    logo?: string;
    color?: string;
    imageColor?: string;
  };
};

interface P extends IPSCR {}
export const SS_TeamSelection = React.forwardRef((props: P, ref) => {
  const [_selectedTeam, setSelectedTeam] = React.useState({
    name: null,
    password: null,
  });

  const refScroll = React.useRef<ScrollView>();
  return (
    <Modalize
      ref={ref}
      scrollViewProps={{
        keyboardShouldPersistTaps: "always",
      }}
      snapPoint={DEVICE_HEIGHT * 0.83}
      modalHeight={DEVICE_HEIGHT * 0.83}
      modalStyle={{ backgroundColor: C.surface01 }}
      alwaysOpen={DEVICE_HEIGHT * 0.83}
      withHandle={false}
    >
      <$_TeamList
        {...props}
        onClose={() => {
          //* NOTE don't enable it in production to prevent user to close the wall
          !!ref.current && ref.current.close();
        }}
        onListPress={(tp: dTeam) => {
          setSelectedTeam({ name: tp.name, password: tp.password });
          !!refScroll.current &&
            refScroll.current.scrollTo({ x: DEVICE_WIDTH });
        }}
      />
    </Modalize>
  );
});

interface d$TeamList extends P {
  onListPress(tp: dTeam): void;
  onClose(): void;
}
const $_TeamList = (props: d$TeamList) => {
  const { onClose, onListPress } = props;

  const [_teamPages, setTeamPages] = React.useState<dTeam[]>([]);
  React.useEffect(function fetchTeamPages() {
    Backend.firebasePages.pages((pages) => {
      const teamPages = Backend.firebasePages.getTeamPages();
      setTeamPages(teamPages);
    });
  }, []);
  return (
    <View
      style={{
        width: DEVICE_WIDTH,
        justifyContent: "center",
        alignItems: "center",
        paddingTop: spacing(5),
      }}
    >
      <Txt.S1
        onPress={onClose}
        style={{ textAlign: "center", marginBottom: spacing(3) }}
      >
        Select your team to get started
      </Txt.S1>
      {_teamPages.map((tp) => (
        <C_TeamItem
          key={`${tp}`}
          {...props}
          page={tp}
          // onSuccess={() => ref.current.close()}
        />
      ))}
      <Buttoon
        status="success"
        appearance="ghost"
        icon={{ name: "flag" }}
        style={{ width: "92%", marginTop: spacing(4) }}
        onPress={() => {
          let teamAreaURL =
            Backend.firestoreHandler._config?.variables.teamAreaURL;
          teamAreaURL = teamAreaURL
            ? teamAreaURL
            : "https://apptakeoff.com/rvp";
          Linking.openURL(teamAreaURL);
        }}
      >
        Add My Team
      </Buttoon>
    </View>
  );
};

interface d$PwInput extends P {
  _selectedTeam: { name?: string; password?: string | number };
  onSuccess(): void;
  onCancel(): void;
}

const $_PasswordInput = (props: d$PwInput) => {
  const { _selectedTeam, onSuccess, onCancel } = props;
  const [entryPassword, setEntryPassword] = React.useState<string | number>("");
  const refInput = React.useRef<Input>();

  React.useEffect(
    function passwordChecking() {
      if (entryPassword == _selectedTeam.password) {
        Backend.firestoreHandler.updateAccount("team", {
          teamName: _selectedTeam.name,
        });
        Toasty({ text1: "Welcome to " + _selectedTeam.name + "!" });
        onSuccess();
      }
    },
    [entryPassword]
  );
  return (
    <View
      style={{
        width: DEVICE_WIDTH,
        justifyContent: "space-between",
        alignItems: "center",
        padding: spacing(5),
      }}
    >
      <Txt.Indicator onPress={onCancel} style={{ textAlign: "center" }}>
        Enter passcode for{"\n"}team {_selectedTeam.name}
      </Txt.Indicator>
      <Kitten.Input
        {...props}
        ref={refInput}
        status="primary"
        placeholder="Passcode"
        value={entryPassword}
        onChangeText={setEntryPassword}
      />
      <Buttoon onPress={onCancel} appearance="ghost">
        Cancel
      </Buttoon>
    </View>
  );
};
