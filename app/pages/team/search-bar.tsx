import K, { Images } from "constant-deprecated";
import React, { Component } from "react";
import {
    Alert,
    Dimensions,
    Image,
    Keyboard,
    TextInput,
    View
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { moderateScale } from "utilities/";
import { backgroundColor } from "./search-list";
import { deniedAccessMessage, hasAccessToPage } from "./TeamPage";

export default class SearchBar extends Component {
  state = {
    value: "",
  };

  onLayout(e) {
    const { onMeasure } = this.props;
    if (onMeasure) {
      this.inputRef.measure((x, y, width, height, pageX, pageY) => {
        onMeasure(x, y, width, height, pageX, pageY);
      });
    }
  }

  _inputRef(ref) {
    const { inputRef } = this.props;
    this.inputRef = ref;
    if (inputRef) inputRef(ref);
  }

  xpress() {
    this.onChangeText("");
    Keyboard.dismiss();
  }

  onChangeText(value) {
    const { onChangeText } = this.props;
    this.setState({ value });
    if (onChangeText) onChangeText(value);
  }
  endEditing() {
    this.onChangeText("");
  }

  onFocus() {
    const { onFocus } = this.props;
    if (onFocus) onFocus();
    if (!hasAccessToPage("search")) {
      Keyboard.dismiss();
      Alert.alert(deniedAccessMessage);
    }
  }

  render() {
    const { value } = this.state;

    return (
      <View
        style={{
          ...Styles.container,
          ...(value.length > 0 ? { backgroundColor } : {}),
        }}
      >
        <TextInput
          style={{
            ...Styles.inputStyle,
            ...(value.length > 0
              ? { backgroundColor: "rgba(25,25,25,0.94)" }
              : {}),
          }}
          ref={this._inputRef.bind(this)}
          autoCorrect={false}
          returnKeyType="done"
          selectionColor="white"
          placeholder="Search for videos, audios, etc."
          placeholderTextColor="rgb(200,200,200)"
          onLayout={this.onLayout.bind(this)}
          value={value}
          onEndEditing={this.endEditing.bind(this)}
          {...this.props}
          onChangeText={this.onChangeText.bind(this)}
          onFocus={this.onFocus.bind(this)}
        />
        {value.length > 0 ? <XButton onPress={this.xpress.bind(this)} /> : null}
      </View>
    );
  }
}

function XButton(props) {
  const { onPress } = props;
  return (
    <View style={Styles.xContainer}>
      <TouchableOpacity onPress={onPress}>
        <View>
          <Image source={Images.X_ICON} style={Styles.xImage} />
        </View>
      </TouchableOpacity>
    </View>
  );
}

const screenWidth = Dimensions.get("window").width;

const Styles = {
  container: {
    width: screenWidth,
    flexDirection: "row",
    justifyContent: "center",
  },
  inputStyle: {
    marginTop: 17,
    color: "white",
    backgroundColor: "rgba(80,80,80,0.5)",
    borderRadius: 8,
    fontSize: moderateScale(19),
    fontFamily: K.Fonts.medium,
    paddingHorizontal: 12,
    paddingVertical: 7,
    alignSelf: "center",
    width: "95%",
  },
  xContainer: {
    position: "absolute",
    right: moderateScale(22),
    top: 28,
    alignSelf: "center",
  },
  xImage: {
    tintColor: "rgb(240,240,240)",
    width: moderateScale(19),
    height: moderateScale(19),
  },
};
