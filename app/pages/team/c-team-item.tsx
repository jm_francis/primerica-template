import { Input } from "@ui-kitten/components";
import Backend, { MediaPageSchema } from "backend/";
import {
  Buttoon,
  dAccessory,
  IconPrimr,
  ICON_NAME,
  Kitten,
  sstyled,
  Toasty,
} from "components/";
import R from "ramda";
import React, { useState } from "react";
import { Image, LayoutAnimation, View } from "react-native";
import { C, spacing } from "utilities/";

/**
 * ### Team Item Component for Team Selection
 * @param props
 */
export function C_TeamItem(props: dTeamItem) {
  const { page } = props;
  const [passwordInputShown, showPasswordInput] = useState(false);

  const MenuIcon = (name: ICON_NAME, props: dAccessory) => (
    <IconPrimr
      name={name}
      size={props.style.width}
      color={C.pitchWhite}
    />
  );

  return (
    <>
      <Kitten.ListItem
        style={{
          backgroundColor: page.mediaItem.color
            ? page.mediaItem.color
            : C.background01,
          borderRadius: 10,
          margin: spacing(2),
        }}
        title={page.name}
        onPress={() => {
          if (!page.password || page.password.toString().length < 2) {
            Toasty({ text1: `Joining ${page.name}...`, visibilityTime: 10000 });
            Backend.firestoreHandler.updateAccount("team", {
              teamName: page.name,
            });
          } else {
            // LayoutAnimation.configureNext(
            //   LayoutAnimation.Presets.easeInEaseOut
            // );
            showPasswordInput(true);
          }
        }}
        accessoryLeft={(props: dAccessory) => {
          return !!page.mediaItem && !!page.mediaItem.logo ? (
            <Image
              source={{ uri: page.mediaItem.logo }}
              style={{
                width: props.style.width * 1.8,
                height: props.style.height * 1.8,
                borderRadius: 5,
                backgroundColor: page.mediaItem.imageColor,
              }}
            />
          ) : (
            MenuIcon("flag", props)
          );
        }}
        accessoryRight={() => (
          <ScoreEdit
            {...props}
            showPasswordInput={passwordInputShown}
            onCancel={() => showPasswordInput(false)}
          />
        )}
      />
    </>
  );
}

/**
 * ###  Button to edit member's score
 * -  Only if u're admin
 * TODO @K->@J: Can you add admin function
 * @author K
 * @version 1.1.29
 */
export const ScoreEdit = React.forwardRef((p: P, ref) => {
  const { onCancel } = p;
  const [_password, setPassword] = useState("");
  const [passwordInputShown, showPasswordInput] = useState(p.showPasswordInput);
  const [btnLabel, setBtnLabel] = useState<number | string>("Passcode");
  // const ref = React.useRef<TextInput>();

  const correctPassword = p.page?.password;

  React.useEffect(function setLabel() {
    if (!!!p.page?.password || R.isEmpty(p.page?.password)) {
      setBtnLabel("Join");
    }
  }, []);

  return (
    <SS.CtnrTeamPw>
      {passwordInputShown && (
        <Kitten.Input
          size="small"
          ref={ref}
          autoFocus={true}
          placeholder={"Team Passcode"}
          value={_password.toString()}
          onChangeText={setPassword}
          secureTextEntry
          selectTextOnFocus
          onBlur={() => {
            LayoutAnimation.configureNext(
              LayoutAnimation.Presets.easeInEaseOut
            );
            setBtnLabel("Passcode");
            showPasswordInput(false);
            setPassword("Passcode");
          }}
        />
      )}
      {passwordInputShown && (
        <Buttoon
          appearance="ghost"
          status="basic"
          size="small"
          // icon={{ name: "x" }}
          onPress={() => {
            LayoutAnimation.configureNext(
              LayoutAnimation.Presets.easeInEaseOut
            );
            setBtnLabel("Passcode");
            showPasswordInput(false);
            setPassword("");
            onCancel && onCancel();
          }}
        >
          Cancel
        </Buttoon>
      )}
      <Buttoon
        appearance="ghost"
        status="warning"
        size="small"
        onPress={() => {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
          setBtnLabel(passwordInputShown ? "Passcode" : "Submit");
          showPasswordInput(!passwordInputShown);
          console.log(
            "password compare: " + correctPassword + " == " + _password
          );
          if (correctPassword == _password) {
            Toasty({ text1: `Joining ${p.page.name}...` });
            Backend.firestoreHandler.updateAccount("team", {
              teamName: p.page.name,
            });
          } else if (btnLabel === "Submit") {
            Toasty({ type: "error", text1: "Incorrect team access code" });
          }
        }}
        icon={{ name: !!passwordInputShown ? "check" : "lock", right: true }}
      >
        {btnLabel}
      </Buttoon>
    </SS.CtnrTeamPw>
  );
});

const SS = {
  CtnrTeamPw: sstyled(View)(() => ({
    flexDirection: "row",
    flex: 1,
    // backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "flex-end",
  })),
};

export interface dTeamItem {
  page: MediaPageSchema;
}

interface P extends dTeamItem {
  showPasswordInput: boolean;
  onCancel?(): void;
}
