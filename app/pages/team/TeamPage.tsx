import Backend from "backend/";
import { MediaPageSchema } from "backend/schemas";
import { eTeamRole } from "backend/schemas/user/user.schema";
import { IconPrimr, Kitten } from "components/";
import CustomPage from "customPage/CustomPage";
import React, { Component } from "react";
import { Image, View } from "react-native";
import { Modalize } from "react-native-modalize";
import { moderateScale } from "react-native-size-matters";
import { C } from "utilities/";
import { SS_TeamSelection as SH_TeamSelection } from "./sh-team-selection";

class TeamPage extends Component {
  props: {
    navigation?: any;
  };

  state = {
    teamPage: null,
  };

  customPageRef = React.createRef();
  scrollRef = null;

  ref$$TeamSelection = React.createRef<Modalize>();

  account = null;

  async componentDidMount() {
    Backend.firestoreHandler.account((account) => {
      if (!this.account) {
        Backend.firebasePages.pages((pages) => {
          this.update();
        });
      }
      this.account = account;
      this.update();
    });
  }
  update() {
    if (!Backend.firebasePages._pages || !Backend.firestoreHandler._account)
      return;

    const teamPage = Backend.firebasePages.getMyTeamPage(); // requires ._pages and ._account
    if (!teamPage) {
      if (this.ref$$TeamSelection.current)
        this.ref$$TeamSelection.current.open();
    } else {
      if (this.ref$$TeamSelection.current)
        this.ref$$TeamSelection.current.close();
    }

    this.setState({ teamPage });
  }

  // customPageRef = null;

  render() {
    const { teamPage } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <$_Header {...this.props} customPageRef={this.customPageRef} />
        <View
          ref={(ref) => (this.scrollRef = ref)}
          style={{ flex: 1, backgroundColor: C.background01 }}
          // stickyHeaderIndices={[0]}
        >
          {teamPage ? (
            <CustomPage
              ref={this.customPageRef}
              pageName={teamPage.name}
              containerStyle={{
                backgroundColor: "transparent",
              }}
              navigation={this.props.navigation}
              tintColor={
                teamPage.mediaItem.team === true
                  ? teamPage.mediaItem.color
                  : undefined
              }
            />
          ) : null}
        </View>
        <SH_TeamSelection ref={this.ref$$TeamSelection} {...this.props} />
      </View>
    );
  }
}

function $_Header(props) {
  const { navigation } = props;
  // const MenuIcon = (name: enum_IconName, props: dAccessory) => (
  //   <IconPrimr name={name} size={props.style.width} color={C.text01} />
  // );

  const [_account, setAccount] = React.useState({});

  React.useEffect(function fetchAccount() {
    Backend.firestoreHandler.account((account) => setAccount(account));
  }, []);

  const teamName = _account?.team?.teamName;
  const teamPage: MediaPageSchema = Backend.firebasePages.getMyTeamPage();
  const teamStyling = teamPage
    ? {
        // backgroundColor: teamPage?.mediaItem.color,
        backgroundColor: C.background01,
      }
    : {};

  // const isTeamOwner = Backend.firestoreHandler._account?.team?.role === eTeamRole.Owner;

  return (
    <>
      <Kitten.TopNavigation
        style={{
          // paddingTop: getStatusBarHeight("safe", "ios-only") + 15,
          backgroundColor: C.background01,
          ...teamStyling,
        }}
        title={teamName ? teamName : "Choose a Team"}
        alignment="center"
        accessoryLeft={() =>
          teamPage?.mediaItem.logo ? (
            <Image
              source={{ uri: teamPage.mediaItem.logo }}
              style={{
                width: 30,
                height: 30,
                marginHorizontal: 15,
                marginTop: -4,
              }}
            />
          ) : null
        }
        accessoryRight={(_props) => <AccessoryRight {...props} />}
      />
    </>
  );
}

function AccessoryRight(props) {
  const { navigation, customPageRef } = props;
  const hasJoinedTeam = Backend.firestoreHandler._account?.team ? true : false;
  const isTeamOwner =
    Backend.firestoreHandler._account?.team?.role === eTeamRole.Owner;
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "center",
      }}
    >
      <IconPrimr
        name="content_share"
        size={moderateScale(isTeamOwner ? 22 : 25)}
        color={hasJoinedTeam ? "white" : "transparent"}
        onPress={() => customPageRef.current.sharePress()}
      />
      {isTeamOwner ? (
        <IconPrimr
          name="admin"
          size={moderateScale(20)}
          color="white"
          onPress={() => navigation.navigate("TeamAdmin")}
        />
      ) : null}
    </View>
  );
}

export default TeamPage;
