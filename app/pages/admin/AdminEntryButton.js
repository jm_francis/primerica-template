import K from "constant-deprecated";
import React, { Component } from "react";
import {
  Dimensions,

  Platform,
  Text, TouchableOpacity,

  View
} from "react-native";
import { moderateScale, verticalScale } from "utilities/";

export default class AdminEntryButton extends Component {
  render() {
    const { onPress } = this.props;
    return (
      <TouchableOpacity onPress={onPress}>
        <View style={Styles.container}>
          <Text style={Styles.title}>Admin Controls</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const screenWidth = Dimensions.get("window").width;

const Styles = {
  container: {
    marginTop: 6,
    backgroundColor: "#ba1c93",
    width: Platform.isPad ? screenWidth * 0.65 : screenWidth * 0.95,
    height: verticalScale(70),
    borderRadius: 13,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    color: "white",
    fontWeight: "600",
    fontFamily: K.Fonts.medium,
    fontSize: moderateScale(25, 0.37)
  }
};
