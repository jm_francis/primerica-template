import Backend from "backend/";
import { Buttoon, Inpuut, Kitten, Toasty, Txt } from "components/";
import { defaultLayoutAnimation } from "pages/listBuilder/configure";
import React, { Component } from "react";
import { LayoutAnimation, TextStyle, View, ViewStyle } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { C, moderateScale, spacing } from "utilities/";

export default class AppSettings extends Component {
  static navigationOptions = {
    title: "App Settings",
  };

  state = {
    disableScoreboard: false,
    disableCalendar: false,
    disableContactManager: false,
  };

  componentDidMount() {
    Backend.firestoreHandler.config(this.config.bind(this));
  }

  config(config) {
    const {
      disableScoreboard,
      disableCalendar,
      disableContactManager,
    } = config.variables;
    this.setState({
      disableScoreboard,
      disableCalendar,
      disableContactManager,
    });
  }

  disableScoreboardSwitch(enabled) {
    Backend.firestoreHandler.updateConfigVariables({
      disableScoreboard: !enabled,
    });
    this.setState({ disableScoreboard: !enabled });
  }
  disableCalendarSwitch(enabled) {
    Backend.firestoreHandler.updateConfigVariables({
      disableCalendar: !enabled,
    });
    this.setState({ disableCalendar: !enabled });
  }
  disableContactManagerSwitch(enabled) {
    Backend.firestoreHandler.updateConfigVariables({
      disableContactManager: !enabled,
    });
    this.setState({ disableContactManager: !enabled });
  }

  render() {
    const {
      disableScoreboard,
      disableCalendar,
      disableContactManager,
    } = this.state;
    return (
      <>
        <Kitten.TopNavigation
          title="Settings"
          style={{
            backgroundColor: C.background01,
          }}
          alignment="center"
          accessoryLeft={() => (
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              onPress={() => {
                this.props.navigation.pop();
              }}
              icon={{
                name: "arrow_left",
                color: "white",
                size: moderateScale(20),
              }}
            />
          )}
        />
        <KeyboardAwareScrollView
          style={Styles.container}
          extraScrollHeight={80}
        >
          <Txt.Indicator
            style={{
              textAlign: "center",
              paddingVertical: spacing(2),
              marginBottom: spacing(2),
            }}
          >
            Your message will be broadcasted{"\n"}to every user in the app.
          </Txt.Indicator>
          <SwitchItem
            title="Competition Scoreboard"
            value={!disableScoreboard}
            onChange={this.disableScoreboardSwitch.bind(this)}
          />
          <SwitchItem
            title="Calendar"
            value={!disableCalendar}
            onChange={this.disableCalendarSwitch.bind(this)}
          />
          <SwitchItem
            title="Contact Manager"
            value={!disableContactManager}
            onChange={this.disableContactManagerSwitch.bind(this)}
          />
          <$_VideoCalls />
        </KeyboardAwareScrollView>
      </>
    );
  }
}

function SwitchItem(props: {
  title: string;
  value: any;
  onChange: () => void;
}) {
  const { title, value, onChange } = props;
  return (
    <Kitten.ListItem
      title={title}
      accessoryRight={() => (
        <Kitten.Toggle checked={value} onChange={onChange} />
      )}
    />
  );
}

function $_VideoCalls(props) {
  const [_videoCalls, setVideoCalls] = React.useState(null);

  React.useEffect(function fetchVideoCalls() {
    Backend.firestoreHandler.config((config) => {
      const calls = config.variables.videoCalls;
      setVideoCalls(calls ? calls : []);
    });
  }, []);

  return (
    <View style={{ marginVertical: spacing(3), paddingHorizontal: spacing(2) }}>
      <Txt.$Title>Video Calls</Txt.$Title>
      <View style={{ height: spacing(2) }} />
      {_videoCalls &&
        _videoCalls.map((call, index) => (
          <VideoCallItem
            key={`call.${index}`}
            title={call.title}
            meetingId={call.meetingId}
            index={index}
          />
        ))}
      <Buttoon
        style={{ marginTop: spacing(1) }}
        icon={{ name: "plus" }}
        status="success"
        onPress={() => {
          if (!Backend.firestoreHandler._config) {
            Toasty({
              text1: "App config loading. Please wait.",
              type: "error",
            });
            return;
          }
          let videoCalls =
            Backend.firestoreHandler._config.variables.videoCalls;
          videoCalls = videoCalls ? videoCalls : [];
          for (let c in videoCalls) {
            if (videoCalls[c].title?.length < 1) {
              Toasty({
                text1: "You have an unfinished item.",
                text2:
                  "Make sure your last added call has a title before adding a new one.",
                type: "error",
              });
              return;
            }
          }
          LayoutAnimation.configureNext(defaultLayoutAnimation);
          Backend.firestoreHandler.updateConfigVariables({
            videoCalls: _videoCalls.concat([{ title: "", meetingId: "" }]),
          });
        }}
      >
        Add call
      </Buttoon>
    </View>
  );
}
function VideoCallItem(props) {
  const { title, meetingId, index } = props;

  const [_title, setTitle] = React.useState(title);
  const [_meetingId, setMeetingId] = React.useState(meetingId);

  function onEndEditing() {
    const videoCalls = [].concat(
      Backend.firestoreHandler._config.variables.videoCalls
    );
    for (let x = 0; x < videoCalls.length; x++) {
      const _call = videoCalls[x];
      if (x === index) continue;
      if (_call.title === _title) {
        setTitle(videoCalls[index].title);
        Toasty({
          text1: "A call with this title already exists.",
          text2: "Please use a different title.",
          type: "error",
        });
        return;
      }
    }
    videoCalls[index] = { title: _title, meetingId: _meetingId };
    Backend.firestoreHandler.updateConfigVariables({ videoCalls });
  }

  return (
    <Kitten.Card>
      <Inpuut
        placeholder="Title"
        value={_title}
        onChangeText={(value) => setTitle(value)}
        onSavePress={onEndEditing}
        returnKeyType="done"
        autoCapitalize="words"
      />
      <Inpuut
        placeholder="Meeting ID or URL"
        value={_meetingId}
        onChangeText={(value) => setMeetingId(value)}
        onSavePress={onEndEditing}
        returnKeyType="done"
        keyboardType="url"
        autoCorrect={false}
        autoCapitalize="none"
      />
      <Buttoon
        status="danger"
        icon={{ name: "trash" }}
        onPress={() => {
          LayoutAnimation.configureNext(defaultLayoutAnimation);
          const videoCalls =
            Backend.firestoreHandler._config.variables.videoCalls;
          delete videoCalls[index];
          Backend.firestoreHandler.updateConfigVariables({ videoCalls });
        }}
      >
        Remove Call
      </Buttoon>
    </Kitten.Card>
  );
}

const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background01,
  } as ViewStyle,
  item: {
    marginVertical: 10,
    width: "94%",
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  } as ViewStyle,
  itemTitle: {
    fontWeight: "bold",
  },
} as TextStyle;
