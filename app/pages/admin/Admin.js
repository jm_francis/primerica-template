//@ts-check
import Backend from "backend/";
import { Buttoon, IconPrimr, Kitten } from "components/";
import K from "constant-deprecated/";
import React, { Component } from "react";
import { Alert, Linking, Platform, View } from "react-native";
import Share from "react-native-share";
import { moderateScale } from "utilities";
import { C } from "utilities";

/**
 * ### Section/page to send personalized messages to existing list
 * -  Similar to HitEmUp app
 * -
 * ---
 * @version 1.2.23 (aka feb 18th, 2021)
 * -  *Refactor*
 * -  *Document the file*
 * @author  JR - Nk - NL
 * 
 *
 *
 */
export default class Admin extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Admin",
  });

  allUsers() {
    console.log("press");
    this.props.navigation.navigate("Users");
  }

  notificationBroadcast() {
    if (
      Backend.firestoreHandler._config.variables.enablePushNotifications ===
      true
    ) {
      this.props.navigation.navigate("NotificationBroadcast");
    } else {
      Alert.alert(
        "Push notification features require additional setup.\n\nWould you like to upgrade now?",
        "",
        [
          {
            text: "No thanks",
            onPress: () => {},
            // style: "cancel",
          },
          {
            text: "Yes!",
            onPress: () => {
              const { appTitle } = Backend.firestoreHandler._config.variables;
              const { name } = Backend.firestoreHandler._account;
              const body = `Hello, this is ${name} with ${appTitle}.\n\nI would like to upgrade my app to have push notifications!`;
              if (Platform.OS === "ios") {
                Linking.openURL(
                  K.Variables.appleBusinessChat + "&body=" + body
                );
              } else {
                Share.shareSingle({
                  subject: `Push Notification upgrade for ${appTitle}`,
                  message: body,
                  social: Share.Social.EMAIL,
                  email: "team@apptakeoff.com",
                });
              }
            },
          },
        ]
      );
    }
  }

  mailcampaign(){
    this.props.navigation.navigate("MailComapaign");
  }

  email() {
    this.props.navigation.navigate("Email");
  }

  appSettings() {
    this.props.navigation.navigate("AppSettings");
  }

  developer() {
    this.props.navigation.navigate("Developer");
  }

  render() {
    const developer =
      Backend.firestoreHandler._account?.email === "demo@demo.com" &&
      !Backend.firestoreHandler._config?.variables.reviewMode;
    return (
      <View style={Styles.container}>
        <Kitten.TopNavigation
          title="Admin Controls"
          style={{
            backgroundColor: C.background01,
          }}
          alignment="center"
          accessoryLeft={() => (
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              onPress={() => {
                this.props.navigation.pop();
              }}
              icon={{
                name: "arrow_left",
                color: "white",
                size: moderateScale(20),
              }}
            />
          )}
        />
        <Kitten.Menu>
          <Kitten.MenuItem
            title="Broadcast a Message"
            onPress={this.notificationBroadcast.bind(this)}
            accessoryLeft={(props) => (
              <IconPrimr
                name="send"
                size={props.style.width * 0.85}
                color={props.style.tintColor}
              />
            )}
            accessoryRight={(props) => (
              <IconPrimr
                name={"chevron_right"}
                size={props.style.width}
                color={props.style.tintColor}
              />
            )}
          />
          <Kitten.MenuItem
            title="Send Campaign Emails"
            onPress={this.mailcampaign.bind(this)}
            accessoryLeft={(props) => (
              <IconPrimr
                name="mail"
                size={props.style.width * 0.85}
                color={props.style.tintColor}
              />
            )}
            accessoryRight={(props) => (
              <IconPrimr
                name={"chevron_right"}
                size={props.style.width}
                color={props.style.tintColor}
              />
            )}
          />
          <Kitten.MenuItem
            title="All Users"
            onPress={this.allUsers.bind(this)}
            accessoryLeft={(props) => (
              <IconPrimr
                name={"people"}
                size={props.style.width * 0.85}
                color={props.style.tintColor}
              />
            )}
            accessoryRight={(props) => (
              <IconPrimr
                name={"chevron_right"}
                size={props.style.width}
                color={props.style.tintColor}
              />
            )}
          />
          {/* <Kitten.MenuItem
            title="Email"
            onPress={this.email.bind(this)}
            accessoryLeft={(props) => (
              <IconPrimr
                name="send"
                size={props.style.width * 0.85}
                color={props.style.tintColor}
              />
            )}
            accessoryRight={(props) => (
              <IconPrimr
                name={"chevron_right"}
                size={props.style.width}
                color={props.style.tintColor}
              />
            )}
          /> */}
          <Kitten.MenuItem
            title="App Settings"
            onPress={this.appSettings.bind(this)}
            accessoryLeft={(props) => (
              <IconPrimr
                name={"tools"}
                size={props.style.width * 0.85}
                color={props.style.tintColor}
              />
            )}
            accessoryRight={(props) => (
              <IconPrimr
                name={"chevron_right"}
                size={props.style.width}
                color={props.style.tintColor}
              />
            )}
          />
          {developer ? (
            <Kitten.MenuItem
              title="Developer"
              onPress={this.developer.bind(this)}
              accessoryLeft={(props) => (
                <IconPrimr
                  name={"eye"}
                  size={props.style.width * 0.85}
                  color={props.style.tintColor}
                />
              )}
              accessoryRight={(props) => (
                <IconPrimr
                  name={"chevron_right"}
                  size={props.style.width}
                  color={props.style.tintColor}
                />
              )}
            />
          ) : null}
        </Kitten.Menu>
      </View>
    );
  }
}

const Styles = {
  container: {
    backgroundColor: "rgb(247,247,247)",
    flex: 1,
  },
  button: {
    marginVertical: 8,
  },
};
