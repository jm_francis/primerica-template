import { Toggle, TopNavigation } from "@ui-kitten/components";
import SomeoneElse from "app/pages/listBuilder/screens/SomeoneElse";
import Backend from "backend/";
import { Toasty } from "components";
import { Buttoon } from "components";
import K from "constant-deprecated";
import BackButton from "navigation/components/BackButton";
import React, { Component } from "react";
import {
  Alert,
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { C, spacing } from "utilities/";
import { moderateScale } from "utilities/";
import UserLevels from "./UserLevels";

export default class User extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam("user", {}).name,
    headerLeft: (
      <BackButton navigation={navigation} text="Back" tintColor="white" />
    ),
  });

  state = {
    user: null,
  };

  componentDidMount() {
    this.uid = this.props.navigation.getParam("uid");
    Backend.adminHandler.users(this.users.bind(this));
  }
  users(users) {
    for (u in users) {
      if (users[u].uid === this.uid) {
        this.setState({ user: users[u] });
        break;
      }
    }
  }

  // ANCHOR: Baning
  unbanUser() {
    const { user } = this.state;
    Backend.adminHandler.unbanUser(user);
  }
  confirmBan() {
    const { user } = this.state;
    Backend.adminHandler.banUser(user);
  }
  banUser() {
    Alert.alert("Are you sure you want to ban this user?", "", [
      { text: "Cancel", style: "cancel" },
      { text: "Yes!", onPress: this.confirmBan.bind(this) },
    ]);
  }

  // ANCHOR: Levels bypass
  unlockLevels() {
    const { user } = this.state;
    Backend.adminHandler.unlockUserLevels(user);
  }
  lockLevels() {
    const { user } = this.state;
    Backend.adminHandler.lockUserLevels(user);
  }

  // ANCHOR: View contact lists
  viewContactLists() {
    const { user } = this.state;
    Backend.listBuilderHandler.connectToOutsiderAccount(user.uid);

    this.props.navigation.navigate("ListBuilderModal", {
      content: <SomeoneElse navigation={this.props.navigation} />,
      title: user.name,
    });
  }
  isContactAccessAllowed() {
    try {
      const { user } = this.state;
      const _uid = Backend.firestoreHandler.uid;
      const isAllowed = !user.listBuilder?.shareTo
        ? false
        : user.listBuilder.shareTo?.includes(_uid);
      return isAllowed;
    } catch (error) {
      console.warn("err >>>", error);
    }
  }

  imitate() {
    const { user } = this.state;
    Backend.firestoreHandler.imitateUser(user);
    Toasty({ text1: `Now imitating ${user.name ? user.name : user.email}` });
  }

  render() {
    const { user } = this.state;

    const developer =
      Backend.firestoreHandler._account?.email === "demo@demo.com" &&
      !Backend.firestoreHandler._config?.variables.reviewMode;

    return (
      <>
        <TopNavigation
          title=""
          style={{ backgroundColor: C.background01 }}
          accessoryLeft={() => (
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              onPress={() => {
                this.props.navigation.pop();
              }}
              icon={{
                name: "arrow_left",
                color: "white",
                size: moderateScale(20),
              }}
            >
              Back
            </Buttoon>
          )}
        />
        <ScrollView style={Styles.container}>
          {user ? <$_UserInfo {...this.props} user={user} /> : null}

          <View style={{ width: "100%", height: 30 }} />

          {user ? (
            <>
              <UserLevels user={user} />

              {user.allLevelsCompleted ? (
                <Buttoon
                  icon={{ name: "lock" }}
                  onPress={this.lockLevels.bind(this)}
                  style={ButtonStyles.container}
                  status="danger"
                >
                  Lock user's levels
                </Buttoon>
              ) : (
                <Buttoon
                  icon={{ name: "unlock" }}
                  onPress={this.unlockLevels.bind(this)}
                  style={ButtonStyles.container}
                  appearance="primary"
                >
                  Unlock user's levels
                </Buttoon>
              )}

              <Buttoon
                appearance="primary"
                icon={{ name: "contacts" }}
                disabled={!this.isContactAccessAllowed()}
                onPress={this.viewContactLists.bind(this)}
                style={ButtonStyles.container}
              >
                View Contact Lists
              </Buttoon>

              {developer ? (
                <Buttoon
                  appearance="primary"
                  icon={{ name: "eye" }}
                  onPress={this.imitate.bind(this)}
                  style={ButtonStyles.container}
                >
                  Imitate User
                </Buttoon>
              ) : null}
            </>
          ) : null}

          {user?.banned ? (
            <Buttoon
              onPress={this.unbanUser.bind(this)}
              style={ButtonStyles.container}
              appearance="ghost"
              status="danger"
            >
              Unban user
            </Buttoon>
          ) : (
            <Buttoon
              onPress={this.banUser.bind(this)}
              style={ButtonStyles.container}
              appearance="ghost"
              status="danger"
              icon={{ name: "ban" }}
            >
              Ban user from app
            </Buttoon>
          )}
        </ScrollView>
      </>
    );
  }
}

/**
 * A member info segment showing name, email, phone (?),
 *  and an admin toggle (visible only on admin's app)
 * NOTE need to work with J on this
 * @param {*} props
 */
function $_UserInfo(props) {
  const { user } = props;

  const [_adminSwitch, setAdminSwitch] = React.useState(user.admin === true);

  /** Check if the current user admin */
  const _isAdmin = Backend.firestoreHandler._account.admin;

  const isMemberAdmin = user.admin === true;

  function setMemberAdmin(toggle) {
    setAdminSwitch(toggle);
    Backend.adminHandler.updateUserData(user, {
      admin: toggle,
    });
  }

  const tabletScale = Platform.isPad
    ? {
        transform: [{ scaleX: moderateScale(1) }, { scaleY: moderateScale(1) }],
      }
    : {};

  return (
    <View style={{ flexDirection: "row", justifyContent: "flex-start" }}>
      <View style={{ flex: 2, justifyContent: "center" }}>
        {user.name ? <Text style={Styles.name}>{user.name}</Text> : null}
        <Text style={Styles.property}>{user.email}</Text>
        {user.phoneNumber ? (
          <Text style={Styles.property}>{user.phoneNumber}</Text>
        ) : null}
        <Text
          style={Styles.property}
        >{`Joined App: ${user.createdAt?.toDate().toLocaleDateString()}`}</Text>
      </View>
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Toggle
          checked={_adminSwitch}
          onChange={(check) => {
            setMemberAdmin(check);
          }}
          style={{ width: "100%", flexDirection: "column", ...tabletScale }}
        />
        <Text style={Styles.toggleText}>
          {_adminSwitch ? "Admin" : "Set as Admin"}
        </Text>
      </View>
    </View>
  );
}

function Button(props) {
  const { onPress, red, style = {}, icon } = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={{
          ...ButtonStyles.container,
          ...(red ? ButtonStyles.redContainer : {}),
          ...style,
        }}
      >
        {icon ? <Image source={icon} style={ButtonStyles.icon} /> : null}
        <Text style={ButtonStyles.title}>{props.children}</Text>
      </View>
    </TouchableOpacity>
  );
}

textStyle = {
  fontFamily: K.Fonts.medium,
  fontSize: moderateScale(18, 0.38),
  color: "white",
  // color: "rgb(20,20,20)",
};

const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background01,
    paddingHorizontal: 12,
    paddingVertical: 20,
  },
  userInfoContainer: { flexDirection: "row" },
  name: {
    ...textStyle,
    fontFamily: K.Fonts.bold,
    fontSize: moderateScale(21, 0.38),
    marginBottom: spacing(1),
  },
  property: {
    ...textStyle,
    marginBottom: 4,
  },
  toggleText: {
    marginTop: 21,
    fontFamily: K.Fonts.bold,
    fontSize: moderateScale(17, 0.38),
    color: "white",
  },
};

const ButtonStyles = {
  container: {
    marginVertical: 5,
  },
  title: {
    flex: 1,
    textAlign: "center",
    color: "white",
    fontSize: moderateScale(20, 0.38),
    fontFamily: K.Fonts.medium,
  },
  icon: {
    position: "absolute",
    left: 23,
    width: 23,
    height: 23,
    tintColor: "white",
  },
};
