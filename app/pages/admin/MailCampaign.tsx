import Backend from "backend/";
import { Buttoon, Kitten, sstyled, Txt } from "components/";
import React from "react";
import {
  RefreshControl, View
} from "react-native";
import HTMLView from "react-native-htmlview";
import UserAvatar from "react-native-user-avatar";
import { NavigationStackScreenProps } from "react-navigation-stack";
import { C, moderateScale, spacing } from "utilities/";

/**
 * ### This page contains previously sent mails
 * -  
 * -
 * ---
 * @version 1.2.22 (aka feb 22th, 2021)
 * -  *Refactor*
 * -  *Document the file*
 * @author  NL
 *
 *
 */
/**
     * Refresh functionality timeout
     */
 const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}


export function MailComapaign(props: P) {
  const [mails, setmails] = React.useState<any>([]);
  const [refreshing, setRefreshing] = React.useState(false);
  const [loader, setloader] = React.useState<any>(true);
  function navigateCreateMail() {
    props.navigation.navigate("CreateMail", {
      onpageEmails : () => {getEmailSent()}
    });
  }

/**
     * Refresh functionality
     */
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(()=> {
      getEmailSent();
      setRefreshing(false);
    })
  }, []);


  /**
     * Each Item rendered
     */
  const renderItem = ({ item, index }) => (
    // return(
    <Kitten.ListItem
      title={item.subject}
      description={() => (
        <View
          style={{
            flexDirection: "row",
            paddingRight: 24,
            paddingTop: 2,
            paddingLeft: 12,
          }}
        >
          <HTMLView value={item.body} stylesheet={SS.htmlStyle} />
        </View>
      )}
      accessoryLeft={() => <UserAvatar size={40} name={item.recipient ? item.recipient.charAt(0).toUpperCase() : item.recipients[0].charAt(0).toUpperCase() } />}
    ></Kitten.ListItem>
    // )
  );

  React.useEffect(function () {
    getEmailSent();
  }, [props.navigation]);

  /**
   * Get sent mails
   */
  function getEmailSent() {
    
    const user = Backend.accountHandler._user;
    
    Backend.adminHandler
      .extractSentMails(user.uid)
      .then((response) => {
        setloader(false);
        let _emails = [];
        response.docs.forEach((item) => {
          _emails.push(item.data());
        });
        setmails(_emails);

      })
      .catch(() => {
        console.log("failed to get send mails");
      });
  }

  return (
    <SS.Ctnr>
      <Kitten.TopNavigation
        title="Send Campaign Email"
        alignment="center"
        style={{ backgroundColor: C.background01 }}
        accessoryLeft={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              props.navigation.pop();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          />
        )}
        accessoryRight={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              navigateCreateMail();
            }}
            icon={{
              name: "plus",
              color: C.primary,
              size: moderateScale(20),
            }}
          />
        )}
      />
      <Txt.$Title>Previously Sent</Txt.$Title>
      {
        loader == true ? 
        <View style={SS.S.loading}>
          <Kitten.Spinner status={"control"} size={"giant"} />
        </View> : <Kitten.List
        data={mails}
        renderItem={renderItem}
        ItemSeparatorComponent={Kitten.Divider}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            tintColor={"#00ffff"}
            progressBackgroundColor={"#00ffff"}
          />
        }
      />
      }
    </SS.Ctnr>
  );
}

const SS = {
  Ctnr: sstyled(View)((p) => ({
    flex: 1,
    backgroundColor: p.C.background01,
  })),
  CtnrContent: sstyled(View)((p) => ({
    backgroundColor: p.C.background,
    paddingVertical: spacing(2),
    paddingHorizontal: spacing(4),
    justifyContent: "flex-start",
  })),
  S: {
    item: {
      marginVertical: 4,
    },
    container: {
      maxHeight: 320,
    },
    loading: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
  },
  htmlStyle: {
    a: {
      fontWeight: "bold",
      color: "purple",
    },
    div: {
      fontFamily: "arial",
      color: "#fff",
    },
    p: {
      fontSize: 10,
      color: "#fff",
    },
  },
};

interface P extends NavigationStackScreenProps {}
