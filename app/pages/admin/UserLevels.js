import Backend from "backend/";
import K from "constant-deprecated";
import React, { Component } from "react";
import { Dimensions, Text, View } from "react-native";
import { moderateScale } from "utilities/";

export default class UserLevels extends Component {
  render() {
    const { user } = this.props;

    let fullLevelProgress = Backend.levelsHandler.calculateTotalLevelProgressForUser(
      user
    );
    let progressText = fullLevelProgress * 100;
    progressText = Number.parseInt(progressText) + "%";

    return (
      <View style={Styles.container}>
        <View
          style={{
            ...Styles.progressContainer,
            width: screenWidth * 0.97 * fullLevelProgress,
          }}
        />
        <Text style={Styles.levelNameText}>Levels Progress</Text>
        <Text style={Styles.levelProgressText}>{progressText}</Text>
      </View>
    );
  }
}

const screenWidth = Dimensions.get("window").width;

const Styles = {
  container: {
    width: "100%",
    height: moderateScale(40, 0.38),
    overflow: "hidden",
    backgroundColor: "rgb(200,200,200)",
    borderRadius: 7,
    marginBottom: 12,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 23,
  },
  progressContainer: {
    backgroundColor: "#49de76",
    height: "100%",
    position: "absolute",
    left: 0,
  },
  levelNameText: {
    fontSize: moderateScale(20.7, 0.38),
    color: "white",
    fontFamily: K.Fonts.bold,
  },
  levelProgressText: {
    fontSize: moderateScale(22.5, 0.38),
    color: "white",
    fontFamily: K.Fonts.bold,
  },
};
