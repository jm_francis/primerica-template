//@ts-check
import Backend from "backend/";
import { Buttoon, Kitten, UserListItem } from "components/";
import K from "constant-deprecated/";
import R from "ramda";
import React, { Component } from "react";
import { ScrollView, TextInput } from "react-native";
import { C, moderateScale } from "utilities/";

export default class Users extends Component {
  state = {
    users: [],
    searchText: "",
  };

  componentDidMount() {
    Backend.adminHandler.users(this.users.bind(this));
  }
  users(users) {
    this.setState({ users });
  }

  userPress(user) {
    this.props.navigation.navigate("User", { uid: user.uid });
  }

  searchTextChange(text) {
    this.setState({ searchText: text });
  }

  render() {
    const { searchText } = this.state;
    const _users = this.state.users;
    let users = [];
    for (let u in _users) {
      const user = _users[u];
      if (searchText.length < 1) users.push(user);
      else if (
        user.name?.toLowerCase().includes(searchText.toLowerCase()) ||
        user.email.toLowerCase().includes(searchText.toLowerCase())
      )
        users.push(user);
    }

    const components = [];
    for (let u in users) {
      const user = users[u];
      components.push(
        <UserListItem
          user={user}
          showLevelProgress={true}
          onPress={() => this.userPress(user)}
          key={`user.${user.phoneNumber}.${user.name}.${u}`}
        />
      );
    }

    return (
      <>
        <Kitten.TopNavigation
          title="All Users"
          style={{
            backgroundColor: C.background01,
          }}
          alignment="center"
          accessoryLeft={() => (
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              onPress={() => {
                this.props.navigation.pop();
              }}
              icon={{
                name: "arrow_left",
                color: "white",
                size: moderateScale(20),
              }}
            />
          )}
          accessoryRight={() => (
            <Buttoon
              status="basic"
              appearance="ghost"
              // icon={{ name: "filter", right: true }}
              size="small"
              onPress={() => {
                //TODO @K->@JF Build the filter fn here
                let sortedUsers = R.sortWith(
                  [R.ascend(R.prop("name"))],
                  [...this.state.users]
                );
                // LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
                this.setState({ users: sortedUsers });
              }}
            >{`${_users.length} users`}</Buttoon>
          )}
        />
        <ScrollView style={Styles.container}>
          <SearchBar onChangeText={this.searchTextChange.bind(this)} />
          {components}
        </ScrollView>
      </>
    );
  }
}

function SearchBar(props) {
  return (
    <TextInput
      style={Styles.searchBarInput}
      autoCorrect={false}
      selectionColor={C.text01}
      placeholderTextColor={C.dim}
      placeholder="Search users"
      {...props}
    />
  );
}

const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background01,
  },
  searchBarInput: {
    paddingHorizontal: 10,
    marginTop: 10,
    marginBottom: 8,
    alignSelf: "center",
    width: "96%",
    height: moderateScale(40, 0.4),
    borderRadius: 9,
    backgroundColor: C.surface01,
    fontSize: moderateScale(18.5, 0.4),
    fontFamily: K.Fonts.medium,
    color: C.text01,
  },
};
