import Backend from "backend/";
import {
  dNotification,
  notificationFirestore,
} from "backend/NotificationHandler/NotificationFirestore";
import notificationHandler from "backend/NotificationHandler/NotificationHandler";
import { Buttoon, Kitten, Toasty, Txt } from "components/";
import React, { Component } from "react";
import { LayoutAnimation, ScrollView, View, ViewStyle } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { C, moderateScale, spacing } from "utilities/";
import { dateFormat, prettyTime } from "utilities/functions/calendar-functions";

export default class NotificationBroadcast extends Component {
  static navigationOptions = {
    title: "Broadcast",
  };

  state = {
    title: "",
    message: "",
    loading: false,
    scheduledDate: null,
  };

  scheduledDate = null;

  async send() {
    const { title, message, scheduledDate } = this.state;
    const _title = title.length > 0 ? title : "";
    const _message = _title === message ? "" : message;

    try {
      this.setState({ loading: true });
      if (scheduledDate) {
        await notificationFirestore.scheduleGlobalNotification(
          _title,
          _message,
          scheduledDate
        );
        Toasty({
          text1: "Your notification has been scheduled.",
          text2: "Go do something else and we'll take care of it from here!",
        });
        this.scheduledDate = null;
        this.setState({ scheduledDate: null });
      } else {
        await notificationHandler.sendNotificationToEveryone(
          _title,
          _message,
          {}
        );
        Toasty({ text1: "Message sent to everyone!" });
      }
      this.setState({
        title: "",
        message: "",
        date: null,
        time: null,
        loading: false,
      });
    } catch (error) {
      console.log(error);
      this.setState({ loading: false });
      Toasty({
        type: "error",
        text1: "Message failed to send!",
        text2: "Something went wrong!",
      });
    }
  }

  scheduleDateSelected(date: Date) {
    this.scheduledDate = date;
  }

  render() {
    const { title, message, loading, scheduledDate } = this.state;

    return (
      <>
        <Kitten.TopNavigation
          title="Broadcast a Message"
          style={{
            backgroundColor: C.background01,
          }}
          alignment="center"
          accessoryLeft={() => (
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              onPress={() => {
                this.props.navigation.pop();
              }}
              icon={{
                name: "arrow_left",
                color: "white",
                size: moderateScale(20),
              }}
            />
          )}
        />
        <ScrollView
          style={Styles.container}
          keyboardShouldPersistTaps={"always"}
        >
          <Txt.Indicator
            style={{
              textAlign: "center",
              paddingVertical: spacing(2),
              marginBottom: spacing(2),
            }}
          >
            Your message will be broadcasted{"\n"}to every user in the app.
          </Txt.Indicator>
          <Kitten.Input
            //status="primary"
            placeholder="Title your message (optional)"
            value={title}
            onChangeText={(title) => this.setState({ title })}
          />
          <Kitten.Input
            textStyle={{ minHeight: 60 }}
            //status="primary"
            multiline
            placeholder="What would you like to say?"
            value={message}
            onChangeText={(message) => this.setState({ message })}
          />
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
              alignItems: "center",
            }}
          >
            <$_Schedule
              disabled={(title.length < 1 && message.length < 1) || loading}
              onDateSelect={(scheduledDate) => this.setState({ scheduledDate })}
              {...this.props}
            />
            <Buttoon
              // icon={{ name: scheduledDate ? "schedule" : "send" }}
              style={{
                marginTop: spacing(2),
              }}
              onPress={this.send.bind(this)}
              disabled={(title.length < 1 && message.length < 1) || loading}
            >
              {scheduledDate ? "Schedule" : "Send Now"}
            </Buttoon>
          </View>

          <$_ScheduledNotifications />
        </ScrollView>
      </>
    );
  }
}

function $_Schedule(props: {
  onDateSelect: (date: Date) => void;
  disabled?: boolean;
}) {
  const { onDateSelect, disabled } = props;

  const selectedDate = React.useRef<Date>();
  const [_datePickerVisible, setDatePickerVisible] = React.useState(false);
  const [_timePickerVisible, setTimePickerVisible] = React.useState(false);
  const [_showSelectedDate, showSelectedDate] = React.useState(false);

  React.useEffect(
    function resetComponent() {
      if (disabled) showSelectedDate(false);
    },
    [disabled]
  );

  let _prettyDate = selectedDate.current && dateFormat(selectedDate.current);
  if (_prettyDate && _prettyDate.substring(0, 2).startsWith("0"))
    _prettyDate = _prettyDate.substring(1, _prettyDate.length);

  return _showSelectedDate ? (
    <View
      style={{
        minHeight: 45,
        justifyContent: "center",
      }}
    >
      <Txt.S1
        style={{
          color: C.hazardYellow,
          textAlign: "center",
        }}
      >{`Schedule for \n${_prettyDate} @ ${prettyTime(
        selectedDate.current
      )}?`}</Txt.S1>
    </View>
  ) : (
    <>
      <Buttoon
        appearance="ghost"
        // status="warning"
        style={{
          marginTop: spacing(1),
        }}
        disabled={disabled}
        // icon={{ name: "schedule" }}
        onPress={() => setDatePickerVisible(true)}
      >
        Schedule
      </Buttoon>
      <DateTimePickerModal
        minimumDate={new Date()}
        isVisible={_datePickerVisible}
        onConfirm={(date) => {
          selectedDate.current = date;
          setDatePickerVisible(false);
          setTimeout(() => {
            setTimePickerVisible(true);
          }, 325);
        }}
        onCancel={() => setDatePickerVisible(false)}
      />
      <DateTimePickerModal
        date={new Date()}
        mode="time"
        isVisible={_timePickerVisible}
        onConfirm={(date) => {
          selectedDate.current.setHours(date.getHours());
          selectedDate.current.setMinutes(date.getMinutes());
          setTimePickerVisible(false);
          showSelectedDate(true);
          onDateSelect(selectedDate.current);
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        }}
        onCancel={() => setTimePickerVisible(false)}
      />
    </>
  );
}

function $_ScheduledNotifications(props) {
  const [_scheduledNotifications, setScheduledNotficiations] = React.useState<
    dNotification[]
  >([]);
  React.useEffect(function fetchScheduledNotifications() {
    Backend.notificationFirestore.scheduled((notis) =>
      setScheduledNotficiations(notis)
    );
  }, []);
  new Date();
  if (_scheduledNotifications.length < 1) return null;
  return (
    <>
      <Txt.$Title>Scheduled Notifications</Txt.$Title>
      <Kitten.List
        style={{
          width: "100%",
        }}
        data={_scheduledNotifications}
        renderItem={({ item }) => <ScheduledItem notification={item} />}
      />
    </>
  );
}

function ScheduledItem(props: { notification: dNotification }) {
  const { notification } = props;
  const date: Date = notification.timestamp.toDate();
  const _prettyDate = dateFormat(date);
  const _prettyTime = prettyTime(date);

  return (
    <Kitten.ListItem
      style={{
        marginVertical: spacing(1),
      }}
      title={notification.title?.length > 0 && notification.title}
      description={notification.message?.length > 0 && notification.message}
      accessoryLeft={() => (
        <View>
          <Txt.S1>{`${_prettyDate}`}</Txt.S1>
          <Txt.P1>{`${_prettyTime}`}</Txt.P1>
        </View>
      )}
    ></Kitten.ListItem>
  );
}

const Styles = {
  container: {
    backgroundColor: C.background01,
    flex: 1,
    paddingTop: spacing(3),
    paddingHorizontal: spacing(3),
  } as ViewStyle,
};
