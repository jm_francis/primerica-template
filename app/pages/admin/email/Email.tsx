import Backend from "backend/";
import { Buttoon } from "components/";
import * as React from "react";
import firebase from "react-native-firebase";

export default function Email(props) {
  const getUserToken = async () => {
    return (await Backend.accountHandler._user.getIdTokenResult()).token;
  };

  const sendEmail = async () => {
    const payload = {
      recipient: "admin@apptakeoff.com",
      email_subject: "Sending a test email",
      email_html_body: "<strong>Hello World! From the app!</strong>",
    };

    const token = await getUserToken();

    const projectId = firebase.app().options.projectId;

    fetch(
      `https://us-central1-${projectId}.cloudfunctions.net/api/services/emails/send-email`,
      {
        method: "POST",
        mode: "cors",
        body: JSON.stringify(payload),
        headers: {
          "content-type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((res) => res.json())
      .then((resp) => console.log(resp))
      .catch((err) => console.log(err));
  };

  return <Buttoon onPress={sendEmail}>SEND EMAIL</Buttoon>;
}
