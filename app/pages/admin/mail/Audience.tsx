import * as React from "react";
import {
  Buttoon,
  IconPrimr,
  Inpuut,
  Kitten,
  sstyled,
  Toasty,
  Txt,
} from "components/";
import {
  Alert,
  Dimensions,
  NativeSyntheticEvent,
  ScrollView,
  TextInputSelectionChangeEventData,
  View,
  Text,
  StyleSheet,
} from "react-native";
import { C, moderateScale, spacing, fn } from "utilities/";
import Backend from "backend/";
import { ListHeaderButton } from "../../listBuilder/components";
import { NavigationStackScreenProps } from "react-navigation-stack";

// interface d {
export const shareColor = "#2265f5";
// };

/**
 * ### This is where admin users select the audience or hierachy that the mails would be sent to
 *  - Detailed explanation (if any)
 *  ----
 *  @example
 *  Copy and paste function is the best...
 *  ----
 *  @version 21.03.10
 *  -  *Brief changelog*
 *  @author  NL
 *
 **/

//  renderLists(){
//     return(
//         {

//         }
//     )
// }

export function Audience(props: P) {
  const [list, setlist] = React.useState<any>([]);

  React.useEffect(function () {
    setlist(Backend.listBuilderHandler.extractLists());
  }, []);

  const [payload, setpayload] = React.useState<any>(
    props.navigation.getParam("payload")
  );

  const finalItems = [];
  const emailAdd = [];
  for (let i = 0; i < list.length; i++) {
    const element = list[i].contacts;
    if (!element) continue;
    for (let j = 0; j < element.length; j++) {
      const contact = element[j];
      finalItems.push(contact);

      const emailAddresses = element[j].emailAddresses;
      for (let p = 0; p < emailAddresses.length; p++) {
        const email = emailAddresses[p].email;
        // console.log(email);
        emailAdd.push(email);
      }
    }
  }

  function listSelected(listData) {
    // var finalItems = [];
    var emailAdd = [];
    for (let i = 0; i < list.length; i++) {
      const element = list[i]?.contacts;
      if (!element) continue;
      for (let j = 0; j < element.length; j++) {
        const emailAddresses = element[j].emailAddresses;
        for (let p = 0; p < emailAddresses.length; p++) {
          const email = emailAddresses[p].email;
          // console.log(email);
          emailAdd.push(email);
        }
      }
    }

    props.navigation.navigate("SendMail", {
      listData,
      emailAdd,
      payload,
      shareMode: true,
    });
  }

  function sendEveryone() {
    // console.log(emailAdd);
    const listData = {
      contacts: finalItems,
      emailAdd: emailAdd,
      id: "everyone",
      title: "Everyone",
    };

    // console.log(listData[0]);
    props.navigation.navigate("SendMail", {
      listData,
      emailAdd,
      shareMode: true,
    });
  }

  return (
    <SS.Ctnr>
      <Kitten.TopNavigation
        title="Choose a List"
        alignment="center"
        style={{ backgroundColor: C.background01 }}
        accessoryLeft={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              props.navigation.pop();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          />
        )}
      />

      {/* <View style={{ width: "100%" }} > */}
      <ScrollView keyboardShouldPersistTaps="always" keyboardDismissMode="none">
        <Txt.$Title>Choose Audience</Txt.$Title>
        <ListHeaderButton
          listData={{ contacts: finalItems, id: "everyone", title: "Everyone" }}
          onPress={sendEveryone}
        />
        <Txt.$Title>Choose List</Txt.$Title>
        {list.map((index, key) => {
          return (
            <ListHeaderButton
              key={"listHeaderButton." + key}
              listData={index}
              onPress={listSelected.bind(index)}
            />
            // <Text>{index.title}</Text>
          );
        })}
      </ScrollView>
      {/* </View> */}
    </SS.Ctnr>
  );
}

const SS = {
  Ctnr: sstyled(View)((p) => ({
    flex: 1,
    backgroundColor: p.C.background01,
  })),
  CtnrContent: sstyled(View)((p) => ({
    backgroundColor: p.C.background,
    paddingVertical: spacing(2),
    paddingHorizontal: spacing(4),
    justifyContent: "flex-start",
  })),
};

interface P extends NavigationStackScreenProps<dPayload> {}

interface dPayload {
  payload: Object;
}
