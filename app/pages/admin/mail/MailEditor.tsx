import Backend from "backend/";
import { Buttoon, IconPrimr, Kitten, sstyled, Toasty, Txt } from "components/";
import React from "react";
import { ScrollView, Text, View } from "react-native";
// import { IndexPath } from "@ui-kitten/components";
import firebase from "react-native-firebase";
import {
  actions,
  RichEditor,
  RichToolbar,
  defaultActions,
} from "react-native-pell-rich-editor";
import { NavigationStackScreenProps } from "react-navigation-stack";
import { C, fn, moderateScale, spacing } from "utilities/";
import FileNetwork from "backend/apis/FileNetwork";

// import { any } from "ramda";

/**
 * ### This contains the mail editor and also can be used in sending single mail
 * -
 * -
 * ---
 * @version 1.2.22 (aka feb 22th, 2021)
 * -  *Refactor*
 * -  *Document the file*
 * @author  NL
 *
 *
 */

export function MailEditor(props: P) {
  const [templateItem, settemplateItem] = React.useState<any>(
    props.navigation.getParam("item")
  );

  const RichText = React.useRef(); //reference to the RichEditor component
  const [article, setArticle] = React.useState(
    templateItem.id == 1 ? "" : templateItem.HtmlBody
  );
  const [subject, setSubject] = React.useState(
    templateItem.id == 1 ? "" : templateItem.Name
  );
  const [recipient, setrecipient] = React.useState<string>("");
  const [cc, setcc] = React.useState<any>("");

  // this function will be called when the editor has been initialized
  function editorInitializedCallback() {
    RichText.current?.registerToolbar(function (items) {
      // items contain all the actions that are currently active
      console.log(
        "Toolbar click, selected items (insert end callback):",
        items
      );
    });
  }

  const getUserToken = async () => {
    return (await Backend.accountHandler._user.getIdTokenResult()).token;
  };

  const saveTemplate = async () => {
    Toasty({
      text1: "Success",
      text2: "Loading...",
      type: "success",
    });

    const user = Backend.accountHandler._user;

    const token = await getUserToken();

    const projectId = firebase.app().options.projectId;

    const payload = {
      appId: projectId,
      userId: user.uid,
      template_name: subject,
      // template_alias: subject,
      template_subject: subject,
      template_html_body: article,
    };

    if (
      payload.template_name == "" ||
      payload.template_subject == "" ||
      payload.template_html_body == ""
    ) {
      return Toasty({
        text1: "Error",
        text2: "Form Error",
        type: "error",
      });
    } else {
      fetch(
        `https://us-central1-${projectId}.cloudfunctions.net/api/services/emails/create-template`,
        {
          method: "POST",
          mode: "cors",
          body: JSON.stringify(payload),
          headers: {
            "content-type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((res) => {
          console.log("responsing");
          console.log(res);
          if (res.status == 200) {
            Toasty({
              text1: "Success",
              text2: "Template saved",
              type: "success",
            });
            props.navigation.getParam("onpageMove")();
          }
        })
        .catch((err) => {
          console.log("an error occured");
          Toasty({
            text1: "Error",
            text2: "an error occured",
            type: "error",
          });
        });
    }
  };

  const sendAudience = async () => {
    const payload = {
      recipient: "paulnanle611@gmail.com",
      email_subject: subject,
      email_html_body: article,
    };

    props.navigation.navigate("Audience", {
      payload,
    });
  };

  const sendMail = async () => {
    const token = await getUserToken();

    const payload = {
      recipient: recipient,
      email_subject: subject,
      email_html_body: article,
      Cc: cc,
    };

    if (
      payload.recipient == "" ||
      payload.email_subject == "" ||
      payload.email_html_body == ""
    ) {
      return Toasty({
        text1: "Error",
        text2: "Form Error",
        type: "error",
      });
    } else {
      Toasty({
        text1: "Success",
        text2: "Loading...",
        type: "success",
      });

      const projectId = firebase.app().options.projectId;

      fetch(
        `https://us-central1-${projectId}.cloudfunctions.net/api/services/emails/send-email`,
        {
          method: "POST",
          mode: "cors",
          body: JSON.stringify(payload),
          headers: {
            "content-type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((res) => {
          if (res.status == 200) {
            Toasty({
              text1: "Success",
              text2: "Mail sent",
              type: "success",
            });
          }
        })
        .catch(() => {
          console.log("an error occured");
          Toasty({
            text1: "Error",
            text2: "Mail not sent",
            type: "error",
          });
        });
    }
  };

  /**
   * Function us used to update template
   */
  const UpdateTemplate = async () => {
    if (templateItem.id == 1) {
      props.navigation.pop();
    } else {
      Toasty({
        text1: "Updating Template",
        text2: "Loading...",
        type: "success",
      });

      const user = Backend.accountHandler._user;

      const token = await getUserToken();

      const projectId = firebase.app().options.projectId;

      const payload = {
        template_id: templateItem.TemplateId.toString(),
        appId: projectId,
        userId: user.uid,
        template_name: subject,
        // template_alias: subject,
        template_subject: subject,
        template_html_body: article,
      };
      console.log(payload);
      if (
        payload.template_name == "" ||
        payload.template_subject == "" ||
        payload.template_html_body == ""
      ) {
        return Toasty({
          text1: "Error",
          text2: "Form Error",
          type: "error",
        });
      } else {
        fetch(
          `https://us-central1-${projectId}.cloudfunctions.net/api/services/emails/update-template`,
          {
            method: "POST",
            mode: "cors",
            body: JSON.stringify(payload),
            headers: {
              "content-type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((res) => {
            if (res.status == 200) {
              Toasty({
                text1: "Success",
                text2: "Template updated",
                type: "success",
              });
              props.navigation.getParam("onpageMove")();
            }
          })
          .catch((err) => {
            console.log("an error occured");
            Toasty({
              text1: "Error",
              text2: "an error occured",
              type: "error",
            });
          });
      }

      props.navigation.pop();
    }
  };
  /**
   * Used in adding images to the mail editor
   */
  function onPressAddImage() {
    // you can easily add images from your gallery
    RichEditor.current?.insert;

    // FileNetwork.selectAndUpload({
    //   title: "My Image!",
    //   type: "image",
    //   folder: "some/cloudinary/path"
    //  }, progress => console.log(progress))

    FileNetwork.selectAndUpload(
      {
        title: "email_" + fn.js.ID(3),
        type: "any",
      },
      (progress) => {
        console.log("progress: ", progress);
        // setProgress(Math.round(progress));
      }
    ).then((result) => {
      RichText.current?.insertImage(result.uri);
      // setMedia({ uri: result.uri, type: result.type });
      console.log("link url: ", result.uri);
    });

    // fn.media.photoFromGallery().then((respon) => {
    //   console.log(respon.imageInfo.path);

    //   RichText.current?.insertImage(respon.imageInfo.path);
    // });
  }
  /**
   * Used in adding videos to the mail editor
   */
  function insertVideo() {
    // you can easily add videos from your gallery
    RichText.current?.insertVideo(
      "https://mdn.github.io/learning-area/html/multimedia-and-embedding/video-and-audio-content/rabbit320.mp4"
    );
  }

  return (
    <SS.Ctnr>
      <Kitten.TopNavigation
        title="Compose"
        alignment="left"
        style={{ backgroundColor: C.background01 }}
        accessoryLeft={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              UpdateTemplate();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          />
        )}
        accessoryRight={() => (
          <>
            {props.navigation.getParam("item").id == 1 ? (
              <Buttoon
                style={{
                  backgroundColor: "transparent",
                  borderColor: "transparent",
                }}
                onPress={() => {
                  saveTemplate();
                }}
                size={"tiny"}
              >
                Save as template
              </Buttoon>
            ) : null}
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              onPress={() => {
                sendMail();
              }}
              icon={{
                name: "send",
                color: "white",
                size: moderateScale(20),
              }}
            />
          </>
        )}
      />

      {/* <SS.CtnrContent> */}
      <ScrollView
        style={[SS.S.scroll, C.accent && SS.S.scrollDark]}
        keyboardDismissMode={"none"}
      >
        <Kitten.Input
          onChangeText={(subject) => setSubject(subject)}
          placeholder={"Subject"}
          value={subject}
        />
        <Kitten.Input
          onChangeText={(recipient) => setrecipient(recipient)}
          placeholder={"Recipient"}
          value={recipient}
          autoCapitalize={"none"}
          accessoryRight={() => (
            <>
              <Buttoon
                onPress={() => {
                  sendAudience();
                }}
                appearance={"outline"}
                icon={{ name: "plus" }}
              />
            </>
          )}
        />
        <Kitten.Input onChangeText={(cc) => setcc(cc)} placeholder={"CC"} />
        <RichEditor
          disabled={false}
          containerStyle={SS.S.editor}
          ref={RichText}
          style={SS.S.rich}
          // backgroundColor={C.primaryDarker}
          placeholder={"please input content"}
          onChange={(text) => setArticle(text)}
          editorInitializedCallback={editorInitializedCallback}
          // onHeightChange={handleHeightChange}
          initialContentHTML={article}
        />
        <RichToolbar
          style={[SS.S.richBar]}
          editor={RichText}
          disabled={false}
          iconTint={C.primary}
          selectedIconTint={C.primaryDarker}
          disabledIconTint={C.grey500}
          onPressAddImage={onPressAddImage}
          iconSize={15}
          actions={[
            "insertPicture",
            // "insertVideo",
            ...defaultActions,
            actions.setStrikethrough,
            actions.heading1,
          ]}
          // map icons for self made actions
          iconMap={{
            ["insertPicture"]: () => (
              <IconPrimr name="camera-retro" color={C.primary} />
            ),
            [actions.heading1]: ({ tintColor }) => (
              <Text style={[SS.S.tib, { color: tintColor }]}>H1</Text>
            ),
            [actions.setStrikethrough]: () => (
              <IconPrimr name="strike" color={C.primary} />
            ),
            // ["insertVideo"]: () => (
            //   <IconPrimr name="videocam" color={C.primary} />
            // )
          }}
          insertPicture={onPressAddImage}
          // insertVideo={insertVideo}
        />
      </ScrollView>
      {/* </SS.CtnrContent> */}
    </SS.Ctnr>
  );
}

const SS = {
  Ctnr: sstyled(View)((p) => ({
    flex: 1,
    backgroundColor: p.C.background01,
  })),
  CtnrContent: sstyled(View)((p) => ({
    backgroundColor: p.C.background,
    paddingVertical: spacing(1),
    paddingHorizontal: spacing(2),
    justifyContent: "flex-start",
  })),

  S: {
    editor: {
      backgroundColor: C.background,
      borderColor: "black",
      borderWidth: 1,
    },
    rich: {
      minHeight: "60%",
      flex: 1,
    },
    richBar: {
      height: 50,
      backgroundColor: "#0000000",
    },
    nav: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginHorizontal: 5,
    },
    topVi: {
      backgroundColor: "#fafafa",
    },

    richBarDark: {
      backgroundColor: "#191d20",
      borderColor: "#696969",
    },
    scroll: {
      backgroundColor: "#ffffff",
    },
    scrollDark: {
      backgroundColor: "#2e3847",
    },
    darkBack: {
      backgroundColor: "#191d20",
    },

    input: {
      flex: 1,
    },
    tib: {
      textAlign: "center",
      color: "#515156",
    },
    flatStyle: {
      paddingHorizontal: 12,
    },
  },
};

interface P extends NavigationStackScreenProps<dTemplateParams> {}

interface dTemplateParams {
  item: Object;
  emailAdd: string[]; // An array of recepients emails
  listData: Object; //An object of contacts list and also title
  shareMode: boolean;
  onpageMove: Function;
}
