import Backend from "backend/";
import { Buttoon, Kitten, sstyled, Toasty, Txt } from "components/";
// import { SS } from "pages/home/s-explore";
import React from "react";
import { View } from "react-native";
import firebase from "react-native-firebase";
import { NavigationStackScreenProps } from "react-navigation-stack";
import { C, moderateScale, spacing } from "utilities/";
/**
 * ### Section/page to send personalized messages to existing list
 * -  Similar to HitEmUp app
 * -
 * ---
 * @version 1.2.22 (aka feb 22th, 2021)
 * -  *Refactor*
 * -  *Document the file*
 * @author  NL
 *
 *
 */

 export function SendMail(props: P){
    // const listData = ;
    const [listData, setlistData] = React.useState<any>(
      props.navigation.getParam("listData")
    );
    const [emails, setemails] = React.useState<any>(
      props.navigation.getParam("emailAdd")
    );
    const [_title, setTitle] = React.useState(
      props.navigation.state.params.listData.title
    );

     const [contacts, setcontacts] = React.useState<any>(
      listData.contacts
     );

     const [payloads, setpayload] = React.useState<any>(props.navigation.getParam("payload"));

    

    const getUserToken = async () => {
      return (await Backend.accountHandler._user.getIdTokenResult()).token;
    };

    const bactcMail = async () => {
      const token = await getUserToken();
      
      console.log(token);

      const projectId = firebase.app().options.projectId;
        const payload = {
          recipients: emails,
          appId:projectId,
          email_subject: payloads.email_subject,
          email_html_body: payloads.email_html_body,
        };
        
        Toasty({
          text1: "Success",
          text2: "Loading...",
          type: "success",
        });
      //   if(payload.recipient == "" || payload.email_subject == "" || payload.email_html_body == ""){
      //     return Toasty({
      //     text1: "Error",
      //     text2: "Form Error",
      //     type: "error",
      //   });
      // }
      // else{
        

        

        fetch(
          `https://us-central1-${projectId}.cloudfunctions.net/api/services/emails/send-email-batch`,
          {
            method: "POST",
            mode: "cors",
            body: JSON.stringify(payload),
            headers: {
              "content-type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
        .then((res) => {
          console.log('responsing');
          console.log(res.status);
          if(res.status == 200){
            Toasty({
              text1: "Success",
              text2: "Mail sent",
              type: "success",
            });
          }
        })
        .catch(() => {
          console.log("an error occured");
          // console.log(err);
          Toasty({
            text1: "Error",
            text2: "Mail not sent",
            type: "error",
          });
        })
      // }
    }

     return(
        <SS.Ctnr>
            <Kitten.TopNavigation
                title={_title}
                alignment="center"
                style={{ backgroundColor: C.background01 }}
                accessoryLeft={() => (
                    <Buttoon
                      style={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                      }}
                      onPress={() => {
                        props.navigation.pop();
                      }}
                      icon={{
                        name: "arrow_left",
                        color: "white",
                        size: moderateScale(20),
                      }}
                    />
                  )}
            />
            <SS.CtnrContent>
            <Txt.H1 style={{ alignSelf: "center" }}>Ready to send to {_title} ? </Txt.H1>
        
          
              <Buttoon icon={{ name: "plane" }} disabled={contacts.length <= 0 ? true: false} onPress={bactcMail}>
                {'Send ' + contacts.length}
              </Buttoon>

              {/* <Txt.$Title>Preview</Txt.$Title>
              <Buttoon >
                {'Preview'}
              </Buttoon> */}
              {/* <Kitten.ButtonGroup
              size="medium"
              appearance="outline"
              status="success"
              style={{ alignSelf: "center" }}
              >
                <Kitten.Button>
                  {"Preview"}
                </Kitten.Button>
                <Kitten.Button>
                  {"Send" }
                </Kitten.Button>
              </Kitten.ButtonGroup> */}


            </SS.CtnrContent>
        </SS.Ctnr>
         
     )
 }

 const SS = {
    Ctnr: sstyled(View)(p=> ({
      flex: 1,
      backgroundColor: p.C.background01,
    })),
    CtnrContent: sstyled(View)(p => ({
      backgroundColor: p.C.background,
      paddingVertical: spacing(2),
      paddingHorizontal: spacing(4),
      justifyContent: "flex-start",
    }))
  }

  interface P extends NavigationStackScreenProps<dSendMailParam> {}


  interface dSendMailParam{
    listData: Object //TODO need to type this,
    emailAdd: Object,
    payload:Object,
    shareMode: boolean
  }