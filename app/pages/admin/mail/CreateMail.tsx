import Backend from "backend/";
import { Buttoon, Kitten, sstyled, Txt, Toasty } from "components/";
import React from "react";
import { Alert, FlatList, RefreshControl, View } from "react-native";
import firebase from "react-native-firebase";
import { NavigationStackScreenProps } from "react-navigation-stack";
import { C, moderateScale, spacing } from "utilities/";
import SwipeableItem from "react-native-swipeable-item";
/**
 * ### Section/page to send personalized messages to existing list
 * -  Similar to HitEmUp app
 * -
 * ---
 * @version 1.2.22 (aka feb 22th, 2021)
 * -  *Refactor*
 * -  *Document the file*
 * @author  NL
 *
 *
 */



const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

export function CreateMail(props: P) {
  const getUserToken = async () => {
    return (await Backend.accountHandler._user.getIdTokenResult()).token;
  };

  function buttonClicked(item: Object) {
    // console.log(item);
    
    props.navigation.navigate("MailEditor", {
      item,
      onpageMove : () => {getEmailTemplate()}
    });
  }

  const [templates, settemplates] = React.useState<any>([]);
  const [refreshing, setRefreshing] = React.useState(false);
  const [loader, setloader] = React.useState<any>(true);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => {
      getEmailTemplate();
      setRefreshing(false);
    });
  }, []);

  
  React.useEffect(function updateCreate () {
    getEmailTemplate();
  
  }, []);

  const deleteTemplate  = async (item) =>{
    console.log(item.TemplateId);

    const token = await getUserToken();
    // console.log(token);
    const projectId = firebase.app().options.projectId;
    const payload = {
      template_id:item.TemplateId.toString()
    };

    console.log(JSON.stringify(payload));

    fetch(
      `https://us-central1-${projectId}.cloudfunctions.net/api/services/emails/delete-template`,
      {
        method: "POST",
        mode: "cors",
        body: JSON.stringify(payload),
        headers: {
          "content-type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
    )
    .then((res) => {
      // console.log('responsing');
      // console.log(res);
      console.log(res.status);
      if(res.status == 200){
        Toasty({
          text1: "Success",
          text2: "Delete Successful",
          type: "success",
        });

        getEmailTemplate();
      }
    })
      .catch((err) => {
        console.log("an error occured");
        console.log(err);
      });
  }

  const Item = ({ item, onPress }) => (
    <Kitten.Card
      onPress={onPress}
      footer={() => (
        <>
          <Buttoon
            icon={{ name: "trash" }}
            status={"danger"}
            appearance={"ghost"}
            style={{alignSelf:"flex-end"}}
            onPress={() => {
              deleteTemplate(item);
              // props.navigation.pop();
            }}
            
          />
        </>
      )}
    >
      {item.id == 1 ? (
        <Txt.H1
          style={{ alignSelf: "center", paddingBottom: 20, paddingTop: 20 }}
        >
          {item.name}
        </Txt.H1>
      ) : (
        <Txt.$Title style={{ padding: 10 }}>{item.Name}</Txt.$Title>
      )}
      <Txt.P1 numberOfLines={4} style={{ padding: 10 }}>
        {item.Subject}
      </Txt.P1>
    </Kitten.Card>
  );



  const getEmailTemplate = async () => {
    const token = await getUserToken();

    
    const projectId = firebase.app().options.projectId;

    fetch(
      `https://us-central1-${projectId}.cloudfunctions.net/api/services/emails/get-templates`,
      {
        method: "GET",
        mode: "cors",
        headers: {
          "content-type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        let _templates = [
          {
            id: 1,
            name: "+",
          },
        ];

        data.forEach((item) => {
          // setmails([...mails, item.data()])
          _templates.push(item);
        });
        settemplates(_templates);
        setloader(false);
        // console.log(_templates);
      })
      .catch((err) => {
        console.log("an error occured");
        console.log(err);
      });
  };

  React.useEffect(
    function () {
      getEmailTemplate();
    },
    [props.navigation]
  );

  return (
    <SS.Ctnr>
      <Kitten.TopNavigation
        title="Create Email"
        alignment="center"
        style={{ backgroundColor: C.background01 }}
        accessoryLeft={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              props.navigation.getParam("onpageEmails")();
              // Alert.alert("it passed here");
              props.navigation.pop();
              
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          />
        )}
      />
      {/* <SS.CtnrContent> */}
      {loader == true ? (
        <View style={SS.S.loading}>
          <Kitten.Spinner status={"control"} size={"giant"} />
        </View>
      ) : (
        <FlatList
          data={templates}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
              tintColor={"#00ffff"}
              progressBackgroundColor={"#00ffff"}
            />
          }
          renderItem={({ item, index }) => {
            if (index == 0) {
              return (
                <Buttoon
                  icon={{
                    name: "plus",
                    size: 35,
                  }}
                  // size={"large"}
                  style={{
                    width: "47%",
                    margin: 5,
                    borderRadius:10
                  }}
                  appearance={"outline"}
                  onPress={() => buttonClicked(item)}
                />
              );
            } else {
              return (
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    margin: 5,
                  }}
                >
                  <Item
                    item={item}
                    onPress={() => buttonClicked(item)}

                    // style={{ backgroundColor }}
                  />
                  {/* <Kitten.item */}
                </View>
              );
            }
          }}
          //Setting the number of column
          numColumns={2}
        />
      )}

      {/* </SS.CtnrContent> */}
    </SS.Ctnr>
  );
}

const SS = {
  Ctnr: sstyled(View)((p) => ({
    flex: 1,
    backgroundColor: p.C.background01,
  })),
  CtnrContent: sstyled(View)((p) => ({
    backgroundColor: p.C.background,
    paddingVertical: spacing(2),
    paddingHorizontal: spacing(4),
    justifyContent: "flex-start",
  })),
  row: sstyled(View)((p) => ({
    flex: 1,
    justifyContent: "space-around",
  })),
  S: {
    loading: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
  },
  htmlStyle: {
    a: {
      fontWeight: "bold",
      color: "purple",
    },
    div: {
      fontFamily: "arial",
    },
    p: {
      fontSize: 10,
      color: "#fff",
    },
  },
};

interface P extends NavigationStackScreenProps {}

interface dTemplateParams {
  item: Object;
  emailAdd: string[]; // An array of recepients emails
  listData:Object; //An object of contacts list and also title
  shareMode: boolean,
  onpageEmails: Function
}

