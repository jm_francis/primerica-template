// import { spacing, PRE, typoPrimr, PROPS_Colors, scale } from "utilities";

import { Buttoon } from "components/";
import { APP_LOGO_TRANSPARENT } from "constant-deprecated/Images";
// import { withTheme, withAuthe } from "engines/providers";
import * as React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
// import { Modalize } from "react-native-modalize";
import { moderateScale } from "react-native-size-matters";
import { C, getBottomSpace, IS_ANDROID, scale, spacing } from "utilities/";
// import { SHEET_EmailAuth } from "./$_EmailAuth";
// import O$_EmailAuth from "./$_EmailAuth";
import { PROPSSCR_Welcome } from "./welcome-page.props";

class WelcomePage extends React.PureComponent<PROPSSCR_Welcome> {
  render() {
    const { navigation } = this.props;
    // const $$emailRef = React.createRef<Modalize>();
    return (
      <View style={SS(C).FULL}>
        <$_MainHall {...this.props} />
        <View style={[SS().$_CTNR]} />
        <$_Footer
          {...this.props}
          onEmailPress={() => {
            // $$emailRef.current.open();
            navigation.navigate("SignIn")
          }}
        />
        {/* <SH_EmailAuth {...this.props} onRef={$$emailRef} /> */}
      </View>
    );
  }
}

function $_MainHall(props) {
  const {} = props;
  return (
    <View style={SS().$_CTNR}>
      {/* <Image
        source={APP_LOGO}
        style={{ width: 200, height: 200, margin: 20 }}
      /> */}
      {/* <IconPrimr name={"placeholder"} size={scale(100)} color={C.text01} /> */}
      <Image
        source={APP_LOGO_TRANSPARENT}
        style={{
          width: scale(120),
          height: scale(120),
        }}
      />
    </View>
  );
}

function $_Footer(props) {
  const { onEmailPress } = props;
  return (
    <View style={SS().FOOTER}>
      <Text style={SS().TITLE}>{"Let's get started!"}</Text>
      <Buttoon
        style={{
          marginBottom: spacing(4),
        }}
        compact
        onPress={onEmailPress}
        icon={{ name: "arrow_right", right: true }}
      >
        Continue with email
      </Buttoon>
      {/* <Buttoon testID="debug" text="To Main Stack" onPress={() => navigation.navigate("MainSTK")} /> */}
      {/* <Text style={SS(C).CAPTION}>{"Powered by\nApp Takeoff"}</Text> */}
    </View>
  );
}

// function SH_EmailAuth(props) {
//   return <SHEET_EmailAuth {...props} />;
// }

const SS = () => {
  return StyleSheet.create({
    FULL: { flex: 1, backgroundColor: C.background01 },
    $_CTNR: {
      flex: 1,
      // paddingHorizontal: PRE().SCR.paddingHorizontal,
      paddingHorizontal: 30,
      justifyContent: "center",
      alignItems: "center",
    },
    WORDMARK: {
      // ...typoPrimr.largeTitle,

      textAlign: "center",
      // color: C.text01,
      color: "white",
    },
    TITLE: {
      // ...typoPrimr.largeTitle,
      fontFamily: IS_ANDROID ? "Lato-Black" : "AvenirNext-Bold",
      fontSize: moderateScale(25),
      textAlign: "center",
      // color: C.text01,
      color: "white",
      marginVertical: spacing(4),
    },
    CAPTION: {
      // ...typoPrimr.caption,
      // fontFamily: "Lato-Black",
      alignItems: "center",
      textAlign: "center",
      color: "grey",
      fontWeight: "700",
    },
    CONTENT: {
      // ...typoPrimr.body,
      lineHeight: 20,
      marginVertical: 40,
      textAlign: "center",
      // color: C.text,
      color: "black",
    },
    FOOTER: {
      paddingBottom: getBottomSpace("safe"),
      // ...PRE().FOOTER
    },
  });
};

// export default withTheme(withAuthe(observer(WelcomeScreen)));
export default WelcomePage;
