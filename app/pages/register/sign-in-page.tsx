import Backend from "backend/";
import { Buttoon, dAccessory, IconPrimr, Kitten, Toasty } from "components/";
import R from "ramda";
import * as React from "react";
import { Alert, Platform, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
// import { useSafeAreaInsets } from "react-native-safe-area-context";
import { scale, spacing } from "utilities/";

export default function SignInPage(props) {
  const { navigation, onRef } = props;

  const [_name, setName] = React.useState("");
  const [_email, setEmail] = React.useState("");
  const [_password, setPassword] = React.useState("");
  const [_access_code, setAccessCode] = React.useState("");

  // Check if access code is correct
  const [_isCodeCorrect, shouldCodeCorrect] = React.useState(true);
  // Toggle password
  const [_pwShown, showPw] = React.useState(false);
  // Show/hide resend button (currently deprecated)
  const [_resendBtnVIS] = React.useState(false);

  // Is authentication task started?
  const [_runningTSKS, startTSKS] = React.useState(false);

  function checkAccessCode() {
    Backend.localHandler.setAppPassword(_access_code);
    const testPassword = Backend.localHandler.testAppPassword();
    shouldCodeCorrect(testPassword);
  }
  async function register(xong?) {
    startTSKS(true);
    console.log(_name);
    try {
      // team access code
      await Backend.localHandler.setAppPassword(_access_code);
      const testPassword = await Backend.localHandler.testAppPassword();
      if (!testPassword) {
        Toasty({
          type: "error",
          text1: "Incorrect Team Access Code",
          text2: "Please ask a team member for the correct team access code.",
        });
        xong();
        return;
      }
      Backend.authHandler
        .connectWithEmail(_name, _email, _password)
        .then((result) => {
          console.log("AUTH CODE: ", result.code);
          xong();
          /**
           * ⚠️⚠️ BEFORE YOU START WORKING ON THIS PART:
           * TAKE A DEEP BREATH, AND UNDERSTAND THE LOGIC
           * IT'S KINDA COMPLICATED AS THE FUNCTION CONNECTING
           *  this.state,
           *  `Backend.authHandler.connectwithEmail()`
           *  user.store
           *
           * @difficulty [***]
           * READY? LET'S GO..
           */
          // console.log("uid: ", result.uid);
          //   console.log("isAdmin:  ", isAdmin);
          switch (result.code) {
            /**
             * @see EMAIL_EXISTED_VERIFIED:
             *
             * Set a successful caption to inform user
             * Should check `userCompletion()`
             */
            case "EMAIL_EXISTED_VERIFIED":
              startTSKS(true);
              Toasty({
                text1: "Login successful",
                text2: _email,
              });

              setTimeout(async () => {
                // setResendBtnVIS(true)
                await Backend.localHandler.setAppPassword(_access_code);
                navigation.navigate("HomeSCR");
              }, 3000);
              break;

            /**
             * @see EMAIL_EXISTED_xVERIFIED:
             *
             * 2 scenarios:
             *  * Log in again but not yet verified:
             *      Set a `welcome back` caption to inform user
             *      Show resend btn after some time in case user want to resend
             *  * Hit resend button (aka re-run `register()` )
             *      Set a `Checking your acc` caption to inform user
             */
            case "EMAIL_EXISTED_xVERIFIED":
              _resendBtnVIS
                ? Toasty({
                    text1: "Checking your account...",
                    text2:
                      "We'll send an email to " +
                      _email +
                      " when it's all done.",
                  })
                : Toasty({
                    text1: "Welcome back",
                    text2: _email,
                  });
              setTimeout(() => {
                // setResendBtnVIS(true)
                navigation.navigate("HomeSCR");
              }, 3000);
              break;
            /**
             * @see EMAIL_xEXISTED:
             *
             * Set caption to inform user
             * Show resend btn after some time in case user want to resend ( deprecated for now and navigate to ProfileSetupFLO)
             */
            case "EMAIL_xEXISTED":
              Toasty({
                text1: "Setting up your account...",
                //* We don't do verification
                // text2:
                //   "We'll send a verification email to " +
                //   _email +
                //   " when it's all done",
              });
              setTimeout(() => {
                // setResendBtnVIS(true)
                navigation.navigate("Permissions");
              }, 3000);
              break;
            /**
             * @see ERR_EMAIL_AUTH:
             * errToast shows internally
             */
            case "ERR_EMAIL_AUTH":
              startTSKS(false);
              break;
            default:
              return null;
              break;
          }
        });
    } catch (error) {
      xong();
      Toasty({
        type: "error",
        text1: "Error with server",
        text2: "Please try again later",
      });
      console.warn("err registering: ", error);
    }
  }

  /**
   * Reset password fn
   */
  function resetPassword(
    xong?,
    title: string = "Reset your password",
    caption: string = "Do you want to reset password for " + _email + "?"
  ) {
    Alert.alert(
      title,
      caption,
      [
        {
          text: "No",
          onPress: () => {
            xong();
          },
        },
        {
          text: "Yes",
          onPress: () =>
            Backend.authHandler.resetPassword(_email).then(() => {
              Toasty({
                text1: "Check your mailbox",
                text2: "A password reset email is sent to\n" + _email,
              });
              xong();
            }),
        },
      ],
      { cancelable: false }
    );
  }
  //@ts-ignore
  const emailRef = React.useRef<Kitten.Input>();
  //@ts-ignore
  const pwRef = React.useRef<Kitten.Input>();
  //@ts-ignore
  const codeRef = React.useRef<Kitten.Input>();

  // const insets = Platform.OS === "ios" ? useSafeAreaInsets() : { top: 0 };
  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: "#222B45" }}
      keyboardShouldPersistTaps="always"
      scrollEnabled={false}
      contentContainerStyle={{
        paddingHorizontal: scale(15),
      }}
    >
      <Kitten.TopNavigation
        alignment="center"
        title="Sign In"
        accessoryLeft={(props) => (
          <Kitten.TopNavigationAction
            icon={(props: dAccessory) => (
              <IconPrimr
                onPress={() => {
                  // onRef.current.close();
                  navigation.goBack();
                }}
                name="x"
                size={props.style.width}
                color={props.style.tintColor}
              />
            )}
          />
        )}
      />
      <View>
        {/* <Kitten.Input
            // style={{ color: C.text, backgroundColor: C.background }}
            placeholder={"Your Name"}
            // label={"First & Last Name"}
            value={_name}
            onChangeText={setName}
            autoFocus={true}
          /> */}
        <Kitten.Input
          ref={emailRef}
          placeholder={"Personal Email"}
          autoFocus={true}
          // disabled={_runningTSKS || !R.isNil(frbsAuthe)}
          autoCapitalize="none"
          keyboardType="email-address"
          value={_email}
          onChangeText={(text) => setEmail(text)}
          onSubmitEditing={() => {
            setTimeout(() => pwRef.current.focus(), 50);
          }}
        />
        <Kitten.Input
          ref={pwRef}
          placeholder={"Personal Password"}
          // disabled={_runningTSKS}
          secureTextEntry={!_pwShown}
          value={_password}
          maxLength={16}
          onChangeText={setPassword}
          onSubmitEditing={() => {
            setTimeout(() => codeRef.current.focus(), 50);
          }}
          accessoryRight={(props: dAccessory) => (
            <IconPrimr
              onPress={() => showPw(!_pwShown)}
              name={_pwShown ? "eye" : "eye_slash"}
              size={props.style.width}
              color={props.style.tintColor}
            />
          )}
        />
        <Kitten.Input
          ref={codeRef}
          placeholder={"App Access Code"}
          // disabled={_runningTSKS || !R.isNil(frbsAuthe)}
          autoCapitalize="none"
          status={_isCodeCorrect ? "basic" : "error"}
          caption={_isCodeCorrect ? null : "App access code incorrect."}
          keyboardType="email-address"
          value={_access_code}
          onChangeText={(text) => setAccessCode(text)}
          onSubmitEditing={() => {
            checkAccessCode();
          }}
        />
      </View>
      <View
        style={{
          flex: 2,
          paddingVertical: scale(15),
          justifyContent: "space-between",
        }}
      >
        <Buttoon
          disabled={R.isNil(_email) || R.isNil(_password)}
          //   tx={
          //     _resendBtnVIS
          //       ? "welcomeScreen.auth.verified"
          //       : "welcomeScreen.auth.letStart"
          //   }
          // text={_resendBtnVIS ? "Verified? Let's Start!" : "Let's Start with Email"}
          progress={true}
          onPress={(xong) => register(xong)}
        >
          Let's Start with Email
        </Buttoon>

        {_resendBtnVIS && (
          <Buttoon
            disabled={R.isNil(_email)}
            appearance="ghost"
            progress={true}
            onPress={(xong) => register(xong)}
          >
            Resend
          </Buttoon>
        )}
        {!_resendBtnVIS && (
          <Buttoon
            appearance="ghost"
            disabled={R.isNil(_email)}
            progress={true}
            onPress={(xong) => resetPassword(xong)}
          >
            Reset Password
          </Buttoon>
        )}
      </View>
    </ScrollView>
  );
}
