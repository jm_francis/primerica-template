import React, { Component } from "react";
import { View, Text, ViewStyle } from "react-native";
import { QuickStyle } from "react-native-shortcuts";
import { verticalScale, scale } from "utilities/";
import Background from "root/Background";
import K from "constant-deprecated";
import Backend from "backend/";
import { Buttoon, Toasty } from "components/";

export default class Permissions extends Component {
  state = {
    show: false,
  };

  submitPress() {
    this.props.navigation.navigate("Home");
  }

  componentDidMount() {
    setTimeout(() => {
      Backend.permissionHandler.requestAll();
      this.setState({ show: true });
    }, 4000);
  }

  render() {
    const { show } = this.state;

    return (
      <>
        <Background />
        <View style={Styles.container}>
          <View style={Styles.contents}>
            <Text style={Styles.prompt}>
              {
                "The app requires certain permissions in order to function properly.\n\nPlease approve all incoming requests..."
              }
            </Text>

            <View style={{ minHeight: 100 }}>
              {show ? (
                <>
                  <Buttoon compact onPress={this.submitPress.bind(this)}>
                    Done
                  </Buttoon>
                </>
              ) : null}
            </View>
          </View>
        </View>
      </>
    );
  }
}

const Styles = {
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  } as ViewStyle,
  contents: {
    width: "100%",
    height: "80%",
    alignItems: "center",
    justifyContent: "space-evenly",
    paddingHorizontal: 20,
  } as ViewStyle,
  prompt: {
    color: "white",
    fontFamily: K.Fonts.medium,
    textAlign: "center",
    fontSize: verticalScale(21.2),
    ...QuickStyle.boxShadow,
  } as ViewStyle,
  submitButton: {
    marginTop: 50,
  } as ViewStyle,
};
