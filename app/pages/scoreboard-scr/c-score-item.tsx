import Backend from "backend/";
import { dScore } from "backend/schemas/scoreboards";
import { dScoreboard } from "backend/ScoreboardHandler/scoreboard-handler.props";
import { Buttoon, dAccessory, Kitten, sstyled, Toasty, Txt } from "components/";
import React, { useState } from "react";
import { LayoutAnimation, TextInput, View } from "react-native";
import { C } from "utilities/";

interface P {
  scoreboard: dScoreboard;
  scoreItem: dScore;
  index: number;
}

/**
 * ### A user's score (list item)
 * 
 * @author jm_francis
 * @version 1.1.26
 * 
 * @example
 * <ScoreItem
      {...props}
      index={idx}
      user={user}
      onSubmit={(score) => {
        updateFRBSScore(u.name, score);
      }}
    />
 */
export function ScoreItem(props: P) {
  const { scoreItem: item, index } = props;
  const user = Backend.adminHandler.getUserByUid(item.uid);
  /** Check if this user is admin */
  return (
    <Kitten.ListItem
      title={`${item.name}`}
      // description={`${item.description} ${index + 1}`}
      accessoryLeft={(p: dAccessory) => (
        // <IconPrimr name={"dot"} color={C.hazardYellow} />
        <SS.MedalCtnr {...p}>
          <SS.TxtMedal {...p}>{index + 1}</SS.TxtMedal>
          {/* {user.profileImage && user.profileImage.length > 0 ? (
            <Avatar source={{ uri: user.profileImage }} size="small" />
          ) : (
            <IconPrimr name={`profile`} size={scale(30)} color={C.pitchWhite} />
          )} */}
        </SS.MedalCtnr>
      )}
      accessoryRight={(p) => <ScoreEdit {...props} />}
    />
  );
}

/**
 * ###  Button to edit member's score
 * -  Only if u're admin
 * TODO @K->@J: Can you add admin function
 * @author K
 * @version 1.1.29
 */
export const ScoreEdit = (p: P) => {
  const [_score, setScore] = useState(p.scoreItem.score);
  const [prevScore, setPrevScore] = useState(0);
  const [urScoreInputShown, showUrScoreInput] = useState(false);
  const [btnLabel, setBtnLabel] = useState<number | string>(p.scoreItem.score);
  const refUrScore = React.useRef<TextInput>();

  const isAdmin = Backend.firestoreHandler._account.admin;
  let CASE = { ADMIN: isAdmin, NON_ADMIN: !isAdmin };

  if (CASE.ADMIN)
    return (
      <SS.CtnrUrScore>
        {urScoreInputShown && (
          <Kitten.Input
            size="small"
            ref={refUrScore}
            placeholder={"New Score"}
            value={_score.toString()}
            onChangeText={(t) => setScore(Number.parseFloat(t))}
            keyboardType={"numeric"}
            selectTextOnFocus
            onBlur={() => {
              LayoutAnimation.configureNext(
                LayoutAnimation.Presets.easeInEaseOut
              );
              setBtnLabel(prevScore);
              showUrScoreInput(false);
              setScore(prevScore);
            }}
          />
        )}
        {urScoreInputShown && (
          <Buttoon
            appearance="ghost"
            status="basic"
            size="small"
            // icon={{ name: "x" }}
            onPress={() => {
              LayoutAnimation.configureNext(
                LayoutAnimation.Presets.easeInEaseOut
              );
              setBtnLabel(prevScore);
              showUrScoreInput(false);
              setScore(prevScore);
            }}
          >
            Cancel
          </Buttoon>
        )}
        <Buttoon
          appearance="outline"
          status="basic"
          onPress={() => {
            LayoutAnimation.configureNext(
              LayoutAnimation.Presets.easeInEaseOut
            );
            setBtnLabel(urScoreInputShown ? _score : "Update");
            showUrScoreInput(!urScoreInputShown);
            setPrevScore(_score);

            try {
              Backend.scoreboardHandler.setUsersScore(
                p.scoreboard,
                p.scoreItem.uid,
                _score
              );
            } catch (error) {
              console.log(error);
              Toasty({
                type: "error",
                text1: "Failed to post score!",
                text2: "Unable to connect to server. Please try again later.",
              });
            }

            setTimeout(() => {
              !urScoreInputShown && refUrScore.current?.focus();
            }, 500);
          }}
          style={{ borderRadius: 60 }}
        >
          <Txt.H5>{btnLabel}</Txt.H5>
        </Buttoon>
      </SS.CtnrUrScore>
    );
  // if (CASE.NON_ADMIN)
  else
    return (
      <SS.CtnrUrScore>
        <Buttoon
          // appearance="ghost"
          status="basic"
          size="small"
          onPress={() => {}}
        >
          {btnLabel}
        </Buttoon>
      </SS.CtnrUrScore>
    );
};

const SS = {
  CtnrUrScore: sstyled(View)((p) => ({
    flexDirection: "row",
    flex: 1,
    // backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "flex-end",
  })),
  MedalCtnr: sstyled(View)((p) => ({
    // width: p.style.width,
    // height: p.style.height,
    // backgroundColor: C.hazardYellow,
    // justifyContent: "center",
    // alignItems: "center",
    // borderRadius: 100,
    flexDirection: "row",
  })),
  TxtMedal: sstyled(Txt)((p) => ({
    fontSize: p.style.width * 0.6,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: C.hazardYellow,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: p.style.width / 2,
    overflow: "hidden",
  })),
};

interface dScoreItem {
  scoreItem: dScore;
  index: number;
  onSubmit(score: number | string): void;
}
