import { Input } from "@ui-kitten/components";
import Backend from "backend/";
import {
  dScore,
  dScoreboard,
} from "backend/ScoreboardHandler/scoreboard-handler.props";
import { Buttoon, IconPrimr, Inpuut, Kitten, Toasty } from "components/";
import { defaultLayoutAnimation } from "pages/listBuilder/configure";
import React, { useState } from "react";
import { Alert, LayoutAnimation, View } from "react-native";
import { C, moderateScale, spacing } from "utilities/";

/**
 * ###  Section to add ur score
 */
export function $_SboardActions(props: {
  scoreboard: dScoreboard;
  navigation: any;
  addAScorePress: () => void;
}) {
  const { scoreboard } = props;

  React.useEffect(function fetchUrScore() {
    Backend.scoreboardHandler.scoreboards((scoreboards) => {
      // const potentialScoreboard = scoreboards.filter((sb) => sb.id === scoreboard.id)
      // scoreboards
      //   .filter((sb) => sb.id === scoreboard.id)[0]
      //   .people.filter((score: dScore) => score.name === account.name)[0]
      //   ?.score;
    });
  }, []);
  const account = Backend.firestoreHandler._account;

  //#region [SECTION ]  Add Your Score
  const [urScore, setUrScore] = useState(0);
  const refUrScore = React.useRef<Input>();

  const [scoreVisible, setScoreVisible] = React.useState(false);
  const [adminOptionsVisible, setAdminOptionsVisible] = React.useState(false);
  const [label1, setLabel1] = React.useState("Your Score");
  //#endregion

  /**
   * Using concat because if you add a null item into a Kitten.ButtonGroup, UIKitten tries to do null.props and then the app crashes
   */
  return (
    <View style={{ alignItems: "center" }}>
      <Kitten.ButtonGroup appearance="ghost">
        {[
          <Buttoon
            icon={
              label1 === "Your Score"
                ? { name: "plus" }
                : { name: "x", color: C.hazardYellow }
            }
            onPress={() => {
              if (!account.name || account.name.length < 1) {
                Toasty({
                  type: "info",
                  text1: "Please set your name in your profile first!",
                });
                return;
              }
              LayoutAnimation.configureNext(
                LayoutAnimation.Presets.easeInEaseOut
              );
              setLabel1(label1 == "Your Score" ? "Cancel" : "Your Score");
              setScoreVisible(!scoreVisible);
            }}
          >
            {label1}
          </Buttoon>,
        ].concat(
          Backend.firestoreHandler._account?.admin === true
            ? [
                <Buttoon
                  icon={
                    adminOptionsVisible ? { name: "check" } : { name: "pen" }
                  }
                  onPress={() => {
                    LayoutAnimation.configureNext(
                      LayoutAnimation.Presets.easeInEaseOut
                    );
                    setAdminOptionsVisible(!adminOptionsVisible);
                  }}
                >
                  {adminOptionsVisible ? "Done" : "Admin"}
                </Buttoon>,
              ]
            : []
        )}
      </Kitten.ButtonGroup>
      {adminOptionsVisible && <$_AdminOptions {...props} />}
      {scoreVisible && (
        <View style={{ paddingHorizontal: spacing(5) }}>
          <Inpuut
            ref={refUrScore}
            autoFocus={true}
            keyboardType="numeric"
            title="Enter Your Score"
            placeholder="0"
            doneText="Save"
            value={urScore}
            onChangeText={(text) => setUrScore(Number.parseFloat(text))}
            onSavePress={() => {
              Alert.alert(
                "Confirmation",
                "Do you to add/overwrite your score?",
                [
                  { text: "Cancel", onPress: () => {} },
                  {
                    text: "Yes",
                    onPress: () => {
                      LayoutAnimation.configureNext(
                        LayoutAnimation.Presets.easeInEaseOut
                      );
                      console.log("saving... " + urScore);
                      Backend.scoreboardHandler.setMyScore(urScore, scoreboard);
                      setLabel1(
                        label1 == "Your Score" ? "Cancel" : "Your Score"
                      );
                      setScoreVisible(false);
                      Toasty({ text1: "Score added" });
                    },
                  },
                ],
                { cancelable: true }
              );
            }}
            isUrl={false}
          />
        </View>
      )}
    </View>
  );
}

interface dAdminOptionsProps {
  scoreboard: dScoreboard;
  navigation: any;
  addAScorePress: () => void;
}
export function $_AdminOptions(props: dAdminOptionsProps) {
  const { scoreboard, navigation, addAScorePress } = props;
  const [_showEditTitle, showEditTitle] = React.useState(false);
  const [_showEditSubtitle, showEditSubtitle] = React.useState(false);
  const [_boardTitle, setBoardTitle] = React.useState(scoreboard?.title);
  const [_boardSubtitle, setBoardSubtitle] = React.useState(
    scoreboard?.subtitle
  );

  /**
   * @note check and worked ✅
   */
  const resetScoreboard = () => {
    Alert.alert("Are you sure you would like to reset this scoreboard?", "", [
      { text: "Cancel", style: "cancel" },
      {
        text: "Yes!",
        onPress: () => {
          Backend.scoreboardHandler.resetScoreboard(scoreboard.title);
          // setMenuVisible(false);
          Toasty({ text1: "Successfully reset scoreboard" });
        },
      },
    ]);
  };
  /**
   * @note check and worked ✅
   */
  const deleteScoreboard = () => {
    Alert.alert(
      "Are you sure you would like to DELETE this scoreboard?",
      "It will be gone forever!",
      [
        { text: "Cancel", style: "cancel" },
        {
          text: "Yes!",
          onPress: async () => {
            await Backend.scoreboardHandler.deleteScoreboard(scoreboard.title);
            navigation.pop();
            Toasty({ text1: "Scoreboard deleted." });
          },
        },
      ]
    );
  };

  async function moveScoreboardLeft() {
    moveScoreboard(0);
  }
  async function moveScoreboardRight() {
    moveScoreboard(1);
  }
  async function moveScoreboard(index: number) {
    const allScoreboards: dScoreboard[] =
      Backend.scoreboardHandler._scoreboards;
    let leftScoreboard: dScoreboard;
    // let indexOfThisScoreboard: number;
    let rightScoreboard: dScoreboard;
    for (let x = 0; x < allScoreboards.length; x++) {
      const sb = allScoreboards[x];
      if (sb.id === scoreboard.id) {
        // indexOfThisScoreboard = x;
        leftScoreboard = x - 1 >= 0 ? allScoreboards[x - 1] : null;
        rightScoreboard =
          x + 1 < allScoreboards.length ? allScoreboards[x + 1] : null;
        break;
      }
    }
    if (index === 0) {
      // left
      if (!leftScoreboard) {
        Toasty({
          text1: "Failed to move scoreboard.",
          text2: "This scoreboard is already all the way to the left.",
          type: "error",
        });
        return;
      }
      /**
       * To create a card moving animation on home-scr
       */
      navigation.pop();
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      try {
        await Backend.scoreboardHandler.updateScoreboard(scoreboard.title, {
          position: leftScoreboard.position - 0.5,
        });
        Toasty({ text1: "Scoreboard moved." });
      } catch (error) {
        console.log(error);
        Toasty({ text1: "Something went wrong on the server.", text2: error });
      }
    } else if (index === 1) {
      // right
      if (!rightScoreboard) {
        Toasty({
          text1: "Failed to move scoreboard.",
          text2: "This scoreboard is already all the way to the right.",
          type: "error",
        });
        return;
      }
      navigation.pop();
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      try {
        await Backend.scoreboardHandler.updateScoreboard(scoreboard.title, {
          position: rightScoreboard.position + 0.5,
        });
        Toasty({ text1: "Scoreboard moved." });
      } catch (error) {
        console.log(error);
        Toasty({ text1: "Something went wrong on the server.", text2: error });
      }
    }
  }

  return (
    <View
      style={{
        borderWidth: 0.7,
        borderColor: C.primary,
        width: "100%",
        borderRadius: 7,
        overflow: "hidden",
      }}
    >
      <Kitten.MenuItem
        style={{ backgroundColor: C.surface }}
        title="Add a Score"
        accessoryLeft={(_props) => (
          <IconPrimr name="plus" color="white" size={moderateScale(15)} />
        )}
        onPress={addAScorePress}
      />
      <Kitten.MenuItem
        style={{ backgroundColor: C.surface }}
        title="Reset Scores"
        accessoryLeft={(_props) => (
          <IconPrimr name="x" color="white" size={moderateScale(15)} />
        )}
        onPress={resetScoreboard}
      />
      <Kitten.MenuGroup title="Edit Title">
        {!_showEditTitle ? (
          <Kitten.MenuItem
            style={{ backgroundColor: C.surface }}
            title="Title"
            accessoryRight={(_props) => (
              <IconPrimr name="edit" color="white" size={moderateScale(15)} />
            )}
            onPress={() => {
              showEditSubtitle(false);
              showEditTitle(true);
            }}
          />
        ) : (
          <Inpuut
            title={undefined}
            autoFocus={true}
            // placeholder="Edit Title"
            doneText="Save"
            value={_boardTitle}
            onChangeText={setBoardTitle}
            onSavePress={async () => {
              await Backend.scoreboardHandler.updateScoreboard(
                scoreboard.title,
                {
                  title: _boardTitle,
                }
              );
              showEditTitle(false);
              Toasty({ text1: "Title updated!" });
            }}
            isUrl={false}
          />
        )}
        {!_showEditSubtitle ? (
          <Kitten.MenuItem
            style={{ backgroundColor: C.surface }}
            title="Subtitle"
            accessoryRight={(_props) => (
              <IconPrimr name="edit" color="white" size={moderateScale(15)} />
            )}
            onPress={() => {
              showEditTitle(false);
              showEditSubtitle(true);
            }}
          />
        ) : (
          <Inpuut
            title={undefined}
            autoFocus={true}
            doneText="Save"
            value={_boardSubtitle}
            onChangeText={setBoardSubtitle}
            onSavePress={async () => {
              await Backend.scoreboardHandler.updateScoreboard(
                scoreboard.title,
                {
                  subtitle: _boardSubtitle,
                }
              );
              showEditSubtitle(false);
              Toasty({ text1: "Subtitle updated!" });
            }}
            isUrl={false}
          />
        )}
      </Kitten.MenuGroup>
      <Kitten.MenuGroup title="Home Screen Layout">
        <Kitten.MenuItem
          style={{ backgroundColor: C.surface }}
          title="Move Left"
          accessoryRight={() => (
            <IconPrimr
              name="arrow_left"
              color="white"
              size={moderateScale(15)}
            />
          )}
          onPress={() => {
            moveScoreboardLeft();
          }}
        />
        <Kitten.MenuItem
          style={{ backgroundColor: C.surface }}
          title="Move Right"
          accessoryRight={() => (
            <IconPrimr
              name="arrow_right"
              color="white"
              size={moderateScale(15)}
            />
          )}
          onPress={() => {
            moveScoreboardRight();
          }}
        />
      </Kitten.MenuGroup>
      <Buttoon
        status="danger"
        appearance="ghost"
        icon={{ name: "trash" }}
        onPress={deleteScoreboard}
      >
        Delete Scoreboard
      </Buttoon>
    </View>
  );
}
