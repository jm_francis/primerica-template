import { Avatar, Input, List, ListItem } from "@ui-kitten/components";
import Backend, { UserSchema } from "backend/";
import { dScoreboard } from "backend/ScoreboardHandler/scoreboard-handler.props";
import { Buttoon, IconPrimr, Inpuut, Toasty } from "components/";
import React from "react";
import { Dimensions, LayoutAnimation, View } from "react-native";
import { C, moderateScale, scale, spacing } from "utilities/";

export function $_AddUsersScores(props: {
  scoreboard: dScoreboard;
  onCancel: () => void;
}) {
  const { onCancel } = props;

  const [_editingUser, setEditingUser] = React.useState<UserSchema | string>(
    null
  );
  const [_users, setUsers] = React.useState<UserSchema[]>([]);
  const [_searchValue, setSearchValue] = React.useState("");

  React.useEffect(function fetchUsers() {
    Backend.adminHandler.users((users) => setUsers(users));
  }, []);

  const potentialUsers: UserSchema[] = _users
    .filter(
      (u) =>
        _searchValue.length > 0 &&
        u.name &&
        u.name.length > 0 &&
        u.name.toLowerCase().includes(_searchValue.toLowerCase())
    )
    .filter((u, idx) => idx < 3);

  // if (
  //   potentialUsers.map((u: UserSchema) =>
  //     _searchValue.toLowerCase().includes(u.name.toLowerCase())
  //   ).length === 0
  // ) {
  //   potentialUsers.push(_searchValue);
  // }

  return (
    <View
      style={{
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: "transparent",
        alignItems: "center",
      }}
    >
      {_editingUser ? (
        typeof _editingUser !== "string" ? (
          <UserItem
            user={_editingUser}
            editing
            onCancel={onCancel}
            {...props}
          />
        ) : (
          <UserItem
            customValue={_searchValue}
            editing
            onCancel={onCancel}
            {...props}
          />
        )
      ) : (
        <>
          <Input
            value={_searchValue}
            onChangeText={(text) => setSearchValue(text)}
            autoFocus={true}
            autoCorrect={false}
            autoCapitalize="words"
            style={{
              borderColor: "transparent",
              width: "97%",
              borderWidth: 0,
              backgroundColor: C.surface01,
            }}
            placeholder="Search users..."
          />
          <List
            style={{
              width: "100%",
            }}
            data={potentialUsers}
            renderItem={(data) => {
              const user: UserSchema = data.item;
              return (
                <UserItem
                  {...props}
                  user={user}
                  onPress={(user: UserSchema) => {
                    LayoutAnimation.configureNext(
                      LayoutAnimation.Presets.easeInEaseOut
                    );
                    setEditingUser(user);
                  }}
                />
              );
            }}
          />
          {_searchValue.length > 0 &&
          potentialUsers.filter((u: UserSchema) =>
            _searchValue.toLowerCase().includes(u.name.toLowerCase())
          ).length === 0 ? (
            <UserItem
              customValue={_searchValue}
              onPress={(str) => {
                setEditingUser(str);
              }}
              onCancel={onCancel}
              {...props}
            />
          ) : null}
        </>
      )}
      <Buttoon onPress={onCancel} appearance="ghost" status="danger">
        Cancel
      </Buttoon>
    </View>
  );
}

/**
 * ### UserItem
 * - TIP: you must either provide user OR customValue, where searchValue is for a custom value
 *
 * @author jm_francis
 * @version 21.2.22
 *
 */
function UserItem(p: {
  scoreboard: dScoreboard;
  user?: UserSchema;
  /**
   * Use a custom string value instead of a specific user to represent the score
   */
  customValue?: string;
  onPress?: (value: UserSchema | string) => void;
  onCancel?: () => void;
  editing?: boolean;
}) {
  const { user, customValue, onPress, onCancel, editing, scoreboard } = p;

  const [_editValue, setEditValue] = React.useState("");

  return (
    <>
      <ListItem
        style={{ width: "100%" }}
        title={customValue ? customValue : user.name}
        disabled={editing}
        onPress={() =>
          onPress && customValue ? onPress(customValue) : onPress(user)
        }
        accessoryLeft={() =>
          !customValue && user.profileImage && user.profileImage.length > 0 ? (
            <Avatar source={{ uri: user.profileImage }} size="small" />
          ) : (
            <IconPrimr name={`profile`} size={scale(30)} color={C.pitchWhite} />
          )
        }
        accessoryRight={() =>
          !editing ? (
            <IconPrimr name="plus" size={moderateScale(17)} color="white" />
          ) : null
        }
      />
      {editing ? (
        <View
          style={{
            position: "absolute",
            right: 0,
            top: spacing(3) - 1.5,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginRight: spacing(2),
          }}
        >
          <Inpuut
            alwaysShowSave
            value={_editValue}
            onChangeText={(text) => setEditValue(text)}
            onSavePress={async () => {
              onCancel();
              if (user) {
                await Backend.scoreboardHandler.setUsersScore(
                  scoreboard,
                  user.uid,
                  Number.parseFloat(_editValue)
                );
              } else if (customValue) {
                await Backend.scoreboardHandler.setNonuserScore(
                  scoreboard,
                  customValue,
                  Number.parseFloat(_editValue)
                );
              }
              Toasty({ text1: "Score added." });
            }}
            doneText="Save"
            autoFocus={true}
            keyboardType="numeric"
            style={{
              width: undefined,
              minWidth: 120,
              borderColor: "transparent",
              // backgroundColor: C.surface01,
            }}
            placeholder="0"
          />
          {/* <IconPrimr name="x" color={C.errorRed} onPress={onCancel} /> */}
        </View>
      ) : null}
    </>
  );
}
