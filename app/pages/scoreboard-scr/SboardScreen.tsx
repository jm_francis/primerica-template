//#region [IMPORT]
import { TopNavigation } from "@ui-kitten/components";
import Backend from "backend/";
import { dScoreboard } from "backend/ScoreboardHandler/scoreboard-handler.props";
import { Buttoon, Kitten, sstyled, Txt } from "components/";
import React from "react";
import { ActivityIndicator, LayoutAnimation, View } from "react-native";
import { NavigationStackScreenProps } from "react-navigation-stack";
import { C, moderateScale, spacing } from "utilities/";
import { ScoreItem } from "./c-score-item";
import { $_AddUsersScores } from "./s-add-user-scores";
import { $_SboardActions } from "./s-sboard-actions";
//#endregion

/**
 * ### A scoreboard screen
 * -  Display specific scoreboard (using its _sid)
 * -  Add Ur score
 * -  Edit member's score (if u r admin)
 *
 * ----
 * @example As is
 * ----
 * @version 1.1.27
 * -  *Add fool-proof try-catch*
 * @author nguyenkhooi
 */
export function SboardPage(props: P) {
  const { navigation } = props;
  const _sbitem = React.useMemo(() => navigation.getParam("item"), []);

  const [_scoreboard, setScoreboard] = React.useState<dScoreboard>(null);
  const [_isAddingScore, setIsAddingScore] = React.useState(false);

  React.useEffect(function fetchScoreboard() {
    Backend.scoreboardHandler.scoreboards((scoreboards) =>
      setScoreboard(scoreboards.filter((sb) => sb.id === _sbitem.id)[0])
    );
  }, []);

  // const [isReady, shouldReady] = useState(false);

  try {
    //#region [SECTION ]  FOOL-PROOF
    // if (isReady == false) throw Error("NOT_READY");

    //#endregion
    return (
      <>
        <TopNavigation
          // title={_scoreboard.title}
          // subtitle={_scoreboard.subtitle}
          alignment="center"
          style={{
            backgroundColor: C.background01,
            marginBottom: spacing(4),
          }}
          accessoryLeft={() => (
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              onPress={() => {
                navigation.pop();
              }}
              icon={{
                name: "arrow_left",
                color: "white",
                size: moderateScale(20),
              }}
            />
          )}
        />
        <SS.Sctnr
          keyboardShouldPersistTaps="always"
          data={_scoreboard.people}
          keyExtractor={(i) => i.uid}
          renderItem={({ item, index }) => (
            <ScoreItem
              scoreItem={item}
              index={index}
              scoreboard={_scoreboard}
            />
          )}
          ListHeaderComponent={() => (
            <View
              style={{
                marginBottom: spacing(5),
                paddingHorizontal: spacing(5),
              }}
            >
              <SS.TxtTitle>{_scoreboard.title}</SS.TxtTitle>
              <SS.TxtSubtitle>{_scoreboard.subtitle}</SS.TxtSubtitle>
              {_isAddingScore ? null : (
                <$_SboardActions
                  scoreboard={_scoreboard}
                  addAScorePress={() => {
                    LayoutAnimation.configureNext(
                      LayoutAnimation.Presets.easeInEaseOut
                    );
                    setIsAddingScore(true);
                  }}
                  {...props}
                />
              )}
              {_isAddingScore ? (
                <$_AddUsersScores
                  scoreboard={_scoreboard}
                  onCancel={() => {
                    LayoutAnimation.configureNext(
                      LayoutAnimation.Presets.easeInEaseOut
                    );
                    setIsAddingScore(false);
                  }}
                  {...props}
                />
              ) : null}
              <View
                style={{
                  marginTop: spacing(4),
                }}
              >
                {Backend.firestoreHandler._account?.admin === true &&
                _scoreboard.people.length > 0 ? (
                  <Txt.Indicator>
                    Tip: Tap on member's score to update it
                  </Txt.Indicator>
                ) : null}
              </View>
            </View>
          )}
          ListEmptyComponent={() => (
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                height: "100%",
              }}
            >
              <Txt.Indicator>Scoreboard empty 😢</Txt.Indicator>
            </View>
          )}
          ListFooterComponent={() => (
            <View
              style={{
                height: 100,
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              {/* <Txt.Indicator>You've reached the end 😃</Txt.Indicator> */}
            </View>
          )}
        ></SS.Sctnr>
      </>
    );
  } catch (error) {
    switch (error.message) {
      case "NOT_READY":
        return (
          <SS.CtnrIdle>
            <ActivityIndicator size="large" color={C.primary} />
          </SS.CtnrIdle>
        );
      default:
        return <></>;
    }
  }
}

const SSS = {
  MedalCtnr: sstyled(View)((p) => ({
    // width: p.style.width,
    // height: p.style.height,
    // backgroundColor: C.hazardYellow,
    // justifyContent: "center",
    // alignItems: "center",
    // borderRadius: 100,
  })),
  TxtMedal: sstyled(Txt)((p) => ({
    fontSize: p.style.width * 0.6,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: C.hazardYellow,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 100,
  })),
};

const SS = {
  Sctnr: sstyled(Kitten.List)((p) => ({
    flex: 1,
    // width: "100%",
    backgroundColor: p.C.background01,
  })),
  CtnrIdle: sstyled(View)((p) => ({
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: p.C.background,
  })),
  CtnrWelcome: sstyled(View)(() => ({
    paddingVertical: spacing(4),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
  })),
  TxtTitle: sstyled(Txt.H6)({
    textAlign: "center",
    fontWeight: "bold",
    marginBottom: spacing(3),
  }),
  TxtSubtitle: sstyled(Txt.P1)((p) => ({
    textAlign: "center",
    marginBottom: 16,
    fontWeight: "bold",
    color: p.C.dim,
  })),
  CtnrUrScore: sstyled(View)((p) => ({
    flexDirection: "row",
    flex: 1,
    // backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
    // position: "absolute",
    // bottom: p.safe.bottom,
    // left: 0,
    width: "60%",
  })),
};

/**
 * ###  Screen Props
 */
interface P extends NavigationStackScreenProps<{ item: dScoreboard }> {}
{
}
