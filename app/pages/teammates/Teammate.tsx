import React, { Component } from "react";
import {
  Buttoon,
  dAccessory,
  ICON_NAME,
  IconPrimr,
  Kitten,
  Txt,
} from "components/";
import { C, getStatusBarHeight, moderateScale, spacing } from "utilities/";
import { Dimensions, ScrollView, Text, View } from "react-native";
import Backend from "backend/";
import { Avatar } from "@ui-kitten/components";
import { scale } from "react-native-size-matters";
import UserLevels from "pages/admin/UserLevels";
import SomeoneElse from "pages/listBuilder/screens/SomeoneElse";

export default class Teammate extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <$_Header {...this.props} />
        <ScrollView style={Styles.container}>
          <$_ProfileInfo {...this.props} />
          <$_Actions {...this.props} />
        </ScrollView>
      </View>
    );
  }
}

export function $_Header(props) {
  const { navigation } = props;

  // const MenuIcon = (name: ICON_NAME, props: dAccessory) => (
  //   <IconPrimr
  //     name={name}
  //     size={18}
  //     color={C.text01}
  //     onPress={() => navigation.goBack()}
  //   />
  // );

  return (
    <>
      <Kitten.TopNavigation
        style={{
          // paddingTop: getStatusBarHeight("safe"),
          backgroundColor: C.background01,
        }}
        alignment="center"
        accessoryLeft={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              navigation.pop();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          >
            Back
          </Buttoon>
        )}
      />
    </>
  );
}

function $_ProfileInfo(props) {
  const { navigation } = props;
  const user = navigation.getParam("user");

  const dimension = screenWidth * 0.6;

  return (
    <View
      style={{
        marginVertical: spacing(5),
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {user.profileImage ? (
        <Avatar
          source={{ uri: user.profileImage }}
          style={{ width: dimension, height: dimension, alignSelf: "center" }}
        />
      ) : (
        <IconPrimr
          name={`profile`}
          preset="default"
          size={dimension * 0.92}
          color={C.pitchWhite}
        />
      )}
      <Text style={Styles.name}>{user.name ? user.name : user.email}</Text>
      <Text style={Styles.info}>
        {user.name ? user.email : "User has not entered their name yet."}
      </Text>
      {user.phoneNumber?.length > 0 ? (
        <Text style={Styles.info}>{user.phoneNumber}</Text>
      ) : null}
      <Text style={Styles.info}>
        {`Joined App ${user.createdAt?.toDate().toLocaleDateString()}`}
      </Text>
    </View>
  );
}

function isContactAccessAllowed(user) {
  try {
    const _uid = Backend.firestoreHandler.uid;
    const isAllowed = !user.listBuilder?.shareTo
      ? false
      : user.listBuilder.shareTo?.includes(_uid);
    return isAllowed;
  } catch (error) {
    console.warn("err >>>", error);
  }
}

function $_Actions(props) {
  const { navigation } = props;
  const user = navigation.getParam("user");

  return (
    <View
      style={{
        alignItems: "center",
        paddingHorizontal: spacing(4),
      }}
    >
      <UserLevels user={user} />
      <Buttoon
        appearance="primary"
        icon={{ name: "contacts" }}
        disabled={!isContactAccessAllowed(user)}
        onPress={() => {
          Backend.listBuilderHandler.connectToOutsiderAccount(user.uid);
          navigation.navigate("ListBuilderModal", {
            content: <SomeoneElse navigation={navigation} />,
            title: user.name,
          });
        }}
        style={{ marginVertical: spacing(3), width: "100%" }}
      >
        View Contact Lists
      </Buttoon>
      {!isContactAccessAllowed(user) ? (
        <Txt
          category="p2"
          style={{ textAlign: "center", marginTop: spacing(1) }}
        >
          This user must give you permission to view their contact lists first.
        </Txt>
      ) : null}
    </View>
  );
}

const screenWidth = Dimensions.get("window").width;

const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background,
  },
  name: {
    fontSize: scale(24),
    color: C.pitchWhite,
    marginTop: 13,
    fontWeight: "bold",
  },
  info: {
    fontSize: scale(12),
    color: C.pitchWhite,
    marginTop: spacing(1),
  },
};
