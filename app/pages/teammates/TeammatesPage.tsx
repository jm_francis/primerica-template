import Backend from "backend/";
import {
  Buttoon,
  dAccessory,
  ICON_NAME,
  IconPrimr,
  Kitten,
  Txt,
  UserListItem,
} from "components/";
import { SHEET_ContactSharing } from "pages/listBuilder/screens/contacts-sharing/contact-sharing";
import React, { Component } from "react";
import { Dimensions, ScrollView, View } from "react-native";
import { C, getStatusBarHeight, moderateScale, spacing } from "utilities/";

export default class TeammatesPage extends Component {
  contactSharingRef = null;

  /**
   * User taps "Share My Account" at the top
   */
  addSomeonePress() {
    this.contactSharingRef.current.open();
  }

  render() {
    this.contactSharingRef = React.createRef();

    return (
      <View style={{ flex: 1 }}>
        <$_Header {...this.props} />
        <ScrollView style={Styles.container}>
          <AddSomeoneItem
            {...this.props}
            onPress={this.addSomeonePress.bind(this)}
          />
          <$_Teammates {...this.props} />
        </ScrollView>
        <SHEET_ContactSharing {...this.props} onRef={this.contactSharingRef} />
      </View>
    );
  }
}

function AddSomeoneItem(props) {
  const { onPress } = props;
  return (
    <Buttoon
      onPress={onPress}
      appearance="ghost"
      icon={{ name: "share" }}
      size="giant"
    >
      Share my account
    </Buttoon>
  );
}

export function $_Header(props) {
  const { navigation } = props;

  const MenuIcon = (name: ICON_NAME, props: dAccessory) => (
    <IconPrimr
      name={name}
      size={18}
      color={C.text01}
      onPress={() => navigation.goBack()}
    />
  );

  return (
    <>
      <Kitten.TopNavigation
        style={{
          backgroundColor: C.background01,
        }}
        title={(_props) => (
          <Kitten.Text
            {..._props}
            style={[
              _props.style,
              {
                marginTop: getStatusBarHeight("safe") - spacing(2),
              },
            ]}
          >
            Teammates
          </Kitten.Text>
        )}
        alignment="center"
        accessoryLeft={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              navigation.pop();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          />
        )}
      />
    </>
  );
}

function $_Teammates(props) {
  const { navigation } = props;
  const [_teammates, setTeammates] = React.useState([]);

  React.useEffect(function fetchTeammates() {
    Backend.adminHandler.users((_users) => {
      const users = [];
      // console.log(users);
      for (let u in _users) {
        const user = _users[u];
        if (
          user.listBuilder?.shareTo &&
          user.listBuilder?.shareTo?.includes(
            Backend.firestoreHandler._account.uid
          )
        ) {
          users.push(user);
        }
      }
      // console.log(users);
      setTeammates(users);
    });
  }, []);

  return (
    <>
      {false || _teammates.length > 0 ? (
        _teammates.map((user) => (
          <UserListItem
            showLevelProgress
            user={user}
            onPress={(_user) => navigation.navigate("Teammate", { user })}
          />
        ))
      ) : (
        <Txt
          category="s2"
          style={{
            marginTop: screenHeight * 0.29,
            textAlign: "center",
            width: "89%",
            alignSelf: "center",
          }}
        >
          Teammates who share their account with you will appear here!
        </Txt>
      )}
    </>
  );
}

const screenHeight = Dimensions.get("window").height;

const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background,
  },
};
