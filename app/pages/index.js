
export * from "./discussion-scr/DiscussionPage";
export * from "./home/HomePage";
export * from "./media/MediaPage";
export * from "./notifications/NotificationPage";
export * from "./permissions/PermissionPage";
export * from "./posst-creator/PosstCreatorPage";
export * from "./scoreboard-old/ScoreboardPage";
export * from "./scoreboard-scr/SboardScreen";
export { default as TeamPage } from "./team/TeamPage";
export { default as ToolsPage } from "./tools/ToolsPage";
export { default as UserPage } from "./users/UserPage";


