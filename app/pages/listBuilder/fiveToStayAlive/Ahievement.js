import React, { Component } from "react";
import { Image, View } from "react-native";
import { DropDown, Loading } from "components";
import { verticalScale } from "utilities/";

const config = {
  red: "rgb(237, 40, 56)",
  blue: "#1769ed"
};

export default class Ahievement extends Component {
  render() {
    const { done, achievement = {} } = this.props;

    const {
      prompt = "Congratulations! Way to kick butt rockstar!",
      imageUrl,
      image
    } = achievement;

    const _img = image
      ? { uri: image }
      : imageUrl && ("" + imageUrl).length > 1
      ? { uri: imageUrl }
      : null;

    const props = { ...this.props };
    if (props.done) delete props.done;

    return (
      <DropDown
        title={prompt}
        {...props}
        externalDone={done}
        yOffset={-100}
        titleStyle={{
          ...this.props.titleStyle,
          marginHorizontal: 20,
          fontWeight: "500",
          marginTop: -15,
          color: config.red
        }}
        titleProps={{
          numberOfLines: 3
        }}
        style={{
          zIndex: 100,
          overflow: "hidden",
          paddingHorizontal: 0,
          paddingBottom: 0,
          backgroundColor: "rgb(15,15,15)",
          ...this.props.style
        }}
      >
        <View style={{ position: "absolute", bottom: "15%" }}>
          <Loading style={Styles.loading} />
        </View>
        <View style={{ width: "100%", height: "70%", marginBottom: -10 }}>
          {_img ? (
            <Image style={Styles.image} resizeMode="contain" source={_img} />
          ) : null}
        </View>
      </DropDown>
    );
  }
}

const Styles = {
  image: {
    width: "105%",
    marginLeft: "-2.5%",
    height: "100%"
  },
  loading: {
    width: verticalScale(90),
    height: verticalScale(90)
  }
};
