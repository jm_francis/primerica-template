import Backend from "backend/";
import { Fonts } from "constant-deprecated";
import {
  BRONZE_MEDAL,

  CRYING_FACE, GOLD_MEDAL, LIGHTNING, MID_FACE,
  PLUS_ICON, SAD_FACE, SILVER_MEDAL
} from "constant-deprecated/Images";
import { defaultLayoutAnimation } from "pages/listBuilder/configure";
import React, { Component } from "react";
import {
  Dimensions, Image,


  LayoutAnimation,
  Platform, Text,



  TouchableOpacity,
  UIManager, View
} from "react-native";
import { QuickStyle } from "react-native-shortcuts";
import { verticalScale } from "utilities/";
import Achievement from "./Ahievement";

const { scoreboardHandler } = Backend;

export default class FiveToStayAlive extends Component {
  state = {
    count: 0,
    showAchievement: false,
    achievement: {},
    streak: 0
  };

  achievementDone() {
    this.setState({ showAchievement: false });
  }

  addPress() {
    if (this.state.count === 5) return;
    scoreboardHandler.addFiveToStayAlive();
    if (this.state.count + 1 === 5)
      setTimeout(() => {
        this.setState({ showAchievement: true });
      }, 450);
  }

  animateIfNeeded(count) {
    if (this.state.count === count) return;
    LayoutAnimation.configureNext(defaultLayoutAnimation);
  }

  fiveToStayAlive(data) {
    const streak = scoreboardHandler.fiveToStayAliveStreak();
    const count = scoreboardHandler.extractFiveToStayAliveCount();
    if (this.state.count !== count) {
      this.animateIfNeeded(count, streak);
      if (count < 5) this.setState({ count, streak });
      else {
        scoreboardHandler.randomFiveToStayAliveAchievement(achievement => {
          this.animateIfNeeded(count, streak);
          this.setState({ count, achievement, streak });
        });
      }
    } else this.setState({ streak });
  }
  fiveToStayAliveAchievement(achievement) {
    this.setState({ achievement });
  }
  componentDidMount() {
    if (Platform.OS === "android")
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);

    scoreboardHandler.fiveToStayAlive(this.fiveToStayAlive.bind(this));
  }

  render() {
    const { count, showAchievement, achievement, streak } = this.state;
    const backgroundColor = generateBackgroundColor(count);
    return [
      <View key="mainview" style={Styles.wrapper}>
        <View style={{ ...Styles.container, ...{ backgroundColor } }}>
          <View style={Styles.row}>
            <Count count={count} />
            {count === 0 ? (
              <Image source={CRYING_FACE} style={Styles.medalImage} />
            ) : null}
            {count === 1 ? (
              <Image source={SAD_FACE} style={Styles.medalImage} />
            ) : null}
            {count === 2 ? (
              <Image source={MID_FACE} style={Styles.medalImage} />
            ) : null}
            {count === 3 ? (
              <Image source={BRONZE_MEDAL} style={Styles.medalImage} />
            ) : null}
            {count === 4 ? (
              <Image source={SILVER_MEDAL} style={Styles.medalImage} />
            ) : null}
            {count >= 5 ? (
              <Image source={GOLD_MEDAL} style={Styles.medalImage} />
            ) : null}
          </View>
        </View>

        <View style={Styles.buttonLayout}>
          {streak > 0 ? (
            <Streak
              streak={streak}
              half={count < 5}
              backgroundColor={backgroundColor}
            />
          ) : null}
          {count < 5 ? (
            <Button
              onPress={this.addPress.bind(this)}
              half={streak > 0}
              backgroundColor={backgroundColor}
            />
          ) : null}
        </View>
      </View>,
      <Achievement
        key="ahievement"
        active={showAchievement}
        done={this.achievementDone.bind(this)}
        achievement={achievement}
      />
    ];
  }
}

class Count extends Component {
  render() {
    const { count } = this.props;
    //'#ff1616'
    // const color = count < 3? dyingTextColor : thrivingTextColor
    return (
      <View style={Styles.countContainer}>
        <Text
          allowFontScaling={false}
          style={{ ...Styles.countText, color: "white" }}
        >
          {`${count}/5`}
        </Text>
        <Text
          allowFontScaling={false}
          style={{
            ...Styles.countText,
            fontSize: verticalScale(20.5),
            marginLeft: 15,
            color: "white"
          }}
        >
          {count < 5 ? "to stay alive" : "day complete!"}
        </Text>
      </View>
    );
  }
}
class Button extends Component {
  render() {
    const half = this.props.half ? { width: screenWidth * 0.47 } : {};
    const backgroundColor = { backgroundColor: this.props.backgroundColor };
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View
          style={{ ...Styles.buttonContainer, ...half, ...backgroundColor }}
        >
          <Image source={PLUS_ICON} style={Styles.buttonImage} />
        </View>
      </TouchableOpacity>
    );
  }
}
class Streak extends Component {
  render() {
    const { streak, backgroundColor } = this.props;
    const half = this.props.half ? { width: screenWidth * 0.47 } : {};
    return (
      <View
        style={{
          ...Styles.buttonContainer,
          flexDirection: "row",
          justifyContent: "space-between",
          paddingHorizontal: 30,
          backgroundColor,
          ...half
        }}
      >
        <Text
          style={{
            ...Styles.streakText,
            fontWeight: "600",
            fontSize: verticalScale(21),
            marginLeft: -15
          }}
        >
          streak
        </Text>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text style={Styles.streakText}>{streak}</Text>
          <Image source={LIGHTNING} style={Styles.streakIcon} />
        </View>
      </View>
    );
  }
}

const screenWidth = Dimensions.get("window").width;
const generateBackgroundColor = count => {
  if (count >= 5) return "rgb(245, 214, 78)"; // gold
  if (count >= 2) return "rgb(39, 214, 91)"; // green
  return "rgb(235, 47, 69)"; // red
};

const Styles = {
  wrapper: {
    width: "95%",
    alignSelf: "center"
  },
  container: {
    marginTop: 17,
    paddingHorizontal: 15,
    paddingVertical: 12,
    width: "100%",
    borderRadius: 7,
    // backgroundColor: 'rgb(37, 50, 232)',
    // backgroundColor: red,
    ...QuickStyle.boxShadow
  },
  row: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  countContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  countText: {
    ...QuickStyle.boxShadow,
    color: "white",
    fontFamily: Fonts.bold,
    fontSize: verticalScale(32)
  },
  medalImage: {
    width: verticalScale(54),
    height: verticalScale(54)
  },
  buttonLayout: {
    marginTop: 4,
    width: "100%",
    justifyContent: "space-evenly",
    flexDirection: "row"
  },
  buttonContainer: {
    width: screenWidth * 0.95,
    height: verticalScale(34),
    borderRadius: 7,
    marginHorizontal: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    color: "white",
    fontSize: verticalScale(24)
  },
  buttonImage: {
    tintColor: "white",
    width: verticalScale(25),
    height: verticalScale(25)
  },
  streakIcon: {
    tintColor: "white",
    width: verticalScale(23),
    height: verticalScale(23),
    marginLeft: -1
  },
  streakText: {
    color: "white",
    fontFamily: Fonts.medium,
    fontWeight: "700",
    fontSize: verticalScale(20.3),
    marginTop: 2
  }
};
