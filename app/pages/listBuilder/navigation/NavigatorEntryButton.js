import Backend from "backend/";
import { Fonts } from "constant-deprecated";
import { PROFILE } from "constant-deprecated/Images";
import React from "react";
import {
  Alert,
  Image,
  Platform, Text,



  TouchableOpacity, View
} from "react-native";
import { moderateScale } from "utilities/";


// TODO: check permissions

export class NavigatorEntryButton extends React.Component {
  press() {
    const lbConfig = Backend.listBuilderHandler._config;
    if (Platform.OS === "android") {
      if (
        lbConfig &&
        lbConfig.disable &&
        lbConfig.disable.includes("android")
      ) {
        Alert.alert("This feature is temporarily disabled.");
        return;
      }
      // PermissionChecker.androidContacts().then(() => {
      // TODO check permissions
      this.props.navigation.navigate("ListBuilder");
      // })
    } else this.props.navigation.navigate("ListBuilder");
  }

  render() {
    const { title = "Contact Manager" } = this.props;
    const half =
      this.props.half === true && Platform.isPad ? { width: "48%" } : {};
    return (
      <View style={{ ...Styles.container, ...half, ...this.props.style }}>
        <TouchableOpacity
          style={Styles.touchable}
          onPress={this.press.bind(this)}
        >
          <View style={Styles.innerContainer}>
            <Image source={PROFILE} style={Styles.icon} />
            <Text allowFontScaling={false} style={Styles.title}>
              {title}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const Styles = {
  container: {
    marginTop: 14,
    width: "95%",
    height: moderateScale(70, 0.25),
    borderRadius: 13,
    overflow: "hidden"
  },
  innerContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: "10%",
    alignItems: "center",
    backgroundColor: "rgb(56, 67, 234)"
  },
  touchable: {
    width: "100%",
    height: "100%"
  },
  icon: {
    marginLeft: Platform.isPad ? -20 : -5,
    tintColor: "white",
    width: moderateScale(38.4),
    height: moderateScale(38.4)
  },
  title: {
    fontSize: moderateScale(25, 0.27),
    color: "rgb(237,237,237)",
    fontFamily: Fonts.medium,
    fontWeight: "600"
  }
};
