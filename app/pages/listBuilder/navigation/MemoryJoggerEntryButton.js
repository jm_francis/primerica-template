import Backend from "backend/";
import { Fonts } from "constant-deprecated";
import { MEMORY_JOGGER } from "constant-deprecated/Images";
import React from "react";
import { Image, Platform, Text, TouchableOpacity, View } from "react-native";
import { moderateScale } from "utilities/";


// TODO check permissions

export class MemoryJoggerEntryButton extends React.Component {
  _press() {
    this.props.navigation.navigate("MemoryJogger");
  }
  press() {
    const memoryJoggerTitle =
      Backend.listBuilderHandler._config.memoryJogger &&
      Backend.listBuilderHandler._config.memoryJogger.title
        ? Backend.listBuilderHandler._config.memoryJogger.title
        : "Build My List";
    // user already has an account
    if (Backend.firestoreHandler._account) {
      const mylists = Backend.firestoreHandler._account.listBuilder.lists;
      for (l in mylists) {
        const list = mylists[l];
        if (list.title === memoryJoggerTitle) {
          // user has done the game already
          this.props.navigation.navigate("ListScreen", {
            listData: list /*popCount: 2*/
          });
          return;
        }
      }
      // user has not gone through the game yet
      this.props.navigation.navigate("MemoryJogger");
    }
  }

  render() {
    const { title = "Build My List" } = this.props;
    return (
      <View style={Styles.container}>
        <TouchableOpacity
          style={Styles.touchable}
          onPress={this.press.bind(this)}
        >
          <View style={Styles.innerContainer}>
            <Image source={MEMORY_JOGGER} style={Styles.icon} />
            <Text style={Styles.title}>{title}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const Styles = {
  container: {
    marginTop: Platform.isPad ? 27 : 16,
    width: Platform.isPad ? "61%" : "95%",
    height: moderateScale(70, 0.4),
    borderRadius: 13,
    overflow: "hidden"
  },
  innerContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: "10%",
    alignItems: "center",
    backgroundColor: "rgb(216, 14, 14)"
  },
  touchable: {
    width: "100%",
    height: "100%"
  },
  icon: {
    width: moderateScale(30),
    height: moderateScale(30),
    tintColor: "white"
  },
  title: {
    fontSize: moderateScale(25, 0.37),
    color: "rgb(237,237,237)",
    fontFamily: Fonts.medium,
    fontWeight: "600"
  }
};
