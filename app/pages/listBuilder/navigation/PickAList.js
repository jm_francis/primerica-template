//@ts-check
import { TopNavigation } from "@ui-kitten/components";
import Backend from "backend/";
import { Buttoon } from "components/";
import BackButton from "navigation/components/BackButton";
import Title from "navigation/components/Title";
import React, { Component } from "react";
import { Platform, ScrollView, View } from "react-native";
import { C, getStatusBarHeight, moderateScale, spacing } from "utilities/";
import { ListHeaderButton } from "../components";
import { shade } from "../configure";

export default class PickAList extends Component {
  static navigationOptions = ({ navigation }) => ({
    // headerTitle: () => (
    //   <Title
    //     style={{
    //       position: "absolute",
    //       bottom: spacing(2),
    //     }}
    //   >
    //     {"Choose a List"}
    //   </Title>
    // ),
    // headerStyle: {
    //   backgroundColor: "#1239c7",
    //   ...(Platform.isPad
    //     ? { height: 90 }
    //     : {
    //         height:
    //           getStatusBarHeight("safe") + (Platform.OS === "android" ? 27 : 0),
    //       }),
    // },
    // headerLeft: () => (
    //   <BackButton
    //     navigation={navigation}
    //     style={{
    //       marginTop: spacing(1) + 7,
    //       marginLeft: spacing(2),
    //     }}
    //   />
    // ),
  });

  state = {
    lists: [],
  };

  componentDidMount() {
    Backend.firestoreHandler.account(this.myAccount.bind(this));
  }
  myAccount(account) {
    const data = account.listBuilder;
    this.setState({ lists: data.lists });
  }

  listSelected(listData) {
    const onSelect = this.props.navigation.getParam("onSelect", () => {});
    onSelect(listData);
    this.props.navigation.pop();
  }

  render() {
    const lbConfig = Backend.listBuilderHandler._config;
    const { lists } = this.state;

    const children = [];
    for (let l in lists) {
      const listData = lists[l];
      if (listData.title === lbConfig.memoryJogger.title) continue;
      children.push(
        <ListHeaderButton
          listData={listData}
          onPress={this.listSelected.bind(this)}
          key={`pickalist.${listData.title}`}
        />
      );
    }
    return (
      <>
        <TopNavigation
          title={"Choose a List"}
          alignment="center"
          style={{ backgroundColor: C.background01 }}
          accessoryLeft={() => (
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              onPress={() => {
                this.props.navigation.pop();
              }}
              icon={{
                name: "arrow_left",
                color: "white",
                size: moderateScale(20),
              }}
            />
          )}
        />
        <ScrollView style={Styles.container}>
          <View style={{ width: "100%", height: 14 }} />
          {children}
        </ScrollView>
      </>
    );
  }
}

const Styles = {
  container: {
    flex: 1,
    backgroundColor: shade(true, 0.89),
  },
};
