//@ts-check
import { Fonts } from "constant-deprecated/";
import { CLIPBOARD, GROUP_OF_PEOPLE } from "constant-deprecated/Images";
import BackButton from "navigation/components/BackButton";
import Title from "navigation/components/Title";
import React from "react";
import { Image, Platform } from "react-native";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { createStackNavigator } from "react-navigation-stack";
import { C, moderateScale, spacing } from "utilities/";
import { shade } from "../configure";
import ListBuilderListsMenu from "../ListBuilderListsMenu";
import ContactSearch from "../screens/contact_search/ContactSearch";

const darkMode = true;

// ANCHOR Components
class ContactSearchPickOne extends React.Component {
  contactSelected(contact) {
    const onSelect = this.props.navigation.getParam("onSelect");
    if (onSelect) onSelect(contact);
    this.props.navigation.pop();
  }
  render() {
    return (
      <ContactSearch
        pickOneContact={this.contactSelected.bind(this)}
        listTitle={this.props.navigation.getParam("listTitle")}
      />
    );
  }
}

// ANCHOR Contacts: Styles, Options, etc.
export const headerBackTitleStyle = Platform.isPad
  ? {
      fontSize: moderateScale(27),
    }
  : {};
const _headerStyle = {
  backgroundColor: C.background01,
  // backgroundColor: "#1239c7",
  // height: getStatusBarHeight("safe") + (Platform.OS === "android" ? 27 : 0),
};
export const headerStyle = Platform.isPad
  ? { ..._headerStyle, height: 90 }
  : _headerStyle;
export const headerTitleStyle = {
  fontFamily: Fonts.medium,
  fontSize: moderateScale(19.25, 0.3),
};
export const backgroundColor = shade(darkMode, 0.89);
export const cardStyle = {
  backgroundColor,
  opacity: 1,
};
export const ipadImageStyle = Platform.isPad
  ? {
      transform: [{ scale: moderateScale(1, 0.4) }],
    }
  : {};
export const ipadLabelStyle = Platform.isPad
  ? {
      marginTop: -15,
      fontSize: moderateScale(16, 0.15),
      marginTop: 6,
      marginLeft: 33,
    }
  : {};

// ANCHOR: Callback functions for communicating
const functions = [];
export const notify = (id, param) => {
  for (let f in functions) {
    const data = functions[f];
    if (data.id === id) data.callback(param);
  }
};
export const subscribe = (id, callback) => {
  functions.push({ id, callback });
};

const modifiedTitleStyle = {
  // made originally to fix iPhone 12 spacing
  position: "absolute",
  bottom: spacing(2),
};

// ANCHOR Navigators

// NOTE contains your contact lists and the collab screens
const RootScreenNavigator = createStackNavigator(
  {
    ListBuilderListsMenu: {
      screen: ListBuilderListsMenu,
    },
  },
  {
    headerMode: "none",
    cardStyle,
    navigationOptions: ({}) => ({
      title: "My Contacts",
      headerTitle: () => (
        <Title style={modifiedTitleStyle}>{"My Contacts"}</Title>
      ),
    }),
    navigationOptions: ({ navigation }) => ({
      headerLeft: (
        <BackButton
          navigation={navigation}
          style={{
            marginTop: spacing(1) + 7,
            marginLeft: spacing(2),
            tintColor: shade(!darkMode),
          }}
          onPress={() => {
            navigation.pop();
          }}
        />
      ),
    }),
  }
);

const PrimaryNavigator = createStackNavigator(
  {
    Root: RootScreenNavigator,
    // ListScreen, // NOTE where an individual list display (now in app navigator)
    SelectSingleContact: {
      screen: ContactSearchPickOne,
      navigationOptions: ({}) => ({ title: "Contacts" }),
    },
  },
  {
    headerMode: "none",
    defaultNavigationOptions: ({}) => ({
      // headerTitle: () => (
      //   <Title style={modifiedTitleStyle}>{"Contact Manager"}</Title>
      // ),
      // headerTintColor: darkMode ? "white" : shade(!darkMode, 0.8),
      // headerStyle,
      // headerBackTitle: "Back",
      // headerBackTitleStyle,
      // headerTitleStyle,
    }),
    cardStyle,
    transitionConfig: () => ({
      containerStyle: {
        backgroundColor: shade(darkMode, 0.89),
      },
    }),
  }
);

export default createStackNavigator(
  {
    index: PrimaryNavigator,
    // ListBuilderModalOld: createStackNavigator(
    //   { ModalContainer, OutsiderListScreen: ListScreen },
    //   {
    //     defaultNavigationOptions: ({ navigation }) => ({
    //       headerTintColor: "white",
    //       headerStyle,
    //       headerTitleStyle,
    //     }),
    //   }
    // ),
  },
  {
    headerMode: "none",
    cardStyle,
    mode: "modal",
  }
);
