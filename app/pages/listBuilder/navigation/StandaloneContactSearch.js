//@ts-check
import { TopNavigation } from "@ui-kitten/components";
import { Buttoon } from "components/";
import K from "constant-deprecated";
import BackButton from "navigation/components/BackButton";
import Title from "navigation/components/Title";
import ContactSearch from "pages/listBuilder/screens/contact_search/ContactSearch";
import React from "react";
import { Platform } from "react-native";
import { C, getStatusBarHeight, moderateScale, spacing } from "utilities/";

export default class StandaloneContactSearch extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    // headerTitle: () => (
    //   <Title
    //     style={{
    //       position: "absolute",
    //       bottom: spacing(2),
    //     }}
    //   >
    //     Phone Contacts
    //   </Title>
    // ),
    // headerTitleStyle: {
    //   fontFamily: K.Fonts.bold,
    //   fontSize: moderateScale(18.4, 0.37),
    //   color: "white",
    // },
    // headerStyle: {
    //   height: Platform.isPad
    //     ? 90
    //     : getStatusBarHeight("safe") + (Platform.OS === "android" ? 27 : 0),
    //   backgroundColor: navigation.getParam("backgroundColor", "#1239c7"),
    // },
    // headerLeft: () => (
    //   <BackButton
    //     navigation={navigation}
    //     style={{
    //       marginTop: spacing(1) + 7,
    //       marginLeft: spacing(2),
    //     }}
    //   />
    // ),
  });

  render() {
    const { navigation } = this.props;
    const listTitle = navigation.getParam("listTitle");
    return (
      <>
        <TopNavigation
          title={"Phone Contacts"}
          alignment="center"
          style={{ backgroundColor: C.background01 }}
          accessoryLeft={() => (
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              onPress={() => {
                this.props.navigation.pop();
              }}
              icon={{
                name: "arrow_left",
                color: "white",
                size: moderateScale(20),
              }}
            />
          )}
        />
        <ContactSearch
          navigation={navigation}
          listTitle={listTitle}
          standalone
        />
      </>
    );
  }
}
