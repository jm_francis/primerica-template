// @ts-check

//@ts-ignore
import { TopNavigation } from "@ui-kitten/components";
import Backend from "backend/";
import { Buttoon, Kitten, Toasty } from "components/";
import FiveToStayAlive from "pages/listBuilder/fiveToStayAlive/FiveToStayAlive";
import React from "react";
import {
  Alert,
  Keyboard,
  LayoutAnimation,
  ScrollView,
  View,
} from "react-native";
import DialogInput from "react-native-dialog-input";
import { check, PERMISSIONS, request } from "react-native-permissions";
import { C, DEVICE_HEIGHT, IS_ANDROID, moderateScale, scale } from "utilities/";
import { ContactsHandler } from "../../backend/ContactsHandler/ContactsHandler";
import { ListHeaderButton } from "./components";
import { defaultLayoutAnimation, shade } from "./configure";
import { notify, subscribe } from "./navigation/ListBuilderNavigator";
import { SHEET_ContactSharing } from "./screens/contacts-sharing/contact-sharing";

const darkMode = true;

export default class ListBuilderListsMenu extends React.Component {
  static navigationOptions = () => ({
    safeAreaInset: { bottom: "never", top: "never" },
  });

  state = {
    lists: [],
    editMode: false,
    showNewListDialog: false,
  };

  componentDidMount() {
    const { outsider } = this.props;
    if (outsider) {
      Backend.listBuilderHandler.outsiderAccount(this.account.bind(this));
      return;
    }

    Backend.firestoreHandler.account(this.account.bind(this));

    subscribe("listBuilderListsMenuEdit", this.edit.bind(this));
  }

  account(_account) {
    const { outsider } = this.props;
    const account = outsider ? outsider.listBuilder : _account.listBuilder;
    if (!account) {
      this.props.navigation.pop();
      return;
    }
    Keyboard.dismiss();

    const accountLists = []; // {data}
    let addedlists = ""; // NOTE: the following is a bit of a rough workaround so duplicate lists don't pop up
    for (let _a in account.lists) {
      // DUPLICATE FIX LOOP
      const a = account.lists[_a]; // a = a list data
      if (!a.contacts) a.contacts = [];
      let duplicate = false;
      for (let _b in account.lists) {
        const b = account.lists[_b];
        if (b.title === a.title) {
          duplicate = true;
          if (
            b.contacts.length >= a.contacts.length &&
            !addedlists.includes(b.title)
          ) {
            accountLists.push(b);
            addedlists += `${b.title},`;
          }
          // else accountLists.push(a)
          break;
        }
      }
      if (duplicate === false) {
        addedlists += `${a.title},`;
        accountLists.push(a);
      }
    }

    /**
     * listBuilder.lists = [{id: string; title: string; contacts: [{givenName: string; familyName: string; emailAddresses: [{label: string; email: string}]; phoneNumbers: [{label: string; number: number}];  }]},]
     */
    this.setState({ lists: accountLists, account });
  }
  renderLists() {
    const { navigation, outsider } = this.props;
    const { lists, editMode } = this.state;
    const listComponents = [];
    for (let i in lists) {
      // REAL LIST LOOP
      const listData = lists[i];
      listComponents.push(
        <ListHeaderButton
          key={"listHeaderButton." + listData.title}
          outsider={outsider}
          listData={listData}
          navigation={navigation}
          editMode={editMode}
          onDeletePress={this.deletePress.bind(this)}
        />
      );
    }
    return listComponents;
  }

  addNewListPress() {
    this.setState({ showNewListDialog: true });
  }
  addNewList(input) {
    this.setState({ showNewListDialog: false });
    LayoutAnimation.configureNext(defaultLayoutAnimation);
    Backend.listBuilderHandler.createList(input);
  }

  edit() {
    LayoutAnimation.configureNext(defaultLayoutAnimation);
    this.setState({ editMode: !this.state.editMode });
  }
  async deletePress(listData) {
    const deleteIt = async () => {
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      await Backend.listBuilderHandler.deleteList(listData);
    };

    if (listData.contacts && listData.contacts.length > 0) {
      Alert.alert(
        "This list has " + listData.contacts.length + " contacts.",
        "Are you sure you want to delete it?",
        [
          {
            text: "Cancel",
            style: "cancel",
          },
          {
            text: "Yes",
            onPress: deleteIt,
          },
        ]
      );
    } else deleteIt();
  }

  $$contactsSharingRef = React.createRef();

  render() {
    const { showNewListDialog, account } = this.state;
    const { outsider } = this.props;

    return (
      <View style={Styles.container}>
        <TopNavigation
          title={
            outsider
              ? outsider.name && outsider.name.length > 0
                ? outsider.name
                : outsider.email
              : "Contact Manager"
          }
          alignment="center"
          style={{ backgroundColor: C.background01 }}
          accessoryLeft={(p) => (
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
              }}
              onPress={() => {
                this.props.navigation.pop();
              }}
              icon={{
                name: "arrow_left",
                color: "white",
                size: moderateScale(20),
              }}
            />
          )}
        />
        <ScrollView
          style={{ flex: 1 }}
          stickyHeaderIndices={[0]}
          showsVerticalScrollIndicator={false}
        >
          <$_ActionBar
            {...this.props}
            account={account}
            onEditPress={() =>
              this.setState({ editMode: !this.state.editMode })
            }
            onSharingPress={() => this.$$contactsSharingRef.current.open()}
          />
          {outsider ? null : <FiveToStayAlive />}
          {outsider ? null : <View style={Styles.space} />}
          {this.renderLists()}
          {outsider ? null : (
            <ListHeaderButton plus onPress={this.addNewListPress.bind(this)} />
          )}
          <View style={Styles.space} />
          {/* <$_StayAlive {...this.props} /> */}
        </ScrollView>

        <DialogInput
          title="Title Your List:"
          hint=""
          dialogStyle={Styles.dialogStyle}
          isDialogVisible={showNewListDialog}
          submitInput={this.addNewList.bind(this)}
          closeDialog={() => this.setState({ showNewListDialog: false })}
        />
        <SHEET_ContactSharing
          modalTopOffset={scale(65)}
          onRef={this.$$contactsSharingRef}
          {...this.props}
        />
      </View>
    );
  }
}

function $_ActionBar(props) {
  const { onSharingPress, outsider, account } = props;

  let fileName = outsider
    ? Backend.adminHandler.getUserByUid(
        Backend.listBuilderHandler.outsiderHandler._outsiderUid
      ).name
    : Backend.firestoreHandler._account.name;
  fileName += "-contacts";

  /**
   * This functions share `contact.csv` file
   */
  function shareContacts() {
    Alert.alert(
      "",
      "Do you want to save your contacts as CSV?",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: () => {
            try {
              IS_ANDROID
                ? check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then(
                    (result) => {
                      switch (result) {
                        case "granted":
                          ContactsHandler.ContactsExport(
                            [
                              "givenName",
                              "familyName",
                              "emailAddresses",
                              "phoneNumbers",
                              "recordID",
                            ],
                            fileName,
                            true,
                            outsider && account ? account.lists : null
                          );
                          break;
                        default:
                          request(
                            PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE
                          ).then((result) => {
                            switch (result) {
                              case "granted":
                                ContactsHandler.ContactsExport(
                                  [
                                    "givenName",
                                    "familyName",
                                    "emailAddresses",
                                    "phoneNumbers",
                                    "recordID",
                                  ],
                                  fileName,
                                  true,
                                  outsider && account ? account.lists : null
                                );
                                break;
                              default:
                                return Toasty({
                                  type: "error",
                                  text1: "Permission Error",
                                  text2:
                                    "Please allow permission in your settings.",
                                });
                                break;
                            }
                          });
                      }
                    }
                  )
                : ContactsHandler.ContactsExport(
                    [
                      "givenName",
                      "familyName",
                      "emailAddresses",
                      "phoneNumbers",
                      "recordID",
                    ],
                    fileName,
                    true,
                    outsider && account ? account.lists : null
                  );
            } catch (error) {
              console.warn("Err saving as csv: ", error);
              return Toasty({
                type: "error",
                text1: "Error Exporting Contacts",
                text2: "Please try again later.",
              });
            }
          },
        },
      ],
      { cancelable: true }
    );
  }
  const [_isEdit, shouldEdit] = React.useState(false);
  const [_moreShown, shouldShowMore] = React.useState(false);

  return (
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end",
        padding: scale(10),
      }}
    >
      {/* <Text style={{ color: "white" }}>{JSON.stringify(_exportedList)}</Text> */}

      {/* <Buttoon
        icon={{ name: "plus", right: true }}
        appearance="icon"
        style={{
          backgroundColor: C.surface01,
        }}
        onPress={() => {
          Toasty({ text1: "Hey", text2: "Add some functions here" });
        }}
      /> */}
      {outsider ? null : (
        <Buttoon
          icon={{ name: _isEdit ? "check" : "trash", right: true }}
          appearance="icon"
          style={{
            backgroundColor: _isEdit ? C.awakenVolt : C.surface01,
          }}
          // onPress={() => onEditPress()}
          onPress={() => {
            notify("listBuilderListsMenuEdit", !_isEdit);
            shouldEdit(!_isEdit);
          }}
        />
      )}
      <Kitten.OverflowMenu
        visible={_moreShown}
        onBackdropPress={() => shouldShowMore(false)}
        anchor={() => (
          <View>
            <Buttoon
              icon={{ name: "share", right: true }}
              appearance="icon"
              style={{
                backgroundColor: C.surface01,
              }}
              onPress={() => shouldShowMore(true)}
            />
          </View>
        )}
      >
        {outsider ? null : (
          <Kitten.MenuItem
            title="Share contacts"
            onPress={() => {
              onSharingPress();
              shouldShowMore(false);
            }}
          />
        )}
        <Kitten.MenuItem
          title="Save as CSV"
          onPress={() => {
            shareContacts();
            shouldShowMore(false);
          }}
        />
      </Kitten.OverflowMenu>
    </View>
  );
}

const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background01,
    // backgroundColor: shade(darkMode, 0.89),
  },
  space: {
    width: "100%",
    height: 20,
  },
  dialogStyle: {
    marginTop: -DEVICE_HEIGHT * 0.1,
  },
};
