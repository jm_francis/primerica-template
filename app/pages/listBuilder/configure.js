const spring = {
  type: "spring",
  springDamping: 0.65
};
export const defaultLayoutAnimation = {
  duration: 600,
  create: {
    ...spring,
    property: "scaleXY"
  },
  update: {
    ...spring
  },
  delete: {
    ...spring,
    property: "scaleXY"
  }
};

export const shade = (dark, percent = 1) => {
  let darkValue = 255 - 255 * percent;
  darkValue = darkValue < 13 ? 13 : darkValue;
  let whiteValue = percent * 255;
  let val = dark ? darkValue : whiteValue;
  return `rgb(${val},${val},${val})`;
};
