const _callAll = (funcs, value) => {
  for (f in funcs) funcs[f](value);
};

import Backend from "backend/";
import K from "constant-deprecated";

class Ledger {
  _data = [];
  _dataCallbacks = [];

  _initDataIfNeeded() {
    if (this._data.length > 0) return;

    this._data = K.MemoryJoggerConfig.screens;
  }

  async saveToFirestore() {
    const listBuilderHandler = Backend.listBuilderHandler;
    const _contacts = this.allContacts();
    const contacts = [];
    for (c in _contacts) {
      const _c = _contacts[c];
      if (_c.recordID && _c.recordID.includes("tmp:")) _c.recordID = undefined;
      contacts.push(_c);
    }
    let listId = listBuilderHandler._config.memoryJogger.id;
    listId = listId? listId : "Build My List"
    const mergeList = listBuilderHandler.getListById(listId);
    const newList = {
      id: listId,
      title: listBuilderHandler._config.memoryJogger.title,
      ...(mergeList ? mergeList : {}),
      contacts: mergeList
        ? listBuilderHandler.mergeContactArrays(mergeList.contacts, contacts)
        : contacts
    };
    await listBuilderHandler.updateList(
      listBuilderHandler._config.memoryJogger.title,
      newList
    );
    return newList;
  }

  data(callback) {
    this._initDataIfNeeded();
    callback(this._data);
    this._dataCallbacks.push(callback);
  }
  allContacts() {
    let contacts = [];
    for (i in this._data) {
      const screen = this._data[i];
      contacts = contacts.concat(screen.contacts ? screen.contacts : []);
    }
    let records = "";
    const filteredContacts = [];
    for (c in contacts) {
      if (records.includes(contacts[c].recordID)) continue;
      records += contacts[c].recordID + ", ";
      filteredContacts.push(contacts[c]);
    }
    return filteredContacts;
  }

  setContact(options) {
    // updates if exists, adds if does not exist
    const { contact, contactIndex, screenIndex } = options;

    const screen = this._data[screenIndex];
    let didFind = false;
    for (c in screen.contacts) {
      const _contact = screen.contacts[c];
      if (contactIndex === c || _contact.recordID === contact.recordID) {
        // does exist, therefore, update existing
        this._data[screenIndex].contacts[c] = {
          priorities: {},
          ..._contact,
          ...contact
        };
        didFind = true;
        break;
      }
    }

    if (!didFind) {
      // does not yet exist, therefore, add new
      this._data[screenIndex].contacts.push({
        priorities: {},
        ...contact
      });
    }

    _callAll(this._dataCallbacks, this._data);
  }
  checkPriorityForContact(priorityId, contact) {
    for (s in this._data) {
      const screen = this._data[s];
      for (c in screen.contacts) {
        const someContact = screen.contacts[c];
        if (contact.recordID === someContact.recordID) {
          let setto = !(someContact.priorities[priorityId] === true);
          screen.contacts[c].priorities[priorityId] = setto;
        }
      }
    }

    _callAll(this._dataCallbacks, this._data);
  }

  recordID(name) {
    return Backend.listBuilderHandler.recordID(name);
  }

  // ANCHOR: Organization functions
  organizeContactsIntoGroups(contacts) {
    const groups = K.MemoryJoggerConfig.groups.concat([]);

    const doesContactFitGroup = (contact, group) => {
      const groupPriorities = group.requiredPriorities; // array
      let contactPriorities = ""; // string
      if (contact.priorities) {
        for (p in contact.priorities)
          if (contact.priorities[p] === true) contactPriorities += p + ",";
      }
      if (contactPriorities.length === 0 && groupPriorities.length === 0)
        return true; // "everyone else"
      if (contactPriorities.split(",").length - 1 !== groupPriorities.length)
        return false;
      for (g in groupPriorities) {
        const gid = groupPriorities[g];
        if (!contactPriorities.includes(gid)) return false;
      }
      return true;
    };

    const contactGroups = [];
    for (g in groups) {
      const group = groups[g];
      group.contacts = [];
      for (c in contacts) {
        const contact = contacts[c];
        if (doesContactFitGroup(contact, group)) group.contacts.push(contact);
      }
      contactGroups.push(group);
    }

    return contactGroups;
  }
}

export const ledger = new Ledger();
