const spring = {
  type: "spring",
  // springDamping: 0.6
  springDamping: 0.85
};

let shouldCancel = false;
const cancelConfig = {
  duration: 0
};

const makeConfig = data => {
  return () => {
    if (shouldCancel) {
      setTimeout(() => {
        shouldCancel = false;
      }, 50);
      return cancelConfig;
    }
    return data;
  };
};

export default {
  cancelNext: () => {
    shouldCancel = true;
  },
  resumeNext: () => {
    shouldCancel = false;
  },
  default: makeConfig({
    duration: 215,
    create: {
      ...spring,
      property: "scaleXY"
    },
    update: {
      ...spring
    },
    delete: {
      ...spring,
      property: "scaleXY"
    }
  }),
  navigationBar: makeConfig({
    duration: 260,
    update: {
      ...spring,
      property: "scaleXY"
    }
  })
};
