import React from "react";
import { Txt } from "components/";
import { View } from "react-native";
import { SHEET_ContactSharing } from "pages/listBuilder/screens/contacts-sharing/contact-sharing";

interface PropTypes {}

/**
 * ### Prompt the user to give their trainer permission to view their new contact lists
 *
 * @author jm_francis
 * @version 1.1.20
 *
 * @example
 * <ShareList />
 */
export function ShareList(props: PropTypes) {
  const shareSheetRef = React.useRef();

  React.useEffect(function displayShareSheet() {
    shareSheetRef.current.open();
  }, []);

  return (
    <View
      style={{
        flex: 1,
        alignContent: "center",
        justifyContent: "center",
        backgroundColor: "#151b33",
      }}
    >
      <SHEET_ContactSharing
        onRef={shareSheetRef}
        hideXButton
        promptTitle="Select your trainer(s)"
      />
    </View>
  );
}
