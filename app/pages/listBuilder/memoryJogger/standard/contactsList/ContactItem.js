import Backend from "backend/";
import K from "constant-deprecated";
import { PLUS_ICON } from "constant-deprecated/Images";
import AnimationConfigs from "memoryJogger/AnimationConfigs";
import { ledger } from "memoryJogger/Ledger";
import React, { Component } from "react";
import {
  Image,




  LayoutAnimation,
  Platform, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, View
} from "react-native";
import { moderateScale } from "utilities/";


export default class ContactItem extends Component {
  state = {
    active: false,
    phoneContacts: [],
    inputValue: ""
  };

  componentDidMount() {
    const { data, last } = this.props;
    Backend.phoneContactsHandler.phoneContacts(this.phoneContacts.bind(this));

    const defaultName = data ? data.givenName + " " + data.familyName : null;
    if (defaultName) this.setState({ inputValue: defaultName });

    if (last && Platform.OS === "android") {
      setTimeout(() => {
        this.inputRef.focus();
      }, 120);
    }
  }

  phoneContacts(phoneContacts) {
    LayoutAnimation.configureNext(AnimationConfigs.default());
    this.setState({ phoneContacts });
  }

  press() {
    LayoutAnimation.configureNext(AnimationConfigs.default());
    this.setState({ active: true });
    if (this.inputRef) this.inputRef.focus();
  }
  endEditing() {
    LayoutAnimation.configureNext(AnimationConfigs.default());
    this.setState({ active: false });
    this.save();
  }
  inputFocused(e) {
    const { onFocus } = this.props;
    if (onFocus)
      onFocus({
        event: e,
        measurements: this.measurements,
        viewRef: this.containerRef
      });

    if (this.state.active === true) return;

    LayoutAnimation.configureNext(AnimationConfigs.default());
    this.setState({ active: true });
  }

  potentialContactSelected(contact) {
    const name = contact.givenName + " " + contact.familyName;
    this.setState({ inputValue: name, active: false });
    this.save(contact);

    const { onFocus } = this.props;
    if (onFocus) {
      setTimeout(() => {
        onFocus({ event: null, measurements: this.measurements });
      }, 100);
    }
  }

  done() {
    const { inputValue } = this.state;
    if (inputValue.length < 1) return;
    const { data, contactIndex, screenIndex, onFocus } = this.props;
    const contactName = data ? data.givenName + " " + data.familyName : null;
    if (inputValue.length > 0 && inputValue !== contactName) {
      const names = inputValue.split(" ");
      let givenName = "";
      for (let x = 0; x < names.length - 1; x++)
        givenName += (x > 0 ? " " : "") + names[x];
      const familyName =
        names[names.length - 1] !== givenName ? names[names.length - 1] : null;
      const newContact = {
        givenName,
        familyName,
        recordID: ledger.recordID(inputValue)
      };
      this.save(newContact);
    }

    LayoutAnimation.configureNext(AnimationConfigs.default());
    this.setState({ active: false });

    if (onFocus) {
      setTimeout(() => {
        onFocus({ event: null, measurements: this.measurements });
      }, 100);
    }
  }

  save(_contact) {
    const { screenIndex = 0, contactIndex, last } = this.props;
    if (!_contact) {
      // TODO: save just the name?
      return;
    }
    let contact = this._contact(_contact);

    ledger.setContact({
      contact,
      contactIndex,
      screenIndex
    });

    if (last) {
      this.setState({ inputValue: "" });
      setTimeout(() => {
        this.press();
      }, 100);
    }
  }

  _contact(contact) {
    // returns given contact, UNLESS, contact is already in Build My List, it will use that object
    const listBuilderHandler = Backend.listBuilderHandler;
    const buildMyList = listBuilderHandler.getListById(
      listBuilderHandler._config.memoryJogger.id
    );
    if (buildMyList) {
      for (c in buildMyList.contacts) {
        const someContact = buildMyList.contacts[c];
        if (ledger.recordID(someContact) === ledger.recordID(contact))
          return someContact;
      }
    }
    return contact;
  }

  onLayout(e) {
    const { onMeasure } = this.props;
    this.containerRef.measureLayout((fx, fy, width, height, px, py) => {
      this.measurements = { fx, fy, width, height, px, py };
      if (onMeasure) onMeasure(fx, fy, width, height, px, py);
    });
  }

  render() {
    const { last } = this.props;
    const { active, phoneContacts, inputValue } = this.state;

    const potentialContacts = [];
    let potentialContactIDs = "";
    for (c in phoneContacts) {
      if (potentialContacts.length >= 4) break;
      if (inputValue.length === 0) break;
      const contact = phoneContacts[c];
      const givenName = contact.givenName ? contact.givenName : "";
      const familyName = contact.familyName ? contact.familyName : "";
      const contactFullName = (givenName + " " + familyName).toLowerCase();
      const val = inputValue.toLowerCase();
      if (potentialContactIDs.includes(contact.recordID)) continue;
      if (
        givenName.toLowerCase().startsWith(val) ||
        familyName.toLowerCase().startsWith(val) ||
        contactFullName.includes(val)
      ) {
        potentialContactIDs += contact.recordID + ", ";
        potentialContacts.push(
          <PotentialContact
            data={contact}
            onSelect={this.potentialContactSelected.bind(this)}
          />
        );
      }
    }

    const underlineStyle = active
      ? {
          backgroundColor: "white",
          width: "100%",
          height: 3
        }
      : {};

    const autoFocus = Platform.OS === "ios" ? last : false;

    return (
      <TouchableWithoutFeedback onPress={this.press.bind(this)}>
        <View
          style={Styles.container}
          ref={ref => (this.containerRef = ref)}
          onLayout={this.onLayout.bind(this)}
        >
          <View style={Styles.row}>
            <View
              style={{
                ...Styles.inputContainer,
                ...(active ? Styles.inputContainerActive : {})
              }}
            >
              <TextInput
                style={Styles.textInput}
                ref={ref => (this.inputRef = ref)}
                autoFocus={autoFocus}
                autoCorrect={false}
                value={inputValue}
                onChangeText={inputValue => this.setState({ inputValue })}
                selectionColor="white"
                autoCapitalize="words"
                blurOnSubmit={false}
                onEndEditing={this.endEditing.bind(this)}
                onFocus={this.inputFocused.bind(this)}
                onSubmitEditing={this.done.bind(this)}
              />
              <View style={underlineStyle} />
            </View>

            {active ? <AddButton onPress={this.done.bind(this)} /> : null}
          </View>

          {active ? potentialContacts : null}

          {active ? null : <View style={Styles.coverView} />}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

function PotentialContact(props) {
  const { data, onSelect } = props;
  const name = data.givenName + " " + data.familyName;
  const phoneNumbers = [];
  for (p in data.phoneNumbers) {
    const object = data.phoneNumbers[p];
    phoneNumbers.push(
      <Text
        style={Styles.potentialContactPhoneNumber}
        key={`number.${object.id}`}
      >
        {object.number}
      </Text>
    );
  }

  return (
    <TouchableOpacity onPress={() => onSelect(data)}>
      <View style={Styles.potentialContactContainer}>
        <View>
          <Text style={Styles.potentialContactName}>{name}</Text>
          {phoneNumbers}
        </View>
        <Image source={PLUS_ICON} style={Styles.plusIcon} />
      </View>
    </TouchableOpacity>
  );
}

function AddButton(props) {
  const { onPress } = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={Styles.addContainer}>
        <Image style={Styles.addPlusIcon} source={PLUS_ICON} />
      </View>
    </TouchableOpacity>
  );
}

const Styles = {
  container: {
    marginTop: 7,
    overflow: "hidden",
    width: "100%",
    borderRadius: 7,
    backgroundColor: "#454f6b",
    paddingVertical: 7,
    paddingHorizontal: 9,
    ...(Platform.OS === "android"
      ? {
          // height: 50
        }
      : {})
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  inputContainer: {
    width: "80%"
  },
  inputContainerActive: {
    marginBottom: 7
  },
  textInput: {
    color: "white",
    fontFamily: K.Fonts.medium,
    fontSize: moderateScale(20),
    width: "100%",
    marginBottom: 0.4,
    paddingBottom: 0
  },

  addContainer: {
    // circle around +
    justifyContent: "center",
    alignItems: "center",
    width: moderateScale(35),
    height: moderateScale(35),
    borderRadius: moderateScale(33) / 2,
    backgroundColor: "#1bcf6b"
  },
  addPlusIcon: {
    // + in circle
    tintColor: "white",
    width: moderateScale(17),
    height: moderateScale(17)
  },

  potentialContactContainer: {
    width: "100%",
    minHeight: moderateScale(30),
    borderRadius: 7,
    marginTop: 6,
    backgroundColor: "#5c6991",
    paddingHorizontal: 12,
    paddingLeft: 4,
    paddingRight: 17,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  potentialContactName: {
    color: "rgb(237,237,237)",
    fontFamily: K.Fonts.bold,
    fontSize: moderateScale(18)
  },
  potentialContactPhoneNumber: {
    marginLeft: 1,
    marginTop: -3,
    color: "white",
    fontFamily: K.Fonts.medium,
    fontSize: moderateScale(16)
  },
  plusIcon: {
    tintColor: "white",
    width: moderateScale(20),
    height: moderateScale(20)
  },

  coverView: {
    position: "absolute",
    width: "100%",
    height: "100%"
  }
};
