import Backend from "backend/";
import { DirectCoverModal } from "components";
import AnimationConfigs from "memoryJogger/AnimationConfigs";
import { ledger } from "memoryJogger/Ledger";
import React, { Component } from "react";
import { LayoutAnimation, View } from "react-native";
import ContactItem from "./ContactItem";


let hasShownCoverModalBefore = false;

export default class ContactsList extends Component {
  state = {
    contactComponents: null,
    directCoverModal: { visible: false }
  };

  componentDidMount() {
    ledger.data(this.ledgerData.bind(this));
  }
  ledgerData(data) {
    const { index } = this.props;
    const contacts = data[index].contacts;
    const contactComponents = [];
    for (c in contacts) {
      const contact = contacts[c];
      contactComponents.push(
        <ContactItem
          key={`contact.${c}`}
          data={contact}
          contactIndex={c}
          screenIndex={index}
          onFocus={this.itemFocused.bind(this)}
        />
      );
    }
    if (this.state.contactComponents)
      LayoutAnimation.configureNext(AnimationConfigs.default());
    this.setState({ contactComponents });
  }

  itemFocused(data) {
    // data = {event,measurements}
    const { onItemFocus } = this.props;
    if (onItemFocus) onItemFocus(data);
  }

  hideDirectCoverModal() {
    this.setState({
      directCoverModal: {
        visible: false
      }
    });
  }

  onMeasureLastItem(fx, fy, width, height, px, py) {
    const buildMyList = Backend.listBuilderHandler.getListById(
      Backend.listBuilderHandler._config.memoryJogger.id
    );
    if (hasShownCoverModalBefore === true || buildMyList) return;
    hasShownCoverModalBefore = true;
    this.setState({
      directCoverModal: {
        visible: true,
        text:
          'If someone is not in your\nphone contacts, you\ncan tap "+" to add\nthem anyway.',
        donePress: this.hideDirectCoverModal.bind(this),
        startX: width - 50,
        endX: width + 5,
        startY: py,
        endY: py + height + 10,
        backgroundOpacity: 0.6
      }
    });
  }

  render() {
    const { index } = this.props;
    const { contactComponents = [], directCoverModal } = this.state;
    return (
      <>
        <View style={Styles.container}>
          {contactComponents}
          <ContactItem
            key="lastitem"
            screenIndex={index}
            last
            onFocus={this.itemFocused.bind(this)}
            onMeasure={this.onMeasureLastItem.bind(this)}
          />
        </View>
        <DirectCoverModal data={directCoverModal} />
      </>
    );
  }
}

const Styles = {
  container: {
    width: "100%",
    paddingVertical: 12,
    paddingHorizontal: 6
  },
  itemContainer: {
    width: "100%",
    borderRadius: 7,
    backgroundColor: "gray",
    flexDirection: "row"
  }
};
