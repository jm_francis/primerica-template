import React, { Component } from "react";
import { View, Text, TouchableWithoutFeedback, Image } from "react-native";
import { CHECK } from "constant-deprecated/Images";
import { moderateScale } from "utilities/";

import K from "constant-deprecated";
import { ledger } from "memoryJogger/Ledger";

export default class PriorityItem extends Component {
  pressed() {
    const { priority, contact } = this.props;
    ledger.checkPriorityForContact(priority.id, contact);
  }

  render() {
    const { priority, contact } = this.props;
    const name = contact.givenName + " " + contact.familyName;

    const selected = contact.priorities
      ? contact.priorities[priority.id]
      : false;

    return (
      <TouchableWithoutFeedback onPress={this.pressed.bind(this)}>
        <View
          style={{
            ...Styles.container,
            ...(selected ? Styles.containerSelected : {})
          }}
        >
          <Text
            style={{
              ...Styles.name,
              ...(selected ? Styles.nameSelected : {})
            }}
          >
            {name}
          </Text>

          <View
            style={{
              ...Styles.checkContainer,
              ...(selected ? Styles.checkContainerSelected : {})
            }}
          >
            <Image
              source={CHECK}
              style={{
                ...Styles.checkImage,
                ...(selected ? Styles.checkImageSelected : {})
              }}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const Styles = {
  container: {
    borderRadius: 5,
    marginVertical: 5,
    backgroundColor: "#4d5166",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 12,
    paddingVertical: 7
  },
  containerSelected: {
    backgroundColor: "#1bcf6b"
  },
  name: {
    color: "rgb(215,215,215)",
    fontFamily: K.Fonts.medium,
    fontSize: moderateScale(19.5)
  },
  nameSelected: {
    color: "white"
  },
  checkContainer: {},
  checkContainerSelected: {},
  checkImage: {
    width: moderateScale(22),
    height: moderateScale(22),
    tintColor: "rgb(140,140,140)"
  },
  checkImageSelected: {
    tintColor: "white"
  }
};
