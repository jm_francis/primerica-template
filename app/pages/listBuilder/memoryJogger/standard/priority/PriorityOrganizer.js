import React, { Component } from "react";
import { View } from "react-native";
import PriorityItem from "./PriorityItem";

import K from "constant-deprecated";
import { ledger } from "memoryJogger/Ledger";

export default class PriorityOrganizer extends Component {
  state = {
    priority: null,
    contacts: []
  };

  componentDidMount() {
    ledger.data(this.ledgerData.bind(this));
  }
  ledgerData(data) {
    const { screenIndex, itemIndex } = this.props;
    const itemData = data[screenIndex].items[itemIndex];

    this.setState({
      priority: itemData.priority,
      contacts: ledger.allContacts()
    });
  }

  render() {
    const { contacts, priority } = this.state;
    if (!priority) return null;
    const items = [];
    for (c in contacts) {
      const contact = contacts[c];
      items.push(
        <PriorityItem
          key={`item.${contact.recordID}`}
          priority={priority}
          contact={contact}
        />
      );
    }

    return (
      <View style={Styles.container}>
        {/* {text?
                    <Text style={Styles.prompt}>{text}</Text>
                : null} */}

        {items}
      </View>
    );
  }
}

const Styles = {
  container: {
    marginTop: 15,
    paddingHorizontal: 12,
    width: "100%",
    marginBottom: 7
  },
  prompt: {
    color: "white",
    fontFamily: K.Fonts.medium,
    fontSize: 24,
    marginBottom: 5
  }
};
