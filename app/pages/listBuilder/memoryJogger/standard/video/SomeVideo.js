import { Button } from "@ui-kitten/components";
import VimeoAPI from "app/backend/apis/VimeoAPI";
import { Txt } from "components";
import { IconPrimr } from "components";
import { MARRIED, SUPPORTIVE, WORKWITH } from "constant-deprecated/Videos";
import React, { Component } from "react";
import { Dimensions, View } from "react-native";
import Video from "react-native-video";
import { scale, spacing } from "utilities/";

const LocalSources = Object.freeze({
  MARRIED,
  SUPPORTIVE,
  WORKWITH,
});

export default class SomeVideo extends Component {
  state = {
    videoUri: null,
    videoThumbnail: null,
    videoPaused: false,
  };

  componentDidMount() {
    const { source } = this.props;
    if (!source.includes("http")) return;

    VimeoAPI.getVideo({ shareLink: source }).then((response, error) => {
      this.setState({
        videoUri: VimeoAPI.getMP4FromVideo(response),
        videoThumbnail: VimeoAPI.getThumbnailFromVideo(response),
      });
      setTimeout(() => {
        this.setState({ videoPaused: !this.state.videoPaused });
      }, 300);
    });
  }

  videoPressed() {
    this.player && this.player.seek(0);
    this.setState({ videoPaused: false });
  }

  render() {
    const { source, emoji = "" } = this.props;
    const { videoUri, videoThumbnail, videoPaused } = this.state;

    let localSource;
    if (!source.includes("http")) {
      localSource = LocalSources[source];
    }

    return (
      <View>
        <View style={Styles.container}>
          {videoUri ? (
            <Video
              ref={(ref) => (this.player = ref)}
              paused={videoPaused}
              onEnd={() => this.setState({ videoPaused: true })}
              ignoreSilentSwitch="ignore"
              resizeMode="cover"
              poster={videoThumbnail}
              posterResizeMode="cover"
              source={{ uri: videoUri }}
              style={Styles.videoStyle}
            />
          ) : null}
          {localSource ? (
            <Video
              ref={(ref) => (this.player = ref)}
              paused={videoPaused}
              onEnd={() => this.setState({ videoPaused: true })}
              source={localSource}
              ignoreSilentSwitch="ignore"
              resizeMode="cover"
              posterResizeMode="cover"
              style={Styles.videoStyle}
            />
          ) : null}
          <Txt style={Styles.emoji} category="h4">{emoji}</Txt>
        </View>
        <View style={{ position: "absolute", bottom: 10, right: 10 }}>
          {/* <Text>{JSON.stringify(videoPaused)}</Text> */}
          {videoPaused? <Button
            onPress={this.videoPressed.bind(this)}
            status={videoPaused ? "primary" : "info"} //* NOTE Will update once we build our design system
            accessoryLeft={() => (
              <IconPrimr size={scale(20)} name="refresh" color="white" />
            )}
          /> : null}
        </View>
      </View>
    );
  }
}

const screenWidth = Dimensions.get("window").width;

const height = screenWidth * 0.95 * 0.65;

const Styles = {
  container: {
    // borderRadius: 7,
    overflow: "hidden",
    // backgroundColor: 'black',
    backgroundColor: "rgb(214,216,213)",
    // width: screenWidth*0.95,
    width: screenWidth,
    borderRadius: 0,
    height,
    alignSelf: "center",
  },
  videoStyle: {
    height,
    width: screenWidth,
  },
  emoji: {
    position: "absolute",
    top: spacing(2),
    right: spacing(2),
  },
};
