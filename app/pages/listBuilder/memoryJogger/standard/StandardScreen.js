import K from "constant-deprecated";
import { ledger } from "memoryJogger/Ledger";
import React, { Component } from "react";
import {
  Dimensions,
  findNodeHandle,
  ScrollView,
  Text,
  View,
} from "react-native";
import { moderateScale, spacing } from "utilities/";
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ContactsList from "./contactsList/ContactsList";
import PriorityOrganizer from "./priority/PriorityOrganizer";
import SomeVideo from "./video/SomeVideo";

const screenHeight = Dimensions.get("window").height;

export default class StandardScreen extends Component {
  state = {
    screenData: null,
  };

  componentDidMount() {
    ledger.data(this.ledgerData.bind(this));
  }
  ledgerData(data) {
    const { index } = this.props;
    this.setState({ screenData: data[index] });
  }

  onItemFocus(data) {
    const { screenData } = this.state;
    if (screenData.contacts.length < 1) return;

    const { event, measurements = {}, viewRef } = data;
    if (!measurements.fy && !viewRef) {
      this.scrollRef.scrollToEnd();
      return;
    }
    const { fx, fy, width, height, px, py } = measurements;
    if (this.scrollRef && Platform.OS === "ios") {
      // NOTE: probably uneeded now with android solution
      this.scrollRef.scrollTo({ x: 0, y: fy, animated: true });
    } else if (this.scrollRef && Platform.OS === "android") {
      viewRef.measureLayout(findNodeHandle(this.scrollRef), (x, y) => {
        this.scrollRef.scrollTo({ x: 0, y, animated: true });
      });
    }
  }

  render() {
    const { index } = this.props;
    const { screenData } = this.state;
    if (!screenData || screenData.length < 1) return null;
    const items = screenData.items;

    const components = [];
    let didAddContactsList = false;
    for (i in items) {
      const item = items[i];
      if (item.itemType === "contacts_list") {
        didAddContactsList = true;
        components.push(
          <ContactsList
            key={`item.${index}`}
            index={index}
            onItemFocus={this.onItemFocus.bind(this)}
          />
        );
      } else if (item.itemType === "priority_organizer")
        components.push(
          <PriorityOrganizer
            key={`item.${index}`}
            screenIndex={index}
            itemIndex={i}
          />
        );
      else if (item.video && item.itemType === "video")
        components.push(
          <SomeVideo
            key={`item.${index}`}
            source={item.video}
            emoji={item.emoji}
          />
        );
      else if (item.text && item.itemType === "text")
        components.push(<SomeText key={`item.${index}`}>{item.text}</SomeText>);
    }

    const extraScrollHeight = didAddContactsList ? screenHeight * 0.677 : 90;

    return (
      <ScrollView
        ref={(ref) => (this.scrollRef = ref)}
        style={Styles.container}
        extraScrollHeight={300}
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}
      >
        {components}
        <View style={{ width: "100%", height: extraScrollHeight }} />
      </ScrollView>
    );
  }
}

function SomeText(props) {
  const text = props.children ? props.children.replace(/\\n/g, "\n") : "";
  return (
    <View style={Styles.textContainer}>
      <Text style={Styles.text} adjustsFontSizeToFit>
        {text}
      </Text>
    </View>
  );
}

const Styles = {
  container: {
    flex: 1,
  },
  textContainer: {
    width: "100%",
    paddingHorizontal: 7,
    paddingVertical: 12,
  },
  text: {
    color: "white",
    fontFamily: K.Fonts.medium,
    fontSize: moderateScale(23),
  },
};
