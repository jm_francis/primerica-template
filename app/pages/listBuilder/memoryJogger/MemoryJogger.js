import React, { Component } from "react";
import {
  View,
  Dimensions,
  Image,
  Platform,
  KeyboardAvoidingView,
  Text,
  Keyboard,
  LayoutAnimation,
  TouchableOpacity,
} from "react-native";
import { moderateScale, verticalScale, scale, spacing, C } from "utilities/";
import TitleButton from "navigation/components/TitleButton";
import Title from "navigation/components/Title";
// import { isIphoneX } from "react-native-iphone-x-helper";
import { LEFT_ARROW, RIGHT_ARROW, CHECK } from "constant-deprecated/Images";

import StandardScreen from "./standard/StandardScreen";
import MessageScreen from "./message/MessageScreen";
import AnimationConfigs from "memoryJogger/AnimationConfigs";

import { ledger } from "memoryJogger/Ledger";
import K from "constant-deprecated";
import { ShareList } from "./share-list/ShareList";
import { Kitten } from "components";
import { Buttoon } from "components";

export default class MemoryJogger extends Component {
  static navigationOptions = ({ navigation }) => ({
    // headerTitle: () => (
    //   <Title
    //     fixAndroid
    //     style={{
    //       position: "absolute",
    //       bottom: spacing(2),
    //     }}
    //   >
    //     Build My List
    //   </Title>
    // ),
    // headerTitleStyle: {
    //   fontFamily: K.Fonts.bold,
    //   fontSize: moderateScale(18.4, 0.37),
    //   color: "white",
    // },
    // headerStyle: {
    //   height: Platform.isPad
    //     ? 90
    //     : (Platform.OS === "android" ? 27 : 0),
    //   backgroundColor: "rgb(214,20,29)",
    // },
    // headerLeft: () => (
    //   <TitleButton
    //     style={{
    //       marginTop: spacing(1) + 7,
    //       marginLeft: spacing(3),
    //     }}
    //     onPress={() => {
    //       navigation.pop();
    //     }}
    //   >
    //     Quit
    //   </TitleButton>
    // ),
  });

  state = {
    screenIndex: -1,
    kb: false,
  };

  componentDidMount() {
    Keyboard.addListener("keyboardWillHide", this.keyboardHide.bind(this));
    Keyboard.addListener("keyboardWillShow", this.keyboardShow.bind(this));
    ledger.data(this.ledgerData.bind(this));
  }
  ledgerData(data) {
    if (this._ledgerData) return;
    this._ledgerData = data;
    this.setState({ screenIndex: 0 });
  }
  keyboardHide() {
    this.setState({ kb: false });
  }
  keyboardShow() {
    this.setState({ kb: true });
  }

  currentScreen() {
    const { screenIndex } = this.state;
    const screenType = this._ledgerData[screenIndex].screenType;

    if (screenType === "message") {
      return (
        <MessageScreen key={`screen.${screenIndex}`} index={screenIndex} />
      );
    }

    if (screenType === "share-list") {
      return <ShareList key={`screen.${screenIndex}`} />;
    }

    return <StandardScreen key={`screen.${screenIndex}`} index={screenIndex} />;
  }

  backPress() {
    const { screenIndex } = this.state;
    if (screenIndex - 1 < 0) return;
    LayoutAnimation.configureNext(AnimationConfigs.navigationBar());
    Keyboard.dismiss();
    this.setState({ screenIndex: screenIndex - 1 });
  }
  nextPress() {
    const { screenIndex } = this.state;
    if (screenIndex + 1 >= ledger._data.length) return;
    LayoutAnimation.configureNext(AnimationConfigs.navigationBar());
    this.setState({ screenIndex: screenIndex + 1 });
  }
  finishPress() {
    ledger.saveToFirestore().then((listData) => {
      this.props.navigation.pop();
      this.props.navigation.navigate("ListScreen", { listData });
    });
  }

  render() {
    const { screenIndex, kb } = this.state;
    if (screenIndex < 0) return null;
    return (
      <View style={Styles.container}>
        <Kitten.TopNavigation
          title="Build My List"
          style={{
            backgroundColor: "rgb(214,20,29)",
          }}
          alignment="center"
          accessoryLeft={() => (
            <Buttoon
              style={{
                backgroundColor: "transparent",
                borderColor: "transparent",
                marginLeft: -spacing(4),
              }}
              onPress={() => {
                this.props.navigation.pop();
              }}
              // icon={{
              //   name: "arrow_left",
              //   color: "white",
              //   size: moderateScale(20),
              // }}
            >
              Quit
            </Buttoon>
          )}
        />
        {this.currentScreen()}
        <NavigationBar
          screenIndex={screenIndex}
          kb={kb}
          onBackPress={this.backPress.bind(this)}
          onNextPress={this.nextPress.bind(this)}
          onFinishPress={this.finishPress.bind(this)}
        />
      </View>
    );
  }
}

function NavigationBar(props) {
  const { onBackPress, onNextPress, onFinishPress, screenIndex, kb } = props;
  const isLastIndex = screenIndex >= ledger._data.length - 1;

  // NOTE: also update StandardScreen scroll view to have this same extra height at bottom
  const height = moderateScale(kb ? 57 : 80 + (!kb ? 5 : 0));
  console.log("NB height: " + height);

  const keyboardVerticalOffset = Platform.isPad ? 130 : 94;

  // const keyboardVerticalOffset = Platform.isPad
  //   ? 215
  //   : true
  //   ? 137
  //   : Platform.OS === "android"
  //   ? 127
  //   : 117;
  console.log("keyboardVOffset: " + keyboardVerticalOffset);

  return (
    <KeyboardAvoidingView
      behavior="padding"
      keyboardVerticalOffset={keyboardVerticalOffset}
      style={{
        width: screenWidth,
        bottom: 0,
        position: "absolute",
        height,
      }}
    >
      <View
        style={{
          width: screenWidth,
          height,
          flexDirection: "row",
        }}
      >
        <BarButton
          visible={screenIndex > 0}
          back
          onPress={onBackPress}
          kb={kb}
        />
        <BarButton visible={!isLastIndex} next onPress={onNextPress} kb={kb} />
        <BarButton
          visible={isLastIndex}
          finish
          onPress={onFinishPress}
          kb={kb}
        />
      </View>
    </KeyboardAvoidingView>
  );
}
function BarButton(props) {
  const { onPress, back, next, finish, visible, kb } = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        flex: visible ? 1 : 0,
        overflow: "hidden",
        ...(visible ? {} : { width: 0 }),
      }}
    >
      <View
        style={{
          ...Styles.barButton,
          backgroundColor: back ? backColor : next ? nextColor : finishColor,
        }}
      >
        <View
          style={{
            ...Styles.barButtonInner,
            marginTop: -12,
            ...(true && !kb ? { marginTop: -10 } : { marginTop: -12 }),
          }}
        >
          {back ? (
            <Image style={Styles.barButtonIconLeft} source={LEFT_ARROW} />
          ) : null}
          <Text style={Styles.barButtonText}>
            {back ? "Back" : next ? "Next" : "Finish"}
          </Text>
          {next ? (
            <Image style={Styles.barButtonIconRight} source={RIGHT_ARROW} />
          ) : null}
          {finish ? (
            <Image style={Styles.barButtonIconRight} source={CHECK} />
          ) : null}
        </View>
      </View>
    </TouchableOpacity>
  );
}

const screenWidth = Dimensions.get("window").width;

const backColor = "#d9292c";
const nextColor = "#3148f5";
const finishColor = "#1bcf6b";

const barButtonIcon = {
  tintColor: "white",
  width: moderateScale(24),
  height: moderateScale(24),
};
const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background01,
  },
  navigationBarContainer: {
    position: "absolute",
    bottom: 0,
    width: screenWidth,
    height: moderateScale(45),
    flexDirection: "row",
  },
  barButton: {
    flex: 1,
    justifyContent: "space-evenly",
    alignItems: "center",
    flexDirection: "row",
  },
  barButtonInner: {
    flexDirection: "row",
    alignItems: "center",
  },
  barButtonText: {
    fontFamily: K.Fonts.bold,
    fontSize: moderateScale(24),
    color: "white",
    textAlign: "center",
  },
  barButtonIconLeft: {
    ...barButtonIcon,
    marginRight: 25,
  },
  barButtonIconRight: {
    ...barButtonIcon,
    marginLeft: 25,
  },
};
