import K from "constant-deprecated";
import { ledger } from "memoryJogger/Ledger";
import React, { Component } from "react";
import { Text, View } from "react-native";
import { moderateScale } from "utilities/";


export default class MessageScreen extends Component {
  state = {
    screenData: null
  };

  componentDidMount() {
    ledger.data(this.ledgerData.bind(this));
  }
  ledgerData(data) {
    const { index } = this.props;
    this.setState({ screenData: data[index] });
  }

  render() {
    const { screenData } = this.state;
    if (!screenData) return null;
    const { message } = screenData;

    return (
      <View style={Styles.container}>
        <Text style={Styles.title} adjustsFontSizeToFit>
          {message}
        </Text>
      </View>
    );
  }
}

const Styles = {
  container: {
    flex: 1,
    backgroundColor: "#151b33",
    marginTop: -124,
    zIndex: -10,
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    color: "white",
    fontFamily: K.Fonts.bold,
    fontSize: moderateScale(36),
    textAlign: "center",
    marginHorizontal: 15
  }
};
