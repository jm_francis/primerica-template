import { Fonts } from "constant-deprecated";
import { LEFT_ARROW } from "constant-deprecated/Images";
import React from "react";
import {
  Dimensions, Image,

  Platform, Text,


  TouchableOpacity, View
} from "react-native";
import { moderateScale } from "utilities/";

export default class MoveButton extends React.Component {
  render() {
    const { rowHeight } = this.props;
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={{ ...Styles.container, height: rowHeight }}>
          <Text allowFontScaling={false} style={Styles.title}>
            Move
          </Text>
          <Image source={LEFT_ARROW} style={Styles.icon} />
        </View>
      </TouchableOpacity>
    );
  }
}

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

const Styles = {
  container: {
    minWidth: Platform.isPad ? screenWidth * 0.5 * 0.286 : 100,
    borderRadius: 10,
    backgroundColor: "rgb(28, 109, 239)",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 12
  },
  title: {
    color: "white",
    fontFamily: Fonts.medium,
    fontWeight: "600",
    fontSize: moderateScale(16.7, 0.27)
  },
  icon: {
    width: moderateScale(20, 0.15),
    height: moderateScale(20, 0.15),
    tintColor: "white",
    transform: [{ rotate: "180deg" }]
  }
};
