import { Fonts } from "constant-deprecated";
import { EMAIL, SMARTPHONE } from "constant-deprecated/Images";
import { shade } from "pages/listBuilder/configure";
import React from "react";
import {
  Alert,

  Image,

  Platform, Text, TouchableOpacity, View
} from "react-native";
import ActionSheet from "react-native-actionsheet";
import email from "react-native-email";
import call from "react-native-phone-call";
import SendSMS from "react-native-sms";
import { C, moderateScale } from "utilities/";

const darkMode = true;

export default class ConnectButton extends React.Component {
  press() {
    const { phone, givenName, familyName, overridePress } = this.props;
    const _email = this.props.email;

    if (overridePress) {
      overridePress({
        givenName,
        phone: phone,
        email: _email
      });
      return;
    }

    if (_email) {
      email([_email], {
        subject: "",
        body: ""
      });
    } else if (phone) {
      this.phoneActionSheet.show();
    }
  }

  phoneActionSheetPress(index) {
    const { phone } = this.props;
    if (index === 0) {
      SendSMS.send(
        {
          body: "",
          recipients: [phone.toString()],
          successTypes: ["sent", "queued"],
          allowAndroidSendWithoutReadPermission: true
        },
        (completed, cancelled, error) => {
          if (error) Alert.alert("Error", error);
        }
      );
    } else if (index === 1) {
      call({
        number: phone,
        prompt: false
      });
    }
  }

  render() {
    const { expand, phone, email, label } = this.props;

    const expandedContainer = expand ? Styles.expandedContainer : {};
    const expandedText = expand ? Styles.expandedText : {};
    const expandedIcon = expand ? Styles.expandedIcon : {};

    const contact = phone ? "" + phone : email;
    return [
      <TouchableOpacity
        onPress={this.press.bind(this)}
        disabled={!expand}
        key="touchable"
      >
        <View
          style={{
            ...this.props.style,
            ...Styles.container,
            ...expandedContainer
          }}
        >
          <Text
            style={{
              ...this.props.textStyle,
              ...Styles.title,
              ...expandedText
            }}
          >
            {contact}
          </Text>
          <View style={Styles.right}>
            {label ? <Text style={Styles.labelText}>{label}</Text> : null}
            <Image
              source={email ? EMAIL : SMARTPHONE}
              style={{ ...Styles.icon, ...expandedIcon }}
            />
          </View>
        </View>
      </TouchableOpacity>,
      <ActionSheet
        ref={ref => (this.phoneActionSheet = ref)}
        key="actionsheet"
        options={["Text", "Call", "Cancel"]}
        cancelButtonIndex={2}
        onPress={this.phoneActionSheetPress.bind(this)}
      />
    ];
  }
}

const fontMod = 0.3;

const Styles = {
  container: {
    borderRadius: 4,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 13
  },
  right: {
    flexDirection: "row"
  },
  labelText: {
    color: darkMode ? shade(true, 0.2) : "rgb(51, 50, 50)",
    fontSize: moderateScale(10, fontMod),
    marginRight: 11,
    fontFamily: Fonts.thin
  },
  expandedContainer: {
    height: Platform.isPad ? "100%" : 35,
    backgroundColor: C.grey600
    // backgroundColor: darkMode ? shade(true, 0.5) : "rgba(179, 180, 154, 1)"
  },
  title: {
    left: -12,
    color: shade(!darkMode, 0.94),
    fontSize: moderateScale(14, fontMod),
    fontFamily: Fonts.medium
  },
  expandedText: {
    left: 0,
    fontSize: moderateScale(17, fontMod),
    color: "white"
  },
  icon: {
    width: moderateScale(18, 0.2),
    height: moderateScale(18, 0.2)
  },
  expandedIcon: {
    width: moderateScale(27, 0.08),
    height: moderateScale(27, 0.08)
  }
};
