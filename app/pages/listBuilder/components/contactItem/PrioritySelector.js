import { Fonts } from "constant-deprecated";
import { STAR } from "constant-deprecated/Images";
import { defaultLayoutAnimation, shade } from "pages/listBuilder/configure";
import React from "react";
import {
  Image,

  LayoutAnimation,

  Platform, Text,



  TouchableWithoutFeedback, UIManager, View
} from "react-native";
import { moderateScale } from "utilities/";
const darkMode = true;

export default class PrioritySelector extends React.Component {
  state = { selectedIndex: 1 };

  press(index) {
    if (this.props.outsider) return this.props.outsider();

    LayoutAnimation.configureNext(defaultLayoutAnimation);

    this.setState({ selectedIndex: index });
    if (this.props.onSelect) this.props.onSelect(index);
  }

  componentDidMount() {
    if (Platform.OS === "android")
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);

    const { defaultIndex = 1 } = this.props;
    this.setState({ selectedIndex: defaultIndex });
  }

  render() {
    const { rowHeight } = this.props;
    return (
      <View style={Styles.container}>
        <Text allowFontScaling={false} style={Styles.title}>
          Priority
        </Text>
        <View style={Styles.starContainer}>
          <Item
            index={1}
            selectedIndex={this.state.selectedIndex}
            onPress={this.press.bind(this)}
            rowHeight={rowHeight}
          />
          <Item
            index={2}
            selectedIndex={this.state.selectedIndex}
            onPress={this.press.bind(this)}
            rowHeight={rowHeight}
          />
          <Item
            index={3}
            selectedIndex={this.state.selectedIndex}
            onPress={this.press.bind(this)}
            rowHeight={rowHeight}
          />
        </View>
      </View>
    );
  }
}

class Item extends React.Component {
  press() {
    this.props.onPress(this.props.index);
  }

  render() {
    const { index = 0, selectedIndex = -1, rowHeight } = this.props;
    const selected = index === selectedIndex;
    const selectedStar = selected ? Styles.selectedStarIcon : {};
    const selectedItem = selected
      ? { ...Styles.selectedItem, width: rowHeight * index }
      : {};

    const stars = [];
    for (let x = 0; x < index; x++)
      stars.push(
        <Image
          source={STAR}
          style={{
            ...Styles.starIcon,
            width: rowHeight * 0.7,
            height: rowHeight * 0.7,
            ...selectedStar
          }}
        />
      );

    return (
      <TouchableWithoutFeedback onPress={this.press.bind(this)}>
        <View style={{ ...Styles.item, height: rowHeight, ...selectedItem }}>
          {stars}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const Styles = {
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  starContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end"
  },
  title: {
    fontFamily: Fonts.medium,
    fontSize: moderateScale(19, 0.25),
    color: shade(!darkMode, 0.93)
  },
  item: {
    paddingHorizontal: 7,
    // height: rowHeight,
    marginHorizontal: 4,
    borderRadius: 7,
    backgroundColor: "rgb(85, 87, 88)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly"
  },
  selectedItem: {
    backgroundColor: "rgb(45, 113, 168)"
  },
  starIcon: {
    // width: rowHeight*0.7,
    // height: rowHeight*0.7,
    tintColor: "rgb(212, 212, 212)"
  },
  selectedStarIcon: {
    tintColor: "rgb(217, 203, 22)"
  }
};
