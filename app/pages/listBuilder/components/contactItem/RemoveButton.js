import { Fonts } from "constant-deprecated";
import { TRASH } from "constant-deprecated/Images";
import React from "react";
import {
  Dimensions, Image,



  TouchableOpacity, View
} from "react-native";
import { moderateScale } from "utilities/";

export default class RemoveButton extends React.Component {
  render() {
    const { rowHeight } = this.props;
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View
          style={{
            height: rowHeight * 2,
            width: rowHeight * 2,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <View
            style={{
              ...Styles.container,
              height: rowHeight * 1.83,
              width: rowHeight * 1.83
            }}
          >
            <Image source={TRASH} style={Styles.icon} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const screenWidth = Dimensions.get("window").width;
const Styles = {
  container: {
    borderRadius: 10,
    backgroundColor: "rgba(254, 42, 42, 0.85)",
    flexDirection: "row",
    paddingHorizontal: 12,
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    color: "white",
    fontFamily: Fonts.medium,
    fontSize: moderateScale(16.7, 0.27)
  },
  icon: {
    width: moderateScale(33, 0.15),
    height: moderateScale(33, 0.15),
    tintColor: "white"
  }
};
