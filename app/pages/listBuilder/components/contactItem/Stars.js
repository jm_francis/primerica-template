import React from "react";
import { View, Image } from "react-native";
import { STAR } from "constant-deprecated/Images";
import { shade } from "pages/listBuilder/configure";
const darkMode = true;
import { moderateScale } from "utilities/";

export default class Stars extends React.Component {
  render() {
    const { count = 1 } = this.props;
    const stars = [];
    for (let x = 0; x < count; x++) stars.push(<Star key={`star.${x}`} />);
    return <View style={Styles.container}>{stars}</View>;
  }
}

class Star extends React.Component {
  render() {
    return <Image source={STAR} style={Styles.starImage} />;
  }
}

const dimension = moderateScale(20, 0.23);
const Styles = {
  container: {
    flexDirection: "row",
    height: dimension,
    marginRight: 9
  },
  starImage: {
    width: dimension,
    height: dimension,
    marginHorizontal: 3,
    tintColor: shade(!darkMode, 0.94)
  }
};
