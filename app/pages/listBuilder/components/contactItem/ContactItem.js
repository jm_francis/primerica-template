//@ts-check
import Backend from "backend/";
import SendSMS from "react-native-sms";
import { Toasty } from "components/";
import { Fonts } from "constant-deprecated/";
import { customPageHandler } from "customPage/handlers/CustomPageHandler";
import { defaultLayoutAnimation, shade } from "pages/listBuilder/configure";
import React from "react";
import {
  Alert,
  Clipboard,
  Dimensions,
  LayoutAnimation,
  Platform,
  Text,
  TouchableWithoutFeedback,
  UIManager,
  View,
} from "react-native";
import ActionSheet from "react-native-actionsheet";
import Contacts from "react-native-contacts";
import Share from "react-native-share";
import { QuickStyle } from "react-native-shortcuts";
import { C, moderateScale } from "utilities/";
import ConnectButton from "./ConnectButton";
import DoneButton from "./DoneButton";
import EditButton from "./EditButton";
import MoveButton from "./MoveButton";
import PrioritySelector from "./PrioritySelector";
import RemoveButton from "./RemoveButton";
import Stars from "./Stars";
import { shareItemsToPrettyMessage } from "customPage/components/s-share";
const darkMode = true;

const unexpandFuncs = [];
const unexpandAll = () => {
  for (let x in unexpandFuncs) unexpandFuncs[x]();
};
const expandThisContact = (contact) => {
  for (let x in unexpandFuncs)
    if (unexpandFuncs[x]().recordID === contact.recordID)
      unexpandFuncs[x](true);
};

export default class ContactItem extends React.Component {
  state = {
    selected: false,
    expand: false,
  };

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({ selected: props.selected });
  }
  componentDidMount() {
    if (Platform.OS === "android")
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);

    unexpandFuncs.push(this.unexpand.bind(this));
    this.setState({ selected: this.props.selected });
  }

  unexpand(psych) {
    LayoutAnimation.configureNext(defaultLayoutAnimation);
    if (psych) this.setState({ expand: true });
    else this.setState({ expand: false });
    return this.props.data;
  }

  press() {
    const selected =
      this.state.selected === true
        ? true
        : this.state.expand === true
        ? true
        : false;
    if (this.props.onPress)
      this.props.onPress(
        this.props.data,
        selected,
        this.viewRef,
        this.itemLayout
      );
    if (this.props.selectable) this.setState({ selected: true });
    if (this.props.expandable) {
      unexpandAll();
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      this.setState({ expand: true });
      if (this.props.didExpand) this.props.didExpand(this.props.data);
    }
  }

  removePress() {
    if (this.props.outsider) return this.isOutsider();
    if (this.props.onRemovePress) this.props.onRemovePress(this.props.data);
  }
  donePress() {
    if (this.props.onDonePress) this.props.onDonePress(this.props.data);
    unexpandAll();
  }
  movePress() {
    if (this.props.outsider) return this.isOutsider();
    if (this.props.onMovePress) this.props.onMovePress(this.props.data);
  }
  edit() {
    const { data, listTitle } = this.props;
    if (this.props.outsider) return this.isOutsider();
    if (!data.recordID) {
      // new contact or pair with existing contact
      // this.actionSheetRef.show()
      this.newContact();
    } else {
      // edit
      Contacts.openExistingContact(data, (err, contact) => {
        if (err || !contact) return;
        Backend.listBuilderHandler.updateContactInList(contact, listTitle);
      });
    }
  }
  priorityChange(index) {
    if (this.props.outsider) return this.isOutsider();
    const { priorityDidChange, listTitle } = this.props;
    const data = {
      ...this.props.data,
      priority: index,
    };
    Backend.listBuilderHandler.updateContactInList(data, listTitle).then(() => {
      if (priorityDidChange) priorityDidChange(data, this.viewRef);
    });
  }

  actionSheetSelected(index) {
    if (index > 1) return;
    else if (index == 0) this.newContact();
    else if (index == 1) this.existingContact();
  }
  newContact() {
    const { data, listTitle } = this.props;
    Backend.phoneContactsHandler
      .createNewPhoneContact(data, listTitle)
      .then((contact) => {
        if (!contact) return;
        setTimeout(() => {
          expandThisContact(contact);
        }, 100);
      })
      .catch((error) => {
        Toasty({ text1: "Error creating phone contract (Err: CI<_>-cNPC)" });
      });
  }
  existingContact() {
    // pair with existing contact
    const { data, listTitle } = this.props;
    this.props.navigation.navigate("SelectSingleContact", {
      listTitle,
      onSelect: this.existingContactSelect.bind(this),
    });
  }
  existingContactSelect(contact) {
    Backend.listBuilderHandler
      .makeContactsOne(contact, this.props.data, this.props.listTitle)
      .then((contact) => {
        setTimeout(() => {
          expandThisContact(contact);
        }, 100);
      });
  }

  isOutsider() {
    Alert.alert("You must be the account holder to edit this info.");
    return false;
  }

  async connectShareMode(connectData) {
    // const contactData = this.props.data;
    const { givenName, phone, email } = connectData;
    const html = email ? true : false;
    const message = shareItemsToPrettyMessage(html);
    if (email) {
      Share.shareSingle({
        email,
        subject: "Primerica",
        message,
        social: "email",
      });
    } else if (phone) {
      SendSMS.send(
        {
          recipients: [phone],
          body: givenName ? message.replace(/🌀/g, givenName) : message,
        },
        (completed, cancelled, error) => {}
      );
    }
  }

  viewRef = React.createRef();

  render() {
    let rowCount = 4;

    const { data, selectable, shareMode } = this.props;
    const { selected, expand } = this.state;
    const firstName = data.givenName ? data.givenName : "";
    const lastName = data.familyName ? data.familyName : "";

    const shareModeCallback = shareMode
      ? this.connectShareMode.bind(this)
      : undefined;

    const phoneNumbers = [];
    const emails = [];
    for (let n in data.phoneNumbers) {
      const number = data.phoneNumbers[n].number;
      const label =
        data.phoneNumbers.length > 1 ? data.phoneNumbers[n].label : "";
      ++rowCount;
      phoneNumbers.push(
        <Row expand={expand} key={`connectButton.${number}`}>
          <ConnectButton
            label={label}
            givenName={data.givenName}
            familyName={data.familyName}
            phone={number}
            expand={expand}
            overridePress={shareModeCallback}
          />
        </Row>
      );
      if (!expand) break;
    }
    for (let n in data.emailAddresses) {
      const email = data.emailAddresses[n].email;
      const label =
        data.emailAddresses.length > 1 ? data.emailAddresses[n].label : "";
      ++rowCount;
      emails.push(
        <Row expand={expand} key={`connectButton.${email}`}>
          <ConnectButton
            label={label}
            email={email}
            expand={expand}
            overridePress={shareModeCallback}
          />
        </Row>
      );
      if (!expand) break;
    }

    const selectedContainer = selected === true ? Styles.selectedContainer : {};
    const unselectedContainer =
      selectable === true && !selected ? Styles.unselectedContainer : {};
    const expandedContainer =
      expand === true
        ? {
            ...Styles.expandedContainer,
            height: rowCount * (rowHeight + rowTopMargin) + 6,
          }
        : {};
    const expandedTopContainer =
      expand === true ? Styles.expandedTopContainer : {};
    const expandedName = expand === true ? Styles.expandedName : {};

    return [
      <TouchableWithoutFeedback onPress={this.press.bind(this)} key="touchable">
        <View
          style={{
            ...Styles.container,
            ...unselectedContainer,
            ...selectedContainer,
            ...expandedContainer,
          }}
          onLayout={(e) => (this.itemLayout = e.nativeEvent.layout)}
          ref={this.viewRef}
          // ref={(ref) => (this.viewRef = ref)}
        >
          <View style={{ ...Styles.topContainer, ...expandedTopContainer }}>
            <Text
              allowFontScaling={false}
              style={{ ...Styles.name, ...expandedName }}
            >{`${firstName} ${lastName}`}</Text>
            {expand ? (
              <EditButton
                onPress={this.edit.bind(this)}
                text={data.recordID ? "Edit" : "Add Info"}
              />
            ) : (
              <Stars count={data.priority} />
            )}
          </View>
          {phoneNumbers}
          {emails}
          {expand ? (
            <Row expand={expand}>
              <PrioritySelector
                defaultIndex={data.priority}
                onSelect={this.priorityChange.bind(this)}
                rowHeight={rowHeight}
                outsider={
                  this.props.outsider ? this.isOutsider.bind(this) : null
                }
              />
            </Row>
          ) : null}
          {expand ? (
            <View
              style={{
                flexDirection: "row",
                height: rowHeight * 2,
                marginTop: rowTopMargin,
                width: "100%",
                justifyContent: "space-between",
                alignItems: "space-between",
              }}
            >
              <RemoveButton
                onPress={this.removePress.bind(this)}
                rowHeight={rowHeight}
              />
              <View
                style={{
                  height: "100%",
                  minWidth: "53%",
                }}
              >
                {!shareMode ? (
                  <MoveButton
                    onPress={this.movePress.bind(this)}
                    rowHeight={rowHeight}
                  />
                ) : (
                  <View style={{ height: rowHeight / 2 }} />
                )}
                <View style={{ height: rowTopMargin, width: "100%" }} />
                <DoneButton
                  onPress={this.donePress.bind(this)}
                  rowHeight={rowHeight}
                />
              </View>
            </View>
          ) : null}
        </View>
      </TouchableWithoutFeedback>,
      <ActionSheet
        ref={(ref) => (this.actionSheetRef = ref)}
        key="actionsheet"
        options={["New Contact", "Use Existing Contact", "Cancel"]}
        cancelButtonIndex={2}
        onPress={this.actionSheetSelected.bind(this)}
      />,
    ];
  }
}

const modScale = 0.25;
const rowHeight = Platform.isPad ? 50 : 37;
const rowTopMargin = Platform.isPad ? 7 : 4;

class Row extends React.Component {
  render() {
    const { expand } = this.props;
    const expandStyle = expand
      ? {
          height: rowHeight,
        }
      : {};
    return (
      <View
        style={{
          maxHeight: rowHeight,
          marginTop: rowTopMargin,
          overflow: "hidden",
          ...expandStyle,
          ...this.props.style,
        }}
      >
        {this.props.children}
      </View>
    );
  }
}

const screenWidth = Dimensions.get("window").width;

export const Styles = {
  container: {
    overflow: "hidden",
    paddingHorizontal: 12,
    paddingVertical: 4,
    marginTop: 10,
    width: Platform.isPad ? screenWidth * 0.47 : "95%",
    height: Platform.isPad ? 130 : 82,
    alignSelf: "center",
    borderRadius: 7,
    // backgroundColor: darkMode ? shade(true, 0.71) : "rgb(235, 229, 198)",
    backgroundColor: C.surface01,
    ...QuickStyle.boxShadow,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  unselectedContainer: {
    // must be selectable
    backgroundColor: darkMode ? shade(true, 0.53) : "rgba(235, 229, 198, 0.7)",
  },
  selectedContainer: {
    borderWidth: 2,
    backgroundColor: darkMode ? shade(true, 0.71) : "rgb(235, 229, 198)",
    borderColor: "rgb(42, 205, 93)",
  },
  expandedContainer: {
    minHeight: 127,
  },
  topContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  expandedTopContainer: {
    marginBottom: 6,
  },
  name: {
    fontSize: moderateScale(20, modScale),
    fontFamily: Fonts.medium,
    color: shade(!darkMode, 0.93),
  },
  expandedName: {
    top: 1.8,
    fontFamily: Fonts.bold,
  },
};
