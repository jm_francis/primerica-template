import { Fonts } from "constant-deprecated";
import { EDIT_PERSON } from "constant-deprecated/Images";
import { shade } from "pages/listBuilder/configure";
import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { moderateScale } from "utilities/";

const darkMode = true;

export default class EditButton extends React.Component {
  render() {
    const { text = "Edit" } = this.props;
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={Styles.container}>
          <Text allowFontScaling={false} style={Styles.title}>
            {text}
          </Text>
          <Image source={EDIT_PERSON} style={Styles.icon} />
        </View>
      </TouchableOpacity>
    );
  }
}

const dimension = moderateScale(23, 0.2);
const Styles = {
  container: {
    marginTop: 2,
    flexDirection: "row",
    alignItems: "flex-end"
  },
  title: {
    color: shade(!darkMode),
    fontFamily: Fonts.medium,
    fontSize: moderateScale(16, 0.3),
    marginRight: 5
  },
  icon: {
    width: dimension,
    height: dimension
  }
};
