//@ts-check

import { PLUS_ICON, TRASH } from "app/constant-deprecated/Images";
import Backend from "backend/";
import { Buttoon, Kitten, sstyled, Txt } from "components/";
// import { Buttoon } from "components";
// import { Txt } from "components";
// import { Kitten } from "components";
import { Fonts } from "constant-deprecated";
import { shade } from "pages/listBuilder/configure";
import React from "react";
import { Dimensions, Image, Text, TouchableOpacity, View } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { QuickStyle } from "react-native-shortcuts";
import { C, moderateScale, verticalScale } from "utilities/";
import UserAvatar from "react-native-user-avatar";

const darkMode = true;

const { listBuilderHandler } = Backend;

/**
 * ###
 *  - LIstHeaderButton Contact Manager
 *  ----
 *  @example
 *  Copy and paste function is the best...
 *  ----
 *  @version 21.03.20
 *  -  *Brief changelog*
 *  @JRandNl
 *
 **/
export default class ListHeaderButton extends React.Component {
  press() {
    const { onPress, navigation, outsider, listData } = this.props;
    if (onPress) onPress(listData);
    if (navigation && !outsider) {
      navigation.navigate("ListScreen", { listData });
    } else if (navigation && outsider) {
      navigation.navigate("OutsiderListScreen", {
        outsider: outsider,
        listData: this.props.listData,
      });
    }
  }

  trashPress() {
    const { onDeletePress, listData } = this.props;
    if (onDeletePress) onDeletePress(listData);
  }

  render() {
    const { listData = {}, color, plus, editMode } = this.props;
    const { title } = listData;

    let container = {};
    if (title === listBuilderHandler._config.memoryJogger.title) {
      container = {
        backgroundColor: "rgb(214,22,29)",
      };
    }

    const contactCount = listData.contacts ? listData.contacts.length : 0;
    return (
      <Kitten.ListItem
        title={title ? title: plus ? <View style={Styles.customPlusContainer}><Image source={PLUS_ICON} style={Styles.plusIcon} /></View>: null}
        description={""}
        accessoryLeft={() => (
          <>
          {
            title ? <UserAvatar size={40} name={title} />: null 
          }
          
          </>
        )}
        accessoryRight={() => (
          <>
            {editMode ? (
              <Buttoon
                icon={{ name: "trash" }}
                status={"danger"}
                onPress={this.trashPress.bind(this)}
              />
            ) : (
              <Txt.H6>{title ? contactCount: null}</Txt.H6>
            )}
          </>
        )}
        onPress={this.press.bind(this)}
      />
    );
    // return (
    //   <View style={Styles.container}>
    //     <TouchableOpacity onPress={this.press.bind(this)}>
    //       <View
    //         style={{
    //           ...Styles.buttonContainer,
    //           ...(color ? { backgroundColor: color } : {}),
    //           ...container,
    //         }}
    //       >
    //         {plus ? <Image source={PLUS_ICON} style={Styles.plusIcon} /> : null}
    //         {title ? <Text style={Styles.title}>{title}</Text> : null}
    //         {plus || title == "Everyone" ? null : (
    //           <Text style={Styles.contactCount}>{`(${contactCount})`}</Text>
    //         )}
    //       </View>
    //     </TouchableOpacity>
    //     {editMode ? (
    //       <TouchableWithoutFeedback onPress={this.trashPress.bind(this)}>
    //         <View style={Styles.trashContainer}>
    //           <Image source={TRASH} style={Styles.trashIcon} />
    //         </View>
    //       </TouchableWithoutFeedback>
    //     ) : null}
    //   </View>
    // );
  }
}

const height = verticalScale(62);
const screenWidth = Dimensions.get("window").width;

const Styles = {
  container: {
    width: screenWidth,
    height: height + moderateScale(11, 0.13) * 2,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  customPlusContainer:{
    width: screenWidth * 0.95,
    alignSelf: "center",
    // ...QuickStyle.boxShadow,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonContainer: {
    flexDirection: "row",
    marginVertical: moderateScale(11, 0.13),
    width: screenWidth * 0.95,
    alignSelf: "center",
    height,
    borderRadius: 7,
    backgroundColor: darkMode ? "rgb(56, 53, 240)" : shade(darkMode, 0.79),
    ...QuickStyle.boxShadow,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: moderateScale(22.4, 0.4),
    color: darkMode ? "rgb(248, 248, 248)" : shade(!darkMode, 0.82),
    fontFamily: Fonts.bold,
    marginHorizontal: 12,
    textAlign: "center",
  },
  contactCount: {
    position: "absolute",
    right: 23,
    fontSize: moderateScale(17, 0.4),
    color: darkMode ? "rgb(248, 248, 248)" : shade(!darkMode, 0.82),
    fontFamily: Fonts.medium,
  },
  plusIcon: {
    tintColor: "white",
    width: moderateScale(26, 0.26),
    height: moderateScale(26, 0.26),
  },
  trashContainer: {
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
    marginRight: 70,
  },
  trashIcon: {
    tintColor: "white",
    width: moderateScale(30),
    height: moderateScale(30),
  },
};
