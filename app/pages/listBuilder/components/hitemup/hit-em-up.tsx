import React from "react";
import { Buttoon, Kitten, sstyled, Toasty, Txt } from "components/";
import Backend from "backend/";
import {
  Alert,
  Dimensions,
  NativeSyntheticEvent,
  ScrollView,
  TextInputSelectionChangeEventData,
  View,
} from "react-native";
import { C, moderateScale, spacing } from "utilities/";
import SendSMS from "react-native-sms";
import { NavigationStackScreenProps } from "react-navigation-stack";
import { shareItemsToPrettyMessage } from "customPage/handlers/CustomPageHandler";

/**
 * ### Section/page to send personalized messages to existing list
 * -  Similar to HitEmUp app
 * -
 * ---
 * @version 1.2.18 (aka feb 18th, 2021)
 * -  *Refactor*
 * -  *Document the file*
 * @author  NL
 *
 *
 */
export function HitEmUp(props: P) {
  const listData = props.navigation.getParam("listData");

  const [_title, setTitle] = React.useState(
    props.navigation.state.params.listData.title
  );

  const [_contacts, setContacts] = React.useState([]);

  React.useEffect(
    /**
     * Get contacts from custom contact list
     */
    function fetchContacts() {
      getContacts();
    },
    []
  );

  /**
   * Value contains message body
   */
  const [_messagebody, setBody] = React.useState(shareItemsToPrettyMessage());

  /**
   * Position of cusor in the input field
   */
  const [caret, setCaret] = React.useState({
    /**
     * Position of cusor in the input field
     */
    start: 0,
    end: 0,
  });

  /**
   * Use counter to get each contact details in the list
   */
  const counter = React.useRef(0);

  function handleSelect(
    e: NativeSyntheticEvent<TextInputSelectionChangeEventData>
  ) {
    setCaret({
      start: e.nativeEvent.selection.start,
      end: e.nativeEvent.selection.end,
    });
  }

  /** Calling to this function you can insert text in the cursor position */
  function insertText(text: string) {
    /**
     * Adds text to cusor position
     */
    setBody(
      _messagebody.substring(0, caret.start) +
        text +
        _messagebody.substring(caret.end, _messagebody.length)
    );
  }

  /**
   * Get contacts and set it to state
   */
  function getContacts() {
    Backend.firestoreHandler.account((account) => {
      const list = Backend.listBuilderHandler.getListById(listData.id);
      setContacts(list.contacts);
    });
  }

  /**
   *
   */
  const contactName =
    _contacts.length > 0 &&
    counter.current <= _contacts.length - 1 &&
    (_contacts[counter.current].displayName
      ? _contacts[counter.current].displayName
      : _contacts[counter.current].givenName);

  /**
   * This contains send message function
   * Skip and Cancel logic
   */
  function sendMessage() {
    if (counter.current === _contacts.length) {
      counter.current = 0;
      Toasty({
        text1: "Success",
        text2: "All messages has been sent",
        type: "success",
      });
      getContacts();
      return;
    }
    /**
     * searches through message and replace this 🌀 with the contact name
     */
    const messagesfinal = _messagebody.replace(
      /🌀/g,
      _contacts[counter.current].displayName
        ? _contacts[counter.current].displayName
        : _contacts[counter.current].givenName
    );

    if (_contacts[counter.current].phoneNumbers.length <= 0) {
      Toasty({
        type: "info",
        text1: `${contactName} does not have a phone number. Skipping...`,
      });
      // return;
      setContacts(
        _contacts
          .slice(0, counter.current)
          .concat(_contacts.slice(counter.current + 1, _contacts.length))
      );
    } else {
      SendSMS.send(
        {
          body: messagesfinal,

          recipients: [_contacts[counter.current].phoneNumbers[0].number],

          successTypes: ["sent", "queued"],
          allowAndroidSendWithoutReadPermission: true,
        },
        (completed, cancelled, error) => {
          if (completed) {
            setContacts(
              _contacts
                .slice(0, counter.current)
                .concat(_contacts.slice(counter.current + 1, _contacts.length))
            );

            counter.current = counter.current + 1;

            // console.log(counter.current);
            if (counter.current === _contacts.length) {
              // Alert.alert("All messages has been sent");
              Toasty({
                text1: "Success",
                text2: "All messages has been sent",
                type: "success",
              });
              counter.current = 0;
              getContacts();
              return;
            } else {
              sendMessage();
            }
          } else if (cancelled) {
            Alert.alert(
              "Cancel Message",
              "Would you like to skip or cancel",
              [
                {
                  text: "Skip",
                  onPress: () => {
                    setContacts(
                      _contacts
                        .slice(0, counter.current)
                        .concat(
                          _contacts.slice(counter.current + 1, _contacts.length)
                        )
                    );

                    counter.current = counter.current + 1;

                    sendMessage();
                  },
                },
                {
                  text: "Cancel",
                  onPress: () => {
                    getContacts();
                  },
                  style: "cancel",
                },
              ],
              { cancelable: false }
            );

            setContacts(_contacts.slice(counter.current));

            console.log("SMS Sent Cancelled");
          } else if (error) {
            Alert.alert("Some Error Occured");
          }
        }
      );
    }
  }

  return (
    <SS.Ctnr>
      <Kitten.TopNavigation
        title={_title}
        alignment="center"
        style={{ backgroundColor: C.background01 }}
        accessoryLeft={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              props.navigation.pop();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          />
        )}
      />
      <ScrollView keyboardShouldPersistTaps="always" keyboardDismissMode="none">
        <SS.CtnrContent>
          <Txt.$Title>Blast Message</Txt.$Title>
          <Kitten.Input
            autoFocus={true}
            multiline={true}
            textStyle={{ minHeight: 74 }}
            placeholder="Type your message..."
            defaultValue={_messagebody}
            onChangeText={setBody}
            onSelectionChange={(e) => handleSelect(e)}
            scrollEnabled={false}
          />
          <Txt.Indicator>Preset fields</Txt.Indicator>
          <Kitten.ButtonGroup
            size="small"
            appearance="outline"
            status="basic"
            style={{ alignSelf: "center" }}
          >
            <Kitten.Button onPress={() => insertText("🌀")}>
              {"🌀 Recipient's Name"}
            </Kitten.Button>
            {/* <Buttoon onPress={() => insertText("{name}")}>{`{phone}`}</Buttoon> */}
          </Kitten.ButtonGroup>
        </SS.CtnrContent>

        <View>
          <Buttoon
            status="success"
            style={{ width: "92%", marginTop: spacing(4) }}
            compact
            disabled={_contacts.length < 1 ? true : false}
            icon={{ name: "plane" }}
            onPress={() => sendMessage()}
          >
            {`Send ${_contacts.length} messages`}
          </Buttoon>
        </View>
        <View
          style={{
            width: "100%",
            height: spacing(8) * 2,
          }}
        />
      </ScrollView>
    </SS.Ctnr>
  );
}
// }

const SS = {
  Ctnr: sstyled(View)((p) => ({
    flex: 1,
    backgroundColor: p.C.background01,
  })),
  CtnrContent: sstyled(View)((p) => ({
    backgroundColor: p.C.background,
    paddingVertical: spacing(2),
    paddingHorizontal: spacing(4),
    justifyContent: "flex-start",
  })),
};

interface P extends NavigationStackScreenProps<dHitEmScrParams> {}

interface dHitEmScrParams {
  listData: Object; //TODO need to type this
  shareMode: boolean;
}
