import AlphaScrollFlatList from "alpha-scroll-flat-list";
import Backend, { API_Contact } from "backend/";
import { Buttoon, Kitten, Toasty } from "components/";
import { Fonts } from "constant-deprecated";
import { defaultLayoutAnimation, shade } from "pages/listBuilder/configure";
import React, { Component } from "react";
import {
  Keyboard,
  LayoutAnimation,
  ScrollView,
  Text,
  TextInput,
  TextInputProps,
  TouchableOpacity,
  View,
} from "react-native";
import { C, moderateScale, scale, spacing } from "utilities/";
import { ContactsHandler } from "../../../../backend/ContactsHandler/ContactsHandler";
import ContactCell from "./ContactCell";
import Contacts from "react-native-contacts";

const darkMode = true;

export default class ContactSearch extends React.Component {
  state = {
    allMyContacts: [],
    inputValue: "",
    listContacts: [],
  };

  clearInput() {
    if (!this.textInputRef) return;
    this.textInputRef.setNativeProps({ text: " " });
    this.textChange("");
    setTimeout(() => {
      this.textInputRef.setNativeProps({ text: "" });
    }, 5);
  }

  addNewContactPress() {
    const newPerson: Partial<Contacts.Contact> = {
      emailAddresses: [
        {
          label: "work",
          email: "",
        },
      ],
      familyName: "",
      givenName: "",
      phoneNumbers: [],
    };

    Backend.phoneContactsHandler
      .createNewPhoneContact(newPerson, this.props.listTitle)
      .then(() => {
        if (this.props.standalone && this.props.navigation)
          this.props.navigation.pop();
      });
  }

  contactPress(data) {
    const selected = this.isContactSelected(data.contact);
    const contact = data.contact;
    const { pickOneContact } = this.props;
    if (pickOneContact) pickOneContact(contact);
    else if (!selected) {
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      Backend.listBuilderHandler.addContactToList(
        contact,
        this.props.listTitle
      );
      setTimeout(this.clearInput.bind(this), 140);
    }
  }

  isContactSelected(someone: Contacts.Contact) {
    const { pickOneContact } = this.props;
    if (pickOneContact) return false;
    const listContacts = Backend.listBuilderHandler.extractContacts(
      this.props.listTitle
    );
    const fn = Backend.listBuilderHandler.fullNameOf;
    const check = (a) =>
      someone.recordID && a.recordID
        ? (a) => a.recordID === someone.recordID
        : (a) => fn(a) === fn(someone);

    console.log("listcontacts: " + listContacts.length);
    for (let l in listContacts) {
      console.log(listContacts[l].givenName + " vs " + someone.givenName);
      if (check(listContacts[l])) return true;
    }
    return false;
  }

  textChange(value = "") {
    const { onTextChange } = this.props;
    if (onTextChange) onTextChange(value, contacts);

    this.setState({ inputValue: value });
  }
  phoneContacts(contacts) {
    this.contacts = contacts;
    const allMyContacts = contacts
      .sort((a, b) => {
        if (a.givenName < b.givenName) return -1;
        if (a.givenName > b.givenName) return 1;
        return 0;
      })
      .filter((c) => (c.givenName && c.givenName.length > 0 ? true : false));
    this.setState({ allMyContacts });
  }

  myAccount(account) {
    const listContacts = Backend.listBuilderHandler.extractContacts(
      this.props.listTitle
    );
    if (!listContacts) return;
    this.setState({ listContacts });
  }
  componentDidMount() {
    Backend.firestoreHandler.account(this.myAccount.bind(this));
    Backend.phoneContactsHandler.phoneContacts(this.phoneContacts.bind(this));
  }

  render() {
    const { inputValue } = this.state;

    // const showTheseContacts = [];
    // const contacts = this.state.allMyContacts;

    // for (let c in contacts) {
    //   const contact = contacts[c];
    //   const givenName = contact.givenName ? contact.givenName : "";
    //   const familyName = contact.familyName ? contact.familyName : "";
    //   const words = inputValue.split(" ");
    //   for (let w in words) {
    //     const word = words[w].toLowerCase();
    //     if (words.length < 1) continue;
    //     if (
    //       inputValue.length < 1 ||
    //       givenName.toLowerCase().includes(word) ||
    //       familyName.toLowerCase().includes(word)
    //     ) {
    //       console.log("pushing " + givenName);
    //       !showTheseContacts.includes(contacts[c]) &&
    //         showTheseContacts.push(contacts[c]);
    //     }
    //   }
    // }
    // alert(JSON.stringify(showTheseContacts))

    return (
      <View style={Styles.container}>
        <$_SearchHeader onChangeText={this.textChange.bind(this)} />
        <View style={{ width: "100%", height: 10 }} />
        <$_ContactList {...this.props} searchValue={inputValue} />
        {/* <View style={{ width: "100%", height: "90%" }}>
          <AlphaScrollFlatList
            style={{ height: "100%" }}
            scrollBarColor="white"
            activeColor="#204ae3"
            ListHeaderComponent={
              <AddContactButton onPress={this.addNewContactPress.bind(this)} />
            }
            keyExtractor={(item) => Backend.listBuilderHandler.getId(item)}
            scrollKey={"givenName"}
            data={showTheseContacts}
            renderItem={({ item }) => (
              <ContactCell
                item={item}
                listContacts={this.state.listContacts}
                onSelect={this.contactPress.bind(this)}
              />
            )}
            itemHeight={moderateScale(38, 0.3)}
          />
        </View> */}
      </View>
    );
  }
}

interface PROPS_SearchBar extends TextInputProps {}
const $_SearchHeader = (props: PROPS_SearchBar) => {
  const { onChangeText } = props;
  const inputRef = React.useRef<TextInput>(null);
  return (
    <View style={Styles.inputContainer}>
      <TextInput
        {...props}
        style={Styles.input}
        // ref={(ref) => (this.textInputRef = ref)}
        ref={inputRef}
        placeholder="Search Phone Contacts"
        placeholderTextColor={shade(!darkMode, 0.64)}
        selectionColor={Styles.input.color}
        onChangeText={onChangeText}
        autoCapitalize="words"
        autoCorrect={false}
      />
    </View>
  );
};

class AddContactButton extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={Styles.addContactButton}>
          <Text style={Styles.addContactButtonTitle}>Add New Contact</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

function $_ContactList(props) {
  const { listTitle, navigation, searchValue } = props;
  const [contactList, setContactList] = React.useState<API_Contact[]>([]);
  function fetchContacts() {
    ContactsHandler.LoadContacts().then((r) => {
      const contacts: Contacts.Contact[] = r.contacts;
      setContactList(contacts);
      setContactList(
        contacts
          .filter((c) => c.givenName?.length > 0)
          .sort((c1, c2) => {
            if (c1.givenName.toUpperCase() > c2.givenName.toUpperCase())
              return 1;
            else if (c1.givenName.toUpperCase() < c2.givenName.toUpperCase())
              return -1;
            return 0;
          })
      );
    });
  }

  const words = searchValue
    .split(" ")
    .map((w) => w.toLowerCase())
    .filter((w) => w.length > 0);
  const _contactList = contactList
    .filter((c) => {
      if (words.length < 1) return true;
      for (let w in words) {
        const word = words[w];
        return (
          c.givenName.toLowerCase().includes(word) ||
          c.familyName.toLowerCase().includes(word)
        );
      }
    })
    .sort((c1, c2) => {
      if (
        `${c1.givenName && c1.givenName.toLowerCase()}${
          c2.familyName && c2.familyName.toLowerCase()
        }` === searchValue.toLowerCase().replace(/ /g, "")
      )
        return 1;
      return -1;
    });

  React.useEffect(fetchContacts, []);

  const newPerson: Partial<Contacts.Contact> = {
    emailAddresses: [
      {
        label: "work",
        email: "",
      },
    ],
    familyName: "",
    givenName: "",
    phoneNumbers: [],
  };

  return (
    <AlphaScrollFlatList
      onScroll={() => Keyboard.dismiss()}
      keyboardShouldPersistTaps="always"
      style={{ height: "100%" }}
      scrollBarColor="white"
      activeColor="#204ae3"
      ListHeaderComponent={() => (
        <Buttoon
          style={{
            marginHorizontal: -spacing(4),
          }}
          appearance="ghost"
          icon={{ name: "plus" }}
          onPress={() => {
            ContactsHandler.CreateContact(newPerson, listTitle, () => {
              fetchContacts();
              navigation.pop();
            });
          }}
        >
          Create Contact
        </Buttoon>
      )}
      keyExtractor={(item) => Backend.listBuilderHandler.getId(item)}
      scrollKey={"givenName"}
      data={_contactList}
      renderItem={({ item }: { item: API_Contact }) => (
        <>
          <ContactCell item={item} listContacts={_contactList} {...props} />
        </>
      )}
      itemHeight={moderateScale(38, 0.3)}
    />
  );
}

export const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background01,
    // backgroundColor: darkMode ? shade(true, 0.8) : "white",
  },
  inputContainer: {
    bottom: 0,
    width: "100%",
    height: moderateScale(50, 0.3),
    // backgroundColor: darkMode? 'rgb(33,33,33)' : shade(darkMode),
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    color: shade(!darkMode, 0.98),
    fontFamily: Fonts.medium,
    fontSize: moderateScale(18, 0.3),
    width: "90%",
    paddingHorizontal: 12,
    height: moderateScale(37, 0.3),
    // backgroundColor: shade(darkMode, 0.67),
    backgroundColor: C.surface01,
    borderRadius: 20,
  },
  addContactButton: {
    width: "80%",
    borderRadius: 7,
    alignSelf: "center",
    height: moderateScale(35, 0.3),
    backgroundColor: "rgb(230,230,230)",
    paddingHorizontal: 15,
    justifyContent: "center",
    alignItems: "center",
  },
  addContactButtonTitle: {
    color: darkMode ? "rgb(42,42,42)" : "rgb(15,15,15)",
    fontFamily: Fonts.bold,
    fontSize: moderateScale(17, 0.3),
  },
};
