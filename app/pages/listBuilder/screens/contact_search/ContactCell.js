import Backend from "backend/";
import { Colors, Fonts } from "constant-deprecated";
import { CHECK, PLUS_ICON } from "constant-deprecated/Images";
import React, { PureComponent } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { C, moderateScale } from "utilities/";
const darkMode = true;

export default class ContactCell extends PureComponent {
  state = {
    forceCheck: false,
  };

  // contact items
  fullName(useThis) {
    const contact = useThis ? useThis : this.props.item;
    return (
      contact.givenName + (contact.familyName ? " " + contact.familyName : "")
    );
  }
  onSelect() {
    const { item, listTitle } = this.props;
    const listContacts = Backend.listBuilderHandler.extractList(listTitle)
      .contacts;
    for (let c in listContacts) {
      const contact = listContacts[c];
      if (
        Backend.listBuilderHandler.getId(contact) ===
        Backend.listBuilderHandler.getId(item)
      )
        return;
    }
    Backend.listBuilderHandler.addContactToList(item, listTitle).then(() => {
      this.setState({ forceCheck: true });
    });
  }
  render() {
    const { forceCheck } = this.state;
    const { inputValue = "", item, listTitle } = this.props;
    const listContacts = Backend.listBuilderHandler.extractList(listTitle)
      .contacts;
    const name = this.fullName();
    const show =
      inputValue.length < 1 ||
      name.toLowerCase().includes(this.props.inputValue.toLowerCase());

    let checked = false;
    for (let c in listContacts)
      if (
        Backend.listBuilderHandler.getId(listContacts[c]) ===
        Backend.listBuilderHandler.getId(this.props.item)
      ) checked = true;
    if (forceCheck) checked = true;

    return show ? (
      <TouchableOpacity
        onPress={this.onSelect.bind(this)}
        key={`cell.${item.recordID}`}
      >
        <View style={Styles.container}>
          <View style={Styles.content}>
            <Text style={Styles.text}>{name}</Text>
            {checked ? (
              <Image style={Styles.checkImage} source={CHECK} />
            ) : null}
            {!checked ? (
              <Image style={Styles.plusImage} source={PLUS_ICON} />
            ) : null}
          </View>

          {!this.props.isLast ? <View style={Styles.line} /> : null}
        </View>
      </TouchableOpacity>
    ) : null;
  }
}

// WARNING: Height (and pad scaling) must stay exactly the same as in ContactSearch

const Styles = StyleSheet.create({
  container: {
    // backgroundColor: darkMode ? "rgb(50,50,50)" : "white",
    backgroundColor: C.surface01,
    height: moderateScale(38, 0.3),
    justifyContent: "center",
  },
  content: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 8,
    paddingRight: 35,
  },
  checkImage: {
    tintColor: darkMode ? "white" : "#30db61",
    width: moderateScale(17, 0.3),
    height: moderateScale(17, 0.3),
  },
  plusImage: {
    width: moderateScale(17, 0.3),
    height: moderateScale(17, 0.3),
    tintColor: "white",
    // tintColor: darkMode ? "rgb(245,245,245)" : "grey",
  },
  text: {
    color: darkMode ? "white" : Colors.black,
    fontSize: moderateScale(17, 0.3),
    fontFamily: Fonts.medium,
  },
  line: {
    width: "90%",
    left: 10,
    bottom: 0,
    position: "absolute",
    backgroundColor: darkMode ? "rgb(245,245,245)" : "rgb(180,180,180)",
    height: moderateScale(0.6, 0.3),
  },
});
