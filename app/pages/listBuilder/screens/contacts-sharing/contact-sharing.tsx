import Backend from "backend/";
import {
  Buttoon,
  dAccessory,
  IconPrimr,
  Kitten,
  Toasty,
  Txt,
} from "components/";
import R from "ramda";
import * as React from "react";
import { TextStyle, View, ViewStyle } from "react-native";
import { Modalize } from "react-native-modalize";
import {
  C,
  DEVICE_HEIGHT,
  moderateScale,
  primrConfig,
  scale,
  spacing,
} from "utilities/";

/**
 * ### Prompt user to share their account information with another user!
 *
 * @author jm_francis
 * @version 1.1.20
 *
 * @example
 * <SHEET_ContactSharing onRef={shareRef} />
 * shareRef.current.open()
 */
export function SHEET_ContactSharing(props: {
  onRef: any;
  promptTitle?: string;
  hideXButton?: boolean;
}) {
  const {
    onRef,
    promptTitle = "Share Your Account",
    hideXButton = false,
  } = props;
  return (
    <Modalize
      {...props}
      scrollViewProps={{
        keyboardShouldPersistTaps: "always",
      }}
      ref={onRef}
      modalHeight={DEVICE_HEIGHT * 0.84}
      modalStyle={{
        backgroundColor: "#222B45", //* color-basic-800
        overflow: "hidden",
      }}
      disableScrollIfPossible={true}
      // adjustToContentHeight={true}
      HeaderComponent={
        <Kitten.TopNavigation
          alignment="center"
          title={promptTitle}
          accessoryLeft={() => (
            <Kitten.TopNavigationAction
              onPress={() => {
                onRef.current.close();
              }}
              icon={(props: dAccessory) =>
                hideXButton ? null : (
                  <IconPrimr
                    name="x"
                    size={props.style.width}
                    color={props.style.tintColor}
                  />
                )
              }
            />
          )}
        />
      }
    >
      <Txt
        category="p2"
        style={{
          ...SS().CAPTION_TXT,
          fontSize: moderateScale(13),
          marginHorizontal: spacing(4),
        }}
      >
        {
          "Your profile will be shared with the selected users including your contact lists."
        }
      </Txt>
      <$_ContactSharing
        {...props}
        onSelectPress={() => onRef.current.close()}
      />
    </Modalize>
  );
}

/**
 * Showing list of admin checkbox that user select
 * to share their contacts with
 * @param props
 */
const $_ContactSharing = (props) => {
  const { onSelectPress } = props;
  const [_users, setUsers] = React.useState([]);
  const [_chooseUsers, chooseUsers] = React.useState([]);
  const [_searchValue, setSearchValue] = React.useState("");

  React.useEffect(function fetchShareTo() {
    const shareTo = Backend.firestoreHandler._account.listBuilder?.shareTo;
    chooseUsers(shareTo ? shareTo : []);
    Backend.adminHandler.users((users) => setUsers(users));
  }, []);

  /**
   * Fn to check/uncheck
   */
  const didAnySelect = React.useRef(false);
  const onSelect = async (userID: string) => {
    const selectedUser = Backend.adminHandler.getUserByUid(userID);
    if (!_chooseUsers) return;
    const unshare = _chooseUsers.includes(userID);
    didAnySelect.current = true;
    const shareTo = _chooseUsers.includes(userID)
      ? _chooseUsers.filter((id) => id != userID)
      : [..._chooseUsers, userID];

    chooseUsers(shareTo);
    try {
      await Backend.firestoreHandler.updateAccount("listBuilder", { shareTo });
      Toasty({
        text1: `Profile ${unshare ? "unshare" : "share"} complete.`,
        text2: `Your profile has been ${unshare ? "unshared" : "shared"} with ${
          selectedUser ? selectedUser.name : "?"
        }`,
      });
    } catch (error) {
      Toasty({
        type: "error",
        text1: `Profile ${unshare ? "unshare" : "share"} failed.`,
        text2: `Try to ${unshare ? "unshared" : "shared"} you profile with ${
          selectedUser?.name
        } again.`,
      });
    }
  };

  /**
   * When the search value changes do this...
   */
  function searchValueChange(value: string) {
    const users = Backend.adminHandler._users;
    setSearchValue(value);
    const _users = [];
    for (let u in users) {
      const user = users[u];
      if (value.length < 1) _users.push(user);
      else if (user?.name?.toLowerCase().includes(value.toLowerCase()))
        _users.push(user);
    }
    setUsers(_users);
  }
  return (
    <>
      <Kitten.Layout style={{ paddingHorizontal: scale(15) }}>
        <Kitten.Input
          value={_searchValue}
          onChangeText={searchValueChange}
          placeholder="Search someone..."
        />
        {/* <Buttoon
          style={{
            marginVertical: spacing(1),
          }}
          disabled={didAnySelect.current === false}
          progress={true}
          onPress={(xong) => {
            onSharePress(xong);
          }}
        >
          Share
        </Buttoon> */}
        {/* <Kitten.Text>{JSON.stringify(_chosenAdmins)}</Kitten.Text> */}
        {_users.map((user, index) => (
          <Kitten.CheckBox
            style={SS().OPTION}
            checked={_chooseUsers && _chooseUsers.includes(user.uid)}
            onChange={() => {
              onSelect(user.uid);
            }}
          >
            {() => (
              <View style={{ paddingLeft: 10 }}>
                <Kitten.Text
                  category="p1"
                  style={{
                    fontFamily: primrConfig.strict["special-font-family"],
                  }}
                >
                  {R.isNil(user.name) ? "Unknown" : user.name}
                </Kitten.Text>
                <Kitten.Text category="c1"> {user.email}</Kitten.Text>
              </View>
            )}
          </Kitten.CheckBox>
        ))}

        {/* <View
          style={{
            paddingVertical: scale(15),
          }}
        >
          <Buttoon
            disabled={_chooseUsers.length == 0}
            progress={true}
            onPress={(xong) => {
              onSharePress(xong);
            }}
          >
            Share
          </Buttoon>
        </View> */}
      </Kitten.Layout>
    </>
  );
};

const SS = () => ({
  OPTION: {
    marginVertical: 4,
    marginHorizontal: 12,
  } as ViewStyle,
  CAPTION_TXT: {
    textAlign: "center",
    color: C.text01,
    marginVertical: scale(10),
  } as TextStyle,
});
