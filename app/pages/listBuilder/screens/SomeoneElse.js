// @ts-check
// TODO outsider person connection
import Backend from "backend/";
import XButton from "navigation/components/XButton";
import React, { Component } from "react";
import { View } from "react-native";
import { C } from "utilities/";
import ListBuilderListsMenu from "../ListBuilderListsMenu";

export default class SomeoneElse extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <XButton
        onPress={() => {
          // contactListHandler.closeOutsiderAccountConnection()
        }}
        navigation={navigation}
        style={{ tintColor: "white" }}
      />
    ),
  });

  state = {
    account: null,
  };

  outsiderAccount(account) {
    this.setState({ account });
  }

  componentDidMount() {
    Backend.listBuilderHandler.outsiderAccount(this.outsiderAccount.bind(this));
  }
  componentWillUnmount() {
    Backend.listBuilderHandler.closeOutsiderAccountConnection();
  }

  render() {
    const { account } = this.state;
    const { navigation } = this.props;
    return (
      <View style={Styles.container}>
        {account ? (
          <ListBuilderListsMenu outsider={account} navigation={navigation} />
        ) : null}
      </View>
    );
  }
}

const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background01,
  },
};
