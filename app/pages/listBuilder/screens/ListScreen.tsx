import { TopNavigation } from "@ui-kitten/components";
import Backend from "backend/";
import { Buttoon, DirectCoverModal } from "components/";
import K, { Config, Fonts } from "constant-deprecated/";
import { LEFT_ARROW } from "constant-deprecated/Images";
import { customPageHandler } from "customPage/handlers/CustomPageHandler";
import { ledger } from "memoryJogger/Ledger";
import PlusButton from "navigation/components/PlusButton";
import { ContactItem } from "pages/listBuilder/components";
import { Styles as ContactItemStyles } from "pages/listBuilder/components/contactItem/ContactItem";
import { defaultLayoutAnimation, shade } from "pages/listBuilder/configure";
import { shareColor } from "pages/listBuilder/ShareToList";
import React from "react";
import {
  Alert,
  Dimensions,
  findNodeHandle,
  Image,
  LayoutAnimation,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  UIManager,
  View,
} from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import ReactNativeHapticFeedback from "react-native-haptic-feedback";
import {
  C,
  getBottomSpace,
  getStatusBarHeight,
  isIphoneX,
  moderateScale,
  spacing,
} from "utilities/";

const darkMode = true;

// const _getStatusBarHeight = () => (isIphoneX() ? getStatusBarHeight() : 7);
const _getStatusBarHeight = () => getStatusBarHeight("safe", "ios-only");
const _getBottomSpace = () => (isIphoneX() ? getBottomSpace() : 17);
const hapticOptions = {
  enableVibrateFallback: false,
  ignoreAndroidSystemSettings: false,
};

const screenWidth = Dimensions.get("window").width;

const fn = Backend.listBuilderHandler.fullNameOf;

let _plusPressHolder = {};

export default class ListScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const shareMode = navigation.getParam("shareMode", false);
    const listData = navigation.getParam("listData", {});
    const title = listData.title;
    let headerColor = "#1239c7";
    if (shareMode) headerColor = shareColor;
    else if (title === "Build My List" || title === "Memory Jogger")
      headerColor = "rgb(216, 14, 14)";
    return {
      // headerTitle: () => (
      //   <Title
      //     style={{
      //       position: "absolute",
      //       bottom: spacing(2),
      //     }}
      //   >
      //     {title}
      //   </Title>
      // ),
      // headerStyle: {
      //   backgroundColor: headerColor,
      //   ...(Platform.isPad
      //     ? { height: 100 }
      //     : {
      //         height:
      //           getStatusBarHeight("safe") +
      //           (Platform.OS === "android" ? 27 : 0),
      //       }),
      // },
      // headerLeft: () => (
      //   <BackButton
      //     style={{
      //       tintColor: darkMode ? "white" : "rgb(13,13,13)",
      //       marginTop: spacing(1) + 7,
      //       marginLeft: spacing(2),
      //     }}
      //     onPress={() => {
      //       // NOTE: old workaround
      //       let popCount = navigation.state.params.popCount;
      //       popCount = popCount && popCount > 0 ? popCount : 1;
      //       for (let x = 0; x < popCount; x++) navigation.pop();
      //     }}
      //   />
      // ),
      // headerRight: () =>
      //   outsider ? null : (
      //     <PlusButton
      //       onPress={_plusPress}
      //       style={{
      //         marginTop: spacing(1) + 7,
      //         marginLeft: spacing(2),
      //         tintColor: shade(!darkMode),
      //       }}
      //     />
      //   ),
    };
  };

  state = {
    contacts: [],
    directCoverModal: { visible: false },
    showNewbieButton: false,
  };

  newContactCreated() {
    // do something? or nah...
  }
  addContactPress() {
    const { listData } = this.props.navigation.state.params;
    this.props.navigation.navigate("StandaloneContactSearch", {
      listTitle: listData.title,
      onNewContactCreated: this.newContactCreated.bind(this),
    });
  }

  componentDidMount() {
    const { listData } = this.props.navigation.state.params;

    if (Platform.OS === "android")
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);

    _plusPressHolder["callback"] = this.addContactPress.bind(this);
    Backend.firestoreHandler.account(this.myAccount.bind(this));

    this.showAddContactsInstructions();

    const shareMode = this.props.navigation.getParam("shareMode", false);
    if (
      listData.title === "Memory Jogger" ||
      (listData.title === "Build My List" && !shareMode)
    ) {
      for (let k in Config.newPeoplePageKeywords) {
        const keyword = Config.newPeoplePageKeywords[k];
        if (
          customPageHandler.isPageActive(keyword) &&
          customPageHandler.presentedPages.length > 0
        ) {
          if (Backend.levelsHandler.hasCompletedLevels()) break;
          this.setState({ showNewbieButton: true });
          break;
        }
      }
    }
  }
  myAccount(_account) {
    const outsider = this.props.navigation.getParam("outsider");
    const account = outsider ? outsider.listBuilder : _account.listBuilder;
    const listData = this.props.navigation.getParam("listData");
    const shareMode = this.props.navigation.getParam("shareMode");

    let theList = null;
    for (let l in account.lists) {
      const list = account.lists[l];
      if (list.id === listData.id) {
        theList = list;
        break;
      }
    }
    if (!theList) {
      console.log("NO LIST");
      return; //should never happen
    }

    let listContacts;
    if (
      listData.title === "Memory Jogger" ||
      listData.title === "Build My List"
    ) {
      listContacts = [];
      const contactGroups = ledger.organizeContactsIntoGroups(theList.contacts);
      for (g in contactGroups) {
        const group = contactGroups[g];
        listContacts.push({ group });
        listContacts = listContacts.concat(group.contacts);
      }
    } else {
      listContacts = theList.contacts;
    }

    let contacts = [];
    for (i in listContacts) {
      const itemData = listContacts[i];
      if (!itemData.recordID && itemData.group) {
        contacts.push({
          component: <Group data={itemData.group} key={`group.${i}`} />,
        });
        continue;
      }
      contacts.push({
        component: (
          <ContactItem
            key={`list.contactItem.${Backend.listBuilderHandler.getId(
              itemData
            )}.${fn(itemData)}`}
            outsider={outsider}
            shareMode={shareMode}
            navigation={this.props.navigation}
            onPress={this.contactItemPressed.bind(this)}
            listTitle={theList.title}
            data={itemData}
            onRemovePress={this.removePress.bind(this)}
            onDonePress={this.donePress.bind(this)}
            onMovePress={this.movePress.bind(this)}
            priorityDidChange={this.contactItemPriorityChange.bind(this)}
            expandable
          />
        ),
        data: itemData,
      });
    }

    // sort by star rating
    contacts = contacts.sort((a, b) => {
      if (!a.data || !b.data) return 1;
      const p1 = a.data.priority ? a.data.priority : 0;
      const p2 = b.data.priority ? b.data.priority : 0;
      if (p1 < p2) return 1;
      if (p1 > p2) return -1;
      return 0;
    });

    if (this.state.contacts.length > 0)
      LayoutAnimation.configureNext(defaultLayoutAnimation);
    this.setState({ contacts: contacts.map((item) => item.component) });
  }
  contactItemPressed(contact, selected, viewRef, itemLayout) {
    ReactNativeHapticFeedback.trigger("impactLight", hapticOptions);
    this.scrollToItem(viewRef, itemLayout);
  }

  removePress(contact) {
    ReactNativeHapticFeedback.trigger("impactLight", hapticOptions);
    const { listData } = this.props.navigation.state.params;
    Backend.listBuilderHandler.removeContactFromList(contact, listData.title);
  }
  donePress() {
    ReactNativeHapticFeedback.trigger("impactLight", hapticOptions);
  }
  movePress(contact) {
    this.props.navigation.navigate("PickAList", {
      onSelect: (list) => {
        this.moveContactToList(contact, list);
      },
    });
  }
  moveContactToList(contact, list) {
    const { listData } = this.props.navigation.state.params;
    Backend.listBuilderHandler.moveContactFromListToList(
      contact,
      Backend.listBuilderHandler.extractList(listData.title),
      list
    );
  }

  contactItemPriorityChange(viewRef) {
    this.scrollToItem(viewRef);
  }

  scrollToItem(viewRef, itemLayout) {
    // console.log("viewRef:" + JSON.stringify(viewRef));
    // if (!viewRef || !this.scrollViewRef) return;
    if (!this.scrollViewRef) return;
    let toY = itemLayout.y - ContactItemStyles.expandedContainer.minHeight * 2;
    toY = toY < 0 ? 0 : toY;
    this.scrollViewRef.scrollTo({ x: 0, y: toY, animated: true });
    // viewRef.measureLayout(findNodeHandle(this.scrollViewRef), (x, y) => {
    // let toY = y - ContactItemStyles.expandedContainer.minHeight * 2;
    // toY = toY < 0 ? 0 : toY;
    // this.scrollViewRef.scrollTo({ x: 0, y: toY, animated: true });
    // });
  }

  retakeExercise() {
    const { listData } = this.props.navigation.state.params;
    this.props.navigation.pop();
    this.props.navigation.navigate("MemoryJogger", {
      retake: listData,
    });
  }
  hitEmUp() {
    // const {
    //   navigation,
    //   // theme: { C },
    // } = this.props;
    // console.log(navigation);
    // navigation.navigate("HitemUp");

    const shareMode = this.props.navigation.getParam("shareMode", false);
    const listData = this.props.navigation.getParam("listData");

    this.props.navigation.navigate("HitemUp", {
      listData,
      shareMode: true,
    });
  }
  async showAddContactsInstructions() {
    const didOpen = await Backend.localHandler.getBuildMyListDidOpen();
    if (didOpen === true) return;

    this.setState({
      directCoverModal: {
        visible: true,
        text: "You can add more contacts by tapping here.",
        doneTitle: "OK",
        donePress: this.showRetakeExerciseInstructions.bind(this),
        startY: 0,
        endY: 120,
        // startY:
        //   _getStatusBarHeight() + 12 + (Platform.OS === "android" ? -14 : 0),
        // endY: _getStatusBarHeight() + (Platform.isPad ? 70 : 60),
        startX: screenWidth - (Platform.isPad ? 90 : 80),
        endX: screenWidth,
      },
    });
  }
  showRetakeExerciseInstructions() {
    if (!this.retakeButtonRef) {
      this.setState({ directCoverModal: { visible: false } });
      Backend.localHandler.setBuildMyListDidOpen();
      return;
    }
    this.retakeButtonRef.measure((x, y, width, height, pageX, pageY) => {
      this.setState({
        directCoverModal: {
          visible: true,
          text: "You can also add more contacts by retaking the exercise.",
          doneTitle: "OK",
          donePress: () => {
            this.setState({ directCoverModal: { visible: false } });
            Backend.localHandler.setBuildMyListDidOpen();
          },
          startY: pageY,
          endY: pageY + height,
        },
      });
    });
  }

  render() {
    const shareMode = this.props.navigation.getParam("shareMode", false);
    const listData = this.props.navigation.getParam("listData");
    const outsider = this.props.navigation.getParam("outsider");
    const { contacts, directCoverModal, showNewbieButton } = this.state;

    return (
      <>
        <View style={{ flex: 1 }}>
          <TopNavigation
            title={listData.title}
            alignment="center"
            style={{ backgroundColor: C.background01 }}
            accessoryLeft={() => (
              <Buttoon
                style={{
                  backgroundColor: "transparent",
                  borderColor: "transparent",
                }}
                onPress={() => {
                  this.props.navigation.pop();
                }}
                icon={{
                  name: "arrow_left",
                  color: "white",
                  size: moderateScale(20),
                }}
              />
            )}
            accessoryRight={() =>
              outsider ? null : (
                <PlusButton
                  onPress={this.addContactPress.bind(this)}
                  style={{
                    marginTop: spacing(1) + 7,
                    marginLeft: spacing(2),
                    tintColor: shade(!darkMode),
                  }}
                />
              )
            }
          />

          <ScrollView
            style={Styles.container}
            ref={(ref) => (this.scrollViewRef = ref)}
          >
            <View style={{ width: "100%", height: Platform.isPad ? 20 : 0 }} />
            <Buttoon
              status="success"
              style={{ width: "92%", marginTop: spacing(4) }}
              compact
              icon={{ name: "bullhorn" }}
              onPress={this.hitEmUp.bind(this)}
            >
              Blast
            </Buttoon>
            {listData.title ===
              Backend.listBuilderHandler._config.memoryJogger.title &&
            !shareMode ? (
              <>
                <Button
                  onPress={this.retakeExercise.bind(this)}
                  viewRef={(ref) => (this.retakeButtonRef = ref)}
                >
                  Retake the Exercise
                </Button>
              </>
            ) : null}

            {contacts.length > 0 ? (
              <View style={Styles.contactsContainer}>{contacts}</View>
            ) : (
              <View style={Styles.noContactsContainer}>
                <Text style={Styles.noContactsText}>No Contacts Added</Text>
              </View>
            )}

            <View
              style={{
                width: "100%",
                height:
                  27 +
                  (showNewbieButton ? Styles.newbieButtonContainer.height : 0),
              }}
            />
          </ScrollView>

          {showNewbieButton ? (
            <NewbieButton navigation={this.props.navigation} />
          ) : null}

          <DirectCoverModal data={directCoverModal} />
        </View>
      </>
    );
  }
}

function Group(props) {
  const { data } = props; // data = the group object
  const { name, requiredPriorities } = data;

  const descriptions = [];
  for (let x in requiredPriorities) {
    const text =
      K.MemoryJoggerConfig.priorityTypes[requiredPriorities[x]].message;
    descriptions.push(
      <Text key={`desc.${x}`} style={Styles.groupDescription}>
        {text}
      </Text>
    );
  }

  return (
    <View style={Styles.groupContainer}>
      <Text style={Styles.groupTitle}>{name}</Text>
      {descriptions}
    </View>
  );
}
function Button(props) {
  const { onPress, viewRef } = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={Styles.topButton}
        ref={(ref) => {
          if (viewRef) viewRef(ref);
        }}
      >
        <Text style={Styles.topButtonText}>{props.children}</Text>
      </View>
    </TouchableOpacity>
  );
}
function NewbieButton(props) {
  const { navigation } = props;
  return (
    <View style={Styles.newbieButtonContainer}>
      <TouchableWithoutFeedback
        style={Styles.newbieButton}
        onPress={() => {
          navigation.pop();
        }}
      >
        <View style={Styles.newbieButton}>
          <Image source={LEFT_ARROW} style={Styles.newbieButtonArrowIcon} />
          <Text style={Styles.newbieButtonTitle}>Continue Training</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

const screenHeight = Dimensions.get("window").height;
const Styles = {
  container: {
    flex: 1,
    // backgroundColor: shade(darkMode),
    backgroundColor: C.background01,
  },
  contactsContainer: {
    width: "100%",
    flexDirection: Platform.isPad ? "row" : undefined,
    flexWrap: Platform.isPad ? "wrap" : undefined,
    justifyContent: "space-evenly",
  },
  overlay: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  noContactsContainer: {
    width: "100%",
    height: screenHeight * 0.8,
    justifyContent: "center",
    alignItems: "center",
  },
  noContactsText: {
    color: darkMode ? "rgb(245,245,245)" : shade(!darkMode, 0.7),
    fontFamily: Fonts.medium,
    textAlign: "center",
    fontSize: moderateScale(22, 0.4),
  },
  topButton: {
    marginTop: 15,
    marginBottom: 5,
    alignSelf: "center",
    width: "90%",
    borderRadius: 7,
    borderWidth: 1,
    borderColor: "white",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(240,240,240,0.2)",
  },
  topButtonText: {
    fontFamily: Fonts.medium,
    fontSize: moderateScale(20, 0.45),
    color: "white",
  },

  groupContainer: {
    ...(Platform.isPad ? { width: screenWidth } : {}),
    marginTop: 27,
    marginBottom: 8,
    marginLeft: 12,
  },
  groupTitle: {
    color: "white",
    fontFamily: Fonts.bold,
    fontSize: moderateScale(24),
  },
  groupDescription: {
    color: "white",
    fontFamily: Fonts.medium,
    fontSize: moderateScale(15),
    marginLeft: 2,
  },

  newbieButtonContainer: {
    position: "absolute",
    bottom: 0,
    width: screenWidth,
    height: moderateScale(50 + _getBottomSpace()),
    backgroundColor: "rgb(27,73,160)",
  },
  newbieButton: {
    marginTop: 6,
    width: screenWidth,
    height: moderateScale(50),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  newbieButtonTitle: {
    color: "white",
    fontSize: moderateScale(25),
    fontFamily: Fonts.medium,
    textAlign: "center",
  },
  newbieButtonArrowIcon: {
    tintColor: "white",
    width: moderateScale(22),
    height: moderateScale(22),
    marginLeft: -20,
    marginRight: 20,
  },
};
