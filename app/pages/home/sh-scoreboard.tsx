import Backend from "backend/";
import { dScoreboard } from "backend/ScoreboardHandler/scoreboard-handler.props";
import { IconPrimr, Kitten, Toasty, Txt } from "components/";
import { defaultLayoutAnimation } from "pages/listBuilder/configure";
import React from "react";
import {
  Alert,
  Keyboard,
  LayoutAnimation,
  Platform,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import ActionSheet from "react-native-actionsheet";
import { Modalize } from "react-native-modalize";
import { C, DEVICE_HEIGHT, moderateScale, scale, spacing } from "utilities/";

interface P {
  scoreboard: {
    position: number;
    people: { name: string; score: number }[];
    title: string;
    id: string;
  };
}
/**
 * Sheet that show given scoreboard info
 *  - U can view score of members of given scoreboard
 *  - U == admin:
 *    - U can edit member's score
 *
 * ---
 * @version 0.10.29
 *
 */
export const SH_Scoreboard = React.forwardRef<Modalize, P>((props: P, ref) => {
  function onClose() {
    ref.current.close();
  }

  return (
    <Modalize
      ref={ref}
      scrollViewProps={{
        keyboardShouldPersistTaps: "always",
      }}
      modalHeight={DEVICE_HEIGHT * 0.85}
      modalStyle={{
        backgroundColor: C.surface01,
        padding: spacing(3),
        borderWidth: 1,
        borderColor: C["color-primary-500"],
        borderBottomWidth: 0,
      }}
      overlayStyle={{ backgroundColor: "#00000098" }}
      withHandle={true}
      HeaderComponent={<$_Header {...props} onCloseSH={onClose} />}
      FooterComponent={
        <View style={{ position: "absolute", bottom: 20, left: 0, right: 0 }}>
          <$_Footer {...props} onCloseSH={onClose} />
        </View>
      }
    >
      <View>
        <$_ScoreList {...props} />
      </View>
    </Modalize>
  );
});

interface dHeader extends P {
  onCloseSH?(): void;
}
function $_Header(props: dHeader) {
  const { scoreboard, onCloseSH } = props;
  const [isMenuVisible, setMenuVisible] = React.useState(false);
  const [isTitleEditable, shouldTitleEditable] = React.useState(false);
  const [isSubtitleEditable, shouldSubtitleEditable] = React.useState(false);
  const [_boardTitle, setBoardTitle] = React.useState(scoreboard?.title);
  const [_boardSubtitle, setBoardSubtitle] = React.useState(
    scoreboard?.subtitle
  );
  const [_account, setAccount] = React.useState(null);

  React.useEffect(function fetchScoreboard() {
    Backend.firestoreHandler.account(setAccount);
  }, []);

  let isAdmin = !!_account && _account.admin;

  const refTitle = React.useRef<TextInput>();
  const refSubtitle = React.useRef<TextInput>();
  function RenderHeader() {
    return (
      <TouchableOpacity
        onPress={() => {
          setMenuVisible(true);
        }}
        disabled={!isAdmin}
      >
        {/* <Txt category="p1">{!!scoreboard && JSON.stringify(scoreboard)}</Txt> */}
        {/* <Txt category="h6" style={{textAlign: "center"}}>{scoreboard?.title}</Txt> */}
        <View>
          <TextInput
            ref={refTitle}
            editable={isTitleEditable}
            value={_boardTitle}
            onChangeText={setBoardTitle}
            onSubmitEditing={setNewTitle}
            placeholder={scoreboard?.title}
            style={{
              alignSelf: "center",
              fontSize: scale(20),
              textAlign: "center",
              color: C.text01,
              fontWeight: "bold",
            }}
            placeholderTextColor={C.text01}
          />
          {isTitleEditable && (
            <View style={{ flexDirection: "row", justifyContent: "center" }}>
              <IconPrimr
                preset="safe"
                name={"check"}
                size={moderateScale(18)}
                color={C.awakenVolt}
                onPress={setNewTitle}
              />

              <IconPrimr
                preset="safe"
                name={"x"}
                size={moderateScale(18)}
                color={C.errorRed}
                onPress={() => {
                  setBoardTitle(scoreboard?.title);
                  shouldTitleEditable(false);
                }}
              />
            </View>
          )}
        </View>
        {(_boardSubtitle && `${_boardSubtitle}`.length > 1) ||
        Backend.firestoreHandler._account.admin === true ? (
          <View>
            <TextInput
              ref={refSubtitle}
              editable={isSubtitleEditable}
              value={_boardSubtitle}
              onChangeText={setBoardSubtitle}
              onSubmitEditing={setNewSubtitle}
              placeholder={scoreboard?.subtitle}
              style={{
                alignSelf: "center",
                fontSize: scale(16),
                textAlign: "center",
                color: C.text01,
                fontWeight: "300",
              }}
              placeholderTextColor={C.text01}
            />
            {isSubtitleEditable && (
              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <IconPrimr
                  preset="safe"
                  name={"check"}
                  size={moderateScale(18)}
                  color={C.awakenVolt}
                  onPress={setNewSubtitle}
                />

                <IconPrimr
                  preset="safe"
                  name={"x"}
                  size={moderateScale(18)}
                  color={C.errorRed}
                  onPress={() => {
                    setBoardSubtitle(scoreboard?.title);
                    shouldSubtitleEditable(false);
                  }}
                />
              </View>
            )}
          </View>
        ) : null}
        {isAdmin && (
          <Txt
            category="c1"
            style={{ textAlign: "center", color: C.dim, marginTop: spacing(1) }}
          >
            Edit
          </Txt>
        )}
      </TouchableOpacity>
    );
  }

  /**
   * @note check and worked ✅
   */
  const setNewTitle = () => {
    Backend.scoreboardHandler.updateScoreboard(scoreboard.title, {
      title: _boardTitle,
    });
    shouldTitleEditable(false);
    Toasty({ text1: "Scoreboard renamed!" });
  };
  const setNewSubtitle = () => {
    Backend.scoreboardHandler.updateScoreboard(scoreboard.title, {
      subtitle: _boardSubtitle,
    });
    shouldSubtitleEditable(false);
    Toasty({ text1: "Subtitle updated!" });
  };

  /**
   * @note check and worked ✅
   */
  const resetScoreboard = () => {
    Alert.alert("Are you sure you would like to reset this scoreboard?", "", [
      { text: "Cancel", style: "cancel" },
      {
        text: "Yes!",
        onPress: () => {
          onCloseSH();
          Backend.scoreboardHandler.resetScoreboard(scoreboard.title);
          setMenuVisible(false);
          Toasty({ text1: "Successfully reset scoreboard" });
        },
      },
    ]);
  };
  /**
   * @note check and worked ✅
   */
  const deleteScoreboard = () => {
    Alert.alert(
      "Are you sure you would like to DELETE this scoreboard?",
      "It will be gone forever!",
      [
        { text: "Cancel", style: "cancel" },
        {
          text: "Yes!",
          onPress: () => {
            onCloseSH();
            Backend.scoreboardHandler.deleteScoreboard(scoreboard.title);
            setMenuVisible(false);
          },
        },
      ]
    );
  };

  async function actionSheetMoveSelected(index: number) {
    const allScoreboards: dScoreboard[] = Backend.scoreboardHandler._scoreboards;
    let leftScoreboard: dScoreboard;
    let indexOfThisScoreboard: number;
    let rightScoreboard: dScoreboard;
    for (let x = 0; x < allScoreboards.length; x++) {
      const sb = allScoreboards[x];
      if (sb.id === scoreboard.id) {
        indexOfThisScoreboard = x;
        leftScoreboard = x - 1 >= 0 ? allScoreboards[x - 1] : null;
        rightScoreboard =
          x + 1 < allScoreboards.length ? allScoreboards[x + 1] : null;
        break;
      }
    }
    if (index === 0) {
      // left
      if (!leftScoreboard) {
        Toasty({
          text1: "Failed to move scoreboard.",
          text2: "This scoreboard is already all the way to the left.",
          type: "error",
        });
        return;
      }
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      onCloseSH();
      await Backend.scoreboardHandler.updateScoreboard(scoreboard.title, {
        position: leftScoreboard.position - 0.5,
      });
      Toasty({ text1: "Scoreboard moved." });
    } else if (index === 1) {
      // right
      if (!rightScoreboard) {
        Toasty({
          text1: "Failed to move scoreboard.",
          text2: "This scoreboard is already all the way to the right.",
          type: "error",
        });
        return;
      }
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      onCloseSH();
      await Backend.scoreboardHandler.updateScoreboard(scoreboard.title, {
        position: rightScoreboard.position + 0.5,
      });
      Toasty({ text1: "Scoreboard moved." });
    }
  }

  const actionSheetRef = React.useRef();

  return (
    <>
      <Kitten.OverflowMenu
        style={{
          ...(Platform.isPad
            ? {
                width: 250,
                minHeight: 337,
              }
            : {}),
        }}
        backdropStyle={{
          height: 500,
        }}
        anchor={RenderHeader}
        visible={isMenuVisible}
        // onSelect={onItemSelect}
        onBackdropPress={() => setMenuVisible(false)}
      >
        <Kitten.MenuItem
          title="Edit Title"
          onPress={() => {
            shouldSubtitleEditable(false);
            shouldTitleEditable(true);
            setTimeout(() => {
              refTitle.current.focus();
              setMenuVisible(false);
            }, 120);
          }}
        />
        <Kitten.MenuItem
          title="Edit Subtitle"
          onPress={() => {
            shouldTitleEditable(false);
            shouldSubtitleEditable(true);
            setTimeout(() => {
              refSubtitle.current.focus();
              setMenuVisible(false);
            }, 120);
          }}
        />
        <Kitten.MenuItem
          title="Move"
          accessoryRight={(_props) => (
            <View style={{ flexDirection: "row" }}>
              <IconPrimr
                name="arrow_left"
                color="white"
                size={moderateScale(15)}
              />
              <IconPrimr
                name="arrow_right"
                color="white"
                size={moderateScale(15)}
              />
            </View>
          )}
          onPress={() => {
            actionSheetRef.current.show();
          }}
        />
        {/* <Kitten.MenuItem title={`Move Position (${scoreboard.position})`} /> */}
        <Kitten.MenuItem title="Reset Scores" onPress={resetScoreboard} />
        <Kitten.MenuItem
          style={{ backgroundColor: C.errorRed }}
          title="Delete"
          onPress={deleteScoreboard}
        />
      </Kitten.OverflowMenu>
      <ActionSheet
        ref={actionSheetRef}
        options={["Move Left", "Move Right", "Cancel"]}
        cancelButtonIndex={2}
        onPress={actionSheetMoveSelected}
      />
    </>
  );
}

interface dScoreList extends P {}
function $_ScoreList(props: dScoreList) {
  const { scoreboard } = props;
  /** Check if this user is admin */
  const _isAdmin = Backend.firestoreHandler._account.admin;
  const [_scoreboard, setScoreboard] = React.useState([]);
  React.useEffect(function updateScore() {
    Backend.scoreboardHandler.scoreboards((scoreboards) => {
      const newScoreboard = Backend.scoreboardHandler.getScoreboardByTitle(
        scoreboard.title
      );
      setScoreboard(newScoreboard);
    });
  }, []);

  const isBoardEmpty = !_scoreboard?.people || _scoreboard?.people.length == 0;

  function updateFRBSScore(userName: string, score) {
    Keyboard.dismiss();
    Backend.scoreboardHandler.updateSomeonesScore(
      scoreboard?.title,
      userName,
      score
    );

    // Toasty({ text1: "Updated score successfully!" });
  }

  return (
    <View
      style={{
        alignItems: "stretch",
        paddingTop: spacing(6),
        paddingHorizontal: spacing(2),
      }}
    >
      {!isBoardEmpty && _isAdmin && (
        <Txt
          category="c2"
          style={{
            textAlign: "center",
            color: C.dim,
            paddingVertical: spacing(2),
          }}
        >
          Tap member's score to edit
        </Txt>
      )}
      {_scoreboard?.people?.map((u, idx) => (
        <ItemScore
          {...props}
          index={idx}
          user={u}
          onSubmit={(score) => {
            updateFRBSScore(u.name, score);
          }}
        />
      ))}
      {isBoardEmpty && (
        <Txt category="p1" style={{ textAlign: "center", color: C.dim }}>
          No scores posted
        </Txt>
      )}
    </View>
  );
}

interface dFooter extends P {
  onCloseSH?(): void;
}
function $_Footer(props: dFooter) {
  const { navigation, scoreboard, onCloseSH } = props;
  const { name } = Backend.firestoreHandler._account;
  const [_name, setName] = React.useState(null);

  React.useEffect(function fetchScoreboard() {
    Backend.firestoreHandler.account((acc) => setName(acc.name));
  }, []);

  /**
   * Update my score, using Backend
   *
   * ---
   * @version 0.10.29
   */
  function updateMyFRBSScore(score) {
    Keyboard.dismiss();
    if (!_name || _name == "") {
      Alert.alert(
        "Before we continue",
        "Complete your profile before adding the score"
      );
      navigation.navigate("You");
    } else {
      // onCloseSH();
      Backend.scoreboardHandler.setMyScore(Number.parseInt(score), scoreboard);
      Toasty({ text1: "Update your score successfully!" });
    }
  }
  return (
    <View
      style={{
        paddingHorizontal: spacing(3),
        opacity: 0.6,
        borderWidth: 1,
        borderColor: C.primary,
        borderRadius: 5,
        justifyContent: "center",
        backgroundColor: C.background,
      }}
    >
      <ItemUrScore
        {...props}
        user={{ name: "Add Your Score", score: "00" }}
        onSubmit={(score) => updateMyFRBSScore(score)}
      />
    </View>
  );
}

/**
 * ### A user's score (list item)
 * @example
 * <ItemScore
      {...props}
      index={idx}
      user={u}
      onSubmit={(score) => {
        updateFRBSScore(u.name, score);
      }}
    />
 */
interface dItemScore extends P {
  user: { name: string; score: number | string };
  index: Number;
  onSubmit(score: number | string): void;
}
function ItemScore(props: dItemScore) {
  const { user, onSubmit, index } = props;
  /** Check if this user is admin */
  const _isAdmin = Backend.firestoreHandler._account.admin;
  const [_score, setScore] = React.useState(user?.score.toString());
  const [isEditing, setEdit] = React.useState(false);
  const refInput = React.useRef<TextInput>();
  React.useEffect(
    function setupEditing() {
      (!_score || _score == "") && setEdit(false);
    },
    [_score]
  );
  // React.useEffect(
  //   function focusInput() {
  //     !!isEditing && refInput.current.focus();
  //   },
  //   [isEditing]
  // );
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginVertical: spacing(2),
        ...(user.score < 1 ? { height: 0 } : {}),
        overflow: "hidden",
      }}
    >
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <IconPrimr
          name={`medal`}
          size={moderateScale(20)}
          color={
            index === 0
              ? C.hazardYellow
              : index === 1
              ? C["color-basic-500"]
              : index === 2
              ? C["color-warning-800"]
              : "transparent"
          }
          containerStyle={{ marginRight: spacing(2) }}
        />
        <Txt style={{ textAlign: "left" }}>{user.name}</Txt>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <TextInput
          value={isEditing ? _score : user.score}
          editable={_isAdmin}
          keyboardType="number-pad"
          onChangeText={(value) => {
            setScore(value);
            setEdit(true);
          }}
          onSubmitEditing={() => {
            onSubmit(_score);
            setEdit(false);
          }}
          selectTextOnFocus={true}
          style={{ fontSize: scale(20), color: C.text01, fontWeight: "bold" }}
          placeholder={user.score.toString()}
          placeholderTextColor={C.text01}
        />
        {isEditing && (
          <IconPrimr
            name={"check"}
            size={scale(20)}
            color={C.awakenVolt}
            onPress={() => {
              onSubmit(_score);
              setEdit(false);
            }}
          />
        )}
        {isEditing && (
          <IconPrimr
            name={"x"}
            size={scale(20)}
            color={C.errorRed}
            onPress={() => {
              setScore(user?.score.toString());
              Keyboard.dismiss();
              setEdit(false);
            }}
          />
        )}
      </View>
    </View>
  );
}

function ItemUrScore(props: Partial<dItemScore>) {
  const { user, onSubmit } = props;
  const [_score, setScore] = React.useState(user?.score);
  const [isEditing, setEdit] = React.useState(false);

  React.useEffect(
    function setupEditing() {
      (!_score || _score == "") && setEdit(false);
    },
    [_score]
  );
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
      }}
    >
      <Txt category="s1">{user.name + " "}</Txt>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Kitten.Input
          value={_score}
          keyboardType="number-pad"
          placeholder={user.score.toString()}
          selectTextOnFocus={true}
          onChangeText={(value) => {
            setScore(value);
            setEdit(true);
          }}
          onSubmitEditing={() => {
            onSubmit(_score);
            setEdit(false);
          }}
        />
        {isEditing && (
          <IconPrimr
            name={"check"}
            size={scale(20)}
            color={C.awakenVolt}
            onPress={() => {
              onSubmit(_score);
              setEdit(false);
            }}
          />
        )}
        {isEditing && (
          <IconPrimr
            name={"x"}
            size={scale(20)}
            color={C.errorRed}
            onPress={() => {
              setScore(user?.score);
              setEdit(false);
            }}
          />
        )}
      </View>
    </View>
  );
}
