import { Txt } from "components/";
import React from "react";
import { Platform, View } from "react-native";
import { Modalize } from "react-native-modalize";
import { C, DEVICE_HEIGHT, spacing } from "utilities/";

interface P {
  y: number;
  onOverlayPress: () => void;
}
export const SH_WelcomeGuide = React.forwardRef((props: P, ref) => {
  const { navigation, onOverlayPress } = props;
  const y = props.y * (Platform.OS === "android" ? 0.5 : 1);
  return (
    <Modalize
      ref={ref}
      // modalTopOffset={y}
      modalHeight={DEVICE_HEIGHT}
      scrollViewProps={{
        keyboardShouldPersistTaps: "always",
      }}
      /// snapPoint={DEVICE_HEIGHT * 0.7}
      // modalHeight={DEVICE_HEIGHT * 0.7}
      modalStyle={{
        backgroundColor: "#00000096",
        padding: spacing(3),
        marginTop: y,
      }}
      overlayStyle={{
        backgroundColor: "transparent" /** transparent */,
      }}
      onOverlayPress={() => {
        ref.current.close();
        onOverlayPress();
      }}
      //   alwaysOpen={-DEVICE_HEIGHT + 200}
      // snapPoint={HEADER_HEIGHT}
      withHandle={false}
      //   onClosed={() => this.setState({ isUserNew: false })}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: "transparent",
          justifyContent: "flex-start",
          alignItems: "center",
        }}
      >
        <View
          style={{
            alignItems: "center",
          }}
        >
          <Txt style={{ fontSize: 100 }}>👆</Txt>
          <Txt.H5
            category={"h3"}
            fontFam="bold"
            style={{
              color: C.text01,
              backgroundColor: C.background01,
              textAlign: "center",
              borderRadius: 10,
            }}
          >
            Welcome!{"\n"}Tap here to get started
          </Txt.H5>
          <Txt.S1
            category={"c1"}
            onPress={() => {
              ref.current.close();
            }}
            style={{
              padding: 10,
              color: C.text01,
              backgroundColor: C.background01,
              textAlign: "center",
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
            }}
          >
            Swipe down to close
          </Txt.S1>
        </View>
      </View>
    </Modalize>
  );
});
