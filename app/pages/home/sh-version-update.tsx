import Backend from "backend/";
import { Txt, Buttoon } from "components/";
import { APP_LOGO } from "constant-deprecated/Images";
import React from "react";
import { View, Image, Linking } from "react-native";
import { Modalize } from "react-native-modalize";
import { C, DEVICE_HEIGHT, DEVICE_WIDTH, spacing } from "utilities/";

const modalHeight = DEVICE_HEIGHT * 0.85;

/**
 * Prompts user to update their app
 */
export const SH_VersionUpdate = React.forwardRef<Modalize>((props, ref) => {
  return (
    <Modalize
      ref={ref}
      modalHeight={modalHeight}
      modalStyle={Styles.modalStyle}
      overlayStyle={{ backgroundColor: "#00000098" }}
      withHandle={true}
      HeaderComponent={null}
      scrollViewProps={{
        scrollEnabled: false
      }}
    >
      <View
        style={{
          flex: 1,
          height: modalHeight,
          justifyContent: "space-around",
          paddingBottom: spacing(7),
        }}
      >
        <$_Prompt />
        <$_App />
        <$_Install />
      </View>
    </Modalize>
  );
});

function $_Prompt(props) {
  return (
    <View
      style={{
        paddingHorizontal: spacing(6),
      }}
    >
      <Txt
        style={{
          fontWeight: "bold",
          textAlign: "center",
        }}
        category="h5"
      >
        Update Available!
      </Txt>
      <Txt
        category="p1"
        style={{
          marginTop: spacing(3),
          textAlign: "center",
        }}
      >
        A more excellent version of your app is ready to be installed.
      </Txt>
    </View>
  );
}

function $_App(props) {
  const { appTitle } = Backend.firestoreHandler._config.variables;
  const imageDimension = DEVICE_WIDTH * 0.27;
  return (
    <View
      style={{
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Image
        source={APP_LOGO}
        style={{
          width: imageDimension,
          height: imageDimension,
          borderRadius: 17,
          marginBottom: spacing(2),
        }}
      />
      <Txt category="s1" style={{ fontWeight: "bold" }}>
        {appTitle}
      </Txt>
    </View>
  );
}

function $_Install(props) {
  const { updateURL, appTitle } = Backend.firestoreHandler._config.variables;
  const app_name = appTitle.replace(/ /g, "").toLowerCase();
  const defaultUpdateURL = `https://get-my.app/t/${app_name}/?update=1`;
  return (
    <View
      style={{
        justifyContent: "space-evenly",
        alignItems: "center",
      }}
    >
      <Buttoon
        icon={{ name: "install" }}
        onPress={() => {
          const url = updateURL?.length > 0 ? updateURL : defaultUpdateURL;
          Linking.openURL(url);
        }}
      >
        Install Update
      </Buttoon>
    </View>
  );
}

const Styles = {
  modalStyle: {
    marginTop: spacing(4),
    backgroundColor: C.surface01,
    padding: spacing(3),
    borderWidth: 1,
    borderColor: C["color-primary-500"],
    borderBottomWidth: 0,
  },
};
