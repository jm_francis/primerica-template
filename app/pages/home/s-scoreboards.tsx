import { Avatar } from "@ui-kitten/components";
import Backend from "backend/";
import { dScore } from "backend/schemas/scoreboards";
import { dScoreboard } from "backend/ScoreboardHandler/scoreboard-handler.props";
import { Buttoon, IconPrimr, Kitten, Txt } from "components/";
import React from "react";
import { Alert, FlatList, LayoutAnimation, View } from "react-native";
import DialogInput from "react-native-dialog-input";
import { moderateScale } from "react-native-size-matters";
import { C, scale, spacing } from "utilities/";

export function S_Scoreboards(props) {
  const { navigation, isUserNew } = props;
  const [_scores, setScores] = React.useState([]);
  const [_account, setAccount] = React.useState(null);
  const [_users, setUsers] = React.useState(null);
  const [_dialogShown, showDialog] = React.useState(false);

  React.useEffect(function fetchScoreboard() {
    Backend.scoreboardHandler.scoreboards(setScores);
    Backend.firestoreHandler.account(setAccount);
    Backend.adminHandler.users(setUsers);
  }, []);

  let isAdmin = !!_account && _account.admin;

  function newScoreboardSubmit(input) {
    Backend.scoreboardHandler.newScoreboard(input);
    showDialog(false);
  }

  function resetButtonPress() {
    Alert.alert("Are you sure you would like to reset all scoreboards?", "", [
      { text: "Cancel", style: "cancel" },
      {
        text: "Yes!",
        onPress: () => {
          Backend.scoreboardHandler.resetAllScoreboards();
        },
      },
    ]);
  }

  const scoreboards: dScoreboard[] = Backend.scoreboardHandler._scoreboards;

  return (
    <View style={{ opacity: isUserNew ? 0.4 : 1 }}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          // paddingTop: spacing(4),
        }}
      >
        <Txt.$Title>Scoreboards</Txt.$Title>
        {/* <Buttoon
          onPress={() => navigation.navigate("Scoreboard")}
          appearance="ghost"
          icon={{ name: "chevron_right", right: true }}
          status="basic"
          size="medium"
        >
          All
        </Buttoon> */}
      </View>

      <FlatList
        data={scoreboards}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item }) => (
          <CardScoreboard
            scoreboard={item}
            onPress={() => {
              // onScoreboardPress(item);
              navigation.navigate("scoreboard-scr", { item });
            }}
          />
        )}
        ListFooterComponent={
          isAdmin && (
            <$_Boardutils
              onAddPress={() => {
                showDialog(true);
              }}
              onResetPress={resetButtonPress}
            />
          )
        }
      />
      <DialogInput
        isDialogVisible={_dialogShown}
        title="Enter a scoreboard name"
        submitInput={newScoreboardSubmit}
        closeDialog={() => showDialog(false)}
      />
      {/* {isUserNew && (
          <View
            style={{
              position: "absolute",
              width: "100%",
              height: "100%",
              backgroundColor: C.surface01,
              opacity: 0.4,
            }}
          />
        )} */}
    </View>
  );
}

/**
 * ###  Scoreboard Utilities,
 *
 * -  Currently located at the end of scoreboard list
 * @version 1.1.12
 */
const $_Boardutils = (props) => {
  const { onAddPress, onResetPress } = props;
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "space-between",
        alignItems: "center",
        marginLeft: spacing(5),
      }}
    >
      <Buttoon
        style={{ flex: 2 }}
        appearance={"outline"}
        size="tiny"
        icon={{ name: "plus" }}
        status="success"
        onPress={onAddPress}
      >
        New
      </Buttoon>
      <Buttoon
        style={{ flex: 1 }}
        appearance={"outline"}
        size="tiny"
        status="danger"
        onPress={onResetPress}
      >
        Reset All
      </Buttoon>
    </View>
  );
};

interface dCardScoreboard {
  scoreboard: dScoreboard;
  onPress?(): void;
}
const CardScoreboard = (props: dCardScoreboard) => {
  const { scoreboard, onPress } = props;
  const isBoardEmpty = !scoreboard?.people || scoreboard?.people.length == 0;
  const topMember: any = scoreboard?.people.length != 0 && scoreboard.people[0];

  const cardHeight = moderateScale(115);
  const avatarDimension = moderateScale(31);
  const [topDudeShown, showTopDude] = React.useState(false);

  React.useEffect(() => {
    setTimeout(() => {
      LayoutAnimation.configureNext(
        LayoutAnimation.create(
          300,
          LayoutAnimation.Types.easeInEaseOut,
          LayoutAnimation.Properties.scaleXY
        )
      );
      showTopDude(true);
    }, 100);
  }, []);

  const topUser =
    scoreboard.people.length > 0
      ? Backend.adminHandler.getUserByUid(scoreboard.people[0].uid)
      : null;

  return (
    <Kitten.Card
      onPress={onPress}
      style={{
        flex: 1,
        width: scale(160),
        height: scale(180),
        justifyContent: "space-between",
        alignItems: "center",
        marginLeft: spacing(5),
      }}
      header={() => {
        return (
          topDudeShown && (
            <View
              style={{
                // alignSelf: scoreboard.people.length == 0 ? "center" : "flex-start",
                alignSelf: "center",
                paddingHorizontal: spacing(3),
              }}
            >
              {/* {scoreboard.people.length == 0 ? (
                <Txt
                  style={{
                    marginTop: "18%",
                    alignSelf: "center",
                  }}
                >
                  No scores
                </Txt>
              ) : null} */}

              <View>
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Avatar
                    source={{ uri: topUser?.profileImage }}
                    style={{ backgroundColor: C.dim }}
                  />
                  <Txt.H6 style={{ marginVertical: spacing(3) }}>
                    {scoreboard.people[0]?.score}
                  </Txt.H6>
                  <Txt.C2>
                    {scoreboard.people.length == 0 ? "No Scores" : "Top Score"}
                  </Txt.C2>
                </View>
              </View>
              {scoreboard.people.length > 0 ? (
                <ScoreItem
                  score={scoreboard.people[0]}
                  color={C.hazardYellow}
                />
              ) : null}
            </View>
          )
        );
      }}
    >
      <View style={{ padding: spacing(3) }}>
        <Txt.C1 style={{ fontWeight: "bold" }} numberOfLines={1}>
          {scoreboard.title}
        </Txt.C1>
      </View>
    </Kitten.Card>
  );
};

function ScoreItem(p: { score: dScore; color: string }) {
  const { score, color } = p;
  return (
    <View
      style={{
        marginBottom: 8,
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      <IconPrimr preset="default" name={"crown"} color={color} size={scale(14)} />
      <Txt.C2 style={{ fontWeight: "bold" }} numberOfLines={1}>
        {" "}
        · {score.name || "unknown"}
      </Txt.C2>
    </View>
  );
}
