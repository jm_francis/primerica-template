import { CardProps } from "@ui-kitten/components";
import { ScoreboardSchema } from "backend/schemas/scoreboards";
import { Kitten, Txt, IconPrimr } from "components/";
// import { ScoreboardSchema, useAppContext, _MOCK_SCOREBOARD } from "engines/";
import React from "react";
import { View } from "react-native";
import { spacing, moderateScale } from "utilities/";

export function CardScoreboard(props: dCardScoreboard) {
  const { scoreboard = _MOCK_SCOREBOARD, onPress } = props;

  return (
    <Kitten.Card
      onPress={onPress}
      style={{
        flex: 1,
        justifyContent: "space-between",
        alignItems: "center",
        marginLeft: spacing(5),
        ...props?.style,
      }}
      footer={() => (
        <View
          style={{
            alignSelf: "flex-start",
            paddingHorizontal: spacing(3),
          }}
        >
          <C_MedalLine
            person={{ uid: "delicious012", name: "Rice Cooker", score: 100 }}
            type="gold"
          />
          <C_MedalLine
            person={{ uid: "okey1734", name: "Soy Sauce", score: 100 }}
            type="silver"
          />
          <C_MedalLine
            person={{ uid: "ohyeadh129", name: "Boba Tea", score: 100 }}
            type="bronze"
          />
        </View>
      )}
      {...props}
    >
      <View style={{ padding: spacing(3) }}>
        <Txt.C1 numberOfLines={1} style={{ fontWeight: "700" }}>
          {scoreboard.title}
        </Txt.C1>
      </View>
    </Kitten.Card>
  );
}

interface dMedalLine {
  type: "gold" | "silver" | "bronze";
  person: ScoreboardSchema["people"][0];
  size: "giant" | "normal";
}
export const C_MedalLine = (p: dMedalLine) => {
  const { type = "gold", person, size = "normal" } = p;
  const { C } = useAppContext();

  function medalColor() {
    switch (type) {
      case "gold": {
        return C["hazardYellow"];
        break;
      }
      case "silver": {
        return C["dim"];
        break;
      }
      case "bronze": {
        return C["color-warning-800"];
        break;
      }
      default:
        return C["dim"];
    }
  }
  return (
    <View
      style={{
        marginBottom: 8,
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      <IconPrimr
        preset="default"
        name={"dot"}
        color={medalColor()}
        size={size == "giant" ? 30 : 14}
      />
      {size == "giant" ? (
        <Txt.H6 numberOfLines={1}> · {person?.name || "unknown"}</Txt.H6>
      ) : (
        <Txt.C2 numberOfLines={1}> · {person?.name || "unknown"}</Txt.C2>
      )}
    </View>
  );
};

interface dCardScoreboard extends CardProps {
  scoreboard: ScoreboardSchema;
  onPress?(): void;
}
