import Backend, { PosstSchema } from "backend/";
// import { FPATH } from "backend/FirestoreHandler/firestore-handler.props";
import { Buttoon, Kitten, sstyled, Txt } from "components/";
import R from "ramda";
import React, { useState } from "react";
import { FlatList, LayoutAnimation, View } from "react-native";
import { PostCard } from "./PosstCard/c-post-card";

interface dExploreScr {}
// export const frefExplore = firebase.firestore().collection(FPATH.POSSTS);
export function S_Explore(props: dExploreScr) {
  const [contentShown, showContent] = useState(false);
  // useEffect(function staging() {
  //   // LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  // }, []);

  const [possts, setPossts] = React.useState<PosstSchema[]>([]);

  React.useEffect(function fetchAllPossts() {
    showContent(false);
    Backend.exploreHandler.possts((possts) => {
      //* sort possts by descending `createdAt` (recent)
      let sortedPossts = R.sortWith([R.descend(R.prop("createdAt"))], possts); //.filter((pst: PosstSchema) => !pst.pinned);

      // LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      setPossts(sortedPossts);
      setTimeout(() => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        showContent(true);
      }, 600);
    });
  }, []);

  // console.log("_MOCK_POSSTS : " + JSON.stringify(_MOCK_POSSTS));
  // console.log("possts : " + JSON.stringify(possts));

  //#region [SECTION ] pagination
  const [loading, setLoading] = React.useState<boolean>(false);
  const [offset, setOffset] = useState(5);
  const [posstsLength, setPosstsLength] = React.useState<number>(0);

  const getData = () => {
    console.log("getData");
    showContent(false);

    Backend.exploreHandler.possts((possts) => {
      //* sort possts by descending `createdAt` (recent)
      let sortedPossts = R.sortWith([R.descend(R.prop("createdAt"))], possts); //.filter((pst: PosstSchema) => !pst.pinned);
      setPosstsLength(sortedPossts.length);
      setPossts(sortedPossts);
      setTimeout(() => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        showContent(true);
      }, 600);
    });
  };

  React.useEffect(() => getData(), []);
  //#endregion

  return (
    <View style={{ flex: 1 }}>
      <Txt.$Title>Explore</Txt.$Title>
      {!R.isEmpty(possts) && contentShown ? ( //TODO @K->@SM Render the list once exploreHandler fetch all possts
        <FlatList
          initialNumToRender={5}
          style={{ flex: 1 }}
          // numColumns={Platform.isPad ? 2 : 1}
          //TODO @K->@SM Fetch all posts to plug in
          data={possts.slice(0, offset)}
          //data={[]}
          ItemSeparatorComponent={() => (
            <Kitten.Divider />
            // <View style={{ width: "100%", height: spacing(5) }} />
          )}
          keyExtractor={(item) => item._pid}
          renderItem={({ item, index }) => <PostCard {...props} item={item} />}
          // renderItem={({ item }) => <Txt>{JSON.stringify(item)}</Txt>}

          ListFooterComponent={() => (
            <Buttoon
              appearance={"ghost"}
              progress
              onPress={(xong) => {
                setTimeout(() => {
                  setOffset(offset + 5);
                  xong();
                }, 600);
              }}
              disabled={offset >= posstsLength}
            >
              {offset < posstsLength ? "Load More" : ""}
            </Buttoon>
          )}

          /**
           * todo i tried onEndReached for "endless scroll" feeling, but the fetching speed is not good enough.
           * So let's use the "Load More" button method instead
           */
          //
          // onEndReachedThreshold={0.4}
          // onEndReached={() => {
          //   if (offset < posstsLength) {
          //     setLoading(true);
          //     setOffset(offset + 5);
          //     setTimeout(() => {
          //       setLoading(false);
          //     }, 2000);
          //   }
          // }}
          // ListFooterComponent={() => loading && <Kitten.Spinner />}
        />
      ) : (
        <A.CtnrBottom>
          <Kitten.Spinner style={{ alignSelf: "center" }} />
        </A.CtnrBottom>
      )}
    </View>
  );
}

const A = {
  CtnrBottom: sstyled(View)({
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 60, //* leave space for FAB button
  }),
};
