import Backend, { UserSchema } from "backend/";
import { Buttoon, IconPrimr, Kitten, sstyled, Toasty, Txt } from "components/";
import { dDiscussionScrParams } from "navigation";
import React from "react";
import { Alert, LayoutAnimation, View } from "react-native";
import { C, moderateScale, spacing } from "utilities/";
import { dCtnrPost } from "./c-post-card";

export function CardFooter(p: dCtnrPost) {
  const { item, navigation } = p;
  // const [_isLiked, shouldLike] = React.useState(false);
  const [_account, setAccount] = React.useState<UserSchema>(null);

  React.useEffect(() => {
    Backend.firestoreHandler.account((account) => setAccount(account));
  }, []);

  const _isLiked = _account && item?.likes.includes(_account?.uid);

  const hasImage =
    item?.medias.length > 0 && item?.medias[0].type.includes("image");

  return (
    <>
      <SS.Ctnr>
        <$_Author {...p} />
        <Kitten.ButtonGroup appearance="ghost" status="basic">
          <Buttoon
            icon={
              _account?.admin && hasImage
                ? {
                    name: "pin",
                    color: item?.pinned ? C.hazardYellow : C.dim,
                  }
                : null
            }
            progress
            disabled={!(_account?.admin && hasImage)}
            onPress={async (xong) => {
              if (!item?.pinned) {
                await Backend.exploreHandler.pinPosst(item);
                Toasty({ text1: "Post pinned to top!" });
                setTimeout(() => {
                  xong();
                }, 1000);
              } else {
                await Backend.exploreHandler.unpinPosst(item);
                Toasty({ text1: "Unpinned post." });
                setTimeout(() => {
                  xong();
                }, 1000);
              }
            }}
          />
          <Buttoon
            progress
            onPress={(xong) => {
              Backend.exploreHandler.likePosst(item);
              xong();
            }}
            icon={{
              name: "heart",
              solid: _isLiked,
              color: _isLiked ? C.errorRed : C.dim,
            }}
            appearance="ghost"
          >
            {item?.likes.length || "0"}
          </Buttoon>
          <Buttoon
            progress
            onPress={(xong) => {
              navigation.navigate<dDiscussionScrParams>("discussion-scr", {
                item: item,
                section: "comment",
              });
              xong();
            }}
            appearance="ghost"
            status="basic"
            icon={{ name: "chat_bubble" }}
          >
            {item?.commentCount ? `${item?.commentCount}` : "0"}
          </Buttoon>
          <Buttoon
            icon={
              _account?.admin
                ? {
                    name: "trash",
                    color: C.dim,
                  }
                : null
            }
            progress
            disabled={!_account?.admin}
            onPress={(xong) => {
              Alert.alert("Confirmation", "Do you want to delete this post?", [
                {
                  text: "Cancel",
                  onPress: () => {
                    xong();
                  },
                },
                {
                  text: "Yes",
                  onPress: () => {
                    Backend.exploreHandler.deletePosst(item).then((r) => {
                      LayoutAnimation.configureNext(
                        LayoutAnimation.Presets.easeInEaseOut
                      );
                      Toasty({ text1: "Post deleted" });
                      xong();
                    });
                  },
                },
              ]);
            }}
          />
        </Kitten.ButtonGroup>

        {/* 
        //todo If the ButtonGroup above works well, remove these parts 👇
        <SS.Actions>
          {_account?.admin && hasImage ? (
            <Buttoon
              onPress={async () => {
                if (!item?.pinned) {
                  await Backend.exploreHandler.pinPosst(item);
                  Toasty({ text1: "Post pinned to top!" });
                } else {
                  await Backend.exploreHandler.unpinPosst(item);
                  Toasty({ text1: "Unpinned post." });
                }
              }}
              appearance="ghost"
              status={"warning"}
              icon={{
                name: "pin",
                color: item?.pinned ? C.hazardYellow : C.dim,
              }}
            />
          ) : null}
          <Buttoon
            progress
            onPress={(xong) => {
              Backend.exploreHandler.likePosst(item);
              xong();
            }}
            status={_isLiked ? "danger" : "basic"}
            icon={{ name: "heart", solid: _isLiked }}
            appearance="ghost"
          >
            {item?.likes.length || "0"}
          </Buttoon>
          <Buttoon
            progress
            onPress={(xong) => {
              navigation.navigate<dDiscussionScrParams>("discussion-scr", {
                item: item,
                section: "comment",
              });
              xong();
            }}
            appearance="ghost"
            status="basic"
            icon={{ name: "chat_bubble" }}
          >
            {item?.commentCount ? `${item?.commentCount}` : "0"}
          </Buttoon>
          {CASE.U_ARE_ADMIN && (
            <Buttoon
              appearance="ghost"
              status="basic"
              icon={{ name: "trash" }}
              progress
              onPress={(xong) => {
                Alert.alert(
                  "Confirmation",
                  "Do you want to delete this post?",
                  [
                    { text: "Cancel", onPress: () => {} },
                    {
                      text: "Yes",
                      onPress: () => {
                        Backend.exploreHandler.deletePosst(item).then((r) => {
                          LayoutAnimation.configureNext(
                            LayoutAnimation.Presets.easeInEaseOut
                          );
                          Toasty({ text1: "Post deleted" });
                        });
                      },
                    },
                  ]
                );
                xong();
              }}
            />
          )}
        </SS.Actions> */}
      </SS.Ctnr>
    </>
  );
}

/**
 * ###  Author section of CardFooter
 * -  Containing Avatar, Author name, and createdAt date
 *
 * @version 1.1.30
 */
function $_Author(p: dCtnrPost) {
  const { isSkeleton, item } = p;
  //#region [SECTION  ] FORMATTING
  // const _formattedDate = fn.calendar.dateFormat(item?.createdAt?.toDate()); //*👈 format date here
  const _authorName = item?.author?.name || "unknown";
  const author = Backend.adminHandler.getUserByUid(item?.author._id);

  // const [_account, setAccount] = React.useState(null);
  // React.useEffect(function fetchAccount() {
  //   Backend.firestoreHandler.account((acc) => setAccount(acc));
  // }, []);

  //#endregion
  return isSkeleton ? (
    <SS.CtnrAuthor>
      <Skeleton.AuthorAvatar source={undefined} size="tiny" />
      <Skeleton.Text />
    </SS.CtnrAuthor>
  ) : (
    <>
      <SS.CtnrAuthor>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          {author && author.profileImage && author.profileImage.length > 0 ? (
            <SS.AuthorAvatar
              source={{ uri: author.profileImage }}
              size="medium"
            />
          ) : (
            <IconPrimr
              name={`profile`}
              preset="default"
              size={moderateScale(23)}
              color={C.pitchWhite}
            />
          )}
          <Txt.C2 numberOfLines={1}>{_authorName}</Txt.C2>
        </View>
      </SS.CtnrAuthor>
    </>
  );
}

const Skeleton = {
  Header: sstyled(View)((p) => ({
    width: 800,
    height: 200,
    backgroundColor: p.C["color-basic-transparent-400"],
  })),
  AuthorAvatar: sstyled(Kitten.Avatar)((p) => ({
    alignSelf: "center",
    marginRight: spacing(2),
    width: moderateScale(20),
    height: moderateScale(20),
    backgroundColor: p.C["color-basic-transparent-400"],
  })),
  Text: sstyled(Txt)((p) => ({
    height: 14,
    width: "60%",
    backgroundColor: p.C["color-basic-transparent-400"],
  })),
};

const SS = {
  Ctnr: sstyled(View)((p) => ({
    flexDirection: "row",
    justifyContent: "space-between",
  })),
  Actions: sstyled(View)((p) => ({
    flexDirection: "row",
    alignItems: "center",
  })),
  TxtBody: sstyled(Txt.C1)((p) => ({
    margin: spacing(2),
    marginBottom: spacing(1),
  })),
  CtnrAuthor: sstyled(View)((p) => ({
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginLeft: spacing(2),
  })),
  AuthorAvatar: sstyled(Kitten.Avatar)((p) => ({
    alignSelf: "center",
    marginRight: spacing(2),
    width: moderateScale(20),
    height: moderateScale(20),
    backgroundColor: p.C.grey600,
  })),
};
