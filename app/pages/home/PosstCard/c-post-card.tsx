import Backend, { PosstSchema } from "backend/";
import Linking from "backend/Linking";
import { Buttoon, Kitten, sstyled, Txt } from "components/";
import { customPageHandler } from "customPage/handlers/CustomPageHandler";
import { Videoh } from "pages/posst-creator/Videoh";
import React, { useLayoutEffect } from "react";
import {
  Image,
  Linking as RNLinking,
  useWindowDimensions,
  View
} from "react-native";
import { NavigationStackProp } from "react-navigation-stack";
import { moderateScale, spacing } from "utilities/";
import { extractLinkFromBody } from "utilities/functions/js-functions";
// import { socialFriendlyTime } from "utilities/functions/calendar-functions";
import { CardFooter } from "./s-card-footer";
export interface dCtnrPost {
  navigation?: NavigationStackProp<{}>;
  item: PosstSchema;
  isSkeleton?: boolean;
}

export function posstPress(item: PosstSchema) {
  if (item?.goToPage) {
    // const goToThisPage = customPageHandler.getPageWithId(item?.goToPage);
    const goToThisPage = Backend.firebasePages.getPageById(item?.goToPage);
    if (goToThisPage) customPageHandler.presentPage(goToThisPage.name);
  } else if (
    item?.body.includes("https://") ||
    item?.body.includes("http://")
  ) {
    const link = extractLinkFromBody(item?.body);
    console.log("extracted link: " + link);
    if (link.includes("zoom.us") && !link.includes("/webinar/register"))
      RNLinking.openURL(link);
    else Linking.openURL(link);
  }
}

/**
 * ### PostCard is <_> to show Post content in ExplorePage list
 * ---
 * PostCard contains:
 * -    CtnrHeader (Avatar, Name, postDate...),
 * -    Content,
 * -    and CtnrFooter (post's actions)
 * @version 0.12.1
 * @param props
 */
export function PostCard(props: dCtnrPost) {
  const { navigation, item, isSkeleton = false } = props;

  //#region [SECTION ] Image size
  const dimension = useWindowDimensions();
  const [mediaDimension, setMediaDimension] = React.useState<{
    width: number;
    height: number;
  }>({ width: 0, height: 0 });
  useLayoutEffect(function getMediaDimension() {
    item?.medias.length > 0 &&
      item?.medias[0].type.includes("image") &&
      Image.getSize(
        item?.medias[0].uri,
        (w, h) => {
          let fixWidth = dimension.width * 0.95;
          const _ratio = fixWidth / w;
          const height = h * _ratio;
          setMediaDimension({ width: fixWidth, height });
        },
        () => {
          let fixWidth = dimension.width * 0.95;
          setMediaDimension({ width: fixWidth, height: 300 });
        }
      );
  }, []);
  //#endregion

  return (
    <Kitten.Card footer={() => CardFooter(props)} disabled>
      {item?.medias.length > 0 && item?.medias[0].type.includes("image") && (
        <SS.ImgCtnr
          source={{ uri: item?.medias[0].uri }}
          resizeMode="contain"
          mediaDimension={mediaDimension}
        />
      )}

      {item?.medias.length > 0 && item?.medias[0].type.includes("video") && (
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "black",
          }}
        >
          <Videoh uri={item?.medias[0].uri} />
        </View>
      )}
      {/* <SS.TxtBody>{item?._pid}</SS.TxtBody> //* for debug */}
      {item?.medias.length > 0 ? (
        <SS.TxtBody>
          {item?.body
            .replace(/\\n/g, "\n")
            .replace(extractLinkFromBody(item?.body), "")}
        </SS.TxtBody>
      ) : (
        <SS.TxtHugeBody>
          {item?.body
            .replace(/\\n/g, "\n")
            .replace(extractLinkFromBody(item?.body), "")}
        </SS.TxtHugeBody>
      )}
      <Buttoon
        icon={extractLinkFromBody(item?.body) ? { name: "link" } : null}
        size="tiny"
        appearance={"ghost"}
        onPress={() => posstPress(item)}
      >
        {extractLinkFromBody(item?.body)
          ? "Go to " + extractLinkFromBody(item?.body).substring(0, 50)
          : ""}
      </Buttoon>
    </Kitten.Card>
  );
}

const Skeleton = {
  Header: sstyled(View)((p) => ({
    width: 800,
    height: 200,
    backgroundColor: p.C["color-basic-transparent-400"],
  })),
  AuthorAvatar: sstyled(Kitten.Avatar)((p) => ({
    alignSelf: "center",
    marginRight: spacing(2),
    width: moderateScale(20),
    height: moderateScale(20),
    backgroundColor: p.C["color-basic-transparent-400"],
  })),
  Text: sstyled(Txt)((p) => ({
    height: 14,
    width: "42%",
    backgroundColor: p.C["color-basic-transparent-400"],
  })),
};

const SS = {
  TxtBody: sstyled(Txt)((p) => ({
    margin: spacing(4),
  })),
  TxtHugeBody: sstyled(Txt.H6)((p) => ({
    margin: spacing(4),
    textAlign: "center",
  })),
  CtnrAuthor: sstyled(View)((p) => ({
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginLeft: spacing(2),
  })),
  AuthorAvatar: sstyled(Kitten.Avatar)((p) => ({
    alignSelf: "center",
    marginRight: spacing(2),
    width: moderateScale(20),
    height: moderateScale(20),
    backgroundColor: p.C.grey600,
  })),
  ImgCtnr: sstyled(Image)((p) => ({
    alignSelf: "center",
    borderRadius: 5,
    width: p.mediaDimension.width,
    height: p.mediaDimension.height,
  })),
};
