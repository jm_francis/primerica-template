//@ts-check
import Backend from "backend/";
import { dConfig } from "backend/FirestoreHandler/firestore-handler.props";
import Linking from "backend/Linking";
import { PosstSchema, UserSchema } from "backend/schemas";
import {
  Buttoon,
  dAccessory,
  IconPrimr,
  ICON_NAME,
  Kitten,
  sstyled,
  Toasty,
  Txt
} from "components/";
import { customPageHandler } from "customPage/handlers/CustomPageHandler";
import React, { Component, useState } from "react";
import { ImageBackground, LayoutAnimation, Platform, View } from "react-native";
import {
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback
} from "react-native-gesture-handler";
import { Modalize } from "react-native-modalize";
import Carousel from "react-native-snap-carousel";
import { NavigationScreenProp, NavigationState } from "react-navigation";
import { C, DEVICE_WIDTH, fn, moderateScale, scale, spacing } from "utilities/";
import {
  dateFormat,
  dCalendarEvent
} from "utilities/functions/calendar-functions";
import { extractLinkFromBody } from "utilities/functions/js-functions";
import { version } from "../../../package.json";
import { posstPress } from "./PosstCard/c-post-card";
import { S_Explore } from "./s-explore";
import { S_Scoreboards } from "./s-scoreboards";
import { SH_VersionUpdate } from "./sh-version-update";
import { SH_WelcomeGuide } from "./sh-welcome-guide";

interface P {
  navigation: NavigationScreenProp<NavigationState>;
}
export class HomePage extends Component<P> {
  state = {
    customPageItems: [],
    admin: false,
    directCoverModal: null,
    isUFirstTime: false,
    currentScoreboard: null,
    greenBarBottomY: 0,
  };
  refSH_WelcomeGuide = React.createRef<Modalize>();
  refSH_Scoreboard = React.createRef<Modalize>();
  refSH_VersionUpdate = React.createRef<Modalize>();

  async componentDidMount() {
    //* Bind this.pages with FRBS Pages
    // Backend.firebasePages.pages(this.pages.bind(this));
    //* Bind this.account with FRBS account
    Backend.firestoreHandler.account(this.account.bind(this));

    customPageHandler.init(this.props.navigation);
    Linking.init(this.props.navigation);

    Backend.permissionHandler.requestAll();

    Backend.firestoreHandler.config(this.config.bind(this));
  }

  /**
   * Check account config
   * @version 0.10.9
   */
  account(account: UserSchema) {
    if (!account) return;
    this.setState({ admin: account.admin });
    /**
     * Only updates user's firestore fcm token if needed
     */
    Backend.notificationHandler.setFCMToken();
  }

  config(config) {
    const myVersion = Number.parseInt(version.replace(/\./g, ""));
    const releaseVersion = Number.parseInt(
      config.variables.releaseVersion.replace(/\./g, "")
    );
    if (myVersion < releaseVersion) {
      this.refSH_VersionUpdate.current?.open();
    }
  }

  isFirstMeasure = true;
  measureHeader(fx, fy, width, height, x, y) {
    console.log(`fy: ${fy} height: ${height} y: ${y}`);
    // if (this.isFirstMeasure) {
    // this.isFirstMeasure = false;
    // return; // the first measure call is without the height of the view, the second is correct
    // }
    Backend.localHandler.getCustomPageDidOpen().then((result) => {
      if (result == false) {
        this.setState(
          {
            greenBarBottomY:
              height + fy + (Platform.OS === "android" ? y + 120 : 0),
            isUFirstTime: true,
          },
          () => {
            if (customPageHandler.getNewPersonPage())
              this.refSH_WelcomeGuide.current?.open();
          }
        );
      } else {
        this.setState({ isUFirstTime: false, greenBarBottomY: 0 });
      }
    });
  }

  render() {
    const { greenBarBottomY, isUFirstTime, admin } = this.state;
    const { navigation } = this.props;
    return (
      <>
        <ScrollView
          style={{ backgroundColor: C.background01, paddingBottom: scale(100) }}
          stickyHeaderIndices={[0]}
        >
          <$_Header
            {...this.props}
            onMeasure={this.measureHeader.bind(this)}
            isAdmin={this.state.admin}
            isUFirstTime={isUFirstTime}
            onNewPress={() => {
              console.log("isUFirstTime: false");
              this.setState({ isUFirstTime: false });
            }}
          />
          <$_HomeFeed {...this.props} isUFirstTime={isUFirstTime} />
        </ScrollView>
        {greenBarBottomY ? (
          <SH_WelcomeGuide
            {...this.props}
            ref={this.refSH_WelcomeGuide}
            y={greenBarBottomY}
            onOverlayPress={() => {
              customPageHandler.presentNewPersonPage();
              this.setState({ isUFirstTime: false });
            }}
          />
        ) : null}
        <SH_VersionUpdate {...this.props} ref={this.refSH_VersionUpdate} />
        {admin ? (
          <Buttoon
            icon={{ name: "plus" }}
            size="large"
            onPress={() => this.props.navigation.navigate("posst-creator-scr")}
            style={{
              position: "absolute",
              right: spacing(2),
              bottom: spacing(5),
              // borderRadius: 100,
            }}
          />
        ) : null}
      </>
    );
  }
}

export function $_HomeFeed(props) {
  const { isUFirstTime, onScoreboardPress } = props;
  const [_account, setAccount] = React.useState(null);
  const [slides, setSlides] = React.useState<PosstSchema[]>([]);

  const [sectionVisibility, setSectionVisibility] = useState(false);
  React.useEffect(function staging() {
    setTimeout(() => {
      setSectionVisibility(true);
    }, 1000);
  }, []);

  React.useEffect(function fetchStuff() {
    Backend.firestoreHandler.account(setAccount);
    // Backend.firestoreHandler.mediaSlides((_slides) => {
    //   setSlides(_slides);
    // });
    Backend.exploreHandler.possts((posts) => {
      // console.log(JSON.stringify(posts));
      setSlides(posts.filter((pst: PosstSchema) => pst.pinned === true));
    });
  }, []);

  const { disableScoreboard } = Backend.firestoreHandler._config.variables;

  return (
    sectionVisibility && (
      <>
        <ScrollView style={{ backgroundColor: C.background01 }}>
          {!disableScoreboard ? (
            <$_Scoreboards {...props} isUFirstTime={isUFirstTime} />
          ) : null}
          <MediaSlider {...props} slides={slides} />
          <S_Explore {...props} />
          <View style={{ height: spacing(2) }} />
        </ScrollView>
      </>
    )
  );
}

/**
 * ### Section displaying simple greetings + name.
 *  - If !name (usually when u first sign up ), there's a button navigating to "you-scr"
 *  - If u are new, there's a green ribbon "New? Start here!"
 * ---
 * @version 0.10.29
 */
export function $_Header(props) {
  const { navigation, onNewPress, onMeasure } = props;
  const MenuIcon = (name: ICON_NAME, props: dAccessory) => (
    <IconPrimr name={name} size={props.style.width} color={C.text01} />
  );

  const [_account, setAccount] = React.useState<UserSchema>(null);
  const [_config, setConfig] = React.useState<dConfig>(null);
  const [_calendarEvents, setCalendarEvents] = React.useState<dCalendarEvent[]>(
    []
  );
  const [showNewRepButton, setShowNewRepButton] = React.useState(false);
  const [, setNotifications] = React.useState([]);

  React.useEffect(function fetchBackend() {
    Backend.firestoreHandler.account((account) => setAccount(account));
    Backend.firestoreHandler.config((config) => setConfig(config));
    Backend.notificationFirestore.global((notifications) =>
      setNotifications(notifications)
    );
    Backend.firebasePages.pages(() => {
      const show = customPageHandler.getNewPersonPage() ? true : false;
      if (show !== showNewRepButton) {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      }
      setShowNewRepButton(show);

      Backend.calendarHandler.calendar((calendarEvents) =>
        setCalendarEvents(calendarEvents)
      );
    });
  }, []);

  const todaysEvents = _calendarEvents.filter(
    (e) => dateFormat(e.endDate) === dateFormat(new Date())
  );

  const name = _account?.name;

  let unreadNotifications = Backend.notificationFirestore.getUnreadNotifications();

  const newPersonRef = React.useRef<View>();

  return (
    <View>
      <Kitten.TopNavigation
        style={{
          // paddingTop: getStatusBarHeight("safe", "ios-only"),
          paddingHorizontal: spacing(3),
          backgroundColor: C.background01,
        }}
        title={`Hi ${!!name ? fn.js.capitalize(name) : "👋"}`}
        accessoryRight={() => (
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-evenly",
            }}
          >
            {!_config?.variables.disableCalendar ? (
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Calendar");
                }}
              >
                <View
                  style={{
                    padding: 7,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <IconPrimr
                      preset={`safe`}
                      name={"calendar"}
                      size={moderateScale(22)}
                      color={"white"}
                    />
                    {/* {todaysEvents.length > 0 ? (
                      <Txt.P2
                        style={{
                          position: "absolute",
                          top: 11.74,
                          left: "32.5%",
                          color: C.errorRed,
                          fontWeight: "700",
                        }}
                      >
                        •
                      </Txt.P2>
                    ) : null} */}
                    {todaysEvents.length > 0 && (
                      <SS.NotificationCounter>
                        <Txt.C1 style={{ fontWeight: "bold" }}>
                          {todaysEvents.length}
                        </Txt.C1>
                      </SS.NotificationCounter>
                    )}
                  </View>
                </View>
              </TouchableOpacity>
            ) : null}
            {_config?.variables.enablePushNotifications ? (
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Notifications");
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <IconPrimr
                    preset={`safe`}
                    name={"bell"}
                    size={moderateScale(22)}
                    color={"white"}
                  />
                  {unreadNotifications.length > 0 && (
                    <SS.NotificationCounter>
                      <Txt.C1 style={{ fontWeight: "bold" }}>
                        {unreadNotifications.length}
                      </Txt.C1>
                    </SS.NotificationCounter>
                  )}
                </View>
              </TouchableOpacity>
            ) : null}
          </View>
        )}
      />

      {showNewRepButton ? (
        <View
          ref={newPersonRef}
          onLayout={() => {
            newPersonRef.current?.measure((fx, fy, width, height, x, y) => {
              console.log(
                "new rep measurements: " + fy,
                +" " + height + " " + y
              );
              onMeasure(fx, fy, width, height, x, y);
            });
          }}
        >
          <Kitten.MenuItem
            title="Start Here!"
            style={{
              backgroundColor: C.awakenVolt,
            }}
            onPress={() => {
              customPageHandler.presentNewPersonPage();
              // onNewPress();
            }}
            accessoryLeft={(props: dAccessory) => MenuIcon("compass", props)}
            accessoryRight={(props: dAccessory) =>
              MenuIcon("chevron_right", props)
            }
          />
        </View>
      ) : null}
    </View>
  );
}

function $_Scoreboards(props) {
  return S_Scoreboards(props);
}

export const MediaSlider = (props: { slides: PosstSchema[] }) => {
  const { slides } = props;
  const [_activeSlide, setActiveSlide] = React.useState(0);

  const _renderItem = ({ item }) => {
    const posst: PosstSchema = item;
    const image = posst.medias.length > 0 ? posst.medias[0].uri : null;
    return (
      <CardCover
        {...props}
        title={posst.body}
        // subtitle={item.description}
        image={image}
        link={item.link}
        onPress={() => posstPress(posst)}
        onPinPress={async () => {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
          await Backend.exploreHandler.unpinPosst(posst);
          Toasty({ text1: "Post unpinned." });
        }}
        containerStyle={{
          height: scale(170),
        }}
      />
    );
  };

  return (
    <View
      style={{
        paddingTop: spacing(4),
        paddingBottom: spacing(1),
      }}
    >
      {slides.length > 0 && (
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Txt.$Title>Pinned</Txt.$Title>
          <IconPrimr
            preset="default"
            name={"pin"}
            color={C.dim}
            size={scale(14)}
          />
        </View>
      )}

      <Carousel
        {...props}
        data={slides}
        loop={true}
        renderItem={_renderItem}
        sliderWidth={DEVICE_WIDTH}
        itemWidth={DEVICE_WIDTH * 0.8}
        onSnapToItem={(index) => setActiveSlide(index)}
      />
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        {slides
          ? slides.map((d, index) => (
              <Txt
                style={[
                  { textAlign: "center" },
                  index == _activeSlide
                    ? { color: C.text01, fontSize: 24 }
                    : { color: C.grey500, fontSize: 18 },
                ]}
              >
                {" "}
                •{" "}
              </Txt>
            ))
          : null}
      </View>
    </View>
  );
};

const CardCover = (props) => {
  const {
    title,
    subtitle,
    image,
    onPress,
    containerStyle = {},
    onPinPress,
  } = props;

  let _title = subtitle !== "invisible" ? title : null;
  _title =
    _title &&
    _title.replace(extractLinkFromBody(_title)?.replace(/\\n/g, "\n"), "🔗");

  // const _subtitle =
  //   subtitle &&
  //   typeof subtitle === "string" &&
  //   subtitle.length > 1 &&
  //   subtitle !== "invisible"
  //     ? subtitle
  //     : null;

  return (
    <>
      <View
        style={{
          height: scale(172),
          padding: scale(10),
          margin: scale(5),
        }}
      >
        <TouchableWithoutFeedback
          onPress={() => (onPress ? onPress(props) : null)}
        >
          <ImageBackground
            {...props}
            resizeMode="contain"
            source={{
              uri: image,
            }}
            imageStyle={{ justifyContent: "flex-start" }}
            style={{
              height: scale(172),
              padding: scale(10),
              // margin: scale(5),
              justifyContent: "flex-end",
              ...containerStyle,
            }}
          >
            <Txt.S2 numberOfLines={2}>{_title}</Txt.S2>
            {/* <Txt category="s1">{_subtitle}</Txt> */}
          </ImageBackground>
        </TouchableWithoutFeedback>
      </View>
      <IconPrimr
        onPress={onPinPress}
        name="pin"
        size={scale(14)}
        color={C.hazardYellow}
        preset="circular"
        containerStyle={{
          backgroundColor: C["color-warning-transparent-500"],
          position: "absolute",
          bottom: spacing(4),
          right: spacing(4),
        }}
      />
    </>
  );
};

const SS = {
  NotificationCounter: sstyled(View)((p) => ({
    marginTop: 5.7,
    backgroundColor: p.C.errorRed,
    paddingHorizontal: spacing(3),
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  })),
};
