import { IndexPath } from "@ui-kitten/components";
import Backend from "backend/";
import { Buttoon, IconPrimr, Kitten, Toasty, Txt } from "components/";
import R from "ramda";
import * as React from "react";
import { Image, TouchableOpacity, View } from "react-native";
import firebase from "react-native-firebase";
import { C, fn, IPSCR, PRE, dImageInfo, scale, spacing } from "utilities/";
import { dPhoto12 } from "utilities/functions/media-functions/media-picker.props";
import { SS } from "./UserPage";

interface P$_profile extends IPSCR {}

export const S_Profile = (props: P$_profile) => {
  const {
    // theme: { C },
} = props;

  // if (!Backend.firestoreHandler._account) return null;

  const [_account, setAccount] = React.useState(null);
  const [visible, setVisible] = React.useState(false);

  React.useEffect(function fetchAccount() {
    // Backend.firestoreHandler.account((account) => setAccount(account));
    Backend.firestoreHandler.account(account => setAccount(account))
  }, []);

  if (!_account) return null;

  const { profileImage } = _account;
  // const [_avatar, setAvatar] = React.useState(profileImage);

  /**
   * Handle Image uploaded from Camera or Gallery
   */
  async function _handleImage(
    imageInfo: dImageInfo,
    shouldBase: { userID: string; bucket: "users" } = {
      userID: Backend.firestoreHandler.uid,
      bucket: "users",
    }
  ) {
    Toasty({ text1: "Updating Profile Image...", type: "info" });
    setVisible(false);
    if (R.isNil(shouldBase)) {
      Backend.firestoreHandler.updateAccount("", {
        profileImage: imageInfo.path,
      });
    } else {
      const { bucket, userID } = shouldBase;
      const refPath = `${bucket}/${userID}/00_${userID}_${fn.js.ID()}.png`;
      await firebase.storage().ref(refPath).putFile(imageInfo.path);
      const uri = await firebase.storage().ref(refPath).getDownloadURL();
      await Backend.firestoreHandler.updateAccount("", {
        profileImage: uri,
      });
      // setAvatar(uri);
      Toasty({ text1: "Profile Image Updated!" });
    }
  }

  const onItemSelect = (index: IndexPath) => {
    setVisible(false);
    switch (index.row) {
      case 0:
        return fn.media
          .photoFromCamera()
          .then((frCam: dPhoto12) => {
            // console.log("frCam: ", frCam);
            _handleImage(frCam.imageInfo);
          })
          .catch(() => {
            /** handle internally */ setVisible(false);
          });
        break;
      case 1:
        return fn.media
          .photoFromGallery()
          .then((frGallery: dPhoto12) => {
            _handleImage(frGallery.imageInfo);
          })
          .catch(() => {
            /** handle internally */ setVisible(false);
          });
        break;
    }
  };

  return (
    <>
      <View style={PRE(C).HEADER.SEPARATOR_CTNR}>
        <Txt style={PRE(C).HEADER.LABEL_TXT}>Profile</Txt>
      </View>

      <View style={[SS(C).$_CONTENT_CTNR, { alignItems: "center" }]}>
        <Kitten.OverflowMenu
          anchor={() => (
            <View>
              <ProfilePhotoCTNR
                onPress={() => setVisible(true)}
                profileImage={profileImage}
              />
            </View>
          )}
          visible={visible}
          onSelect={onItemSelect}
          onBackdropPress={() => setVisible(false)}
        >
          <Kitten.MenuItem title="Take Photo" />
          <Kitten.MenuItem title="Choose Photo" />
        </Kitten.OverflowMenu>
      </View>
    </>
  );
};

/**
 *  (🚩1) Pick and show image (SECTION Placeholder<->)
 *    In the frontal <-> (e.g.the one in ProSttScr.js):
 *      Image is picked thru [(🚩1.1.1&2) photoFromCamera(), photoFromGallery()]
 *        -> `handleImage()` is fired -> setImageInfo -> `imageInfo` is updated
 *    In this internal <-> (this file):
 *      `setImageInfo()` whenever receive new `imageInfo`
 *        -> `_imageInfo` updated -> pic shown (~ 1st `useEffect()` (🚩1.2))
 *
 */
const ProfilePhotoCTNR = (props: PROPS_ImgCtnr) => {
  const {
    // theme: { C },
    onPress,
    profileImage,
  } = props;

  switch (!!profileImage) {
    case true:
      return (
        <>
          <Image
            source={{ uri: !!profileImage ? profileImage : "" }}
            style={{
              ...SS(C).IMG_CTNR,
              justifyContent: "center",
              alignItems: "center",
            }}
          />
          <Buttoon appearance="ghost" onPress={onPress}>
            Change
          </Buttoon>
        </>
      );
      break;
    case false:
      return (
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <TouchableOpacity
            onPress={onPress}
            style={{
              ...SS(C).IMG_CTNR,
              backgroundColor: C["color-primary-transparent-600"],
              borderWidth: scale(3),
              borderColor: C.primary,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <IconPrimr name={"plus"} color={C.text01} size={scale(60)} />
          </TouchableOpacity>
          <Buttoon appearance="ghost" onPress={onPress}>
            Add Profile Image
          </Buttoon>
        </View>
      );
      break;
    default:
      return (
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <TouchableOpacity
            onPress={onPress}
            style={{
              ...SS(C).IMG_CTNR,
              backgroundColor: C["color-primary-transparent-600"],
              borderWidth: scale(3),
              borderColor: C.primary,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <IconPrimr name={"plus"} color={C.text01} size={scale(60)} />
          </TouchableOpacity>
          <Txt
            category="s2"
            style={{ textAlign: "center", marginVertical: spacing(1) }}
          >
            Add Profile Picture
          </Txt>
        </View>
      );
      break;
  }
};

interface PROPS_ImgCtnr {
  onPress(): void;
  profileImage?: string;
}
