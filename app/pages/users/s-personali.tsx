import Backend from "backend/";
import { Inpuut, Txt } from "components/";
import R from "ramda";
import * as React from "react";
import { View } from "react-native";
import { C, IPSCR, PRE } from "utilities/";
import { SS } from "./UserPage";

interface P$_personali extends IPSCR {}

export const S_Personali = (props: P$_personali) => {
  const {
    // theme: { C },
  } = props;

  if (!Backend.firestoreHandler._account) return null;

  const [_account, setAccount] = React.useState(null);
  React.useEffect(function fetchAccount() {
    Backend.firestoreHandler.account((account) => {
      setName(account.name ? account.name : "");
      setEmail(account.email ? account.email : "");
      setPhoneNumber(account.phoneNumber ? account.phoneNumber : "");
      setAccount(_account);
    });
  }, []);

  const { name, email, phoneNumber } = Backend.firestoreHandler._account;
  const [_name, setName] = React.useState(name);
  const [_email, setEmail] = React.useState(email);
  const [_phone, setPhoneNumber] = React.useState(phoneNumber);

  return (
    <>
      <View style={PRE(C).HEADER.SEPARATOR_CTNR}>
        <Txt style={PRE(C).HEADER.LABEL_TXT}>Personal Info</Txt>
      </View>
      <View style={[SS(C).$_CONTENT_CTNR]}>
        <Inpuut
          title="Your Name"
          autoCapitalize="words"
          doneText="Save"
          status={(R.isNil(_name) || _name == "") && "warning"}
          value={_name}
          onChangeText={setName}
          onSavePress={() =>
            Backend.firestoreHandler.updateAccount("", {
              name: _name,
            })
          }
        />
        <Inpuut
          title="Phone Number"
          doneText="Save"
          status={(R.isNil(_phone) || _phone == "") && "warning"}
          value={_phone}
          onChangeText={setPhoneNumber}
          onSavePress={() =>
            Backend.firestoreHandler.updateAccount("", {
              phoneNumber: _phone,
            })
          }
        />
        <Inpuut title="Email" doneText="Save" value={_email} disabled />
        {/* <Kitten.Input
          status={(R.isNil(_name) || _name == "") && "warning"}
          style={{ color: C.text, backgroundColor: C.background }}
          placeholder={"Your Name"}
          label={"First & Last Name"}
          value={_name}
          onChangeText={setName}
          onSubmitEditing={() =>
            Backend.firestoreHandler.updateAccount("", {
              name: _name,
            })
          }
        />

        <Kitten.Input
          status={(R.isNil(_phone) || _phone == "") && "warning"}
          style={{ color: C.text, backgroundColor: C.background }}
          placeholder={"Phone Number"}
          label={"Phone Number"}
          value={_phone}
          onChangeText={setPhoneNumber}
          onSubmitEditing={() =>
            Backend.firestoreHandler.updateAccount("", {
              phoneNumber: _phone,
            })
          }
        />
        <Kitten.Input
          status={(R.isNil(_email) || _email == "") && "warning"}
          disabled={true}
          style={{ color: C.text, backgroundColor: C.background }}
          placeholder={"Email"}
          label={"Email"}
          value={_email}
          onChangeText={setEmail}
        /> */}
      </View>
    </>
  );
};
