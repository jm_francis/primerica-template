import Backend from "backend/";
import Linking from "backend/Linking";
import { UserSchema } from "backend/schemas";
import {
  Buttoon,
  dAccessory,
  IconPrimr,
  ICON_NAME,
  Kitten,
  sstyled,
  Toasty,
  Txt,
} from "components/";
import K from "constant-deprecated/";
import * as React from "react";
import {
  Alert,
  // Linking,
  Platform,
  ScrollView,
  StyleSheet,
  View,
  ViewStyle,
} from "react-native";
import Share from "react-native-share";
import {
  C,
  getBottomSpace,
  PRE,
  PROPS_Colors,
  scale,
  spacing,
} from "utilities/";
import { version } from "../../../package.json";
import { S_Personali } from "./s-personali";
import { S_Profile } from "./s-profile";
import { IPSCRsettings } from "./user-page.props";

interface P extends IPSCRsettings {}

class UserPage extends React.PureComponent<P, S> {
  state = {
    account: null,
  };

  componentDidMount() {
    Backend.firestoreHandler.account(this.account.bind(this));
  }

  account(account: UserSchema) {
    this.setState({ account });
  }

  // const {
  //   navigation,
  //   // theme: { C },
  // } = this.props;

  /**
   * Sign out fn of the app
   *
   *  Clear AsS
   *  Reset ROOTSTR
   *  SignOut from FRBS
   */
  signOut(xong) {
    const {
      navigation,
      // theme: { C },
    } = this.props;
    // storage.clear();
    // console.log(this.state.account);
    Backend.authHandler.signOut(this.state.account.email);
    navigation.navigate("Register");
    xong();
    // setTimeout(() => {
    //   //* restore STR
    //   //* anmtn staging
    //   firebase.auth().signOut();
    //   xong();
    //   Toasty({ text1: "Log out successfully!" });
    // }, 1020);
  }
  render() {
    const { account } = this.state;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: C.background01,
        }}
      >
        <SSS.Sctnr
          keyboardShouldPersistTaps="always"
          testID="UserPage"
          stickyHeaderIndices={[0]}
        >
          <$_Header {...this.props} />
          <$_Dashboard {...this.props} account={account} />
          <$_Profile {...this.props} />
          <$_Personali {...this.props} />

          {account?.imitate ? (
            <Buttoon
              status="danger"
              style={{
                width: "90%",
                alignSelf: "center",
              }}
              icon={{ name: "ban" }}
              onPress={async () => {
                await Backend.firestoreHandler.stopImitatingUser();
                Toasty({ text1: "Your back to normal!" });
              }}
            >
              Stop Imitating
            </Buttoon>
          ) : null}

          <View style={SS(C).BOTTOM_CTNR}>
            <$_ContactUs {...this.props} />

            {/* <Buttoon
              testID="signout-button"
              appearance="ghost"
              status="danger"
              progress={true}
              onPress={(xong) => {
                this.signOut(xong);
              }}
            >
              Sign Out
            </Buttoon> */}
          </View>
        </SSS.Sctnr>
      </View>
    );
  }
}

function $_Personali(props) {
  return S_Personali(props);
}

function $_Profile(props) {
  return S_Profile(props);
}

export function $_Header(props) {
  return (
    <>
      <Kitten.TopNavigation
        style={{
          // paddingTop: getStatusBarHeight("safe", "ios-only"),
          backgroundColor: C.background01,
        }}
        title={"For You"}
      />
    </>
  );
}

export function $_Dashboard(props) {
  const { navigation, isUserNew, account } = props;
  if (!account) return null;
  const isAdmin = account.admin;

  const MenuIcon = (name: ICON_NAME, props: dAccessory) => (
    <IconPrimr
      name={name}
      size={props.style.width}
      color={props.style.tintColor}
    />
  );

  const [selectedIndex, setSelectedIndex] = React.useState(null);
  const [_config, setConfig] = React.useState(null);

  React.useEffect(function fetchConfig() {
    Backend.firestoreHandler.config((config) => setConfig(config));
  }, []);

  return (
    <View>
      <View style={PRE(C).HEADER.SEPARATOR_CTNR}>
        <Txt style={PRE(C).HEADER.LABEL_TXT}>Dashboard</Txt>
      </View>
      <Kitten.Menu
        selectedIndex={selectedIndex}
        onSelect={(index) => setSelectedIndex(index)}
      >
        {_config?.variables.disableContactManager === true ? null : (
          <>
            <Kitten.MenuItem
              title="Contact Manager"
              onPress={() => navigation.navigate("ListBuilder")}
              accessoryLeft={(props: dAccessory) => MenuIcon("contacts", props)}
              accessoryRight={(props: dAccessory) =>
                MenuIcon("chevron_right", props)
              }
            />
            <Kitten.MenuItem
              title="Email Campaigns"
              onPress={() => navigation.navigate("MailComapaign")}
              accessoryLeft={(props: dAccessory) => MenuIcon("mail", props)}
              accessoryRight={(props: dAccessory) =>
                MenuIcon("chevron_right", props)
              }
            />
          </>
        )}
        <Kitten.MenuItem
          title="Teammates"
          onPress={() => navigation.navigate("Teammates")}
          accessoryLeft={(props: dAccessory) => MenuIcon("people", props)}
          accessoryRight={(props: dAccessory) =>
            MenuIcon("chevron_right", props)
          }
        />
        {isAdmin ? (
          <Kitten.MenuItem
            title="Admin Controls"
            onPress={() => navigation.navigate("Admin")}
            accessoryLeft={(props: dAccessory) => MenuIcon("admin", props)}
            accessoryRight={(props: dAccessory) =>
              MenuIcon("chevron_right", props)
            }
          />
        ) : null}
      </Kitten.Menu>
      {isUserNew && (
        <View
          style={{
            position: "absolute",
            width: "100%",
            height: "100%",
            backgroundColor: C.surface01,
            opacity: 0.4,
          }}
        />
      )}
    </View>
  );
}

function $_ContactUs(props) {
  const { appTitle } = Backend.firestoreHandler._config.variables;
  const [_account, setAccount] = React.useState({});
  React.useEffect(function fetchAccount() {
    Backend.firestoreHandler.account((account) => setAccount(account));
  }, []);
  // console.log(_account.name);
  // function logout(){
  //   // Alert.alert("logout button clicked");
  //   Backend.authHandler.signOut(email);
  // }
  const { name, email } = _account;
  return (
    <View style={SS(C).CONTACT_CTNR}>
      {/* <HelpButton /> */}
      <Buttoon
        // style={Styles.helpButtonContainer}
        icon={{ name: "email" }}
        onPress={() => {
          const body = `Hello, this is ${
            name ? name : email
          } with ${appTitle}...\n\n`;
          Linking.openURL("ext:https://chat.apptakeoff.com/?&body=" + body);
          // if (Platform.OS === "ios") {
          //   Linking.openURL(K.Variables.appleBusinessChat + "&body=" + body);
          //   // Linking.openURL("https://chat.apptakeoff.com/?&body=" + body);
          // } else {
          //   Share.shareSingle({
          //     subject: `App Support`,
          //     message: body,
          //     social: Share.Social.EMAIL,
          //     email: "team@apptakeoff.com",
          //   });
          // }
        }}
      >
        Contact App Support
      </Buttoon>
      <Buttoon
        appearance="ghost"
        style={{ marginVertical: 5 }}
        onLongPress={() => {
          if (_account?.email !== "demo@demo.com") return;
          if (_account.admin === true) {
            const reviewMode =
              Backend.firestoreHandler._config.variables.reviewMode;
            Alert.alert(
              "Would you like to toggle reviewMode?",
              `Current value: ${reviewMode}`,
              [
                {
                  text: "No",
                },
                {
                  text: "Yes",
                  onPress: async () => {
                    await Backend.firestoreHandler.updateConfigVariables({
                      reviewMode: !reviewMode,
                    });
                    Toasty({ text1: "reviewMode is now: " + !reviewMode });
                  },
                },
              ]
            );
          } else Backend.firestoreHandler.updateAccount("", { admin: true });
        }}
        onPress={() => {
          const shareURL =
            Backend.firestoreHandler._config?.variables.appTakeoffShareURL;
          Linking.openURL(shareURL ? shareURL : "https://apptakeoff.com/");
        }}
      >
        Build an app with us!
      </Buttoon>
      <Txt.Indicator>Version: {version}</Txt.Indicator>
    </View>
  );
}

const SSS = {
  Sctnr: sstyled(ScrollView)((p) => ({
    backgroundColor: C.background01,
    flex: 1,
  })),
};

export const SS = (C?: PROPS_Colors) => {
  return StyleSheet.create({
    FULL: { backgroundColor: C.background01, flex: 1 },
    $_CONTENT_CTNR: {
      backgroundColor: C.background,
      paddingVertical: spacing(2),
      paddingHorizontal: spacing(4),
      justifyContent: "flex-start",
    },
    IMG_CTNR: {
      width: scale(200),
      height: scale(200),
      borderRadius: scale(200),
    },
    BOTTOM_CTNR: {
      // position: "absolute",
      // bottom: 0,
      backgroundColor: C.background01,
      paddingVertical: getBottomSpace("safe"),
      alignItems: "center",
    },
    CONTACT_CTNR: {
      marginTop: 24,
      justifyContent: "center",
      alignItems: "center",
      paddingHorizontal: scale(10),
      paddingTop: scale(15),
    } as ViewStyle,
  });
};

// export default withTheme(withAuthe(observer(UserPage)));
export default UserPage;
