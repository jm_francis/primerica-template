import Backend from "backend/";
import {
  Buttoon,
  dAccessory,
  IconPrimr,
  ICON_NAME,
  Inpuut,
  Kitten,
  Toasty,
  Txt,
} from "components/";
import React, { Component } from "react";
import { ScrollView, View } from "react-native";
import { C, getStatusBarHeight, moderateScale, spacing } from "utilities/";

export default class Developer extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <$_Header {...this.props} />
        <ScrollView
          style={Styles.container}
          keyboardShouldPersistTaps={"always"}
        >
          <$_Settings {...this.props} />
        </ScrollView>
      </View>
    );
  }
}

export function $_Header(props) {
  const { navigation } = props;
  const BackButton = (name: ICON_NAME, props: dAccessory) => (
    <IconPrimr
      name={name}
      size={18}
      color={C.text01}
      onPress={() => navigation.pop()}
    />
  );

  return (
    <>
      <Kitten.TopNavigation
        title="Developer"
        style={{
          backgroundColor: C.background01,
        }}
        alignment="center"
        accessoryLeft={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              navigation.pop();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          />
        )}
      />
    </>
  );
}

function $_Settings(props) {
  const [releaseVersion, setReleaseVersion] = React.useState("");
  const [updateURL, setUpdateURL] = React.useState("");
  const [_pushNotificationsEnabled, enablePushNotifications] = React.useState(
    false
  );

  React.useEffect(function fetchConfig() {
    Backend.firestoreHandler.config((config) => {
      setReleaseVersion(config.variables.releaseVersion);
      setUpdateURL(config.variables.updateURL);
      enablePushNotifications(config.variables.enablePushNotifications);
    });
  }, []);

  async function saveVariable(data: Object) {
    try {
      await Backend.firestoreHandler.updateConfigVariables(data);
      Toasty({ text1: "Save complete!" });
    } catch (err) {
      Toasty({ type: "error", text1: err.message });
    }
  }
  function saveReleaseVersion() {
    saveVariable({ releaseVersion });
  }
  function saveUpdateURL() {
    saveVariable({ updateURL });
  }
  async function updatePushNotifications() {
    try {
      await Backend.firestoreHandler.updateConfigVariables({
        enablePushNotifications: !_pushNotificationsEnabled,
      });
      Toasty({
        text1: `${
          !_pushNotificationsEnabled ? "Enabled" : "Disabled"
        } Push Notifications.`,
        type: !_pushNotificationsEnabled ? "success" : "info",
      });
    } catch (err) {
      Toasty({ type: "error", text1: err.message });
    }
  }

  const app_name = Backend.firestoreHandler._config.variables.appTitle
    .replace(/ /g, "")
    .toLowerCase();
  const defaultUpdateURL = `https://get-my.app/t/${app_name}/?update=1`;

  return (
    <View
      style={{
        alignSelf: "center",
        marginHorizontal: spacing(4),
        marginVertical: spacing(4),
      }}
    >
      <Inpuut
        title="Release Version"
        value={releaseVersion}
        onChangeText={setReleaseVersion}
        onSavePress={saveReleaseVersion}
      />
      <Inpuut
        isUrl
        title="App Update URL"
        placeholder={defaultUpdateURL}
        value={updateURL}
        onChangeText={setUpdateURL}
        onSavePress={saveUpdateURL}
      />
      <SwitchItem
        title="Push Notifications"
        value={_pushNotificationsEnabled}
        onChange={() => {
          updatePushNotifications();
          enablePushNotifications(!_pushNotificationsEnabled);
        }}
      />
    </View>
  );
}

function SwitchItem(props: {
  title: string;
  value: any;
  onChange: () => void;
}) {
  const { title, value, onChange } = props;
  return (
    <View
      style={{
        marginVertical: spacing(2),
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
      }}
    >
      <Txt style={{ fontWeight: "bold" }}>{title}</Txt>
      <Kitten.Toggle checked={value} onChange={onChange} />
    </View>
  );
}

const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background,
  },
};
