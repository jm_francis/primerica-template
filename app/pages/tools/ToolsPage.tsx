import Backend from "backend/";
import { openZoomWithIdIfPossible } from "backend/apis/ZoomAppHandler";
import {
  handleAddToCalendarPress,
  handleLinkPress,
} from "backend/Calendar/CalendarFunctions";
import {
  Buttoon,
  dAccessory,
  IconPrimr,
  ICON_NAME,
  Kitten,
  sstyled,
  Toasty,
  Txt,
} from "components/";
import R from "ramda";
import * as React from "react";
import {
  Dimensions,
  ScrollView,
  StyleSheet,
  View,
  ViewStyle,
} from "react-native";
import ActionSheet from "react-native-actionsheet";
import Accordion from "react-native-collapsible/Accordion";
import firebase from "react-native-firebase";
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import HTML from "react-native-render-html";
import {
  C,
  getBottomSpace,
  moderateScale,
  PROPS_Colors,
  scale,
  spacing,
} from "utilities/";
import {
  dateFormat,
  dCalendarEvent,
} from "utilities/functions/calendar-functions";
import { IPSCRsettings } from "./tools-page.props";

interface P extends IPSCRsettings {}

class ToolsPage extends React.PureComponent<P, S> {
  static navigationOptions = {
    title: "Schedule",
  };

  state = {
    disableCalendar: false,
  };

  componentDidMount() {
    Backend.firestoreHandler.config(this.config.bind(this));
  }

  config(config) {
    const { disableCalendar } = config.variables;
    this.setState({ disableCalendar });
  }

  signOut(xong) {
    const {
      navigation,
      // theme: { C },
    } = this.props;
    navigation.navigate("Register");
    setTimeout(() => {
      //* restore STR
      //* anmtn staging
      firebase.auth().signOut();
      xong();
      Toasty({ text1: "Log out successfully!" });
    }, 1020);
  }
  render() {
    const { disableCalendar } = this.state;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: C.background01,
        }}
      >
        <ScrollView style={SS(C).FULL} stickyHeaderIndices={[0]}>
          <$_Header {...this.props} />
          <$_Meetings {...this.props} />
          {!disableCalendar ? <$_Calendar {...this.props} /> : null}
        </ScrollView>
      </View>
    );
  }
}

export function $_Header(props) {
  return (
    <>
      <Kitten.TopNavigation
        title="Schedule"
        style={{
          backgroundColor: C.background01,
        }}
        // alignment="center"
        accessoryLeft={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              props.navigation.pop();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          />
        )}
      />
    </>
  );
}

function arrayIncludesDate(dateArray: Date[], date: Date) {
  for (let d in dateArray) {
    const someDate = dateArray[d];
    if (dateFormat(someDate) === dateFormat(date)) return true;
  }
  return false;
}

function $_Calendar(props) {
  const { navigation, isAdmin, isUserNew } = props;
  const [date, setDate] = React.useState(new Date());
  const [_events, setEvents] = React.useState<dCalendarEvent[]>([]);

  React.useEffect(function fetchCalendarData() {
    Backend.calendarHandler.calendar((events: dCalendarEvent[]) => {
      setEvents(events);
    });
  }, []);

  function renderDayCell({ date }, style) {
    const eventDates = R.pluck("date")(_events);
    return (
      <View style={[styles.dayContainer, style.container]}>
        <Txt style={style.text}>{`${date.getDate()}`}</Txt>
        {arrayIncludesDate(eventDates, date) ? (
          <IconPrimr
            preset={"safe"}
            name={"dot"}
            size={10}
            color={C.awakenVolt}
          />
        ) : null}
      </View>
    );
  }

  return (
    <View>
      <Kitten.Calendar
        style={{ width: "100%" }}
        date={date}
        min={new Date("1950/12/31")}
        max={new Date("2090/12/31")}
        filter={(date) => {
          if (!_events) return false;
          const eventDates = R.pluck("date")(_events);
          if (arrayIncludesDate(eventDates, date)) return true;
          return false;
        }}
        onSelect={(nextDate) => setDate(nextDate)}
        renderDay={renderDayCell}
      />
      <$_Events {...props} date={date} events={_events} />
    </View>
  );
}

function $_Events(props) {
  const { date, events } = props;

  const [_actionSheetOptions, setActionSheetOptions] = React.useState([
    "Add to Calendar",
    "Go to Link",
    "Cancel",
  ]);

  const todaysEvents = R.filter(
    (event: dCalendarEvent) => dateFormat(event.date) === dateFormat(date),
    events
  );

  const selectedEvent = React.useRef(null);
  const itemActionSheet = React.useRef(null);

  function itemActionSelected(index: number) {
    if (index === 0) {
      // add to calendar
      handleAddToCalendarPress(selectedEvent.current);
    } else if (index === 1) {
      // go to link
      handleLinkPress(selectedEvent.current);
    }
  }

  function onItemPress(e: dCalendarEvent) {
    selectedEvent.current = e;
    setActionSheetOptions([
      "Add to Calendar",
      e.description.includes("zoom.us") &&
      !e.description.includes("/webinar/register/")
        ? "Join Zoom Call"
        : "Go to Link",
      "Cancel",
    ]);
    setTimeout(() => {
      itemActionSheet.current.show();
    }, 25);
  }

  const monthAbbreviation = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  return (
    <>
      <Txt.$Title>Schedule</Txt.$Title>
      <Kitten.List
        data={todaysEvents}
        ItemSeparatorComponent={Kitten.Divider}
        renderItem={({
          item,
          index,
        }: {
          item: dCalendarEvent;
          index: number;
        }) => (
          <>
            <Kitten.ListItem
              style={{
                borderTopColor: C.background01,
                borderTopWidth: 10,
                backgroundColor: C.surface01,
              }}
              onPress={() => onItemPress(item)}
              title={item.title}
              description={item.time}
              accessoryRight={(p) => (
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    padding: spacing(3),
                    backgroundColor: C["color-primary-600"],
                    borderRadius: 10,
                  }}
                >
                  <Txt.H4>{item.date.getDate()}</Txt.H4>
                  <Txt.S1>{monthAbbreviation[item.date.getMonth()]}</Txt.S1>
                </View>
              )}
            />
            <View
              style={{
                paddingHorizontal: spacing(4),
                paddingBottom: 20,
                // paddingVertical: spacing(3),
                // borderBot: 10,
                borderBottomRightRadius: 10,
                backgroundColor: C.surface01,
              }}
            >
              {item.description ? (
                !(
                  item.description.includes("<a href") ||
                  item.description.includes("<html")
                ) ? (
                  <TouchableWithoutFeedback onPress={() => onItemPress(item)}>
                    <View>
                      <Txt.P2>{item.description}</Txt.P2>
                    </View>
                  </TouchableWithoutFeedback>
                ) : (
                  <TouchableWithoutFeedback onPress={() => onItemPress(item)}>
                    <View
                      style={{
                        overflow: "hidden",
                      }}
                    >
                      <HTML
                        tagsStyles={{ a: { color: "white" } }}
                        baseFontStyle={{ color: "white", fontSize: scale(17) }}
                        html={item.description}
                        imagesMaxWidth={Dimensions.get("window").width * 0.9}
                        onLinkPress={() => onItemPress(item)}
                      />
                      <View
                        style={{
                          position: "absolute",
                          top: 0,
                          width: Dimensions.get("window").width,
                          height: Dimensions.get("window").height,
                        }}
                      />
                    </View>
                  </TouchableWithoutFeedback>
                )
              ) : null}
            </View>
          </>
        )}
      />
      <ActionSheet
        ref={(ref) => (itemActionSheet.current = ref)}
        options={_actionSheetOptions}
        cancelButtonIndex={2}
        onPress={itemActionSelected}
      />
    </>
  );
}

export function $_Meetings(props) {
  const { navigation, isAdmin, isUserNew } = props;
  const [selectedIndex, setSelectedIndex] = React.useState([]);
  const [videoCalls, setVideoCalls] = React.useState([]);
  const [_teamPage, setTeamPage] = React.useState(null);
  const MenuIcon = (name: ICON_NAME, props: dAccessory) => (
    <IconPrimr
      name={name}
      size={props.style.width}
      color={props.style.tintColor}
    />
  );

  React.useEffect(function getTeamPage() {
    Backend.firebasePages.pages((pages) =>
      setTeamPage(Backend.firebasePages.getMyTeamPage())
    );
    Backend.firestoreHandler.config((config) =>
      setVideoCalls(config.variables.videoCalls)
    );
    Backend.firestoreHandler.account((account) => {
      setTeamPage(Backend.firebasePages.getMyTeamPage());
    });
  }, []);

  const sections = [];
  if (videoCalls?.length > 0) {
    sections.push({
      header: {
        title: "Video Calls",
        icon: "videocam",
      },
      contents: videoCalls
        .filter((call) => call.title.length > 0)
        .map((call) => ({
          title: call.title,
          onPress: () => openZoomWithIdIfPossible(call.meetingId),
        })),
    });
  }

  if (_teamPage && _teamPage.zoom) {
    sections.push({
      header: {
        title: `${_teamPage.name} Call`,
        icon: "flag",
      },
      contents: [
        {
          title: "Join Team Call",
          onPress: () => openZoomWithIdIfPossible(_teamPage.zoom),
        },
      ],
    });
  }

  return (
    <View
      style={{
        marginVertical: spacing(2),
      }}
    >
      <Accordion
        activeSections={selectedIndex}
        sections={sections}
        renderSectionTitle={() => <></>}
        renderHeader={(accordion) => (
          <Kitten.MenuItem
            title={accordion.header.title}
            style={{
              backgroundColor: C["color-primary-transparent-400"],
              borderColor: C.primary,
              borderWidth: 1,
              borderRadius: 5,
              margin: spacing(1),
            }}
            disabled={true}
            accessoryLeft={(props: dAccessory) =>
              MenuIcon(accordion.header.icon, props)
            }
            accessoryRight={(props: dAccessory) =>
              MenuIcon("arrow_down", props)
            }
          />
        )}
        renderContent={(accordion) => {
          return accordion.contents.map((item) => (
            <Kitten.MenuItem
              title={item.title}
              style={{
                paddingLeft: spacing(5),
                backgroundColor: C.background01,
              }}
              onPress={item.onPress}
              accessoryLeft={(props: dAccessory) => <Txt>•</Txt>}
              accessoryRight={(props: dAccessory) =>
                MenuIcon("chevron_right", {
                  ...props,
                  style: { ...props.style, tintColor: "white" },
                })
              }
            />
          ));
        }}
        onChange={setSelectedIndex}
      />
    </View>
  );
}

const SSS = {
  CardEvent: sstyled(Kitten.Card)((p) => ({
    marginVertical: spacing(3),
  })),
};

export const SS = (C?: PROPS_Colors) => {
  return StyleSheet.create({
    FULL: { backgroundColor: C.background01, flex: 1 },
    $_CONTENT_CTNR: {
      backgroundColor: C.background,
      paddingVertical: spacing(4),
      paddingHorizontal: spacing(4),
      justifyContent: "flex-start",
    },
    IMG_CTNR: {
      width: scale(200),
      height: scale(200),
      borderRadius: scale(200),
    },
    BOTTOM_CTNR: {
      backgroundColor: C.background01,
      paddingVertical: getBottomSpace("safe"),
      alignItems: "center",
    },
    CONTACT_CTNR: {
      marginTop: 24,
      justifyContent: "center",
      alignItems: "center",
      paddingHorizontal: scale(10),
      paddingTop: scale(15),
    } as ViewStyle,
  });
};

const styles = StyleSheet.create({
  dayContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    aspectRatio: 1,
  },
  value: {
    fontSize: 12,
    fontWeight: "400",
  },
});

// export default withTheme(withAuthe(observer(UserPage)));
export default ToolsPage;
