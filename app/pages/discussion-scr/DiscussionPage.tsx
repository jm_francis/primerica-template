import Backend from "backend/";
import { sstyled, Toasty, Txt } from "components/";
import { dDiscussionScrParams } from "navigation/";
import * as R from "ramda";
import * as React from "react";
import { Alert, Keyboard, LayoutAnimation, View } from "react-native";
import {
  Bubble,
  BubbleProps,
  GiftedChat,
  GiftedChatProps,
  IMessage,
  Send,
  SendProps,
} from "react-native-gifted-chat";
import { NavigationStackScreenProps } from "react-navigation-stack";
import { C, IS_ANDROID, PROPS_Colors, scale, spacing } from "utilities/";

/**
 * ### Discussion page of selected post.
 * ---
 * @version 1.1.22
 * @author K
 */
export function DiscussionPage(props: P) {
  const { navigation } = props;
  let _posstItem = navigation.getParam("item");
  let _isCommentPressed = navigation.getParam("section");
  const refChat = React.useRef<GiftedChat>();

  //! SM: fetch the current user id
  const _currentUser = Backend.firestoreHandler._account;

  // _posstItem?._pid : _pid

  //TODO @K->@SM Fill the value from the comments sub-collection belonging to a specific post
  const [comments, setComments] = React.useState<IMessage[]>([]);

  const onSend = React.useCallback((newMessages: IMessage[] = []) => {
    //@ts-ignore since CommentSchema extends IMessage
    // setComments(GiftedChat.append(comments, newMessages));
    console.log("posting comment: " + newMessages[newMessages.length - 1].text);
    Backend.exploreHandler.addComment(
      _posstItem,
      newMessages[newMessages.length - 1].text
    );
    Keyboard.dismiss();
  }, []);
  // const { C } = useAppContext();

  React.useEffect(() => {
    // _isCommentPressed == "comment" && refChat.current?.focusTextInput(); //! Not working, but not too important
    Backend.exploreHandler.openComments(_posstItem, (comments) => {
      let sortedComments = R.sortWith(
        [R.descend(R.prop("createdAt"))],
        comments
      );
      setComments(sortedComments);
    });
  }, []);
  React.useEffect(function closeComments() {
    return () => {
      console.log("CLOSE COMMENTS");
      Backend.exploreHandler.closeComments();
    };
  }, []);

  try {
    if (!!!_posstItem) throw Error("NO_ITEM");

    return (
      <SS.CTNR_COMMENTS>
        {/* //? Showing postcard is not the best design choice. 
            //?Might think of some other way */}
        {/* <PostCard item={_posstItem} /> */}
        <GiftedChat
          ref={refChat}
          messages={comments}
          // messagesContainerStyle={{
          //   fontSize: scale(16),
          // }}
          onSend={onSend}
          {...SS.CONFIG(C).CONVO}
          renderBubble={(p) => Bubbly({ ...p, C, pid: _posstItem._pid })}
          renderSend={(p) => Sendy({ ...p, C })}
          isKeyboardInternallyHandled={IS_ANDROID ? false : true}
          renderUsernameOnMessage={true}
          //TODO @K->@SM: use real current user data
          // _uid,username and avatar return these from the collection
          user={{
            _id: _currentUser.uid,
            name: _currentUser.name,
            avatar:
              _currentUser.profileImage &&
              _currentUser.profileImage.length > 0 &&
              _currentUser.profileImage,
          }}
        />
      </SS.CTNR_COMMENTS>
    );
  } catch (error) {
    switch (error.message) {
      case "NO_ITEM":
        return (
          <SS.CTNR_ERROR>
            <Txt.H6>Cannot load this discussion</Txt.H6>
          </SS.CTNR_ERROR>
        );
        break;
      default:
        console.warn("Error in disscusion-scr: ", error.message);
        return (
          <SS.CTNR_ERROR>
            <Txt.H6>Cannot load this discussion</Txt.H6>
          </SS.CTNR_ERROR>
        );
        break;
    }
  }
}
interface dSendy extends SendProps<IMessage> {
  C: PROPS_Colors;
}

/**
 * ### Custom Send Button<_> for CommentPage
 * ---
 * @version 0.12.2
 * @param props
 */
function Sendy(props: dSendy) {
  const { C } = props;
  return (
    <Send
      {...props}
      containerStyle={{
        justifyContent: "center",
        alignItems: "center",
        paddingRight: spacing(3),
      }}
    >
      {/* <Buttoon appearance="ghost">Send</Buttoon> */}
      <Txt.S1 style={{ color: C.primary }}>Send</Txt.S1>
    </Send>
  );
}
interface dBubbly extends BubbleProps<IMessage> {
  C: PROPS_Colors;
  pid: string;
}

/**
 * ### Custom Comment Bubble<_> for CommentPage
 * ---
 * @version 1.2.6
 * -  *Add Delete comment function*
 * @author  K
 */
function Bubbly(props: dBubbly) {
  const { C, pid } = props;
  return (
    <Bubble
      {...props}
      // renderMessageText={(p) => {
      //   <Txt>{p._id}</Txt>;
      // }}
      // position={"left"}
      wrapperStyle={{
        //* member's bubbly
        left: {
          width: "95%",
          backgroundColor: C["surface"],
        },
        //* u's bubbly
        right: {
          width: "95%",
          backgroundColor: C.primary,
        },
      }}
      textStyle={{
        left: { color: C.text, fontSize: scale(12) },
        right: {
          color: C.text01,
          fontSize: scale(12),
        },
      }}
      onLongPress={() => {
        Alert.alert("Confirmation", "Do you want to delete this comment?", [
          { text: "Cancel", onPress: () => {} },
          {
            text: "Yes",
            onPress: () => {
              LayoutAnimation.configureNext(
                LayoutAnimation.Presets.easeInEaseOut
              );
              Backend.exploreHandler.deleteComment({
                _pid: pid, //* posst id
                _id: props.currentMessage._id, //* comment id
              });
            },
          },
        ]);
      }}
    />
  );
}

const SS = {
  CTNR_COMMENTS: sstyled(View)((p) => ({
    flex: 1,
    backgroundColor: p.C.background,
  })),
  CTNR_ERROR: sstyled(View)(() => ({
    width: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  })),
  CONFIG: (C: PROPS_Colors) => ({
    CONVO: {
      renderAvatarOnTop: true,
      renderUsernameOnMessage: true,
      alwaysShowSend: true,
      showUserAvatar: true,
      alignTop: true,
      placeholder: "Comment",
      messagesContainerStyle: { backgroundColor: C.background },
      textInputStyle: {
        paddingLeft: spacing(3),
        backgroundColor: C.background,
        color: C.text,
        borderTopWidth: 0,
      },
      containerStyle: {
        backgroundColor: C.background,
        paddingRight: spacing(3),
      },
    } as GiftedChatProps,
  }),
};

interface P extends NavigationStackScreenProps<dDiscussionScrParams> {}
