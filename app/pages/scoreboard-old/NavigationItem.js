import { Fonts } from "constant-deprecated";
import { CROWN } from "constant-deprecated/Images";
import React from "react";
import {
  Dimensions, Image,
  Platform, Text,



  TouchableOpacity, View
} from "react-native";
import { moderateScale, verticalScale } from "constant-deprecated/node_modules/utilities";

export class NavigationItem extends React.Component {
  press() {
    this.props.navigation.navigate("Scoreboard");
  }

  render() {
    const { title = "Competition Scoreboard" } = this.props;
    const half =
      this.props.half === true && Platform.isPad ? { width: "48%" } : {};
    return (
      <View style={{ ...Styles.container, ...half }}>
        <TouchableOpacity
          style={Styles.touchable}
          onPress={this.press.bind(this)}
        >
          <View style={Styles.innerContainer}>
            <Image source={CROWN} style={Styles.icon} />
            <Text adjustsFontSizeToFit numberOfLines={1} style={Styles.title}>
              {title}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

const Styles = {
  container: {
    marginTop: 10,
    width: screenWidth * 0.95,
    height: moderateScale(70, 0.25),
    borderRadius: 13,
    overflow: "hidden"
  },
  innerContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: "10%",
    alignItems: "center",
    backgroundColor: "#e52020"
  },
  touchable: {
    width: "100%",
    height: "100%"
  },
  icon: {
    marginLeft: Platform.isPad ? -20 : 0,
    width: verticalScale(30),
    height: verticalScale(30)
  },
  title: {
    fontSize: moderateScale(25, 0.37),
    color: "rgb(237,237,237)",
    fontFamily: Fonts.medium,
    fontWeight: "600",
    maxWidth: "86%"
  }
};
