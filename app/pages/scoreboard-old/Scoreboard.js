import React, { Component } from "react";
import {
  ScrollView,
  View,
  Alert,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  LayoutAnimation,
  Platform,
  UIManager
} from "react-native";
import DialogInput from "react-native-dialog-input";
import RankingPane, { Styles as RPStyles } from "./panes/RankingPane";
import { getStatusBarHeight } from "react-native-iphone-x-helper";
import { Fonts } from "constant-deprecated/*";
import BackButton from "navigation/components/BackButton";
import { moderateScale, verticalScale, scale } from "constant-deprecated/node_modules/utilities";
import { defaultLayoutAnimation } from "pages/listBuilder/configure";
import { SCOREBOARD_BACKGROUND } from "constant-deprecated/Images";
import BlurView from "override/BlurView";

import Backend from "backend";
const { firestoreHandler, scoreboardHandler } = Backend;

export default class Scoreboard extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerMode: "float"
  });

  state = {
    pages: [],
    onTopItem: null,
    activeItemTitle: null,
    admin: false,
    newScoreboardDialogVisible: false
  };

  backButtonPress() {
    this.props.navigation.pop();
  }

  scores(pages) {
    LayoutAnimation.configureNext(defaultLayoutAnimation);
    this.setState({ pages });
  }
  componentDidMount() {
    if (Platform.OS === "android")
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);

    scoreboardHandler.scores(this.scores.bind(this));
    firestoreHandler.account(this.account.bind(this));
  }

  account(account) {
    this.setState({ admin: account.admin });
  }

  topItemBackPress() {
    LayoutAnimation.configureNext(defaultLayoutAnimation);
    this.setState({ onTopItem: null, activeItemTitle: null });
  }
  itemPressed(title) {
    this.scrollViewRef.scrollTo({ x:0, y:0 })
    if (Platform.OS === "ios") {
      this.setState({ activeItemTitle: title });
      return;
    }
    const { pages } = this.state;
    for (p in pages) {
      const page = pages[p];
      if (page.title === title) {
        this.setState({ onTopItem: page.title, activeItemTitle: title });
        break;
      }
    }
  }

  resetButtonPress() {
    Alert.alert(
      "Are you sure you would like to reset all of the scoreboards?",
      "",
      [
        { text: "Cancel", style: "cancel" },
        {
          text: "Yes!",
          onPress: () => {
            scoreboardHandler.resetScoreboard();
          }
        }
      ]
    );
  }

  newScoreboardPress() {
    this.setState({ newScoreboardDialogVisible: true });
  }
  newScoreboardSubmit(input) {
    scoreboardHandler.newScoreboard(input);
    this.setState({ newScoreboardDialogVisible: false });
  }

  render() {
    const {
      pages,
      admin,
      newScoreboardDialogVisible,
      activeItemTitle
    } = this.state;
    const items = [];
    let onTopItem = null;
    let activePage = null;
    for (p in pages) {
      const page = pages[p];
      if (page.title === this.state.onTopItem && Platform.OS === "android") {
        onTopItem = (
          <RankingPane
            data={page}
            onPress={this.itemPressed.bind(this)}
            onBackPress={this.topItemBackPress.bind(this)}
            forceActive={Platform.OS === "android"}
            subtitle={page.subtitle}
            key={`item.${page.title}`}
          />
        );
      }
      items.push(
        <RankingPane
          data={page}
          onPress={this.itemPressed.bind(this)}
          onBackPress={this.topItemBackPress.bind(this)}
          admin={admin}
          key={`item.${page.title}`}
        />
      );
    }

    const imageMod = { opacity: 0.65 };
    return (
      <>
        <View style={Styles.container}>
          {SCOREBOARD_BACKGROUND ? (
            <View style={Styles.backgroundImageContainer}>
              <Image
                source={SCOREBOARD_BACKGROUND}
                style={{ ...Styles.backgroundImage, ...imageMod }}
              />
            </View>
          ) : null}
          {!activeItemTitle ? (
            <View
              style={{ width: screenWidth, height: Styles.header.height }}
            />
          ) : null}
          <ScrollView ref={ref => this.scrollViewRef = ref} style={{ flex: 1 }} scrollEnabled={!activeItemTitle}>
            {items}
            {admin === true ? (
              <NewScoreboardButton
                onPress={this.newScoreboardPress.bind(this)}
              />
            ) : null}
            {admin === true ? (
              <ResetButton onPress={this.resetButtonPress.bind(this)} />
            ) : null}
            <View style={{ width: "100%", height: 52 }} />
          </ScrollView>
          {!activeItemTitle ? (
            <Header backButtonPress={this.backButtonPress.bind(this)} />
          ) : null}
          {onTopItem ? (
            <View
              style={{
                position: "absolute",
                width: screenWidth,
                height: screenHeight
              }}
            >
              {onTopItem}
            </View>
          ) : null}
        </View>
        <DialogInput
          isDialogVisible={newScoreboardDialogVisible}
          title="Enter a scoreboard name:"
          submitInput={this.newScoreboardSubmit.bind(this)}
          closeDialog={() =>
            this.setState({ newScoreboardDialogVisible: false })
          }
        />
      </>
    );
  }
}

class Header extends Component {
  render() {
    return (
      <BlurView blurAmount={10} style={Styles.header} disableAndroid>
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <BackButton
            style={Styles.backButton}
            onPress={this.props.backButtonPress}
          />
          <Text allowFontScaling={false} style={Styles.headerTitle}>
            Scoreboard
          </Text>
          <View style={Styles.backButton} />
        </View>
      </BlurView>
    );
  }
}
function ResetButton(props) {
  const { onPress } = props;
  const text = props.text ? props.text : "Reset all Scoreboards";
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={{
          ...RPStyles.itemContainer,
          ...RPStyles.globalContainer,
          backgroundColor: "rgba(245, 54, 54,0.75)"
        }}
      >
        <Text allowFontScaling={false} style={RPStyles.itemTitle}>
          {text}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
function NewScoreboardButton(props) {
  const { onPress } = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={{
          ...RPStyles.itemContainer,
          ...RPStyles.globalContainer,
          backgroundColor: "rgba(52, 235, 122, 0.9)"
        }}
      >
        <Text allowFontScaling={false} style={RPStyles.itemTitle}>
          {"New Scoreboard"}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

// const backgroundImageWidth = Platform.isPad ? { width: "100%" } : {};

const Styles = {
  container: {
    flex: 1,
    backgroundColor: "rgb(15,15,15)"
  },
  header: {
    position: "absolute",
    top: 0,
    width: screenWidth,
    height: moderateScale(getStatusBarHeight() + 60, 0.25),
    backgroundColor: "rgba(15, 106, 255, 0.5)",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  headerTitle: {
    color: "white",
    fontSize: verticalScale(20),
    fontFamily: Fonts.medium,
    marginBottom: 11
  },
  backButton: {
    marginTop: 2,
    width: verticalScale(22),
    height: verticalScale(22)
  },
  backgroundImageContainer: {
    position: "absolute",
    width: screenWidth,
    height: screenHeight,
    justifyContent: 'center',
    alignItems: 'center'
  },
  backgroundImage: {
    width: "100%",
    height: "100%"
  }
};
