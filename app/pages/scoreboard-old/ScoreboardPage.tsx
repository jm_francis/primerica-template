import { Txt } from "components/";
import * as React from "react";

/**
 * Page to display leaderboard
 * @param props
 */
export function ScoreboardPage(props) {
  return <Txt>Scoreboard Page</Txt>;
}
