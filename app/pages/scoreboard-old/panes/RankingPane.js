import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  Platform,
  Image,
  Dimensions,
  Alert,
  LayoutAnimation,
  UIManager,
} from "react-native";
import DialogInput from "react-native-dialog-input";
import ActionSheet from "react-native-actionsheet";
import { QuickStyle } from "react-native-shortcuts";
import { Fonts } from "constant-deprecated/*";
import { X_ICON } from "constant-deprecated/Images";
import {
  getStatusBarHeight,
  getBottomSpace,
} from "utilities/";
import { TouchableOpacity } from "react-native-gesture-handler";
import { defaultLayoutAnimation } from "pages/listBuilder/configure";
import { config } from "pages/scoreboard-old/config";
import { moderateScale, verticalScale, scale } from "constant-deprecated/node_modules/utilities";

import RankItem from "./components/RankItem";
import RankUpdateButton from "./components/RankUpdateButton";
import RankInput from "./components/RankInput";
import BlurView from "override/BlurView";

import Backend from "backend";
import { Buttoon } from "components";
const { scoreboardHandler } = Backend;

export default class RankingPane extends Component {
  state = {
    expanded: false,
    showRankInput: false,
    dialogInputData: null,
  };

  onPress() {
    const { onPress, data } = this.props;
    if (onPress) onPress(data.title);

    if (Platform.OS === "ios") {
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      this.setState({ expanded: true });
    }
  }
  backPress() {
    const { onBackPress } = this.props;
    if (onBackPress) onBackPress();

    if (Platform.OS === "ios") {
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      this.setState({ expanded: false });
    }
  }
  rankUpdatePress() {
    this.setState({ showRankInput: true });
  }
  rankInputBackgroundPress() {
    this.setState({ showRankInput: false });
  }

  rankInputDone(value) {
    const { data } = this.props;
    scoreboardHandler.setMyScore(Number.parseInt(value), data);
    this.setState({ showRankInput: false });
  }
  rankInputCancel() {
    this.setState({ showRankInput: false });
  }

  resetScoreboard() {
    const { data } = this.props;
    Alert.alert("Are you sure you would like to reset this scoreboard?", "", [
      { text: "Cancel", style: "cancel" },
      {
        text: "Yes!",
        onPress: () => {
          scoreboardHandler.resetScoreboard(data.title);
        },
      },
    ]);
  }
  deleteScoreboard() {
    const { data } = this.props;
    Alert.alert(
      "Are you sure you would like to DELETE this scoreboard?",
      "It will be gone forever!",
      [
        { text: "Cancel", style: "cancel" },
        {
          text: "Yes!",
          onPress: () => {
            this.backPress();
            scoreboardHandler.deleteScoreboard(data.title);
          },
        },
      ]
    );
  }
  editPress() {
    this.editActionSheet.show();
  }
  setNewTitle(input) {
    const { data } = this.props;
    this.backPress();
    scoreboardHandler.updateScoreboard(data.title, {
      title: input,
    });
    this.setState({ dialogInputData: null });
  }
  setNewPosition(input) {
    const { data } = this.props;
    const number = Number.parseInt(input);
    scoreboardHandler.updateScoreboard(data.title, {
      position: number,
    });
    this.setState({ dialogInputData: null });
  }
  editActionSelected(index) {
    if (index === 0) {
      // change title
      this.setState({
        dialogInputData: {
          title: "New Title:",
          submitInput: this.setNewTitle.bind(this),
        },
      });
    } else if (index === 1) {
      // move position
      this.setState({
        dialogInputData: {
          title: "Enter a position number:",
          submitInput: this.setNewPosition.bind(this),
        },
      });
    } else if (index === 2) {
      // reset scoreboard
      this.resetScoreboard();
    } else if (index === 3) {
      // delete scoreboard
      this.deleteScoreboard();
    }
  }

  componentDidMount() {
    if (Platform.OS === "android")
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);

    // android
    if (this.props.forceActive) {
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      this.setState({ expanded: true });
    }
  }

  render() {
    const { data, admin } = this.props;
    const { title, people, subtitle, id: scoreboardID } = data;
    // const people = data.people.sort((a, b) => {
    //   return a.score < b.score;
    // });
    const { expanded, dialogInputData } = this.state;
    const containerStyle = {
      ...Styles.globalContainer,
      ...(expanded ? Styles.pageContainer : Styles.itemContainer),
      ...(expanded ? { zIndex: 2 } : {}),
    };

    const items = [];
    for (p in people) {
      const person = people[p];
      if (person.score)
        items.push(
          <RankItem
            data={person}
            key={`rankitem.${person.name}`}
            scoreboardID={scoreboardID}
          />
        );
      if (items.length >= 10) break;
    }

    const dialogInput = dialogInputData ? (
      <DialogInput
        isDialogVisible={dialogInputData ? true : false}
        title={dialogInputData.title}
        submitInput={dialogInputData.submitInput}
        closeDialog={() => this.setState({ dialogInputData: null })}
      />
    ) : null;

    return (
      <>
        <MaybeTouchable
          onPress={this.onPress.bind(this)}
          touchable={expanded === false}
          key="maybetouchable"
        >
          <BlurView style={containerStyle} blurAmount={10} disableAndroid>
            <Text
              numberOfLines={expanded ? 2 : 1}
              adjustsFontSizeToFit
              style={{
                ...Styles.itemTitle,
                ...(expanded ? { width: "70%" } : {}),
              }}
            >
              {title}
            </Text>
            {expanded && subtitle ? (
              <Text
                numberOfLines={1}
                adjustsFontSizeToFit
                style={{ ...Styles.itemTitle, ...Styles.itemSubtitle }}
              >
                {subtitle}
              </Text>
            ) : null}
            <Content expanded={expanded}>
              <View style={{ width: "100%", height: 18 }} />
              {items}
              <RankUpdateButton onPress={this.rankUpdatePress.bind(this)} />
            </Content>
            <Back expanded={expanded} onPress={this.backPress.bind(this)} />
            {admin ? (
              <Edit expanded={expanded} onPress={this.editPress.bind(this)} />
            ) : null}
          </BlurView>
        </MaybeTouchable>
        <RankInput
          key="rankinput"
          active={this.state.showRankInput}
          backgroundPress={this.rankInputBackgroundPress.bind(this)}
          cancel={this.rankInputCancel.bind(this)}
          done={this.rankInputDone.bind(this)}
        />
        <ActionSheet
          ref={(ref) => (this.editActionSheet = ref)}
          options={[
            "Change Title",
            `Move Position (${data.position})`,
            "Reset Scoreboard",
            "DELETE Scoreboard",
            "Cancel",
          ]}
          cancelButtonIndex={4}
          onPress={this.editActionSelected.bind(this)}
        />
        {dialogInput}
      </>
    );
  }
}

class MaybeTouchable extends Component {
  render() {
    const { touchable = true, onPress } = this.props;
    return touchable ? (
      <TouchableOpacity onPress={onPress}>
        {this.props.children}
      </TouchableOpacity>
    ) : (
      this.props.children
    );
  }
}

class Content extends Component {
  render() {
    const { expanded } = this.props;
    return expanded ? (
      <ScrollView style={{ flex: 1 }}>{this.props.children}</ScrollView>
    ) : null;
  }
}

function Back(props) {
  return props.expanded ? (
    <View style={Styles.backButton}>
      <TouchableOpacity onPress={props.onPress}>
        <Image style={Styles.backButtonImage} source={X_ICON} />
      </TouchableOpacity>
    </View>
  ) : null;
}
function Edit(props) {
  return props.expanded ? (
    <View style={Styles.editButtonContainer}>
      <TouchableOpacity onPress={props.onPress}>
        <View>
          <Text style={Styles.editButtonText}>Edit</Text>
        </View>
      </TouchableOpacity>
    </View>
  ) : null;
}

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

const boxShadow =
  Platform.OS === "ios"
    ? {
        ...QuickStyle.boxShadow,
      }
    : {};

export const Styles = {
  globalContainer: {
    backgroundColor:
      Platform.OS === "ios" ? "rgba(15, 106, 255, 0.5)" : config.blue,
  },
  itemContainer: {
    width: "95%",
    borderRadius: 7,
    marginTop: verticalScale(getStatusBarHeight() + 20),
    boxShadow,
    paddingVertical: 15,
    paddingHorizontal: 10,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  itemTitle: {
    color: "white",
    fontFamily: Fonts.bold,
    fontSize: verticalScale(22),
    textAlign: "center",
    alignSelf: "center",
  },
  itemSubtitle: {
    fontSize: verticalScale(18),
    fontFamily: Fonts.medium,
  },
  pageContainer: {
    position: "absolute",
    width: screenWidth,
    height: screenHeight,
    paddingTop: getStatusBarHeight() + 26,
    paddingBottom: getBottomSpace(),
  },
  backButton: {
    position: "absolute",
    top: getStatusBarHeight() + 31,
    left: 20,
  },
  backButtonImage: {
    width: verticalScale(20),
    height: verticalScale(20),
    tintColor: "white",
  },
  editButtonContainer: {
    position: "absolute",
    right: 23,
    top: getStatusBarHeight() + 24,
  },
  editButtonText: {
    color: "white",
    fontFamily: Fonts.medium,
    fontSize: moderateScale(22),
  },
};
