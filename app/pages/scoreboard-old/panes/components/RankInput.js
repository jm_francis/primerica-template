import Backend from "backend/";
import { DropDown } from "components";
import React, { Component } from "react";
import { Platform } from "react-native";
import { moderateScale } from "constant-deprecated/node_modules/utilities";

const { scoreboardHandler } = Backend;

export default class RankInput extends Component {
  onChangeText(value) {
    const { onChangeText } = this.props;
    this.inputValue = value;
    if (onChangeText) onChangeText(value);
  }

  done() {
    const { done } = this.props;
    if (done) done(this.inputValue);
  }

  render() {
    const { active, backgroundPress, cancel, defaultValue } = this.props;
    const yOffset = Platform.OS === "android" ? -55 : 0;
    const title = scoreboardHandler._name;
    return (
      <DropDown
        active={active}
        backgroundPress={backgroundPress}
        yOffset={yOffset}
        parentStyle={{
          ...(active ? { zIndex: 10 } : {})
        }}
        title={title}
        input={{
          onChangeText: this.onChangeText.bind(this),
          defaultValue,
          placeholder: "Enter Score",
          keyboardType: "number-pad",
          style: {
            fontSize: moderateScale(30, 0.25)
          }
        }}
        cancel={cancel}
        done={this.done.bind(this)}
      />
    );
  }
}
