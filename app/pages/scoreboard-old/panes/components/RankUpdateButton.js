import { Fonts } from "constant-deprecated";
import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { QuickStyle } from "react-native-shortcuts";
import { moderateScale, verticalScale } from "constant-deprecated/node_modules/utilities";

export default class RankUpdateButton extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={{ ...Styles.container, ...this.props.style }}>
          <Text style={Styles.title}>Edit My Score</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const Styles = {
  container: {
    width: "95%",
    borderRadius: 7,
    marginTop: 10,
    ...QuickStyle.boxShadow,
    backgroundColor: "#0c3d8c",
    paddingVertical: 15,
    paddingHorizontal: moderateScale(10, 0.25),
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    color: "white",
    fontSize: verticalScale(22),
    fontFamily: Fonts.medium
  }
};
