//@ts-check
import { Input } from "@ui-kitten/components";
import Backend from "backend/";
import { dAccessory, IconPrimr, Kitten } from "components/";
import { Fonts } from "constant-deprecated/";
import React, { Component } from "react";
import { View } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { QuickStyle } from "react-native-shortcuts";
import { moderateScale, primrConfig, verticalScale } from "utilities/";

export default class RankItem extends Component {
  state = { _score: null };

  render() {
    const { data, scoreboardID } = this.props;
    const { name } = data;

    //* NOTE don't use adjustsFontSizeToFit cuz it'll affect android UI
    return (
      <Kitten.Layout style={Styles.container}>
        <Kitten.Text
          category="h6"
          style={{ fontFamily: primrConfig.strict["special-font-family"] }}
        >
          {name}
        </Kitten.Text>
        <$_Score {...this.props} />
      </Kitten.Layout>
    );
  }
}

/**
 * Score Segment that is editable by admin
 * @param {*} props
 */
function $_Score(props) {
  const {
    data: { score, name },
    scoreboardID,
  } = props;

  /** Check if this user is admin */
  const _isAdmin = Backend.firestoreHandler._account.admin;
  const [_score, setScore] = React.useState(score);
  const [isEditing, setEdit] = React.useState(false);

  const inputRef = React.useRef();
  // React.useEffect(
  //   function InputAutoFocus() {
  //     if (isEditing) {
  //       inputRef.current.focus();
  //     }
  //   },
  //   [isEditing]
  // );

  // React.useEffect(
  //   function FormatScore() {
  //     //* if score is left blank, set it to 0
  //     if (_score == "") {
  //       setScore(0);
  //     }
  //   },
  //   [isEditing]
  // );

  return (
    <View style={{ flexDirection: "row", alignItems: "center" }}>
      {isEditing ? (
        <Input
          ref={inputRef}
          placeholder={score && `${score}  `}
          value={_score}
          onChangeText={setScore}
          onSubmitEditing={() =>
            Backend.scoreboardHandler.updateSomeonesScore(
              scoreboardID,
              name,
              _score
            )
          }
          accessoryRight={(props: dAccessory) => (
            <TouchableWithoutFeedback
              // disabled={!_password}
              onPress={() => setEdit(!isEditing)}
            >
              <IconPrimr
                name={"check"}
                size={props.style.width}
                color={props.style.tintColor}
              />
            </TouchableWithoutFeedback>
          )}
          selectTextOnFocus={true}
          onBlur={() => {
            setEdit(false);
          }}
        />
      ) : (
        <>
          <Kitten.Button
            onPress={() => {
              setEdit(!isEditing);
              // inputRef.current.focus();
            }}
            disabled={!_isAdmin}
            appearance="ghost"
            status={"basic"} //* NOTE Will update once we build our design system
            accessoryRight={(props: dAccessory) =>
              _isAdmin && (
                <IconPrimr
                  name={"pen"}
                  size={props.style.width * 0.7}
                  color={props.style.tintColor}
                />
              )
            }
          >
            <Kitten.Text>{_score}</Kitten.Text>
          </Kitten.Button>
        </>
      )}
    </View>
  );
}

const Styles = {
  container: {
    width: "95%",
    borderRadius: 7,
    marginTop: 10,
    ...QuickStyle.boxShadow,
    // backgroundColor: "white",
    paddingVertical: 15,
    paddingHorizontal: moderateScale(10, 0.2),
    alignSelf: "center",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  name: {
    color: "rgb(20,20,20)",
    fontFamily: Fonts.medium,
    fontWeight: "600",
    fontSize: verticalScale(18),
  },
  score: {
    color: "rgb(20,20,20)",
    fontFamily: Fonts.medium,
    fontWeight: "400",
    fontSize: verticalScale(21),
  },
};
