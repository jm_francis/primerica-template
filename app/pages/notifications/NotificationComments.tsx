import { Avatar } from "@ui-kitten/components";
import Backend from "backend/";
import {
  dComment,
  dNotification,
} from "backend/NotificationHandler/NotificationFirestore";
import {
  Buttoon,
  dAccessory,
  ICON_NAME,
  IconPrimr,
  Kitten,
  Toasty,
  Txt,
} from "components/";
import R from "ramda";
import React, { Component, memo } from "react";
import { Alert, Linking, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { moderateScale, scale } from "react-native-size-matters";
import { C, spacing } from "utilities/";
import { extractLinkFromBody } from "utilities/functions/js-functions";

interface PropTypes {
  navigation: any;
}

const avatarDimension = moderateScale(24);

export default class NotificationComments extends Component<PropTypes> {
  state = {
    notification: null,
  };

  componentDidMount() {
    const { navigation } = this.props;
    const notification: dNotification = navigation.getParam("notification");
    Backend.notificationFirestore.global((notifications) => {
      for (let n in notifications) {
        const noti = notifications[n];
        if (noti.id === notification.id) this.setState({ notification: noti });
      }
    });
    Backend.notificationFirestore.team((notifications) => {
      for (let n in notifications) {
        const noti = notifications[n];
        if (noti.id === notification.id) this.setState({ notification: noti });
      }
    });
  }

  scrollRef = null;

  render() {
    const { notification } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <$_Header {...this.props} />
        <KeyboardAwareScrollView
          ref={(ref) => (this.scrollRef = ref)}
          keyboardShouldPersistTaps="always"
          style={Styles.container}
          extraScrollHeight={83}
        >
          <$_NotificationDetails {...this.props} notification={notification} />
          <$_Comments {...this.props} notification={notification} />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

export function $_Header(props: { navigation }) {
  const { navigation } = props;

  const MenuIcon = (name: ICON_NAME, props: dAccessory) => (
    <IconPrimr
      name={name}
      size={18}
      color={C.text01}
      onPress={() => navigation.goBack()}
    />
  );

  return (
    <>
      <Kitten.TopNavigation
        // title="Comments"
        style={{
          backgroundColor: C.background01,
        }}
        accessoryLeft={(_props: dAccessory) => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              props.navigation.pop();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          >
            Back
          </Buttoon>
        )}
      />
    </>
  );
}

function $_NotificationDetails(props) {
  const { notification, navigation } = props;
  if (!notification) return null;
  const title =
    notification.title?.length > 0 ? notification.title : notification.message;
  const message =
    notification?.message.length > 0 && title !== notification.message
      ? notification.message
      : null;

  function deletePress() {
    Alert.alert(
      "Are you sure you want to delete this notification from the app?",
      "",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: async () => {
            navigation.goBack();
            await Backend.notificationFirestore.deleteNotification(
              notification
            );
            Toasty({ text1: "Notification deleted." });
          },
        },
      ]
    );
  }

  let link = extractLinkFromBody(title);
  if (!link) link = extractLinkFromBody(message);

  return (
    <View style={{ padding: spacing(1), paddingTop: spacing(2) }}>
      {Backend.firestoreHandler._account.admin === true ? (
        <IconPrimr
          containerStyle={{ alignSelf: "flex-end" }}
          color={C.errorRed}
          name="trash"
          size={moderateScale(17)}
          onPress={deletePress}
        />
      ) : null}
      <Txt category="h6" style={{ fontSize: moderateScale(16) }}>
        {title}
      </Txt>
      {message ? <Txt category="p2">{message}</Txt> : null}
      {link ? (
        <Buttoon
          onPress={() => Linking.openURL(link)}
          icon={{ name: "link" }}
          appearance="ghost"
        >
          Open Link
        </Buttoon>
      ) : null}
    </View>
  );
}

function $_Comments(props) {
  const { notification } = props;
  if (!notification) return null;
  const comments: dComment[] = notification.comments;

  const [_newComment, setNewComment] = React.useState("");

  async function submitComment() {
    await Backend.notificationFirestore.writeComment(notification, _newComment);
    setNewComment("");
    Toasty({ text1: "Comment posted!" });
  }

  function commentPress(comment: dComment) {
    if (
      comment.uid === Backend.firestoreHandler.uid ||
      Backend.firestoreHandler._account.admin === true
    ) {
      Alert.alert(
        `Would you like to delete ${
          comment.uid === Backend.firestoreHandler.uid ? "your" : "this"
        } comment?`,
        "",
        [
          {
            text: "No",
            style: "cancel",
          },
          {
            text: "Yes",
            onPress: async () => {
              await Backend.notificationFirestore.removeComment(
                notification,
                comment
              );
              Toasty({ text1: "Comment removed!" });
            },
          },
        ]
      );
    }
  }

  return (
    <View style={{ marginTop: spacing(3) }}>
      <Txt
        category="s2"
        style={{ marginHorizontal: spacing(1), marginBottom: spacing(2) }}
      >
        {comments.length > 0 ? "Comments" : "No comments."}
      </Txt>
      {comments.map((comment) => (
        <CommentItem comment={comment} onPress={() => commentPress(comment)} />
      ))}
      <Kitten.Input
        autoFocus={true}
        style={{
          marginTop: spacing(3),
        }}
        textStyle={{
          minHeight: moderateScale(65),
        }}
        multiline={true}
        maxLength={200}
        placeholder="Type something..."
        status="ghost"
        value={_newComment}
        onChangeText={(value) => setNewComment(value)}
      />
      <Buttoon
        onPress={submitComment}
        disabled={_newComment.length < 1}
        appearance="ghost"
        style={{
          alignSelf: "flex-end",
          marginTop: -10,
        }}
      >
        Send
      </Buttoon>
    </View>
  );
}

const CommentItem = memo(
  (props: { onPress: () => void; comment: dComment }) => {
    const { onPress, comment } = props;

    return (
      <Kitten.ListItem
        onPress={onPress}
        style={{
          paddingHorizontal: spacing(1),
        }}
        title={comment.name}
        description={comment.message}
        accessoryLeft={(_props) => (
          <View
            style={{
              width: "9%",
              alignItems: "center",
            }}
          >
            {comment.profileImage ? (
              <Avatar
                {..._props}
                source={{ uri: comment.profileImage }}
                style={{
                  width: avatarDimension,
                  height: avatarDimension,
                }}
              />
            ) : (
              <IconPrimr
                {..._props}
                color="white"
                size={avatarDimension}
                name="profile"
              />
            )}
          </View>
        )}
      />
    );
  },
  (prevProps, nextProps) => !R.equals(prevProps, nextProps)
);

const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background,
  },
};
