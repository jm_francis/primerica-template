import Backend, { MediaPageSchema } from "backend/";
import {
  dNotification,
  sortNotifications,
} from "backend/NotificationHandler/NotificationFirestore";
import { topicIdFromName } from "backend/NotificationHandler/NotificationHandler";
import { IconPrimr, Kitten, Txt } from "components/";
import * as React from "react";
import { StyleSheet, View, Image } from "react-native";
import { C, fn, moderateScale, scale, spacing } from "utilities/";
import {
  dateAsNumber,
  socialFriendlyTime,
} from "utilities/functions/calendar-functions";

// let __MOCK_NOTIFICATIONS = [
//   { id: 0, title: "First noti", message: "Sup!" },
//   { id: 0, title: "Second noti", message: "Bro Sup Bro Sup Bro Sup Bro!" },
// ];

export function NotificationPage(props) {
  const { navigation } = props;

  const [_globalNotifications, setGlobalNotifications] = React.useState<
    dNotification[]
  >([]);
  const [_teamNotifications, setTeamNotifications] = React.useState<
    dNotification[]
  >([]);

  const allNotifications = sortNotifications(
    _globalNotifications.concat(_teamNotifications)
  );

  React.useEffect(function fetchNotifications() {
    Backend.notificationFirestore.global((notifications) =>
      setGlobalNotifications(notifications ? notifications : [])
    );
    Backend.notificationFirestore.team((notifications) =>
      setTeamNotifications(notifications ? notifications : [])
    );
    if (
      !Backend.firestoreHandler._account.notifications?.lastRead ||
      Backend.notificationFirestore.getUnreadNotifications().length > 0
    ) {
      Backend.firestoreHandler.updateAccount("notifications", {
        lastRead: new Date(),
      });
    }
  }, []);

  function notificationPress(notification: dNotification) {
    navigation.navigate("NotificationComments", { notification });
  }

  return (
    <View
      style={{
        backgroundColor: C.background01,
        flex: 1,
      }}
    >
      <Kitten.TopNavigation
        style={{ ...SS.CTNR_HEADER }}
        title={"Notifications"}
        alignment="center"
        accessoryLeft={() => (
          <IconPrimr
            name={"arrow_left"}
            size={20}
            color={C.text01}
            onPress={() => navigation.pop()}
          />
        )}
      />
      {allNotifications != null ? (
        <Kitten.List
          data={allNotifications}
          renderItem={({ item }) => (
            <NotificationItem item={item} onPress={notificationPress} />
          )}
          ItemSeparatorComponent={Kitten.Divider}
          keyExtractor={(item, idx) => `${idx}`}
        />
      ) : (
        <Txt
          category="s2"
          style={{
            fontSize: moderateScale(11),
            marginTop: -8,
            marginRight: 4,
            alignSelf: "center",
            color: C.grey600,
          }}
        >
          No notifications
        </Txt>
      )}
    </View>
  );
}

const NotificationItem = (props: {
  item: dNotification;
  onPress: (noti: dNotification) => void;
}) => {
  const { item, onPress } = props;

  const [_myTeam, setMyTeam] = React.useState<MediaPageSchema>(null);

  React.useEffect(() => {
    Backend.firestoreHandler.account((account) => {
      const myTeam = Backend.firebasePages.getMyTeamPage();
      setMyTeam(myTeam);
    });
  }, []);

  const read =
    dateAsNumber(item.timestamp) <
    dateAsNumber(Backend.firestoreHandler._account.notifications?.lastRead);

  const title = item.title && item.title.length > 0 ? item.title : item.message;
  const description =
    item.message && item.message.length > 0 ? item.message : null;

  let notiDate: Date = item.timestamp.toDate();
  if (!notiDate) notiDate = new Date();
  const time = socialFriendlyTime(notiDate);

  return (
    <Kitten.ListItem
      onPress={() => onPress(item)}
      style={{
        marginVertical: spacing(1),
      }}
      title={title}
      description={description===title? "" : description}
      accessoryLeft={() =>
        _myTeam && item.channel === topicIdFromName(_myTeam.name) ? (
          _myTeam.mediaItem.logo ? (
            <Image
              source={{ uri: _myTeam.mediaItem.logo }}
              style={{
                width: scale(23),
                height: scale(23),
              }}
            />
          ) : (
            <IconPrimr
              name="flag"
              size={scale(23)}
              color={read ? C.dim : C.errorRed}
            />
          )
        ) : (
          <IconPrimr
            name={"dot"}
            size={moderateScale(10)}
            color={read ? "transparent" : C.errorRed}
          />
        )
      }
      accessoryRight={() => (
        <View style={{ alignItems: "flex-end" }}>
          <Txt.P1 style={{ color: C.dim }}>{`${time}`}</Txt.P1>
          <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
            <Txt.P1 style={{ color: C.dim }}>{item.comments.length}</Txt.P1>
            <IconPrimr name="chat_bubble" size={scale(14)} color={C.dim} />
          </View>
        </View>
      )}
    />
  );
};

const SS = StyleSheet.create({
  CTNR_HEADER: {
    paddingHorizontal: spacing(3),
    backgroundColor: C.background01,
    alignItems: "center",
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
  },
});
