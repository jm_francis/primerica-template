import Backend from "backend/";
import { openZoomWithIdIfPossible } from "backend/apis/ZoomAppHandler";
import {
  dAccessory,
  IconPrimr,
  Kitten,
  Toasty
} from "components/";
import { customPageHandler } from "customPage/CustomPageHandler";
import R from "ramda";
import React, { Component } from "react";
import {
  Dimensions,
  Image,
  Linking,
  ScrollView,
  TextInput,
  View
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import {
  C,
  dMediaPage,
  fn,
  getStatusBarHeight,
  IPSCR,
  PROPS_Colors,
  spacing
} from "utilities/";
import Item from "./Item";
import SearchBar from "./SearchBar";
import SearchList from "./SearchList";

const screenWidth = Dimensions.get("window").width;

export const deniedAccessMessage =
  "You must complete all of the training levels before you can access media here!";
export const hasAccessToPage = (_page) => {
  const page =
    _page === "search"
      ? {
          mediaItem: { team: false },
        }
      : _page;
  const config = Backend.firestoreHandler._config.variables;
  if (
    config.levels &&
    config.levels.lockMedia &&
    !Backend.levelsHandler.hasCompletedLevels() &&
    page.mediaItem.team != "true" &&
    page.mediaItem.team != true
  )
    return false;
  return true;
};

class TeamPage extends Component {
  state = {
    everyoneItems: [],
    teamItems: [],
    searchItems: [],
  };

  scrollRef = null;

  render() {
    const { searchItems } = this.state;

    return (
      <ScrollView
        ref={(ref) => (this.scrollRef = ref)}
        style={{ flex: 1, backgroundColor: C.background01 }}
        stickyHeaderIndices={[0]}
      >
        <$_SearchableHeader {...this.props} />
        <$_TeamMedia {...this.props} />
        <View
          style={{
            width: screenWidth,
            height: 20,
          }}
        />
      </ScrollView>
    );
  }
}

export function $_SearchableHeader(props) {
  const { navigation } = props;
  const [_searchItems, setSearchItems] = React.useState([]);

  if (!Backend.firestoreHandler._account) return null;

  const {
    _account: { name },
  } = Backend.firestoreHandler;

  const searchBarRef = React.useRef<TextInput>();

  function searchTextChange(text: string) {
    const searchItems = Backend.firebasePages.itemSearch(text);
    setSearchItems(searchItems);
  }
  return (
    <View style={{ backgroundColor: C.background01 }}>
      <Kitten.TopNavigation
        style={{
          paddingTop: getStatusBarHeight("safe", "ios-only"),
          backgroundColor: C.background01,
        }}
        title={"Team " + fn.js.capitalize(!!name ? name : "")}
      />
      <SearchBar
        inputRef={searchBarRef}
        onChangeText={searchTextChange}
        // onFocus={this.searchBarFocus.bind(this)}
        // onMeasure={this.searchBarMeasure.bind(this)}
        navigation={navigation}
      />
      {_searchItems.length > 0 ? (
        <SearchList
          items={_searchItems}
          onItemPress={searchBarRef.current.onChangeText("")}
        />
      ) : null}
    </View>
  );
}

function $_TeamMedia(props) {
  const [_teamMedia, setTeamMedia] = React.useState([]);
  React.useEffect(function fetchTeamMedia() {
    Backend.firebasePages.pages((rawPages) => pagesSorting(rawPages, "team"));
  }, []);

  /**
   * Sort pages[] based on specific `types`
   *
   * NOTE 📖 rawPages
   * NOTE turn into a g_hook
   */
  function pagesSorting(rawPages: any[], type: "hierarchy" | "team" = "team") {
    const positionedPages = rawPages.sort((a, b) => {
      return a.position > b.position;
    });
    let everyoneItems = [];
    let teamItems = [];
    let keys = [];

    /** NOTE 📖 duplicate */
    //* for live reloading page content without creating duplicates
    const duplicate = (key: string | number) => {
      for (var k in keys) if (keys[k] === key) return true;
      keys.push(key);
    };

    for (var p in positionedPages) {
      const page = positionedPages[p];
      const { mediaItem } = page;

      if (
        !mediaItem.visible ||
        mediaItem.visible == false ||
        mediaItem.visible == "false"
      )
        continue;

      const key = `media.pages.${page.name}`;
      if (duplicate(key)) continue;

      //* add key to `page`
      const polishedPage = R.mergeLeft(page, { key });

      if (mediaItem.team == true) teamItems.push(polishedPage);
      //   teamItems.push<Item key={key} page={page} />);
      //   else everyoneItems.push(<Item key={key} page={page} />);
      else everyoneItems.push(polishedPage);
    }
    setTeamMedia(everyoneItems);
  }

  return (
    <FlatList
      keyExtractor={(item) => item.key}
      data={_teamMedia}
      // renderItem={({ item }) => <ListItem {...props} page={item} />}
      renderItem={({ item }) => <Item key={item.key} page={item} />}
      // renderItem={({ item }) => <Txt>{JSON.stringify(R.keys(item.mediaItem))}</Txt>}
    />
  );
}
interface dListItem extends IPSCR {
  page: dMediaPage;
}
const ListItem = (props: dListItem) => {
  const { navigation, page } = props;
  const [_isPasswordBypassed, shouldBypassPassword] = React.useState(false);
  const [entryPassword, setEntryPassword] = React.useState("");

  const refPassword = React.useRef();

  /** Bypass pw if it's saved in ass */
  React.useEffect(function checkPassword() {
    Backend.localHandler.getSavedPasswordForPage(page.name).then((value) => {
      if ("" + page.password == value) {
        shouldBypassPassword(true);
      }
    });
  }, []);

  const MenuIcon = (name: ICON_NAME, props: dAccessory) => (
    <IconPrimr
      name={name}
      size={props.style.width}
      color={props.style.tintColor}
    />
  );

  function handlePress() {
    const { page, zoom } = props;
    const password = "" + page.password;

    const config = Backend.firestoreHandler._config.variables;
    if (!hasAccessToPage(page)) {
      Toasty({ text1: "Access Denied", text2: deniedAccessMessage });
      return;
    }

    if (!_isPasswordBypassed && page.password && password.length > 1 && !zoom) {
      setEntryPassword(password);
      refPassword.current && refPassword.current.present();
    } else open();
  }

  function open() {
    const { page, zoom } = props;
    const { mediaItem } = page;
    const password = "" + page.password;
    const meetingId = page.zoom;

    if (password && password.length > 1) {
      shouldBypassPassword(true);
      Backend.localHandler.savePasswordForPage(page.name, password);
    }

    if (zoom && meetingId && meetingId.toString().length > 1) {
      openZoomWithIdIfPossible(meetingId);
    } else if (mediaItem.link && mediaItem.link.length > 1) {
      Linking.openURL(mediaItem.link);
      return;
    } else customPageHandler.presentPage(page.name);
  }

  return (
    <>
      <Kitten.MenuItem
        style={{
          backgroundColor: C.background01,
          borderRadius: 10,
          margin: spacing(1),
        }}
        title={page.name}
        onPress={
          () =>
            /** NOTE change to suitable page */
            handlePress()
          // navigation.navigate("ListBuilder")
        }
        accessoryLeft={(props: dAccessory) => {
          return !!page.mediaItem && !!page.mediaItem.logo ? (
            <Image
              source={{ uri: page.mediaItem.logo }}
              style={{
                width: props.style.width * 1.8,
                height: props.style.height * 1.8,
              }}
            />
          ) : (
            MenuIcon("rocket", props)
          );
        }}
        accessoryRight={(props: dAccessory) => MenuIcon("chevron_right", props)}
      />
      {/* <PasswordPopUp
        key="password"
        popUpRef={(ref) => refPassword(ref)}
        entryGranted={open}
        password={entryPassword}
      /> */}
    </>
  );
};

const SS = (C: PROPS_Colors) => {
  return {
    layoutItems: {
      flexDirection: "row",
      justifyContent: "space-evenly",
      flexWrap: "wrap",
    },
    allMediaTitle: {
      marginTop: 10,
    },
  };
};

// const Media = Page.create(TeamPage, "Team");

export default TeamPage;
