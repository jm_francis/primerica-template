import Backend from "backend/";
import { Fonts } from "constant-deprecated/";
import { removePageExtensions } from "constant-deprecated/Config";
import { presetLogos } from "constant-deprecated/Images";
import { customPageHandler } from "customPage/handlers/CustomPageHandler";
import { openZoomWithIdIfPossible } from "backend/apis/ZoomAppHandler";
import React from "react";
import {
  Alert,
  Dimensions,
  Image,
  Linking,
  Platform,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { QuickStyle } from "react-native-shortcuts";
import { moderateScale } from "utilities/";
import { deniedAccessMessage, hasAccessToPage } from "../media/MediaPage";

const { localHandler } = Backend;

// integrates some stuff for zoom/video calls page

export default class Item extends React.Component {
  state = {
    entryPassword: null,
    presetLogo: null,
  };

  componentDidMount() {
    const { page } = this.props;
    const { mediaItem } = page;
    const { presetLogo } = mediaItem;

    if (presetLogo) {
      this.setState({ presetLogo: presetLogos[presetLogo] });
    }

    localHandler.getSavedPasswordForPage(page.name).then((value) => {
      if ("" + page.password == value) {
        this.bypassPassword = true;
      }
    });
  }

  handlePress() {
    const { page, zoom } = this.props;
    const password = "" + page.password;

    const config = Backend.firestoreHandler._config.variables;
    if (!hasAccessToPage(page)) {
      Alert.alert(deniedAccessMessage);
      return;
    }

    if (!this.bypassPassword && page.password && password.length > 1 && !zoom) {
      this.setState({ entryPassword: password });
    } else this.open();
  }

  open() {
    const { page, zoom } = this.props;
    const { mediaItem } = page;
    const password = "" + page.password;
    const meetingId = page.zoom;

    if (password && password.length > 1) {
      this.bypassPassword = true;
      localHandler.savePasswordForPage(page.name, password);
    }

    if (zoom && meetingId && meetingId.toString().length > 1) {
      openZoomWithIdIfPossible(meetingId);
    } else if (mediaItem.link && mediaItem.link.length > 1) {
      Linking.openURL(mediaItem.link);
      return;
    } else customPageHandler.presentPage(page.name);
  }

  render() {
    const { page } = this.props;
    const { mediaItem } = page;
    const { color = "rgb(46, 60, 249)", imageColor } = mediaItem;
    const logo = this.state.presetLogo
      ? this.state.presetLogo
      : mediaItem.logo
      ? { uri: mediaItem.logo }
      : null;

    //FireAdmin corrections...
    const name = removePageExtensions(page.name);

    let backgroundColor = color;
    if (
      (color && color == "rgba(0, 0, 0, 1)" && color == "#000000") ||
      color == ""
    )
      backgroundColor = "rgb(46, 60, 249)";

    let imageBackgroundColor = imageColor ? imageColor : "rgb(20,20,20)";
    if (
      imageBackgroundColor === "rgba(0, 0, 0, 1)" ||
      imageBackgroundColor === "#000000" ||
      imageBackgroundColor == ""
    )
      imageBackgroundColor = "rgb(20,20,20)";

    return [
      <TouchableOpacity key="item" onPress={this.handlePress.bind(this)}>
        <View
          style={{
            ...Styles.item,
            backgroundColor,
            justifyContent: logo ? "flex-start" : "center",
          }}
        >
          {logo ? (
            <View
              style={{
                ...Styles.imageView,
                backgroundColor: imageBackgroundColor,
              }}
            >
              <Image
                source={logo}
                style={{
                  width: imageDimension,
                  height: imageDimension,
                }}
              />
            </View>
          ) : null}

          <Text
            numberOfLines={2}
            adjustsFontSizeToFit
            allowFontScaling
            style={{
              ...Styles.text,
              textAlign: logo ? "left" : "center",
            }}
          >
            {name}
          </Text>
        </View>
      </TouchableOpacity>,
    ];
  }
}

const modFactor = 0.25;
const imageDimension = moderateScale(39, modFactor); // ^^
const imageViewDimension = moderateScale(52, modFactor); // VV
const screenWidth = Dimensions.get("window").width;

const Styles = {
  item: {
    width: Platform.isPad ? screenWidth * 0.45 : screenWidth * 0.95,
    borderRadius: 7,
    paddingVertical: 10,
    marginTop: 15,
    height: moderateScale(65, modFactor),
    alignItems: "center",
    alignSelf: "center",
    flexDirection: "row",
  },
  text: {
    ...QuickStyle.boxShadow,
    color: "white",
    fontFamily: Fonts.bold,
    fontSize: moderateScale(21, 0.15),
    flex: 1,
    marginRight: 10,
  },
  imageView: {
    borderRadius: imageViewDimension / 2,
    overflow: "hidden",
    width: imageViewDimension,
    height: imageViewDimension,
    marginRight: 16,
    marginLeft: 10,
    alignItems: "center",
    justifyContent: "center",
  },
};
