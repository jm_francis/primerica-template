import { Fonts } from "constant-deprecated";
import React from "react";
import { Text, View } from "react-native";
import { moderateScale } from "utilities/";

export default class Title extends React.Component {
  render() {
    const { style, textStyle, lineStyle, textProps = {} } = this.props;
    return (
      <View style={[Styles.parent, style]}>
        <View style={[Styles.line, lineStyle]} />

        <Text
          allowFontScaling={false}
          style={[Styles.text, textStyle]}
          {...textProps}
        >
          {this.props.children}
        </Text>

        <View style={[Styles.line, lineStyle]} />
      </View>
    );
  }
}

const Styles = {
  parent: {
    width: "100%",
    marginTop: 20,
    height: 45,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginLeft: 6,
    textAlign: "center",
    color: "rgb(247, 52, 47)",
    fontFamily: Fonts.bold,
    fontSize: moderateScale(23, 0.2)
  },
  line: {
    flex: 1,
    height: moderateScale(3.2, 0.2),
    borderRadius: 10,
    marginLeft: 7,
    backgroundColor: "rgb(247, 47, 47)"
  }
};
