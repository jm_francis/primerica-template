import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  Platform,
  Image,
  Dimensions,
  Keyboard,
  TouchableOpacity
} from "react-native";
import { moderateScale, verticalScale } from "utilities/";

import { PLAY, DOCUMENT, LOADING } from "app/constant-deprecated/Images";
import VimeoAPI from "app/backend/apis/VimeoAPI";
import K from "app/constant";
import { customPageHandler } from "app/customPage/CustomPageHandler";

export default class SearchList extends Component {
  state = {
    keyboardHeight: 0
  };

  componentDidMount() {
    Keyboard.addListener("keyboardWillShow", e => {
      this.setState({ keyboardHeight: e.endCoordinates.height });
    });
    Keyboard.addListener("keyboardWillHide", e => {
      this.setState({ keyboardHeight: 0 });
    });
  }

  itemPress(itemData) {
    const { onItemPress } = this.props;
    const pageName = itemData.fromPage;
    customPageHandler.presentPage(pageName, { scrollToItem: itemData.title });
    if (onItemPress) onItemPress();
  }

  render() {
    const { keyboardHeight } = this.state;
    const { items } = this.props;

    const itemComponents = [];
    for (i in items) {
      const item = items[i];
      itemComponents.push(
        <Item
          data={item}
          onPress={this.itemPress.bind(this)}
          key={`item.${item.title}`}
        />
      );
      if (itemComponents.length > 20) break;
    }

    return (
      <View style={Styles.container}>
        <ScrollView
          style={Styles.scrollView}
          keyboardShouldPersistTaps={"always"}
          showsVerticalScrollIndicator={false}
        >
          {itemComponents}

          <View
            style={{
              width: "100%",
              height: keyboardHeight > 0 ? keyboardHeight + 200 : 260
            }}
          />
        </ScrollView>
      </View>
    );
  }
}

function Item(props) {
  const [thumbnail, setThumbnail] = React.useState(null);

  const { data, onPress } = props;
  const itemType =
    data.media && data.media.includes("vimeo.com")
      ? "video"
      : data.url
      ? "document"
      : "audio";

  if (itemType === "video" && data.media.includes("vimeo.com")) {
    VimeoAPI.getVideo({ shareLink: data.media }).then(videoData => {
      const image = VimeoAPI.getThumbnailFromVideo(videoData, 0);
      setThumbnail(image);
    });
  }

  return (
    <TouchableOpacity onPress={() => onPress(data)}>
      <View style={Styles.itemContainer}>
        <View style={Styles.imageBox}>
          {itemType === "document" ? (
            <Image style={Styles.itemIcon} source={DOCUMENT} />
          ) : null}
          {itemType === "audio" ? (
            <Image style={Styles.itemIcon} source={PLAY} />
          ) : null}
          {itemType === "video" ? (
            <Image style={Styles.itemIcon} source={LOADING} />
          ) : null}
          {thumbnail ? (
            <Image
              style={Styles.itemThumbnailImage}
              source={{ uri: thumbnail }}
            />
          ) : null}
        </View>
        <Text style={Styles.itemTitle} adjustsFontSizeToFit numberOfLines={2}>
          {data.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
const itemHeight = moderateScale(72, 0.2);

export const backgroundColor = "rgba(40,40,40,0.93)";

const Styles = {
  container: {
    position: "absolute",
    top: Platform.isPad ? 82 : 57,
    width: screenWidth,
    backgroundColor,
    height: screenHeight,
    paddingTop: 8
  },
  scrollView: {
    flex: 1
  },

  itemContainer: {
    marginVertical: 4,
    overflow: "hidden",
    backgroundColor: "rgba(85,85,85,0.72)",
    alignSelf: "center",
    borderRadius: 7,
    width: screenWidth * 0.96,
    height: itemHeight,
    flexDirection: "row",
    paddingRight: itemHeight + 15,
    alignItems: "center"
  },
  itemTitle: {
    color: "white",
    fontSize: moderateScale(20),
    fontFamily: K.Fonts.medium
  },
  imageBox: {
    width: itemHeight,
    height: itemHeight,
    marginRight: 12,
    justifyContent: "center",
    alignItems: "center"
  },
  itemThumbnailImage: {
    position: "absolute",
    height: itemHeight,
    width: itemHeight
  },
  itemIcon: {
    tintColor: "white",
    width: itemHeight * 0.37,
    height: itemHeight * 0.37,
    alignSelf: "center"
  }
};
