//#region [IMPORT]
import { Buttoon, sstyled, Toasty, Txt } from "components/";
import React, { useState } from "react";
import { ActivityIndicator, ScrollView, View } from "react-native";
import {
  NavigationStackProp,
  NavigationStackScreenProps,
} from "react-navigation-stack";
import { C, spacing } from "utilities/";
//#endregion

/**
 * ### A quick screen for a quick start
 *
 * ----
 * @example As is
 * ----
 * @version 1.1.22AT
 * -  *Add fool-proof try-catch*
 * @author nguyenkhooi
 */
export function QuickPage(props: P) {
  const { navigation } = props;

  const [isReady, shouldReady] = useState(false);
  try {
    //#region [SECTION ]  FOOL-PROOF
    if (isReady == false) throw Error("NOT_READY");

    //#endregion
    return (
      <SS.Sctnr>
        <SS.CtnrWelcome>
          <SS.TxtTitle>{"Welcome to Quick Screen"}</SS.TxtTitle>
          <Buttoon onPress={() => Toasty({ text1: "Hey there" })}>
            {"Try me"}
          </Buttoon>
        </SS.CtnrWelcome>
      </SS.Sctnr>
    );
  } catch (error) {
    switch (error.message) {
      case "NOT_READY":
        return (
          <SS.CtnrIdle>
            <ActivityIndicator size="large" color={C.primary} />
          </SS.CtnrIdle>
        );
      default:
        return <></>;
    }
  }
}

const SS = {
  Sctnr: sstyled(ScrollView)((p) => ({
    flex: 1,
    width: "100%",
    backgroundColor: p.C.background,
  })),
  CtnrIdle: sstyled(View)((p) => ({
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: p.C.background,
  })),
  CtnrWelcome: sstyled(View)(() => ({
    paddingVertical: spacing(4),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
  })),
  TxtTitle: sstyled(Txt.H6)({
    textAlign: "center",
  }),
};

/**
 * ###  Screen Props
 */
interface P
  extends NavigationStackScreenProps<{
    /** Params Type */
  }> {}
{
}
