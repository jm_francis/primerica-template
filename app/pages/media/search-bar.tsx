import React, { Component } from "react";
import {
  TextInput,
  Image,
  View,
  Dimensions,
  Keyboard,
  Alert,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { C, moderateScale, spacing } from "utilities/";

import K from "constant-deprecated/";
import { X_ICON } from "constant-deprecated/Images";
import { backgroundColor } from "./search-list";
import { hasAccessToPage, deniedAccessMessage } from "./MediaPage";
import { Input } from "@ui-kitten/components";

interface Props {
  inputRef?: any;
  onMeasure?: Function;
  onChangeText: (text: string) => void;
  onFocus?: () => void;
}

/**
 * ### Search bar component designed for media searching
 * @author jm_francis
 * @version 1.1.20
 * @example
 * <SearchBar
        ref={searchBarRef}
        onChangeText={text => console.log(text)}
        onFocus={() => {}}
        onMeasure={(fx,fy,width,height,px,py) => {}}
      />
 */
export class SearchBar extends Component<Props> {
  state = {
    value: "",
  };

  inputRef = null;

  onLayout(e) {
    const { onMeasure } = this.props;
    if (onMeasure) {
      this.inputRef.measure((x, y, width, height, pageX, pageY) => {
        onMeasure(x, y, width, height, pageX, pageY);
      });
    }
  }

  _inputRef(ref: any) {
    // const { inputRef } = this.props;
    this.inputRef = ref;
    if (this.props.inputRef) this.props.inputRef(ref);
  }

  xpress() {
    this.onChangeText("");
    Keyboard.dismiss();
  }

  onChangeText(value) {
    const { onChangeText } = this.props;
    this.setState({ value });
    if (onChangeText) onChangeText(value);
  }
  endEditing() {
    this.onChangeText("");
  }

  onFocus() {
    const { onFocus } = this.props;
    if (onFocus) onFocus();
    if (!hasAccessToPage("search")) {
      Keyboard.dismiss();
      Alert.alert(deniedAccessMessage);
    }
  }

  render() {
    const { value } = this.state;

    return (
      <View
        style={{
          ...Styles.container,
          ...(value.length > 0 ? { backgroundColor } : {}),
        }}
      >
        <TextInput
          style={{
            ...Styles.inputStyle,
            ...(value.length > 0
              ? { backgroundColor: C["color-basic-transparent-300"] }
              : {}),
          }}
          ref={this._inputRef.bind(this)}
          autoCorrect={false}
          returnKeyType="done"
          selectionColor="white"
          placeholder="Search for content..."
          placeholderTextColor="rgb(200,200,200)"
          onLayout={this.onLayout.bind(this)}
          value={value}
          onEndEditing={this.endEditing.bind(this)}
          {...this.props}
          onChangeText={this.onChangeText.bind(this)}
          onFocus={this.onFocus.bind(this)}
        />
        {value.length > 0 ? <XButton onPress={this.xpress.bind(this)} /> : null}
      </View>
    );
  }
}

function XButton(props) {
  const { onPress } = props;
  return (
    <View style={Styles.xContainer}>
      <TouchableOpacity onPress={onPress}>
        <View>
          <Image source={X_ICON} style={Styles.xImage} />
        </View>
      </TouchableOpacity>
    </View>
  );
}

const screenWidth = Dimensions.get("window").width;

const Styles = {
  container: {
    width: screenWidth,
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: spacing(2)
  },
  inputStyle: {
    marginTop: 17,
    color: "white",
    backgroundColor: C["color-basic-transparent-300"],
    borderRadius: 8,
    fontSize: moderateScale(18),
    fontFamily: K.Fonts.medium,
    paddingHorizontal: 12,
    paddingVertical: spacing(2),
    alignSelf: "center",
    width: "95%",
  },
  xContainer: {
    position: "absolute",
    right: moderateScale(22),
    top: 28,
    alignSelf: "center",
  },
  xImage: {
    tintColor: "rgb(240,240,240)",
    width: moderateScale(19),
    height: moderateScale(19),
  },
};
