//@ts-check
import Backend from "backend/";
import { dAccessory, ICON_NAME, IconPrimr, Kitten, Buttoon } from "components/";
import {
  BIG_EVENT,
  BUILDING,
  MONEY_HOUSE,
  PARTNERSHIP,
  PIE_CHART,
  RECRUITING,
  ROCKET,
  SOCIAL_MEDIA,
  TOOLBOX,
} from "constant-deprecated/Images";
import { customPageHandler } from "customPage/handlers/CustomPageHandler";
import R from "ramda";
import React from "react";
import { Image, View } from "react-native";
import { moderateScale } from "react-native-size-matters";
import { C, IPSCR, scale, spacing } from "utilities/";
import { MediaPageSchema } from "backend/schemas";
import { SearchBar } from "./search-bar";
import SearchList from "./search-list";
import { TopNavigation } from "@ui-kitten/components";

const PRESET_LOGO = {
  big_event: BIG_EVENT,
  building: BUILDING,
  money_house: MONEY_HOUSE,
  partnership: PARTNERSHIP,
  pie_chart: PIE_CHART,
  recruiting: RECRUITING,
  rocket: ROCKET,
  social_media: SOCIAL_MEDIA,
  toolbox: TOOLBOX,
};

export const deniedAccessMessage =
  "You must complete all of the training levels before you can access media here!";
export const hasAccessToPage = (_page) => {
  const page =
    _page === "search"
      ? {
          mediaItem: { team: false },
        }
      : _page;
  const config = Backend.firestoreHandler._config.variables;
  if (
    config.levels &&
    config.levels.lockMedia &&
    !Backend.levelsHandler.hasCompletedLevels() &&
    page.mediaItem.team != "true" &&
    page.mediaItem.team != true
  )
    return false;
  return true;
};

export function MediaPage(props) {
  const [selectedIndex, setSelectedIndex] = React.useState(null);
  const [_mediaPages, setMediaPages] = React.useState<MediaPageSchema[]>(null);
  React.useEffect(function fetchMedia() {
    Backend.firebasePages.pages((rawPages) => pagesSorting(rawPages));
  }, []);

  /**
   * Sort pages[] based on specific `types`
   *
   * NOTE 📖 rawPages
   * NOTE turn into a g_hook
   */
  function pagesSorting(rawPages: MediaPageSchema[]) {
    const positionedPages = rawPages.sort((a, b) => {
      return a.position - b.position;
    });
    let everyoneItems = [];
    // let teamItems = [];
    let keys = [];

    /** NOTE 📖 duplicate */
    const duplicate = (key: string | number) => {
      for (var k in keys) if (keys[k] === key) return true;
      keys.push(key);
    };

    for (var p in positionedPages) {
      const page = positionedPages[p];
      const { mediaItem } = page;

      if (
        !mediaItem.visible ||
        mediaItem.visible == "false" ||
        mediaItem.visible == false
      )
        continue;

      if (mediaItem.team == "true" || mediaItem.team == true) continue;

      const key = `media.pages.${page.name}`;
      if (duplicate(key)) continue;

      //* add key to `page`
      const polishedPage = R.mergeLeft(page, { key });

      everyoneItems.push(polishedPage);
    }
    setMediaPages(everyoneItems);
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: C.background,
      }}
    >
      <TopNavigation
        title="Media"
        style={{ backgroundColor: C.background01 }}
      />

      <$_SearchableHeader />

      {!!_mediaPages && (
        <Kitten.Menu
          selectedIndex={selectedIndex}
          onSelect={(index) => setSelectedIndex(index)}
        >
          {_mediaPages
            .map((page) => <ListItem {...props} page={page} />)
            .concat([<ListItem {...props} more />])}
        </Kitten.Menu>
      )}
    </View>
  );
}

interface dListItem extends IPSCR {
  page?: MediaPageSchema;
  more?: boolean;
  navigation?: any;
}
const ListItem = (props: dListItem) => {
  const { more, navigation } = props;
  const page = !more
    ? props.page
    : {
        name: "More",
        mediaItem: {
          logo: null,
          presetLogo: null,
        },
      };
  const MenuIcon = (name: ICON_NAME, props: dAccessory) => (
    <View
      style={{
        height: moderateScale(props.style.height * 1.8),
        justifyContent: "center",
      }}
    >
      <IconPrimr
        name={name}
        size={props.style.width}
        color={props.style.tintColor}
      />
    </View>
  );

  async function itemPress() {
    if (more) navigation.navigate("More");
    else customPageHandler.presentPage(page.name);
  }

  return (
    <Kitten.MenuItem
      style={{
        backgroundColor: C.background01,
        borderRadius: 10,
        margin: spacing(1),
      }}
      title={page.name}
      onPress={itemPress}
      accessoryLeft={(props: dAccessory) => {
        return (!!page.mediaItem && !!page.mediaItem.logo) ||
          page.mediaItem.presetLogo ? (
          <Image
            source={
              page.mediaItem.logo?.length > 1
                ? { uri: page.mediaItem.logo }
                : PRESET_LOGO[page.mediaItem.presetLogo]
            }
            style={{
              width: moderateScale(props.style.width * 1.8),
              height: moderateScale(props.style.height * 1.8),
            }}
          />
        ) : more ? (
          MenuIcon("more", props)
        ) : (
          MenuIcon("rocket", props)
        );
      }}
      accessoryRight={(props: dAccessory) => MenuIcon("chevron_right", props)}
    />
  );
};

export function $_SearchableHeader(props: {}) {
  const [_searchItems, setSearchItems] = React.useState([]);

  const searchBarRef = React.useRef<SearchBar>();

  function searchTextChange(text: string) {
    const searchItems = Backend.firebasePages.itemSearch(text);
    setSearchItems(searchItems);
  }
  return (
    <View style={{ zIndex: 100 }}>
      <SearchBar ref={searchBarRef} onChangeText={searchTextChange} />
      {_searchItems.length > 0 ? (
        <SearchList
          searchResponses={_searchItems}
          onItemPress={() => searchBarRef?.current?.xpress()}
        />
      ) : null}
    </View>
  );
}

function $_More(props) {
  const { navigation } = props;
  return (
    <Buttoon
      onPress={() => navigation.navigate("More")}
      style={{
        margin: spacing(3),
        width: "98%",
        alignSelf: "center",
        borderWidth: 1,
        borderRadius: 7,
        borderColor: C.primary,
      }}
      appearance="ghost"
      icon={{ name: "arrow_right", right: true }}
    >
      More
    </Buttoon>
  );
}
