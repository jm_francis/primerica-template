import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  Platform,
  Image,
  Dimensions,
  Keyboard,
  TouchableOpacity,
} from "react-native";
import { C, moderateScale } from "utilities/";
import { PLAY, DOCUMENT, LOADING } from "constant-deprecated/Images";
import VimeoAPI from "backend/apis/VimeoAPI";
import K from "constant-deprecated/";
import { customPageHandler } from "customPage/handlers/CustomPageHandler";
import { dMediaPageItem } from "backend/schemas";
import { dItemSearchResponse } from "backend/FirebasePages";

interface Props {
  onItemPress: (item: dMediaPageItem) => void;
  searchResponses: dItemSearchResponse[];
}

/**
 * ### List of media items that may be relevant to current search entry
 * 
 * @author jm_francis
 * @version 1.1.20
 * 
 * @example
 * {_searchItems.length > 0 ? (
        <SearchList
          items={_searchItems}
          onItemPress={searchBarRef.current.onChangeText("")}
        />
      ) : null}
 */
export default class SearchList extends Component<Props> {
  state = {
    keyboardHeight: 0,
  };

  componentDidMount() {
    Keyboard.addListener("keyboardWillShow", (e) => {
      this.setState({ keyboardHeight: e.endCoordinates.height });
    });
    Keyboard.addListener("keyboardWillHide", (e) => {
      this.setState({ keyboardHeight: 0 });
    });
  }

  itemPress(response: dItemSearchResponse) {
    const { onItemPress } = this.props;
    const pageName = response.fromPage;
    customPageHandler.presentPage(pageName, {
      scrollToItem: response.mediaItem.title,
    });
    onItemPress && onItemPress(response.mediaItem);
  }

  render() {
    const { keyboardHeight } = this.state;
    const { searchResponses } = this.props;

    const itemComponents = [];
    for (let i in searchResponses) {
      const response = searchResponses[i];
      itemComponents.push(
        <Item
          itemResponse={response}
          onPress={this.itemPress.bind(this)}
          key={`item.${response.mediaItem.id}`}
        />
      );
      if (itemComponents.length > 20) break;
    }

    return (
      <View style={Styles.container}>
        <ScrollView
          style={Styles.scrollView}
          keyboardShouldPersistTaps={"always"}
          showsVerticalScrollIndicator={false}
        >
          {itemComponents}

          <View
            style={{
              width: "100%",
              height: keyboardHeight > 0 ? keyboardHeight + 200 : 260,
            }}
          />
        </ScrollView>
      </View>
    );
  }
}

function Item(props: {
  itemResponse: dItemSearchResponse;
  onPress: (itemResponse: dItemSearchResponse) => void;
}) {
  const [thumbnail, setThumbnail] = React.useState(null);

  const { itemResponse, onPress } = props;
  const itemType =
    itemResponse.mediaItem?.media &&
    itemResponse.mediaItem?.media.includes("vimeo.com")
      ? "video"
      : itemResponse.mediaItem?.url
      ? "document"
      : "audio";

  if (
    itemType === "video" &&
    itemResponse.mediaItem?.media.includes("vimeo.com")
  ) {
    VimeoAPI.getVideo({ shareLink: itemResponse.mediaItem?.media }).then(
      (videoData) => {
        const image = VimeoAPI.getThumbnailFromVideo(videoData, 0);
        setThumbnail(image);
      }
    );
  }

  return (
    <TouchableOpacity onPress={() => onPress(itemResponse)}>
      <View style={Styles.itemContainer}>
        <View style={Styles.imageBox}>
          {itemType === "document" ? (
            <Image style={Styles.itemIcon} source={DOCUMENT} />
          ) : null}
          {itemType === "audio" ? (
            <Image style={Styles.itemIcon} source={PLAY} />
          ) : null}
          {itemType === "video" ? (
            <Image style={Styles.itemIcon} source={LOADING} />
          ) : null}
          {thumbnail ? (
            <Image
              style={Styles.itemThumbnailImage}
              source={{ uri: thumbnail }}
            />
          ) : null}
        </View>
        <Text style={Styles.itemTitle} adjustsFontSizeToFit numberOfLines={2}>
          {itemResponse.mediaItem?.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
const itemHeight = moderateScale(72, 0.2);

export const backgroundColor = C.background;

const Styles = {
  container: {
    position: "absolute",
    top: Platform.isPad ? 82 : 57,
    width: screenWidth,
    backgroundColor,
    height: screenHeight,
    paddingTop: 8,
  },
  scrollView: {
    flex: 1,
  },

  itemContainer: {
    marginVertical: 4,
    overflow: "hidden",
    backgroundColor: C["color-basic-transparent-300"],
    alignSelf: "center",
    borderRadius: 7,
    width: screenWidth * 0.96,
    height: itemHeight,
    flexDirection: "row",
    paddingRight: itemHeight + 15,
    alignItems: "center",
  },
  itemTitle: {
    color: "white",
    fontSize: moderateScale(20),
    fontFamily: K.Fonts.medium,
  },
  imageBox: {
    width: itemHeight,
    height: itemHeight,
    marginRight: 12,
    justifyContent: "center",
    alignItems: "center",
  },
  itemThumbnailImage: {
    position: "absolute",
    height: itemHeight,
    width: itemHeight,
  },
  itemIcon: {
    tintColor: "white",
    width: itemHeight * 0.37,
    height: itemHeight * 0.37,
    alignSelf: "center",
  },
};
