import { Fonts } from "constant-deprecated";
import React from "react";
import {
  Image,
  Linking, Platform, Text,
  TouchableWithoutFeedback, View
} from "react-native";
import { C } from "utilities/"

const dimension = 100;

export default class AppItem extends React.Component {
  handlePress() {
    const { data } = this.props;
    const link =
      Platform.OS === "ios"
        ? data.iosLink
          ? data.iosLink
          : data.link
        : data.androidLink
        ? data.androidLink
        : data.link;
    Linking.openURL(link);
  }

  render() {
    const { data } = this.props;

    return (
      <TouchableWithoutFeedback onPress={this.handlePress.bind(this)}>
        <View style={Styles.item}>
          <View
            style={{
              ...Styles.imageView,
              backgroundColor: data.logo ? undefined : C["color-basic-800"]
            }}
          >
            {data.logo ? (
              <Image
                style={{
                  width: dimension,
                  height: dimension
                }}
                source={{ uri: data.logo }}
              />
            ) : null}
          </View>

          {data.logo ? (
            <Text
              numberOfLines={1}
              adjustsFontSizeToFit={true}
              allowFontScaling
              style={{
                ...Styles.title,
                fontSize: data.logo ? 26 : 20,
                marginTop: data.logo ? 5 : -2
              }}
            >
              {data.name}
            </Text>
          ) : null}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const boxShadow = {
  shadowOffset: { width: 0, height: 1 },
  shadowColor: "black",
  shadowOpacity: 0.12,
  shadowRadius: 3
}

const Styles = {
  item: {
    height: dimension + 30,
    marginVertical: 20,
    marginHorizontal: 12,
    width: 140,
    justifyContent: "center",
    alignItems: "center"
  },
  imageView: {
    ...boxShadow,
    overflow: "hidden",
    width: dimension,
    height: dimension,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    maxWidth: 140,
    color: "white",
    textAlign: "center",
    fontFamily: Fonts.medium
  }
};
