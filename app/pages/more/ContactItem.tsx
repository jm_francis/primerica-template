import { Fonts } from "constant-deprecated";
import React from "react";
import { Dimensions, Text, TouchableOpacity, View } from "react-native";
import ActionSheet from "react-native-actionsheet";
import email from "react-native-email";
import call from "react-native-phone-call";
import { C, moderateScale } from "utilities/";

export default class ContactItem extends React.Component {
  press() {
    const { data } = this.props;
    const contactPhone = "" + (data.contactPhone ? data.contactPhone : "");
    const contactEmail = data.contactEmail ? data.contactEmail : "";

    if (contactPhone.length > 1 && contactEmail.length > 1)
      this.actionSheet.show();
    else if (contactPhone.length > 1) this.actionSelected(0);
    else if (contactEmail.length > 1) this.actionSelected(1);
  }

  actionSelected(index) {
    const contactPhone = "" + this.props.data.contactPhone;
    const contactEmail = "" + this.props.data.contactEmail;

    if (index === 0) {
      call({
        number: contactPhone,
        prompt: false
      });
    } else if (index === 1) {
      email([contactEmail], {
        subject: "",
        body: ""
      });
    }
  }

  render() {
    const { data } = this.props;
    const { name, contactEmail, contactPhone } = data;

    return (
      <View>
        <TouchableOpacity onPress={this.press.bind(this)}>
          <View style={Styles.container}>
            <Text
              numberOfLines={2}
              adjustsFontSizeToFit
              style={Styles.titleText}
            >
              {name}
            </Text>
            {contactPhone ? (
              <Text adjustsFontSizeToFit style={Styles.contactText}>
                {contactPhone}
              </Text>
            ) : null}
            {contactEmail ? (
              <Text adjustsFontSizeToFit style={Styles.contactText}>
                {contactEmail}
              </Text>
            ) : null}
          </View>
        </TouchableOpacity>
        <ActionSheet
          ref={ref => (this.actionSheet = ref)}
          options={["Phone", "Email", "Cancel"]}
          cancelButtonIndex={2}
          onPress={this.actionSelected.bind(this)}
        />
      </View>
    );
  }
}

const modScale = 0.3;

const Styles = {
  container: {
    width: Dimensions.get("window").width * 0.9,
    minHeight: moderateScale(45, modScale),
    borderRadius: 15,
    backgroundColor: C["color-basic-800"],
    paddingHorizontal: 20,
    paddingVertical: 6,
    justifyContent: "center",
    alignItems: "flex-start",
    marginVertical: 7
  },
  titleText: {
    color: "white",
    fontFamily: Fonts.bold,
    fontSize: moderateScale(19, modScale)
  },
  contactText: {
    color: "rgb(18, 127, 255)",
    fontFamily: Fonts.medium,
    fontSize: moderateScale(17, modScale)
  }
};
