import React, { Component } from "react";
import { View, Text, ScrollView } from "react-native";
import { QuickStyle } from "react-native-shortcuts/";
import { Fonts } from "constant-deprecated/";
import AppItem from "./AppItem";
import ContactItem from "./ContactItem";
import OtherItem from "./OtherItem";
import { scale, moderateScale } from "utilities/";

import Backend from "backend/";
import { Buttoon, IconPrimr, Kitten } from "components/";
import { getStatusBarHeight } from "react-native-iphone-x-helper";
import { C } from "utilities/";
import { spacing } from "utilities/";

export default class More extends Component {
  state = {
    hideItems: false,
    items: [],
    contacts: [],
    otherItems: [],
  };

  componentDidMount() {
    Backend.firestoreHandler.more(this.more.bind(this));
    const hideItems =
      Backend.firestoreHandler._account?.email === "demo@demo.com" &&
      Backend.firestoreHandler._config?.variables.reviewMode;
    this.setState({ hideItems });
  }

  more(noSortData) {
    const data = noSortData.sort((a, b) => {
      return a.position > b.position;
    });
    let items = [];
    const contacts = [];
    otherItems = [];
    let index = 0;
    for (d in data) {
      const itemData = data[d];
      if (!itemData) {
        console.log("no item data");
        continue;
      }
      if (
        (itemData.contact || itemData.contactEmail || itemData.contactPhone) &&
        ("" + itemData.contact).length > 1
      )
        contacts.push(
          <ContactItem key={`more.contactItem${index++}`} data={itemData} />
        );
      else if (itemData.iosLink || itemData.androidLink)
        items.push(<AppItem key={`more.item${index++}`} data={itemData} />);
      else if (itemData.otherLink)
        otherItems.push(
          <OtherItem key={`more.item${index++}`} data={itemData} />
        );
    }

    this.setState({ items, contacts, otherItems });
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: C.background,
        }}
      >
        <$_Header {...this.props} />
        <ScrollView style={{ flex: 1 }}>
          {this.state.items.length > 0 && !this.state.hideItems ? (
            <Text style={Styles.title}>Tap an Item</Text>
          ) : null}
          <View style={Styles.itemsView}>
            {this.state.hideItems ? null : this.state.items}
          </View>

          {this.state.contacts.length > 0 ? (
            <Text style={Styles.title}>Contacts</Text>
          ) : null}
          <View style={Styles.contactsView}>{this.state.contacts}</View>

          {this.state.otherItems.length > 0 ? (
            <Text style={Styles.title}>More Resources</Text>
          ) : null}
          <View style={Styles.otherView}>{this.state.otherItems}</View>

          <View
            style={{
              ...QuickStyle.fillWidth,
              height: 33,
            }}
          />
        </ScrollView>
      </View>
    );
  }
}

function $_Header(props) {
  const { navigation } = props;
  return (
    <>
      <Kitten.TopNavigation
        accessoryLeft={(_props) => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              navigation.pop();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          />
        )}
        style={{
          backgroundColor: C.background01,
        }}
        // alignment="center"
        title={"More"}
      />
    </>
  );
}

const Styles = {
  button: {
    margin: 2,
    backgroundColor: "rgb(48,48,48)",
  },
  title: {
    fontFamily: Fonts.medium,
    fontSize: moderateScale(24, 0.3),
    color: "white",
    textAlign: "center",
    marginTop: 42,
    marginBottom: 20,
  },
  itemsView: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
  },
  contactsView: {
    flex: 1,
    alignItems: "center",
  },
  otherView: {
    flex: 1,
    alignItems: "center",
  },

  helpButtonContainer: {
    flexDirection: "row",
    marginTop: 43,
    marginBottom: 22,
    alignSelf: "center",
    width: "70%",
    paddingVertical: 8,
    borderRadius: 8,
    backgroundColor: "rgb(48,48,48)",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  helpButtonText: {
    fontSize: moderateScale(21, 0.3),
    color: "white",
    fontFamily: Fonts.medium,
    textAlign: "center",
  },
  helpButtonIcon: {
    width: moderateScale(27),
    height: moderateScale(27),
    tintColor: "white",
  },
  contactContainer: {
    marginTop: 24,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: scale(10),
    paddingTop: scale(15),
  },
};
