import { Fonts } from "constant-deprecated";
import React from "react";
import {
  Dimensions,
  Linking, Text,
  TouchableOpacity, View
} from "react-native";
import { C, moderateScale } from "utilities/";

export default class OtherItem extends React.Component {
  press() {
    const link = this.props.data.otherLink;
    Linking.openURL(link);
  }

  render() {
    const { data } = this.props;
    const { name, otherLink } = data;

    return (
      <TouchableOpacity onPress={this.press.bind(this)}>
        <View style={Styles.container}>
          <Text numberOfLines={2} adjustsFontSizeToFit style={Styles.titleText}>
            {name}
          </Text>
          <Text adjustsFontSizeToFit style={Styles.contactText}>
            {otherLink}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const modScale = 0.3;

const Styles = {
  container: {
    width: Dimensions.get("window").width * 0.9,
    minHeight: moderateScale(45, modScale),
    borderRadius: 15,
    backgroundColor: C["color-basic-800"],
    paddingHorizontal: 20,
    paddingVertical: 8,
    justifyContent: "center",
    alignItems: "flex-start",
    marginVertical: 7
  },
  titleText: {
    color: "white",
    fontFamily: Fonts.bold,
    fontSize: moderateScale(18, modScale)
  },
  contactText: {
    color: "rgb(18, 127, 255)",
    fontFamily: "HelveticaNeue-Thin",
    fontSize: moderateScale(16, modScale)
  }
};
