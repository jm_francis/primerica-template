import { TopNavigation } from "@ui-kitten/components";
import Backend from "backend/";
import notificationHandler from "backend/NotificationHandler/NotificationHandler";
import { MediaPageSchema } from "backend/schemas";
import { eTeamBundle } from "backend/schemas/user/user.schema";
import {
  Buttoon,
  dAccessory,
  IconPrimr,
  ICON_NAME,
  Kitten,
  Toasty,
  UserListItem,
} from "components/";
import K from "constant-deprecated/";
import React, { Component } from "react";
import { Alert, Linking, ScrollView, View } from "react-native";
import { C, getStatusBarHeight, moderateScale, spacing } from "utilities/";

export default class TeamAdmin extends Component {
  state = {
    myTeam: null,
    teamUsers: null,
  };

  componentDidMount() {
    Backend.adminHandler.users(this.allUsers.bind(this));
  }

  didGetAllUsers = false;
  allUsers(allUsers) {
    if (this.didGetAllUsers === true) return;
    this.didGetAllUsers = true;
    Backend.firebasePages.pages((pages: MediaPageSchema[]) => {
      const teamPage: MediaPageSchema = Backend.firebasePages.getMyTeamPage();
      const teamUsers = allUsers.filter(
        (u) =>
          u.team?.teamName === teamPage.name &&
          u.uid !== Backend.firestoreHandler.uid
      );
      this.setState({
        myTeam: teamPage,
        teamUsers,
      });
    });
  }

  render() {
    const { myTeam, teamUsers } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <$_Header {...this.props} team={myTeam} users={teamUsers} />
        <ScrollView style={Styles.container}>
          <$_BroadcastMessage />
          <$_TeamMembers {...this.props} team={myTeam} users={teamUsers} />
        </ScrollView>
      </View>
    );
  }
}

export function $_Header(props) {
  const { team, users, navigation } = props;

  const MenuIcon = (name: ICON_NAME, props: dAccessory) => (
    <IconPrimr
      name={name}
      size={18}
      color={C.text01}
      onPress={() => navigation.goBack()}
    />
  );

  return (
    <>
      <TopNavigation
        style={{
          paddingTop: getStatusBarHeight("safe"),
          backgroundColor: C.background01,
        }}
        title={(_props) => (
          <Kitten.Text
            {..._props}
            style={[
              _props.style,
              {
                marginTop: getStatusBarHeight("safe") - spacing(2),
              },
            ]}
          >
            {team ? `${team.name} Admin` : "Loading..."}
          </Kitten.Text>
        )}
        alignment="center"
        accessoryLeft={() => (
          <Buttoon
            style={{
              backgroundColor: "transparent",
              borderColor: "transparent",
            }}
            onPress={() => {
              navigation.pop();
            }}
            icon={{
              name: "arrow_left",
              color: "white",
              size: moderateScale(20),
            }}
          />
        )}
      />
    </>
  );
}

function $_TeamMembers(props) {
  const { team, users, navigation } = props;
  return (
    <View>
      {users &&
        users.map((user) => (
          <UserListItem
            showLevelProgress
            user={user}
            onPress={(_user) => {
              navigation.navigate("Teammate", { user });
            }}
          />
        ))}
      {/* <$_MockMembers {...props} /> */}
    </View>
  );
}

/**
 * Form demoing the app
 */
function $_MockMembers(props) {
  const { navigation } = props;

  const members = [
    {
      name: "Frodo Baggins",
      email: "frodo@baggins.com",
      profileImage:
        "https://openpsychometrics.org/tests/characters/test-resources/pics/LOTR/1.jpg",
    },
    {
      name: "Pippin Took",
      email: "pippin@tooktook.com",
      profileImage:
        "https://static.wikia.nocookie.net/lotr/images/0/0a/Pippinprintscreen.jpg",
    },
    {
      name: "Samwise Gamgee",
      email: "samwise@gamgee.net",
      profileImage:
        "https://a1cf74336522e87f135f-2f21ace9a6cf0052456644b80fa06d4f.ssl.cf2.rackcdn.com/images/characters_opt/p-the-lord-of-the-rings-sean-astin.jpg",
    },
  ];

  return (
    <>
      {members.map((member) => (
        <UserListItem
          user={member}
          showLevelProgress
          onPress={(_user) => {
            navigation.navigate("Associate", { user: member });
          }}
        />
      ))}
    </>
  );
}

function $_BroadcastMessage(props) {
  const account = Backend.firestoreHandler._account;
  const premier = account.team?.bundle === eTeamBundle.Premier;

  function sendTeamMessage() {
    if (!premier) {
      const body = `Hi, this is ${account.name} with the ${Backend.firestoreHandler._config.variables.appTitle} app. I am interested in upgrading my team area!`;
      Alert.alert(
        "You must upgrade your team's bundle to use this feature.",
        "Upgrading is easy!",
        [
          {
            text: "Cancel",
            style: "cancel",
          },
          {
            text: "Upgrade now!",
            onPress: () =>
              Linking.openURL(K.Variables.appleBusinessChat + "&body=" + body),
          },
        ]
      );
    }
    Alert.prompt(
      "Enter your message:",
      "Every user in your team will receive a notification!",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "Send!",
          onPress: (message) => {
            notificationHandler
              .sendNotificationToTeam(account.team.teamName, message, {})
              .then(() => {
                Toasty({ text1: "Your message was just sent to your team!" });
              })
              .catch((error) => {
                console.log(error);
                Toasty({
                  type: "error",
                  text1: "Message failed to send. Try again later.",
                });
              });
          },
        },
      ]
    );
  }

  return (
    <Buttoon
      icon={{ name: "send" }}
      appearance="ghost"
      // disabled={!premier}
      onPress={sendTeamMessage}
      style={{
        width: "100%",
        backgroundColor: C.surface,
        marginTop: spacing(2),
      }}
      status="primary"
    >
      Broadcast a Message
    </Buttoon>
  );
}

const Styles = {
  container: {
    flex: 1,
    backgroundColor: C.background,
  },
};
