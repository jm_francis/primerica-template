/**
 * @deprecated Use MediaPageSchema instead! OR U DIE X_X
 * A CustomPage created by the user/admin and all its settings
 */
export interface dMediaPage {
  id: string;
  key?: string;
  zoom?: string;
  password?: string | number;
  mediaItem: {
    imageColor?: string;
    color?: string;
    link?: string;
    logo?: string;
    presetLogo?: string;
    team?: boolean;
    visible?: boolean;
  };
  name?: string;
  pageItem: {
    description?: number;
    imageUrl?: number;
    uploadImage?: string;
    visible: boolean;
    link?: string;
  };
  position: number;
  content?: dMediaPageItem[];
}

/**
 * An item that exists on a CustomPage/MediaPage in the pages content
 */
export interface dMediaPageItem {
  id?: string;
  title?: string;
  paragraph?: string;
  media?: string;
  url?: string;
  toPage?: string;
  position: number;
}
