/* eslint-disable @typescript-eslint/interface-name-prefix */
import { TextStyle } from "react-native";
import { C } from "utilities/";

export type ValueOf<T> = T[keyof T];
export type KeyOf<T> = keyof T;

export type PROPS_Colors = typeof C;
export type ENUM_Palette = ValueOf<PROPS_Colors>;

export type PROPS_PrimrTypo = {
  largeTitle: TextStyle;
  headline: TextStyle;
  title: TextStyle;
  titleEmphasized: TextStyle;
  subtitle: TextStyle;
  subtitleEmphasized: TextStyle;
  body: TextStyle;
  bodyEmphasized: TextStyle;
  caption: TextStyle;
  captionEmphasized: TextStyle;
};

/**
 * Props for theme provider
 */
export interface PROPS_PrimrTheme {
  C: PROPS_Colors;
  dark?: boolean;
}

/**
 * List of theme index
 */
export type ENUM_Theme = `themeLight`; //* Add more theme index here if wanted
export interface PROPS_ThemeProvider {
  theme: ENUM_Theme;
  setTheme?(theme: ENUM_Theme): void;
  children: React.ReactNode;
}
