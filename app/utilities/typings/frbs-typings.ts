/**
 * Any type of ID (userID, profileID...)
 */
export type zID = string | null;

export interface IRgetFCM {
  /**
   * Promise message
   */
  message: string;
  /**
   * User Data (flat list)
   */
  userData: object;
}
