import { NavigationInjectedProps } from "react-navigation"
import { PROPS_PrimrTheme } from "."

export interface IPSCR extends NavigationInjectedProps<{}> {
  theme: PROPS_PrimrTheme
}
