import { PROPS_PrimrTheme } from "."

export interface PROPSCOMP {
  testID?: string
  theme?: PROPS_PrimrTheme
}
