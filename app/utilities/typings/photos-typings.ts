import { scale } from "utilities/";

/**
 * `imageInfo` (e.g. imageInfo00, imageInfo01) contains props of such image
 */
export interface dImageInfo {
  /** Local path of an image */
  path: string | null;
  /** Image size */
  size: { height: number | null; width: number | null };
  /** Uri of an image, usually FRBS link */
  uri?: string | null;
  /** Storage bucket of an image, usually FRBS's Storage subfolders */
  bucket?: "users" | null;
}

export const __imageInfo: dImageInfo = {
  path: `https://randomuser.me/api/portraits/women/95.jpg`,
  size: { height: scale(134), width: scale(134) },
  uri: "https://randomuser.me/api/portraits/women/95.jpg",
  bucket: null,
};

export type mediaType = "photo" | "video";
