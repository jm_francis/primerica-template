import { useWindowDimensions } from "react-native";

function debounce(fn, ms) {
  let timer;
  return () => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      timer = null;
      fn.apply(this, arguments);
    }, ms);
  };
}

/**
 * A hook that gets dynamic dimensions,
 * and use such value to extend `react-native-size-matters`
 * ---
 * @example
 * ```
 * const { WIDTH, HEIGHT, s, vs, ms, mvs } = useDimension("window")
 *
 * ```
 * ---
 * @version 0.9.19
 * @author nguyenkhooi
 */
export function useDimension(type: "screen" | "window" = "window"): dDime {
  const { width: WIDTH, height: HEIGHT } = useWindowDimensions();

  /**
   * Extensions of `react-native-size-matters`,
   * using dynamic WIDTH and HEIGHT
   * ---
   *
   * @see react-native-size-matters
   */
  //Default guideline sizes are based on standard ~5" screen mobile device
  const guidelineBaseWidth = 350;
  const guidelineBaseHeight = 680;
  const [shortDimension, longDimension] =
    WIDTH < HEIGHT ? [WIDTH, HEIGHT] : [HEIGHT, WIDTH];

  let s = (size: number) => (shortDimension / guidelineBaseWidth) * size;
  let vs = (size: number) => (longDimension / guidelineBaseHeight) * size;
  let ms = (size: number, factor = 0.5) => size + (s(size) - size) * factor;
  let mvs = (size: number, factor = 0.5) => size + (vs(size) - size) * factor;
  return { WIDTH, HEIGHT, s, vs, ms, mvs };
}

let breakPoints = { xs: 0, sm: 576, md: 768, lg: 992, xl: 1200, xxl: 1400 };
export interface dDime {
  WIDTH: number;
  HEIGHT: number;
  s(size: number): number;
  vs(size: number): number;
  ms(size: number): number;
  mvs(size: number): number;
  // breakPoint: "xs" | "sm" | "md" | "lg" | "xl" | "xxl";
}
