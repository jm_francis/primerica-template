import { dImageInfo, PROPSCOMP } from "utilities/";

export interface iHProps extends PROPSCOMP {
  disabled?: boolean;
  /** Should we 'base' the image based on `userID` and `bucket`? */
  shouldBase?: { userID: string; bucket: "users" };
  /** Image URI/path */
  imageURI?: string | null;
  //* function
  /** handleImage() externally */
  handleImage(iURL: string): void;
  handleRemoval?(): void;
  onUploadImage?(): void;
  onRemoveImage?(): void;
  onPress?(): void;
}

/**
 * Enum of media picker error codes
 */
export enum ERROR_PICKER {
  PHOTO_GALLERY_DONE_IMPORTED = "PHOTO_GALLERY_DONE_IMPORTED",
  PHOTO_GALLERY_ERR = "PHOTO_GALLERY_ERR",
  PHOTO_GALLERY_CANCELLED = "PHOTO_GALLERY_CANCELLED",
  PHOTO_CAM_DONE_IMPORTED = "PHOTO_CAM_DONE_IMPORTED",
  PHOTO_CAM_CANCELLED = "PHOTO_CAM_CANCELLED",
  PHOTO_CAM_ERR = "PHOTO_CAM_ERR",
}

/**
 * Promise result of media picker
 */
export interface dPhoto12 {
  code: ERROR_PICKER;
  imageInfo?: dImageInfo;
  error?: any;
}
