import { Toasty } from "components/";
import ImageCropPicker, { Image } from "react-native-image-crop-picker";
import { dImageInfo, scale } from "utilities/";
import { ERROR_PICKER, dPhoto12 } from "./media-picker.props";

/** 
 * Import photo from gallery
 * 
 * @example
 * ```
 *  fn.media
      .photoFromGallery()
      .then(imageInfo => handleImage(imageInfo))
      .catch( handle internally )
      // .catch(err => console.log('errXYZ: ', err));
* ```
* ---
* @version 0.11.2
* - *Fix unidentified ERROR_PICKER code*
*/
export const photoFromGallery = (imageSize?: number) => {
  let _imageSize = imageSize || scale(134);
  return new Promise<dPhoto12>((resolve, reject) => {
    ImageCropPicker.openPicker({
      width: _imageSize * 3,
      height: _imageSize * 3,
      cropping: true,
      includeBase64: true,
      includeExif: false,
    })
      .then((image: Image) => {
        // console.log('ImgUploader<> Images from Gallery : ', image.path);
        var imageInfo: dImageInfo = {
          size: { width: image.width, height: image.height },
          path: image.path,
        };
        resolve({
          code: ERROR_PICKER.PHOTO_GALLERY_DONE_IMPORTED,
          imageInfo: imageInfo,
        });
      })
      .catch((error) => {
        console.log("ImgUploader<> PHOTO_GALLERY_ERR: ", error.message);
        switch (true) {
          case error.message.includes("User cancelled image selection"):
            Toasty({ text1: "Cancelled", type: "error" });
            reject({ code: ERROR_PICKER.PHOTO_GALLERY_CANCELLED, error });
            break;
          default:
            Toasty({
              text1: `Error importing photo from gallery`,
              type: "error",
            });
            reject({ code: ERROR_PICKER.PHOTO_GALLERY_ERR, error });
            break;
        }
      });
  });
};

/** 
 * Import photo from camera
 * 
 * ---
 * @example
 * ``` 
 * fn.media
    .photoFromCamera()
    .then(imageInfo => handleImage(imageInfo))
    .catch(handle internally)
    // .catch(err => console.log('errXYZ: ', err));
 * ```
 * ---
 * @version 0.11.2
 * - *Fix unidentified ERROR_PICKER code*
 */
export const photoFromCamera = (imageSize?: number) => {
  let _imageSize = imageSize || scale(134);
  return new Promise((resolve, reject) => {
    ImageCropPicker.openCamera({
      width: _imageSize * 3,
      height: _imageSize * 3,
      cropping: true,
      useFrontCamera: true,
      compressImageQuality: 0.9,
      includeBase64: true,
      includeExif: false,
    })
      .then((image: Image) => {
        // console.log("ImgUploader<> Images from Gallery : ", image.path);
        var imageInfo: dImageInfo = {
          size: { width: image.width, height: image.height },
          path: image.path,
        };
        resolve({
          code: ERROR_PICKER.PHOTO_CAM_DONE_IMPORTED,
          imageInfo: imageInfo,
        });
      })
      .catch((error) => {
        console.log("ImgUploader<> PHOTO_CAM_ERR: ", error);
        switch (true) {
          case error.message.includes("Error: User cancelled image selection"):
            reject({ code: ERROR_PICKER.PHOTO_CAM_CANCELLED, error });
            break;
          default:
            Toasty({
              text1: `Error importing photo from camera`,
              type: "error",
            });
            reject({ code: ERROR_PICKER.PHOTO_CAM_ERR, error });
            break;
        }
      });
  });
};
