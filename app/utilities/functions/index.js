// @ts-check
import * as calendar from "./calendar-functions";
import * as js from "./js-functions";
import * as media from "./media-functions";
export const fn = {
  js,
  media,
  calendar,
};

// export default functions
