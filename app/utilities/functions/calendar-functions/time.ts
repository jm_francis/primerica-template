import React, { useState } from "react";
import { fn } from "../index";

export function parseISOString(s) {
  var b = s.split(/\D+/);
  return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
}

export function socialFriendlyTime(givenDate: Date) {
  const today: Date = new Date();
  const oneday = 24 * 60 * 60 * 1000;
  const yesterday: Date = new Date(today.getTime());
  yesterday.setTime(yesterday.getTime() - oneday);
  // const timestamp: Date = item.timestamp.toDate();
  let time: string = givenDate.toLocaleTimeString([], {
    hour: "numeric",
    minute: "2-digit",
  });
  if (
    givenDate.getDate() !== today.getDate() ||
    givenDate.getMonth() !== today.getMonth()
  ) {
    if (
      givenDate.getDate() === yesterday.getDate() &&
      givenDate.getMonth() === yesterday.getMonth()
    ) {
      time = "Yesterday";
    } else {
      const days = Math.floor((today.getTime() - givenDate.getTime()) / oneday);
      const weeks = Math.floor(days / 7);
      time =
        weeks > 0
          ? `${weeks} week${weeks > 1 ? "s" : ""} ago`
          : `${days} day${days > 1 ? "s" : ""} ago`;
    }
  }
  return time;
}

export function timeToAMPMString(date) {
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let ampm = hours >= 12 ? "PM" : "AM";
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  const strTime = hours + ":" + minutes + " " + ampm;
  return strTime;
}

export function prettyTime(date: Date) {
  const AmPm = date.getHours() > 12 ? "PM" : "AM";
  return `${date.getHours() > 12 ? date.getHours() - 12 : date.getHours()}:${
    date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
  } ${AmPm}`;
}

export function dateFormat(_date) {
  if (!_date) return null;
  let date = _date;
  if (_date.toDate) date = _date.toDate();
  const month = date.getMonth() + 1;
  const monthStr = month < 10 ? "0" + month : `${month}`;
  const dateStr =
    date.getDate() < 10 ? "0" + date.getDate() : "" + date.getDate();
  return `${monthStr}/${dateStr}/${date.getFullYear()}`;
}

/**
 * Returns date as number whether it is a JavaScript Date or Firestore Timestamp
 */
export function dateAsNumber(_date) {
  if (!_date) return null;
  let date = _date;
  if (_date.toDate) date = _date.toDate();
  return date.getTime();
}

/**
 * @deprecated  under construction
 */
export function useCalendar() {
  let Config = {
    // calendarId: "cal_XhJglGwjmwC0JlbX_mJqQeLrjprq0XGlqJlUL4w",
    calendarId: "cal_XDAY9fQIYSfBHW2A_AILwqmN@lQLNtj2eNmVu3Q",
    calendarAccessToken: "DSGu18kQSYlsPAAKKpuXT-RsgLJM4zQV",
  };
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [events, setEvents] = useState<dCalendarEvent[]>([]);

  function fetchEvents() {
    /**
     * Proxy URL is to fix the CORS policy issue on web
     */
    const proxyUrl = "https://cors-anywhere.herokuapp.com/";
    setLoading(true);
    fetch(
      `${proxyUrl}https://api.cronofy.com/v1/events?tzid=Etc/UTC&calendar_ids[]=${Config.calendarId}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${Config.calendarAccessToken}`,
          // "Access-Control-Allow-Origin": "https://apptakeoff.com/",
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        let events: dCalendarEvent[] = [];
        for (let e in json.events) {
          const event = json.events[e];
          // console.log("parsing event " + event.start);
          const date = fn.calendar.parseISOString(event.start);
          const endDate = fn.calendar.parseISOString(event.end);

          const startTime = date
            ? fn.calendar.timeToAMPMString(date).replace(":NaN", ":00")
            : "";
          const endTime = endDate
            ? fn.calendar.timeToAMPMString(endDate).replace(":NaN", ":00")
            : null;
          const time = endTime ? `${startTime} - ${endTime}` : `${startTime}`;

          events.push({
            title: event.summary,
            description: event.description,
            date,
            endDate,
            location: event.location,
            time,
          });
        }

        setEvents(events);
        setLoading(false);

        // console.log("FINAL EVENTS: " + JSON.stringify(events));
      })
      .catch((err) => {
        console.log(err);
        setError(err);
        // do something?
        setLoading(false);
        return;
      });
  }

  let data = { events: events, fetch: fetchEvents };

  React.useEffect(function initialize() {
    fetchEvents;
  }, []);

  return [data, loading, error];
}

export interface dCalendarEvent {
  title?: string;
  description?: string;
  date: Date;
  endDate: Date;
  location: string;
  time?: string;
}
