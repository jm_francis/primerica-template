import { Platform } from "react-native";
import {
  scale as _scale,
  verticalScale as _verticalScale,
  moderateScale as _moderateScale
} from "react-native-size-matters";

export const scale = value => {
  if (!Platform.isPad) return value;
  return _scale(value);
};

export const verticalScale = value => {
  if (!Platform.isPad) return value;
  return _verticalScale(value);
};

export const moderateScale = (a, b) => {
  if (!Platform.isPad) return a;
  return _moderateScale(a, b);
};
