import { PushNotificationObject } from "react-native-push-notification";
// import { colors } from "utilities";

export const PushNotiCONFIG: PushNotificationObject = {
  color: "blue",
  ticker: "My Notification Ticker", // (optional)
  autoCancel: true, // (optional) default: true
  largeIcon: "ic_launcher", // (optional) default: "ic_launcher"
  smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher"
  vibrate: true, // (optional) default: true
  vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
  tag: "some_tag", // (optional) add tag to message
  group: "group", // (optional) add group to message
  ongoing: false, // (optional) set whether this is an "ongoing" notification
  actions: ["Yes", "No"], // (Android only) See the doc for notification actions to know more

  /* iOS only properties */
  alertAction: "view", // (optional) default: view
  category: "", // (optional) default: empty string
  userInfo: {}, // (optional) default: {} (using null throws a JSON value '<null>' error)

  /* iOS and Android properties */
  title: "title",
  message: "caption",
  soundName: "default",
  number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
};
