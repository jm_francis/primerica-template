/* eslint-disable indent */
// import { sanFranciscoWeights, robotoWeights } from "react-native-typography";
import { TextStyle } from "react-native";
import { IS_ANDROID, scale } from "../helpers";
import { PROPS_PrimrTypo } from "../typings";

const robotoWeights = {
  bold: { fontWeight: "bold" },
  medium: { fontWeight: "700" },
  regular: { fontWeight: "500" },
};

const sanFranciscoWeights = {
  bold: { fontWeight: "bold" },
  semibold: { fontWeight: "700" },
  regular: { fontWeight: "500" },
};

/**
 * @description Typography preset based on Primr's design system
 *
 * The various styles of fonts are defined in the <Text /> component.
 *
 * @see TfontFamily for fontFamily on Android
 */
export const typoPrimr: PROPS_PrimrTypo = {
  largeTitle: IS_ANDROID
    ? ({
        fontFamily: "Lato-Black",
        letterSpacing: -1.5,
        fontSize: scale(30),
      } as TextStyle)
    : ({
        fontFamily: "AvenirNext-Bold",
        fontWeight: "700",
        letterSpacing: -1,
        fontSize: scale(30),
      } as TextStyle),
  headline: IS_ANDROID
    ? ({ ...robotoWeights.medium, fontSize: scale(24) } as TextStyle)
    : ({ ...sanFranciscoWeights.semibold, fontSize: scale(24) } as TextStyle),
  title: IS_ANDROID
    ? ({ ...robotoWeights.regular, fontSize: scale(20) } as TextStyle)
    : ({ ...sanFranciscoWeights.regular, fontSize: scale(20) } as TextStyle),
  titleEmphasized: IS_ANDROID
    ? ({ ...robotoWeights.medium, fontSize: scale(20) } as TextStyle)
    : ({ ...sanFranciscoWeights.semibold, fontSize: scale(20) } as TextStyle),
  subtitle: IS_ANDROID
    ? ({ ...robotoWeights.regular, fontSize: scale(16) } as TextStyle)
    : ({ ...sanFranciscoWeights.regular, fontSize: scale(16) } as TextStyle),
  subtitleEmphasized: IS_ANDROID
    ? ({ ...robotoWeights.medium, fontSize: scale(16) } as TextStyle)
    : ({ ...sanFranciscoWeights.semibold, fontSize: scale(16) } as TextStyle),
  body: IS_ANDROID
    ? ({ ...robotoWeights.regular, fontSize: scale(14) } as TextStyle)
    : ({ ...sanFranciscoWeights.regular, fontSize: scale(14) } as TextStyle),
  bodyEmphasized: IS_ANDROID
    ? ({ ...robotoWeights.medium, fontSize: scale(14) } as TextStyle)
    : ({ ...sanFranciscoWeights.semibold, fontSize: scale(14) } as TextStyle),
  caption: IS_ANDROID
    ? ({ ...robotoWeights.regular, fontSize: scale(12) } as TextStyle)
    : ({ ...sanFranciscoWeights.regular, fontSize: scale(12) } as TextStyle),
  captionEmphasized: IS_ANDROID
    ? ({ ...robotoWeights.bold, fontSize: scale(12) } as TextStyle)
    : ({ ...sanFranciscoWeights.bold, fontSize: scale(12) } as TextStyle),
};
