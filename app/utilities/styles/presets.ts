import R from "ramda";
import { StyleSheet, TextStyle, ViewStyle } from "react-native";
import {
  C as __colors,
  PROPS_Colors,
  typoPrimr
} from "utilities/";
import { getBottomSpace, scale } from "../helpers";
import { spacing } from "./spacing";

/**
 * A set of preset styles for the codebase, based on Primr's design system
 */
export const PRE = (mau?: PROPS_Colors) => {
  let C: PROPS_Colors = R.isNil(mau) ? __colors : mau;
  return {
    HEADER: {
      SECTIONS: {
        ...typoPrimr.subtitleEmphasized,
        paddingVertical: spacing(2),
        color: C.grey900,
      } as TextStyle,
      SEPARATOR_CTNR: {
        backgroundColor: C.surface,
        flexDirection: "row",
        height: scale(30),
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: spacing(3),
      } as ViewStyle,
      LABEL_TXT: {
        ...typoPrimr.bodyEmphasized,
        color: C.dim,
      } as TextStyle,
    },

    /**
     * Technically a styled button
     * that when user click, an input will show up
     *
     * We use this to fix the android input display issue that
     * not show the beginning of the text
     */
    pseudoInput: {
      style: {
        backgroundColor: C.transparent,
        // alignItems: "flex-start",
        // paddingLeft: spacing(5),
        // paddingVertical: spacing(2),
        // borderWidth: 1,
        // borderColor: C.line,
        // borderRadius: scale(10),
        // height: scale(50)
      } as ViewStyle,
      textStyle: {
        ...typoPrimr.subtitleEmphasized,
        textAlign: "left",
        color: C.text,
        // marginVertical: spacing(2),
      } as TextStyle,
    },
    BUTTON: {
      ICONCTNR: {
        borderRadius: 500, //* round af
        backgroundColor: C.transparent,
      } as ViewStyle,
      SAFE_ICONCTNR: {
        padding: scale(5),
        borderRadius: 500, //* round af
        backgroundColor: C.transparent,
      } as ViewStyle,
    },
    CARD: {
      ACCORDION: {
        OVERVIEWCTNR_00: {
          borderColor: C.surface,
          borderTopLeftRadius: scale(10),
          borderTopRightRadius: scale(10),
          paddingTop: scale(10),
        } as ViewStyle,
        OVERVIEWCTNR_1: {
          borderColor: C.surface01,
          borderTopLeftRadius: scale(10),
          borderTopRightRadius: scale(10),
          paddingTop: scale(10),
        } as ViewStyle,
        OVERVIEWCTNR_2: {
          borderColor: C.primaryDarker,
          borderTopLeftRadius: scale(10),
          borderTopRightRadius: scale(10),
          paddingTop: scale(10),
        } as ViewStyle,
        CONTENTCTNR: {
          padding: scale(10),
          backgroundColor: C.surface01,
          borderBottomEndRadius: scale(10),
          borderBottomLeftRadius: scale(10),
          marginBottom: scale(5),
        } as ViewStyle,
        CONTENTCTNR_2: {
          padding: scale(10),
          backgroundColor: C.primaryDarker,
          borderBottomEndRadius: scale(10),
          borderBottomLeftRadius: scale(10),
          marginBottom: scale(5),
        } as ViewStyle,
        CONTENTCTNR_00: {
          padding: scale(10),
          backgroundColor: C.surface,
          borderBottomEndRadius: scale(10),
          borderBottomLeftRadius: scale(10),
          marginBottom: scale(5),
        } as ViewStyle,
        CONTENT_BUTTONCTNR_1: {
          alignItems: "stretch",
          paddingVertical: scale(5),
        } as ViewStyle,
        CONTENT_BUTTONCTNR_2: {
          alignItems: "center",
          paddingVertical: scale(5),
        } as ViewStyle,
        CONTENT_BUTTONCTNR_3: {
          alignItems: "flex-end",
          paddingVertical: scale(5),
        } as ViewStyle,
      },
    },
    FORM: {
      CTNR_SAFE: {
        paddingHorizontal: spacing(5),
        backgroundColor: C.surface,
        // paddingBottom: getBottomSpace("safe"),
      } as ViewStyle,
      CTNR: {
        paddingHorizontal: 0,
        backgroundColor: C.surface,
      } as ViewStyle,
      INPUT_LABEL: {
        ...typoPrimr.subtitleEmphasized,
        fontFamily: `Montserrat-Bold`,
        color: C.text,
        flex: 1,
      } as TextStyle,
    },

    /**
     * CARP Preset for Google Places Autocomplete
     */
    PLACES: {
      container: { backgroundColor: C.transparent } as ViewStyle,
      textInputContainer: {
        height: scale(60),
        backgroundColor: C.surface,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        justifyContent: "flex-start",
      } as ViewStyle,
      textInput: {
        backgroundColor: C.transparent,
        ...typoPrimr.subtitle,
        color: C.text,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 10,
        paddingRight: 10,
        // borderWidth: 1,
      } as TextStyle,
      description: {
        ...typoPrimr.body,
        color: C.text,
        borderTopWidth: 0,
        borderBottomWidth: 0,
      } as TextStyle,
      predefinedPlacesDescription: {
        ...typoPrimr.subtitle,
        color: C.primary,
        borderTopWidth: 0,
        borderBottomWidth: 0,
      } as TextStyle,
      separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: C.transparent,
      } as ViewStyle,
      poweredContainer: {
        justifyContent: "flex-end",
        alignItems: "center",
        backgroundColor: C.transparent,
      } as ViewStyle,
    },
    SCR: {
      paddingHorizontal: spacing(3),
      // paddingBottom: getBottomSpace("safe"),
    } as ViewStyle,
    /** General footer CTNR */
    FOOTER: {
      paddingHorizontal: spacing(5),
      paddingBottom: getBottomSpace("safe"),
    } as ViewStyle,
  };
};
