import { primrLight } from "./primr-light";

export const themeLight = {
  /**
   * The colors is available to use, but prefer using the name.
   */
  ...primrLight,
  grey900: primrLight["color-basic-900"],
  grey600: primrLight["color-basic-600"],
  grey500: primrLight["color-basic-500"],
  pitchWhite: primrLight["color-basic-100"],
  /**
   * A helper for making something see-thru. Use sparingly as many layers of transparency
   * can cause older Android devices to slow down due to the excessive compositing required
   * by their under-powered GPUs.
   */
  transparent: "rgba(0, 0, 0, 0)",
  /**
   * The main tinting colors.
   */
  primary: primrLight["color-primary-500"],
  /**
   * The main tinting color, but darker.
   */
  primaryDarker: primrLight["color-primary-700"],
  /**
   * The sceondary tinting colors.
   */
  accent: primrLight["color-info-500"],
  /**
   * The secondary tinting colors, but darker.
   */
  accentDarker: primrLight["color-info-700"],
  /**
   * The screen background.
   */
  background: primrLight["color-basic-1100"],
  /**
   * The `dark` screen background.
   */
  background01: primrLight["color-basic-1100"],
  /**
   * The screen surface. Usually used for modal screen
   */
  surface: primrLight["color-basic-900"],
  /**
   * The `dark` screen surface.
   */
  surface01: primrLight["color-basic-900"],
  /**
   * A subtle color used for borders and lines.
   */
  line: primrLight["color-basic-800"],
  /**
   * The default color of text in many components.
   */
  text: primrLight["color-basic-100"],
  /**
   * The '01' color of text in many dark-background components.
   */
  text01: primrLight["color-basic-100"],
  /**
   * Secondary information.
   */
  dim: primrLight["color-basic-600"],
  /**
   * Error messages and icons.
   */
  errorRed: primrLight["color-danger-500"],
  /**
   * Warning color
   */
  hazardYellow: primrLight["color-warning-500"],
  /**
   * Info color
   */
  infoBlue: primrLight["color-info-500"],
  /**
   * Success color
   */
  awakenVolt: primrLight["color-success-600"],
  /**
   * Admin color
   */
  admin: primrLight["color-danger-300"],
};

// export const C = primrLight;

export const C = themeLight;
