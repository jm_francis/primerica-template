//@ts-check
import { moderateScale, verticalScale } from "./SizeMatters";

export * from "./configs";
export * from "./functions";
export * from "./helpers";
export * from "./styles";
export * from "./typings";
export { moderateScale as scale, verticalScale, moderateScale };

