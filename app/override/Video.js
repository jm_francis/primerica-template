import React from "react";
import { View, Modal as _Modal, Platform } from "react-native";
import Orientation from "react-native-orientation";
import VideoPlayer from "react-native-video-controls";
import _Video from "react-native-video";

class Modal extends React.Component {
  render() {
    return Platform.OS === "ios?" ? (
      this.props.children
    ) : (
      <_Modal style={{ flex: 1 }}>
        <View
          style={{
            flex: 1,
            backgroundColor: "black"
          }}
        >
          {this.props.children}
        </View>
      </_Modal>
    );
  }
}

export class Video extends React.Component {
  state = {
    duration: 0,
    paused: false,
    end: false
  };

  componentDidMount() {
    Orientation.unlockAllOrientations();
  }

  onFullscreenPlayerWillPresent() {
    const { onFullscreenPlayerWillPresent } = this.props;
    if (onFullscreenPlayerWillPresent) onFullscreenPlayerWillPresent();

    if (Platform.OS === "ios") return;

    this.toggle();
  }
  onFullscreenPlayerWillDismiss() {
    const { onFullscreenPlayerWillDismiss } = this.props;
    if (onFullscreenPlayerWillDismiss) onFullscreenPlayerWillDismiss();

    if (Platform.OS === "android") return;

    this.toggle();
  }

  onEnd() {
    this.toggle();
  }
  toggle() {
    const { onEnd } = this.props
    if (Platform.OS === "ios") return;
    const end = !this.state.end;
    if (end === true) Orientation.lockToPortrait();
    this.setState({ end }, () => {
      end && onEnd && onEnd()
    });
  }

  onLoad(data) {
    const { onLoad } = this.props;
    if (onLoad) onLoad(data);
  }
  onProgress(data) {
    const { onProgress } = this.props;
    if (onProgress) onProgress(data);
  }

  render() {
    return this.state.end === false ? (
      <Modal>
        {Platform.OS === "ios" ? (
          <_Video
            resizeMode="contain"
            {...this.props}
            onLoad={this.onLoad.bind(this)}
            onProgress={this.onProgress.bind(this)}
            onEnd={this.onEnd.bind(this)}
            paused={this.state.paused}
            onFullscreenPlayerWillPresent={this.onFullscreenPlayerWillPresent.bind(
              this
            )}
            onFullscreenPlayerWillDismiss={this.onFullscreenPlayerWillDismiss.bind(
              this
            )}
            style={{
              ...this.props.style,
              flex: 1
            }}
            ref={ref => {
              this.props.videoRef(ref);
              this.videoRef = ref;
            }}
          >
            {this.props.children}
          </_Video>
        ) : null}

        {Platform.OS === "android" ? (
          <VideoPlayer
            {...this.props}
            toggleResizeModeOnFullscreen={true}
            onBack={this.onEnd.bind(this)}
            onEnterFullscreen={this.onFullscreenPlayerWillPresent.bind(this)}
            onExitFullscreen={this.onFullscreenPlayerWillDismiss.bind(this)}
            onEnd={this.onEnd.bind(this)}
            onPlay={this.onLoad.bind(this)}
          />
        ) : null}
      </Modal>
    ) : null;
  }
}
