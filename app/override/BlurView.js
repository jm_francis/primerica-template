import React from "react";
import { Platform, View } from "react-native";
import { BlurView as _BlurView } from "@react-native-community/blur";

export default class BlurView extends React.Component {
  render() {
    const { disableAndroid } = this.props;
    const androidStyle = disableAndroid
      ? {}
      : {
          backgroundColor: "rgba(0, 0, 0, 0.5)"
        };
    return Platform.OS === "ios" ? (
      <_BlurView blurType="regular" {...this.props}>
        {this.props.children}
      </_BlurView>
    ) : (
      <View
        {...this.props}
        style={{
          ...this.props.style,
          ...androidStyle
        }}
      >
        {this.props.children}
      </View>
    );
  }
}
