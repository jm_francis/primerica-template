import { AnimatedText, Animations } from "animation";
import { Fonts } from "constant-deprecated";
import React from "react";
import { QuickStyle } from "utilities/";

export default class StaggerText extends React.Component {
  animations = new StaggerAnimations();

  componentDidMount() {
    setTimeout(() => {
      this.present();
    }, 150);
  }

  present(data = {}) {
    const { delay, callback } = data;
    this.animations.present(data);
  }
  dismiss(callback) {
    this.animations.dismiss(callback);
  }

  render() {
    const { text = "", textStyle = {}, style = {} } = this.props;

    return (
      <AnimatedText
        ref={ref => (this.animations.titleRef = ref)}
        text={text}
        style={{
          ...style
        }}
        textStyle={{
          color: "white",
          fontSize: 20,
          fontFamily: Fonts.medium,
          ...QuickStyle.boxShadow,
          ...textStyle
        }}
        defaultValues={{
          opacity: 0
        }}
        offsetX={-10}
      />
    );
  }
}

const titleAnimationValues = {
  stagger: 15,
  duration: 200
};
class StaggerAnimations extends Animations {
  present(data) {
    const { delay = 0, callback } = data;
    if (!this.titleRef) return;
    this.titleRef
      .timing("opacity", {
        toValue: 1,
        delay,
        ...titleAnimationValues
      })
      .start(callback);
  }
  dismiss(callback) {
    this.titleRef
      .timing("opacity", {
        toValue: 0,
        reverse: true,
        ...titleAnimationValues,
        duration: 33
      })
      .start(callback);
  }
}
