const skip = 4;

export default class CircleRanges {
  static inputRange(multiply = 0) {
    const range = [];
    //let x=0-(multiply*360-1);
    for (let x = 0; x <= 360 + multiply * 360; x += skip) range.push(x / 360);
    return range;
  }
  static range(value, multiply = 0, offset = 0) {
    const range = [];
    //let d=0-(multiply*360-1);
    for (let d = offset; d <= 360 + offset + multiply * 360; d += skip) {
      const deg = d + 180;
      const angle = (deg * Math.PI) / 180;
      range.push(value(angle));
    }
    return range;
  }
  static outputRangeX(radius, multiply, offset) {
    return this.range(
      angle => {
        return radius + radius * Math.sin(angle);
      },
      multiply,
      offset
    );
  }
  static outputRangeY(radius, multiply, offset) {
    return this.range(
      angle => {
        return radius + radius * Math.cos(angle);
      },
      multiply,
      offset
    );
  }
}
