import { Animated, Easing } from "react-native";
import CircleRanges from "./CircleRanges";

export default class Animations {
  static INSTANT = set => {
    if (set === true) {
      Animations.INSTANT_OVERRIDE = {
        delay: 0,
        timing: 0
      };
    } else {
      Animations.INSTANT_OVERRIDE = {};
    }
  };
  static INSTANT_OVERRIDE = {};

  // store values
  values = {};
  initialValues = {};

  // create values
  create(key, _value = 0) {
    if (this.values[key]) return this.values[key];
    const value = new Animated.Value(_value);
    this.values[key] = value;
    this.initialValues[key] = _value;
    return value;
  }
  interpolate(key, _data = {}) {
    if (!this.values[key]) this.create(key);
    const data = Object.assign(
      {},
      {
        inputRange: [0, 1],
        outputRange: [0, 0],
        extrapolate: "clamp",
        useNativeDriver: true
      },
      _data
    );
    return this.values[key].interpolate(data);
  }
  circleInterpolateX(key, radius, multiply) {
    return this.interpolate(key, {
      inputRange: CircleRanges.inputRange(multiply),
      outputRange: CircleRanges.outputRangeX(radius, multiply)
    });
  }
  circleInterpolateY(key, radius, multiply) {
    return this.interpolate(key, {
      inputRange: CircleRanges.inputRange(multiply),
      outputRange: CircleRanges.outputRangeY(radius, multiply)
    });
  }

  // animate values
  timing(key, data) {
    const { toValue = 0, toInitialValue, toOffset } = data;
    const value =
      toInitialValue === true
        ? this.initialValues[key]
        : toOffset
        ? this.values[key]._value + toOffset
        : toValue;
    return Animated.timing(this.values[key], {
      useNativeDriver: false,
      ...data,
      ease: Easing.inOut(),
      toValue: value,
      ...Animations.INSTANT_OVERRIDE
    });
  }
  /**
   * key: string
   * data: { toValue=0, toInitialValue, toOffset, delay, ... }
   */
  spring(key, data) {
    const { toValue = 0, toInitialValue, toOffset, delay = 0 } = data;
    const value =
      toInitialValue === true
        ? this.initialValues[key]
        : toOffset
        ? this.values[key]._value + toOffset
        : toValue;
    return Animated.spring(this.values[key], {
      useNativeDriver: false,
      ...data,
      toValue: value
    });
  }

  //sets: animate a set of values at once
  timingSet(keys, _data) {
    const { stagger = 0 } = _data;
    const animations = [];
    for (k in keys) {
      const key = keys[k];
      const data = {
        delay: k * stagger,
        ..._data
      };
      animations.push(this.timing(key, data));
    }
    return Animated.parallel(animations);
  }
  springSet(keys, _data) {
    const { stagger = 0, massStagger = 0 } = _data;
    const animations = [];
    let index = 0;
    for (k in keys) {
      const key = keys[k];
      const data = {
        delay: k * stagger,
        ..._data,
        mass: _data.mass ? _data.mass + index * massStagger : undefined
      };
      animations.push(this.spring(key, data));
      ++index;
    }
    return Animated.parallel(animations);
  }

  //presets
  toOne(key, data) {
    return Animated.timing(this.values[key], {
      toValue: 1,
      duration: 200,
      ease: Easing.inOut(),
      ...data
    });
  }
}
