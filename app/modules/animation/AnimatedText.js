import React from "react";
import { View, Text, Animated } from "react-native";
import Animations from "./Animations";

export default class AnimatedText extends React.Component {
  animations = new Animations();

  timing(value, data) {
    const { stagger = 0, delay = 0, reverse } = data;

    const values = {};
    for (v in this.animations.values)
      if (v.includes(value)) values[v] = this.animations.values[v];

    let index = reverse === true ? Object.keys(values).length : 0;
    const animations = [];
    for (v in values) {
      const a = this.animations.timing(v, {
        useNativeDriver: true,
        ...data,
        delay: stagger * (reverse === true ? --index : index++) + delay
      });
      animations.push(a);
    }

    return Animated.parallel(animations);
  }
  spring(value, data) {
    const { stagger = 0, delay = 0, reverse } = data;

    const values = {};
    for (v in this.animations.values)
      if (v.includes(value)) values[v] = this.animations.values[v];

    let index = reverse === true ? Object.keys(values).length : 0;
    const animations = [];
    for (v in this.animations.values) {
      if (!v.includes(value)) continue;
      const a = this.animations.spring(v, {
        useNativeDriver: true,
        ...data,
        delay: stagger * (reverse === true ? --index : index++) + delay
      });
      animations.push(a);
    }

    return Animated.parallel(animations);
  }

  _index = 0;
  renderCharacters(_text) {
    const text = _text + "";
    const {
      textStyle = {},
      charStyles = [],
      defaultValues = {},
      offsetX = 0
    } = this.props;
    const { x = 0, y = 0, scale = 1, opacity = 1 } = defaultValues;

    const components = [];
    for (let x = 0; x < text.length; x++) {
      const index = this._index;
      const char = text.charAt(x);
      const charStyle = charStyles[x];
      const animateStyle = {
        transform: [
          { translateX: this.animations.create(`x${index}`, x) },
          { translateY: this.animations.create(`y${index}`, y) },
          { scale: this.animations.create(`scale${index}`, scale) }
        ],
        opacity: this.animations.create(`opacity${index}`, opacity)
      };
      this._index++;
      components.push(
        <Character
          key={`char.${index}`}
          textStyle={textStyle}
          style={[charStyle, animateStyle]}
        >
          {char}
        </Character>
      );
    }
    const { textAlign = "center" } = textStyle;
    const align = textAlign
      ? textAlign.replace("left", "flex-start").replace("right", "flex-end")
      : undefined;
    return (
      <View
        key={text}
        style={{
          marginRight: offsetX < 0 ? Math.abs(offsetX) : 0,
          marginLeft: offsetX > 0 ? offsetX : 0,
          flexDirection: "row",
          flexWrap: "wrap",
          alignSelf: align
        }}
      >
        {components}
      </View>
    );
  }

  render() {
    const { style, text } = this.props;

    const sets = text.split("</>");
    const setComponents = [];
    for (s in sets) setComponents.push(this.renderCharacters(sets[s]));

    return (
      <Animated.View
        style={{
          ...style
        }}
      >
        {setComponents}
      </Animated.View>
    );
  }
}

class Character extends React.Component {
  render() {
    const { style } = this.props;
    const textStyle = {
      ...this.props.textStyle
      //...
    };

    return (
      <Animated.View style={style}>
        <Text style={textStyle}>{this.props.children}</Text>
      </Animated.View>
    );
  }
}
