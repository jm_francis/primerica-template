import React, { Component } from "react";
import { View, Text } from "react-native";
import { QuickStyle, Cheat } from "react-native-shortcuts";

export default class PostView extends Component {
  static getBottomYOfPosts(posts) {
    let y = 0;
    for (x in posts) {
      const post = posts[x];
      y += post.props.height;
      y += post.props.margin ? post.props.margin : 20;
    }
    return y;
  }

  render() {
    const userStyle = Cheat.makeStyleArray(this.props.style);
    const thisStyle = [
      QuickStyle.fillWidthWithSpace(13),
      QuickStyle.boxShadow,
      {
        borderRadius: 3,
        backgroundColor: "#f8fbfb",
        height: this.props.height,
        marginTop: this.props.margin ? this.props.margin : 20
      }
    ];
    const style = thisStyle.concat(userStyle);

    return <View style={style}>{this.props.children}</View>;
  }
}
