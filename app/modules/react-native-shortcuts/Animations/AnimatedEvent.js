import { Animated } from "react-native";
// import { Animated } from 'react-native-reanimated'

export default class AnimatedEvent {
  static scrollYOffset(value, listener, nativeDriver = true) {
    return Animated.event(
      [
        {
          nativeEvent: {
            contentOffset: {
              y: value
            }
          }
        }
      ],
      {
        listener: event => {
          if (listener) listener(event);
        },
        useNativeDriver: false
        // useNativeDriver: nativeDriver? nativeDriver : true
      }
    );
  }
  static scrollXOffset(value, listener, nativeDriver = true) {
    return Animated.event(
      [
        {
          nativeEvent: {
            contentOffset: {
              x: value
            }
          }
        }
      ],
      {
        listener: event => {
          if (listener) listener(event);
        },
        useNativeDriver: false
        // useNativeDriver: nativeDriver? nativeDriver : true
      }
    );
  }
}
