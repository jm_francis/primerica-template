import React from "react";
import { StyleSheet, Dimensions } from "react-native";

const screenDimensions = Dimensions.get("window");

export default class QuickStyle {
  // FILLERS //
  static fillWidth = {
    width: screenDimensions.width
  };
  static fillHeight = {
    height: screenDimensions.height
  };
  static fillScreen = {
    width: screenDimensions.width,
    height: screenDimensions.height
  };
  static fillWidthWithSpace(space) {
    return { width: screenDimensions.width - space * 2 };
  }
  static fillHeightWithSpace(space) {
    return { height: screenDimensions.height - space * 2 };
  }
  static fillScreenWithSpace(space) {
    return {
      height: screenDimensions.height - space * 2,
      width: screenDimensions.width - space * 2
    };
  }

  // CENTER //
  //experimental...
  /*
	static centerHorizontalFromStyle(style) {
		let width = Cheat.getValueFromStyle('width', style);
		if(!width){
			console.log('UH OH... Could not find width in centerHorizontalFromStyle');
			return {}
		}
		const screenWidth = Dimensions.get('window').width;
		const x = (screenWidth-width)/2;
		return { leftMargin: x }//left?
	}
	*/

  // SHADOW //
  static headerShadow = {
    shadowOffset: { width: 0, height: 10 },
    shadowColor: "black",
    shadowOpacity: 0.27,
    shadowRadius: 16
  };
  static boxShadow = {
    shadowOffset: { width: 0, height: 1 },
    shadowColor: "black",
    shadowOpacity: 0.12,
    shadowRadius: 3
  };
  static textShadow = {
    textShadowOffset: { width: 0, height: 1 },
    textShadowColor: "black",
    textShadowRadius: 3
  };

  // FLEX //
  static flexViewCenterVertical = {
    flex: 1,
    justifyContent: "center"
  };

  // TEXT //
  static thinText = {
    fontFamily: "HelveticaNeue-Thin",
    fontSize: 24,
    color: "white",
    textAlign: "center"
  };
  static lightText = {
    fontFamily: "HelveticaNeue-Light",
    fontSize: 24,
    color: "white",
    textAlign: "center"
  };
  static regularText = {
    fontFamily: "HelveticaNeue",
    fontSize: 24,
    color: "white",
    textAlign: "center"
  };

  // TEXT INPUT
  static thinTextInput = {
    fontFamily: "HelveticaNeue-Thin",
    fontSize: 34,
    height: 75,
    color: "white",
    textAlign: "center"
  };

  // POSTS LAYOUT
  static postsLayout = {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center"
  };
}

/*const QuickStyle = StyleSheet.create({
	// FILLERS //
	fillWidth: {
		width: screenDimensions.width,
	},
	fillHeight: {
		height: screenDimensions.height,
	},
	fillScreen: {
		width: screenDimensions.width,
		height: screenDimensions.height,
	},

	// SHADOW //
	headerShadow: {
		shadowOffset: { width:0,height:10 },
		shadowColor: 'black',
		shadowOpacity: 0.27,
		shadowRadius: 16,
	},

	// FLEX //
	flexViewCenterVertical: {
		flex: 1,
		justifyContent: 'center',
	}


});

export default QuickStyle;*/
