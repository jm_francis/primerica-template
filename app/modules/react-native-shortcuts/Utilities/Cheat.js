export default class Cheat {
  static makeStyleArray(style) {
    if (!style) return [];
    else if (typeof style === "array") return style;
    else return [style];
  }

  static getValueFromStyle(value, style) {
    const sty = Cheat.makeStyleArray(style);
    let returnValue;
    for (x in sty) {
      const values = sty[x];
      for (v in values) {
        val = values[v];
        if (val[value]) {
          returnValue = val[value];
        }
      }
    }
    return returnValue;
  }

  static getHeightOfViews(views) {
    let height = 0;
    for (x in views) {
      const view = views[x];
      const styles = Cheat.makeStyleArray(view.props.style);
      for (s in styles) {
        const style = styles[s];
        if (style.height) height += style.height;
      }
    }
    return height;
  }

  static isDictionary(object) {
    return (
      object === "object" &&
      object !== null &&
      !(object instanceof Array) &&
      !(object instanceof Date)
    );
  }
  static isFunction(obj) {
    return obj && {}.toString.call(obj) === "[object Function]";
  }

  //@date
  //@formatAMPM
  static timeToAMPMString(date) {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let ampm = hours >= 12 ? "PM" : "AM";
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    const strTime = hours + ":" + minutes + " " + ampm;
    return strTime;
  }
  //@makeDoubleZero
  static makeDoubleZero(numStr) {
    const toStr = numStr.toString();
    return toStr.length > 1 ? toStr : `0${toStr}`;
  }
  //@loopDateRange
  static loopDateRange(startDate, endDate, func) {
    for (
      let date = new Date(startDate);
      date <= endDate;
      date.setDate(date.getDate() + 1)
    ) {
      func(date);
    }
  }
  //@getYear
  static getYear(date = new Date()) {
    return date.getFullYear();
  }
  //@getMonthNumber
  static getMonthNumber(date = new Date()) {
    return date.getMonth() + 1;
  }
  //@getDay
  static getDay(date = new Date()) {
    return date.getDate();
  }
  //@getSimpleDateString
  static getSimpleDateString(date = new Date()) {
    const month = Cheat.makeDoubleZero(Cheat.getMonthNumber(date));
    const day = Cheat.makeDoubleZero(Cheat.getDay(date));
    return `${Cheat.getYear(date)}-${month}-${day}`;
  }

  //@alertOnce
  static didAlertOnce = false;
  static alertOnce(string) {
    if (Cheat.didAlertOnce) return;
    Cheat.didAlertOnce = true;
    alert(string);
  }

  //not yet tested
  static scaleLockedLengths(length, toLength, length2) {
    const percent = toLength / length;
    return length2 * percent;
  }

  //@colors
  static toHex(x) {
    const hex = x.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
  }
  static rgbToHex(r, g, b) {
    return "#" + Cheat.toHex(r) + Cheat.toHex(g) + Cheat.toHex(b);
  }
  static rgbStringToHex(rgb) {
    if (rgb.includes("#")) return rgb;
    let str = rgb;
    let r, g, b;
    str = str.substring(4, str.length - 1);
    //r
    let nStr = str.substring(0, str.indexOf(","));
    r = Number(nStr);
    str = str.substring(str.indexOf(",") + 1, str.length);
    //g
    nStr = str.substring(0, str.indexOf(","));
    g = Number(nStr);
    str = str.substring(str.indexOf(",") + 1, str.length);
    //b
    nStr = str.substring(0, str.length);
    b = Number(nStr);
    return Cheat.rgbToHex(r, g, b);
  }
  static addAlphaToRGB(alpha, rgb) {
    let color = rgb;
    color = color.replace("rgb", "rgba");
    color = color.replace(")", `,${alpha})`);
    return color;
  }
}
