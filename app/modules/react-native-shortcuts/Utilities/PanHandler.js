import { PanResponder } from "react-native";

export default class PanHandler {
  static createPanResponder(events) {
    const trueEvents = {
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true
    };
    const allEvents = Object.assign({}, trueEvents, events);

    const panResponder = PanResponder.create(allEvents);
    return panResponder;
  }
}
