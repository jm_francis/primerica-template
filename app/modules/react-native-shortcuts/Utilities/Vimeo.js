import React, { Component } from "react";
import RNFetchBlob from "rn-fetch-blob";

export default class Vimeo {
  static data = {};
  static init(data) {
    Vimeo.data = data;
  }
  static updateData(data) {
    Vimeo.data = Object.assign({}, Vimeo.data, data);
    return Vimeo.data;
  }

  static clientIdStr(clientId = Vimeo.clientId) {
    return clientId ? "?client_id=" + clientId : "";
  }

  static getAlbumIdFromUri(uri) {
    let uriStr = uri;
    const albumStr = "album/";
    const start = uriStr.indexOf(albumStr) + albumStr.length;
    return uriStr.substring(start, uriStr.length);
  }
  static getVideoIdFromUri(uri) {
    let uriStr = uri;
    const str = "vimeo.com/";
    const start = uriStr.indexOf(str) + str.length;
    uriStr = uriStr.substring(start, uriStr.length);
    const end = uriStr.indexOf("/");
    return uriStr.substring(0, end);
  }

  static getMP4FromVideo(videoData) {
    if (!videoData || !videoData.files) {
      console.log(
        "WARNING: videoData or videoData.files is null in getMP4FromVideo in Vimeo.js"
      );
      return null;
    }
    const files = videoData.files;
    let link;
    for (f in files) {
      const file = files[f];
      if (file.quality == "sd") {
        link = file.link;
      }
    }
    const end = link.indexOf("&oauth");
    link = link.substring(0, end);
    return link;
  }
  static getThumbnailFromVideo(videoData) {
    if (!videoData || !videoData.pictures || !videoData.pictures.sizes) {
      console.log(
        "WARNING: videoData, videoData.pictures, or videoData.pictures.sizes is null in getThumbnailFromVideo(videoData) in Vimeo.js"
      );
      return null;
    }
    const pictures = videoData.pictures.sizes;
    let setLink = null;
    for (p in pictures) {
      const pic = pictures[p];
      const link = pic.link;
      if (link.includes("200x150")) {
        setLink = link;
        break;
      }
      setLink = link;
    }
    const end = setLink.indexOf("?");
    setLink = setLink.substring(0, end);
    return setLink;
  }
  static getVideo(data = Vimeo.data) {
    Vimeo.updateData(data);
    const { accessToken, userId } = Vimeo.data;

    let uri = ``;
    if (data.shareLink)
      uri = `https://api.vimeo.com/users/${userId}/videos/${this.getVideoIdFromUri(
        data.shareLink
      )}`;
    //TODO everything else!
    else if (data.path)
      uri = `https://api.vimeo.com/users/${userId}${data.path}`;

    uri = uri + "?fields=files,name,link,pictures.sizes.link";

    const promise = new Promise((resolve, reject) => {
      RNFetchBlob.fetch("GET", uri, {
        Authorization: "Bearer " + accessToken
      })
        .then(data => {
          resolve(JSON.parse(data.data));
        })
        .catch(error => {
          return reject(error);
        });
    });

    return promise;
  }

  //get albums
  static getAlbums(data = Vimeo.data) {
    Vimeo.updateData(data);
    const { accessToken, userId } = Vimeo.data;

    const uri =
      "https://api.vimeo.com/users/" + userId + "/albums" + Vimeo.clientIdStr();
    return RNFetchBlob.fetch("GET", uri, {
      Authorization: "Bearer " + accessToken
    }).then(data => {
      return JSON.parse(data.data).data;
    });
  }

  //get videos (options: albumId,uri,object)
  static getVideos(data = Vimeo.data) {
    Vimeo.updateData(data);
    const { accessToken, userId } = Vimeo.data;
    let path;
    if (data.uri) path = uri.includes("videos") ? uri : uri + "/videos";
    else if (data.albumId)
      path = `/users/${userId}/albums/${data.albumId}/videos`;
    else if (data.albumObject) {
      // console.log('uri used: '+album)
      path = data.albumObject.uri + "/videos";
    } else if (data.shareLink)
      path = `/users/${userId}/albums/${this.getAlbumIdFromUri(
        data.shareLink
      )}/videos`;
    else path = "/users/" + userId + "/videos";

    const uri = "https://api.vimeo.com" + path + Vimeo.clientIdStr();
    console.log(uri);
    return new Promise((resolve, reject) => {
      RNFetchBlob.fetch("GET", uri, {
        Authorization: "Bearer " + accessToken
      })
        .then(data => {
          //alert(JSON.stringify(data))
          resolve(JSON.parse(data.data).data);
        })
        .catch(error => {
          //return reject(error);
          reject(error);
        });
    });
    //return promise;
  }
}
