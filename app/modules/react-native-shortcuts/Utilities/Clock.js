export default class Clock {
  //static clocks = [];

  static startCountdown(toDate, func) {
    countdownFunction = () => {
      const now = new Date().getTime();
      const distance = toDate - now;

      const days = Math.floor(distance / (1000 * 60 * 60 * 24));
      const hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        if (func) func(-1); // -1 = timer done
      } else if (func) {
        func({
          days: days,
          hours: hours,
          minutes: minutes,
          seconds: seconds
        });
      }
    };
    countdownFunction();
    const int = setInterval(countdownFunction, 1000);
    //Clock.clocks.push(int);
    return int;
  }
}
