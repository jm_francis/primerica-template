import React, { Component } from "react";
import { View, Text, Animated } from "react-native";
import { QuickStyle, Cheat } from "react-native-shortcuts";

export default class CountdownClock extends Component {
  render() {
    //view style
    const userStyle = Cheat.makeStyleArray(this.props.style);
    const widthStyle = QuickStyle.fillWidthWithSpace(45);
    const width = this.props.width ? this.props.width : widthStyle.width;
    const thisStyle = [
      {
        backgroundColor: "white",
        borderRadius: 10,
        width: width,
        height: width * 0.29,
        alignSelf: "center"
      }
    ];
    const finalStyle = thisStyle.concat(userStyle);

    return (
      <View style={finalStyle}>
        <View
          style={{
            flex: 1,
            justifyContent: "space-around",
            flexDirection: "row"
          }}
        >
          <Number text="days" number="23" />
          <Number text="hours" number="4" />
          <Number text="minutes" number="5" />
        </View>
      </View>
    );
  }
}

class Number extends Component {
  render() {
    //style
    const userStyle = Cheat.makeStyleArray(this.props.style);
    const thisStyle = [{}];
    const finalStyle = thisStyle.concat(userStyle);

    //backgroundStyle
    const userBackgroundStyle = Cheat.makeStyleArray(
      this.props.backgroundStyle
    );
    const thisBackgroundStyle = [
      {
        backgroundColor: "rgb(66, 66, 66)",
        borderRadius: 6
      }
    ];
    const finalBackgroundStyle = thisBackgroundStyle.concat(
      userBackgroundStyle
    );

    //number
    const userNumberStyle = Cheat.makeStyleArray(this.props.numberStyle);
    const thisNumberStyle = [
      QuickStyle.regularText,
      {
        color: "white",
        fontFamily: "AppleSDGothicNeo-Light",
        fontSize: 30,
        margin: 10
      }
    ];
    const finalNumberStyle = thisNumberStyle.concat(userNumberStyle);

    //label style
    const userLabelStyle = Cheat.makeStyleArray(this.props.labelStyle);
    const thisLabelStyle = [
      QuickStyle.regularText,
      {
        color: "black",
        fontSize: 13
      }
    ];
    const finalLabelStyle = thisLabelStyle.concat(userLabelStyle);
    return (
      <View style={finalStyle}>
        <View style={finalBackgroundStyle}>
          <Text style={finalNumberStyle}>{this.props.number}</Text>
        </View>
        <Text style={finalLabelStyle}>{this.props.text}</Text>
      </View>
    );
  }
}
