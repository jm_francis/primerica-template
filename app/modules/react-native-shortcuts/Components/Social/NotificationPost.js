import React, { Component } from "react";
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Image,
  Dimensions
} from "react-native";
import { QuickStyle, Cheat } from "react-native-shortcuts";

export default class NotificationPost extends Component {
  canPress = true;
  handlePress() {
    if (this.canPress) {
      this.props.onPress();
      this.canPress = false;
      setTimeout(
        (() => {
          this.canPress = true;
        }).bind(this),
        2000
      );
    }
  }

  render() {
    //props
    const theme = this.props.theme ? this.props.theme : "light"; // dark / light(default)
    const {
      blur,
      textStyle,
      titleStyle,
      text,
      title,
      image,
      imageTitle,
      darkenImage,
      absoluteChildren
    } = this.props;
    const blurRadius = blur ? 10 : 0;

    // THEME PROPERTIES
    let backgroundColor, textColor;
    switch (theme) {
      case "light":
        backgroundColor = "rgb(232, 232, 232)";
        textColor = "rgb(45, 45, 45)";
        break;
      case "dark":
        backgroundColor = "rgb(59, 58, 58)";
        textColor = "white";
        break;
      case "red":
        backgroundColor = "rgb(223, 70, 70)";
        textColor = "white";
      default:
        break;
    }

    // VIEW STYLE
    const userStyle = Cheat.makeStyleArray(this.props.style);
    const hMargin = 11;
    const padding = 5;
    const thisStyle = [
      QuickStyle.fillWidthWithSpace(hMargin),
      {
        minHeight: 80,
        borderRadius: 10,
        backgroundColor: backgroundColor,
        opacity: blur ? 0.8 : 1,
        marginTop: 14,
        padding: padding,
        overflow: "hidden"
      }
    ];
    const finalStyle = thisStyle.concat(userStyle);
    let viewHeight = Cheat.getValueFromStyle("height", finalStyle);
    if (!viewHeight)
      viewHeight = Cheat.getValueFromStyle("minHeight", finalStyle);
    let viewWidth = Cheat.getValueFromStyle("width", finalStyle);

    // ALL TEXT STYLES
    const themeTextStyle = {
      color: textColor
    };

    // TITLE STYLE
    const userTitleStyle = Cheat.makeStyleArray(this.props.titleStyle);
    const thisTitleStyle = [
      QuickStyle.regularText,
      themeTextStyle,
      {
        fontSize: 16.5,
        margin: 6,
        marginBottom: 4,
        textAlign: "left"
      }
    ];
    const finalTitleStyle = thisTitleStyle.concat(userTitleStyle);

    // TEXT STYLE
    const userTextStyle = Cheat.makeStyleArray(textStyle);
    const thisTextStyle = [
      QuickStyle.thinText,
      themeTextStyle,
      {
        fontSize: 16,
        textAlign: "left",
        margin: 6,
        marginTop: 0
      }
    ];
    const finalTextStyle = thisTextStyle.concat(userTextStyle);

    // IMAGE //TODO combined style
    let imageView = image ? (
      <Image
        source={image}
        style={[
          QuickStyle.fillWidthWithSpace(hMargin),
          {
            height: 150,
            margin: -padding
          }
        ]}
      />
    ) : null;

    // IMAGE TITLE
    const userImageTitleStyle = Cheat.makeStyleArray(
      this.props.imageTitleStyle
    );
    const thisImageTitleStyle = [
      QuickStyle.thinText,
      {
        color: textColor,
        textAlign: "left",
        fontSize: 35,
        position: "absolute",
        margin: 16
      }
    ];
    const finalImageTitleStyle = thisImageTitleStyle.concat(
      userImageTitleStyle
    );
    const imgTitle =
      image && imageTitle ? (
        <Text
          numberOfLines={this.props.scaleText === true ? 1 : 0}
          adjustsFontSizeToFit={this.props.scaleText}
          style={finalImageTitleStyle}
        >
          {imageTitle}
        </Text>
      ) : null;

    const imgDescription =
      text && image ? (
        <Text
          style={[
            QuickStyle.thinText,
            {
              color: textColor,
              position: "absolute",
              top: 55,
              marginLeft: 16
            },
            userTextStyle
          ]}
        >
          {text}
        </Text>
      ) : null;

    const children = absoluteChildren ? (
      <View
        style={{
          position: "absolute",
          height: absoluteChildren > 1 ? absoluteChildren : viewHeight,
          width: viewWidth
        }}
      >
        {this.props.children}
      </View>
    ) : (
      this.props.children
    );

    return (
      <TouchableWithoutFeedback
        onPress={this.handlePress.bind(this)}
        onPressIn={this.props.onPressIn}
        onPressOut={this.props.onPressOut}
      >
        <View blurRadius={blurRadius} style={finalStyle}>
          {title ? <Text style={finalTitleStyle}>{title}</Text> : null}
          {text && !image ? <Text style={finalTextStyle}>{text}</Text> : null}

          {imageView}
          {darkenImage ? (
            <View
              style={{
                backgroundColor: "black",
                position: "absolute",
                width: Dimensions.get("window").width,
                height: 500,
                opacity: 0.25
              }}
            />
          ) : null}
          {imgTitle}
          {imgDescription}

          {children}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
