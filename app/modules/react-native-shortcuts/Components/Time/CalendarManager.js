import React, { Component } from "react";
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Linking,
  Dimensions
} from "react-native";
import { QuickStyle } from "react-native-shortcuts";
// import { LocaleConfig } from 'react-native-calendars';
import HTML from "react-native-render-html";

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

export default class CalendarManager {
  //@themes
  static darkTheme = {
    calendarBackground: "rgb(69, 68, 68)",
    backgroundColor: "rgb(241, 241, 242)",
    arrowColor: "white",
    todayTextColor: "rgb(255, 74, 74)",
    monthTextColor: "white",
    textMonthFontSize: 18,
    textDisabledColor: "rgb(90, 90, 90)",
    dayTextColor: "white",
    textSectionTitleColor: "white"
  };

  //@monthName
  static monthName(index) {
    return monthNames[index - 1];
  }

  //@render
  static RIGHT_MARGIN = 16;
  static renderItem(item, firstItemInDay, handlePress) {
    let description = item.description;
    let url = null;
    if (description.includes("http")) {
      url = description;
      if (url.includes("<a") && url.includes("href")) {
        const i1 = url.indexOf('href="') + 6;
        const str1 = url.substring(i1, url.length);
        url = url.substring(i1, url.indexOf(str1) + str1.indexOf('"'));
      }
    }

    return (
      <TouchableWithoutFeedback
        onPress={() => {
          if (handlePress) {
            handlePress(item, url);
            return;
          }
          if (url) Linking.openURL(url);
        }}
      >
        <View
          style={[
            QuickStyle.boxShadow,
            {
              flex: 1,
              backgroundColor: "rgb(254,254,254)",
              marginRight: CalendarManager.RIGHT_MARGIN,
              marginTop: firstItemInDay ? 29 : 0,
              marginBottom: CalendarManager.RIGHT_MARGIN,
              minHeight: 90
            }
          ]}
        >
          <View
            style={{
              flex: 1,
              paddingVertical: 16,
              paddingHorizontal: 22
            }}
          >
            <Text
              style={[
                QuickStyle.regularText,
                {
                  color: "rgb(94, 105, 115)",
                  fontSize: 16,
                  textAlign: "left",
                  marginBottom: 10
                }
              ]}
            >
              {item.time}
            </Text>
            <Text
              style={[
                QuickStyle.regularText,
                {
                  color: "rgb(94, 105, 115)",
                  fontSize: 17,
                  textAlign: "left",
                  marginBottom: 6
                }
              ]}
            >
              {item.title}
            </Text>
            {!url ? (
              <Text
                style={[
                  QuickStyle.thinText,
                  {
                    color: "rgb(110, 122, 134)",
                    fontSize: 15,
                    textAlign: "left",
                    marginBottom: 6
                  }
                ]}
              >
                {description}
              </Text>
            ) : (
              <HTML
                html={description}
                imagesMaxWidth={Dimensions.get("window").width * 0.9}
              />
            )}
          </View>
          <View
            style={{
              width: "100%",
              height: "100%",
              position: "absolute"
            }}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
  static renderEmptyDate() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center"
        }}
      >
        <View
          style={{
            height: 1,
            marginRight: CalendarManager.RIGHT_MARGIN,
            marginLeft: 5,
            marginTop: 16,
            backgroundColor: "rgba(125, 139, 152, 0.3)"
          }}
        />
      </View>
    );
  }
}
