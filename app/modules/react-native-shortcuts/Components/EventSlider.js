import React, { Component } from "react";
import {
  View,
  ScrollView,
  Dimensions,
  Image,
  Animated,
  Linking,
  Easing,
  TouchableWithoutFeedback
} from "react-native";
import { QuickStyle, Cheat, AnimatedEvent } from "react-native-shortcuts";
import { AnimatedCircularProgress } from "react-native-circular-progress";

class Slide extends Component {
  state = {
    data: null
  };
  componentDidMount() {
    const { data } = this.props;
    Image.prefetch(data.imageSource.uri).then(() => {
      this.setState({ data });
    });
  }
  openLink(link) {
    if (!link) return;
    Linking.openURL(link).catch(err => console.log(err));
  }
  render() {
    const { width, height } = this.props;
    const { data } = this.state;
    if (!data) return <View />;
    const { imageLoadStart, imageLoadEnd } = data;
    const imageSource = data.imageSource;
    return (
      <View>
        <TouchableWithoutFeedback
          onPress={() => {
            this.openLink(data.link);
          }}
        >
          <View
            style={{
              width: width,
              height: height,
              overflow: "hidden"
            }}
          >
            <View
              style={[
                {
                  overflow: "hidden",
                  width,
                  height,
                  backgroundColor: "black"
                }
              ]}
            >
              <Image
                style={{
                  width,
                  height
                }}
                source={imageSource}
                onLoadStart={imageLoadStart}
                onLoadEnd={imageLoadEnd}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

export default class EventSlider extends Component {
  static sliderCount = 0;

  constructor() {
    super();

    this.state = {
      focusDot: 0,
      animX: new Animated.Value(0),
      progressCircleFill: 0
    };

    this.slideID = EventSlider.sliderCount++;
  }

  currentSlide = 0;
  maxSlideLength = undefined;
  delay = 4800;
  width = 0;
  isDragging = false;
  timerIsGoing = false;

  componentDidMount() {
    if (!this.props.pause) this.next();
  }
  firstSlideUpdateCall = true;
  next() {
    //keep trying until more than 1 slide is loaded to avoid a double delay
    if (this.maxSlideLength < 2) {
      setTimeout(
        (() => {
          this.next();
        }).bind(this),
        100
      );
      return;
    }

    const { onSlideUpdate, slides } = this.props;
    if (
      this.firstSlideUpdateCall &&
      slides &&
      slides.length > 0 &&
      onSlideUpdate
    ) {
      this.firstSlideUpdateCall = false;
      onSlideUpdate(0, slides[0]);
    }

    //know which slide to go to next
    let nextSlide = this.currentSlide + 1;
    if (nextSlide >= this.maxSlideLength) nextSlide = 0;

    this.timerIsGoing = true;
    // this.refs.progressCircle.performLinearAnimation(100,this.delay);
    this.refs.progressCircle.animate(100, this.delay, Easing.quad);
    this.timeout = setTimeout(
      (() => {
        if (this.isDragging || !this.scrollViewRef) {
          this.timerIsGoing = false;
          return;
        }
        if (!this.props.pause) {
          this.scrollViewRef.scrollTo({
            x: Dimensions.get("window").width * nextSlide,
            y: 0,
            animated: true
          });
        }

        this.refs.progressCircle.animate(0, 0, Easing.quad);
        this.currentSlide = nextSlide;

        const { onSlideUpdate } = this.props;
        if (onSlideUpdate)
          onSlideUpdate(
            this.currentSlide,
            this.props.slides[this.currentSlide]
          );

        this.next();
      }).bind(this),
      this.delay
    );
  }

  handleScrollMomentumEnd(event) {
    const xOffset = event.nativeEvent.contentOffset.x;
    this.currentSlide = xOffset / this.width;

    const { onSlideUpdate } = this.props;
    if (onSlideUpdate)
      onSlideUpdate(this.currentSlide, this.props.slides[this.currentSlide]);

    this.setFocusDot(this.currentSlide);
    this.isDragging = false;
    if (this.timerIsGoing == false) this.next();
  }
  handleDragBegins() {
    this.isDragging = true;
    // this.refs.progressCircle.performLinearAnimation(0,200);
    this.refs.progressCircle.animate(0, 200, Easing.quad);
    this.timerIsGoing = false;
    clearTimeout(this.timeout);
  }

  setFocusDot(index) {
    this.setState({
      focusDot: index
    });
  }

  //@makeTextComponent
  makeTextComponent(textProps) {
    const animations = this.props.animations ? this.props.animations : {};
    const { animX } = this.state;
    const index = textProps.index;
    const zero = index * this.width;
    const inputRange = [zero - this.width, zero, zero + this.width];
    const titleX = animX.interpolate({
      inputRange: inputRange,
      outputRange: [this.width * 1.15, 0, -this.width / 6],
      extrapolate: "clamp",
      useNativeDriver: true
    });
    const descriptionX = animX.interpolate({
      inputRange: inputRange,
      outputRange: [this.width * 1.5, 0, +this.width / 6],
      extrapolate: "clamp",
      useNativeDriver: true
    });
    const opacityAnim = animX.interpolate({
      inputRange: inputRange,
      outputRange: [0.7, 1, 0],
      extrapolate: "clamp",
      useNativeDriver: true
    });
    //title
    const userTitleStyle = Cheat.makeStyleArray(textProps.titleStyle);
    const thisTitleStyle = [
      QuickStyle.regularText,
      {
        color: "rgb(233, 231, 231)",
        fontSize: 18,
        textAlign: "left",
        transform: [{ translateX: titleX }],
        opacity: opacityAnim
      }
    ];
    const finalTitleStyle = thisTitleStyle.concat(userTitleStyle);
    //description
    const userDescriptionStyle = Cheat.makeStyleArray(this.descriptionStyle);
    const thisDescriptionStyle = [
      QuickStyle.regularText,
      {
        color: "rgb(233, 231, 231)",
        fontSize: 13.3,
        textAlign: "left",
        marginTop: 1,
        transform: [{ translateX: descriptionX }],
        opacity: opacityAnim
      }
    ];
    const finalDescriptionStyle = thisDescriptionStyle.concat(
      userDescriptionStyle
    );

    let transY = this.height - 50,
      transX = this.showDots == false ? 12 : 20;
    transY = this.showDots == false ? transY - 5 : transY - 15;
    if (this.props.showTextBackground) {
      transY += 4;
      transX -= 1;
    }
    const component = (
      <Animated.View
        key={`eventslider-${this.sliderID}-textComponent-${index}`}
        pointerEvents={"none"}
        style={[
          QuickStyle.fillWidthWithSpace(20),
          {
            position: "absolute",
            height: 50,
            transform: [{ translateY: transY }, { translateX: transX }],
            opacity: animations.opacity
          }
        ]}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center"
          }}
        >
          <Animated.Text style={finalTitleStyle}>
            {textProps.title}
          </Animated.Text>
          <Animated.Text style={finalDescriptionStyle}>
            {textProps.description}
          </Animated.Text>
        </View>
      </Animated.View>
    );
    return component;
  }

  render() {
    //delay
    if (this.props.delay) this.delay = this.props.delay;

    //style
    const userStyle = Cheat.makeStyleArray(this.props.style);
    const sideMargin = 12;
    let height = Cheat.getValueFromStyle("height", userStyle);
    height = height ? height : 200;
    const thisStyle = [
      QuickStyle.fillWidthWithSpace(sideMargin),
      {
        maxHeight: height,
        alignSelf: "center",
        overflow: "hidden"
      }
    ];
    let finalStyle = thisStyle.concat(userStyle);

    let width = Cheat.getValueFromStyle("width", finalStyle);
    width = width ? width : Dimensions.get("window").width - sideMargin * 2;
    this.width = width;
    this.height = height;

    //slides
    const userSlides = this.props.slides;
    //text components
    const textComponents = [];

    const slides = [];
    for (s in userSlides) {
      const data = userSlides[s];
      const textComponent =
        data.title || data.description
          ? this.makeTextComponent({
              index: s,
              title: data.title,
              description: data.description
            })
          : null;
      if (textComponent) textComponents.push(textComponent);
      slides.push(<Slide data={data} width={width} height={height} />);
    }
    this.maxSlideLength = slides.length;

    // props animations
    const propsOpacityAnim = this.props.animations
      ? this.props.animations.opacity
      : undefined;
    const propsTextBackgroundOpacityAnim = this.props.animations
      ? this.props.animations.textBackgroundOpacity
      : undefined;

    // text background
    const { showTextBackground } = this.props;
    const doNotShowPlease = userSlides.length === 1 && !userSlides[0].title;
    const textBackground = showTextBackground && !doNotShowPlease ? (
      <Animated.View
        key={`eventslider${this.sliderID}-textbackground`}
        style={{
          opacity: propsTextBackgroundOpacityAnim
        }}
      >
        <Animated.View
          pointerEvents={"none"}
          style={{
            height: 52,
            marginTop: -52,
            width: this.width,
            backgroundColor: "rgba(0, 0, 0, 0.5)",
            opacity: propsOpacityAnim
          }}
        />
      </Animated.View>
    ) : null;

    // dots
    this.showDots = showTextBackground ? false : this.props.showDots;
    const dots =
      this.showDots == false ? null : (
        <EventSliderDots
          key={`eventslider-dots-${this.sliderID}`}
          focusIndex={this.state.focusDot}
          dotCount={3}
          style={{
            marginTop: height - 12,
            opacity: propsOpacityAnim
          }}
        />
      );

    // progress circle
    const { showProgressCircle } = this.props;
    const { progressCircleFill } = this.state;
    const progressCircle = showProgressCircle ? (
      <Animated.View
        key={`eventslider${this.sliderID}-progressCircle`}
        style={{
          opacity: propsOpacityAnim,
          position: "absolute",
          transform: [
            { translateX: this.width - 36 },
            { translateY: this.height - 36 }
          ]
        }}
      >
        <AnimatedCircularProgress
          ref={"progressCircle"}
          pointerEvents={"none"}
          fill={progressCircleFill}
          size={23}
          width={4.3}
          rotation={360}
          tintColor={"rgb(223, 223, 223)"}
          backgroundColor={"transparent"}
        />
      </Animated.View>
    ) : null;

    //animX
    const { animX } = this.state;

    //unpause if needed
    if (!this.timerIsGoing && !this.props.pause) this.next();

    return (
      <>
        <ScrollView
          ref={view => (this.scrollViewRef = view)}
          scrollEventThrottle={16}
          onScroll={AnimatedEvent.scrollXOffset(animX, null, true)}
          onMomentumScrollEnd={this.handleScrollMomentumEnd.bind(this)}
          onScrollBeginDrag={this.handleDragBegins.bind(this)}
          showsHorizontalScrollIndicator={false}
          horizontal
          pagingEnabled
          style={finalStyle}
        >
          {slides.length < 1 ? null : slides}
        </ScrollView>
        {dots}
        {textBackground}
        {textComponents}
        {progressCircle}
      </>
    );
  }
}

export class EventSliderDots extends Component {
  createDot(on, id) {
    const dimension = 7.75;
    return (
      <View
        key={`eventslider-adotinspace-${id}`}
        style={{
          width: dimension,
          height: dimension,
          borderRadius: dimension / 2,
          opacity: 0.7,
          backgroundColor: on ? "rgb(235, 232, 232)" : "rgb(110, 110, 110)",
          marginHorizontal: 5
        }}
      />
    );
  }

  render() {
    const dots = [];
    const dotCount = this.props.dotCount ? this.props.dotCount : 3;
    const focusIndex = this.props.focusIndex ? this.props.focusIndex : 0;
    for (x = 0; x < dotCount; x++) {
      const focus = x == focusIndex;
      dots.push(this.createDot(focus, x));
    }

    const userStyle = Cheat.makeStyleArray(this.props.style);
    const thisStyle = [
      {
        alignSelf: "center",
        position: "absolute"
      }
    ];
    const finalStyle = thisStyle.concat(userStyle);

    return (
      <Animated.View style={finalStyle}>
        <View
          style={{
            flex: 1,
            justifyContent: "space-around",
            alignSelf: "center",
            flexDirection: "row"
          }}
        >
          {dots}
        </View>
      </Animated.View>
    );
  }
}
