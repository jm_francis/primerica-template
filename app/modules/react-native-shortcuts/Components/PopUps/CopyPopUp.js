import React, { Component } from "react";
import { View, Text, Clipboard, TouchableOpacity } from "react-native";
import { QuickStyle, Cheat, PopUp } from "react-native-shortcuts";

export default class CopyPopUp extends Component {
  constructor() {
    super();

    this.state = {
      buttonText: "Copy"
    };
  }

  handlePress() {
    Clipboard.setString(this.props.link);
    this.setState({
      buttonText: "Copied!"
    });
    this.dismiss();
  }

  present() {
    this.popUpRef.present();
  }
  dismiss() {
    this.popUpRef.dismiss(() => {
      this.setState({
        buttonText: "Copy"
      });
    });
  }

  render() {
    const { link = "http://mylink.com" } = this.props;

    const userTextStyle = Cheat.makeStyleArray(this.props.textStyle);
    const thisTextStyle = [
      {
        fontSize: 22,
        color: "rgb(148, 148, 148)",
        textAlign: "center",
        margin: 20,
        fontFamily: "HelveticaNeue-Thin"
      }
    ];
    const finalTextStyle = thisTextStyle.concat(userTextStyle);

    return (
      <PopUp ref={ref => (this.popUpRef = ref)} {...this.props}>
        <View
          style={{
            flex: 1,
            justifyContent: "space-around",
            alignItems: "center"
          }}
        >
          <Text style={finalTextStyle}>{link}</Text>

          <TouchableOpacity onPress={this.handlePress.bind(this)}>
            <View
              style={[
                QuickStyle.fillWidthWithSpace(PopUp.MARGIN + 45),
                {
                  backgroundColor: "rgb(52, 240, 116)",
                  borderRadius: 10,
                  height: 45
                }
              ]}
            >
              <View
                style={{
                  flex: 1,
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    color: "white",
                    fontSize: 20,
                    fontFamily: "Helvetica",
                    textAlign: "center"
                  }}
                >
                  {this.state.buttonText}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </PopUp>
    );
  }
}
