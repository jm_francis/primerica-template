import React, { Component } from "react";
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  Animated,
  Image,
  Platform
} from "react-native";
import { QuickStyle, Cheat } from "react-native-shortcuts";
import { BlurView as _BlurView } from "react-native-blur";

class BlurView extends Component {
  render() {
    return Platform.OS === "ios" ? (
      <_BlurView {...this.props}>{this.props.children}</_BlurView>
    ) : (
      <View
        {...this.props}
        style={{
          ...this.props.style,
          backgroundColor: "rgba(0,0,0,0.8)"
        }}
      >
        {this.props.children}
      </View>
    );
  }
}

export default class PopUp extends Component {
  static MARGIN = 33;

  constructor() {
    super();

    this.state = {
      visible: false,
      opacityAnim: new Animated.Value(0)
    };
  }

  animateDuration = 350;
  dismiss(finish) {
    Animated.timing(this.state.opacityAnim, {
      toValue: 0,
      duration: this.animateDuration,
      delay: 0,
      useNativeDriver: true
    }).start(
      (() => {
        this.setState({
          visible: false
        });
        if (finish) {
          try {
            finish();
          } catch (error) {}
        }
      }).bind(this)
    );
  }

  present() {
    this.setState({
      visible: true
    });
    Animated.timing(this.state.opacityAnim, {
      toValue: 1,
      duration: this.animateDuration,
      delay: 0,
      useNativeDriver: true
    }).start();
  }

  render() {
    const userStyle = Cheat.makeStyleArray(this.props.style);
    const thisStyle = [
      QuickStyle.fillWidthWithSpace(PopUp.MARGIN),
      {
        backgroundColor: "white",
        height: 200,
        borderRadius: 10
      }
    ];
    const finalStyle = thisStyle.concat(userStyle);

    return (
      <Modal visible={this.state.visible} transparent={true}>
        <Animated.View
          style={{
            flex: 1,
            opacity: this.state.opacityAnim
          }}
        >
          <BlurView
            blurAmount={10}
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableWithoutFeedback onPress={this.dismiss.bind(this)}>
              <View
                style={[
                  QuickStyle.fillScreen,
                  {
                    position: "absolute"
                  }
                ]}
              />
            </TouchableWithoutFeedback>

            <View style={finalStyle}>{this.props.children}</View>
          </BlurView>
        </Animated.View>
      </Modal>
    );
  }
}
