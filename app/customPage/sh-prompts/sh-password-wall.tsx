import { Input } from "@ui-kitten/components";
import Backend, { MediaPageSchema } from "backend/";
import { Buttoon, Kitten, Txt } from "components/";
import React from "react";
import { View } from "react-native";
import { Modalize } from "react-native-modalize";
import { C, DEVICE_HEIGHT, DEVICE_WIDTH, spacing } from "utilities/";

interface P {
  navigation: any;
  page: MediaPageSchema;
}
export const SS_PasswordWall = React.forwardRef((props: P, ref: any) => {
  const { navigation, page } = props;
  // const [passwordForPage, setPasswordForPage] = React.useState<string>(null);

  // React.useEffect(function getPasswordForPage() {
  //   setPassBackend.localHandler
  //       .getSavedPasswordForPage(page.name)
  // }, [])

  React.useEffect(
    function openIfNeeded() {
      ref.current && ref.current.close();
      if (!page) return;
      Backend.localHandler
        .getSavedPasswordForPage(page.name)
        .then((savedPassword) => {
          if (
            page.mediaItem.team !== true &&
            page.password &&
            `${page.password}`.length > 1 &&
            savedPassword !== `${page.password}`
          )
            ref.current.open();
          else ref.current && ref.current.close();
        });
    },
    [page]
  );

  return (
    <Modalize
      ref={ref}
      scrollViewProps={{
        keyboardShouldPersistTaps: "always",
      }}
      snapPoint={DEVICE_HEIGHT * 0.85}
      modalHeight={DEVICE_HEIGHT * 0.85}
      modalStyle={{ backgroundColor: C.surface01 }}
      alwaysOpen={DEVICE_HEIGHT * 0.85}
      withHandle={false}
    >
      {page && (
        <$_PasswordInput
          {...props}
          onSuccess={() => !!ref.current && ref.current.close()}
          onCancel={() => {
            navigation.pop();
            return !!ref.current && ref.current.close();
          }}
        />
      )}
    </Modalize>
  );
});

interface d$PwInput extends P {
  page: MediaPageSchema;
  onSuccess(): void;
  onCancel(): void;
}

const $_PasswordInput = (props: d$PwInput) => {
  const { page, onSuccess, onCancel } = props;
  const pageName = page.name;
  const [entryPassword, setEntryPassword] = React.useState<string | number>("");
  const refInput = React.useRef<Input>();

  // const page = Backend.firebasePages.getPageWithName(pageName);

  async function passwordInputChange(value: string) {
    setEntryPassword(value);
    if (`${page.password}` === `${value}`) {
      await Backend.localHandler.savePasswordForPage(pageName, `${value}`);
      onSuccess();
    }
  }

  return (
    <View
      style={{
        width: DEVICE_WIDTH,
        justifyContent: "space-between",
        alignItems: "center",
        padding: spacing(5),
      }}
    >
      <Txt
        category={"h6"}
        fontFam="bold"
        onPress={onCancel}
        style={{ textAlign: "center" }}
      >
        Enter password for page
      </Txt>
      <Kitten.Input
        {...props}
        autoFocus={true}
        style={{
          marginVertical: "7%",
        }}
        ref={refInput}
        status="primary"
        placeholder="Password"
        value={entryPassword}
        onChangeText={passwordInputChange}
      />
      <Buttoon onPress={onCancel} appearance="ghost">
        Cancel
      </Buttoon>
    </View>
  );
};
