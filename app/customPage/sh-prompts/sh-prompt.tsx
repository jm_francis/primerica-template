import { Input } from "@ui-kitten/components";
import { Buttoon, Kitten, Txt } from "components/";
import React from "react";
import {
  NativeSyntheticEvent,
  TextInputSelectionChangeEventData,
  View,
} from "react-native";
import { Modalize } from "react-native-modalize";
import {
  C,
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  moderateScale,
  spacing,
} from "utilities/";

// interface PromptProps extends IPSCR {
//   options?: dOptions;
// }
export const SS_Prompt = React.forwardRef((props, ref: any) => {
  const [_options, setOptions] = React.useState<dOptions | null>(null);

  const modalRef = React.useRef(null);

  React.useEffect(function closeInitially() {
    ref.current.close();
  }, []);

  React.useImperativeHandle(ref, () => ({
    openWithOptions: function (options: dOptions) {
      modalRef.current.open();
      setOptions(options);
    },
    close: () => {
      modalRef.current.close();
      setOptions(null);
    },
  }));

  return (
    <Modalize
      ref={modalRef}
      scrollViewProps={{
        keyboardShouldPersistTaps: "always",
      }}
      snapPoint={DEVICE_HEIGHT * 0.85}
      modalHeight={DEVICE_HEIGHT * 0.85}
      modalStyle={{ backgroundColor: C.surface01 }}
      alwaysOpen={DEVICE_HEIGHT * 0.85}
      withHandle={false}
    >
      {_options ? (
        <$_Input
          options={_options}
          onSubmit={(text) => {
            _options?.onSubmit(text);
            ref.current?.close();
          }}
          onCancel={() => {
            _options.onCancel();
            return !!ref.current && ref.current.close();
          }}
        />
      ) : null}
    </Modalize>
  );
});

export interface dOptions {
  title: string;
  subtitle?: string;
  thumbnail?: string;
  defaultValue?: string;
  onSubmit(text: string): void;
  onCancel(): void;
}

const $_Input = (props: {
  options: dOptions;
  onCancel: () => void;
  onSubmit: (value: string) => void;
}) => {
  const { options, onCancel, onSubmit } = props;
  const { title, subtitle, defaultValue = "" } = options;
  const [entry, setEntry] = React.useState<string>(defaultValue);
  const refInput = React.useRef<Input>();
  /**
   * Position of cusor in the input field
   */
  const [caret, setCaret] = React.useState({
    start: 0,
    end: 0,
  });
  function handleSelect(
    e: NativeSyntheticEvent<TextInputSelectionChangeEventData>
  ) {
    setCaret({
      start: e.nativeEvent.selection.start,
      end: e.nativeEvent.selection.end,
    });
  }

  async function inputChange(value: string) {
    setEntry(value);
  }

  React.useEffect(() => {
    defaultValue && setEntry(defaultValue);
    defaultValue &&
      setCaret({ start: defaultValue.length, end: defaultValue.length });
  }, []);

  return (
    <View
      style={{
        width: DEVICE_WIDTH,
        justifyContent: "space-between",
        alignItems: "center",
        padding: spacing(5),
      }}
    >
      <Txt.H6
        category={"h6"}
        onPress={onCancel}
        numberOfLines={2}
        style={{ textAlign: "center", marginBottom: spacing(1) }}
      >
        {title?.length > 0 ? title : entry}
      </Txt.H6>
      {subtitle?.length > 0 ? (
        <Txt.P2
          onPress={onCancel}
          style={{ textAlign: "center", fontWeight: "600" }}
        >
          {subtitle}
        </Txt.P2>
      ) : null}
      <Kitten.Input
        {...props}
        autoFocus={true}
        autoCapitalize={"none"}
        autoCorrect={false}
        style={{
          marginVertical: "7%",
        }}
        ref={refInput}
        status="primary"
        placeholder="Type Here"
        value={entry}
        onChangeText={inputChange}
        multiline={true}
        onSelectionChange={handleSelect}
      />
      <$_SpecialCharacters
        onSelect={(char) => {
          setEntry(
            entry.substring(0, caret.start) +
              char +
              entry.substring(caret.end, entry.length)
          );
        }}
      />
      <Buttoon
        onPress={() => {
          console.log("done button press");
          onSubmit(entry);
        }}
        icon={{ name: "check" }}
        style={{
          width: moderateScale(150),
        }}
      >
        Done
      </Buttoon>
      <Buttoon onPress={onCancel} appearance="ghost" status="danger">
        Cancel
      </Buttoon>
      <View
        style={{
          width: "100%",
          height: spacing(8),
        }}
      />
    </View>
  );
};

function $_SpecialCharacters(props: { onSelect: (char: string) => void }) {
  const { onSelect } = props;
  return (
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      <Kitten.Button
        size="small"
        appearance="outline"
        status="basic"
        style={{ alignSelf: "center", marginBottom: spacing(5) }}
        onPress={() => onSelect("🌀")}
      >
        {"🌀 Recipient's Name"}
      </Kitten.Button>
    </View>
  );
}
