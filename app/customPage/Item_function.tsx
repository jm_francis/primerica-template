import Backend from "backend/";
import { UserSchema, dMediaPageItem } from "backend/";
import FileNetwork from "backend/apis/FileNetwork";
import K, { Config, Fonts } from "constant-deprecated/";
import { BACK_BUTTON, LOCK_ICON } from "constant-deprecated/Images";
import React from "react";
import {
  Dimensions,
  Platform,
  TouchableOpacity,
  View,
  Image,
  Text,
  TouchableWithoutFeedback,
} from "react-native";
import { moderateScale } from "utilities/";
import AudioBlock from "./items/audios/AudioBlock";
import DropboxFolder from "./items/audios/DropboxFolder";
import { customPageHandler } from "./handlers/CustomPageHandler";
import Paragraph from "./items/text/Paragraph";
import Title from "./items/text/Title";
import VideoBlock from "./items/videos/VideoBlock";
import YouTubeVideo from "./items/youtube/YouTubeVideo";

const androidCompleteGreen = "rgb(66, 245, 138)";

/**
 * The state of an item when in share mode
 */
export interface dShareSelect {
  mediaItem: dMediaPageItem;
  /**
   * Whether or not the item is selected for sharing
   */
  selected: boolean;
  /**
   * The source is predetermined for you to use (likely came from item.url or item.media)
   */
  source: string;
  // @deprecated Just use mediaItem.title
  title: string;
}

interface dProps {
  item: dMediaPageItem;
  pageName: string;
  onPress: (data: any) => void;
  coverPress?: (data: any) => void;
  coverStyle?: any;
  /**
   * When an item is selected/tapped in share mode
   */
  onShareSelect: (data: dShareSelect) => void;
  /**
   * When an item gets its latest measurements, this function is called
   */
  onMeasure: (mediaItem: dMediaPageItem, measurements: any) => void;
  /**
   * When the piece of content is considered complete by the system (example: user finishes watching the video)
   */
  // onAutoComplete: () => void;
}

export default function Item(props: dProps) {
  // props
  const {
    item,
    pageName,
    onPress,
    coverPress,
    coverStyle,
    onShareSelect,
    onMeasure,
  } = props;
  const { media, title, paragraph, url, topage, id } = item;
  const preset =
    item.preset === "button" ? "button" : topage || url ? "button" : "none";

  // state
  const [_account, setAccount] = React.useState<UserSchema>(null);
  const [_activateShare, setActivateShare] = React.useState(false);
  //   const [_rendered, setRendered] = React.useState(false);
  const [_neededTitle, setNeededTitle] = React.useState(null);
  const [_completed, setCompleted] = React.useState(false);
  const [_progress, setProgress] = React.useState<number>(
    Backend.levelsHandler.calculateProgressForLevel(topage)
  );
  const [_locked, setLocked] = React.useState(false);
  const [_isSharedSelected, setShareSelected] = React.useState(false);
  const [_deepShare, setDeepShare] = React.useState(false); // TODO: why is this in existence?
  const [_renderOptions, setRenderOptions] = React.useState(null);
  const [_failedToLoad, setFailedToLoad] = React.useState(false);

  // refs
  const containerRef = React.useRef(null);
  const shareSource = React.useRef(null);
  const isVideo = React.useRef(false);
  const isDropboxFolder = React.useRef(false);

  // mount
  React.useEffect(function () {
    if (media && media.includes("drive.google.com")) {
      FileNetwork.getVideoMetadata(media).then((videoMetadata) => {
        if (!videoMetadata.json.error) {
          setRenderOptions({
            googleDriveFileType: videoMetadata.fileType,
            googleDriveThumbnail: videoMetadata.thumbnail,
          });
        } else {
          setFailedToLoad(true);
        }
      });
    } else if (
      media &&
      typeof media === "string" &&
      media.includes("cloudinary.com") &&
      item.thumbnail
    ) {
      setRenderOptions({
        googleDriveFileType: "mp4/video",
        googleDriveThumbnail: item.thumbnail,
      });
    }

    customPageHandler.subscribe(update);
    Backend.firestoreHandler.account((account) => {
      setAccount(account);
      setLocked(isLocked());
      /**
       * if this goes to a page that is a level
       */
      if (Backend.firebasePages.isLevelPage(topage))
        setProgress(Backend.levelsHandler.calculateProgressForLevel(topage));
      /**
       * if this item is an item that exists on a level page
       */
      const thisLevel =
        _account && _account.levels && _account.levels[pageName]
          ? _account.levels[pageName][id]
          : {};
      if (!thisLevel || !thisLevel.complete) setCompleted(false);
      else if (thisLevel.complete) setCompleted(true);
    });
  }, []);

  // functions
  function handlePress() {
    if (coverPress) {
      coverPress(item);
      return;
    }

    if (onPress) onPress(item);

    if (_activateShare === true && !item.topage) {
      const title = _neededTitle ? _neededTitle : item.title;
      onShareSelect({
        mediaItem: item,
        selected: !_isSharedSelected,
        source: shareSource.current,
        title,
      });
    }
  }

  function onLayout(e: any) {
    if (!onMeasure) return;
    containerRef.current.measure((x, y, width, height, pageX, pageY) => {
      onMeasure(item, { x, y, width, height, pageX, pageY });
    });
  }

  function isLocked() {
    const account = Backend.firestoreHandler._account;
    let usesLevelsFeature = false; // (if this specific page is part of the levels feature)
    const keywords = K.Config.enablingKeywordsForProgressFeature;
    if (topage)
      for (let k in keywords)
        if (topage.toLowerCase().includes(keywords[k].toLowerCase()))
          usesLevelsFeature = true;
    if (usesLevelsFeature) {
      const progress = Backend.levelsHandler.progressForPreviousLevel(topage);
      if (progress < 1 && !account.admin && !account.allLevelsCompleted) {
        return true;
      }
    }
    return false;
  }

  function anyPress() {
    onPress && onPress(item);
  }

  async function reRender() {
    // await this.componentDidMount();
    // this.setState({ rendered: false }, () => {
    //   this.setState({ rendered: true }, () => {
    //     return;
    //   });
    // });
  }

  function onAutoComplete() {
    Backend.levelsHandler.updateLevelItem(pageName, item.id, true);
  }

  function renderContent() {
    // const { googleDriveFileType, googleDriveThumbnail } = _renderOptions;
    const googleDriveFileType =
      _renderOptions && _renderOptions.googleDriveFileType;
    const googleDriveThumbnail =
      _renderOptions && _renderOptions.googleDriveThumbnail;
    let video, youtube, audio;

    let content = null;

    if (media) {
      if (
        media.includes("vimeo.com") ||
        media.includes("cloudinary.com") ||
        (media.includes("drive.google.com") &&
          googleDriveFileType &&
          googleDriveFileType.includes("video"))
      )
        video = media;
      else if (
        media.includes("dropbox.com") ||
        (media.includes("drive.google.com") &&
          googleDriveFileType &&
          googleDriveFileType.includes("audio"))
      )
        audio = media;
      else if (media.includes("youtu.be") || media.includes("youtube.com"))
        youtube = media;
    }

    if (youtube) {
      shareSource.current = youtube;
      content = (
        <YouTubeVideo
          source={youtube}
          onPress={anyPress}
          onAutoComplete={() => onAutoComplete && onAutoComplete()}
        />
      );
    }

    if (video) {
      shareSource.current = video;
      isVideo.current = true;
      content = (
        <VideoBlock
          nameOfVideo={setTitleIfNeeded}
          source={video}
          onPress={anyPress}
          onAutoComplete={() => onAutoComplete && onAutoComplete()}
          thumbnail={googleDriveThumbnail}
          reRender={reRender}
        />
      );
    }

    if (audio) {
      shareSource.current = audio;
      let dbstr = audio.substring(0, audio.indexOf("?"));
      dbstr = dbstr.substring(dbstr.length - 5, dbstr.length);
      if (dbstr.includes(".") || audio.includes("drive.google.com")) {
        content = (
          <AudioBlock
            source={audio}
            title={title}
            onPress={anyPress}
            complete={_completed}
          />
        );
      } else {
        isDropboxFolder.current = true;
        content = (
          <DropboxFolder
            source={audio}
            onPress={anyPress}
            onAutoComplete={onAutoComplete}
          />
        );
      }
    }

    if (url && ("" + url).length > 1) {
      shareSource.current = url;
    }

    return content;
  }

  function update(data: any) {
    // const { shareStates } = data;
    const isShareSelected = customPageHandler.shareStateOf(
      pageName,
      shareSource.current
    );
    setActivateShare(data.activateShare);
    setShareSelected(isShareSelected);
    // this.setState({ activateShare, shareSelected });
  }
  function setTitleIfNeeded(title: string) {
    if (item.title && ("" + item.title).length > 1) {
    } else setNeededTitle(title);
  }

  // #region stuff
  if (_failedToLoad) return null;

  const content = renderContent();

  const hideTitle = media
    ? (media.includes("dropbox.com") && !isDropboxFolder.current) ||
      (media.includes("drive.google.com") && !isVideo.current)
    : false;

  const androidComplete = _completed === true && Platform.OS === "android";

  // should use progress feature?
  let enableProgress = false;
  const keywords = Config.enablingKeywordsForProgressFeature;
  for (let k in keywords) {
    const kw = keywords[k];
    if (topage && topage.toLowerCase().includes(kw)) enableProgress = true;
  }

  const activateShare = topage || (!content && !url) ? false : _activateShare;

  const presetStyle: any =
    preset === "button"
      ? {
          ...Styles.button,
          ...(enableProgress ? Styles.buttonWithProgress : {}),
        }
      : {};

  const activateShareStyle = _activateShare ? Styles.activateShare : {};
  const activateShareSelected =
    _activateShare && _isSharedSelected ? Styles.activateShareSelected : {};
  const shareStyles =
    _deepShare === false
      ? {
          ...activateShareStyle,
          ...activateShareSelected,
        }
      : {};

  const justTitleForPad = !content && preset !== "button" && Platform.isPad;
  const titleContainerStyle = justTitleForPad
    ? {
        width: screenWidth * 0.9,
        alignItems: "center",
      }
    : {};
  let titleStyle = justTitleForPad
    ? {
        textAlign: "center",
        transform: [{ scale: 1.12 }],
      }
    : {};
  titleStyle = {
    ...titleStyle,
    ...(enableProgress
      ? {
          maxWidth: buttonWidth * 0.65,
        }
      : {}),
    ...(androidComplete
      ? {
          color: androidCompleteGreen,
        }
      : {}),
  };

  const completeStyle =
    _completed === true
      ? {
          shadowOffset: { width: 0, height: 1 },
          shadowRadius: 3.7,
          shadowOpacity: 0.92,
          shadowColor: "#8aff9d",
        }
      : {};

  const arrowStyle = {
    ...Styles.arrow,
    ...(androidComplete
      ? {
          tintColor: androidCompleteGreen,
        }
      : {}),
  };

  const progressStyle = {
    position: "absolute",
    top: 0,
    left: 0,
    height: presetStyle.height,
    width: screenWidth * _progress,
    backgroundColor: "#16e07b",
  };

  const progressPercentTextStyle = {
    ...Styles.progressPercent,
    ...(_progress >= 0.9 ? { fontFamily: Fonts.bold, color: "white" } : {}),
  };

  const progressPercent = `${Math.round(_progress * 100)}%`;

  const finalTitle = _neededTitle ? _neededTitle : title;
  // #endregion stuff

  return (
    <View
      style={{ ...completeStyle }}
      ref={(ref) => (containerRef.current = ref)}
      onLayout={onLayout}
    >
      <TouchableOpacity
        activeOpacity={preset === "button" && !activateShare ? 0.2 : 1}
        onPress={handlePress}
      >
        <View
          style={{
            overflow: "hidden",
            marginTop: 16,
            justifyContent: "center",
            alignSelf: "center",
            ...presetStyle,
            ...shareStyles,
            ...titleContainerStyle,
            ...(enableProgress
              ? {
                  alignItems: "flex-start",
                }
              : {}),
          }}
        >
          {enableProgress ? <View style={progressStyle} /> : null}
          {enableProgress && !_locked ? (
            <Text style={progressPercentTextStyle}>{progressPercent}</Text>
          ) : null}
          <TouchableWithoutFeedback onPress={handlePress}>
            <View
              style={{
                ...(_deepShare === true
                  ? {
                      ...activateShareStyle,
                      ...activateShareSelected,
                    }
                  : {}),
                flexDirection: "row",
              }}
            >
              {(title || _neededTitle) && hideTitle === false ? (
                <Title button={preset === "button"} style={titleStyle}>
                  {finalTitle}
                </Title>
              ) : null}
              {paragraph ? <Paragraph>{paragraph}</Paragraph> : null}
              {preset === "button" &&
              !(enableProgress && finalTitle.length > 9) ? (
                <Image source={BACK_BUTTON} style={arrowStyle} />
              ) : null}
            </View>
          </TouchableWithoutFeedback>

          <View style={{}}>{content}</View>
          {(activateShare && _deepShare === false) || preset === "button" ? (
            <View
              style={{
                position: "absolute",
                backgroundColor:
                  Platform.OS === "ios"
                    ? "transparent"
                    : "rgba(43, 246, 170, 0.001)",
                width: "100%",
                height: "100%",
              }}
            />
          ) : null}

          {_locked ? <LockedCover /> : null}
          {coverPress ? (
            <View
              style={{
                position: "absolute",
                width: "100%",
                height: "100%",
                ...coverStyle,
              }}
            />
          ) : null}
        </View>
      </TouchableOpacity>
    </View>
  ); //: null;
}

function LockedCover(props) {
  return (
    <View style={Styles.lockedCover}>
      <View style={Styles.lockedCoverInner}>
        <Image style={Styles.lockedIcon} source={LOCK_ICON} />
      </View>
    </View>
  );
}

const screenWidth = Dimensions.get("window").width;
const buttonWidth = Platform.isPad ? screenWidth * 0.6 : screenWidth * 0.94;
const buttonHeight = moderateScale(63, 0.25);

export const Styles = {
  button: {
    marginTop: Platform.isPad ? 30 : 14,
    borderRadius: 7,
    backgroundColor: "rgb(0, 106, 231)",
    alignItems: "center",
    paddingHorizontal: Platform.isPad ? 36 : 27,
    paddingVertical: 5,
    height: buttonHeight,
    width: buttonWidth,
  },
  buttonWithProgress: {
    // background of unfilled space
    // backgroundColor: '#709e9c'
    backgroundColor: "rgb(150,150,150)",
  },
  activateShare: {
    borderColor: "rgba(205, 205, 205, 0.6)",
    borderWidth: 1.88,
    borderRadius: 12,
  },
  activateShareSelected: {
    borderColor: "#40ff93",
    borderWidth: 3.8,
  },
  arrow: {
    width: 32,
    height: 32,
    tintColor: "white",
    transform: [{ rotate: "180deg" }],
    alignSelf: "center",
  },
  progressPercent: {
    position: "absolute",
    right: 20,
    color: "rgba(240,240,240,0.84)",
    alignSelf: "center",
    fontSize: moderateScale(18),
    fontFamily: Fonts.medium,
  },

  lockedCover: {
    position: "absolute",
    width: buttonWidth,
    height: buttonHeight,
    backgroundColor: "rgba(50,50,50,0.5)",
  },
  lockedCoverInner: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    paddingRight: 40,
  },
  lockedIcon: {
    tintColor: "rgb(30,30,30)",
    width: 25,
    height: 25,
  },
};
