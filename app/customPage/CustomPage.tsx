import Backend, {
  dMediaPageItem,
  eTeamRole,
  MediaPageSchema,
  UserSchema,
} from "backend/";
import React from "react";
import { ScrollView, View } from "react-native";
import { subscribe } from "root/TrackPlayerServices";
import TrackPlayer from "react-native-track-player";
import { C } from "utilities/";
import { customPageHandler } from "./handlers/CustomPageHandler";
import { SS_PasswordWall } from "./sh-prompts/sh-password-wall";
import Player from "./items/audios/player/Player";
import { PageSharingInstructions } from "./components/page-sharing-instructions";
import ProgressBar from "./components/ProgressBar";
import Item from "./items/Item";
import { $_Header } from "./components/page-header";
import { $_Sharing } from "./components/s-share";
import { $_EditPage } from "./components/s-edit-page";
import { editPagePress } from "./handlers/EditPageHandler";
import { SS_Prompt } from "./sh-prompts/sh-prompt";

interface dProps {
  navigation: any;
  pageName: string;
  staticPage?: MediaPageSchema;
}

export default function CustomPage(props: dProps) {
  const { navigation, pageName, staticPage } = props;

  // #region refs
  const scrollViewRef = React.useRef<ScrollView>(null);
  const passwordWallRef = React.useRef<any>();
  const promptRef = React.useRef<any>();
  // #endregion

  // #region states
  const [_account, setAccount] = React.useState<UserSchema>(null);
  const [_page, setPage] = React.useState<MediaPageSchema>(null);
  const [progress, setProgress] = React.useState<number>(0);
  const [audioPlayer, setAudioPlayer] = React.useState<any>(null);
  const [showInstructions, setShowInstructions] = React.useState<boolean>(
    false
  );
  const [shareModeEnabled, setShareModeEnabled] = React.useState<boolean>(
    false
  );
  const [editModeEnabled, setEditModeEnabled] = React.useState<boolean>(false);
  const [checkboxItem, setCheckboxItem] = React.useState<dMediaPageItem>(null);
  // #endregion

  // #region component mount
  React.useEffect(function () {
    // activates sharing mode when the share button (top right) is pressed
    // customPageHandler.subscribe((data) => {
    //   setShareModeActive(data.activateShare);
    //   if (data.activateShare === true) setEditMode(false);
    // });

    customPageHandler.subscribe((response) =>
      setShareModeEnabled(response.activateShare)
    );

    // show instructions on how to use the page features if user is new to the app
    Backend.localHandler.getCustomPageDidOpen().then((didOpen) => {
      !didOpen && setShowInstructions(true);
      Backend.localHandler.setCustomPageDidOpen(); // remembers that the user has now seen the instructions
    });

    // activates audio features
    TrackPlayer.getState().then((state) => trackStateChange());
    subscribe("state", trackStateChange);

    // listens to the backend for changes
    Backend.firestoreHandler.account((account) => {
      setAccount(account);
      Backend.firebasePages.isLevelPage(pageName) &&
        !staticPage &&
        setProgress(Backend.levelsHandler.calculateProgressForLevel(pageName));
    });
    Backend.firebasePages.pages((pages) =>
      setPage(
        staticPage
          ? staticPage
          : Backend.firebasePages.getPageWithName(pageName)
      )
    );
    return () => {
      customPageHandler.customPageUnmounted();
    };
  }, []);
  // #endregion

  // #region functions
  function trackStateChange() {
    TrackPlayer.getCurrentTrack().then((track) => {
      if (!track) {
        setAudioPlayer(null);
      } else {
        setAudioPlayer(<Player onAutoComplete={audioAutoComplete} />);
      }
    });
  }
  function audioAutoComplete() {
    // TODO when an audio finishes, mark is complete (if this page is a level)
    // an audio reached 0.9+ and should auto complete the level item if needed
    if (!Backend.firebasePages.isLevelPage(pageName)) return;
    // if (this.lastPlayedAudioItem && this.progressBar) {
    //   if (this.progressBar.itemData.title === this.lastPlayedAudioItem.title)
    //     this.progressBarChecked(this.lastPlayedAudioItem);
    //   this.lastPlayedAudioItem = null;
    // }
  }
  /**
   * When a media item on the screen gets pressed
   */
  function itemPressed(mediaItem: dMediaPageItem) {
    if (editModeEnabled) {
      // TODO: why this here?
      alert("Editing: " + mediaItem.title);
      return;
    }

    // if the topage field is specified, open the custom page it goes to!
    mediaItem.topage && customPageHandler.presentPage(mediaItem.topage);

    // TODO
    // if (
    //   mediaItem.media &&
    //   (mediaItem.media.includes("dropbox") ||
    //     mediaItem.media.includes("drive.google.com"))
    // ) {
    //   this.lastPlayedAudioItem = mediaItem;
    // }

    // show the tapped item at the top of the progress bar to be checked off if desired
    Backend.firebasePages.isLevelPage(pageName) &&
      Backend.levelsHandler.isCompletableItem(mediaItem) &&
      setCheckboxItem(mediaItem);
  }
  /**
   * If the item was searched for outside of this page, scroll to it immediately
   */
  function itemMeasure(item: dMediaPageItem, measurements: any) {
    const scrollToItem = navigation.getParam("scrollToItem");
    if (!scrollToItem || item.title !== scrollToItem) return;
    const { y } = measurements;
    scrollViewRef.current.scrollTo({ x: 0, y, animated: false });
  }
  // #endregion

  const canEditPage =
    _account?.admin ||
    (_account?.team?.teamName === pageName &&
      _account?.team?.role == eTeamRole.Owner);

  return (
    <>
      <$_Header
        {...props}
        pageName={pageName}
        onSharePress={() =>
          customPageHandler.activateShareMode(!shareModeEnabled)
        }
      />
      <ScrollView
        ref={scrollViewRef}
        style={{
          flex: 1,
          backgroundColor: C.background01,
        }}
        keyboardShouldPersistTaps={"always"}
        stickyHeaderIndices={
          _page && Backend.firebasePages.isLevelPage(pageName) ? [0] : []
        }
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          flexWrap: "wrap",
          flexDirection: "row",
          justifyContent: "space-evenly",
        }}
      >
        {_page && Backend.firebasePages.isLevelPage(pageName) && (
          <>
            <ProgressBar
              page={_page}
              completion={progress}
              checkboxItem={checkboxItem}
            />
          </>
        )}
        {_page?.content.map((item: dMediaPageItem) => (
          <Item
            item={item}
            pageName={pageName}
            onPress={itemPressed}
            onMeasure={itemMeasure}
            shareModeEnabled={shareModeEnabled}
            editModeEnabled={editModeEnabled}
            onEditModePress={(_i) => editPagePress(promptRef, pageName, _i)}
          />
        ))}
        {canEditPage ? (
          <$_EditPage
            pageName={pageName}
            active={editModeEnabled}
            onEditToggle={() => {
              setEditModeEnabled(!editModeEnabled);
              // setShareModeEnabled(false);
              customPageHandler.activateShareMode(false);
            }}
          />
        ) : null}
        <View style={{ width: "100%", height: shareModeEnabled ? 77 : 0 }} />
      </ScrollView>
      <$_Sharing
        visible={shareModeEnabled}
        navigation={navigation}
        onShareModeCancel={() => customPageHandler.activateShareMode(false)}
      />
      <PageSharingInstructions
        visible={showInstructions}
        onDonePress={() => setShowInstructions(false)}
      />
      <SS_PasswordWall
        {...props}
        ref={passwordWallRef}
        page={_page}
        navigation={navigation}
      />
      <SS_Prompt ref={promptRef} />
    </>
  );
}
