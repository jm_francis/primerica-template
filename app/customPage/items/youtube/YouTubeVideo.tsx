import { PLAY } from "app/constant-deprecated/Images";
import { Styles } from "constant-deprecated/";
import React from "react";
import { Image, TouchableWithoutFeedback, View } from "react-native";
import YouTube from "react-native-youtube";
import Title from "../text/Title";
const cpStyles = Styles.customPage;

interface d {
  title?: string;
  source: string;
  onPress?: () => void;
  onAutoComplete?: () => void;
}

/**
 * ### Play a video from YouTube
 * - source must be a YouTube video share link/url
 *  ----
 *  @example
 * <YouTubeVideo
    title={title}
    source={uri}
    onPress={() => {}}
    onAutoComplete={() => {}}
  />
 *  ----
 *  @version 21.03.29
 *  -  Made function component
 *  @author  jm_francis
 *
 **/
export default function YouTubeVideo(props: d) {
  const { title, source, onPress, onAutoComplete } = props;

  const [startVideo, setStartVideo] = React.useState<boolean>(false);

  let id = source.replace("https://youtu.be/", "");
  id = id.substring(0, id.includes("?") ? id.indexOf("?") : id.length);
  const width = cpStyles.video.width;
  const height = cpStyles.video.height;

  function pressed() {
    onPress && onPress();
    setStartVideo(true);
  }

  function stateChange() {
    onAutoComplete && onAutoComplete();
  }

  return (
    <>
      {title ? <Title>{title}</Title> : null}
      <TouchableWithoutFeedback onPress={pressed}>
        <View
          style={{
            width,
            height,
            borderRadius: cpStyles.video.borderRadius,
            backgroundColor: "black",
            overflow: "hidden",
            alignSelf: "center",
          }}
        >
          {startVideo ? (
            <YouTube
              apiKey="AIzaSyDHvbVhBCuD4a0leefsM5GHdPtqwDt-I4U"
              videoId={id}
              fullscreen={true}
              play={true}
              onReady={() => {}}
              onChangeState={stateChange}
              onPress={() => {}}
              style={{
                width,
                height,
              }}
            />
          ) : (
            <View
              style={{
                width,
                height,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={{ uri: `https://img.youtube.com/vi/${id}/0.jpg` }}
                style={{
                  position: "absolute",
                  width,
                  height,
                }}
              />
              <Image
                source={PLAY}
                style={{
                  width: 35,
                  height: 35,
                  tintColor: "white",
                  opacity: 0.87,
                }}
              />
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    </>
  );
}
