import Backend from "backend/";
import { Loading } from "components/";
import { Fonts } from "constant-deprecated/";
import { PLAY } from "constant-deprecated/Images";
import React from "react";
import {
  Dimensions,
  Image,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import TrackPlayer from "react-native-track-player";
import { moderateScale } from "utilities/";

const { DropboxAPI, FileNetwork } = Backend;
const androidCompleteGreen = "rgb(66, 245, 138)";

const State = Object.freeze({
  IDLE: "idle",
  LOADING: "loading",
  LOADED: "loaded",
});

const prettyName = (name: string, keepExtension = false) => {
  let newName = name.substring(0, 3).includes("*")
    ? name.substring(name.indexOf("*") + 1, name.length)
    : name;
  if (keepExtension === false)
    newName = newName.substring(0, newName.length - 4);
  return newName;
};

interface dProps {
  /**
   * The title the user/admin has set for this audio on the app side
   */
  title?: string;
  onPress: () => void;
  /**
   * Whether or not the user has completed this audio (pertaining to the New Rep Levels feature)
   * (this only applies if the CustomPage this AudioBlock is on is used as a Levels page)
   */
  complete?: boolean;
  /**
   * Source of the audio
   */
  source?: string;
  /**
   * More info pertaining to the audio like the name (ex: data the Dropbox API provides)
   */
  metadata?: any;
  /**
   * If this is a single item/file in a playlist/folder (ex: a Dropbox Folder)
   */
  playlistItem?: any;
  /**
   * Add some additional style to the component
   */
  style?: any;
  /**
   * Add some additional style to the title
   */
  textStyle?: any;
}

/**
 * ### The component representing an audio
 * 
 * basic example
 * @example
 * <AudioBlock
    source={audioSource}
    title={title}
    onPress={() => {}}
    complete={false}
  />

 unique example (ex: dropbox folders)
  @example
  <AudioBlock
    playlistItem={true}
    onPress={() => {}}
    metadata={itemMetadata}
    style={{}}
    textStyle={{}}
  />
 *
 * @author jm_francis
 * @version 1.3.19
 */
export default function AudioBlock(props: dProps) {
  const {
    source,
    onPress,
    metadata,
    playlistItem,
    complete,
    title,
    style,
    textStyle,
  } = props;

  const [audioName, setAudioName] = React.useState("Loading...");
  const [state, setLoadingState] = React.useState(State.IDLE);

  React.useEffect(function didMount() {
    if (metadata) {
      const audioName = prettyName(metadata.name, !playlistItem);
      setAudioName(audioName);
      return;
    }

    setAudioName(title);
  }, []);

  /**
   * This is the first function to call to get an audio to play
   */
  function play() {
    if (metadata) {
      // dropbox folder item
      folder_playDropbox();
    } else if (source && source.includes("drive.google.com")) {
      playGoogleDrive();
    } else {
      playDropbox();
    }

    setLoadingState(State.LOADING);
  }
  /**
   * fetch a Dropbox audio's streaming uri that is in a Dropbox folder
   */
  async function folder_playDropbox() {
    const uri = await DropboxAPI.folder_getStreamingURI(metadata.path);
    playAudio(metadata.name, uri);
  }
  /**
   * fetch an audio's streaming uri from Dropbox and play it
   */
  async function playDropbox() {
    const uri = await DropboxAPI.getStreamingURI(source);
    playAudio(title, uri);
  }
  /**
   * determine the Google Drive streaming uri and play it
   */
  function playGoogleDrive() {
    const audioUri = FileNetwork.uriFromSource(source);
    playAudio(title, audioUri);
  }

  /**
   * play an audio that has been fetched from some API already
   */
  function playAudio(title: string, uri: string) {
    TrackPlayer.setupPlayer().then(() => {
      TrackPlayer.add([
        {
          id: uri,
          url: { uri: uri },
          title,
          artist: Backend.firestoreHandler._config.variables.appTitle,
        },
      ]).then(() => {
        TrackPlayer.skip(uri).then(() => {
          TrackPlayer.play();
        });
        setLoadingState(State.LOADED);
      });
    });
  }

  // function playbackPress() {
  //   if (state === State.IDLE) {
  //     play();
  //     setLoadingState(State.LOADING);
  //   }
  // }

  const _title = playlistItem ? audioName : title;

  const androidComplete = complete === true && Platform.OS === "android";

  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          onPress && onPress();
          play();
        }}
      >
        <View
          style={{
            ...Styles.container,
            ...style,
            elevation: 100,
            borderColor: "red",
            boderWidth: 4,
          }}
        >
          <View style={Styles.iconBox}>
            {state === State.LOADING ? (
              <Loading />
            ) : (
              <Image
                resizeMode={"contain"}
                source={PLAY}
                style={{
                  ...Styles.playIcon,
                  ...(androidComplete
                    ? { tintColor: androidCompleteGreen }
                    : {}),
                }}
              />
            )}
          </View>
          <Text
            numberOfLines={2}
            adjustsFontSizeToFit
            style={{
              ...Styles.title,
              ...textStyle,
              ...(androidComplete ? { color: androidCompleteGreen } : {}),
            }}
          >
            {_title}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const screenWidth = Dimensions.get("window").width;
const width = screenWidth * 0.94;

const Styles = {
  container: {
    alignSelf: "center",
    width: Platform.isPad ? width * 0.482 : width,
    height: moderateScale(68, 0.2),
    backgroundColor: "rgb(35, 23, 230)",
    borderRadius: 7,
    marginVertical: 6,
    paddingLeft: 15,
    paddingRight: 30,
    paddingVertical: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  iconBox: {
    height: "40%",
    width: "10%",
  },
  playIcon: {
    flex: 1,
    alignSelf: "center",
    tintColor: "white",
  },
  title: {
    color: "white",
    textAlign: "right",
    fontSize: moderateScale(22, 0.1),
    width: "85%",
    fontFamily: Fonts.bold,
  },
  audioName: {
    marginTop: -3,
    marginRight: 6,
    textAlign: "right",
    color: "rgb(250,250,250)",
    fontFamily: Fonts.medium,
    fontSize: moderateScale(13, 0.1),
  },
};
