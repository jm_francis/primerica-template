import DropboxAPI from "backend/apis/DropboxAPI";
import { Fonts } from "constant-deprecated/";
import React from "react";
import { moderateScale } from "utilities/";
import AudioBlock from "./AudioBlock";

interface dProps {
  source: string;
  onPress: () => void;
  onAutoComplete: () => void;
}

/**
 * ### A dropbox folder with a bunch of audios
 * 
 * @example
 * <DropboxFolder
    source={audio}
    onPress={this.anyPress.bind(this)}
    onAutoComplete={this.onAutoComplete.bind(this)}
  />

  @author jm_francis
  @version 1.3.19
 */
export default function DropboxFolder(props: dProps) {
  const { source, onPress, onAutoComplete } = props;

  const [audioComponents, setAudioComponents] = React.useState([]);

  function handlePress() {
    onPress && onPress();
    if (onAutoComplete) onAutoComplete();
  }

  async function load() {
    const content = await DropboxAPI.folder_getContent(source);
    const audioComponents = [];
    for (let c in content) {
      const item = content[c];
      const audioComponent = (
        <AudioBlock
          playlistItem={true}
          onPress={handlePress}
          metadata={item}
          style={Styles.itemContainer}
          textStyle={Styles.itemText}
        />
      );
      audioComponents.push(audioComponent);
    }
    return audioComponents;
  }

  React.useEffect(() => {
    load().then((audioComponents) => setAudioComponents(audioComponents));
  }, []);

  return <>{audioComponents}</>;
}

const modScale = 0.3;

const Styles = {
  itemContainer: {
    height: moderateScale(50, modScale),
  },
  itemText: {
    fontFamily: Fonts.medium,
    fontSize: moderateScale(20, modScale),
  },
};
