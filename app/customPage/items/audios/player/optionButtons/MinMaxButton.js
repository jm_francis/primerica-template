import React from "react";
import { View, Image, Animated, TouchableOpacity } from "react-native";
import { moderateScale, verticalScale, scale } from "utilities/";
import { BACK_BUTTON } from "constant-deprecated/Images";
import { Animations as _Animations } from "animation";

export default class MinMaxButton extends React.Component {
  animations = new Animations();

  toggleOn = false;

  componentDidMount() {
    if (this.toggleOn) this.animations.on().start();
    else this.animations.off().start();
  }

  press() {
    if (this.toggleOn) {
      this.toggleOn = false;
      this.animations.off().start();
    } else {
      this.toggleOn = true;
      this.animations.on().start();
    }
    if (this.props.onPress) this.props.onPress(this.toggleOn);
  }

  render() {
    const imageRotate = this.animations.interpolate("imageRotate", {
      inputRange: [0, 1],
      outputRange: ["90deg", "270deg"]
    });

    return (
      <TouchableOpacity onPress={this.press.bind(this)}>
        <View style={Styles.container}>
          <Animated.Image
            source={BACK_BUTTON}
            style={{
              ...Styles.image,
              transform: [{ rotate: imageRotate }]
            }}
          />
        </View>
      </TouchableOpacity>
    );
  }
}

const ad = {
  tension: 50,
  friction: 7
};

class Animations extends _Animations {
  on() {
    return this.spring("imageRotate", {
      toValue: 1,
      ...ad
    });
  }
  off() {
    return this.spring("imageRotate", {
      toValue: 0,
      ...ad
    });
  }
}

const dimension = moderateScale(25, 0.25);
const Styles = {
  container: {
    marginRight: 10,
    width: dimension,
    height: dimension,
    margin: 10
  },
  image: {
    width: dimension,
    height: dimension,
    tintColor: "white"
  }
};
