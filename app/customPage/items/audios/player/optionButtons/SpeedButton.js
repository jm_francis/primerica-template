import { Fonts } from "constant-deprecated";
import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { moderateScale } from "utilities/";

export default class SpeedButton extends React.Component {

  press() {
    this.props.onPress(this.props.speed);
  }

  render() {
    const { speed, selectedSpeed } = this.props
    const on = selectedSpeed == speed;

    const selectedContainer = on ? Styles.selectedContainer : {};
    const selectedText = on ? Styles.selectedText : {};

    return (
      <TouchableOpacity onPress={this.press.bind(this)}>
        <View
          style={{
            ...Styles.container,
            ...selectedContainer
          }}
        >
          <Text
            style={{
              ...Styles.text,
              ...selectedText
            }}
          >{`${speed}x`}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const modScale = 0.3;
const dimension = moderateScale(40, modScale);
const Styles = {
  container: {
    width: dimension,
    height: dimension,
    borderRadius: dimension / 2,
    borderWidth: 1.5,
    borderColor: "white",
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontSize: moderateScale(14, modScale),
    color: "white",
    fontFamily: Fonts.medium
  },
  selectedContainer: {
    borderWidth: 2.5
  },
  selectedText: {
    fontFamily: Fonts.bold,
    fontSize: moderateScale(15, modScale)
  }
};
