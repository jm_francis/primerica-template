import { Fonts } from "constant-deprecated";
import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { moderateScale } from "utilities/";
import { modScale } from "./Player";

export default class ControlButton extends Component {
  render() {
    const { onPress, title } = this.props;
    return (
      <TouchableOpacity style={Styles.touchableContainer} onPress={onPress}>
        <View>
          <Text style={Styles.text}>{title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const Styles = {
  touchableContainer: {
    flex: 1
  },
  text: {
    fontSize: moderateScale(18, modScale),
    textAlign: "center",
    fontFamily: Fonts.bold,
    color: "white"
  }
};
