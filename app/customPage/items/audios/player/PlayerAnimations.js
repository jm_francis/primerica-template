import { Animated } from "react-native";
import { Animations } from "animation";
import { Styles } from "./Player";
import { moderateScale } from "utilities/";

export default class PlayerAnimations extends Animations {
  minimize() {
    return this.spring("y", {
      toValue:
        Styles.container.height -
        Styles.progressView.height -
        moderateScale(15, 0.1),
      friction: 8,
      tension: 20,
      useNativeDriver: true
    });
  }
  maximize() {
    return this.spring("y", {
      toValue: 0,
      friction: 8,
      tension: 20,
      useNativeDriver: true
    });
  }
}
