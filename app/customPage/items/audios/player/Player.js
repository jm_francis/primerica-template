import { Fonts } from "constant-deprecated";
import React, { Component } from "react";
import { Animated, Platform, Text, View } from "react-native";
import TrackPlayer from "react-native-track-player";
import { subscribe } from "root/TrackPlayerServices";
import { moderateScale } from "utilities/";
import ControlButton from "./ControlButton";
import MinMaxButton from "./optionButtons/MinMaxButton";
import SpeedButton from "./optionButtons/SpeedButton";
import PlayerAnimations from "./PlayerAnimations";
import ProgressBar from "./ProgressBar";


const _callAll = (funcs, value) => {
  for (f in funcs) funcs[f](value);
};

export default class Player extends Component {
  static callbackFunctions = [];
  static subscribe(callback) {
    this.callbackFunctions.push(callback);
  }

  animations = new PlayerAnimations();

  state = {
    playbackState: TrackPlayer.STATE_NONE,
    title: "Loading...",
    selectedSpeed: 1.0,
  };

  async componentDidMount() {
    subscribe("state", this.onStateChange.bind(this));
    TrackPlayer.getState().then((state) => {
      this.onStateChange(state);
    });

    const total = await TrackPlayer.getDuration();
    if (total < 2) this.animations.maximize().start();
    else this.animations.minimize().start();
  }

  async onStateChange() {
    TrackPlayer.getState().then((state) => {
      this.setState({ playbackState: state });
    });
    TrackPlayer.getCurrentTrack().then((track) => {
      if (!track) return;
      TrackPlayer.getTrack(track).then((trackData) => {
        if (this.state.title !== trackData.title) {
          this.setState({ title: trackData.title, selectedSpeed: 1.0 });
        }
      });
    });
  }

  onTogglePlayback() {
    const { selectedSpeed } = this.state;
    const { onTogglePlayback = () => {} } = this.props;
    onTogglePlayback();

    TrackPlayer.getState().then((state) => {
      if (state === TrackPlayer.STATE_PAUSED) {
        TrackPlayer.play();
        TrackPlayer.setRate(selectedSpeed);
        this.setState({ playbackState: TrackPlayer.STATE_PLAYING });
        this.animations.maximize().start();
      } else {
        TrackPlayer.pause();
        this.setState({ playbackState: TrackPlayer.STATE_PAUSED });
      }
    });
  }
  onPrevious() {
    TrackPlayer.getPosition().then((seconds) => {
      TrackPlayer.seekTo(seconds - 15);
    });
  }
  onNext() {
    TrackPlayer.getPosition().then((seconds) => {
      TrackPlayer.seekTo(seconds + 15);
    });
  }

  minMaxPress(on) {
    if (on) this.animations.maximize().start();
    else this.animations.minimize().start();
  }
  speedButtonPress(speed) {
    this.setState({ selectedSpeed: speed });
    TrackPlayer.getState().then((state) => {
      if (state === TrackPlayer.STATE_PAUSED) return;
      TrackPlayer.setRate(speed);
    });
  }

  onAutoComplete() {
    const { onAutoComplete } = this.props;
    if (onAutoComplete) {
      onAutoComplete();
    }
  }

  render() {
    const { selectedSpeed, title } = this.state;
    const { style = {}, artwork, loadingState } = this.props;
    let middleButtonText = "";

    if (this.state.playbackState === TrackPlayer.STATE_PLAYING)
      middleButtonText = "Pause";
    else middleButtonText = "Play";

    const animationStyles = {
      transform: [
        { translateY: this.animations.create("y", Styles.container.height) },
      ],
    };

    return (
      <Animated.View
        key={title}
        style={{
          ...Styles.container,
          ...animationStyles,
          ...style,
        }}
      >
        <View style={Styles.progressView}>
          <View style={Styles.titleContainer}>
            <Text adjustsFontSizeToFit numberOfLines={2} style={Styles.title}>
              {title}
            </Text>
            <MinMaxButton onPress={this.minMaxPress.bind(this)} />
          </View>
          <ProgressBar onAutoComplete={this.onAutoComplete.bind(this)} />
        </View>
        <View style={Styles.controls}>
          <ControlButton title={"- 15s"} onPress={this.onPrevious.bind(this)} />
          <ControlButton
            title={middleButtonText}
            onPress={this.onTogglePlayback.bind(this)}
          />
          <ControlButton title={"+ 15s"} onPress={this.onNext.bind(this)} />
        </View>
        <View style={Styles.speedContainer}>
          <SpeedButton
            selectedSpeed={selectedSpeed}
            onPress={this.speedButtonPress.bind(this)}
            speed={1}
          />
          <SpeedButton
            selectedSpeed={selectedSpeed}
            onPress={this.speedButtonPress.bind(this)}
            speed={1.5}
          />
          <SpeedButton
            selectedSpeed={selectedSpeed}
            onPress={this.speedButtonPress.bind(this)}
            speed={2}
          />
        </View>
      </Animated.View>
    );
  }
}

export const modScale = 0.3;

export const Styles = {
  container: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: Platform.isPad ? 210 : 165,
    backgroundColor: "rgb(4, 97, 255)",
  },
  progressView: {
    width: "100%",
    height: 50,
  },
  titleContainer: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  title: {
    width: "80%",
    color: "white",
    fontFamily: Fonts.medium,
    marginHorizontal: 15,
    fontSize: moderateScale(18, modScale),
    marginTop: 4,
  },
  controls: {
    marginTop: moderateScale(14, modScale),
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  speedContainer: {
    width: "100%",
    marginTop: moderateScale(10, modScale),
    flexDirection: "row",
    justifyContent: "space-around",
  },
};
