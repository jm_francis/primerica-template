import React from "react";
import { View, Platform } from "react-native";
import { moderateScale } from "utilities/";
import { ProgressComponent } from "react-native-track-player";

export default class ProgressBar extends ProgressComponent {
  justRan = false;

  UNSAFE_componentWillUpdate(nextProps, nextState) {
    const { onAutoComplete } = this.props;
    const completion = nextState.position / nextState.duration;
    if (completion > 0.05 && completion < 0.5) {
      this.justRan = false;
    }
    if (completion > 0.9 && !this.justRan) {
      this.justRan = true;
      if (onAutoComplete) onAutoComplete();
    }
  }

  render() {
    return (
      <View style={Styles.container}>
        <View
          style={{
            flex: this.getProgress(),
            backgroundColor: "rgb(0, 2, 237)"
          }}
        />
        <View
          style={{
            flex: 1 - this.getProgress(),
            backgroundColor: "rgb(218, 219, 217)"
          }}
        />
      </View>
    );
  }
}

const Styles = {
  container: {
    alignSelf: "center",
    width: Platform.isPad ? "96%" : "94%",
    height: moderateScale(4, 0.1),
    borderRadius: 2,
    borderRadius: 6,
    overflow: "hidden",
    marginTop: 2,
    flexDirection: "row"
  }
};
