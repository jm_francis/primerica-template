import { Fonts } from "constant-deprecated";
import React from "react";
import { Dimensions, Platform, Text, View } from "react-native";
import { Cheat } from "react-native-shortcuts";
import { moderateScale } from "utilities/";
const screenWidth = Dimensions.get("window").width;

export default class Title extends React.Component {
  render() {
    const { button = false } = this.props;

    const userStyle = Cheat.makeStyleArray(this.props.style);
    const thisStyle = [
      {
        color: "white",
        fontFamily: Fonts.bold,
        fontSize: moderateScale(23, 0.4),
        ...(button && Platform.OS === "android"
          ? {
              top: -3.4
            }
          : {})
      }
    ];
    const finalStyle = thisStyle.concat(userStyle);

    const viewStyle = {
      marginTop: button === true ? 0 : Platform.isPad ? 14 : 5,
      marginHorizontal: 12,
      width: button
        ? undefined
        : Platform.isPad
        ? screenWidth * 0.92 * 0.5
        : screenWidth * 0.92,
      justifyContent: "flex-end",
      height: Platform.isPad ? (button ? undefined : 90) : undefined
    };

    return (
      <View style={viewStyle}>
        <Text
          adjustsFontSizeToFit
          allowsFontScaling
          numberOfLines={2}
          style={finalStyle}
        >
          {this.props.children}
        </Text>
      </View>
    );
  }
}
