import { Fonts } from "constant-deprecated";
import React from "react";
import { Dimensions, Text, View } from "react-native";
import { moderateScale } from "utilities/";
const screenWidth = Dimensions.get("window").width;

export default class Paragraph extends React.Component {
  render() {
    const { textStyle, style } = this.props;

    const paragraphStyle = {
      color: "white",
      fontFamily: Fonts.medium,
      fontSize: moderateScale(17, 0.4),
      ...textStyle
    };

    const viewStyle = {
      marginTop: 18,
      // paddingHorizontal: 12,
      width: screenWidth * 0.88,
      ...style
    };

    return (
      <View style={viewStyle}>
        <Text adjustsFontSizeToFit allowsFontScaling style={paragraphStyle}>
          {this.props.children}
        </Text>
      </View>
    );
  }
}
