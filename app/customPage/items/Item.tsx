import Backend, { dMediaPageItem, UserSchema } from "backend/";
import React from "react";
import { View, ViewStyle } from "react-native";
import AudioBlock from "./audios/AudioBlock";
import { customPageHandler } from "../handlers/CustomPageHandler";
import {
  ItemType,
  ServiceType,
  TypeOfItem,
  TypeResponse,
} from "../handlers/TypeProvider";
import Paragraph from "./text/Paragraph";
import Title from "./text/Title";
import VideoBlock from "./videos/VideoBlock";
import YouTubeVideo from "./youtube/YouTubeVideo";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";

// #region types
interface dItem {
  item: dMediaPageItem;
  pageName: string;
  onPress: (item: dMediaPageItem) => void;
  onMeasure: (item: dMediaPageItem, measurements: any) => void;
  /**
   * If share mode is enabled, the item will become selectable and deselectable to be shared
   */
  shareModeEnabled?: boolean;
  /**
   * If edit mode is enabled, the item will call the onEditModePress prop when tapped
   * - the appearance of the item will also change to show it is in edit mode
   */
  editModeEnabled?: boolean;
  /**
   * If the item is tapped when editModeEnabled is true
   */
  onEditModePress?: (item: dMediaPageItem) => void;
}
// #endregion

/**
 * ### An item on a CustomPage that could be something like a video, audio, link, or button
 *  ----
 *  @example
 *  Copy and paste function is the best...
 *  ----
 *  @version 21.03.26
 *  -  refactor
 *  @author jm_francis
 *
 **/
export default function Item(props: dItem) {
  const {
    item,
    pageName,
    onPress,
    onMeasure,
    // onShareSelect,
    shareModeEnabled,
    editModeEnabled,
    onEditModePress,
  } = props;

  // #region state
  const [typeResponse, setTypeResponse] = React.useState<TypeResponse>(null);
  const [_account, setAccount] = React.useState<UserSchema>(null);
  // const [shareModeEnabled, setShareModeEnabled] = React.useState(false);
  const [_completed, setCompleted] = React.useState(false);
  const [_isSharedSelected, setShareSelected] = React.useState(false);
  //   const [_deepShare, setDeepShare] = React.useState(false); // TODO: why is this in existence?
  // #endregion

  // #region refs
  const containerRef = React.useRef(null);
  // #endregion

  // mount
  React.useEffect(function () {
    TypeOfItem(item).then((response) => setTypeResponse(response));

    // customPageHandler.subscribe((data) =>
    //   setShareModeEnabled(data.activateShare)
    // );
    Backend.firestoreHandler.account((account) => {
      setAccount(account);
      //   setLocked(isLocked());
      /**
       * if this goes to a page that is a level
       */
      //   if (Backend.firebasePages.isLevelPage(item.topage))
      //     setProgress(
      //       Backend.levelsHandler.calculateProgressForLevel(item.topage)
      //     );
      /**
       * if this item is an item that exists on a level page
       */
      //   const thisLevel =
      //     _account && _account.levels && _account.levels[pageName]
      //       ? _account.levels[pageName][item.id]
      //       : {};
      //   if (!thisLevel || !thisLevel.complete) setCompleted(false);
      //   else if (thisLevel.complete) setCompleted(true);
    });
  }, []);
  React.useEffect(
    function resetShareMode() {
      if (shareModeEnabled === false) setShareSelected(false);
    },
    [shareModeEnabled]
  );

  function onLayout(e: any) {
    if (!onMeasure) return;
    containerRef.current.measure((x, y, width, height, pageX, pageY) => {
      onMeasure(item, { x, y, width, height, pageX, pageY });
    });
  }

  function coverPress() {
    if (shareModeEnabled && customPageHandler.isItemShareable(item)) {
      const source =
        typeResponse.serviceType === ServiceType.NONE ? item.url : item.media;
      !_isSharedSelected &&
        customPageHandler.addShareItem(pageName, {
          mediaItem: item,
          selected: !_isSharedSelected,
          source,
        });
      _isSharedSelected && customPageHandler.removeShareItem(pageName, item);
      setShareSelected(!_isSharedSelected);
    }
    if (editModeEnabled) onEditModePress(item);
  }

  const _shareModeEnabledStyle =
    shareModeEnabled && customPageHandler.isItemShareable(item)
      ? Styles.activateShare
      : {};
  const _shareModeSelectedStyle =
    shareModeEnabled && _isSharedSelected ? Styles.activateShareSelected : {};
  const shareModeStyle = {
    ..._shareModeEnabledStyle,
    ..._shareModeSelectedStyle,
  };
  //   const shareStyles =
  //     _deepShare === false
  //       ? {
  //           ...activateShareStyle,
  //           ...activateShareSelected,
  //         }
  //       : {};
  const editModeCoverStyle = editModeEnabled
    ? {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "rgb(0,0,255)",
        backgroundColor: "rgba(0,0,255,0.14)",
      }
    : {};

  const completeStyle = _completed === true ? Styles.completed : {};

  return typeResponse?.itemType === ItemType.ERROR ? null : (
    <View
      ref={containerRef}
      style={{
        ...Styles.container,
        ...completeStyle,
        ...shareModeStyle,
        ...editModeCoverStyle,
      }}
      onLayout={onLayout}
    >
      {(() => {
        switch (typeResponse?.itemType) {
          case ItemType.VIDEO:
            return typeResponse.serviceType === ServiceType.YOUTUBE ? (
              <YouTubeVideo
                title={item.title}
                source={item.media}
                onPress={() => {}}
                onAutoComplete={() => {}}
              />
            ) : (
              <VideoBlock
                serviceType={typeResponse.serviceType}
                title={item.title}
                source={item.media}
                onPress={() => {}}
                onAutoComplete={() => {}}
              />
            );
          case ItemType.AUDIO:
            return (
              <AudioBlock
                source={item.media}
                title={item.title}
                onPress={() => {}}
                complete={false}
              />
            );
          case ItemType.BUTTON:
            return null;
          case ItemType.LEVEL_BUTTON:
            return null;
          case ItemType.TITLE:
            return <Title>{item.title}</Title>;
          case ItemType.PARAGRAPH:
            return <Paragraph>{item.paragraph}</Paragraph>;
        }
        return null;
      })()}
      <Cover
        active={shareModeEnabled || editModeEnabled}
        onPress={coverPress}
      />
    </View>
  );
}

interface dCoverProps {
  active: boolean;
  onPress: () => void;
  style?: ViewStyle;
}
function Cover(props: dCoverProps) {
  const { active, onPress, style = {} } = props;
  return active ? (
    <View
      style={{
        position: "absolute",
        width: "100%",
        height: "100%",
        ...style,
      }}
    >
      <TouchableWithoutFeedback onPress={onPress}>
        <View
          style={{
            width: "100%",
            height: "100%",
          }}
        />
      </TouchableWithoutFeedback>
    </View>
  ) : null;
}

export const Styles = {
  container: {
    width: "100%",
    overflow: "hidden",
    marginTop: 16,
    justifyContent: "center",
    alignSelf: "center",
  },
  activateShare: {
    borderColor: "rgba(205, 205, 205, 0.6)",
    borderWidth: 1.88,
    borderRadius: 12,
  },
  activateShareSelected: {
    borderColor: "#40ff93",
    borderWidth: 3.8,
  },
  completed: {
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 3.7,
    shadowOpacity: 0.92,
    shadowColor: "#8aff9d",
  },
};
