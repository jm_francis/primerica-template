import { Colors } from "constant-deprecated/";
import { PLAY_BUTTON } from "constant-deprecated/Images";
import React from "react";
import { Image } from "react-native";

interface dProps {}

/**
 * ### Brief description
 * Play button image that displays on top of a video (VideoBlock component)
 *  ----
 *  @example
 *  See VideoBlock.tsx
 *  ----
 *  @version 21.03.29
 *  -  made functional component
 *  @author jm_francis
 *
 **/
export function VideoPlayImage(props: dProps) {
  const {} = props;
  return (
    <Image
      source={PLAY_BUTTON}
      resizeMode={"contain"}
      style={{
        opacity: 0.75,
        height: 50,
        width: 60,
        marginBottom: -8,
        marginLeft: -4,
        tintColor: Colors.vimeoBlue,
      }}
    />
  );
}
