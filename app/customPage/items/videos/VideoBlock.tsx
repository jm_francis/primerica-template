//@ts-check
import VimeoAPI from "app/backend/apis/VimeoAPI";
import Backend from "backend/";
import { Styles as constantStyles } from "constant-deprecated/";
import Video, {
  OnLoadData,
  OnProgressData,
  VideoProperties,
} from "react-native-video";
import { Video as OverrideVideo } from "override/Video";
import React from "react";
import { Text } from "@ui-kitten/components";
import { Image, Platform, TouchableOpacity, View } from "react-native";
import { VideoPlayImage } from "./PlayButton";
import { ServiceType } from "customPage/handlers/TypeProvider";
import Title from "../text/Title";
import { Buttoon } from "components";
import { customPageHandler } from "customPage/handlers/CustomPageHandler";
const { FileNetwork } = Backend;

const VideoComponent = (_props: VideoProperties) =>
  Platform.OS === "ios" ? <Video {..._props} /> : <OverrideVideo {..._props} />;

interface dProps {
  source: string;
  onPress?: () => void;
  /**
   * The service supporting the video (ex: Vimeo, GoogleDrive)
   * This is needed to get the thumbnail properly
   */
  serviceType?: ServiceType;
  /**
   * Optionally give the video a title
   */
  title?: string;
  /**
   * When the video reaches a point at which it can be considered watched
   * (usually 90% of the video being watched triggers this)
   */
  onAutoComplete?: () => void;
  /**
   * IF you have fetched some metadata from a service like Google Drive, provide it here
   * Ex: Google Drive video thumbnail
   */
  // metadata?: any;
}

/**
 * ### A video item - originally intended for CustomPage
 * - If the source prop is not a direct streaming uri, please provide the type of service like Vimeo, GoogleDrive, etc. so the component knows how to fetch the thumbnail and video uri
 *  ----
 *  @example
 *  <VideoBlock
      serviceType={ServiceType.VIMEO}
      title={title}
      source={uri}
      onPress={() => { console.log("Hey, you hit me!") }}
      onAutoComplete={() => { console.log("I am all or mostly watched! :D") }}
    />
 *  ----
 *  @version 21.03.29
 *  @author  jm_francis
 *
 **/
export default function VideoBlock(props: dProps) {
  const {
    source,
    serviceType = ServiceType.NONE,
    title,
    onPress,
    onAutoComplete,
  } = props;

  // #region state
  /**
   * The streamUri may differ from the provided source prop so always use this for the video uri
   */
  const [streamUri, setStreamUri] = React.useState<string>(null);
  const [startVideo, setStartVideo] = React.useState<boolean>(false);
  const [thumbnail, setThumbnail] = React.useState<string>(null);
  // #endregion

  // #region refs
  const videoRef = React.useRef<any>(null);
  // #endregion

  // #region get thumbnail
  React.useEffect(function getThumbnail() {
    if (serviceType === ServiceType.CLOUDINARY) {
      // ANCHOR: CLOUDINARY
      setStreamUri(source);
      setThumbnail(FileNetwork.getThumbnailFromURL(source));
    } else if (serviceType === ServiceType.VIMEO) {
      // ANCHOR: VIMEO
      VimeoAPI.getVideo({ shareLink: source }).then((response, error) => {
        const videoSource = VimeoAPI.getMP4FromVideo(response);
        const videoThumbnail = VimeoAPI.getThumbnailFromVideo(response);
        /* NOTE the following can be used in the future if needed (so don't delete this)
         * const titleFromVimeo = response.name
         * const videoLinkFromVimeo = response.link
         */

        setStreamUri(videoSource);
        setThumbnail(videoThumbnail);
      });
    } else if (serviceType === ServiceType.GOOGLE_DRIVE) {
      // ANCHOR: GOOGLE DRIVE
      const videoSource = FileNetwork.uriFromSource(source);
      setStreamUri(videoSource);
      FileNetwork.getVideoMetadata(source).then((response) => {
        if (!response.json.error) {
          setThumbnail(response.thumbnail);
        }
      });
    }
  }, []);
  // #endregion

  // #region video callback functions
  function videoLoaded(data: OnLoadData) {}
  function handleProgress(progress?: OnProgressData) {
    if (!progress) return;
    if (progress.currentTime / progress.seekableDuration > 0.9)
      onAutoComplete && onAutoComplete();
  }
  function playbackEnded() {
    setStartVideo(false);
  }
  // #endregion

  return (
    <>
      {title ? <Title>{title}</Title> : null}
      <TouchableOpacity
        disabled={startVideo}
        style={Styles.touchStyle}
        onPress={() => {
          onPress && onPress();
          setStartVideo(true);
        }}
      >
        <View style={Styles.body}>
          <View style={Styles.image}>
            {thumbnail ? (
              <Image source={{ uri: thumbnail }} style={Styles.image} />
            ) : (
              <Text style={Styles.processingText}>...</Text>
            )}
          </View>
          <View style={Styles.absolute}>
            <VideoPlayImage />
          </View>
        </View>
      </TouchableOpacity>
      {startVideo ? (
        <VideoComponent
          ref={videoRef}
          source={{ uri: streamUri }}
          style={Platform.OS === "ios" ? Styles.videoIOS : {}}
          resizeMode="contain"
          controls={true}
          ignoreSilentSwitch={"ignore"}
          paused={false}
          pictureInPicture={true}
          fullscreen={true}
          preventsDisplaySleepDuringVideoPlayback={true}
          onLoad={videoLoaded}
          onProgress={handleProgress}
          onEnd={playbackEnded}
          onFullscreenPlayerWillDismiss={playbackEnded}
          onVideoFullscreenPlayerDidDismiss={playbackEnded}
        />
      ) : null}
    </>
  );
}

const cpStyles = constantStyles.customPage;
const width = cpStyles.video.width;
const height = cpStyles.video.height;
const Styles = {
  touchStyle: {
    alignSelf: "center",
  },
  body: {
    width,
    height,
    borderRadius: cpStyles.video.borderRadius,
    backgroundColor: "black",
    overflow: "hidden",
  },
  image: {
    width,
    height,
    justifyContent: "center",
    alignItems: "center",
  },
  processingText: {
    color: "white",
    fontSize: 39,
    fontWeight: "bold",
    letterSpacing: 3,
  },
  absolute: {
    position: "absolute",
    padding: 12,
    width,
    height,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
  },
  videoIOS: {
    overflow: "hidden",
    borderRadius: cpStyles.video.borderRadius,
    position: "absolute",
    top: 0,
    width: cpStyles.video.width,
    height: cpStyles.video.height,
    alignSelf: "center",
  },
};
