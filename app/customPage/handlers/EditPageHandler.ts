import Backend, { dMediaPageItem } from "backend/";
import { Toasty } from "components/";
import { defaultLayoutAnimation } from "pages/listBuilder/configure";
// import React from "react";
import { Alert, LayoutAnimation } from "react-native";

let itemToMove: dMediaPageItem;

export async function editPagePress(
  promptRef,
  pageName: string,
  item: dMediaPageItem
) {
  //   const itemToMove = React.useRef<dMediaPageItem>(null);
  const page = Backend.firebasePages.getPageWithName(pageName);
  if (itemToMove) {
    /**
     * Move an item's order of layout on the screen :D
     */
    let newPosition = item.position;
    for (let i = 0; i < page.content.length; i++) {
      const _item = page.content[i];
      if (_item.id == item.id) {
        const _previousItem = page.content[i - 1];
        let _previousPosition =
          i === 0 ? page.content[0].position - 1 : _previousItem.position;
        if (_item.position === _previousPosition) {
          _previousPosition -= 0.001;
          await Backend.firebasePages.updatePageContent(pageName, {
            ..._previousItem,
            position: _previousPosition,
          });
        }
        newPosition = median([_item.position, _previousPosition]);
        break;
      }
    }
    console.log("new item position: " + newPosition);
    await Backend.firebasePages
      .updatePageContent(pageName, {
        ...itemToMove,
        position: newPosition,
      })
      .then(() => {
        Toasty({ text1: "Move complete." });
      })
      .catch(() => uhOh());
    itemToMove = null;
    return;
  }
  const title = `${
    item.paragraph ? item.paragraph : item.title ? item.title : ""
  }`;
  const quotedTitle = `"${title}"`;
  Alert.alert("Choose an option.", quotedTitle, [
    {
      text: `Edit ${item.paragraph ? "Text" : "Title"}`,
      onPress: () => {
        prompt(promptRef, {
          // title,
          defaultValue: title,
          onSubmit: (text) => {
            console.log("TEXT: " + text);
            loading();
            const updatedItem = { ...item };
            if (updatedItem.paragraph) updatedItem.paragraph = text;
            else updatedItem.title = text;
            Backend.firebasePages
              .updatePageContent(pageName, updatedItem)
              .then(() => success("Title updated!"))
              .catch(() => uhOh());
          },
        });
      },
    },
    {
      text: "Adjust position",
      onPress: () => {
        itemToMove = item;
        Alert.alert("Tap an item you want to place this item on top of.");
      },
    },
    {
      text: "Delete",
      onPress: () => {
        Alert.alert(
          "Confirmation",
          `Are you sure you want to delete this item from your page?\n${quotedTitle}`,
          [
            {
              text: "No",
              style: "cancel",
            },
            {
              text: "Yes",
              onPress: () => {
                loading("Deleting item...");
                Backend.firebasePages
                  .updatePageContent(pageName, item, {
                    remove: true,
                  })
                  .then(() => {
                    success("Item deleted.");
                    LayoutAnimation.configureNext(defaultLayoutAnimation);
                  })
                  .catch(() => uhOh());
              },
            },
          ]
        );
      },
    },
    {
      text: "Cancel",
      style: "cancel",
    },
  ]);
}

export function inputPrompt(
  promptRef: any,
  title: string,
  subtitle = "",
  submit: (value: string) => void,
  delay = 0
) {
  setTimeout(() => {
    prompt(promptRef, {
      title,
      subtitle: subtitle,
      onSubmit: submit,
    });
  }, delay);
}
export interface dPromptOptions {
  title?: string;
  subtitle?: string;
  defaultValue?: string;
  submitText?: string;
  onSubmit: (value: string) => void;
  onCancel?: () => void;
}
export function prompt(promptRef: any, _options: dPromptOptions) {
  const options = {
    title: "",
    subtitle: "",
    defaultValue: "",
    submitText: "Submit",
    onSubmit: (value) => {
      console.log("entered value:" + value);
    },
    onCancel: () => {},
    ..._options,
  };
  promptRef.current?.openWithOptions(options);
}

function uhOh() {
  Toasty({
    type: "error",
    text1:
      "Something went wrong. Please check your internet connection and try again.",
  });
}
function loading(text = "Updating...") {
  Toasty({
    type: "info",
    text1: text,
  });
}
function success(text = "Update complete!") {
  Toasty({
    text1: text,
  });
}

const median = (arr) => {
  const mid = Math.floor(arr.length / 2),
    nums = [...arr].sort((a, b) => a - b);
  return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
};
