import Backend, { dMediaPageItem } from "backend/";
const { firebasePages } = Backend;
import { typeOfPage, PageType } from "backend/FirebasePages";
import { Toasty } from "components";
import { CustomPageNavigationOptions } from "navigation/CustomDisplay";

// TODO: customPageLevel no longer needed (replaced by presentedPages) (other files will need to be updated though)

/**
 * The state of an item when in share mode
 */
export interface dShareSelect {
  mediaItem: dMediaPageItem;
  /**
   * Whether or not the item is selected for sharing
   */
  selected: boolean;
  /**
   * The source is predetermined for you to use (likely came from item.url or item.media)
   */
  source: string;
}

export interface dCallbackResponse {
  activateShare: boolean;
  shareStates: dShareSelect[];
}

export default class CustomPageHandler {
  customPageLevel = 0;
  presentedPages = [];
  navigation: any = null;

  shareStates: any = {};
  _callbacks: ((response: dCallbackResponse) => void)[] = [];

  init(navigation: any) {
    this.navigation = navigation;
  }

  isPageActive(pageName: string) {
    const presentedPages = this.presentedPages;
    for (let p in presentedPages)
      if (pageName === presentedPages[p]) return true;
    return false;
  }

  presentPage(
    pageName: string,
    _options?: Partial<CustomPageNavigationOptions>
  ) {
    const options = _options ? _options : {};
    const { scrollToItem } = options;
    if (
      (this.presentedPages.length > 0 &&
        this.presentedPages[this.presentedPages.length - 1] === pageName) ||
      !firebasePages.getPageWithName(pageName)
    ) {
      Toasty({ text1: "Page not found.", type: "error" });
      return;
    }
    this.presentedPages.push(pageName);
    // const tintColor =
    //   pageData.mediaItem.team === true ? pageData.mediaItem.color : undefined;
    ++this.customPageLevel;
    this.navigation.navigate(`CustomDisplay${this.customPageLevel}`, <
      CustomPageNavigationOptions
    >{
      pageName,
      scrollToItem,
    });
  }
  presentNewPersonPage() {
    const newPersonPage = this.getNewPersonPage();
    newPersonPage && this.presentPage(newPersonPage.name);
  }
  getNewPersonPage() {
    const pages = Backend.firebasePages._pages;
    for (let p in pages) {
      const pg = pages[p];
      if (typeOfPage(pg) === PageType.NewPerson) return pg;
    }
  }

  customPageUnmounted() {
    this.presentedPages.pop();
    --this.customPageLevel;
    console.log("custompageLevel: " + this.presentedPages.length);
    if (this.customPageLevel < 1) this.activateShareMode(false);
    else this.updatePages(null);
  }

  subscribe(callback: (response: dCallbackResponse) => void) {
    this._callbacks.push(callback);
    this.updatePages(callback);
  }
  updatePages(onlyThisCallback: (response: dCallbackResponse) => void) {
    const data: dCallbackResponse = {
      activateShare: this.shareStates.activateShare,
      shareStates: this.shareStates,
    };
    if (onlyThisCallback) {
      onlyThisCallback(data);
      return;
    }
    for (let s in this._callbacks) this._callbacks[s](data);
  }

  /**
   * ### Returns all the media items selected to be shared
   */
  compileShareItems(): dMediaPageItem[] {
    const items: dMediaPageItem[] = [];
    for (let k in this.shareStates) {
      const state = this.shareStates[k];
      for (let i in state) items.push(state[i].mediaItem);
    }
    return items;
  }
  /**
   * ### Whether or not this media item is shareable
   * - example: a title by itself would not be considered shareable
   */
  isItemShareable(item: dMediaPageItem): boolean {
    if (typeof item.paragraph === "string" && item.paragraph.length > 1)
      return true;
    if (typeof item.media === "string" && item.media.length > 2) return true;
    if (typeof item.url === "string" && item.url.length > 2) return true;
    return false;
  }
  /**
   * ### Enable (or disable) share mode
   */
  activateShareMode(status = true) {
    if (status === false) {
      this.shareStates = {};
    }
    this.shareStates.activateShare = status;
    if (status === false) this.shareStates = { activateShare: false };
    this.updatePages(null);
  }
  /**
   * ### Add an item to the list of items that will be shared
   */
  addShareItem(pageName: string, data: dShareSelect) {
    if (!this.shareStates[pageName]) this.shareStates[pageName] = [];
    this.shareStates[pageName].push(data);

    this.updatePages(null);
    // console.log(JSON.stringify(this.shareStates));
  }
  /**
   * ### Remove an item from the list of items that will be shared
   */
  removeShareItem(pageName: string, mediaItem: dMediaPageItem) {
    // const array = this.shareStates[pageName];
    this.shareStates[pageName] = this.shareStates[pageName].filter(
      (value: dShareSelect) => {
        if (value.mediaItem.id === mediaItem.id) return false;
        return true;
      }
    );

    this.updatePages(null);
    // console.log(JSON.stringify(this.shareStates));
  }
  /**
   * ### Get the share state of an item by providing its share source/uri
   * - Returns true if the item is in the list of items to be shared and false if not
   */
  shareStateOf(pageName: string, item: dMediaPageItem): boolean {
    const shareState = this.shareStates[pageName];
    for (let s in shareState) {
      if (shareState[s].item.id === item.id) return true;
    }
    return false;
  }
}

export const customPageHandler = new CustomPageHandler();

export function shareItemsToPrettyMessage(renderHTML = false) {
  let message = "";
  const shareItems = customPageHandler.compileShareItems();
  console.log(shareItems);
  let index = 0;
  for (let i in shareItems) {
    const shareItem = shareItems[i];
    const text =
      typeof shareItem.title === "string" && shareItem.title.length > 1
        ? shareItem.title
        : typeof shareItem.paragraph === "string" &&
          shareItem.paragraph.length > 1
        ? shareItem.paragraph
        : "";
    const source =
      typeof shareItem.media === "string" && shareItem.media.length > 2
        ? shareItem.media
        : typeof shareItem.url === "string" && shareItem.url.length > 2
        ? shareItem.url
        : null;
    // if (!shareItem.title || !shareItem.source) continue;
    if (renderHTML && source) {
      message += `<a href="${source}">${text}</a><br><br>`;
    } else
      message +=
        text +
        (source ? ` \n${source}` : "") +
        (index + 1 == shareItems.length ? "" : " \n\n");

    ++index;
  }
  return renderHTML ? "<html>" + message + "</html>" : message;
}
