import Backend, { dMediaPageItem } from "backend/";
import FileNetwork, { dGoogleDriveData } from "backend/apis/FileNetwork";

/**
 * ### The service being used
 * - if not a service, NONE (example: text, url)
 */
export enum ServiceType {
  CLOUDINARY,
  GOOGLE_DRIVE,
  DROPBOX,
  VIMEO,
  YOUTUBE,
  NONE,
}
/**
 * ### The type of media to display
 */
export enum ItemType {
  TITLE,
  PARAGRAPH,
  VIDEO,
  AUDIO,
  /**
   * A list of audios (in Dropbox)
   */
  AUDIO_FOLDER,
  BUTTON,
  LEVEL_BUTTON,
  ERROR,
}

export interface TypeResponse {
  serviceType: ServiceType;
  itemType: ItemType;
  data?: any | dGoogleDriveData;
}

function stringValue(value: any): string {
  return value && typeof value === "string" && value.length > 2 ? value : null;
}

/**
 * ### Determines the type of item
 * - also provides the service (ex: Cloudinary, Dropbox) and any data fetched from that service like the thumbnail
 * @example
 * const itemTypeData = await TypeOfItem(mediaItem)
 * ---
 * @auhor jm_francis
 * @version 21.3.26
 */
export async function TypeOfItem(item: dMediaPageItem): Promise<TypeResponse> {
  const media = stringValue(item.media);
  const url = stringValue(item.url);
  const topage = stringValue(item.topage);
  const title = stringValue(item.title);
  const paragraph = stringValue(item.paragraph);

  // ANCHOR: Cloudinary
  if (media?.includes("cloudinary.com")) {
    // TODO: assumes video, but should also support audio
    return {
      serviceType: ServiceType.CLOUDINARY,
      itemType: ItemType.VIDEO,
      //   data: { thumbnail: FileNetwork.getThumbnailFromURL(media) },
    };
  }
  // ANCHOR: YouTube
  else if (media?.includes("youtu.be") || media?.includes("youtube.com")) {
    return {
      serviceType: ServiceType.YOUTUBE,
      itemType: ItemType.VIDEO,
    };
  }
  // ANCHOR: Vimeo
  else if (media?.includes("vimeo.com")) {
    return {
      serviceType: ServiceType.VIMEO,
      itemType: ItemType.VIDEO,
    };
  }
  // ANCHOR: Dropbox
  else if (media?.includes("dropbox.com")) {
    let mediaType = ItemType.AUDIO;
    let dbstr = media.substring(0, media.indexOf("?"));
    dbstr = dbstr.substring(dbstr.length - 5, dbstr.length);
    if (!dbstr.includes(".")) mediaType = ItemType.AUDIO_FOLDER;
    return {
      serviceType: ServiceType.DROPBOX,
      itemType: mediaType,
    };
  }
  // ANCHOR: Google Drive
  else if (media?.includes("drive.google.com")) {
    const videoMetadata = await FileNetwork.getVideoMetadata(item.media);
    if (!videoMetadata.json.error) {
      return {
        serviceType: ServiceType.GOOGLE_DRIVE,
        itemType: videoMetadata.fileType.includes("video")
          ? ItemType.VIDEO
          : videoMetadata.fileType.includes("audio")
          ? ItemType.AUDIO
          : ItemType.ERROR,
        data: videoMetadata,
      };
    } else
      return {
        serviceType: ServiceType.GOOGLE_DRIVE,
        itemType: ItemType.ERROR,
        data: videoMetadata,
      };
  }
  // ANCHOR: Button
  else if (url || topage) {
    let mediaType = ItemType.BUTTON;
    if (topage && Backend.firebasePages.isLevelPage(topage))
      mediaType = ItemType.LEVEL_BUTTON;
    return {
      serviceType: ServiceType.NONE,
      itemType: mediaType,
    };
  }
  // ANCHOR: Title
  else if (title) {
    return {
      serviceType: ServiceType.NONE,
      itemType: ItemType.TITLE,
    };
  }
  // ANCHOR: Paragraph
  else if (paragraph) {
    return {
      serviceType: ServiceType.NONE,
      itemType: ItemType.PARAGRAPH,
    };
  }
  // ANCHOR: No data provided - item is completely empty/bank (UI should render nothing)
  return {
    serviceType: ServiceType.NONE,
    itemType: ItemType.ERROR,
    data: null,
  };
}
