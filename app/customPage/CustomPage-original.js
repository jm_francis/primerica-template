// @ts-check
import Backend, { eTeamRole } from "backend/";
import { Buttoon, DirectCoverModal, Toasty } from "components/";
import { Config } from "constant-deprecated/";
import { defaultLayoutAnimation } from "pages/listBuilder/configure";
import { MemoryJoggerEntryButton } from "pages/listBuilder/navigation/MemoryJoggerEntryButton";
import React, { Component } from "react";
import {
  Alert,
  Dimensions,
  LayoutAnimation,
  Platform,
  ScrollView,
  View,
} from "react-native";
import ActionSheet from "react-native-actionsheet";
import { isIphoneX } from "react-native-iphone-x-helper";
import Share from "react-native-share";
import TrackPlayer from "react-native-track-player";
import { subscribe } from "root/TrackPlayerServices";
import { C, moderateScale, spacing, verticalScale } from "utilities/";
import Player, { Styles as PlayerStyles } from "./items/audios/player/Player";
// export const customPageHandler = new CustomPageHandler()
import { customPageHandler } from "./handlers/CustomPageHandler";
import Item from "./items/Item";
import ProgressBar from "./components/ProgressBar";
import { SS_PasswordWall } from "./sh-prompts/sh-password-wall";
import { SS_Prompt } from "./sh-prompts/sh-prompt";
import ShareBar from "./components/ShareBar";
import ShareModal from "./components/share-modal";

const screenWidth = Dimensions.get("window").width;

const ShareOptions = Object.freeze({
  EMAIL: 1,
  APP_CONTACTS: 0,
  SOCIAL_MEDIA: 2,
});

const promptRef = React.createRef();

const median = (arr) => {
  const mid = Math.floor(arr.length / 2),
    nums = [...arr].sort((a, b) => a - b);
  return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
};

export default class CustomPage extends Component {
  state = {
    activateShare: false,
    editMode: false,
    audioPlayer: null,
    directCoverModal: {},
    progress: -1,
    account: null,
    pageItems: null,
  };

  passwordWallRef = React.createRef();

  componentDidMount() {
    this.passwordWallRef.current && this.passwordWallRef.current.close();
    promptRef.current && promptRef.current.close();

    customPageHandler.subscribe(this.update.bind(this));
    this.showInstructionIfNeeded();

    TrackPlayer.getState().then((state) => {
      this.trackStateChange();
    });
    subscribe("state", this.trackStateChange.bind(this));

    // progress feature
    let shouldUseProgressBar = false;
    const keywords = Config.enablingKeywordsForProgressFeature;
    for (let k in keywords) {
      const kw = keywords[k];
      if (this.props.name.toLowerCase().includes(kw))
        shouldUseProgressBar = true;
    }
    this.shouldUseProgressBar = shouldUseProgressBar;

    Backend.firestoreHandler.account(this.account.bind(this));
    Backend.firebasePages.pages(this.pages.bind(this));
  }

  account(account) {
    const pageName = this.props.name;
    if (this.shouldUseProgressBar) {
      const progress = Backend.levelsHandler.calculateProgressForLevel(
        pageName
      );
      this.setState({ progress, account });
    } else {
      this.setState({ account });
    }
  }
  async pages() {
    const { name } = this.props;
    const page = Backend.firebasePages.getPageWithName(name);
    if (!page) return;
    this.setState({ pageItems: page.content });
    const savedPassword = await Backend.localHandler.getSavedPasswordForPage(
      name
    );
    if (
      page.mediaItem.team !== true &&
      page.password &&
      `${page.password}`.length > 1 &&
      savedPassword !== `${page.password}`
    ) {
      this.passwordWallRef.current && this.passwordWallRef.current.open();
    } else {
      this.passwordWallRef.current && this.passwordWallRef.current.close();
    }
  }
  async showInstructionIfNeeded() {
    const didOpen = await Backend.localHandler.getCustomPageDidOpen();
    if (!didOpen) {
      this.setState({
        directCoverModal: {
          startY: isIphoneX() ? 43 : 0,
          endY: 127,
          startX: screenWidth - (Platform.isPad ? 90 : 67),
          endX: screenWidth,
        },
      });
      Backend.localHandler.setCustomPageDidOpen();
    }
  }

  update(data) {
    const { activateShare } = data;
    this.setState({ activateShare });
  }

  trackStateChange() {
    TrackPlayer.getCurrentTrack().then((track) => {
      if (!track) {
        this.setState({ audioPlayer: null });
      } else {
        this.setState({
          audioPlayer: (
            <Player onAutoComplete={this.audioAutoComplete.bind(this)} />
          ),
        });
      }
    });
  }

  //top right share gets turned on
  sharePress() {
    customPageHandler.activateShareMode(!this.state.activateShare);
  }
  //an item gets selected or unselected
  shareSelected(data) {
    const { selected } = data;
    if (selected === true)
      customPageHandler.addShareItem(this.props.name, data);
    else customPageHandler.removeShareItem(this.props.name, data);
  }
  shareCancelled() {
    customPageHandler.activateShareMode(false);
  }
  share() {
    this.shareModal.present();
  }
  shareAction(index) {
    const { EMAIL, APP_CONTACTS, SOCIAL_MEDIA } = ShareOptions;

    const html = index == EMAIL;
    const message = customPageHandler.shareItemsAsMessage(html);

    if (index == EMAIL) {
      Share.shareSingle({
        subject: `${Backend.firestoreHandler._config?.variables.appTitle} - Shared Content`,
        message,
        social: "email",
      });
    } else if (index == SOCIAL_MEDIA) {
      Share.open({
        subject: "",
        message,
      });
    } else if (index == APP_CONTACTS) {
      const allLists = Backend.listBuilderHandler.extractLists();
      let contactsCount = 0;
      let buildMyListData;
      for (l in allLists) {
        const listTitle = allLists[l].title;
        if (listTitle === "Build My List" || listTitle === "Memory Jogger") {
          buildMyListData = allLists[l];
          continue;
        }
        const listContacts = Backend.listBuilderHandler.extractContacts(
          listTitle
        );
        contactsCount += listContacts.length;
      }
      if (contactsCount > 0 || !buildMyListData)
        this.props.navigation.navigate("ShareToList");
      else
        this.props.navigation.navigate("ListScreen", {
          listData: buildMyListData,
          shareMode: true,
        });
    }
  }

  itemPressed(data) {
    const { editMode } = this.state;
    const pageName = this.props.name;

    if (editMode) {
      alert("Editing: " + data.title);
      return;
    }

    console.log("topage: "+data.topage)
    if (data.topage) {
      customPageHandler.presentPage(data.topage);
      return;
    }

    if (
      data.media &&
      (data.media.includes("dropbox") ||
        data.media.includes("drive.google.com"))
    ) {
      this.lastPlayedAudioItem = data;
    }

    if (this.progressBar) {
      if (!Backend.levelsHandler.isCompletableItem(data)) return;
      const levels = Backend.firestoreHandler._account.levels;
      if (!levels) return;
      const active =
        levels &&
        levels[pageName] &&
        levels[pageName][data.id] &&
        levels[pageName][data.id].complete;
      if (!this.state.activateShare) {
        this.progressBar.showCheckbox(data, active);
      }
    }
  }

  progressBarChecked(itemData) {
    if (!this.shouldUseProgressBar) return;
    this.progressBar.showCheckbox(itemData, true);
    Backend.levelsHandler.updateLevelItem(this.props.name, itemData.id, true);
  }

  audioAutoComplete() {
    // an audio reached 0.9+ and should auto complete the level item if needed
    if (!this.shouldUseProgressBar) return;

    if (this.lastPlayedAudioItem && this.progressBar) {
      if (this.progressBar.itemData.title === this.lastPlayedAudioItem.title)
        this.progressBarChecked(this.lastPlayedAudioItem);
      this.lastPlayedAudioItem = null;
    }
  }

  hideDirectCoverModal() {
    this.setState({ directCoverModal: {} });
  }

  itemMeasure(item, measurements) {
    const scrollToItem = this.props.navigation.getParam("scrollToItem");
    if (!scrollToItem || item.title !== scrollToItem) return;
    const { y } = measurements;
    this.scrollRef.scrollTo({ x: 0, y, animated: false });
  }

  editToggle(status) {
    this.setState({ editMode: status });
  }

  async itemCoverPress(item) {
    const pageName = this.props.name;
    const page = Backend.firebasePages.getPageWithName(pageName);
    if (this.movingItem) {
      /**
       * Move an item's order of layout on the screen :D
       */
      let newPosition = item.position;
      for (let i = 0; i < page.content.length; i++) {
        const _item = page.content[i];
        if (_item.id == item.id) {
          const _previousItem = page.content[i - 1];
          let _previousPosition =
            i === 0 ? page.content[0].position - 1 : _previousItem.position;
          if (_item.position === _previousPosition) {
            _previousPosition -= 0.001;
            await Backend.firebasePages.updatePageContent(pageName, {
              ..._previousItem,
              position: _previousPosition,
            });
          }
          newPosition = median([_item.position, _previousPosition]);
          break;
        }
      }
      console.log("new item position: " + newPosition);
      await Backend.firebasePages
        .updatePageContent(pageName, {
          ...this.movingItem,
          position: newPosition,
        })
        .then(() => {
          Toasty({ text1: "Move complete." });
        })
        .catch(() => uhOh());
      this.movingItem = null;
      return;
    }
    const title = `${
      item.paragraph ? item.paragraph : item.title ? item.title : ""
    }`;
    const quotedTitle = `"${title}"`;
    Alert.alert("Choose an option.", quotedTitle, [
      {
        text: `Edit ${item.paragraph ? "Text" : "Title"}`,
        onPress: () => {
          prompt({
            // title,
            defaultValue: title,
            onSubmit: (text) => {
              console.log("TEXT: " + text);
              loading();
              const updatedItem = { ...item };
              if (updatedItem.paragraph) updatedItem.paragraph = text;
              else updatedItem.title = text;
              Backend.firebasePages
                .updatePageContent(pageName, updatedItem)
                .then(() => success("Title updated!"))
                .catch(() => uhOh());
            },
          });
        },
      },
      {
        text: "Adjust position",
        onPress: () => {
          this.movingItem = item;
          Alert.alert("Tap an item you want to place this item on top of.");
        },
      },
      {
        text: "Delete",
        onPress: () => {
          Alert.alert(
            "Confirmation",
            `Are you sure you want to delete this item from your page?\n${quotedTitle}`,
            [
              {
                text: "No",
                style: "cancel",
              },
              {
                text: "Yes",
                onPress: () => {
                  loading("Deleting item...");
                  Backend.firebasePages
                    .updatePageContent(pageName, item, {
                      remove: true,
                    })
                    .then(() => {
                      success("Item deleted.");
                      LayoutAnimation.configureNext(defaultLayoutAnimation);
                    })
                    .catch(() => uhOh());
                },
              },
            ]
          );
        },
      },
      {
        text: "Cancel",
        style: "cancel",
      },
    ]);
  }

  leaveTeam(xong) {
    const subtext =
      Backend.firestoreHandler._account.team.role === eTeamRole.Owner
        ? "As the team leader, we do not recommend leaving your team!"
        : "";
    Alert.alert(
      "Final check",
      `Are you sure you want to leave your team?\n${subtext}`,

      [
        {
          text: "Cancel",
          style: "cancel",
          onPress: () => {
            xong();
          },
        },
        {
          text: "Yes",
          onPress: () => {
            Toasty({
              text1: "Leaving team...",
              type: "info",
              visibilityTime: 10000,
            });
            Backend.notificationHandler.leaveTeam(
              Backend.firestoreHandler._account.team.teamName
            );
            Backend.firestoreHandler.updateAccount("", {
              team: null,
            });
            xong();
          },
        },
      ]
    );
  }

  render() {
    const { directCoverModal, account, pageItems, editMode } = this.state;

    const pageName = this.props.name;
    let shouldUseProgressBar = false;

    const canEditPage =
      account?.admin ||
      (account?.team?.teamName === pageName &&
        account?.team?.role == eTeamRole.Owner);

    const keywords = Config.enablingKeywordsForProgressFeature;
    for (let k in keywords) {
      const kw = keywords[k];
      if (pageName.toLowerCase().includes(kw)) shouldUseProgressBar = true;
    }

    const lbConfig = Backend.listBuilderHandler._config;

    let itemsData = pageItems ? pageItems : [];
    const items = [];
    for (let i in itemsData) {
      const iData = itemsData[i];
      if (
        iData.topage &&
        (iData.topage.includes("Memory Jogger") ||
          iData.topage.includes("List Builder") ||
          iData.topage.includes("Build My List"))
      ) {
        const memoryJoggerTitle =
          lbConfig && lbConfig.memoryJogger && lbConfig.memoryJogger.title
            ? lbConfig.memoryJogger.title
            : "Build My List";
        items.push(
          <MemoryJoggerEntryButton
            key="item.memoryjogger"
            navigation={this.props.navigation}
            title={memoryJoggerTitle}
          />
        );
        continue;
      }
      if (!iData.media && !iData.url && !iData.paragraph && !iData.title)
        continue;
      let item = (
        <Item
          key={`item.${pageName}.${iData.id}`}
          onPress={this.itemPressed.bind(this)}
          coverPress={editMode ? this.itemCoverPress.bind(this) : null}
          coverStyle={{
            borderRadius: 10,
            backgroundColor: "rgba(3, 69, 252, 0.18)",
            borderWidth: 0.8,
            borderColor: "rgba(3, 69, 252, 0.8)",
          }}
          pageName={pageName}
          item={iData}
          onShareSelect={this.shareSelected.bind(this)}
          onAutoComplete={this.progressBarChecked.bind(this)}
          navigation={this.props.navigation}
          onMeasure={this.itemMeasure.bind(this)}
        />
      );
      items.push(item);
    }

    const isMyTeamPage = account?.team?.teamName === pageName;

    return (
      <View
        style={{
          flex: 1,
          // backgroundColor: Colors.darkGrey,
          backgroundColor: C.background01,
          ...(this.props.containerStyle ? this.props.containerStyle : {}),
        }}
      >
        <ScrollView
          ref={(ref) => (this.scrollRef = ref)}
          style={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-evenly",
          }}
        >
          {this.state.progress > -1 ? (
            <View style={{ width: "100%", height: moderateScale(40) }} />
          ) : null}
          {items}
          {this.props.children}

          {canEditPage ? (
            <$_EditPage
              {...this.props}
              onEditToggle={this.editToggle.bind(this)}
            />
          ) : null}

          {isMyTeamPage ? (
            <Buttoon
              style={{
                marginTop: spacing(3),
              }}
              appearance="ghost"
              status="danger"
              progress
              // icon={{ name: "arrow_left" }}
              onPress={(xong) => this.leaveTeam(xong)}
            >
              Leave Team
            </Buttoon>
          ) : null}

          <View
            style={{
              width: "100%",
              height:
                this.state.activateShare === true ? verticalScale(90) : 30,
            }}
          />
          <View
            style={{
              width: "100%",
              height: this.state.audioPlayer
                ? PlayerStyles.container.height
                : 0,
            }}
          />
        </ScrollView>

        <SS_Prompt ref={promptRef} />

        {shouldUseProgressBar === true ? (
          <ProgressBar
            ref={(ref) => (this.progressBar = ref)}
            completion={this.state.progress}
            onCheck={this.progressBarChecked.bind(this)}
          />
        ) : null}

        {this.state.activateShare === true ? (
          <ShareBar
            cancelled={this.shareCancelled.bind(this)}
            sharePress={this.share.bind(this)}
          />
        ) : this.state.audioPlayer ? (
          this.state.audioPlayer
        ) : null}

        <ShareModal
          ref={(ref) => (this.shareModal = ref)}
          onExistingListPress={() =>
            this.shareAction(ShareOptions.APP_CONTACTS)
          }
          onEmailPress={() => this.shareAction(ShareOptions.EMAIL)}
          onTextPress={() => this.shareAction(ShareOptions.SOCIAL_MEDIA)}
        />

        <DirectCoverModal
          text={"\n\nNote:\n\nYou can tap the \nshare icon to share\nmedia."}
          donePress={this.hideDirectCoverModal.bind(this)}
          visible={directCoverModal.endY ? true : false}
          startY={directCoverModal.startY}
          endY={directCoverModal.endY}
          startX={directCoverModal.startX}
          endX={directCoverModal.endX}
        />

        <SS_PasswordWall
          {...this.props}
          ref={this.passwordWallRef}
          pageName={pageName}
        />
      </View>
    );
  }
}

function inputPrompt(title, subtitle = "", submit, delay = 0) {
  setTimeout(() => {
    prompt({
      title,
      subtitle: subtitle,
      onSubmit: submit,
    });
  }, delay);
}
function prompt(_options) {
  const options = {
    title: "",
    subtitle: "",
    defaultValue: "",
    submitText: "Submit",
    onSubmit: (value) => {
      console.log("entered value:" + value);
    },
    onCancel: () => {},
    ..._options,
  };
  promptRef.current?.openWithOptions(options);
}

function uhOh() {
  Toasty({
    type: "error",
    text1:
      "Something went wrong. Please check your internet connection and try again.",
  });
}
function loading(text = "Updating...") {
  Toasty({
    type: "info",
    text1: text,
  });
}
function success(text = "Update complete!") {
  Toasty({
    text1: text,
  });
}

function $_EditPage(props) {
  const { name, onEditToggle } = props;
  const pageName = name;

  const account = Backend.firestoreHandler._account;
  const teamPage = account ? Backend.firebasePages.getMyTeamPage() : null;

  const [progress, setProgress] = React.useState(-1);

  async function addItemToFirebasePage(item) {
    try {
      await Backend.firebasePages.updatePageContent(pageName, item, {
        add: true,
      });
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      Toasty({ text1: "Item added." });
      return;
    } catch (error) {
      uhOh();
      console.log(error);
      return error;
    }
  }

  async function uploadVideo({ title, folder }, progressCallback) {
    try {
      const videoInfo = await Backend.FileNetwork.selectAndUpload(
        { title, type: "video", folder },
        progressCallback
      );
      if (!videoInfo || !videoInfo.uri) {
        console.log("CustomPage: NO videoURL returned =(");
        return;
      }
      await addItemToFirebasePage({
        title,
        media: videoInfo.uri,
        // media: FileNetwork.shareLinkWithFileId(fileId),
      });
      progressCallback(-1);
      return;
    } catch (error) {
      console.log(error);
      Toasty({
        type: "error",
        text1: "Something went wrong.",
        text2:
          "Please make sure you have permisions to your device's photo library enabled in your settings for this app.",
      });
    }
  }

  async function addItemSelected(index) {
    if (index === 0) {
      // title
      inputPrompt("Enter Title:", "", (title) => {
        addItemToFirebasePage({ title });
      });
    } else if (index === 1) {
      // paragraph
      inputPrompt("Enter Message:", "", (message) => {
        addItemToFirebasePage({ paragraph: message });
      });
    } else if (index === 2) {
      // webpage URL
      inputPrompt("Enter a title for your Webpage:", "", (title) => {
        // setTimeout(() => {
        if (!title) {
          Alert.alert("Please provide some text first.");
          return;
        }
        inputPrompt(
          "Paste a URL:",
          "",
          (_url) => {
            // if (!title || !url) {
            //   Alert.alert("Please provide a title and URL to add this item.");
            //   return;
            // }
            const url = _url && _url.length > 1 ? _url : "  ";
            addItemToFirebasePage({ title, url: url.length > 1 ? url : "  " });
          },
          350
        );
        // }, 350);
      });
    } else if (index === 3) {
      // upload a video
      Alert.alert(
        "Where would you like to upload your file from?",
        "Choose a method.",
        [
          {
            text: `From my ${
              Platform.OS === "ios"
                ? Platform.isPad
                  ? "iPad"
                  : "iPhone"
                : Platform.isPad
                ? "Tablet"
                : "Android"
            }`,
            onPress: () => {
              // upload video
              inputPrompt("Video Title:", "", (title) => {
                const app_name = Backend.firestoreHandler._config?.variables.appTitle.replace(
                  / /g,
                  ""
                );
                let folder = `clients/${app_name}`;
                if (teamPage?.name === pageName)
                  folder = `clients/${app_name}/subteams/${account.team.teamName}`;

                uploadVideo({ title, folder }, (progress) => {
                  LayoutAnimation.configureNext(defaultLayoutAnimation);
                  setProgress(progress);
                });
              });
            },
          },
          // {
          //   text: "From my Computer",
          // },
          {
            text: "Insert Supported Link",
            onPress: () => {
              inputPrompt("Video Title:", "", (title) => {
                inputPrompt(
                  "Paste Share Link:",
                  "If your share link is not properly setup or supported it will not show up.",
                  (media) => {
                    addItemToFirebasePage({ title, media });
                  },
                  350
                );
              });
            },
          },
          {
            text: "Cancel",
            style: "cancel",
          },
        ]
      );
    }
  }

  const addItemActionSheet = React.useRef(null);
  function addItem() {
    addItemActionSheet.current.show();
  }

  const isEditing = React.useRef(false);
  function edit() {
    isEditing.current = !isEditing.current;
    onEditToggle(isEditing.current);
  }

  return (
    <>
      <View
        style={{
          width: "90%",
          marginVertical: spacing(5),
          alignItems: "center",
          overflow: "hidden",
        }}
      >
        {progress > -1 ? (
          <View
            style={{
              width: "90%",
              height: 5,
              borderRadius: 10,
              backgroundColor: "gray",
              marginBottom: spacing(4),
            }}
          >
            <View
              style={{
                position: "absolute",
                height: 5,
                left: 0,
                width: `${progress < 8 ? 7 : progress}%`,
                backgroundColor: C.awakenVolt,
              }}
            />
          </View>
        ) : null}
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Buttoon
            appearance="outline"
            onPress={edit}
            style={{ width: "48.4%" }}
            status={isEditing.current ? "success" : "primary"}
            icon={isEditing.current ? { name: "check" } : { name: "pen" }}
          >
            {isEditing.current ? "Done" : "Edit Page"}
          </Buttoon>
          <Buttoon
            onPress={addItem}
            style={{ width: "48.4%" }}
            icon={{ name: "plus" }}
          >
            Add item
          </Buttoon>
        </View>
      </View>
      <ActionSheet
        ref={(ref) => (addItemActionSheet.current = ref)}
        options={[
          "Add a Title",
          "Add a Message",
          "Add a Webpage",
          "Add a Video",
          "Cancel",
        ]}
        cancelButtonIndex={4}
        onPress={addItemSelected}
      />
    </>
  );
}
