import { TopNavigation } from "@ui-kitten/components";
import { Buttoon, IconPrimr, Txt } from "components/";
import { removePageExtensions } from "constant-deprecated/Config";
import React from "react";
import { Platform, View } from "react-native";
import { C, moderateScale, spacing } from "utilities/";

interface d {
  pageName: string;
  onSharePress: () => void;
  navigation: any;
}

/**
 * ### The top navigation intended for CustomPage
 *  @version 21.03.25
 *  @author jm_francis
 *
 **/
export function $_Header(props: d) {
  const { pageName, onSharePress, navigation } = props;
  return (
    <TopNavigation
      style={{
        backgroundColor: C.background01,
      }}
      alignment="center"
      title={(evaProps) => (
        <Txt.H6
          {...evaProps}
          numberOfLines={2}
          style={{
            width: "75%",
            textAlign: "center",
          }}
        >
          {removePageExtensions(pageName)}
        </Txt.H6>
      )}
      accessoryLeft={(p) => (
        <Buttoon
          onPress={() => navigation.pop()}
          style={{ backgroundColor: "transparent", borderWidth: 0 }}
          icon={{
            name: "arrow_left",
            color: C.text01,
            size: moderateScale(20),
          }}
        />
      )}
      accessoryRight={() => (
        <ShareButton
          onPress={onSharePress}
          style={{
            marginTop: spacing(1) + 7,
            marginLeft: spacing(2),
          }}
        />
      )}
    />
  );
}

function ShareButton(props: { style?: any; onPress: () => void }) {
  const { style = {}, onPress } = props;
  const dimension = moderateScale(38, 0.33);
  return (
    <View
      style={{
        width: dimension,
        height: dimension,
        marginRight: 12,
        ...style,
      }}
    >
      <IconPrimr
        name="content_share"
        size={moderateScale(Platform.isPad ? 23 : 24)}
        color="white"
        onPress={onPress}
      />
    </View>
  );
}
