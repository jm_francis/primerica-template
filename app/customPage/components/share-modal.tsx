import { Animations as _Animations } from "animation";
import K from "constant-deprecated/";
import { MAIL, PROFILE, TEXT_MESSAGE } from "constant-deprecated/Images";
import React, { Component } from "react";
import {
  Animated,
  Dimensions,
  Image,
  Modal,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { getBottomSpace } from "react-native-iphone-x-helper";
import { moderateScale } from "utilities/";

interface PropTypes {
  onExistingListPress: () => void;
  onEmailPress: () => void;
  onTextPress: () => void;
}

export default class ShareModal extends Component<PropTypes> {
  animations = new Animations();

  state = {
    visible: false,
  };

  present() {
    this.setState({ visible: true }, () => {
      this.animations.present().start();
    });
  }
  dismiss(callback) {
    this.animations.dismiss().start(callback);
    setTimeout(() => {
      this.setState({ visible: false });
    }, 277);
  }

  onExistingListPress() {
    const { onExistingListPress } = this.props;
    this.dismiss(() => {
      setTimeout(onExistingListPress, 50);
    });
  }
  onEmailPress() {
    const { onEmailPress } = this.props;
    this.dismiss();
    onEmailPress();
  }
  onTextPress() {
    const { onTextPress } = this.props;
    this.dismiss(() => {
      setTimeout(onTextPress, 250);
    });
  }

  render() {
    const { visible } = this.state;

    return (
      <Modal visible={visible} animationType="fade" transparent={true}>
        <View style={Styles.container}>
          <TouchableWithoutFeedback
            onPress={this.dismiss.bind(this)}
            style={{ width: screenWidth, height: screenHeight }}
          />
          <Animated.View
            style={{
              ...Styles.optionsContainer,
              transform: [
                {
                  translateY: this.animations.create(
                    "y",
                    Styles.optionsContainer.height
                  ),
                },
              ],
            }}
          >
            <Option
              title="Existing List"
              icon={PROFILE}
              onPress={this.onExistingListPress.bind(this)}
            />
            <Option
              title="Send Email"
              icon={MAIL}
              onPress={this.onEmailPress.bind(this)}
            />
            <Option
              title="Text & Share"
              icon={TEXT_MESSAGE}
              onPress={this.onTextPress.bind(this)}
            />
            <Option title="Cancel" onPress={() => this.dismiss()} cancel />
          </Animated.View>
        </View>
      </Modal>
    );
  }
}

function Option(props) {
  const { title, icon, onPress, cancel } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        ...Styles.optionContainer,
        ...(cancel ? Styles.optionContainerCancel : {}),
      }}
    >
      <View
        style={{
          ...Styles.optionContainer,
          ...(cancel ? Styles.optionContainerCancel : {}),
        }}
      >
        {icon ? <Image style={Styles.optionIcon} source={icon} /> : null}
        <Text
          style={{
            ...Styles.optionTitle,
            ...(cancel ? Styles.optionTitleCancel : {}),
          }}
        >
          {title}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

class Animations extends _Animations {
  present() {
    return this.spring("y", {
      toValue: 0,
      useNativeDriver: true,
    });
  }
  dismiss() {
    if (Platform.OS === "android") {
      return this.timing("y", {
        duration: 90,
        toInitialValue: true,
        useNativeDriver: true,
      });
    }
    return this.spring("y", {
      toInitialValue: true,
      useNativeDriver: true,
    });
  }
}

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

const Styles = {
  container: {
    flex: 1,
    backgroundColor: "rgba(7,7,7,0.55)",
  },
  background: {
    top: 0,
    position: "absolute",
    width: screenWidth,
    height: screenHeight,
  },
  optionsContainer: {
    overflow: "hidden",
    position: "absolute",
    bottom: 0,
    width: screenWidth,
    height: moderateScale(260 + getBottomSpace() / 1.5),
    marginBottom: getBottomSpace() / 1.5,
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  optionContainer: {
    width: "97%",
    flexDirection: "row",
    alignItems: "center",
    height: moderateScale(57),
    borderRadius: 10,
    backgroundColor: "white",
    paddingLeft: 17,
  },
  optionContainerCancel: {
    justifyContent: "center",
    paddingLeft: 0,
  },
  optionIcon: {
    tintColor: "#1d49c2",
    width: moderateScale(27),
    height: moderateScale(27),
    marginRight: 30,
  },
  optionTitle: {
    color: "#1d49c2",
    fontSize: moderateScale(23),
    fontFamily: K.Fonts.medium,
  },
  optionTitleCancel: {
    color: "red",
  },
};
