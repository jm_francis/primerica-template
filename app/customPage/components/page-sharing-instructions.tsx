import { DirectCoverModal } from "components/";
import React from "react";
import { Dimensions, Platform } from "react-native";
import { isIphoneX } from "utilities/";

/**
 * ### Teaches the new user how to use the sharing feature
 *  - Ghosts out the entire screen and points to the top right where the share icon is and explains what that button does!
 *  ----
 *  @example 
 *  <PageSharingInstructions
        visible={showInstructions}
        onDonePress={() => setShowInstructions(false)}
      />
 *  ----
 *  @version 21.03.24
 *  @author jm_francis
 *  
 **/
export function PageSharingInstructions(props: {
  visible: boolean;
  onDonePress: () => void;
}) {
  const { visible, onDonePress } = props;
  const screenWidth = Dimensions.get("window").width;
  return (
    <DirectCoverModal
      text={"\n\nNote:\n\nYou can tap the \nshare icon to share\nmedia."}
      donePress={onDonePress}
      visible={visible}
      startY={isIphoneX() ? 43 : 0}
      endY={127}
      startX={screenWidth - (Platform.isPad ? 90 : 67)}
      endX={screenWidth}
    />
  );
}
