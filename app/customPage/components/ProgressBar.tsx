// import { Animations } from "animation/";
import Backend, { dMediaPageItem, MediaPageSchema } from "backend/";
import { Txt } from "components/";
import { Fonts } from "constant-deprecated/";
import { CHECK } from "constant-deprecated/Images";
import React from "react";
import {
  Dimensions,
  Image,
  LayoutAnimation,
  TouchableOpacity,
  UIManager,
  View,
} from "react-native";
import { moderateScale } from "utilities/";

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

interface dProgressBar {
  page: MediaPageSchema;
  /**
   * How much of the progress bar to fill (0.0 - 1.0)
   */
  completion: number;
  /**
   * The media item that is currently being played or viewed (and can be marked as complete)
   */
  checkboxItem?: dMediaPageItem;
}
/**
 * ### The progress bar that displays the completion of the currently opened level page
 *  ----
 *  @example
 *  <ProgressBar
      page={_page}
      completion={progress}
      checkboxItem={someItem}
    />
 *  ----
 *  @version 21.03.24
 *  @author jm_francis
 *
 **/
export default function ProgressBar(props: dProgressBar) {
  const { page, checkboxItem, completion } = props;

  const [_checkboxItem, setCheckboxItem] = React.useState<dMediaPageItem>(null);
  const [_completion, setCompletion] = React.useState<number>(0);

  const isChecked = Backend.levelsHandler.isItemComplete(page, _checkboxItem);
  const didPageOpen = React.useRef<boolean>(false);

  // animate the progress bar when the completion number changes
  React.useEffect(
    function completionChange() {
      // completion > -1 && LayoutAnimation.configureNext(layoutAnimationConfig);
      didPageOpen.current &&
        LayoutAnimation.configureNext(layoutAnimationConfig);
      setCheckboxItem(checkboxItem);
      setCompletion(completion);
      didPageOpen.current = true;
    },
    [completion, checkboxItem]
  );

  function onCheck(active: boolean) {
    page &&
      _checkboxItem &&
      Backend.levelsHandler.updateLevelItem(
        page.name,
        _checkboxItem.id,
        active
      );
  }

  return (
    <View>
      {/* <Animated.View
        style={{
          ...Styles.container,
          transform: [
            {
              translateY: animations.create(
                "containerY",
                -Styles.checkboxView.height
              ),
            },
          ],
        }}
      > */}
      <View
        style={{
          ...Styles.checkboxView,
          marginTop: _checkboxItem ? 0 : -Styles.checkboxView.height,
          ...(_checkboxItem ? {} : { opacity: 0 }),
        }}
      >
        <Txt.S1
          allowFontScaling
          adjustsFontSizeToFit
          numberOfLines={1}
          style={{
            ...Styles.checkboxTitle,
            ...(isChecked ? Styles.checkboxTitleActive : {}),
          }}
        >
          {_checkboxItem && _checkboxItem.title}
        </Txt.S1>
        <Checkbox onCheck={onCheck} active={isChecked} />
      </View>
      <View
        style={{
          ...Styles.progress,
          backgroundColor: "gray",
        }}
      >
        <View
          style={{
            ...Styles.progress,
            width: screenWidth * _completion,
          }}
        />
      </View>
      {/* </Animated.View> */}
    </View>
  );
}

/**
 * ### A basic checkbox
 *  @version 21.03.24
 *  @author jm_francis
 *
 **/
function Checkbox(props: {
  active: boolean;
  onCheck: (active: boolean) => void;
}) {
  const { active = false, onCheck } = props;
  return (
    <TouchableOpacity onPress={() => onCheck(true)}>
      <View
        style={{
          ...Styles.checkboxImageWrapper,
          ...(active ? Styles.checkboxImageWrapperActive : {}),
        }}
      >
        <Image
          style={{
            ...Styles.checkboxImage,
            ...(active ? Styles.checkboxImageActive : {}),
          }}
          source={CHECK}
        />
      </View>
    </TouchableOpacity>
  );
}

const screenWidth = Dimensions.get("window").width;

const Styles = {
  container: {
    zIndex: 10,
    position: "absolute",
    top: 0,
    width: "100%",
    backgroundColor: "grey",
  },
  progress: {
    backgroundColor: "#49f58b",
    height: moderateScale(4.5),
    left: 0,
  },
  checkboxView: {
    backgroundColor: "#345d78",
    height: moderateScale(40),
    width: screenWidth,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 16,
  },
  checkboxImageWrapper: {
    borderRadius: 7,
    borderWidth: 2,
    borderColor: "rgb(188,188,188)",
    backgroundColor: "rgba(180,180,180,0.125)",
    width: moderateScale(30),
    height: moderateScale(30),
    justifyContent: "center",
    alignItems: "center",
  },
  checkboxImageWrapperActive: {
    backgroundColor: "#1ed46d",
    borderColor: "#1ed46d",
  },
  checkboxImage: {
    width: "70%",
    height: "70%",
    tintColor: "rgb(188,188,188)",
  },
  checkboxImageActive: {
    tintColor: "white",
  },
  checkboxTitle: {
    width: "94%",
    paddingRight: 7,
    color: "rgb(220,220,220)",
    fontFamily: Fonts.medium,
    fontSize: moderateScale(18),
  },
  checkboxTitleActive: {
    color: "white",
  },
};

// class ProgressBarAnimations extends Animations {
//   showCheckboxView() {
//     return this.spring("containerY", {
//       toValue: 0,
//       tension: 10,
//       friction: 10,
//       useNativeDriver: true,
//     });
//   }
//   hideCheckboxView() {
//     return this.spring("containerY", {
//       toValue: -Styles.checkboxView.height,
//       tension: 10,
//       friction: 10,
//       useNativeDriver: true,
//     });
//   }
// }

const spring = {
  type: "spring",
  springDamping: 0.65,
};
const layoutAnimationConfig = {
  duration: 600,
  create: {
    ...spring,
    property: "scaleXY",
  },
  update: {
    ...spring,
  },
  delete: {
    ...spring,
    property: "scaleXY",
  },
};
