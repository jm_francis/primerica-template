import Backend, { dMediaPageItem } from "backend/";
import { Buttoon, Toasty } from "components/";
import { defaultLayoutAnimation } from "pages/listBuilder/configure";
import React from "react";
import { Alert, LayoutAnimation, Platform, View } from "react-native";
import { C, spacing } from "utilities/";
import ActionSheet from "react-native-actionsheet";
import { dOptions, SS_Prompt } from "../sh-prompts/sh-prompt";

interface dProps {
  pageName: string;
  /**
   * Whether or not edit mode is active
   */
  active: boolean;
  /**
   * When the "Edit" button is pressed, or "Done" when already in edit mode
   */
  onEditToggle: () => void;
}

/**
 * ### The options to edit the page
 *  - Detailed explanation (if any)
 *  ----
 *  @example 
 * <$_EditPage
    pageName={name}
    active={editMode}
    onEditToggle={() => setEditMode(!editMode)}
    />
 *  ----
 *  @version 21.03.25
 *  @author  jm_francis
 **/
export function $_EditPage(props: dProps) {
  const { pageName, active, onEditToggle } = props;

  // #region variables
  const account = Backend.firestoreHandler._account;
  const teamPage = account ? Backend.firebasePages.getMyTeamPage() : null;
  // #endregion

  // #region state
  const [progress, setProgress] = React.useState(-1);
  // #endregion

  // #region refs
  const promptRef = React.useRef<any>(null);
  const addItemActionSheet = React.useRef(null);
  // #endregion

  function addItem() {
    addItemActionSheet.current.show();
  }
  function edit() {
    onEditToggle();
  }

  // #region prompt functions
  function inputPrompt(
    title: string,
    subtitle = "",
    submit: (value: string) => void,
    delay = 0
  ) {
    setTimeout(() => {
      prompt({
        title,
        subtitle: subtitle,
        onSubmit: submit,
        onCancel: () => {},
      });
    }, delay);
  }
  function prompt(_options: dOptions) {
    const options = {
      title: "",
      subtitle: "",
      defaultValue: "",
      submitText: "Submit",
      onSubmit: (value: string) => {
        console.log("entered value:" + value);
      },
      onCancel: () => {},
      ..._options,
    };
    promptRef.current?.openWithOptions(options);
  }
  function uhOh() {
    Toasty({
      type: "error",
      text1:
        "Something went wrong. Please check your internet connection and try again.",
    });
  }
  // #endregion

  async function addItemToFirebasePage(item: Partial<dMediaPageItem>) {
    try {
      await Backend.firebasePages.updatePageContent(pageName, item, {
        add: true,
      });
      LayoutAnimation.configureNext(defaultLayoutAnimation);
      Toasty({ text1: "Item added." });
      return;
    } catch (error) {
      uhOh();
      console.log(error);
      return error;
    }
  }

  async function uploadVideo(
    { title, folder },
    progressCallback: (progress: number) => void
  ) {
    try {
      const videoInfo = await Backend.FileNetwork.selectAndUpload(
        { title, type: "video", folder },
        progressCallback
      );
      if (!videoInfo || !videoInfo.uri) {
        console.log("CustomPage: NO videoURL returned =(");
        return;
      }
      await addItemToFirebasePage({
        title,
        media: videoInfo.uri,
        // media: FileNetwork.shareLinkWithFileId(fileId),
      });
      progressCallback(-1);
      return;
    } catch (error) {
      console.log(error);
      Toasty({
        type: "error",
        text1: "Something went wrong.",
        text2:
          "Please make sure you have permisions to your device's photo library enabled in your settings for this app.",
      });
    }
  }

  async function addItemSelected(index: number) {
    if (index === 0) {
      // title
      inputPrompt("Enter Title:", "", (title: string) => {
        addItemToFirebasePage({ title });
      });
    } else if (index === 1) {
      // paragraph
      inputPrompt("Enter Message:", "", (message: string) => {
        addItemToFirebasePage({ paragraph: message });
      });
    } else if (index === 2) {
      // webpage URL
      inputPrompt("Enter a title for your Webpage:", "", (title: string) => {
        // setTimeout(() => {
        if (!title) {
          Alert.alert("Please provide some text first.");
          return;
        }
        inputPrompt(
          "Paste a URL:",
          "",
          (_url: string) => {
            // if (!title || !url) {
            //   Alert.alert("Please provide a title and URL to add this item.");
            //   return;
            // }
            const url = _url && _url.length > 1 ? _url : "  ";
            addItemToFirebasePage({ title, url: url.length > 1 ? url : "  " });
          },
          350
        );
        // }, 350);
      });
    } else if (index === 3) {
      // upload a video
      Alert.alert(
        "Where would you like to upload your file from?",
        "Choose a method.",
        [
          {
            text: `From my ${
              Platform.OS === "ios"
                ? Platform.isPad
                  ? "iPad"
                  : "iPhone"
                : Platform.isPad
                ? "Tablet"
                : "Android"
            }`,
            onPress: () => {
              // upload video
              inputPrompt("Video Title:", "", (title) => {
                const app_name = Backend.firestoreHandler._config?.variables.appTitle.replace(
                  / /g,
                  ""
                );
                let folder = `clients/${app_name}`;
                if (teamPage?.name === pageName)
                  folder = `clients/${app_name}/subteams/${account.team.teamName}`;

                uploadVideo({ title, folder }, (progress) => {
                  LayoutAnimation.configureNext(defaultLayoutAnimation);
                  setProgress(progress);
                });
              });
            },
          },
          // {
          //   text: "From my Computer",
          // },
          {
            text: "Insert Supported Link",
            onPress: () => {
              inputPrompt("Video Title:", "", (title: string) => {
                inputPrompt(
                  "Paste Share Link:",
                  "If your share link is not properly setup or supported it will not show up.",
                  (media: string) => {
                    addItemToFirebasePage({ title, media });
                  },
                  350
                );
              });
            },
          },
          {
            text: "Cancel",
            style: "cancel",
          },
        ]
      );
    }
  }

  return (
    <>
      <View
        style={{
          width: "90%",
          marginVertical: spacing(5),
          alignItems: "center",
          overflow: "hidden",
        }}
      >
        {progress > -1 ? (
          <View
            style={{
              width: "90%",
              height: 5,
              borderRadius: 10,
              backgroundColor: "gray",
              marginBottom: spacing(4),
            }}
          >
            <View
              style={{
                position: "absolute",
                height: 5,
                left: 0,
                width: `${progress < 8 ? 7 : progress}%`,
                backgroundColor: C.awakenVolt,
              }}
            />
          </View>
        ) : null}
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Buttoon
            appearance="outline"
            onPress={edit}
            style={{ width: "48.4%" }}
            status={active ? "success" : "primary"}
            icon={active ? { name: "check" } : { name: "pen" }}
          >
            {active ? "Done" : "Edit Page"}
          </Buttoon>
          <Buttoon
            onPress={addItem}
            style={{ width: "48.4%" }}
            icon={{ name: "plus" }}
          >
            Add item
          </Buttoon>
        </View>
      </View>
      <ActionSheet
        ref={(ref) => (addItemActionSheet.current = ref)}
        options={[
          "Add a Title",
          "Add a Message",
          "Add a Webpage",
          "Add a Video",
          "Cancel",
        ]}
        cancelButtonIndex={4}
        onPress={addItemSelected}
      />
      <SS_Prompt ref={promptRef} />
    </>
  );
}
