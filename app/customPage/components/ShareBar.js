import { Fonts } from "constant-deprecated";
import React from "react";
import {
  Animated, Dimensions, Text, TouchableOpacity, View
} from "react-native";
import { isIphoneX } from "react-native-iphone-x-helper";
import { verticalScale } from "utilities/";

const height = verticalScale(60);

export default class ShareBar extends React.Component {
  cancelPress() {
    Animated.timing(this.y, {
      toValue: height,
      duration: 150,
      delay: 0,
      useNativeDriver: true
    }).start(this.props.cancelled);
  }

  y = new Animated.Value(60);

  componentDidMount() {
    Animated.timing(this.y, {
      toValue: 0,
      duration: 150,
      delay: 0,
      useNativeDriver: true
    }).start();
  }

  render() {
    return (
      <Animated.View
        style={{
          ...Styles.bar,
          transform: [{ translateY: this.y }]
        }}
      >
        <Button
          onPress={this.cancelPress.bind(this)}
          textStyle={{
            color: "rgb(251, 57, 57)"
          }}
        >
          Cancel
        </Button>
        <Button onPress={this.props.sharePress}>Share</Button>
      </Animated.View>
    );
  }
}

class Button extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={Styles.button}>
          <Text
            style={{
              ...Styles.buttonText,
              ...this.props.textStyle
            }}
          >
            {this.props.children}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const Styles = {
  bar: {
    position: "absolute",
    width: Dimensions.get("window").width,
    height,
    bottom: 0,
    backgroundColor: "rgba(52, 50, 50, 0.99)",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  button: {
    marginTop: isIphoneX() ? 7 : 15,
    marginHorizontal: verticalScale(18)
  },
  buttonText: {
    fontFamily: Fonts.bold,
    color: "#40ff93",
    fontSize: verticalScale(21)
  }
};
