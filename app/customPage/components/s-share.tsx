import { shareItemsToPrettyMessage } from "customPage/handlers/CustomPageHandler";
import React from "react";
import ShareModal from "./share-modal";
import Share from "react-native-share";
import Backend from "backend/";
import ShareBar from "customPage/components/ShareBar";

/**
 * ### All of the content sharing features on a CustomPage
 *  @version 21.03.25
 *  @author jm_francis
 **/
export function $_Sharing(props: {
  visible: boolean;
  navigation: any;
  onShareModeCancel: () => void;
}) {
  const { visible, navigation, onShareModeCancel } = props;
  const shareModal = React.useRef<ShareModal>(null);

  function shareAction(option: ShareOptions) {
    const { EMAIL, APP_CONTACTS, SOCIAL_MEDIA } = ShareOptions;

    const html = option == EMAIL;
    const message = shareItemsToPrettyMessage(html);

    if (option == EMAIL) {
      Share.shareSingle({
        subject: `${Backend.firestoreHandler._config?.variables.appTitle} - Shared Content`,
        message,
        social: "email",
      });
    } else if (option == SOCIAL_MEDIA) {
      Share.open({
        subject: "",
        message,
      });
    } else if (option == APP_CONTACTS) {
      const allLists = Backend.listBuilderHandler.extractLists();
      let contactsCount = 0;
      let buildMyListData;
      for (let l in allLists) {
        const listTitle = allLists[l].title;
        if (listTitle === "Build My List" || listTitle === "Memory Jogger") {
          buildMyListData = allLists[l];
          continue;
        }
        const listContacts = Backend.listBuilderHandler.extractContacts(
          listTitle
        );
        contactsCount += listContacts.length;
      }
      if (contactsCount > 0 || !buildMyListData)
        navigation.navigate("ShareToList");
      else
        navigation.navigate("ListScreen", {
          listData: buildMyListData,
          shareMode: true,
        });
    }
  }

  return (
    <>
      {visible ? (
        <ShareBar
          cancelled={() => onShareModeCancel()}
          sharePress={() => {
            shareModal.current.present();
          }}
        />
      ) : null}

      <ShareModal
        ref={shareModal}
        onExistingListPress={() => shareAction(ShareOptions.APP_CONTACTS)}
        onEmailPress={() => shareAction(ShareOptions.EMAIL)}
        onTextPress={() => shareAction(ShareOptions.SOCIAL_MEDIA)}
      />
    </>
  );
}
enum ShareOptions {
  EMAIL = "email",
  APP_CONTACTS = "app_contacts",
  SOCIAL_MEDIA = "social_media",
}
