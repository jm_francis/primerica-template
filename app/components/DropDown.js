//@ts-check
import { Animations as _Animations } from "animation";
import BlurView from "override/BlurView";
import React, { Component } from "react";
import {
  Animated,
  Dimensions,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";

import { moderateScale } from "utilities/";

export default class DropDown extends Component {
  animations = new Animations();

  state = {
    show: false,
    boxHeight: 0,
  };

  UNSAFE_componentWillReceiveProps(props) {
    const { active = false } = props;
    if (active) {
      this.setState({ show: true }, () => {
        this.animations.present().start();
      });
    } else {
      this.animations.dismiss().start(() => {
        this.setState({ show: false });
      });
    }
  }

  render() {
    const { show } = this.state;
    const {
      title,
      titleProps = {},
      blurProps = {},
      input,
      cancel,
      done,
      externalDone,
      backgroundPress = () => {},
    } = this.props;
    const modalAnim = {
      opacity: this.animations.create("modalOpacity", 0),
    };
    const boxAnim = {
      transform: [
        {
          translateY: this.animations.create(
            "boxY",
            -screenHeight / 2 - Styles.box.height
          ),
        },
      ],
    };
    const layoutButtons = {
      ...Styles.layoutButtons,
      ...(cancel && done
        ? { height: Styles.buttonContainer.height * 2 + 7 }
        : {}),
    };

    const avoidingStyle = {
      transform: [{ translateY: this.props.yOffset }],
    };

    return show ? (
      <Animated.View
        style={{ ...Styles.modal, ...modalAnim, ...this.props.parentStyle }}
      >
        <TouchableWithoutFeedback onPress={backgroundPress}>
          <BlurView style={Styles.container} {...blurProps} blurAmount={5}>
            <KeyboardAvoidingView
              behavior="padding"
              keyboardVerticalOffset={100}
              style={{ ...avoidingStyle }}
            >
              <Animated.View style={{ ...Styles.contentWrapper, ...boxAnim }}>
                <View style={{ ...Styles.box, ...this.props.style }}>
                  {title ? (
                    <Title {...titleProps} style={this.props.titleStyle}>
                      {title}
                    </Title>
                  ) : null}
                  {input ? <Input {...input} /> : null}
                  {done || cancel ? (
                    <View style={layoutButtons}>
                      {done ? <Button title="Done" onPress={done} /> : null}
                      {cancel ? (
                        <Button
                          title="Cancel"
                          style={{ backgroundColor: "#ef2d2d" }}
                          onPress={cancel}
                        />
                      ) : null}
                    </View>
                  ) : null}
                  {this.props.children}
                </View>
                {externalDone ? (
                  <Button
                    title="Done"
                    external
                    onPress={externalDone}
                    style={{ marginTop: 20 }}
                  />
                ) : null}
              </Animated.View>
            </KeyboardAvoidingView>
          </BlurView>
        </TouchableWithoutFeedback>
      </Animated.View>
    ) : null;
  }
}

export class Title extends Component {
  render() {
    return (
      <Text
        adjustsFontSizeToFit
        style={{ ...Styles.title, ...this.props.style }}
      >
        {this.props.children}
      </Text>
    );
  }
}
export class Input extends Component {
  render() {
    const { line = true } = this.props;
    const inputstyle = { ...Styles.input, ...this.props.style };
    return (
      <View style={Styles.inputContainer}>
        <TextInput
          key="textinput"
          autoFocus
          selectionColor={inputstyle.color}
          {...this.props}
          style={inputstyle}
        />
        {line ? (
          <View
            key="line"
            style={{ ...Styles.line, ...this.props.lineStyle }}
          />
        ) : null}
      </View>
    );
  }
}
export class Button extends Component {
  press() {
    Keyboard.dismiss();
    this.props.onPress();
  }
  render() {
    return (
      <TouchableOpacity onPress={this.press.bind(this)}>
        <View style={{ ...Styles.buttonContainer, ...this.props.style }}>
          <Text style={{ ...Styles.buttonTitle, ...this.props.titleStyle }}>
            {this.props.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

const Styles = {
  modal: {
    position: "absolute",
    zIndex: 10,
    width: screenWidth,
    height: screenHeight,
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  contentWrapper: {
    alignItems: "center",
  },
  box: {
    width: moderateScale(250, 0.25),
    height: moderateScale(315, 0.25),
    backgroundColor: "white",
    borderRadius: 10,
    paddingTop: 30,
    paddingBottom: 10,
    paddingHorizontal: 15,
    justifyContent: "space-between",
    alignItems: "center",
  },
  title: {
    color: "rgb(15,15,15)",
    fontFamily: Platform.OS === "ios" ? "AvenirNext-Medium" : "Lato-Regular",
    alignSelf: "center",
    fontSize: moderateScale(22, 0.25),
  },
  inputContainer: {
    width: "85%",
    // height: moderateScale(30,0.25)
  },
  input: {
    fontSize: moderateScale(25, 0.25),
    color: "rgb(15,15,15)",
    fontFamily: Platform.OS === "ios" ? "AvenirNext-Medium" : "Lato-Regular",
    textAlign: "center",
  },
  lineStyle: {
    width: "95%",
    backgroundColor: "rgb(15,15,15)",
    height: moderateScale(1.5, 0.25),
    borderRadius: 0.7,
  },
  layoutButtons: {
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
  },
  buttonContainer: {
    width: moderateScale(180, 0.25),
    height: moderateScale(40, 0.25),
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgb(46, 247, 117)",
  },
  buttonTitle: {
    color: "white",
    fontFamily: Platform.OS === "ios" ? "AvenirNext-Medium" : "Lato-Regular",
    fontSize: moderateScale(20, 0.25),
  },
};

class Animations extends _Animations {
  resetBox() {
    return this.timing("boxY", {
      toInitialValue: true,
      duration: 0,
      useNativeDriver: true,
    });
  }
  present(offset = 0) {
    const modalOpacity = this.timing("modalOpacity", {
      toValue: 1,
      duration: 200,
      useNativeDriver: true,
    });
    const boxY = this.spring("boxY", {
      toValue: offset,
      tension: 20,
      friction: 6,
      useNativeDriver: true,
    });
    const reset = this.resetBox();
    const meat = Animated.parallel([modalOpacity, boxY]);
    return Animated.sequence([reset, meat]);
  }
  dismiss() {
    const modalOpacity = this.timing("modalOpacity", {
      toValue: 0,
      duration: 200,
      useNativeDriver: true,
    });
    const boxY = this.spring("boxY", {
      toValue: screenHeight / 2,
      tension: 20,
      friction: 6,
      useNativeDriver: true,
    });
    return Animated.parallel([modalOpacity, boxY]);
  }
}
