
import React from "react";
import { Image, Animated, Easing } from "react-native";
import { LOADING } from "constant-deprecated/Images";
import { scale, moderateScale, verticalScale } from "utilities/";

export default class Loading extends React.Component {
  rotate = new Animated.Value(0);

  componentDidMount() {
    this.animate();
  }
  animate() {
    const a = Animated.timing(this.rotate, {
      toValue: 1,
      duration: 500,
      ease: Easing.linear,
      useNativeDriver: true
    });
    const b = Animated.timing(this.rotate, {
      toValue: 0,
      duration: 0,
      useNativeDriver: true
    });
    Animated.sequence([a, b]).start(this.animate.bind(this));
  }

  render() {
    const { style, imageStyle } = this.props;

    return (
      <Animated.View
        style={[
          Styles.container,
          {
            transform: [
              {
                rotate: this.rotate.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0deg", "360deg"]
                })
              }
            ]
          },
          style
        ]}
      >
        <Image
          source={LOADING}
          resizeMode="contain"
          style={[Styles.image, imageStyle]}
        />
      </Animated.View>
    );
  }
}

const dimension = moderateScale(27, 0.24);

const Styles = {
  container: {
    justifyContent: "center",
    alignItems: "center"
  },
  image: {
    width: dimension,
    height: dimension,
    tintColor: "white"
  }
};
