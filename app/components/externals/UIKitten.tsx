import * as UIKT from "@ui-kitten/components";

/**
 *
 * @description Customized UI Kitten components befonre export it as <Kitten.[]>
 */
export const Kitten = { ...UIKT };

export type dAccessory = {
  style: {
    height: number;
    marginHorizontal: number;
    tintColor: string;
    width: number;
  };
};

// export default Kitten;
