// @ts-check
import { UP_ARROW } from "constant-deprecated/Images";
import React, { Component } from "react";
import {
  Dimensions,
  Image,
  Modal,
  Platform,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { moderateScale } from "utilities/";

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

/**
 * @example
 * <DirectCoverModal
 * visible={true/false}
 * text=""
 * doneTitle="OK"
 * donePress={()=>{}} // required if you want done title to show up
 * onSelectionPress={()=>{}} // the highlighted area
 * startY endY startX endX
 * justText={true/false} // to just have a text prompt
 * backgroundOpacity={0.5} />
 */
export default class DirectCoverModal extends Component {
  state = {
    doneButtonYOffset: 0,
  };

  doneButtonLayout(e) {
    const { doneButtonYOffset } = this.state;
    if (doneButtonYOffset !== 0) return;
    this.doneButtonRef.measure((x, y, width, height, pageX, pageY) => {
      const difference = pageY + height + 27 - screenHeight;
      if (difference > 0) {
        this.setState({ doneButtonYOffset: -difference });
      }
    });
  }

  render() {
    const { doneButtonYOffset } = this.state;
    const data = this.props.data ? this.props.data : this.props;
    const {
      visible,
      text,
      backgroundOpacity = 0.87,
      doneTitle = "OK",
      donePress = null,
      onSelectionPress,
      startY = 0,
      endY = 0,
      startX = 0,
      endX = screenWidth,
      justText = false,
    } = data;

    const selectionWidth = endX - startX;
    const arrowDimensions = selectionWidth < 72 ? selectionWidth * 0.72 : 72;
    const scaledOffset = arrowDimensions / 72;

    const backgroundColor = `rgba(20,20,20,${backgroundOpacity})`;

    return (
      <Modal
        style={Styles.container}
        animationType="fade"
        transparent={true}
        visible={visible}
      >
        <View style={Styles.container}>
          <View
            style={{
              ...Styles.cover,
              backgroundColor,
              top: 0,
              height: startY,
            }}
          />

          <TouchableWithoutFeedback onPress={onSelectionPress}>
            <View
              style={{
                width: screenWidth,
                top: startY,
                height: endY - startY,
              }}
            />
          </TouchableWithoutFeedback>

          <View
            style={{
              ...Styles.cover,
              backgroundColor,
              top: startY,
              height: endY - startY,
              left: 0,
              width: startX,
            }}
          />
          {/* horizontal selection space */}
          <View
            style={{
              ...Styles.cover,
              backgroundColor,
              top: startY,
              height: endY - startY,
              left: endX,
              width: screenWidth - endX,
            }}
          />

          <View
            style={{
              ...Styles.cover,
              backgroundColor,
              top: endY,
              height: screenHeight - endY,
            }}
          >
            <View
              style={{
                ...Styles.bottomCoverContainer,
                ...(justText ? Styles.bottomCoverContainerCenter : {}),
              }}
            >
              {justText ? null : (
                <Image
                  source={UP_ARROW}
                  style={{
                    ...Styles.upArrowIcon,
                    width: arrowDimensions,
                    height: arrowDimensions,
                    marginLeft: startX,
                    marginTop: Styles.upArrowIcon.marginTop * scaledOffset,
                  }}
                />
              )}
              <Text
                style={{
                  ...Styles.promptText,
                  marginTop: doneButtonYOffset / 2,
                }}
              >
                {text}
              </Text>
              {donePress ? (
                <DoneButton
                  text={doneTitle}
                  onPress={donePress}
                  style={{
                    marginTop:
                      Styles.doneButtonContainer.marginTop +
                      doneButtonYOffset / 2,
                  }}
                  viewRef={(ref) => (this.doneButtonRef = ref)}
                  onLayout={this.doneButtonLayout.bind(this)}
                />
              ) : null}
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

function DoneButton(props) {
  const { text, onPress, style = {}, viewRef = () => {} } = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        {...props}
        style={{
          ...Styles.doneButtonContainer,
          ...style,
        }}
        ref={(ref) => viewRef(ref)}
      >
        <Text style={Styles.doneButtonText}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
}

// const coverBackgroundColor = 'rgba(20,20,20,0.9)'
const Styles = {
  container: {
    flex: 1,
  },
  cover: {
    position: "absolute",
    width: screenWidth,
    // backgroundColor: coverBackgroundColor
  },
  bottomCoverContainer: {
    flex: 1,
    alignItems: "center",
  },
  bottomCoverContainerCenter: {
    justifyContent: "center",
  },
  promptText: {
    color: "white",
    fontSize: moderateScale(30),
    textAlign: "center",
    fontFamily: Platform.OS === "ios" ? "AvenirNext-Bold" : "Lato-Black",
    marginHorizontal: 15,
  },
  upArrowIcon: {
    tintColor: "white",
    width: moderateScale(72),
    height: moderateScale(72),
    marginTop: 24,
    marginBottom: 32,
  },
  doneButtonContainer: {
    marginTop: 127,
  },
  doneButtonText: {
    color: "white",
    fontFamily: Platform.OS === "ios" ? "AvenirNext-Bold" : "Lato-Black",
    fontSize: moderateScale(44),
    textAlign: "center",
  },
};
