import { Button, ButtonProps } from "@ui-kitten/components";
import { dAccessory } from "components/externals/UIKitten";
import R from "ramda";
import * as React from "react";
import { ActivityIndicator, Keyboard } from "react-native";
import { scale } from "utilities/";
import { dIconPrimr, IconPrimr } from "../icons";

//@ts-ignore
interface P extends ButtonProps {
  onPress: (
    /** progress function to call when we want to hide spinner
     * @see example:2
     */
    xong?: Function
  ) => void;
  icon?: dIconPrimr & {
    /** Is icon on the right? */
    right?: boolean;
  };

  /**
   * Should button be wrapped around its children ("compact")?
   */
  compact?: boolean;

  progress?: boolean;
}

/**
 * ### This is button component
 * @author nguyenkhoi
 * @version 0.9.21
 * @example
 *  <Buttoon
      onPress={() => {}}
      appearance="ghost"
      icon={{ name: "chevron_right"}}
      status="basic"
      size="medium"
    >
      Nine-nine!
    </Buttoon>
  * @example 
  * <Buttoon
  *   progress
      onPress={(xong) => {setTimeout(xong(),1000)}}
      appearance="ghost"
      icon={{ name: "chevron_right"}}
      status="basic"
      size="medium"
    >
      Nine-nine!
    </Buttoon>   

 */
export default function Buttoon(props: P) {
  const {
    icon,
    compact = false,
    appearance,
    disabled,
    onPress,
    progress,
    textStyle,
  } = props;
  const [_loading, setLoading] = React.useState(false);
  function _onPress() {
    setLoading(progress);
    Keyboard.dismiss();
    //@ts-ignore
    onPress && onPress(() => setLoading(false));
  }

  return (
    <Button
      {...props}
      onPress={_onPress}
      style={[
        props.style,
        compact && { alignSelf: "center" },
        disabled && appearance == "ghost" && { backgroundColor: "transparent" },
        appearance == "icon" && {
          borderRadius: scale(100),
          borderWidth: 0,
          width: scale(20),
          height: scale(20),
          margin: scale(3),
        },
      ]}
      accessoryLeft={(props: dAccessory) => {
        return _loading ? (
          <ActivityIndicator color={props.style.tintColor} />
        ) : (
          !R.isNil(icon) && R.isNil(icon.right) && (
            <IconPrimr
              preset={`safe`}
              name={`arrow_left`}
              size={props.style.width * 0.8}
              color={props.style.tintColor}
              {...icon}
            />
          )
        );
      }}
      accessoryRight={(props: dAccessory) => {
        return (
          !R.isNil(icon) &&
          !R.isNil(icon.right) && (
            <IconPrimr
              preset={`safe`}
              name={`arrow_left`}
              size={props.style.width * 0.8}
              color={props.style.tintColor}
              {...icon}
            />
          )
        );
      }}
    />
  );
}
