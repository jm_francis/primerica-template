import { Input, InputProps } from "@ui-kitten/components";
import { Buttoon, Kitten, Txt } from "components/";
import React from "react";
import { View } from "react-native";
import { spacing } from "utilities/";
import { sstyled } from "../sstyled/sstyled";

/**
 * ### Get and save input from the user
 * A "Save" button pops up within the input once they change something.
 * 
 * @author jm_francis
 * @version 1.1.26
 * 
 * @example
 * <Inpuut
        title="Foo"
        doneText="Save"
        value={foo}
        onChangeText={setFoo}
        onSavePress={saveFoo}
        isUrl={false}
      />
 */
export const Inpuut = React.forwardRef(
  (props: dInpuut, ref: React.MutableRefObject<Input>) => {
    const {
      title,
      style = {},
      inputStyle = {},
      doneText = "Save",
      alwaysShowSave = false,
      onChangeText,
      onSavePress,
      isUrl,
    } = props;

    const [_saveEnabled, enableSave] = React.useState(false);

    const refSaveShown = React.useRef<boolean>(false);
    const saveEnabled = alwaysShowSave ? true : refSaveShown.current;

    const inputProps = { ...props };

    /**
     * ### url formatter
     * -  If the isUrl prop is set to true, the following 3 Input props will be set automatically
     * @example
     * <Inpuut url />
     */
    if (isUrl) inputProps.autoCorrect = false;
    if (isUrl) inputProps.autoCapitalize = "none";
    if (isUrl) inputProps.keyboardType = "url";
    delete inputProps.title;

    return (
      <SS.Ctnr style={style}>
        {title ? (
          <Txt.S1 style={{ fontWeight: "bold", marginBottom: spacing(1) }}>
            {title}
          </Txt.S1>
        ) : null}
        <Kitten.Input
          ref={ref}
          accessoryRight={(_props) =>
            saveEnabled ? (
              <Buttoon
                appearance="ghost"
                size="small"
                style={{ padding: 0, margin: 0 }}
                onPress={() => {
                  // enableSave(false);
                  refSaveShown.current = false;
                  onSavePress();
                }}
              >
                {doneText}
              </Buttoon>
            ) : null
          }
          style={{ width: "100%", ...inputStyle }}
          autoCorrect={false}
          {...inputProps}
          onChangeText={(text) => {
            // enableSave(true);
            refSaveShown.current = true;
            onChangeText(text);
          }}
        />
      </SS.Ctnr>
    );
  }
);

const SS = {
  Ctnr: sstyled(View)((p: { style?: {} }) => ({
    width: "100%",
    alignItems: "flex-start",
    ...p.style,
    // marginVertical: spacing(2),
  })),
};

interface dInpuut extends InputProps {
  /**
   * The title displays above the input
   */
  title?: string;
  inputStyle?: {};
  doneText?: string;
  alwaysShowSave?: boolean;
  value: any;
  isUrl?: boolean;
  placeholder?: string;
  onChangeText?: (text: string) => void;
  onSavePress?: () => void;
  disabled?: boolean;
}
