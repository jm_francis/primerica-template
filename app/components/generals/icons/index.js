/** 
 * Originally stayed in `./assets`,
 * but it's a hard time to get `./assets` working with TS alias
 * so I turn it into a component,
 * which still makes sense.
 */

export { default as IconPrimr } from "./primr-icon";
export * from "./primr-icon.props";

