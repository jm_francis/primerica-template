// import { IPcolors, IPpalette } from "./utils-typings";
import { ViewStyle } from "react-native";
import { KeyOf } from "utilities/";
import { iconOptions } from "./primr-icon";

/**
 * A list of icon names used in IconPrimr for type-check
 */
export type ICON_NAME = KeyOf<typeof iconOptions>;

/**
 * Props of Icon<>
 */
export interface dIconPrimr {
  name: ICON_NAME;
  size?: number;
  color?: string;
  solid?: boolean;
  containerStyle?: ViewStyle;
  preset?: "default" | "safe" | "circular" | "header";
  disabled?: boolean;
  onPress?(): void;
  onLongPress?(): void;
}
export type ICON_PKG =
  | "ion"
  | "fa5"
  | "fa"
  | "feather"
  | "material"
  | "material-community"
  | "fontisto"
  | "entypo"
  | "simple-line-icon"
  | "antdesign";

export type dIconOptions = {
  type: ICON_PKG;
  icon: string;
  scale?: number;
  solid?: boolean;
};
