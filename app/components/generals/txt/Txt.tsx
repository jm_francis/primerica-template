import { Text, TextProps } from "@ui-kitten/components";
import Fonts from "constant-deprecated/Fonts";
import { Platform } from "react-native";
import { scale } from "utilities/";
import { sstyled } from "../sstyled/sstyled";

const H1 = sstyled(Text)((p) => ({
  fontFamily: Fonts["bold"],
  color: p.C.text,
  fontSize: scale(36),
  fontWeight: Platform.select({ web: "800", ios: "800", android: "bold" }),
}));
const H2 = sstyled(Text)((p) => ({
  fontFamily: Fonts["bold"],
  color: p.C.text,
  fontSize: scale(32),
  fontWeight: Platform.select({ web: "800", ios: "800", android: "bold" }),
}));
const H3 = sstyled(Text)((p) => ({
  fontFamily: Fonts["bold"],
  color: p.C.text,
  fontSize: scale(30),
  fontWeight: Platform.select({ web: "800", ios: "800", android: "bold" }),
}));
const H4 = sstyled(Text)((p) => ({
  fontFamily: Fonts["bold"],
  color: p.C.text,
  fontSize: scale(26),
  fontWeight: Platform.select({ web: "800", ios: "800", android: "bold" }),
}));
const H5 = sstyled(Text)((p) => ({
  fontFamily: Fonts["bold"],
  color: p.C.text,
  fontSize: scale(22),
  fontWeight: Platform.select({ web: "800", ios: "800", android: "bold" }),
}));
const H6 = sstyled(Text)((p) => ({
  fontFamily: Fonts["bold"],
  color: p.C.text,
  fontSize: scale(18),
  fontWeight: Platform.select({ web: "800", ios: "800", android: "bold" }),
}));
const S1 = sstyled(Text)((p) => ({
  fontFamily: Fonts["bold"],
  color: p.C.text,
  fontSize: scale(15),
  fontWeight: Platform.select({ web: "600", ios: "600", android: "bold" }),
}));
const S2 = sstyled(Text)((p) => ({
  fontFamily: Fonts["bold"],
  color: p.C.text,
  fontSize: scale(13),
  fontWeight: Platform.select({ web: "600", ios: "600", android: "bold" }),
}));
const P1 = sstyled(Text)((p) => ({
  fontFamily: Fonts["medium"],
  color: p.C.text,
  fontSize: scale(14),
  fontWeight: Platform.select({ web: "400", ios: "400", android: "normal" }),
  // letterSpacing: 0.5,
}));
const P2 = sstyled(Text)((p) => ({
  fontFamily: Fonts["medium"],
  color: p.C.text,
  fontSize: scale(12),
  fontWeight: Platform.select({ web: "400", ios: "400", android: "normal" }),
  // letterSpacing: 0.5,
}));
const C1 = sstyled(Text)((p) => ({
  fontFamily: Fonts["medium"],
  color: p.C.text,
  fontSize: scale(12),
  fontWeight: Platform.select({ web: "400", ios: "400", android: "normal" }),
}));
const C2 = sstyled(Text)((p) => ({
  fontFamily: Fonts["medium"],
  color: p.C.text,
  fontSize: scale(12),
  fontWeight: Platform.select({ web: "400", ios: "400", android: "normal" }),
}));
const $Title = sstyled(S1)((p) => ({
  padding: scale(10),
  fontFamily: Fonts["bold"],
  color: p.C.grey600,
  fontWeight: Platform.select({ web: "700", ios: "700", android: "bold" }),
}));

const Indicator = sstyled(C2)((p) => ({
  fontFamily: Fonts["bold"],
  textAlign: "center",
  color: p.C.dim,
  fontSize: scale(12),
  fontWeight: Platform.select({ web: "400", ios: "400", android: "normal" }),
}));

/**
 * ###  A text component of the project,
 * depending on ui-kitten's Text
 * ---
 * @example
 * ```
 * <Txt>👋</Txt>
 * ```
 * ---
 * @version 1.1.29
 * - *No more `ms(size)` to reduce font size*
 */
export const Txt: dTxtC0 = P1;
Txt.H1 = H1;
Txt.H2 = H2;
Txt.H3 = H3;
Txt.H4 = H4;
Txt.H5 = H5;
Txt.H6 = H6;
Txt.S1 = S1;
Txt.S2 = S2;
Txt.P1 = P1;
Txt.P2 = P2;
Txt.C1 = C1;
Txt.C2 = C2;
Txt.$Title = $Title;
Txt.Indicator = Indicator;

export interface dTxtC0 extends React.FC<TextProps> {
  /** Heading 1 */
  H1?: React.FC<TextProps>;
  H2?: React.FC<TextProps>;
  H3?: React.FC<TextProps>;
  H4?: React.FC<TextProps>;
  H5?: React.FC<TextProps>;
  H6?: React.FC<TextProps>;
  /** Subheading 1 */
  S1?: React.FC<TextProps>;
  S2?: React.FC<TextProps>;
  P1?: React.FC<TextProps>;
  P2?: React.FC<TextProps>;
  C1?: React.FC<TextProps>;
  C2?: React.FC<TextProps>;
  /** Section Title */
  $Title?: React.FC<TextProps>;
  Indicator?: React.FC<TextProps>;
}
// export const Txt = Text;
