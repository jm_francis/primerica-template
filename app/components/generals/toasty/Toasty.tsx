import Toast from "react-native-toast-message";
export { default as Toast } from "react-native-toast-message";

/**
 * An animated toast message component that can be called as function.
 *
 * @example
 * Toasty({
    type: "error",
    text1: "No, an empty post is not a thing",
    text2: "Please add some content.",
    visibilityTime: 4000,
  });
 *
 * @param message
 * @param title
 * @param type
 * @param position
 * @param visibilityTime
 * @param autoHide
 * @param topOffset
 * @param bottomOffset
 */
export function Toasty(props: PROPS_Toasty) {
  const {
    text1,
    text2,
    type = "success",
    position = "top",
    visibilityTime = 2000,
    autoHide = true,
    topOffset = 30,
    bottomOffset = 40,
  } = props;
  Toast.show({
    text1,
    text2,
    type,
    position,
    visibilityTime,
    autoHide,
    topOffset,
    bottomOffset,
  });
}

type PROPS_Toasty = {
  text1: string;
  text2?: string;
  type?: "success" | "error" | "info";
  position?: "top" | "bottom";
  visibilityTime?: number;
  autoHide?: boolean;
  topOffset?: number;
  bottomOffset?: number;
};

/**
 * Custom Toast appearance.
 *  If nothing change, remember to mask them
 *  or the toast won't show up
 */
export const toastConfig = {
  //   success: (internalState) => (
  //     <View style={{ height: 60, width: "100%", backgroundColor: "pink" }}>
  //       <Text>{internalState.text1}</Text>
  //     </View>
  //   ),
  // success: () => {},
  // error: () => {},
  // info: () => {},
  // any_custom_type: () => {},
};
