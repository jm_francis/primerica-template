import Backend from "backend/";
import React from "react";
import { View } from "react-native";
import { scale } from "react-native-size-matters";
import { C } from "utilities/";
import { Kitten } from "./externals";
import { IconPrimr, Txt } from "./generals";

interface PropTypes {
  user: any;
  style?: any;
  onPress: (user: any) => any;
  showLevelProgress?: Boolean;
}

export default function UserListItem(props: PropTypes) {
  const { user, style = {}, onPress, showLevelProgress } = props;

  const levelProgress = Math.floor(
    Backend.levelsHandler.calculateTotalLevelProgressForUser(user) * 100
  );

  return (
    <Kitten.ListItem
      style={{ backgroundColor: "transparent", ...style }}
      // title={(props) => (
      //   <Txt
      //     style={{
      //       fontWeight: "bold",
      //       marginLeft: spacing(3),
      //       backgroundColor: !!!user.name && C.dim,
      //       // width: !!!user.name && "30%",
      //     }}
      //   >
      //     {/* {user.name ? user.name : user.email} */}
      //     {user.name || "     "}
      //   </Txt>
      // )}
      title={user.name || "___"}
      description={user.email}
      accessoryLeft={(props) => (
        <View
          style={{
            minWidth: scale(32),
            ...(user.profileImage ? {} : { marginLeft: -4 }),
          }}
        >
          {user.profileImage ? (
            <Kitten.Avatar size="small" source={{ uri: user.profileImage }} />
          ) : (
            <IconPrimr name={`profile`} size={scale(30)} color={C.pitchWhite} />
          )}
        </View>
      )}
      accessoryRight={(props) =>
        showLevelProgress ? <Txt>{`${levelProgress}%`}</Txt> : null
      }
      {...props}
      onPress={() => {
        console.log('press')
        onPress(user)
      }}
    />
  );
}
