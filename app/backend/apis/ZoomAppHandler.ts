import { Linking, Platform, Alert } from "react-native";
import { AppInstalledChecker } from "react-native-check-app-install";

const downloadZoomApp =
  Platform.OS === "ios"
    ? "https://itunes.apple.com/us/app/zoom-cloud-meetings/id546505307?mt=8"
    : "https://play.google.com/store/apps/details?id=us.zoom.videomeetings&hl=en_US";

export const openZoomWithIdIfPossible = (_meetingId) => {
  let meetingId = _meetingId;
  if (meetingId === 0 || meetingId === "0") {
    Alert.alert("Video Call has not yet been setup.");
    return;
  }
  if (
    typeof meetingId === "string" &&
    (meetingId.includes("http") ||
      meetingId.includes(".com") ||
      meetingId.includes("zoom"))
  ) {
    if (!meetingId.includes("http")) meetingId = `https://${meetingId}`;
    Linking.openURL(meetingId);
    return;
  }

  AppInstalledChecker.checkURLScheme("zoomus").then((isInstalled) => {
    if (!isInstalled) {
      Linking.openURL(downloadZoomApp);
    } else Linking.openURL(`zoomus://zoom.us/join?confno=${meetingId}?zc=0`);
  });
};
