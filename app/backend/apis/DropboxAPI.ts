import { Config } from "constant-deprecated/";
import { Alert } from "react-native";

async function post(url: string, body: {}, requiredItem?: string) {
  const tokens = Config.dropboxAccessTokens.concat([]);
  let tokenIndex = 0;
  async function attemptFetch() {
    if (tokenIndex + 1 > tokens.length) {
      console.log("ALL TOKENS FAILED");
      return null;
    }

    const token = tokens[tokenIndex++];
    console.log(
      "Attempting " + url + " with " + token + " (" + tokenIndex + ")"
    );
    const res = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    if (res.ok == false) {
      // failed with token
      return await attemptFetch();
    }
    const json = await res.json();
    if (requiredItem && !json[requiredItem]) {
      // failed with token according to requiredItem
      return await attemptFetch();
    }
    return json;
  }

  const json = await attemptFetch();
  return json;
}

export default class DropboxAPI {
  // ANCHOR: Files

  /**
   * Gets the proper file or folder metadata
   */
  static async getMetadata(shareLink: string) {
    const metadata1 = await post(
      `https://api.dropboxapi.com/2/sharing/get_shared_link_metadata`,
      {
        url: shareLink,
      }
    );
    const { id } = metadata1;
    const metadata2 = await post(
      `https://api.dropboxapi.com/2/sharing/get_file_metadata`,
      {
        file: id,
      },
      "path_lower"
    );
    return metadata2;
  }
  /**
   * Get a direct uri to the audio file for streaming
   */
  static async getStreamingURI(shareLink: string) {
    const metadata = await DropboxAPI.getMetadata(shareLink);
    if (!metadata) {
      Alert.alert(
        "Something went wrong.",
        "Unable to load audio at this time."
      );
      return false;
    }
    const path = metadata.path_lower;
    const json = await post(
      `https://api.dropboxapi.com/2/files/get_temporary_link`,
      {
        path: path,
      }
    );
    return json.link;
  }

  // ANCHOR: Folders

  /**
   * Get content of a folder (returns entries[] array)
   * Each entry contains { name, uri, id }
   */
  static async folder_getContent(shareLink: string) {
    const metadata = await post(
      `https://api.dropboxapi.com/2/sharing/get_shared_link_metadata`,
      {
        url: shareLink,
      },
      "path_lower"
    );
    if (!metadata) return null;
    const content = await post(
      `https://api.dropboxapi.com/2/files/list_folder`,
      {
        path: metadata.path_lower,
      }
    );
    const entries = [];
    for (let e in content.entries) {
      const entry = content.entries[e];
      entries.push({
        name: entry.name,
        path: entry.path_lower,
        id: entry.id,
      });
    }
    return entries;
  }
  /**
   * Gets streaming uri for a file that was fetched with getFolderContent(shareLink: string)
   */
  static async folder_getStreamingURI(filePath: string) {
    const json = await post(
      `https://api.dropboxapi.com/2/files/get_temporary_link`,
      {
        path: filePath,
      }
    );
    return json.link;
  }
}
