import { Config } from "constant-deprecated";
import RNFetchBlob from "rn-fetch-blob";

export default class VimeoAPI {
  static getAlbumIdFromUri(uri) {
    let uriStr = uri;
    const albumStr = "album/";
    const start = uriStr.indexOf(albumStr) + albumStr.length;
    return uriStr.substring(start, uriStr.length);
  }
  static getVideoIdFromUri(uri) {
    let uriStr = uri;
    const str = "vimeo.com/";
    const start = uriStr.indexOf(str) + str.length;
    uriStr = uriStr.substring(start, uriStr.length);
    const end = uriStr.includes("/") ? uriStr.indexOf("/") : uriStr.length;
    return uriStr.substring(0, end);
  }
  static getUserIdFromUri(uri) {
    let uriStr = uri;
    uriStr = uriStr.replace("https://", "");
    if (uriStr.split("/").length < 3) return null;
    return uriStr.substring(uriStr.split("/")[1]);
  }

  static getMP4FromVideo(videoData) {
    if (!videoData || !videoData.files) {
      console.log(
        "WARNING: videoData or videoData.files is null in getMP4FromVideo in VimeoAPI.js"
      );
      return null;
    }
    const files = videoData.files;
    let link;
    for (f in files) {
      const file = files[f];
      if (file.quality == "sd") {
        link = file.link;
      }
    }
    const end = link.indexOf("&oauth");
    link = link.substring(0, end);
    return link;
  }
  static getThumbnailFromVideo(videoData, size = 1) {
    if (!videoData || !videoData.pictures || !videoData.pictures.sizes) {
      console.log(
        "WARNING: videoData, videoData.pictures, or videoData.pictures.sizes is null in getThumbnailFromVideo(videoData) in VimeoAPI.js"
      );
      return null;
    }
    const pictures = videoData.pictures.sizes;
    let setLink = "";
    for (p in pictures) {
      const pic = pictures[p];
      const link = pic.link;
      if (size === 1 && link.includes("200x150")) {
        setLink = link;
        break;
      } else if (size == 0 && link.includes("100x75")) {
        setLink = link;
        break;
      }
      setLink = link;
    }
    const end = setLink.indexOf("?");
    setLink = setLink.substring(0, end);
    return setLink;
  }
  static fetch(uri, index) {
    const { accessToken } = Config.vimeoAccessTokens[index];
    return new Promise((resolve, reject) => {
      RNFetchBlob.fetch("GET", uri, {
        Authorization: "Bearer " + accessToken
      })
        .then(data => {
          const response = JSON.parse(data.data);
          if (response.error) resolve(false);
          else resolve(response);
        })
        .catch(error => resolve(false));
    });
  }
  static attemptVideoFetch(data, callback, index = 0) {
    // const { userId } = Config.vimeoAccessTokens[index]
    const extractedUserId = this.getUserIdFromUri(data.shareLink);
    const userId = extractedUserId
      ? extractedUserId
      : Config.vimeoAccessTokens[index].userId;

    // TODO other options?
    let uri = ``;
    if (data.shareLink)
      uri = `https://api.vimeo.com/users/${userId}/videos/${this.getVideoIdFromUri(
        data.shareLink
      )}`;
    else if (data.path)
      uri = `https://api.vimeo.com/users/${userId}${data.path}`;
    uri = uri + "?fields=files,name,link,pictures.sizes.link";

    const nextIndex = ++index;
    const tryAgain = () => {
      VimeoAPI.attemptVideoFetch(data, callback, nextIndex);
    };

    VimeoAPI.fetch(uri, index).then(response => {
      if (response === false) {
        if (nextIndex >= Config.vimeoAccessTokens.length) callback(null);
        else tryAgain();
      } else callback(response);
    });
  }
  static attemptAlbumFetch(data, callback, index = 0) {
    const { userId } = Config.vimeoAccessTokens[index];

    let path;
    if (data.albumId) path = `/users/${userId}/albums/${data.albumId}/videos`;
    else if (data.albumObject) path = data.albumObject.uri + "/videos";
    else if (data.shareLink)
      path = `/users/${userId}/albums/${VimeoAPI.getAlbumIdFromUri(
        data.shareLink
      )}/videos`;
    const uri = "https://api.vimeo.com" + path;

    const tryAgain = () => {
      VimeoAPI.attemptAlbumFetch(data, callback, index + 1);
    };

    VimeoAPI.fetch(uri, index).then(response => {
      if (response === false) {
        if (index + 1 >= Config.vimeoAccessTokens.length) callback(null);
        else tryAgain();
      } else callback(response.data);
    });
  }

  static getVideo(data) {
    return new Promise((resolve, reject) => {
      const callback = response => {
        response ? resolve(response) : reject();
      };
      VimeoAPI.attemptVideoFetch(data, callback);
    });
  }

  //get videos (options: albumId, uri, object)
  static getVideos(data) {
    return new Promise((resolve, reject) => {
      const callback = response => {
        response ? resolve(response) : reject();
      };
      VimeoAPI.attemptAlbumFetch(data, callback);
    });
  }
}
