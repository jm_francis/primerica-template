import axios from "axios";
import Backend from "backend/Backend";
import { Toasty } from "components/";
import { Keys } from "constant-deprecated/";
import R from "ramda";
import ImageCropPicker, { Options } from "react-native-image-crop-picker";
const { GOOGLE_API_KEY } = Keys;

export enum dMediaFileType {
  video,
  image,
}

export interface dGoogleDriveData {
  thumbnail: string;
  fileType: string;
  json: any;
}

/**
 * ###  A Media picker and upload function
 * -  using ImageCropPicker, cloudinary
 */
export default class FileNetwork {
  /**
   * @deprecated
   * -  Returns a direct uri to access the file itself
   * Ex usage: react-native-track-player, react-native-video
   */
  static uriFromSource(source: string) {
    const regex = /\/d\/(.*)\/view/i;
    const _fileId = source.match(regex)[1];
    return `https://www.googleapis.com/drive/v3/files/${_fileId}?alt=media&key=${GOOGLE_API_KEY}`;
  }

  /**
   * @deprecated
   * Returns a share link with the given file id
   */
  static shareLinkWithFileId(fileId: string) {
    return `https://drive.google.com/file/d/${fileId}/view?usp=sharing`;
  }

  /**
   * @deprecated
   * (async) Returns { thumbnail: string, fileType: string, json: {} }
   */
  static async getVideoMetadata(source: string): Promise<dGoogleDriveData> {
    const regex = /\/d\/(.*)\/view/i;
    const _fileId = source.match(regex)[1];
    const uri = `https://www.googleapis.com/drive/v3/files/${_fileId}?key=${GOOGLE_API_KEY}&fields=thumbnailLink%2C%20thumbnailVersion%2C%20mimeType`;
    const res = await fetch(uri, { method: "GET" });
    const json = await res.json();
    return {
      thumbnail: json.thumbnailLink,
      fileType: json.mimeType,
      json,
    };
  }

  static async fetchUploadData(folder: string) {
    const baseURL =
      "https://us-central1-app-takeoff-dev.cloudfunctions.net/api";
    const res = await fetch(`${baseURL}/services/fetch-signed-upload-props`, {
      method: "POST",
      mode: "cors",
      body: JSON.stringify({ folder }),
      headers: {
        "content-type": "application/json",
      },
    }).catch((e) => console.warn("error fetchUploadData: ", e));
    const json = await res.json();
    if (!json) {
      console.log("Failed to fetch upload props!");
      return null;
    }
    return json;
  }

  /**
   * ###  Prompts user to pick a video or image on their phone
   * - Uploads video/image to Cloudinary and returns the uri, width, and height, and file type
   * - The uri can then be easily be used in a <Video /> or <Image /> component
   *
   * @version 1.1.22
   * @author nguyenkhooi, jm_francis
   *
   * @example
   * FileNetwork.selectAndUpload({
   *  title: "My Video!",
   *  type: "video",
   *  folder: "some/cloudinary/path"
   * }, progress => console.log(progress))
   *
   *
   */
  static async selectAndUpload(
    options: {
      title: string;
      type: Options["mediaType"];
      folder?: string;
    },
    progressCallback: (progress: number) => void
  ): Promise<{ uri: string; type: string; width: number; height: number }> {
    return new Promise(async (resolve, reject) => {
      try {
        //#region [SECTION ]  PICKER
        const media = await ImageCropPicker.openPicker({
          mediaType: type,
          writeTempFile: false,
          // compressImageMaxHeight: 1500,
          // compressImageMaxWidth: 1500,
        }).catch((error) => {
          throw new Error(error.message);
        });

        //#endregion

        //#region [SECTION ]  FILE PATH
        const {
          title,
          type,
          folder = `apptakeoff/apps/${Backend.firestoreHandler._config.variables.appTitle.replace(
            / /g,
            ""
          )}`,
        } = options;

        const uploadData = await this.fetchUploadData(folder);
        const { uploadURL, upload_params } = uploadData;

        //#endregion

        //#region [SECTION ]  Upload and get url link
        if (!R.isNil(media)) {
          if (Array.isArray(media)) {
            //! We're not supporting multiple media atm
            throw Error(ERR.ARRAY_MEDIA);
          } else {
            Toasty({
              text1: "Getting media info...",
              type: "info",
              visibilityTime: 6000,
            });
            //#region [SECTION ] FORM PREPARATION
            const localUri = "file:///" + media?.path?.split("file:/").join("");
            const file: any = {
              name: title,
              title,
              uri: localUri,
              type: media.mime,
            };

            const form: FormData = new FormData();
            form.append("file", file, file.name);
            form.append("api_key", upload_params.api_key);
            form.append("eager", upload_params.eager);
            form.append("folder", upload_params.folder);
            form.append("signature", upload_params.signature);
            form.append("timestamp", upload_params.timestamp);
            form.append("unique_filename", upload_params.unique_filename);
            form.append("use_filename", upload_params.use_filename);
            // form.append("upload_preset", uploadData.upload_preset);

            console.log(
              "📹 uploading " +
                upload_params.use_filename +
                ", " +
                file.name +
                " to folder " +
                upload_params.folder +
                " type: ",
              file.type
            );
            //#endregion

            //#region [SECTION ] FETCHING
            const uploadResponse = await axios(uploadURL, {
              method: "POST",
              headers: {
                "Content-Type": "application/json; charset=UTF-8",
              },
              data: form,
              onUploadProgress: (p) => {
                let uploadPercent = (p.loaded / p.total) * 100;
                progressCallback(uploadPercent);
              },
            }).catch((error) =>
              console.warn("📹 Error axios: ", error.message)
            );

            let responseData: typeof MOCK_RESPONSE["data"] =
              uploadResponse?.data;

            // const videoURL: string = uploadResponse.data["secure_url"];

            // console.log(
            //   "📹 uploaded video json: ",
            //   JSON.stringify(uploadResponse)
            // );
            //#endregion
            resolve({
              uri: responseData["secure_url"],
              type: responseData["resource_type"],
              width: responseData["width"],
              height: responseData["height"],
            });
          }
        }
      } catch (error) {
        console.log("📹 error in selectAndUpload(): ", error);
        switch (true) {
          case error.message.includes("User cancelled image selection"):
            reject("User cancelled image selection");
            // Toasty({ text1: "Cancelled", type: "error" });
            // reject({ code: ERROR_PICKER.PHOTO_GALLERY_CANCELLED, error });
            break;
          default:
            reject("Error uploading user media");
            Toasty({
              text1: "Error uploading your media",
              text2: "Please try again",
              type: "error",
            });
            break;
        }

        throw error;
      }
    });
  }

  /**
   * ###  Returns a URI to the thumbnail image for the video
   *
   * @author jm_francis
   * @version 1.1.12
   *
   * @example
   * const thumbnailURI = getThumbnailFromURL("cloudinary.com/my_url.mp4")
   */
  static getThumbnailFromURL(videoURL: string): string {
    return videoURL
      .replace(".mp4", ".jpg")
      .replace(".mov", ".jpg")
      .replace("HEIC", "jpg");
  }
  /**
   * ###  Returns a modified URI that blurs any extra space in the video or image
   *
   * @author jm_francis
   * @version 1.2.12
   *
   * @example
   * const url = blurredBorderFromURL("cloudinary.com/my_url.mp4")
   */
  static blurredBorderFromURL(url: string): string {
    return url.replace(
      "/upload",
      "/upload/c_pad,h_320,w_480,b_blurred:400:15/"
    );
  }
}

/**
 * ###  Mock response for FileNetwork
 * @version 1.2.3
 */
const MOCK_RESPONSE = {
  data: {
    asset_id: "a8ebf3b0e38cc8fd757cb4574e4fa89f",
    public_id:
      "apptakeoff/apps/PrimericaTemplate/posst_5FsMmuSIAOXpBgNQMNNJTtGC09y1_90o_ikxiq5",
    version: 1612376116,
    version_id: "41c4b74b6669e020da510ce64042b4d4",
    signature: "5ae17d82882b68b50fbc0e36b613f3a7a321de88",
    width: 1080,
    height: 2220,
    format: "jpg",
    resource_type: "image",
    created_at: "2021-02-03T18:15:16Z",
    tags: [],
    bytes: 362224,
    type: "upload",
    etag: "8e66dc9f63bcdd44b982ecd1cc08e8e3",
    placeholder: false,
    url:
      "http://res.cloudinary.com/https-apptakeoff-com/image/upload/v1612376116/apptakeoff/apps/PrimericaTemplate/posst_5FsMmuSIAOXpBgNQMNNJTtGC09y1_90o_ikxiq5.jpg",
    secure_url:
      "https://res.cloudinary.com/https-apptakeoff-com/image/upload/v1612376116/apptakeoff/apps/PrimericaTemplate/posst_5FsMmuSIAOXpBgNQMNNJTtGC09y1_90o_ikxiq5.jpg",
    access_mode: "public",
    original_filename: "posst_5FsMmuSIAOXpBgNQMNNJTtGC09y1_90o",
  },
  status: 200,
  headers: {
    "transfer-encoding": "chunked",
    "x-request-id": "671bf673b7ef584d63ad33fe69801660",
    connection: "keep-alive",
    "content-type": "application/json; charset=utf-8",
    "x-ua-compatible": "IE=Edge,chrome=1",
    "x-xss-protection": "1; mode=block",
    vary: "Accept-Encoding",
    etag: 'W/"3fcca5016e3d7eccf019af6ce97cf210"',
    date: "Wed, 03 Feb 2021 18:15:17 GMT",
    status: "200 OK",
    server: "cloudinary",
    "cache-control": "max-age=0, private, must-revalidate",
  },
  config: {
    url: "https://api.cloudinary.com/v1_1/https-apptakeoff-com/auto/upload",
    method: "post",
    data: {
      _parts: [
        [
          "file",
          {
            name: "posst_5FsMmuSIAOXpBgNQMNNJTtGC09y1_90o",
            title: "posst_5FsMmuSIAOXpBgNQMNNJTtGC09y1_90o",
            uri:
              "file://///data/user/0/com.apptakeoff.primericatemplate/cache/react-native-image-crop-picker/Screenshot_20210203-093820_PrimericaTemplate.jpg",
            type: "image/jpeg",
          },
        ],
        ["api_key", "766448662923757"],
        ["eager", ""],
        ["folder", "apptakeoff/apps/PrimericaTemplate"],
        ["signature", "1133fcdf68cd656323d27ffaa506b452267e6d16"],
        ["timestamp", 1612376115.68],
        ["unique_filename", true],
        ["use_filename", true],
      ],
    },
    headers: { Accept: "application/json, text/plain, */*" },
    transformRequest: [null],
    transformResponse: [null],
    timeout: 0,
    xsrfCookieName: "XSRF-TOKEN",
    xsrfHeaderName: "X-XSRF-TOKEN",
    maxContentLength: -1,
    maxBodyLength: -1,
  },
  request: {
    UNSENT: 0,
    OPENED: 1,
    HEADERS_RECEIVED: 2,
    LOADING: 3,
    DONE: 4,
    readyState: 4,
    status: 200,
    timeout: 0,
    withCredentials: true,
    upload: {},
    _aborted: false,
    _hasError: false,
    _method: "POST",
    _response:
      '{"asset_id":"a8ebf3b0e38cc8fd757cb4574e4fa89f","public_id":"apptakeoff/apps/PrimericaTemplate/posst_5FsMmuSIAOXpBgNQMNNJTtGC09y1_90o_ikxiq5","version":1612376116,"version_id":"41c4b74b6669e020da510ce64042b4d4","signature":"5ae17d82882b68b50fbc0e36b613f3a7a321de88","width":1080,"height":2220,"format":"jpg","resource_type":"image","created_at":"2021-02-03T18:15:16Z","tags":[],"bytes":362224,"type":"upload","etag":"8e66dc9f63bcdd44b982ecd1cc08e8e3","placeholder":false,"url":"http://res.cloudinary.com/https-apptakeoff-com/image/upload/v1612376116/apptakeoff/apps/PrimericaTemplate/posst_5FsMmuSIAOXpBgNQMNNJTtGC09y1_90o_ikxiq5.jpg","secure_url":"https://res.cloudinary.com/https-apptakeoff-com/image/upload/v1612376116/apptakeoff/apps/PrimericaTemplate/posst_5FsMmuSIAOXpBgNQMNNJTtGC09y1_90o_ikxiq5.jpg","access_mode":"public","original_filename":"posst_5FsMmuSIAOXpBgNQMNNJTtGC09y1_90o"}',
    _url: "https://api.cloudinary.com/v1_1/https-apptakeoff-com/auto/upload",
    _timedOut: false,
    _trackingName: "unknown",
    _incrementalEvents: false,
    responseHeaders: {
      "transfer-encoding": "chunked",
      "X-Request-Id": "671bf673b7ef584d63ad33fe69801660",
      Connection: "keep-alive",
      "Content-Type": "application/json; charset=utf-8",
      "X-UA-Compatible": "IE=Edge,chrome=1",
      "X-XSS-Protection": "1; mode=block",
      Vary: "Accept-Encoding",
      ETag: 'W/"3fcca5016e3d7eccf019af6ce97cf210"',
      Date: "Wed, 03 Feb 2021 18:15:17 GMT",
      Status: "200 OK",
      Server: "cloudinary",
      "Cache-Control": "max-age=0, private, must-revalidate",
    },
    _requestId: null,
    _headers: { accept: "application/json, text/plain, */*" },
    _responseType: "",
    _sent: true,
    _lowerCaseResponseHeaders: {
      "transfer-encoding": "chunked",
      "x-request-id": "671bf673b7ef584d63ad33fe69801660",
      connection: "keep-alive",
      "content-type": "application/json; charset=utf-8",
      "x-ua-compatible": "IE=Edge,chrome=1",
      "x-xss-protection": "1; mode=block",
      vary: "Accept-Encoding",
      etag: 'W/"3fcca5016e3d7eccf019af6ce97cf210"',
      date: "Wed, 03 Feb 2021 18:15:17 GMT",
      status: "200 OK",
      server: "cloudinary",
      "cache-control": "max-age=0, private, must-revalidate",
    },
    _subscriptions: [],
    responseURL:
      "https://api.cloudinary.com/v1_1/https-apptakeoff-com/auto/upload",
  },
};

enum ERR {
  ARRAY_MEDIA = "ARRAY_MEDIA",
}
