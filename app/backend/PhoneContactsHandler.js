//@ts-check
import Backend from "backend/";
import { Alert, AppState } from "react-native";
import Contacts from "react-native-contacts";
import {
  check as checkPermission,
  PERMISSIONS,
  RESULTS,
} from "react-native-permissions";

const _callAll = (funcs, value) => {
  for (f in funcs) funcs[f](value);
};

class PhoneContactsHandler {
  // ANCHOR: Variables
  _phoneContacts = null;

  // ANCHOR: Callbacks
  _phoneContactsCallbacks = [];

  // ANCHOR: Subscribers
  phoneContacts(callback) {
    if (this._phoneContacts) callback(this._phoneContacts);
    this._phoneContactsCallbacks.push(callback);
  }

  // ANCHOR: Update functions
  async createNewPhoneContact(data, listTitle) {
    if (!data.givenName) data.givenName = "";
    if (!data.familyName) data.familyName = "";
    // Contacts.addContact(data, (error, newContact) => {
    Contacts.openContactForm(data, (error, newContact) => {
      if (error) {
        Alert.alert("Something went wrong.", "Unable to access your contacts.");
        return error;
      }

      this._phoneContacts = this._phoneContacts.concat([newContact]);
      _callAll(this._phoneContactsCallbacks, this._phoneContacts);

      Contacts.openExistingContact(newContact, (err, contact) => {
        if (err) {
          Alert.alert(
            "Failed to create a new contact in your phone",
            "Please make sure you have provided permission in your settings."
          );
          return err;
        }
        if (!contact) {
          Contacts.deleteContact(data, () => {});
          return null;
        }

        this._phoneContacts = this._phoneContacts.concat([contact]);
        _callAll(this._phoneContactsCallbacks, this._phoneContacts);

        // NOTE: Updates listBuilderHandler
        Backend.listBuilderHandler
          .makeContactsOne(data, contact, listTitle)

          .then((_contact) => {
            this._phoneContacts = this._phoneContacts.concat([contact]);
            _callAll(this._phoneContactsCallbacks, this._phoneContacts);
            return _contact;
          });
      });
    });
  }

  // ANCHOR: init
  init() {
    this.loadPhoneContacts();
    AppState.addEventListener("change", this._appStateChange.bind(this));
  }
  async loadPhoneContacts() {
    let permission;
    if (Platform.OS === "ios") {
      permission = await checkPermission(PERMISSIONS.IOS.CONTACTS);
    } else {
      permission = await checkPermission(PERMISSIONS.ANDROID.READ_CONTACTS);
    }
    if (permission !== RESULTS.GRANTED) {
      // Alert.alert(
      //   "Failed to load phone contacts. Please provide permission in your settings!"
      // );
      return;
    }

    Contacts.getAll((err, contacts) => {
      if (err) return; // TODO: handle error?
      this._phoneContacts = contacts;
      _callAll(this._phoneContactsCallbacks, this._phoneContacts);
    });
  }

  // ANCHOR: App State Function
  _appStateChange(state) {
    if (state === "active") this.loadPhoneContacts();
  }
}

export const phoneContactsHandler = new PhoneContactsHandler();
