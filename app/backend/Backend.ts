import { RNFirebase } from "react-native-firebase";
import { accountHandler } from "./AccountHandler";
import { adminHandler } from "./AdminHandler";
import DropboxAPI from "./apis/DropboxAPI";
import FileNetwork from "./apis/FileNetwork";
import { authHandler } from "./AuthHandler/AuthHandler";
import { calendarHandler } from "./Calendar/CalendarHandler";
import { ContactsHandler } from "./ContactsHandler/ContactsHandler";
import { firebasePages } from "./FirebasePages";
import { firestoreHandler } from "./FirestoreHandler/FirestoreHandler";
import { levelsHandler } from "./LevelsHandler";
import { listBuilderHandler } from "./ListBuilderHandler";
import { localHandler } from "./LocalHandler";
import notificationService from "./NotificationHandler/NotificationService";
import notificationHandler from "./NotificationHandler/NotificationHandler";
import { notificationFirestore } from "./NotificationHandler/NotificationFirestore";
import { permissionHandler } from "./PermissionHandler";
import { phoneContactsHandler } from "./PhoneContactsHandler";
import { scoreboardHandler } from "./ScoreboardHandler/ScoreboardHandler";
import { exploreHandler } from "./ExploreHandler/ExploreHandler";

const Backend = {
  accountHandler,
  firestoreHandler,
  levelsHandler,
  firebasePages,
  localHandler,
  calendarHandler,
  listBuilderHandler,
  scoreboardHandler,
  phoneContactsHandler,
  permissionHandler,
  adminHandler,
  ContactsHandler,
  authHandler,
  notificationService,
  notificationHandler,
  notificationFirestore,
  DropboxAPI,
  FileNetwork,
  exploreHandler,
  init: (loggedInCallback: () => void, needsToRegisterCallback: () => void) => {
    phoneContactsHandler.init(); //* REVIEW this causes a crash. Update: fixed
    scoreboardHandler.initWhenReady();
    firestoreHandler.initConfig((config) => {
      // executes once after first config fetch
      if (config.variables.enablePushNotifications) {
        notificationFirestore.init();
      }
      listBuilderHandler.init(); // inits when account becomes available
    });
    accountHandler.init(
      (user: RNFirebase.User) => {
        // already logged in
        firestoreHandler.initWithUser(user);
        firebasePages.fetchPagesIfNeeded();
        loggedInCallback();

        calendarHandler.init();

        notificationHandler.init();

        exploreHandler.init();
      },
      () => {
        // not logged in
        needsToRegisterCallback();

        // calendarHandler.init()
      }
    );
  },
};

export default Backend;
