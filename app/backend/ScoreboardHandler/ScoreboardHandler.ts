import Backend, { UserSchema } from "backend/";
import { AsyncStorage } from "react-native";
import firebase from "react-native-firebase";
import { dScoreboard } from "./scoreboard-handler.props";

const store = "@scoreboard";

const _callAll = (funcs: Array<Function>, value: any) => {
  for (let f in funcs) funcs[f](value);
};

const datediff = (first, second) => {
  return Math.abs(Math.round((second - first) / (1000 * 60 * 60 * 24)));
};

export interface dAchievement {
  prompt: string;
  image: string;
}

class ScoreboardHandler {
  // callbacks
  _scoreboardCallbacks: ((scores: dScoreboard[]) => void)[] = [];
  _fiveToStayAliveCallbacks: Array<Function> = [];
  _fiveToStayAliveAchievementsCallbacks: ((
    scores: dAchievement[]
  ) => void)[] = [];

  // variables
  _scoreboards: dScoreboard[] = null;
  _fiveToStayAlive = null;
  _fiveToStayAliveAchievements: dAchievement[] = null;

  /**
   * ### Fires callback anytime a scoreboard or score changes and gives all updated scoreboards in the parameter
   * All scoreboards come pre-sorted in their proper order
   * All scores in scoreboard come pre-sorted with highest scores coming first [{score: 100}, {score: 99}]
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.scoreboards(setScoreboards)
   */
  scoreboards(callback: (scores: dScoreboard[]) => void) {
    this._scoreboardCallbacks.push(callback);
    if (this._scoreboards) callback(this._scoreboards);
  }
  /**
   * Fires callback with users five to stay alive data
   */
  fiveToStayAlive(callback: Function) {
    if (this._fiveToStayAlive) callback(this._fiveToStayAlive);
    this._fiveToStayAliveCallbacks.push(callback);
  }
  /**
   * ### Fires callback with latest achievements [{ prompt: string, image: string }]
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.fiveToStayAliveAchievements(achievement => {})
   */
  fiveToStayAliveAchievements(callback: Function) {
    if (this._fiveToStayAliveAchievements)
      callback(this._fiveToStayAliveAchievements);
    this._fiveToStayAliveAchievementsCallbacks.push(callback);
  }
  /**
   * ### Calculates and returns current 5 to stay alive streak for user
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * const streak = scoreboardHandler.fiveToStayAliveStreak()
   */
  fiveToStayAliveStreak(): number {
    const counts = this._fiveToStayAlive.counts
      .sort((a, b) => a.date.seconds < b.date.seconds)
      .filter((i) => i.count === 5);
    let streak = 0;
    for (let x = 0; x < counts.length; x++) {
      const obj = counts[x];
      const today = new Date();
      if (
        x === 0 &&
        // && !dateIsXBehindDate(obj.date.toDate(),2,today)
        datediff(obj.date.toDate(), today) > 1
      )
        return 0;
      if (x === 0) continue;
      const lastObj = counts[x - 1];
      if (datediff(lastObj.date.toDate(), obj.date.toDate()) <= 1) streak += 1;
    }
    return streak;
  }

  /**
   * ### Returns a random achievement that the user has not yet seen
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * randomFiveToStayAliveAchievement((achievement) => {  })
   */
  randomFiveToStayAliveAchievement(callback: Function) {
    this.fiveToStayAliveAchievements((achievements: dAchievement[]) => {
      if (achievements.length === 0) return;
      AsyncStorage.getItem(`${store}:usedAchievements`, (err, _data) => {
        const data = typeof _data === "string" ? JSON.parse(_data) : {};
        const usedOnes = data.achievements ? data.achievements : [];
        const filtered: dAchievement[] = achievements.filter((a) => {
          for (let u in usedOnes)
            if (usedOnes[u].prompt == a.prompt) return false;
          return true;
        });
        if (filtered.length === 0) {
          AsyncStorage.setItem(
            `${store}:usedAchievements`,
            JSON.stringify({
              achievements: [achievements[0]],
            })
          );
          callback(achievements[0]);
        } else {
          const randomIndex = Math.floor(Math.random() * filtered.length + 0);
          AsyncStorage.setItem(
            `${store}:usedAchievements`,
            JSON.stringify({
              achievements: usedOnes.concat([filtered[randomIndex]]),
            })
          );
          callback(filtered[randomIndex]);
        }
      });
    });
  }

  /**
   * ### Sets currently logged in users score in provided scoreboard
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.setMyScore(250, someScoreboard)
   */
  setMyScore(score: number, scoreboard: dScoreboard) {
    const uid = Backend.firestoreHandler._account.uid;
    const name = Backend.firestoreHandler._account.name;
    let foundit = false;
    let people = scoreboard.people.map((p) => {
      if (
        p.uid === Backend.firestoreHandler._account.uid ||
        (!p.uid && p.name === name)
      ) {
        foundit = true;
        // If they updated the name then reflect that when they update the score as well
        p.name = Backend.firestoreHandler._account.name;
        return { ...p, uid, score };
      }
      return p;
    });
    if (!foundit)
      people = people.concat([
        {
          uid,
          name: Backend.firestoreHandler._account.name,
          score,
        },
      ]);
    const updateObj = {};
    updateObj[scoreboard.id] = { ...scoreboard, people };
    return firebase
      .firestore()
      .collection("scoreboard")
      .doc("scores")
      .update(updateObj);
  }

  /**
   * ### Adds 1 to the x/5 for the Five to Stay Alive feature
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.addFiveToStayAlive()
   */
  addFiveToStayAlive() {
    const uid = Backend.firestoreHandler._account.uid;
    const updateObj = {};
    const me = this._fiveToStayAlive ? this._fiveToStayAlive : {};
    let counts = me.counts ? me.counts : [];
    const currentObj = this.extractFiveToStayAliveCount(true);
    const todaydate = new Date();
    const todaydateTimestamp = firebase.firestore.Timestamp.fromDate(todaydate);
    const today = !currentObj
      ? {
          count: 1,
          date: todaydateTimestamp,
        }
      : {
          ...currentObj,
          count: currentObj.count + 1,
        };
    let didOverride = false;
    counts = counts.map((i) => {
      if (this._isDateEqualToDate(i.date.toDate(), todaydate)) {
        didOverride = true;
        return today;
      }
      return i;
    });
    if (!didOverride) counts.push(today);

    console.log("counts: " + JSON.stringify(counts));

    updateObj[uid] = {
      name: Backend.firestoreHandler._account.name,
      ...me,
      counts,
    };

    return firebase
      .firestore()
      .collection("scoreboard")
      .doc("fiveToStayAlive")
      .update(updateObj);
  }

  /**
   * ### Resets all the scores on given scoreboard (leave scoreboard title blank to reset all scoreboards)
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example:
   * scoreboardHandler.resetScoreboard("Top 10 People")
   */
  resetScoreboard(title?: string) {
    // if title is specified, just that one scoreboard will be reset, otherwise, all scoreboards will be reset
    const newScores = {};
    for (let s in this._scoreboards) {
      const page: dScoreboard = this._scoreboards[s];
      if (!title || title == page.title) {
        page.people = [];
        newScores[page.id] = page;
      }
    }
    return firebase
      .firestore()
      .collection("scoreboard")
      .doc("scores")
      .update(newScores);
  }
  /**
   * ### Resets all scores on all scoreboards
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.resetAllScoreboards()
   */
  resetAllScoreboards() {
    return this.resetScoreboard();
  }
  /**
   * ### Update given scoreboard with any given properties
   * Provided properties will overwrite old ones (only do use to update people array, instead use .setMyScore or .updateSomeonesScore function)
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.updateScoreboard("Top 10 People", { title: "Best People Ever" })
   */
  updateScoreboard(title: string, data: Object) {
    const newScores = {};
    for (let s in this._scoreboards) {
      let page: any = this._scoreboards[s];
      if (page.title === title) {
        page = { ...page, ...data };
      }
      newScores[page.id] = page;
    }
    return firebase
      .firestore()
      .collection("scoreboard")
      .doc("scores")
      .update(newScores);
  }

  /**
   * ### Returns the scoreboard that has the given title
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * const scoreboard = scoreboardHandler.getScoreboardByTitle("All Timers")
   */
  getScoreboardByTitle(title: string): dScoreboard {
    const scoreboards = this._scoreboards;
    for (let s in scoreboards) {
      const _scoreboard = scoreboards[s];
      if (_scoreboard.title === title) {
        return _scoreboard;
      }
    }
    return null;
  }

  /**
   * @deprecated Use scoreboardHandler.setUsersScore now instead
   * ### Will update the given user's score on the given scoreboard
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.updateSomeonesScore("Top 10 People", "Joey Smith", 45)
   */
  updateSomeonesScore(title: string, uid: string, newScore: number) {
    const scoreboard = { ...this.getScoreboardByTitle(title) };
    for (let p in scoreboard.people) {
      const _person = scoreboard.people[p];
      if (_person.uid == uid || (!_person.uid && _person.name === uid)) {
        scoreboard.people[p] = { ..._person, score: newScore };
      }
    }
    return this.updateScoreboard(title, scoreboard);
  }

  /**
   * ### Set the score of the specific user
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.setUsersScore(scoreboard, user.uid, 500)
   */
  setUsersScore(scoreboard: dScoreboard, uid: string, score: number) {
    const user: UserSchema = Backend.adminHandler.getUserByUid(uid);
    let didUpdate = false;
    for (let p in scoreboard.people) {
      const _person = scoreboard.people[p];
      if (_person.uid == uid) {
        didUpdate = true;
        scoreboard.people[p] = { ..._person, score };
      }
    }
    if (!didUpdate) {
      // if the user did not already have a score in the scoreboard, add a new one for them
      scoreboard.people.push({
        uid,
        name: user.name,
        score,
      });
    }
    return this.updateScoreboard(scoreboard.title, scoreboard);
  }

  /**
   * ### Add a score not related to a specific user
   *
   * @author jm_francis
   * @version 1.2.22
   *
   * @example
   * scoreboardHandler.setNonuserScore(scoreboard, name, 500)
   */
  setNonuserScore(scoreboard: dScoreboard, name: string, score: number) {
    let didUpdate = false;
    for (let p in scoreboard.people) {
      const _person = scoreboard.people[p];
      if (_person.name == name) {
        didUpdate = true;
        scoreboard.people[p] = { ..._person, score };
      }
    }
    if (!didUpdate) {
      // if the user did not already have a score in the scoreboard, add a new one for them
      scoreboard.people.push({
        name,
        score,
      });
    }
    return this.updateScoreboard(scoreboard.title, scoreboard);
  }

  /**
   * ### Create a new scoreboard with given title
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.newScoreboard("Some New Scoreboard")
   */
  newScoreboard(title: string) {
    const newScores = {};
    let highestPosition: number = 0;
    for (let s in this._scoreboards) {
      const page = this._scoreboards[s];
      if (page.position && page.position > highestPosition)
        highestPosition = page.position;
      newScores[page.id] = page;
    }
    highestPosition++;
    const newScoreboard = {
      title,
      id: title,
      position: highestPosition,
      people: [],
    };
    newScores[newScoreboard.id] = newScoreboard;
    return firebase
      .firestore()
      .collection("scoreboard")
      .doc("scores")
      .update(newScores);
  }
  /**
   * ### Delete scoreboard with given title
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.deleteScoreboard("My Favorite Scoreboard")
   */
  deleteScoreboard(title: string) {
    const newScores = {};
    for (let s in this._scoreboards) {
      const page = this._scoreboards[s];
      if (page.title === title) continue;
      newScores[page.id] = page;
    }
    return firebase
      .firestore()
      .collection("scoreboard")
      .doc("scores")
      .set(newScores);
  }

  /**
   * ### Returns today's five to stay alive count for currently logged in user
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * const count = scoreboardHandler.extractFiveToStayAliveCount()
   */
  extractFiveToStayAliveCount(
    giveEntireObject: Boolean = false
  ): number | Object | any {
    if (!this._fiveToStayAlive) return 0;
    const counts = this._fiveToStayAlive.counts;
    for (let c in counts) {
      const obj = counts[c];
      const date = obj.date.toDate();
      const today = new Date();
      if (this._isDateEqualToDate(date, today))
        return giveEntireObject === true ? obj : obj.count;
    }
    return 0;
  }
  /**
   * ### Checks to see if both dates are equal in day and year
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * const isEqual = this._isDateEqualToDate(yesterday, today)
   */
  _isDateEqualToDate(date1: Date, date2: Date): Boolean {
    if (
      date1.getDate() == date2.getDate() &&
      date1.getDay() == date2.getDay() &&
      date1.getFullYear() == date2.getFullYear()
    )
      return true;
    return false;
  }

  didInit = false;

  /**
   * ### Runs the ._loadDatabase function as soon as the firestoreHandler._account exists
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * scoreboardHandler.initWhenReady()
   */
  initWhenReady() {
    Backend.firestoreHandler.account(
      (() => {
        if (this.didInit) return;
        this.didInit = true;
        this._loadDatabase();
      }).bind(this)
    );
  }

  /**
   * ### Returns given scoreboard with all scores sorted from greatest to least
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * const updatedScoreboard = scoreboardHandler.sortScoreboardScores(someScoreboard)
   */
  sortScoreboardScores(_scoreboard: dScoreboard): dScoreboard {
    const scoreboard: dScoreboard = { ..._scoreboard };
    const sortedScores = scoreboard.people.sort((a, b) => b.score - a.score);
    scoreboard.people = sortedScores;
    return scoreboard;
  }

  /**
   * ### Loads all data from firestore needed for scoreboard features
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * this._loadDatabase()
   */
  _loadDatabase() {
    firebase
      .firestore()
      .collection("scoreboard")
      .doc("config")
      .collection("scoreboardAchievements")
      .get()
      .then((query) => {
        if (!query) return;
        const achievements = [];
        query.forEach((doc) => {
          const data = doc.data();
          achievements.push(data);
        });
        this._fiveToStayAliveAchievements = achievements;
        _callAll(
          this._fiveToStayAliveAchievementsCallbacks,
          this._fiveToStayAliveAchievements
        );
      });
    firebase
      .firestore()
      .collection("scoreboard")
      .onSnapshot((query) => {
        const uid = Backend.firestoreHandler.uid;
        query.forEach((q) => {
          const key = q.id;
          const data: any = q.data();
          if (key === "scores") {
            const scoreboards: dScoreboard[] = [];
            for (let k in data)
              scoreboards.push(
                this.sortScoreboardScores({ ...data[k], id: k })
              );
            const sortedScores: dScoreboard[] =
              scoreboards.length > 0 && scoreboards[0].position
                ? scoreboards.sort((a, b) => a.position - b.position)
                : scoreboards;
            // removes all scores that are 0
            this._scoreboards = sortedScores.map((sboard) => {
              sboard.people = sboard.people.filter(
                (person) => person?.score > 0
              );
              return sboard;
            });
            _callAll(this._scoreboardCallbacks, this._scoreboards);
          } else if (key === "fiveToStayAlive") {
            const me = data[uid];
            if (me) {
              this._fiveToStayAlive = me;
              _callAll(this._fiveToStayAliveCallbacks, this._fiveToStayAlive);
            }
          }
        });
        if (!this._fiveToStayAlive) {
          console.log("fiveToStayAlive init");
          firebase
            .firestore()
            .collection("scoreboard")
            .doc("fiveToStayAlive")
            .set({});
        }
      });
  }
}

export const scoreboardHandler = new ScoreboardHandler();
