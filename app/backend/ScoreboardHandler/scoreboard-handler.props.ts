/**
 * ### A user's score in a scoreboard
 * 
 * @author jm_francis
 * @version 1.1.26
 * 
 * @example
 * {
 *  uid,
 *  name: "Jeremy Francis",
 *  score: 500,
 * }
 * 
 * @example
 * {
 *  name: "Team 1",
 *  score: 6500
 * }
 */
export interface dScore {
  uid?: string;
  name: string;
  score: number;
}
export interface dScoreboard {
  title: string;
  subtitle?: string,
  id: string;
  position: number;
  people: dScore[];
}
