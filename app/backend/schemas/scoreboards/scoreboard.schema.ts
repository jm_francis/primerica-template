export interface ScoreboardSchema {
  title: string;
  subtitle?: string;
  id: string;
  position: number;
  people: dScore[];
}

export interface dScore {
  uid: string;
  name: string;
  score: number;
}

export const _MOCK_SCOREBOARD: ScoreboardSchema = {
  people: [{ uid: "adfsdf", name: "Khoi", score: 20 }],
  id: "mock_id_12903;",
  position: 0,
  title: "Mock Scoreboard",
  subtitle: "Mock Subtitle",
};
