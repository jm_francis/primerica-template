import { dListBuilderList } from "backend/FirestoreHandler/firestore-handler.props";
import { Timestamp } from "react-native-firebase/firestore";

/**
 * ###  User Doc Schema
 * -    Should be identical to such doc in FRBS.
 * ---
 * @version 1.1.20
 * @author jm_francis
 * ---
 * @example
 * const USER_DEFAULT = {
    uid: "",
    name: "",
    email: "",
    phoneNumber: "",
    createdAt: new Date(),
    admin: false,
    banned: false,
    allLevelsCompleted: false,
    notifications: {
      lastRead: new Date(Date.now() - 24 * 60 * 60 * 1000),
    },
    levels: {},
    listBuilder: {
      lists: [],
      shareTo: [],
    },
    imitate: false,
    team: null,
  };
 */
export interface UserSchema {
  uid?: string;
  /**
   * Firebase cloud messaging tokens (push notifications)
   * - each token represents a device that notifications can be sent to individually
   */
  fcmTokens?: string[];
  name?: string;
  email: string;
  phoneNumber?: string;
  profileImage?: string;
  createdAt: Date | Timestamp;
  /**
   * Whether or not the user has access to admin functionalities throughout the app
   */
  admin?: boolean;
  /**
   * If the user is banned, they will get kicked out of the app when opening it
   */
  banned?: boolean;
  /**
   * If the user has gone through all levels in the "New? Start here!" area, this is true, OR if an admin set this to true allowing them to bypass going through all the levels manually
   */
  allLevelsCompleted?: boolean;
  /**
   * Lists each level and the individual items of each level the user has completed
   */
  levels?: {};
  listBuilder?: {
    lists: dListBuilderList[];
    /**
     * All the user ids this user has given permission to view their personal profile, specifically their contact lists
     */
    shareTo: string[];
  };
  notifications?: {
    lastRead: Date | Timestamp;
  };
  /**
   * If user is currently imitating/simulating
   * another user's account to test their account
   */
  imitate?: boolean;
  /**
   * The team the user has joined, if they have joined a team
   */
  team?: dTeam;
}

export interface dTeam {
  teamName: string;
  /**
   * A user's role in their team
   * A regular member is just role null/undefined
   */
  role?: eTeamRole;
  /**
   * The team bundle being paid for in the particular team
   * Null/undefined is the standard
   */
  bundle?: eTeamBundle;
}

export enum eTeamRole {
  Owner = "Owner",
}

export enum eTeamBundle {
  Premier = "Premier",
}

export const USER_DEFAULT: UserSchema = {
  uid: "",
  name: "",
  email: "",
  phoneNumber: "",
  profileImage: null,
  createdAt: new Date(),
  admin: false,
  banned: false,
  allLevelsCompleted: false,
  notifications: {
    lastRead: new Date(Date.now() - 24 * 60 * 60 * 1000),
  },
  levels: {},
  listBuilder: {
    lists: [],
    shareTo: [],
  },
  imitate: false,
  team: null,
};

// interface dPersonali extends Partial<firebase.UserInfo> {}

// const MOCK_U_CLAIM = {
//   name: "Khoi TRan",
//   picture: "https://i.redd.it/iibrptucse951.png",
//   roles: ["ADMIN", "MEMBER"],
//   iss: "https://securetoken.google.com/primr-exp",
//   aud: "primr-exp",
//   auth_time: 1605968586,
//   user_id: "LCCRTnZd8gXiN2OasfdejkJdDjOIJS",
//   sub: "LCCRTnZd8gXiN2OasfdejkJdDjOIJS",
//   iat: 1606663708,
//   exp: 1606667308,
//   email: "drkhoi16@gmail.com",
//   email_verified: true,
//   firebase: {
//     identities: {
//       "google.com": ["116691316164218372162"],
//       email: ["drkhoi16@gmail.com"],
//     },
//     sign_in_provider: "google.com",
//   },
// };
