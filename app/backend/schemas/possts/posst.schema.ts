import firebase, { RNFirebase } from "react-native-firebase";
import { Timestamp } from "react-native-firebase/firestore";

/**
 * ### Posst Doc Schema
 *
 * ---
 * @version 1.1.20
 * @author  nguyenkhooi
 * ---
 * @example see MOCK_POSST
 */
export interface PosstSchema {
  _pid: string;
  author: {
    _id: string;
    /**
     * @deprecated maybe since name & avatar might be changed
     */
    name: string;
    /**
     * @deprecated maybe since name & avatar might be changed
     */
    avatar: string;
  };
  body: string;
  /**
   * The doc id of the page to open
   */
  goToPage?: string;
  /**
   * @deprecated soon
   */
  media?: {
    uri: string;
    type: "image" | "video";
  };
  medias: {
    uri: string;
    type: "image" | "video";
  }[];
  createdAt: Timestamp | RNFirebase.firestore.FieldValue;
  updatedAt: Timestamp | RNFirebase.firestore.FieldValue;
  scheduledAt: Timestamp | RNFirebase.firestore.FieldValue;
  /**
   * Array of uids who have liked the posst
   */
  likes: string[];
  status: POSST_STATUS;
  pinned: boolean;
  /**
   * The number of comments that there are, so you can know this without having to fetch the comments collection
   */
  commentCount: number;
}

export enum POSST_STATUS {
  /**
   * ###  Whether posst is posted or not
   * -  Useful for scheduled posst
   */
  POSTED = "POSTED",
  PENDING = "PENDING",
  DELETED = "DELETED",
  REPORTED = "REPORTED",
  SCHEDULED = "SCHEDULED",
}

//@ts-ignore
export const _MOCK_POSSTS = new Array(2).fill({
  _pid: "p000",
  author: {
    _id: "id_000",
    name: "Rice Cooker",
    avatar: "https://i.redd.it/iibrptucse951.png",
  },
  body: "This is a mock post",
  link: null,
  goToPage: null,
  medias: [{ uri: "https://i.redd.it/iibrptucse951.png", type: "image" }],
  createdAt: firebase.firestore.Timestamp.fromDate(new Date()),
  likes: [
    { uid: "id_111", name: "Khoi Trannn" },
    { uid: "id_222", name: "Jeremy Francis" },
    { uid: "id_333", name: "Doodle dude" },
  ],
  status: "POSTED",
  pinned: true,
}) as PosstSchema[];

/* {
  _pid: "ebf4Fe7es4tsgYuNcwWw",
  createdAt: {
    seconds: 1611253800,
    nanoseconds: 0,
  },
  author: {
    _id: " VzRw44ZSZxUQPXUi4fA1sh0J5hq2",
    avatar: "https://i.redd.it/iibrptucse951.png",
    name: "Rocket",
  },
  updatedAt: {
    seconds: 1611253800,
    nanoseconds: 0,
  },
  medias: [
    {
      type: "image",
      uri: "https://i.redd.it/iibrptucse951.png",
    },
  ],
  body: "This is a mock post",
  likescount: 0,
  likes: [
    {
      uid: " VzRw44ZSZxUQPXUi4fA1sh0J5hq2",
      name: "Some Dude",
    },
  ],
  status: "POSTED",
  scheduledAt: {
    seconds: 1611272040,
    nanoseconds: 0,
  },
} */
