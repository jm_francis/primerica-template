export type TeamSchema = {
  name: string;
  position: number;
  password?: string | number;
  zoom?: string;
  mediaItem: {
    logo?: string;
    color?: string;
    imageColor?: string;
  };
};
