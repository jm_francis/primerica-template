/**
 * ###  Schema for media pages
 * -  Custom page is set up by users,
 * containing their own media and contents
 * ---
 * @version 1.1.12
 * @author jm_francis
  * @example
  * const MediaPage = {
    id: "",
    zoom: undefined,
    password: undefined,
    mediaItem: {
      imageColor: undefined,
      color: undefined,
      link: undefined,
      logo: undefined,
      presetLogo: undefined,
      team: false,
      visible: false,
    },
    name: "",
    pageItem: {
      description: "",
      imageUrl: undefined,
      uploadImage: undefined,
      visible: false,
      link: undefined,
    },
    position: 0,
    content: [],
  };
  */
export interface MediaPageSchema {
  id: string;
  key?: string;
  /**
   * If this is a team that has a zoom link or ID it can go here
   */
  zoom?: string;
  /**
   * If the page requires a password to get into
   */
  password?: string | number;
  /**
   * Configuration for how the page might display and function in the "Media" area of the app
   */
  mediaItem: {
    /**
     * @deprecated Image displays as is now
     */
    imageColor?: string;
    /**
     * @deprecated There is only 1 default color now
     */
    color?: string;
    /**
     * Open a URL instead of a page
     */
    link?: string;
    /**
     * Give the page a custom icon
     */
    logo?: string;
    /**
     * Use an icon that the app supports locally
     */
    presetLogo?: string;
    /**
     * Whether or not this media page represents a team
     */
    team?: boolean;
    /**
     * If true or "true", will be displayed directly on the "Media" area of the app
     */
    visible?: boolean | string;
  };
  /**
   * The name of the page :D
   */
  name: string;
  /**
   * Configuration for how the page might display and function in the home page of the app
   * @deprecated Now using social feature to control content of the home screen
   */
  pageItem: {
    description?: string;
    imageUrl?: string;
    uploadImage?: string;
    visible: boolean | string;
    link?: string;
  };
  /**
   * Order in which the page might show up on the "Media" area of the app.
   * For example, a media page with position -3 will show up before one with position 1
   */
  position: number;
  /**
   * All of the content items the page contains
   */
  content?: dMediaPageItem[];
}

export interface dMediaPageItem {
  id?: string;
  /**
   * The title of this item!
   */
  title?: string;
  /**
   * Show some longer paragraph text on the screen
   */
  paragraph?: string;
  /**
   * A link to either a video or audio file on a supported service like an integrated Vimeo account, Cloudinary, or Dropbox
   */
  media?: string;
  /**
   * To open a web page in the app (or outside of the app)
   */
  url?: string;
  /**
   * If value is provided, this item will be a button that opens another MediaPage that exists in the app
   */
  topage?: string;
  /**
   * Order in which this item is displayed on the page
   * For example, an item with position -3 will show up before one with position 1
   */
  position: number;
}

const MOCK_MediaPage = {
  id: "",
  zoom: undefined,
  password: undefined,
  mediaItem: {
    imageColor: undefined,
    color: undefined,
    link: undefined,
    logo: undefined,
    presetLogo: undefined,
    team: false,
    visible: false,
  },
  name: "",
  pageItem: {
    description: "",
    imageUrl: undefined,
    uploadImage: undefined,
    visible: false,
    link: undefined,
  },
  position: 0,
  content: [],
};
