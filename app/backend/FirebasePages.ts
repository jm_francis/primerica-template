import Backend from "backend/";
import K from "constant-deprecated";
import { removePageExtensions } from "constant-deprecated/Config"; // TODO make functions class
import firebase, { RNFirebase } from "react-native-firebase";
import { MediaPageSchema, dMediaPageItem } from "./schemas";

export enum PageType {
  Home = "home",
  NewPerson = "new",
  Media = "media",
  Team = "team",
  Scoreboard = "scoreboard",
  ContactManager = "contactmanager",
  Other = "other", //* did not meet a category therefore may not be in use or is in use as a subpage with the "ToPage" field inside another page
  Dead = "dead", //* page.title is empty or page is null
}

interface dPageUpdateOptions {
  remove?: boolean;
  add?: boolean;
}

export interface dItemSearchResponse {
  mediaItem: dMediaPageItem;
  /**
   * The page the item was found on/exists on
   */
  fromPage: string;
}

const _callAll = (funcs: ((value: any) => void)[], value: any) => {
  for (let f in funcs) funcs[f](value);
};

/**
 * Returns the correct (enum) PageType for given page
 */
export function typeOfPage(page: MediaPageSchema): PageType {
  if (!page || !page.name) return PageType.Dead;
  const name = page.name;
  if (name === "New Recruits" || name === "New Representatives")
    return PageType.NewPerson;
  if (page.pageItem.visible === true) {
    if (
      name.includes("Competition Scoreboard") ||
      name.includes("competitionscoreboard")
    )
      return PageType.Scoreboard;
    if (
      name.includes("Contact Manager") ||
      name.includes("contactmanager") ||
      name.includes("myprospecting")
    )
      return PageType.ContactManager;
    return PageType.Home;
  }
  if (page.mediaItem.visible === true) {
    if (page.mediaItem.team === true) return PageType.Team;
    return PageType.Media;
  }
  return PageType.Other;
}

export default class FirebasePages {
  _pages: MediaPageSchema[] = null;

  /**
   * Finds the page with the given name and returns the page!
   */
  getPageWithName(name: string): MediaPageSchema {
    const lowercaseName = name.toLowerCase();
    const pgs = this._pages;
    for (let p in pgs) {
      const data = pgs[p];
      const lowercasePageName = data.name.toLowerCase();
      if (
        lowercaseName == lowercasePageName ||
        lowercaseName == removePageExtensions(lowercasePageName)
      ) {
        return data;
      }
    }
    return null;
  }
  /**
   * Finds the page with the given id and returns the page!
   */
  getPageById(id: string): MediaPageSchema {
    const pgs = this._pages;
    for (let p in pgs) {
      const data = pgs[p];
      if (data.id === id) return data;
    }
    return null;
  }

  _pagesCallbacks: ((pages: MediaPageSchema[]) => void)[] = [];
  pages(finish: (pages: MediaPageSchema[]) => void) {
    this._pagesCallbacks.push(finish);

    if (this._pages) {
      finish(this._pages);
      return;
    }

    this.fetchPagesIfNeeded();
  }

  /**
   * Updates the given page by adding, modifying, or removing an item in the .content[] of the page.
   * The options can be used as follows: {
   *    add: true, // will add the given item to the page.content
   *    remove: true, // will remove the item from the page.content
   *    // specifying nothing in the options will modify the given item that already exists in the page.
   * }
   * Ex: updatePageContent("Media to Share", {some_item}, { add: true })
   */
  async updatePageContent(
    pageName: string,
    item: Partial<dMediaPageItem>,
    options: dPageUpdateOptions = {}
  ) {
    let position = 0;
    const pageItems = this.getPageWithName(pageName).content;
    if (pageItems.length > 0)
      position = pageItems[pageItems.length - 1].position + 1;
    return await firebase.functions().httpsCallable("updatePageContent")({
      pageName,
      item: {
        title: "",
        paragraph: "",
        media: "",
        url: "",
        toPage: "",
        position,
        ...item,
      },
      options,
    });
  }

  didFetch = false;
  /**
   * Will fetch all the pages on the backend and open their pageContent .onSnapshot()s if not already executed before
   */
  fetchPagesIfNeeded() {
    if (Backend.firestoreHandler._account && !this.didFetch) {
      this.didFetch = true;
      this._fetchPages();
    }
  }

  pagesRef: RNFirebase.firestore.CollectionReference;
  initialLoadComplete: boolean = false;

  /**
   * Fetches all the pages in the backend and opens an onSnapshot for the pageContent for each page.
   */
  _fetchPages(finish?: (pages: MediaPageSchema[]) => void) {
    this.pagesRef = firebase.firestore().collection("pages");
    const pages = [];
    this.pagesRef
      .orderBy("position", "asc")
      .get()
      .then((query) => {
        let index = 0;
        let length = 0;
        query.forEach(() => {
          ++length;
        });
        query.forEach((doc) => {
          const pageData = doc.data();
          const page: MediaPageSchema = <MediaPageSchema>{
            ...pageData,
            id: doc.id,
          };
          const isLast = index == length - 1;

          this.pagesRef
            .doc(doc.id)
            .collection("pageContent")
            .orderBy("position", "asc")
            .onSnapshot((subquery) => {
              const content: dMediaPageItem[] = [];
              subquery.forEach((subdoc) => {
                const subdocData = <dMediaPageItem>{
                  id: subdoc.id,
                  ...subdoc.data(),
                };
                content.push(subdocData);
              });
              page.content = content;

              let _didfind = false;
              for (let p in pages) {
                const _pg = pages[p];
                if (_pg.id === page.id) {
                  pages[p] = page;
                  _didfind = true;
                  break;
                }
              }
              if (!_didfind) pages.push(page);
              this._pages = pages.filter(
                (p) => p.name && typeof p.name === "string"
              );

              if (isLast) {
                this.initialLoadComplete = true;
                if (finish) finish(pages);
                _callAll(this._pagesCallbacks, this._pages);
              }

              if (this.initialLoadComplete) {
                _callAll(this._pagesCallbacks, this._pages);
              }
            });
          ++index;
        });
      });
  }

  /**
   * Searches the page.content of every page on the backend to find desired piece of content
   * Excludes team pages and items that do not have a .media or .url.
   * Ex: const validItems: dMediaPageItem[] = itemSearch("Mike Sharp Testimony")
   */
  itemSearch(string: string): dItemSearchResponse[] {
    if (string.length < 1) return [];

    const loopThroughItems = (
      callback: (item: dMediaPageItem, fromPage: string) => void
    ) => {
      for (let p in this._pages) {
        const page = this._pages[p];
        if (page.mediaItem.visible === true && page.mediaItem.team === true)
          continue;
        for (let i in page.content) {
          // const item = {
          //   ...page.content[i],
          //   fromPage: page.name,
          // };
          const item: dMediaPageItem = page.content[i];
          if (item.media || item.url) callback(item, page.name);
        }
      }
    };

    let usedItems = "";
    const responses: dItemSearchResponse[] = [];
    loopThroughItems((mediaItem: dMediaPageItem, fromPage: string) => {
      if (mediaItem.title.toLowerCase().startsWith(string.toLowerCase())) {
        if (usedItems.includes(mediaItem.title)) return;
        responses.push({ mediaItem, fromPage });
        usedItems += mediaItem.title + ", ";
      }
    });
    loopThroughItems((mediaItem: dMediaPageItem, fromPage: string) => {
      if (mediaItem.title.toLowerCase().includes(string.toLowerCase())) {
        if (usedItems.includes(mediaItem.title)) return;
        responses.push({ mediaItem, fromPage });
        usedItems += mediaItem.title + ", ";
      }
    });

    return responses;
  }

  /**
   * Returns an array of all pages in the system and in their sorted position
   * (page.position)
   */
  getSortedPages(): MediaPageSchema[] {
    const noSortPages = this._pages;
    return noSortPages.sort((a, b) => {
      return a.position - b.position;
      // if (a.position > b.position) return 1;
      // if (a.position < b.position) return -1;
      // return 0;
    });
  }

  /**
   * Returns all pages that should display on the home screen
   * (page.pageItem.visible === true)
   */
  getHomePages(): MediaPageSchema[] {
    const allPages = this.getSortedPages();

    const pages = [];
    for (let p in allPages) {
      const somePage = allPages[p];
      if (
        typeOfPage(somePage) === PageType.Home ||
        (typeOfPage(somePage) === PageType.NewPerson &&
          somePage.pageItem.visible === true)
      )
        pages.push(somePage);
    }
    return pages;
  }

  /**
   * Returns all pages that are team pages
   * (page.mediaItem.visible === true && page.mediaItem.team === true)
   */
  getTeamPages(): MediaPageSchema[] {
    const allPages = this.getSortedPages();

    const pages = [];
    for (let p in allPages) {
      const somePage = allPages[p];
      if (typeOfPage(somePage) === PageType.Team) pages.push(somePage);
    }
    return pages;
  }

  /**
   * Returns the page for the team of the currently logged in user (assuming they are part of a team)
   */
  getMyTeamPage(): MediaPageSchema {
    const account = Backend.firestoreHandler._account;
    if (!account || !account.team || !this._pages) return null;
    const teamPages = this.getTeamPages();
    for (let t in teamPages) {
      const teamPage = teamPages[t];
      if (teamPage.name === account.team.teamName) return teamPage;
    }
    return null;
  }

  /**
   * Whether or not this is a level page (a level page has a completion percentage)
   */
  isLevelPage(name: string) {
    let shouldUseProgressBar = false;
    const keywords = K.Config.enablingKeywordsForProgressFeature;
    for (let k in keywords) {
      const kw = keywords[k];
      if (name?.toLowerCase().includes(kw)) shouldUseProgressBar = true;
    }
    return shouldUseProgressBar;
  }
}

export const firebasePages = new FirebasePages();
