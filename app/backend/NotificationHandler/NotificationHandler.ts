import Backend from "backend/";
import { Toasty } from "components";
import firebase from "react-native-firebase";
import PushNotification from "react-native-push-notification";
import { dNotification, NotificationStatus } from "./NotificationFirestore";

export function topicIdFromName(name: string): string {
  return `team_${name.replace(/ /g, "")}`;
}

class NotificationHandler {
  /**
   * Whether or not the subscriber functions ran yet
   * - Refer to the init() function for this
   */
  didSubscribe = false;

  /**
   * ### Fetch the notifications stored in firestore
   * - Needs to run when app starts up and user is logged in to subscribe the user to the "global" topic
   * - This function also subscribes the current user to the global topic
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationHandler.init()
   */
  async init() {
    // firebase.messaging().onMessage(this.onMessage.bind(this));

    if (Backend.accountHandler._user?.admin === true) {
      firebase.messaging().subscribeToTopic("admin");
      console.log("subscribed to admin");
    } else {
      firebase.messaging().unsubscribeFromTopic("admin");
    }
    firebase.messaging().subscribeToTopic("global");
    console.log("subscribed to global");
    Backend.firebasePages.pages(async (pages) => {
      if (this.didSubscribe) return;
      this.didSubscribe = true;
      const myTeamPage = Backend.firebasePages.getMyTeamPage();
      if (myTeamPage) {
        const topic = topicIdFromName(myTeamPage.name);
        firebase.messaging().subscribeToTopic(topic);
        console.log("subscribed to team channel: " + topic);
      }
    });
  }

  // async onMessage(message: any) {
  //   console.log("NEW MESSAGE: " + JSON.stringify(message));
  // }

  /**
   * ### Set the current user's fcmToken in firestore
   * - only updates firestore with new token if it has not yet been added
   *
   * @author jm_francis
   * @version 1.2.22
   *
   * @example
   * notificationHandler.setFCMToken()
   */
  async setFCMToken() {
    const tok = await firebase.messaging().getToken();
    const fcmTokens = Backend.firestoreHandler._account.fcmTokens;
    if (fcmTokens && !fcmTokens.includes(tok)) {
      fcmTokens.push(tok);
      Backend.firestoreHandler.updateAccount("", { fcmTokens });
    }
  }

  /**
   * ### Unsubscribes from a team channel
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationHandler.leaveTeam(team.name)
   */
  async leaveTeam(teamName: string) {
    const topic = topicIdFromName(teamName);
    Backend.notificationFirestore.leaveTeam();
    return await firebase.messaging().subscribeToTopic(topic);
  }

  /**
   * ### Sends push notification to everyone subscribed to topic "global"
   * - HINT: everyone is subscribed to "global" by default :D
   * - set "disableAddToFirestore" in data param to true to send the notification without adding into firestore/the bell area
   *   (example of this in ExploreHandler.createPosst)
   *
   * @author jm_francis
   * @version 1.2.12
   *
   * @example
   * notificationHandler.sendNotificationToEveryone("BIG DAY", "LESS GO TEAM WE GOT THIS!", {})
   */
  async sendNotificationToEveryone(
    title: string,
    message: string,
    data?: {
      disableAddToFirestore?: boolean;
    }
  ) {
    if (!Backend.firestoreHandler._config.variables.enablePushNotifications) {
      setTimeout(() => {
        Toasty({
          type: "info",
          text1: "Push notifications are disabled 😢",
          text2:
            "No notification was sent, but you can enable notifications in Admin Controls.",
        });
      }, 4500);
      return null;
    }
    if (!data?.disableAddToFirestore) {
      Backend.notificationFirestore.addNotification({
        channel: "global",
        title,
        message,
        status: NotificationStatus.SENT,
      });
    } else {
      delete data?.disableAddToFirestore;
    }
    return await firebase.functions().httpsCallable("tellEveryone")({
      title,
      message,
      data: {
        channel: "global",
        // ...(data ? data : {}),
      },
    });
  }

  /**
   * ### Sends a push notification to everyone in the specified team
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationHandler.sendNotificationToTeam(team.name, "Hey team!", {})
   */
  async sendNotificationToTeam(teamName: string, message: string, data?: any) {
    Backend.notificationFirestore.addNotification({
      channel: topicIdFromName(teamName),
      title: teamName,
      message,
    });
    return await firebase.functions().httpsCallable("tellATeam")({
      title: teamName,
      teamName,
      message,
      data: {
        channel: topicIdFromName(teamName),
      },
    });
  }

  _onNotification: any;
  _onRegister: any;

  /**
   * ### Should run when the app receives a notification from outer space
   * - Pretty sure this only executes if the notificaion is tapped on by the user
   *
   * @author unknown
   * @version unknown
   *
   * @example
   * notificationFirestore.onNotification(notification)
   */
  onNotification(notification) {
    console.log("Tapped notification: " + JSON.stringify(notification));

    if (typeof this._onNotification === "function") {
      this._onNotification(notification);
    }
  }

  onRegister(token: string) {
    console.log("NotificationHandler: registered token: ", token);

    if (typeof this._onRegister === "function") {
      this._onRegister(token);
    }
  }

  onAction(notification) {
    console.log("Notification action received:");
    console.log(notification.action);
    console.log(notification);

    if (notification.action === "Yes") {
      PushNotification.invokeApp(notification);
    }
  }

  // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  onRegistrationError(err) {
    console.log(err);
  }

  attachRegister(handler) {
    this._onRegister = handler;
  }

  attachNotification(handler) {
    this._onNotification = handler;
  }
}

const handler = new NotificationHandler();

PushNotification.configure({
  // (optional) Called when Token is generated (iOS and Android)
  onRegister: handler.onRegister.bind(handler),

  // (required) Called when a remote or local notification is opened or received
  onNotification: (notification) => {
    console.log("ON NOTI: " + JSON.stringify(notification));
    handler.onNotification(notification);
    // handler.onNotification.bind(handler);
  },

  // (optional) Called when Action is pressed (Android)
  onAction: handler.onAction.bind(handler),

  // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  onRegistrationError: handler.onRegistrationError.bind(handler),

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,

  /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   */
  requestPermissions: false,
});

export default handler;
