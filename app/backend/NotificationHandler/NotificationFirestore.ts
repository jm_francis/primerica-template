import Backend from "backend/Backend";
import firebase from "react-native-firebase";
import { Timestamp } from "react-native-firebase/firestore";
import { dateAsNumber } from "utilities/functions/calendar-functions/time";
import { topicIdFromName } from "./NotificationHandler";

const _callAll = (funcs: Function[], value: any) => {
  for (let f in funcs) funcs[f](value);
};

export interface dComment {
  uid: string;
  timestamp: Timestamp | Date;
  name: string;
  profileImage?: string;
  message: string;
}

export enum NotificationStatus {
  SENT = "sent",
  SCHEDULED = "scheduled",
}
export interface dNotification {
  channel: string;
  id?: string;
  status: NotificationStatus;
  /**
   * When the notification was sent or when it is scheduled to be sent
   */
  timestamp?: Timestamp | Date;
  title: string;
  message: string;
  video?: string;
  url?: string;
  comments: dComment[];
}

class NotificationFirestore {
  _global: dNotification[] = null;
  _team: dNotification[] = null;
  _scheduled: dNotification[] = null;

  _globalCallbacks: ((notis: dNotification[]) => void)[] = [];
  _teamCallbacks: ((notis: dNotification[]) => void)[] = [];
  _scheduledCallbacks: ((notis: dNotification[]) => void)[] = [];

  /**
   * ### Live callback for notifications sent to everyone
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationFirestore.global(notifications => setNotifications(notifications))
   */
  global(callback: (value: dNotification[]) => void) {
    if (this._global) callback(this._global);
    this._globalCallbacks.push(callback);
  }
  /**
   * ### Live callback for notifications sent to the team you are part of
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationFirestore.team(notifications => setNotifications(notifications))
   */
  team(callback: (value: dNotification[]) => void) {
    if (this._team) callback(this._team);
    this._teamCallbacks.push(callback);
  }
  /**
   * ### Live callback for notifications that are scheduled to go out in the future
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationFirestore.scheduled(notifications => setNotifications(notifications))
   */
  scheduled(callback: (value: dNotification[]) => void) {
    if (this._scheduled) callback(this._scheduled);
    this._scheduledCallbacks.push(callback);
  }

  /**
   * ### Returns all notifications that the current user has not seen yet
   * - A notification is seen when the user taps the notification area and then goes back out
   * - Make sure notifications have been fetched before using this (most likely they have been)
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * const unreadNotifications = notificationFirestore.getUnreadNotifications()
   */
  getUnreadNotifications(): dNotification[] {
    const account = Backend.firestoreHandler._account;
    let notis = this._global?.concat(this._team ? this._team : []);
    let unreadNotifications: dNotification[] = [];
    for (let n in notis) {
      const noti: dNotification = notis[n];
      if (
        account?.notifications?.lastRead &&
        dateAsNumber(noti.timestamp) >
          dateAsNumber(account.notifications.lastRead)
      )
        unreadNotifications.push(noti);
    }
    return unreadNotifications;
  }

  /**
   * ### Unsubscribe from team notifications in firestore
   *
   * @author jm_francis
   * @version 1.2.6
   *
   * @example
   * notificationFirestore.leaveTeam()
   */
  async leaveTeam() {
    this.teamNotificationsRef && this.teamNotificationsRef();
    this._team = null;
    _callAll(this._teamCallbacks, this._team);
  }

  /**
   * ### Adds notification into firestore
   * - Then causes the notification details to show up in the notification area of the app
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationFirestore.addNotification({
   *  channel: "global",
   *  title: "Important update!",
   *  message: "Big team call in 10 minutes!"
   * })
   */
  async addNotification(_notification: Partial<dNotification>) {
    const notification = {
      channel: "global",
      title: "",
      message: "",
      status: NotificationStatus.SENT,
      ..._notification,
    };
    return await firebase
      .firestore()
      .collection("notifications")
      .doc("channels")
      .collection(notification.channel)
      .add({
        comments: [],
        ...notification,
        timestamp: new Date(),
      });
  }
  /**
   * ### Schedule a notification to go out in the future
   * - Cloud functions will pick up on this and send it when scheduled
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationFirestore.scheduleGlobalNotification("SUPER SATURDAY", "LETS KILL IT THIS WEEKEND!", saturdayDate)
   */
  async scheduleGlobalNotification(
    title: string,
    message: string,
    timestamp: Date
  ) {
    const notification: dNotification = {
      channel: "global",
      comments: [],
      status: NotificationStatus.SCHEDULED,
      title,
      message,
      timestamp,
    };
    return await firebase
      .firestore()
      .collection("notifications")
      .doc("channels")
      .collection("global")
      .add(notification);
  }
  /**
   * ### Deletes a previously sent notification from the notification list in the app
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationFirestore.deleteGlobalNotification(someNotification)
   */
  async deleteNotification(notification: dNotification) {
    return await firebase
      .firestore()
      .collection("notifications")
      .doc("channels")
      .collection(notification.channel ? notification.channel : "global")
      .doc(notification.id)
      .delete();
  }

  /**
   * ### Add a comment to the comments area tied to the specific notification
   *
   * @jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationFirestore.writeComment(notification, "This is my comment!")
   */
  async writeComment(notification: dNotification, message: string) {
    const account = Backend.firestoreHandler._account;
    const comment: dComment = {
      uid: Backend.firestoreHandler.uid,
      timestamp: new Date(),
      message,
      name: account.name,
      profileImage: account.profileImage,
    };
    console.log(
      `path: notifications/channels/${notification.channel}/${notification.id}/comments`
    );
    const ref = firebase
      .firestore()
      .collection("notifications")
      .doc("channels")
      .collection(notification.channel ? notification.channel : "global");
    return await ref.doc(notification.id).update({
      comments: notification.comments.concat([comment]),
    });
  }

  /**
   * ### Removes the given comment from the comments section
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * removeComment(notification, comment)
   */
  async removeComment(notification: dNotification, comment: dComment) {
    const globalRef = firebase
      .firestore()
      .collection("notifications")
      .doc("channels")
      .collection(notification.channel ? notification.channel : "global");
    for (let c in notification.comments) {
      const _comment = notification.comments[c];
      if (
        _comment.message === comment.message &&
        _comment.uid === comment.uid
      ) {
        delete notification.comments[c];
        break;
      }
    }
    return await globalRef.doc(notification.id).set(notification);
  }

  didInit = false;
  teamNotificationsRef = null;

  /**
   * ### Load the user's team notifications in firestore
   * - when the user leaves their team, this onSnapshot will close
   *
   * @author jm_francis
   * @version 1.2.6
   *
   * @example
   * notificationFirestore.subscribeToTeamNotifications("The Go Getters")
   */
  async subscribeToTeamNotifications(teamName: string) {
    this.teamNotificationsRef && this.teamNotificationsRef();
    this.teamNotificationsRef = firebase
      .firestore()
      .collection("notifications")
      .doc("channels")
      .collection(topicIdFromName(teamName))
      .onSnapshot((query) => {
        const notifications = [];
        query.forEach((doc) => {
          notifications.push({ ...doc.data(), id: doc.id });
        });
        this._team = sortAndSetup(notifications);
        _callAll(this._teamCallbacks, this._team);
      });
  }

  /**
   * ### Load the notifications in firestore initially
   * - Runs once after fetching account from firestoreHandler
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * notificationFirestore.init()
   */
  async init() {
    Backend.firestoreHandler.account((account) => {
      if (!account || this.didInit) return;
      this.didInit = true;
      this._init();
      if (account.team) {
        this.subscribeToTeamNotifications(account.team.teamName);
      }
    });
  }
  async _init() {
    firebase
      .firestore()
      .collection("notifications")
      .doc("channels")
      .collection("global")
      // .where("status", "==", NotificationStatus.SENT)
      .onSnapshot((query) => {
        const notifications: dNotification[] = [];
        query.forEach((doc) => {
          const noti: dNotification = <dNotification>doc.data();
          noti.id = doc.id;
          if (noti.timestamp) notifications.push(noti);
        });
        // const allNotifications = notifications
        //   .sort(
        //     (n1: dNotification, n2: dNotification) =>
        //       dateAsNumber(n2.timestamp) - dateAsNumber(n1.timestamp)
        //   )
        //   .map((noti) => {
        //     noti.comments = noti.comments.sort(
        //       (c1: dComment, c2: dComment) =>
        //         dateAsNumber(c1.timestamp) - dateAsNumber(c2.timestamp)
        //     );
        //     return noti;
        //   });
        const allNotifications = sortAndSetup(notifications);
        this._global = allNotifications.filter(
          (notification) =>
            notification.status === NotificationStatus.SENT ||
            !notification.status
        );
        this._scheduled = allNotifications
          .filter(
            (notification) =>
              notification.status === NotificationStatus.SCHEDULED
          )
          .sort(
            (n1: dNotification, n2: dNotification) =>
              dateAsNumber(n1.timestamp) - dateAsNumber(n2.timestamp)
          );
        _callAll(this._globalCallbacks, this._global);
        _callAll(this._scheduledCallbacks, this._scheduled);
      });
  }
}

export function sortNotifications(
  notifications: dNotification[]
): dNotification[] {
  return notifications.sort(
    (n1: dNotification, n2: dNotification) =>
      dateAsNumber(n2.timestamp) - dateAsNumber(n1.timestamp)
  );
}
function sortAndSetup(notifications: dNotification[]): dNotification[] {
  return sortNotifications(notifications).map((noti) => {
    noti.comments = noti.comments?.sort(
      (c1: dComment, c2: dComment) =>
        dateAsNumber(c1.timestamp) - dateAsNumber(c2.timestamp)
    );
    return noti;
  });
}

export const notificationFirestore = new NotificationFirestore();
