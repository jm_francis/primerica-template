import Backend from "backend/";
import { openZoomWithIdIfPossible } from "backend/apis/ZoomAppHandler";
import { Alert, Linking } from "react-native";
import * as AddCalendarEvent from "react-native-add-calendar-event";

/**
 * extractUrl(fromString)
 */
export const extractUrlFrom = (string = "") => {
  let url = string;
  if (!url) return null;
  if (!url.includes("http")) return null;
  const href = '<a href="';
  if (url.includes(href)) {
    url = url.substring(url.indexOf(href) + href.length, url.indexOf("</a>"));
    url = url.substring(0, url.indexOf('"'));
    url = url.replace(" ", "?");
  } else {
    url = url.substring(url.indexOf("http"), url.length);
    url = url.substring(0, url.indexOf(" "));
  }
  return url;
};

/**
 * handleLinkPress(data={item,url})
 */
const handleLinkPress = (item) => {
  let url = extractUrlFrom(item.description);
  if (item.location && !url) {
    url = extractUrlFrom(item.location?.description);
  }

  if (url && url.includes("zoom.us")) {
    const meetingId = url
      .replace("https://zoom.us/j/", "")
      .replace("https://success.zoom.us/j/", "");
    openZoomWithIdIfPossible(meetingId);
  } else if (url && item.description && item.description.includes("http")) {
    const extractedUrl = extractUrlFrom(item.description);
    Linking.openURL(extractedUrl);
  } else {
    Alert.alert("No link found.");
  }
};

/**
 * handleAddToCalendarPress(data={item})
 */
const handleAddToCalendarPress = (item) => {
  let url;
  if (item.description && item.description.includes("http"))
    url = extractUrlFrom(item.description);

  AddCalendarEvent.presentEventCreatingDialog({
    title: item.title,
    url: url ? url : "",
    notes: item.description ? item.description : "",
    date: item.date.toISOString(),
    endDate: item.endDate.toISOString(),
    location: item.location ? item.location.description : "",
  })
    .then(() => {})
    .catch((error) => {
      Alert.alert(
        "An error occurred and we were unable to add this event to your calendar."
      );
      console.log("handleAddToCalendarPress: " + error.message);
    });
};

export {
  extractUrlFrom as extractUrl,
  handleLinkPress,
  handleAddToCalendarPress,
};
