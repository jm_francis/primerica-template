import Backend from "backend/";
import K from "constant-deprecated";
import RNFetchBlob from "rn-fetch-blob";
const Config = K.Config;

function parseISOString(s) {
  return new Date(Date.parse(s));
  // var b = s.split(/\D+/);
  // return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
}

const _callAll = (funcs, value) => {
  for (let f in funcs) funcs[f](value);
};

export interface dCalendarEvent {
  title?: string;
  description?: string;
  date: Date;
  endDate: Date;
  location: string;
  time?: string;
}

function timeToAMPMString(date) {
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let ampm = hours >= 12 ? "PM" : "AM";
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  const strTime = hours + ":" + minutes + " " + ampm;
  return strTime;
}

export const dateFormat = (_date) => {
  if (!_date) return null;
  let date = _date;
  if (_date.toDate) date = _date.toDate();
  const month = date.getMonth() + 1;
  const monthStr = month < 10 ? "0" + month : `${month}`;
  const dateStr =
    date.getDate() < 10 ? "0" + date.getDate() : "" + date.getDate();
  return `${date.getFullYear()}-${monthStr}-${dateStr}`;
};

/**
 * Returns date as number whether it is a JavaScript Date or Firestore Timestamp
 */
export function dateAsNumber(_date) {
  let date = _date;
  if (_date.toDate) date = _date.toDate();
  return date.getTime();
}

class CalendarHandler {
  // ANCHOR: Variables
  _calendar: dCalendarEvent[] = null;

  // ANCHOR: Callbacks
  _calendarCallbacks: ((calendar: dCalendarEvent[]) => void)[] = [];

  // ANCHOR: Subscribers
  calendar(callback: (calendar: dCalendarEvent[]) => void) {
    if (this._calendar) callback(this._calendar);
    this._calendarCallbacks.push(callback);
  }

  _didInit = false;
  _didLoadConfig = false;

  /**
   * Load everything needed for the calendar
   */
  init() {
    if (this._didInit) return;
    this._didInit = true;
    this._loadCalendarData();
  }

  _loadCalendarData() {
    Backend.firestoreHandler.config((config) => {
      if (this._didLoadConfig) return;
      this._didLoadConfig = true;
      this.__loadCalendarData();
    });
  }
  __loadCalendarData() {
    const day = 86400000;
    const dateRangeStart = new Date();
    dateRangeStart.setTime(dateRangeStart.getTime() - day * 10);
    const dateRangeEnd = new Date();
    dateRangeEnd.setTime(dateRangeEnd.getTime() + day * 30 * 4);

    const startStr = dateFormat(dateRangeStart);
    const endStr = dateFormat(dateRangeEnd);

    console.log("initializing calendar...");
    RNFetchBlob.fetch(
      "GET",
      `https://api.cronofy.com/v1/events?tzid=Etc/UTC&calendar_ids[]=${Backend.firestoreHandler._config.keys.calendarId}&from=${startStr}&to=${endStr}`,
      {
        Authorization: `Bearer ${Backend.firestoreHandler._config.keys.calendarAccessToken}`,
        "Content-Type": "application/json",
      }
    )
      .then((response) => response.json())
      .then((json) => {
        let events = [];
        for (let e in json.events) {
          const event = json.events[e];
          const date = parseISOString(event.start);
          const endDate = parseISOString(event.end);

          const startTime = date
            ? timeToAMPMString(date).replace(":NaN", ":00")
            : "";
          const endTime = endDate
            ? timeToAMPMString(endDate).replace(":NaN", ":00")
            : null;
          const time = endTime ? `${startTime} - ${endTime}` : `${startTime}`;

          events.push({
            title: event.summary,
            description: event.description,
            date,
            endDate,
            location: event.location,
            time,
          });
        }

        console.log(`${events.length} loaded from calendar`);

        this._calendar = events;
        _callAll(this._calendarCallbacks, this._calendar);
      })
      .catch((err) => {
        console.log("Calendar fetching error:" + err);
        // do something?
        return;
      });
  }
}

export const calendarHandler = new CalendarHandler();
