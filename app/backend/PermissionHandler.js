import { Platform, Alert } from "react-native";
import {
  request,
  requestMultiple,
  PERMISSIONS,
  RESULTS,
} from "react-native-permissions";
import PushNotification from "react-native-push-notification";
import Backend from "backend/";

const DENIED = () => {
  Alert.alert(
    "Not enabling permissions may cause unexpected issues in the app.",
    "You can go into your phone settings to re enable these at any time."
  );
};

class PermissionHandler {
  async requestAll() {
    try {
      // contacts
      let result;
      if (Platform.OS === "ios") {
        let result = await request(PERMISSIONS.IOS.CONTACTS);
      } else {
        await request(PERMISSIONS.ANDROID.WRITE_CONTACTS);
        result = await request(PERMISSIONS.ANDROID.READ_CONTACTS);
      }
      // if (result !== RESULTS.GRANTED) DENIED();
      // push notifications
      if(Backend.firestoreHandler._config.variables.enablePushNotifications === true) {
        PushNotification.requestPermissions();
      }
    } catch (error) {
      console.log(error);
    }
  }
}

export const permissionHandler = new PermissionHandler();
