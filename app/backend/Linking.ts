import { Linking as RNLinking } from "react-native";

export default class Linking {
  static navigation;
  /**
   * Linking class needs access to the navigation prop from react-navigation to use the Browser screen for displaying in app browser
   */
  static init(navigation) {
    Linking.navigation = navigation;
  }

  /**
   * Will open specified url in Browser screen by default.
   * If ext: is provided before the string, the link will open in a browser outside of the app.
   */
  static openURL(url: string) {
    if (url.substring(0, 4).includes("ext:")) {
      const realURL = url.substring(4, url.length);
      RNLinking.openURL(realURL);
    } else {
      Linking.navigation.navigate("Browser", {
        source: url
      });
    }
  }
}
