import Backend from "backend/";
import { AsyncStorage } from "react-native";

const ACCESS = Object.freeze({
  DENIED: false,
  REGULAR: true,
  ADMIN: "admin",
});

class LocalHandler {
  ACCESS = ACCESS;

  // ANCHOR: App Password
  setAppPassword(password) {
    return AsyncStorage.setItem("@Local:appPassword", password);
  }
  getAppPassword() {
    return AsyncStorage.getItem("@Local:appPassword");
  }
  async testAppPassword() {
    //* NOTE ERROR: null is not an object (evaluating '_index.default.firestoreHandler._config.variables')
    const variables = Backend.firestoreHandler._config.variables;
    const pwd = variables.appPassword;
    const adminPwd = variables.adminPassword;
    const localPassword = await this.getAppPassword();
    if (!localPassword) return false;
    if (localPassword.toLowerCase() === pwd.toLowerCase()) {
      return ACCESS.REGULAR;
    }
    if (localPassword.toLowerCase() === adminPwd.toLowerCase()) {
      return ACCESS.ADMIN;
    }
    return ACCESS.DENIED;
  }

  // ANCHOR: RVP passwords
  savePasswordForPage(pageName, password) {
    return AsyncStorage.setItem(`@PagePasswords:${pageName}`, password);
  }
  getSavedPasswordForPage(pageName) {
    return AsyncStorage.getItem(`@PagePasswords:${pageName}`);
  }

  // ANCHOR: Calendar - has opened before
  setCalendarDidOpen(didOpen = true) {
    const value = didOpen ? "true" : "false";
    AsyncStorage.setItem("@Local:calendarDidOpen", value);
  }
  async getCalendarDidOpen() {
    const result = await AsyncStorage.getItem("@Local:calendarDidOpen");
    if (result === "true") return true;
    else if (!result || result === "false") return false;
  }

  // ANCHOR: Custom page - has opened before
  setCustomPageDidOpen(didOpen = true) {
    const value = didOpen ? "true" : "false";
    AsyncStorage.setItem("@Local:customPageDidOpen", value);
  }
  async getCustomPageDidOpen() {
    const result = await AsyncStorage.getItem("@Local:customPageDidOpen");
    if (result === "true") return true;
    else if (!result || result === "false") return false;
  }

  // ANCHOR: Permissions - has opened before
  setPermissionsDidOpen(didOpen = true) {
    const value = didOpen ? "true" : "false";
    AsyncStorage.setItem("@Local:permissionsDidOpen", value);
  }
  async getPermissionsDidOpen() {
    const result = await AsyncStorage.getItem("@Local:permissionsDidOpen");
    if (result === "true") return true;
    else if (!result || result === "false") return false;
  }

  // ANCHOR: Build My List (post exercise and see list page) - has opened before
  setBuildMyListDidOpen(didOpen = true) {
    const value = didOpen ? "true" : "false";
    AsyncStorage.setItem("@Local:buildMyListDidOpen", value);
  }
  async getBuildMyListDidOpen() {
    const result = await AsyncStorage.getItem("@Local:buildMyListDidOpen");
    if (result === "true") return true;
    else if (!result || result === "false") return false;
  }

  // ANCHOR: Share via a List
  setDidShowShareCopyPrompt(didOpen = true) {
    const value = didOpen ? "true" : "false";
    AsyncStorage.setItem("@Local:didShowShareCopyPrompt", value);
  }
  async getDidShowShareCopyPrompt() {
    const result = await AsyncStorage.getItem("@Local:didShowShareCopyPrompt");
    if (result === "true") return true;
    else if (!result || result === "false") return false;
  }
}

export const localHandler = new LocalHandler();
