import { UserSchema,EmailSchema } from "backend/schemas";
import R from "ramda";
import firebase from "react-native-firebase";

const _callAll = (funcs: ((value: any) => void)[], value: any) => {
  for (let f in funcs) funcs[f](value);
};

class AdminHandler {
  // ANCHOR: Variables
  _users: UserSchema[] = null;

  // ANCHOR: Callbacks
  _usersCallbacks: ((user: UserSchema[]) => void)[] = [];

  // ANCHOR: Subscribers
  users(callback: (users: UserSchema[]) => void) {
    this.initIfNeeded();
    if (this._users) callback(this._users);
    this._usersCallbacks.push(callback);
  }
  /**
   * ### Returns the user with the specified uid
   * 
   * @author jm_francis
   * @version 1.1.26
   * 
   * @example
   * adminHandler.getUserByUid(uid)
   */
  getUserByUid(uid: string): UserSchema {
    for(let u in this._users) {
      if(this._users[u].uid === uid)
        return this._users[u]
    }
    return null;
  }

  /**
   * ### Update the given user's data in firestore
   * 
   * @author jm_francis
   * @version 1.1.26
   * 
   * @example
   * adminHandler.updateUserData(someUser, { admin: true })
   */
  async updateUserData(_user: UserSchema, data: {}) {
    const user = { ..._user };
    delete user.uid;
    const updatedUser = {
      ...user,
      ...data,
    };
    await firebase
      .firestore()
      .collection("users")
      .doc(_user.uid)
      .set(updatedUser);
    return { ...updatedUser, uid: _user.uid };
  }

  /**
   * ### 
   *  - Detailed explanation (if any)
   *  ----
   *  @example 
   *  Copy and paste function is the best...
   *  ----
   *  @version 21.03.16
   *  -  *Brief changelog*
   *  @author  Your Name
   *  
   **/
   async extractSentMails(userId: String) {
   
   
    const response = firebase.firestore().collection("emails").where('userId', '==', userId);
    const mails = await response.get()
    // console.log(mails);
    // .onSnapshot((result) => {
    //   const emails: EmailSchema[] = [];
    //   // result.forEach((u) => {
    //   //   const user = <EmailSchema>{ ...u.data() };
    //   //   user.uid = u.id;
    //   //   users.push(user);
    //   // });
    //   return emails;
    // })
    
    return mails;

    
  }

  /**
   * ### Ban a user from the app
   * (will block them out when try to open the app or login)
   * 
   * @author jm_francis
   * @version 1.1.26
   * 
   * @example
   * adminHandler.banUser(someUser, true)
   */
  banUser(_user: UserSchema, banned = true) {
    return this.updateUserData(_user, {
      banned,
    });
  }
  /**
   * ### Unban a banned user from the app so they can get in again
   * 
   * @author jm_francis
   * @version 1.1.26
   * 
   * @example
   * adminHandler.unbanUser(someUser)
   */
  unbanUser(user: UserSchema) {
    return this.banUser(user, false);
  }

  /**
   * ### Enables given user to access all levels without having to through one level at a time
   * (in New Rep area)
   * 
   * @author jm_francis
   * @version 1.1.26
   * 
   * @example
   * adminHandler.unlockUserLevels(someUser)
   */
  unlockUserLevels(user: UserSchema) {
    return this.updateUserData(user, { allLevelsCompleted: true });
  }
  /**
   * ### If a user's levels were unlocked by an admin, this will re lock them
   * (in New Rep area)
   * 
   * @author jm_francis
   * @version 1.1.26
   * 
   * @example
   * adminHandler.lockUsersLevels(someUser)
   */
  lockUserLevels(user: UserSchema) {
    return this.updateUserData(user, { allLevelsCompleted: false });
  }

  didInit: boolean = false;

  /**
   * ### Will open an onSnapshot to get all the users in firestore
   * This should only be run once, but will cancel if accidently run twice
   * 
   * @author jm_francis
   * @version 1.1.26
   * 
   * @example
   * adminHandler.initIfNeeded()
   */
  initIfNeeded() {
    if (this.didInit) return;
    this.didInit = true;

    firebase
      .firestore()
      .collection("users")
      .onSnapshot((result) => {
        const users: UserSchema[] = [];
        result.forEach((u) => {
          const user = <UserSchema>{ ...u.data() };
          user.uid = u.id;
          users.push(user);
        });
        this._users = users
          .sort((a, b) => {
            if (
              R.isNil(a) &&
              R.isNil(a.name) &&
              R.isNil(b) &&
              R.isNil(b.name)
            ) {
              const aName = a.name.toLowerCase();
              const bName = b.name.toLowerCase();
              if (aName > bName) return 1;
              else if (aName < bName) return -1;
              return 0;
            }
          })
          .sort((a, b) => a.createdAt?.toDate() - b.createdAt?.toDate());

        _callAll(this._usersCallbacks, this._users);
      });
  }
}

export const adminHandler = new AdminHandler();
