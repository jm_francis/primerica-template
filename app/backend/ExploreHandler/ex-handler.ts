import { FPATH } from "backend/FirestoreHandler/firestore-handler.props";
import { useEffect, useState } from "react";
import firebase from "react-native-firebase";

function useExploreHandler() {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    let snapshotting = firebase
      .firestore()
      .collection(FPATH.POSSTS)
      .onSnapshot((r) => {
        if (!r.empty) {
          const data = r.docs;
          setData(data)
          setLoading(false);
        } else {
          setLoading(false);
        }
      });

    return snapshotting;
  }, []);

  return { data, create, update, delete,  error, loading };
}

enum EXP_TYPE  {
    COLLECTION = "COLLECTION",
    DOC = "DOC"
}

const update = (type, payload) => {
try {
    if (pay)
} catch (error) {
    
}

}