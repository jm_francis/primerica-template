import Backend from "backend/Backend";
import { FPATH } from "backend/FirestoreHandler/firestore-handler.props";
import {
  CMT_STATUS,
  CommentSchema,
  PosstSchema,
  POSST_STATUS,
} from "backend/schemas/possts";
import { Toasty } from "components/";
import R from "ramda";
import firebase from "react-native-firebase";

// export const frefExplore = firebase.firestore().collection(FPATH.POSSTS);
const _callAll = (funcs: Function[], value: any) => {
  for (let f in funcs) funcs[f](value);
};

class ExploreHandler {
  // ANCHOR: Variables
  _possts: PosstSchema[] = null;
  _comments: CommentSchema[] = null;

  // ANCHOR: Callbacks
  _posstsCallbacks: ((possts: PosstSchema[]) => void)[] = [];
  _commentsCallbacks: ((comments: CommentSchema[]) => void)[] = [];

  //ANCHOR : Reference to the collection
  _posstsRef = firebase.firestore().collection(FPATH.POSSTS);

  /**
   * ### Get live updates for the _possts variable
   *
   * @author Saurabh M.
   * @version 0.0.1
   *
   * @example
   * exploreHandler.possts(value => setState({ value }))
   */
  possts(callback: (possts: PosstSchema[]) => void) {
    if (this._possts) callback(this._possts);
    this._posstsCallbacks.push(callback);
  }

  commentsRef = null;
  openComments(
    posst: PosstSchema,
    callback: (comments: CommentSchema[]) => void
  ) {
    if (this._comments) callback(this._comments);
    this._commentsCallbacks.push(callback);

    if (!this.commentsRef) {
      this._openCommentsSnapshot(posst);
    }
  }
  _openCommentsSnapshot(posst: PosstSchema) {
    this.commentsRef = this._posstsRef
      .doc(posst._pid)
      .collection("comments")
      .onSnapshot((query) => {
        const comments: CommentSchema[] = [];
        query.forEach((doc) => {
          const comment = <CommentSchema>doc.data();
          comments.push({
            _id: doc.id,
            ...comment,
            createdAt: comment?.createdAt?.toDate(),
          });
        });
        this._comments = comments;
        _callAll(this._commentsCallbacks, this._comments);
        // update posst commentCount if needed
        if (posst.commentCount !== this._comments?.length) {
          this._posstsRef
            .doc(posst._pid)
            .update({ commentCount: this._comments.length });
        }
      });
  }
  closeComments() {
    this.commentsRef && this.commentsRef();
    this.commentsRef = null;
    this._comments = [];
    this._commentsCallbacks = [];
  }

  /**
   * ### Create a new post!
   * - Any missing variables will get automatically placed in so you don't have to worry too much! :D
   *
   * @author jm_francis
   * @version 1.2.12
   *
   * @example
   * exploreHandler.createPosst({
   *    body: "",
   *    medias: ["https://cloudinary.com/uploaded_video.mp4"]
   * })
   */
  async createPosst(posst: Partial<PosstSchema>) {
    const account = Backend.firestoreHandler._account;
    const finalPosst: PosstSchema = {
      _pid: "",
      updatedAt: new Date(),
      createdAt: new Date(),
      author: {
        _id: account.uid,
        name: account.name,
        avatar:
          account.profileImage &&
          account.profileImage.length > 0 &&
          account.profileImage,
      },
      likes: [],
      medias: [],
      scheduledAt: null,
      status: posst.scheduledAt ? POSST_STATUS.SCHEDULED : POSST_STATUS.POSTED,
      body: "",
      pinned: false,
      commentCount: 0,
      ...posst,
    };
    await this._posstsRef.add(finalPosst);

    if (finalPosst.scheduledAt) return;

    Backend.notificationHandler.sendNotificationToEveryone(
      `New post by ${account.name}`,
      `${finalPosst.body}${
        finalPosst.body.replace(/ /g, "").length == 0
          ? "Tap to open!"
          : "\n\nTap to open!"
      }`,
      {
        // will not add the notification to the bell area because there is no need
        disableAddToFirestore: true,
      }
    );
  }

  /**
   * ### Pin a posst!
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * exploreHandler.pinPosst(somePosst)
   */
  pinPosst(posst: PosstSchema) {
    return this._posstsRef.doc(posst._pid).update({
      pinned: true,
    });
  }
  /**
   * ### Unpin a posst!
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * exploreHandler.unpinPosst(somePosst)
   */
  unpinPosst(posst: PosstSchema) {
    return this._posstsRef.doc(posst._pid).update({
      pinned: false,
    });
  }

  /**
   * ### Delete a post!
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * exploreHandler.deletePosst(somePosst)
   */
  deletePosst(posst: PosstSchema) {
    return this._posstsRef.doc(posst._pid).update({
      status: POSST_STATUS.DELETED,
    });
  }

  /**
   * ### Like a post!
   * - adds the uid of the current user into the likes array for the posst
   * - will remove like if they have liked before
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * exploreHandler.likePosst(somePosst)
   */
  async likePosst(posst: PosstSchema) {
    const account = Backend.firestoreHandler._account;
    if (posst.likes.includes(account.uid)) {
      posst.likes = posst.likes.filter((like) => like !== account.uid);
    } else {
      posst.likes = posst.likes.concat([account.uid]);
    }
    return this._posstsRef.doc(posst._pid).update(posst);
  }

  /**
   * ### Add a comment to the specified posst
   * - posts comment as currently logged in user
   *
   * @author jm_francis
   * @version 1.1.30
   *
   * @example
   * exploreHandler.addComment(posst, "Hey everyone!")
   */
  async addComment(posst: PosstSchema, text: string) {
    const account = Backend.firestoreHandler._account;
    const comment: CommentSchema = {
      _pid: posst._pid,
      createdAt: new Date(),
      user: {
        _id: account.uid,
        name: account.name,
        avatar:
          account.profileImage &&
          account.profileImage.length > 0 &&
          account.profileImage,
      },
      text,
      status: CMT_STATUS.POSTED,
    };
    return this._posstsRef
      .doc(posst._pid)
      .collection("comments")
      .add(comment)
      .then(() => {
        if (!this.commentsRef) this._openCommentsSnapshot(posst);
      });
  }
  /**
   * ### Delete specified comment
   *
   * @author K, jm_francis
   * @version 1.2.6
   * -  *Add Error Handler*
   *
   * @example
   * exploreHandler.deleteComment(someComment)
   */
  async deleteComment(comment: { _id: string | number; _pid: string }) {
    try {
      // console.log("_id: ", comment?._id);
      if (
        !!!comment?._id ||
        !!!comment?._pid ||
        R.isEmpty(comment?._id) ||
        R.isEmpty(comment?._pid)
      )
        throw Error("Not Enough Props");
      return this._posstsRef
        .doc(comment._pid)
        .collection("comments")
        .doc(<string>comment._id)
        .delete()
        .then((r) =>
          Toasty({
            text1: "Comment deleted",
          })
        )
        .catch((error) => {
          // console.warn("Error deleting post: ", error);
          throw Error(error.message);
        });
    } catch (error) {
      console.warn("errorrrrr: ", error);
      let defaultToast = Toasty({
        text1: "Error deleting comment",
        text2: "Please try again later",
        type: "error",
      });
      if (error.message.includes("Not Enough Props")) {
        console.warn("Please provide comment's _pid and _id");
        return defaultToast;
      }
      return defaultToast;
    }
  }

  /**
   * ### Fetches data from firestore and starts calling UI callbacks
   * - This function should only be run once when the app starts
   * - This function is R of the CRUD operation (read)
   * @author Saurabh M.
   * @version 0.0.1
   *
   * @example
   * exploreHandler.init()
   */
  async init() {
    this._posstsRef.where("status", "==", POSST_STATUS.POSTED).onSnapshot(
      (collection) => {
        const results: PosstSchema[] = [];

        collection.forEach((doc) => {
          results.push({
            ...(<PosstSchema>doc.data()),
            _pid: doc.id,
          });
        });

        this._possts = results.sort(
          (p1: PosstSchema, p2: PosstSchema) =>
            p2.createdAt?.toDate().getTime() - p1.createdAt?.toDate().getTime()
        );

        //const result = doc.data();
        // TODO: NOTE!! "result" is an array in here
        _callAll(this._posstsCallbacks, this._possts);
      },
      (error) => {
        console.log(`Encountered error: ${error}`);
      }
    );
  }
}
export const exploreHandler = new ExploreHandler();
