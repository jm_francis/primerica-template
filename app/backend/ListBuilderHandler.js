import Backend from "backend/";
import K from "constant-deprecated";
import firebase, { firestore } from "react-native-firebase";
import { EmailSchema } from "backend/schemas";
const Config = K.Config;

const _callAll = (funcs, value) => {
  for (f in funcs) funcs[f](value);
};

// NOTE Documentation
// this account's lists are contained in account.listBuilder.lists and are accessed from firestoreHandler.account()

class ListBuilderHandler {
  // ANCHOR: Variables
  _config = null;
  _scores = null;

  // ANCHOR: Callbacks
  _scoresCallbacks = [];

  // ANCHOR: Subscribers
  scores(callback) {
    if (this._scores) callback(this._scores);
    this._scoresCallbacks.push(callback);
  }

  // ANCHOR: Update for non account things
  scoreHandler = new ScoreHandler();
  outsiderHandler = new OutsiderHandler();

  setMyScore = this.scoreHandler.setMyScore.bind(this.scoreHandler);
  outsiderAccount = this.outsiderHandler.outsiderAccount.bind(
    this.outsiderHandler
  );
  connectToOutsiderAccount = this.outsiderHandler.connectToOutsiderAccount.bind(
    this.outsiderHandler
  );
  closeOutsiderAccountConnection = this.outsiderHandler.closeOutsiderAccountConnection.bind(
    this.outsiderHandler
  );

  // ANCHOR: Create & Delete
  /**
   * createList(listTitle, data={})
   */
  createList(listTitle, data = {}) {
    return this.updateList(listTitle, {
      title: listTitle,
      id: listTitle.toLowerCase().replace(/ /g, "."),
      contacts: [],
      ...data,
    });
  }
  /**
   * deleteList(listData={})
   */
  deleteList(listData) {
    const lists = this.extractLists();
    const newLists = [];
    for (l in lists) {
      const list = lists[l];
      if (list.id === listData.id) continue;
      newLists.push(list);
    }

    return Backend.firestoreHandler.updateAccount("listBuilder", {
      lists: newLists,
    });
  }
  // ANCHOR: Update
  updateList(listTitle, mergeData) {
    const lists = this.extractLists();

    const newLists = [];
    let didMergeToExistingList = false;
    for (l in lists) {
      let list = lists[l];
      if (list.title === listTitle) {
        didMergeToExistingList = true;
        list = { ...list, ...mergeData };
      }
      newLists.push(list);
    }

    if (!didMergeToExistingList) {
      // create a new list for it (ex: MemoryJogger)
      newLists.push(mergeData);
    }

    return Backend.firestoreHandler.updateAccount("listBuilder", {
      lists: newLists,
    });
  }
  updateContactInList(contact, listTitle) {
    const list = this.extractList(listTitle);
    const fn = this.fullNameOf;
    if (contact.recordID) {
      list.contacts = this.extractContacts(listTitle).map((c) =>
        c.recordID === contact.recordID ? contact : c
      );
    } else {
      list.contacts = this.extractContacts(listTitle).map((c) =>
        fn(c) === fn(contact) ? contact : c
      );
    }

    return this.updateList(listTitle, list);
  }

  addContactToList(contact, listTitle) {
    const list = this.extractList(listTitle);
    list.contacts = this.extractContacts(listTitle).concat([contact]);

    return this.updateList(listTitle, list);
  }
  removeContactFromList(contact, listTitle) {
    const list = this.extractList(listTitle);
    const fn = this.fullNameOf;
    if (contact.recordID)
      list.contacts = this.extractContacts(listTitle).filter(
        (a) => a.recordID !== contact.recordID
      );
    else
      list.contacts = this.extractContacts(listTitle).filter(
        (a) => fn(a) !== fn(contact)
      );

    return this.updateList(listTitle, list);
  }
  moveContactFromListToList(contact, _fromList, _toList) {
    let lists = this.extractLists();

    const fromList = { ..._fromList };
    const toList = { ..._toList };

    // removes from list
    fromList.contacts = fromList.contacts.filter(
      (c) => this.getId(c) !== this.getId(contact)
    );
    // adds to new list
    toList.contacts = toList.contacts
      ? toList.contacts.concat([contact])
      : [contact];

    const newLists = [];
    for (l in lists) {
      let list = lists[l];
      if (list.title === fromList.title) list = fromList;
      else if (list.title === toList.title) list = toList;
      newLists.push(list);
    }
    return Backend.firestoreHandler.updateAccount("listBuilder", {
      lists: newLists,
    });
  }
  makeContactsOne(contact1, contact2, listTitle) {
    // NOTE assumes 1 is a contact not on the phone already (no recordID)
    const dbContact = contact1.recordID ? contact2 : contact1;
    const phoneContact = contact1.recordID ? contact1 : contact2;
    const list = this.extractList(listTitle);
    const contacts = list.contacts
      ? list.contacts.filter((i) => {
          if (
            !i.recordID &&
            dbContact.givenName == i.givenName &&
            dbContact.familyName == i.familyName
          )
            return false;
          return true;
        })
      : [];
    const newContact = { ...dbContact, ...phoneContact };
    contacts.push(newContact);
    list.contacts = contacts;

    return this.updateList(listTitle, list);
  }



  // ANCHOR: Functions
  extractLists() {
    // returns all lists in account
    const account = Backend.firestoreHandler._account;
    const acc = account.listBuilder ? account.listBuilder : {};
    return acc.lists ? acc.lists : [];
  }
  extractList(listTitle) {
    // returns list with title from account
    const lists = this.extractLists();
    for (l in lists) if (lists[l].title == listTitle) return lists[l];
    return null;
  }
  extractContacts(listTitle) {
    // returns all contacts from list with title
    const list = this.extractList(listTitle);
    return list && list.contacts ? list.contacts : [];
  }
  fullNameOf(contact) {
    // returns first and last name of contact
    return `${contact.givenName ? contact.givenName : "__"} ${
      contact.familyName ? contact.familyName : ""
    }`;
  }
  getId(contact) {
    // returns a unique key for given contact
    if (contact.recordID) return contact.recordID;
    return `${contact.givenName ? contact.givenName : ""}${
      contact.familyName ? contact.familyName : ""
    }`;
  }

  // ANCHOR: init
  init() {
    Backend.firestoreHandler.account((account) => {
      if (!account) return;
      if (this.didInit) return;
      this.didInit = true;
      this._init(account);
    });
  }
  async _init(account) {
    const { listBuilder = {} } = account;
    const config = Backend.firestoreHandler._config;
    // this._config = Config.contactListConfig
    //   ? Config.contactListConfig
    //   : Config.listBuilderConfig? Config.listBuilderConfig;
    this._config = {
      active: true,
      memoryJogger: { id: "Build My List", title: "Build My List" },
      globalLists: config.variables.listBuilder?.defaultLists,
    };

    console.log("init with default lists: "+JSON.stringify(this._config.globalLists))

    // firebase
    //   .firestore()
    //   .collection("listBuilder")
    //   .onSnapshot(query => {
    //     query.forEach(doc => {
    //       const key = doc.id;
    //       const data = doc.data();
    //       if (key === "config") {
    //         this._config = { ...listBuilderConfig, ...data };
    //       } else if (key === "scores") {
    //         this._scores = generateScores(data);
    //         _callAll(this._scoresCallbacks, this._scores);
    //       }
    //     });

    // add in config lists that do not exist yet
    // const { listBuilder={} } = account // NOTE: now above
    const { lists = [] } = listBuilder;
    let global_list_keys = ""; // all the lists that are and are not in the globalLists
    const globalLists = this._config.globalLists;
    for (g in globalLists) global_list_keys += globalLists[g].id + ", ";

    const newLists = []; // any new lists that need to be added can be placed here

    let list_keys = ""; // all the lists already in the user's account
    for (l in lists) list_keys += lists[l].id + ", ";

    if (lists.length < 1) {
      for (i in globalLists) // add in any new global lists to the user's account
        if (!list_keys.includes(globalLists[i].id))
          newLists.push(globalLists[i]);
    }

    if (newLists.length > 0) {
      // add in new lists if there are any
      Backend.firestoreHandler.updateAccount("listBuilder", {
        lists: lists.concat(newLists),
      });
    }
    // }); // onSnapshot end
  }

  // ANCHOR: generate contact record ID
  recordID(_name) {
    // takes name as string or a contact {object}
    let name = _name;
    if (typeof _name === "object") {
      if (_name.recordID) return _name.recordID;
      name =
        (_name.givenName ? _name.givenName : "") +
        (_name.familyName ? " " + _name.familyName : "");
    }
    return "tmp:" + name.replace(/ /g, "");
  }

  // ANCHOR: save time functions
  getListById(id) {
    const account = Backend.firestoreHandler._account;
    if (!account.listBuilder || !account.listBuilder.lists) return null;
    const lists = account.listBuilder.lists;
    for (l in lists) {
      const list = lists[l];
      if (list.id === id) return lists[l];
    }
    return null;
  }
  mergeContactArrays(contacts1, contacts2) {
    const allContacts = contacts1.concat(contacts2);
    const mergedContacts = [];
    let recordIDs = "";
    for (c in allContacts) {
      const contact = allContacts[c];
      const recordID = contact.recordID
        ? contact.recordID
        : this.recordID(contact);
      if (!recordIDs.includes(recordID)) mergedContacts.push(contact);
      recordIDs += recordID + ", ";
    }
    return mergedContacts;
  }
}

const _setDateTime = (date, time) => {
  // NOTE sets the given date's time to whatever is given in in format of 0:00 AM/PM
  const index = time.indexOf(":"); // replace with ":" for differently displayed time.
  const index2 = time.indexOf(" ");

  let hours = Number.parseInt(time.substring(0, index));
  let minutes = Number.parseInt(time.substring(index + 1, index2));

  let mer = time.substring(index2 + 1, time.length);
  if (mer == "PM") {
    hours = hours + 12;
  }

  date.setHours(hours);
  date.setMinutes(minutes);
  date.setSeconds("00");

  return date;
};
const generateScores = (data) => {
  const resetTime = data.resetTime ? data.resetTime : "12:00 AM";
  const scores = [];
  const fullday = 86400000;
  const now = new Date();
  const tomorrow = new Date();
  tomorrow.setTime(now.getTime() + fullday);
  for (i in data) {
    if (!data[i].name) continue;
    const item = data[i];
    const postedAt = item.timestamp.toDate();
    const ts = postedAt.getTime(); // posted at
    let e = _setDateTime(new Date(), resetTime).getTime(); // expires at
    if (e < now.getTime()) e = _setDateTime(tomorrow, resetTime);
    const diff = e - ts; // expires at - posted at
    if (diff <= fullday && diff >= 0 && fullday - diff >= 0) scores.push(item);
  }
  return scores.sort((a, b) => a.score < b.score);
};

class OutsiderHandler {
  // ANCHOR: Variables
  _outsiderUid = null;
  _outsiderAccount = null;

  // ANCHOR: Callbacks
  _outsiderAccountCallbacks = [];

  // ANCHOR: Subscribers
  outsiderAccount(callback) {
    if (this._outsiderAccount) callback(this._outsiderAccount);
    this._outsiderAccountCallbacks.push(callback);
  }
  async connectToOutsiderAccount(uid) {
    this._outsiderUid = uid;
    this.unsubscribe = null;
    this.unsubscribe = firebase
      .firestore()
      .collection("users")
      .doc(uid)
      .onSnapshot((doc) => {
        this._outsiderAccount = doc.data();
        _callAll(this._outsiderAccountCallbacks, this._outsiderAccount);
      });
  }
  closeOutsiderAccountConnection() {
    if (this.unsubscribe) this.unsubscribe();
  }
}

class ScoreHandler {
  setMyScore(score) {
    if (!listBuilderHandler._scores) return false;
    const scoreObject = {
      name: Backend.firestoreHandler._account.name,
      score,
      timestamp: new Date(),
    };
    // const updateArray = listBuilderHandler._scores
    const updateObject = {};
    updateObject[Backend.firestoreHandler.uid] = scoreObject;
    return firebase
      .firestore()
      .collection("listBuilder")
      .doc("scores")
      .set(updateObject, { merge: true });
  }
}

export const listBuilderHandler = new ListBuilderHandler();

// Functions

const makeRandomString = (length) => {
  let result = "";
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ023456789";
  const charactersLength = characters.length;
  for (var i = 0; i < length; i++)
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  return result;
};
