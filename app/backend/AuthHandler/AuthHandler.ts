import Backend from "backend/";
import { Toasty } from "components/";
import R from "ramda";
import firebase from "react-native-firebase";

interface checkEmailandPassRES {
  code: `PW_SHORT` | `PW_GOOD` | `EMPW_GOOD`;
}
function checkEmailandPassword(email: string, password: string) {
  return new Promise<checkEmailandPassRES>((resolve, reject) => {
    !R.isNil(password) && password.length < 6
      ? resolve({ code: `PW_SHORT` })
      : resolve({ code: `EMPW_GOOD` });
  });
}

/**
 * TODO finish this
 * @param email User email
 * @param password User password
 */
interface c2EmailRES {
  code:
    | `EMAIL_EXISTED_VERIFIED`
    | `EMAIL_EXISTED_xVERIFIED`
    | `EMAIL_xEXISTED`
    | `ERR_EMAIL_AUTH`;
  uid?: string;
}

async function connectWithEmail(name: string, email: string, password: string) {
  Toasty({ text1: "Loading..." });
  return new Promise<c2EmailRES>(async (resolve, reject) => {
    try {
      //*  TODO: add this
      // checkEmailandPassword()
      const doRegister = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password); //TODO @K->@NL: Why do you put name in the password field???
      // Toasty.hide() //* don't hide Toast here
      if (doRegister.user) {
        Backend.firestoreHandler.initWithUser(doRegister.user.uid);
        /** 🚩1: If user already verified his email... */
        if (!!doRegister.user.emailVerified) {
          console.log("EMAIL_EXISTED_VERIFIED");
          resolve({
            code: "EMAIL_EXISTED_VERIFIED",
            uid: doRegister.user.uid,
          });
        } else {
          /**
           *  🚩2:...if user hasn't verified yet...
           *  @see https://stackoverflow.com/a/58110763
           *  @see https://firebase.google.com/docs/auth/web/email-link-auth#send_an_authentication_link_to_the_users_email_address
           */
          const { user } = doRegister;
          console.log(`EMAIL_EXISTED_xVERIFIED. Sending verMail...`);
          //@ts-ignore
          // user.sendEmailVerification().catch(error => {
          //   throw new Error(error)
          // })
          resolve({ code: `EMAIL_EXISTED_xVERIFIED`, uid: user.uid });
        }
      }
    } catch (error) {
      console.log("auth error: ", error.code);
      /**
       * Create new user with Email and Pass if xEXISTED
       * Send veriMail
       */
      if (error.code.includes("auth/user-not-found")) {
        await firebase
          .auth()
          .createUserWithEmailAndPassword(email, password)
          .then((_data) => {
            console.log("EMAIL_xEXISTED");
            Backend.firestoreHandler
              .signUp(_data.user.email, name || "")
              .then(() => {
                resolve({ code: `EMAIL_xEXISTED` });
                Toasty({
                  text1: "Welcome on board",
                  type: "success",
                });
              });
            // created.user.sendEmailVerification().catch((error) => {
            //   throw new Error(error);
            // });
            // firebase
            //   .firestore()
            //   .collection("users")
            //   .doc(created.user.uid)
            //   .set({ admin: true, email: created.user.email })
            //   .then(() =>
            //     resolve({ code: `EMAIL_xEXISTED`, uid: created.user.uid })
            //   );
          });
      } else {
        /**
         * Return errLabel() for the errToast
         */
        function errLabel() {
          switch (error.code) {
            case "auth/email-already-in-use":
              return "The email address is already in use by another account."; //* == error.message
              break;
            case "auth/invalid-email":
              return "The email address is badly formatted. Please try again";
              break;
            case "auth/weak-password":
              return "Weak password. Please try again";
              break;
            case "auth/wrong-password":
              return "Wrong password. Please try again";
              break;
            case "auth/unknown":
              /**
               * This is when `sendEmailVerification` received multiple requests
               */
              const message = R.includes(
                "We have blocked all requests",
                error.message
              )
                ? "Our server is overheated right now. Please try again later"
                : "There is problem connecting with server.\nPlease try again";
              return message;
            /** ⚠️ Special case */
            case "auth/user-not-found":
              return "New user created for\n" + email;
              break;
            default:
              console.log("(auth) err: ", error.message);
              return "There is problem connecting with server.\nPlease try again";
              break;
          }
        }
        resolve({ code: `ERR_EMAIL_AUTH` });
        Toasty({
          text1:
            error.code == "auth/user-not-found" ? "Welcome on board" : "Error",
          text2: errLabel(),
          type: error.code == "auth/user-not-found" ? "success" : "error",
        });
      }
    }
  });
}

function resetPassword(email: string) {
  Toasty({
    text1: "Loading...",
    buttonText: "⌛",
  });
  return new Promise(async (resolve, reject) => {
    if (email) {
      await firebase
        .auth()
        .sendPasswordResetEmail(email)
        .then(() => resolve({ code: `RESET_EMAIL_SENT` }))
        .catch((error) => {
          console.log("resetPassword error: ", error);
        });
    } else {
      resolve({ code: `EMAIL_NOT_FOUND`, message: `Please type your email` });
    }
  });
}

function sendEmailVerification(email: string) {
  Toasty({
    text1: "Loading...",
    buttonText: "⌛",
  });
  return new Promise(async (resolve, reject) => {
    const currentUser = firebase.auth().currentUser;
    // console.log("mail verified? ", currentUser.emailVerified)
    !!currentUser.emailVerified
      ? null
      : currentUser
          .sendEmailVerification()
          .then((result) => console.log("email-verification RES: ", result))
          .catch((error) => {
            throw new Error(error);
          });

    if (email) {
      await firebase
        .auth()
        .sendPasswordResetEmail(email)
        .then(() => resolve({ code: `RESET_EMAIL_SENT` }))
        .catch((error) => {
          console.log("resetPassword error: ", error);
        });
    } else {
      resolve({ code: `EMAIL_NOT_FOUND`, message: `Please type your email` });
    }
  });
}

/**
 * ### Fn to sign out from frbs auth()
 * Remember to reset local storage
 * and navigate back to login screen
 * after this
 * ---
 * @version 1.1.22
 * -  Add callback fn
 */
async function signOut() {
  if (firebase.auth().currentUser) {
    console.log("Logout button clicked");
    firebase
      .auth()
      .signOut()
      .catch((e) => console.warn("error signing out: ", e));
    // c && c();
    Toasty({ text1: "See you soon!" });
  }
}

export const authHandler = {
  connectWithEmail,
  checkEmailandPassword,
  resetPassword,
  sendEmailVerification,
  signOut,
};
