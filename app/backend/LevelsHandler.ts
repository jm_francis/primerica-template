import Backend from "backend/";
import { dMediaPageItem, MediaPageSchema, UserSchema } from "backend/schemas";
import K from "constant-deprecated/";

/**
 * If the page/level name is "New Person - Level 2", the function will return the number 2
 */
const indexFromName = (name: string): number =>
  Number.parseInt(
    name.substring(name.length - 2, name.length).replace(" ", "")
  );

/**
 * Returns the valid level enabling keyword used in the given page/level name
 *
 * Ex: keywordFromName("Some Page - Class 4") // would return "Class"
 *
 * Level enabling keywords can be found in Config.js
 */
const keywordFromName = (name: string): string => {
  const levelKeywords = K.Config.enablingKeywordsForProgressFeature;
  for (let k in levelKeywords) {
    if (name && name.toLowerCase().includes(levelKeywords[k].toLowerCase()))
      return levelKeywords[k];
  }
  return null;
};

class LevelsHandler {
  // ANCHOR: Variables
  // ANCHOR: Callbacks
  // ANCHOR: Subscribers

  /**
   * Returns all pages with the level feature eneabled on them
   * A page qualifies by having a level enabling keyword and number at the end of its name like: "Some Page - Level 2" "Another Page - Class 3"
   */
  getPagesWithLevels(): MediaPageSchema[] {
    const pages = Backend.firebasePages._pages;
    const levelKeywords = K.Config.enablingKeywordsForProgressFeature;
    const levelPages: MediaPageSchema[] = [];
    for (let p in pages) {
      const page = pages[p];
      for (let l in levelKeywords) {
        const keyword = levelKeywords[l].toLowerCase();
        if (page.name.toLowerCase().includes(keyword)) levelPages.push(page);
      }
    }
    return levelPages;
  }

  /**
   * Returns the last levels page. For example, if there are a total of 6 levels, the page with a name like "New Reps - Level 6" would get returned.
   */
  getLastPage(): MediaPageSchema {
    // if (this._lastPage) return this._lastPage;
    const levelPages = this.getPagesWithLevels();
    let lastPage: MediaPageSchema;
    let lastPageIndex = 0;
    for (let l in levelPages) {
      const page = levelPages[l];
      const pageIndex = indexFromName(page.name);
      if (pageIndex > lastPageIndex) {
        lastPage = page;
        lastPageIndex = pageIndex;
      }
    }
    return lastPage;
  }

  /**
   * Returns the page that contains specified keyword
   * Ex: _pageWithKeyword(" - Level 5")
   */
  _pageWithKeyword(keyword: string): MediaPageSchema {
    const pages = Backend.firebasePages._pages;
    for (let p in pages) {
      const page = pages[p];
      if (!page.name || typeof page.name !== "string") continue;
      if (page.name.toLowerCase().includes(keyword.toLowerCase())) return page;
    }
    return null;
  }

  /**
   * Returns percentage progress of specified level (gets from currently logged in account or if an altAccount is specified it calculates their progress on that level)
   * Ex: calculateProgressForLevel("New Rep - Level 2")
   */
  calculateProgressForLevel(pageName: string, altAccount?: UserSchema): number {
    if (!pageName) return 1;
    const account = Backend.firestoreHandler._account;
    if (
      !account ||
      !account.levels ||
      !Backend.firebasePages.isLevelPage(pageName)
    )
      return 0;
    const levels = altAccount ? altAccount.levels : account.levels;
    if (!levels) {
      return 0;
    }
    const level = levels[pageName] ? levels[pageName] : null;
    const page = Backend.firebasePages.getPageWithName(pageName);
    if (!page || !level) return 0;
    const items = page.content;
    let completeCount = 0;
    let incompleteCount = 0;
    for (let i in items) {
      const item = items[i];
      if (!this.isCompletableItem(item)) continue;
      const levelItem = level[item.id];
      if (!levelItem || !levelItem.complete) ++incompleteCount;
      else if (levelItem.complete) ++completeCount;
    }
    const result = completeCount / (completeCount + incompleteCount);
    const lastPage = this.getLastPage();
    if (lastPage.name === page.name && result === 1) {
      Backend.firestoreHandler.updateAccount("", {
        allLevelsCompleted: true,
      });
    }
    return result;
  }

  /**
   * Returns the progress percentage of the level that comes before the specified level
   * Ex: progressForPreviousLevel("New Rep - Level 2") // would return level progress for "New Rep - Level 1"
   */
  progressForPreviousLevel(levelKey: string): number {
    const config = Backend.firestoreHandler._config.variables;
    if (config && config.levels && config.levels.allowLevelSkipping === true) {
      return 1;
    }

    const levelIndex =
      Number.parseInt(
        levelKey
          .substring(levelKey.length - 2, levelKey.length)
          .replace(" ", "")
      ) - 1;
    if (!levelIndex) {
      return 1;
    }
    if (levelIndex < 1) return 1;

    const levelKeyword = keywordFromName(levelKey) + " " + levelIndex;
    const page = this._pageWithKeyword(levelKeyword);
    const previousLevelName = page.name;
    // const previousLevelName = levelKey.replace(`${levelIndex + 1}`, `${levelIndex}`)

    const progressforlevel = this.calculateProgressForLevel(previousLevelName);

    return progressforlevel;
  }

  /**
   * Returns the overall levels progress for the specified user. (the average of all existing levels)
   * Ex: calculateTotalLevelProgressForUser(user)
   */
  calculateTotalLevelProgressForUser(_user: UserSchema): number {
    const user = _user ? _user : Backend.firestoreHandler._account;
    let fullLevelProgress = 0;
    for (let l in user.levels) {
      const levelProgress = Backend.levelsHandler.calculateProgressForLevel(
        l,
        user
      );
      fullLevelProgress = (fullLevelProgress + levelProgress) / 2;
    }
    return fullLevelProgress;
  }

  isItemComplete(page: MediaPageSchema, item: dMediaPageItem): boolean {
    if (!item) return true;
    const levels = Backend.firestoreHandler._account.levels;
    return (
      levels &&
      levels[page.name] &&
      levels[page.name][item.id] &&
      levels[page.name][item.id].complete
    );
  }

  /**
   * Returns true if the specified dMediaPageItem is more than just some text by itself (like a video or audio)
   * Otherwise, if returned false, it will not be an item that counts toward level progress
   */
  isCompletableItem(data: dMediaPageItem): boolean {
    // ex: if item is just a title/text, it should not be checkable
    const valid = (str: string) => str && str.length > 1;
    if (valid(data.media) || valid(data.url)) return true;
    return false;
  }

  /**
   * If the user has completed all levels, or user is an admin, or their levels have been bypassed, returns true
   * If they have not completed going through all their levels, returns false
   */
  hasCompletedLevels(): boolean {
    const account = Backend.firestoreHandler._account;
    if (account.admin) return true;
    return account.allLevelsCompleted;
  }

  /**
   * Updates the specified item as either complete or incomplete in its page/level for currently logged in user
   * itemId is the document ID in firebase for that dMediaPageItem
   * Ex: updateLevelItem("New Rep - Level 1", docIdOfItem, true)
   */
  updateLevelItem(levelName: string, itemId: string, complete: boolean) {
    const data = {
      ...Backend.firestoreHandler._account.levels,
    };
    if (!data[levelName]) data[levelName] = {};
    if (
      data[levelName][itemId] &&
      data[levelName][itemId].complete === complete
    ) {
      // do nothing, already set to this value
      return;
    }
    data[levelName][itemId] = { complete };
    Backend.firestoreHandler.updateAccount("levels", data);
  }
}

export const levelsHandler = new LevelsHandler();
