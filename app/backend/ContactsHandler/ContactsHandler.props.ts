import Contacts from "react-native-contacts";
export interface dContact extends Contacts.Contact {}

export type API_Contact = keyof dContact;

export type RES_LoadContacts = {
  code: "CONTACTS_X_LOADED" | "CONTACTS_LOADED" | "X_PERMISSIONS";
  contacts?: dContact[] | null[];
};

export type PROPS_ExportableContactInfo = {
  emailAddresses: [{ label: string; email: string }];
  familyName: string;
  givenName: string;
  phoneNumbers: [{ label: string; number: string }];
  recordID: string;
};

export type RES_JSONtoCSV = {
  code: "EXPORT_DONE" | "EXPORT_ERROR" | "X_PERMISSIONS";
  csvFile?: any;
};
