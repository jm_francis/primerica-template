import Backend from "backend/";
import { Toasty } from "components/";
import R from "ramda";
import { Alert, Platform } from "react-native";
import Contacts, { Contact } from "react-native-contacts";
import {
  check as checkPermission,
  PERMISSIONS,
  RESULTS,
} from "react-native-permissions";
import Share from "react-native-share";
import RNFetchBlob from "rn-fetch-blob";
import { fn, IS_ANDROID } from "utilities/";
import {
  API_Contact,
  dContact,
  PROPS_ExportableContactInfo,
  RES_JSONtoCSV,
  RES_LoadContacts,
} from "./ContactsHandler.props";

/**
 *  Check Permission and Get all users' Contacts
 */
async function LoadContacts() {
  console.log("Loading Contacts ...");
  return new Promise<RES_LoadContacts>(async (resolve) => {
    await checkPermission(
      IS_ANDROID ? PERMISSIONS.ANDROID.READ_CONTACTS : PERMISSIONS.IOS.CONTACTS
    ).then((r) => {
      switch (r) {
        case RESULTS.GRANTED:
          Contacts.getAll((err, contacts) => {
            if (err) {
              console.log("Err loading contacts: ", err);
              resolve({ code: `CONTACTS_X_LOADED`, contacts: [] });
            } else {
              resolve({ code: `CONTACTS_LOADED`, contacts: contacts });
            }
          });
          break;
        default:
          Toasty({
            type: "error",
            text1: "Failed to load phone contacts",
            text2: "Please provide permission in your settings",
          });
          // Alert.alert(
          //   "Failed to load phone contacts. Please provide permission in your settings!"
          // );
          resolve({ code: `X_PERMISSIONS`, contacts: [] });
          break;
      }
    });
  });
}

/**
 * ###  Converts array of Contacts to CSV
 * -  and saves with specified fileName
 * @author  K
 * @version 1.2.25
 * -  Build json2csv based on personal lib
 *
 */
async function JSONtoCSV(
  data: Contact[],
  fileName: string = "contact.csv"
): Promise<RES_JSONtoCSV> {
  console.log("Exporting contacts to CSV...: ", data);

  let csv = fn.js.toCSV({ data });

  const dir =
    Platform.OS === "ios"
      ? RNFetchBlob.fs.dirs.DocumentDir
      : RNFetchBlob.fs.dirs.DownloadDir;
  const pathToWrite = `${dir}/${fileName}`;
  console.log("path: ", pathToWrite);
  return new Promise<RES_JSONtoCSV>(async (resolve, reject) => {
    await checkPermission(
      IS_ANDROID
        ? PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE
        : PERMISSIONS.IOS.CONTACTS
    ).then((r) => {
      switch (r) {
        case RESULTS.GRANTED:
          RNFetchBlob.fs
            .writeFile(pathToWrite, csv, "utf8")
            .then(() => {
              resolve({ code: "EXPORT_DONE", csvFile: csv });
            })
            .catch((e) => {
              reject({ code: "EXPORT_ERROR", csvFile: null });
              Toasty({
                type: "error",
                text1: "Error",
                text2: "Error happened when exporting",
              });
              console.info(e);
            });
          break;
        default:
          Toasty({
            type: "error",
            text1: "Permission Denied",
            text2: "Please enable storage permission in your settings",
          });
          console.info("Export permission err: ");
          resolve({ code: `X_PERMISSIONS`, csvFile: null });
          break;
      }
    });
  });
}

export function getExportableContacts(
  choosenFields: API_Contact[],
  altlists?: []
): any[] {
  try {
    /** User's Contact Groups */
    const userListBuilder = altlists
      ? altlists
      : Backend.firestoreHandler._account.listBuilder.lists;

    /**
     * User's Contact Lists, grouped by separated array
     *
     * userContacts= [[...],[...]...]
     */
    // @ts-ignore
    const userContacts = fn.js.vLookup(userListBuilder, "contacts");
    // console.log("User Contacts", userContacts);

    let exportableList = [];

    /**
     * Extract each group in userContacts
     * and get needed contact info
     * to push into updatedList[]
     */
    userContacts.forEach((group: dContact[]) => {
      /** ignore group in which group.length == 0 (empty) */
      if (group.length != 0) {
        group.forEach((_fullContactInfo) => {
          /**
           * exportedContact: contact info with choosen fields only:
           *
           */
          const exportedContactInfo: PROPS_ExportableContactInfo = R.pickAll(
            choosenFields,
            _fullContactInfo
          );

          let _emailAddresses: string;
          let _phoneNumbers: string;

          /** Return stringifed list of emails */
          if (
            exportedContactInfo.emailAddresses &&
            exportedContactInfo.emailAddresses.length > 0
          ) {
            _emailAddresses = fn.js
              .vLookup(exportedContactInfo.emailAddresses, "email")
              .join();
            // console.log("emails: ", _emailAddresses);
          }

          /** Return stringifed list of phone numbers */
          if (
            exportedContactInfo.phoneNumbers &&
            exportedContactInfo.phoneNumbers.length > 0
          ) {
            _phoneNumbers = fn.js
              .vLookup(exportedContactInfo.phoneNumbers, "number")
              .join();
            // console.log("numbers: ", _phoneNumbers);
          }

          return exportableList.push(
            R.merge(exportedContactInfo, {
              emailAddresses: _emailAddresses,
              phoneNumbers: _phoneNumbers,
            })
          );
        });
      }
    });

    /**
     * Last step, remove duplicates via recordID
     */
    let uniqueRecordID = [];
    let uniqueExportableList = [];
    exportableList.forEach((contact: PROPS_ExportableContactInfo) => {
      if (!uniqueRecordID.includes(contact.recordID)) {
        uniqueRecordID.push(contact.recordID);
        uniqueExportableList.push(contact);
      }
    });

    /** Return Final Result */
    return uniqueExportableList;
  } catch (error) {
    console.warn("Err fetching exportable contacts:", error);
    Toasty({
      type: "error",
      text1: "Error",
      text2: "Something wrong with preparing your contacts.",
    });
    return null;
  }
}

/**
 * Super-function that get contact lists based on `choosenFields`,
 * remove duplicates,
 * and export as csv file with `fileName` and `shouldShare` ft
 * @param choosenFields
 * @param fileName
 * @param shouldShare
 */
function ContactsExport(
  choosenFields: API_Contact[] = [
    "givenName",
    "familyName",
    "emailAddresses",
    "phoneNumbers",
    "recordID",
  ],
  fileName: string = "Contact",
  shouldShare: boolean = true,
  altLists?: []
) {
  console.log("exporting... " + (altLists ? "(with alt lists)" : ""));

  const exportableContacts = getExportableContacts(choosenFields, altLists);

  JSONtoCSV(exportableContacts, `${fileName}.csv`)
    .then(async (r) => {
      if (r.code == "EXPORT_DONE" && shouldShare) {
        const dir = IS_ANDROID
          ? RNFetchBlob.fs.dirs.DownloadDir
          : RNFetchBlob.fs.dirs.DocumentDir;
        Share.open({
          title: "Export Contacts",
          url: `file://${dir}/${fileName}.csv`,
          subject: fileName,
        });
      }
    })
    .catch((err) => {
      console.info("err exporting: ", err);
      Toasty({
        type: "error",
        text1: "Error",
        text2: "There's a problem exporting your contacts.",
      });
    });
}

/**
 * 1) Opens a contact create form from react-native-contacts to create a brand new contact in user's phone
 * 2) Adds that contact to specified list
 * Ex: CreateContact(someContact, "Prospects", () => { console.log("Done!"); })
 */
async function CreateContact(
  data: Partial<Contact>,
  listTitle: string,
  onComplete?: () => void
) {
  Contacts.openContactForm(<dContact>data, async (error, contact) => {
    if (!contact) return null;
    !!contact && console.log("Contact: ", contact);
    if (!!error) {
      console.warn("Error adding contact", error);
      Toasty({
        text1: "Failed to add contact.",
        text2: "Something went wrong.",
        type: "error",
      });
      if (onComplete) onComplete();
    } else {
      await Backend.listBuilderHandler.makeContactsOne(
        data,
        contact,
        listTitle
      ); // updates backend with contact id
      if (onComplete) onComplete();
    }
  });
}

/**
 * Fn to Handle User's Contact
 * @param props
 */
export const ContactsHandler = { LoadContacts, ContactsExport, CreateContact };
