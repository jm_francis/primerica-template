import Backend from "backend/";
import { Alert } from "react-native";
import firebase, { RNFirebase } from "react-native-firebase";

const _callAll = (funcs: ((value: any[]) => void)[], value: any) => {
  for (let f in funcs) funcs[f](value);
};

class AccountHandler {
  // ANCHOR: Variables
  _user: RNFirebase.User = null;

  // ANCHOR: Callbacks
  _userCallbacks: ((user: RNFirebase.User) => void)[] = [];

  // ANCHOR: Register
  /** 1. Creates a firebase account
   *  2. Executes firestoreHandler.initWithUser(theNewAccount)
   *  3. Executes firestoreHandler.signUp(dataYouGave)
   */
  signUp(data: {
    name: string;
    phoneNumber: string;
    email: string;
    password: string;
  }) {
    const { name="", phoneNumber, email, password } = data;

    firebase
      .auth()
      .createUserWithEmailAndPassword(email.toLowerCase(), password)
      .then(async (userData) => {
        await Backend.firestoreHandler.initWithUser(userData.user.uid);
        Backend.firestoreHandler.signUp(email, name);
      })
      .catch((error) => {
        console.log(error);
        // if(error.includes("TypeError")) return;
        Alert.alert(
          "It looks like there may be an account using this email already, or your email or password is invalid."
        );
      });
  }
  loginWithEmailAndPassword(email: string, password: string) {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((userData) => {
        Backend.firestoreHandler.initWithUser(userData.user.uid);
      })
      .catch((error) => {
        console.log(error);
        // if(error.includes("TypeError")) return;
        Alert.alert("Incorrect email or password. Please try again.");
      });
  }
  sendPasswordResetToEmail(email: string) {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  loggedInCallback: (user: RNFirebase.User) => void;
  needsToRegisterCallback: () => void;

  // ANCHOR: init
  init(
    loggedInCallback: (user: RNFirebase.User) => void,
    needsToRegisterCallback: () => void
  ) {
    // firebase.auth().signOut();
    // return;
    this.loggedInCallback = loggedInCallback;
    this.needsToRegisterCallback = needsToRegisterCallback;
    firebase.auth().onAuthStateChanged((user) => {
      if (this._user?.uid && user?.uid) return;
      console.log("auth state changed");
      this._user = user;
      if (!user) {
        if (this.needsToRegisterCallback) {
          this.needsToRegisterCallback();
          this.needsToRegisterCallback = null;
        }
      } else {
        if (this.loggedInCallback) {
          this.loggedInCallback(user);
          this.loggedInCallback = null;
        }
      }
    });
  }
}

export const accountHandler = new AccountHandler();
