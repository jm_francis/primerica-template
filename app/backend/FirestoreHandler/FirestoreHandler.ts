import Backend from "backend/";
import { updateWithKeys } from "constant-deprecated/Config";
import { Alert, Platform } from "react-native";
import rnFirebase, { RNFirebase } from "react-native-firebase";
// import jsFirebase from "firebase";
import {
  dConfig,
  dListBuilderList,
  dMediaSlide,
  dMoreItem,
  FPATH,
} from "./firestore-handler.props";
import { UserSchema } from "backend/schemas";
import { USER_DEFAULT } from "backend/schemas/user/user.schema";
let firebase = Platform.select({
  android: rnFirebase,
  ios: rnFirebase,
  // web: jsFirebase.initializeApp({}),
});

let TEST_UID: string = null;

const _callAll = (funcs: ((value: any) => void)[], value: any) => {
  for (let f in funcs) funcs[f](value);
};

class FirestoreHandler {
  /**
   * @deprecated uid is now cirectly in the account
   * ### The uid of the currently logged in user
   */
  uid: string = null;

  /**
   * ### The currently logged in user's data from firestore
   */
  _account: UserSchema = null;
  /**
   * ### The config for the app
   */
  _config: dConfig = null;
  /**
   * @deprecated We are now migrating to the social possts
   * ### The image slides that show up at the top of the home screen
   */
  _mediaSlides: dMediaSlide[] = null;
  /**
   * ### All the items that go on the More page of the app
   */
  _more: dMoreItem[] = null;


  _accountCallbacks: ((account: UserSchema) => void)[] = [];
  _configCallbacks: ((config: dConfig) => void)[] = [];
  _mediaSlidesCallback: ((slides: dMediaSlide[]) => void)[] = [];
  _moreCallbacks: ((moreItems: dMoreItem[]) => void)[] = [];

  /**
   * ### Get live updates for the user's account
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * firestoreHandler.account(account => setAccount(account))
   */
  account(callback: (account: UserSchema) => void) {
    if (this._account) callback(this._account);
    this._accountCallbacks.push(callback);
  }

  
  
  /**
   * ### Get live updates for firestore config
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * firestoreHandler.config(config => setConfig(config))
   */
  config(callback: (config: dConfig) => void) {
    if (this._config) callback(this._config);
    this._configCallbacks.push(callback);
  }
  /**
   * ### Get live updates for the media slides that go across on the home screen
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * firestoreHandler.mediaSlides(slides => setSlides(slides))
   */
  mediaSlides(callback: (slides: dMediaSlide[]) => void) {
    if (this._mediaSlides) callback(this._mediaSlides);
    this._mediaSlidesCallback.push(callback);
  }
  /**
   * ### Get live updates for the content that appears on the "More" screen
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * firestoreHandler.more(more => console.log(more))
   */
  more(callback: (moreItems: dMoreItem[]) => void) {
    if (this._more) callback(this._more);
    this._moreCallbacks.push(callback);
  }

  /**
   * ### Creates a user document in firestore with necessary defaults
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * firestoreHandler.signUp("demo@demo.com")
   */
  async signUp(email: string, name: string): Promise<void> {
    const defaultListBuilderLists: dListBuilderList[] =
      Backend.firestoreHandler._config.variables.listBuilder?.defaultLists;
    const data: UserSchema = {
      listBuilder: {
        lists: defaultListBuilderLists ? defaultListBuilderLists : [],
        shareTo: [],
      },
      levels: [],
      email,
      name,
      // ...defaultData,
      createdAt: new Date(),
    };
    const approval = await Backend.localHandler.testAppPassword();
    if (approval === Backend.localHandler.ACCESS.ADMIN) data.admin = true;
    // delete data["password"];

    if (!this.uid) {
      console.log("NO UID!? =(");
      return;
    }

    const uid = TEST_UID ? TEST_UID : this.uid;
    return await firebase
      .firestore()
      .collection(FPATH.USERS)
      .doc(uid)
      .set(data);
  }

  /**
   * ### Update the currently logged in user's account
   * - Update the currently logged in users account in firestore by merging in/overring given changes.
   * If the where string has a value, the given data will merge into that field without completely overwriting it.
   *
   * @example
   * Backend.firestoreHandler.updateAccount("", { admin: true })
   * @example
   * Backend.firestoreHandler.updateAccount("listBuilder", { shareTo: [...] })
   *
   * - The second example would still preserve the listBuilder.lists and just merge the .shareTo change in with it.
   *
   * @author jm_francis
   * @version 1.1.19
   */
  updateAccount(
    where: string = "",
    data: Partial<UserSchema> | {}
  ): Promise<void> {
    if (!this._account) return null; // NOTE: just in case

    const updateObject = {
      ...this._account,
      ...(where === "" ? data : {}),
    };
    if (updateObject.imitate) {
      delete updateObject.imitate;
    }
    if (where) {
      updateObject[where] = {
        ...(this._account[where] ? this._account[where] : {}),
        ...data,
      };
    }
    const uid = TEST_UID ? TEST_UID : this.uid;
    return firebase
      .firestore()
      .collection(FPATH.USERS)
      .doc(uid)
      .set(updateObject);
  }

  /**
   * ### Updates the config with given fields
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * firestoreHandler.updateConfigVariables({ appTitle: "My Cool App" })
   */

  updateConfigVariables(data: Partial<dConfig>): Promise<void> {
    if (!this._config) return null;
    return firebase
      .firestore()
      .collection(FPATH.CONFIG)
      .doc("variables")
      .set({ ...this._config.variables, ...data });
  }

  /**
   * ### Fetch the config with .onSnapshot and call all callback functions anytime an update to the config occurs
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * initConfig((config) => { })  // callback function will only be called once after config is fetched
   */
  initConfig(oneTimeCallback: (config: dConfig) => void) {
    // Config
    firebase
      .firestore()
      .collection(FPATH.CONFIG)
      .onSnapshot((query) => {
        const result: dConfig = <dConfig>{};
        if (query.docs.length < 1) {
          Alert.alert("Be sure to check your internet connection.");
          return;
        }
        query.forEach((doc) => {
          const key = doc.id;
          const data = doc.data();
          result[key] = data;
        });
        this._config = result;
        updateWithKeys(this._config.keys);
        if (oneTimeCallback) oneTimeCallback(this._config);
        _callAll(this._configCallbacks, this._config);
      });
  }

  refs = [];

  /**
   * ### Simulates you being logged in as the specified user
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * firestoreHandler.imitateUser(someUser)
   */
  async imitateUser(user: UserSchema) {
    this._account = user;
    TEST_UID = user.uid;
    for (let r in this.refs) this.refs[r]();
    this.uid = null;
    this.refs = [];
    this.initWithUser(user.uid);
  }
  /**
   * ### Stops simulating the current user account being simulated
   * - See firestoreHandler imitateUser function
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * firestoreHandler.stopImitatingUser()
   */
  async stopImitatingUser() {
    TEST_UID = null;
    for (let r in this.refs) this.refs[r]();
    this.uid = null;
    this.refs = [];
    this.initWithUser(Backend.accountHandler._user);
  }

  /**
   * ### Initiates .onSnapshot()s for all necessary collections
   * - If this function gets run a second time, it will automatically cancel the execution to avoid duplication.
   *
   * @author jm_francis
   * @version 1.1.26
   *
   * @example
   * firestoreHandler.initWithUser(authUser)
   */
  async initWithUser(user: RNFirebase.User) {
    let ref: () => void;
    // user comes from accountHandler
    if (this.uid) return;
    this.uid = user.uid;
    // Account
    const uid = TEST_UID ? TEST_UID : this.uid;
    ref = firebase
      .firestore()
      .collection(FPATH.USERS)
      .doc(uid)
      .onSnapshot((doc) => {
        const account: UserSchema = <UserSchema>doc.data();
        this._account = { ...USER_DEFAULT, ...account, uid: doc.id };
        if (TEST_UID) this._account.imitate = true;
        else if (this._account.email !== user.email) {
          // this is a workaround fix that will rarely be executed
          this.updateAccount("", {
            email: user.email,
          });
        }
        _callAll(this._accountCallbacks, this._account);
      });

    this.refs.push(ref);

    // Media Slider
    firebase
      .firestore()
      .collection("eventSlider")
      .get()
      .then((query) => {
        let result = [];
        query.forEach((doc) => {
          const data: dMediaSlide = <dMediaSlide>doc.data();
          data.title =
            data.title &&
            typeof data.title === "string" &&
            data.title.length > 1
              ? data.title
              : "";
          data.description =
            data.description &&
            typeof data.description === "string" &&
            data.description.length > 1
              ? data.description
              : "";
          result.push(data);
        });
        result = result.sort((a, b) => {
          return a.position - b.position;
        });
        this._mediaSlides = result;
        _callAll(this._mediaSlidesCallback, this._mediaSlides);
      });

    this.refs.push(ref);

    // More
    ref = firebase
      .firestore()
      .collection(FPATH.MORE)
      .onSnapshot((query) => {
        const result = [];
        query.forEach((doc) => {
          const data: dMoreItem = <dMoreItem>doc.data();
          result.push(data);
        });
        this._more = result;
        _callAll(this._moreCallbacks, this._more);
      });

    this.refs.push(ref);
  }
}

export const firestoreHandler = new FirestoreHandler();
