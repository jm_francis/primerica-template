import { dTeam } from "backend/schemas";
import { Contact } from "react-native-contacts";
import { Timestamp } from "react-native-firebase/firestore";

export enum FPATH {
  USERS = "users",
  CONFIG = "config",
  NOTIFICATIONS = "notifications",
  PAGES = "pages",
  SCOREBOARDS = "scoreboards",
  MORE = "more",
  POSSTS = "possts",
}

/**
 * goes with ListBuilderHandler
 */
export interface dListBuilderList {
  title: string;
  id: string;
  contacts: Contact[];
}

/**
 * @deprecated Please now import { UserSchema } from "backend/schemas"
 */
export interface dAccount {
  uid?: string;
  name?: string;
  email: string;
  phoneNumber?: string;
  createdAt: Date | Timestamp;
  admin?: boolean;
  banned?: boolean;
  allLevelsCompleted?: boolean;
  notifications?: {
    lastRead: Date | Timestamp;
  };
  levels?: {};
  listBuilder?: {
    lists: dListBuilderList[];
    shareTo: string[];
  };
  /**
   * If user is currently imitating/simulating
   * another user's account to test their account
   */
  imitate?: boolean;
  team?: dTeam;
}

export interface dConfig {
  keys: {
    calendarAccessToken?: string;
    calendarId?: string;
    dropbox?: string[];
    vimeo?: { accessToken: string; userId: string }[];
  };
  variables: {
    adminPassword: string;
    appPassword: string;
    appTakeoffShareURL?: string;
    appTitle: string;
    disableCalendar: boolean;
    disableContactManager: boolean;
    disableScoreboard: boolean;
    disableMore: boolean;
    enablePushNotifications: boolean;
    googleDrive?: { folderId: string };
    levels?: {
      allowLevelSkipping?: boolean;
      lockMedia?: boolean;
    };
    listBuilder: {
      defaultLists: dListBuilderList[];
    };
    releaseVersion?: string;
    updateURL?: string;
    reviewMode?: boolean;
    videoCalls?: { title: string; meetingId: string }[];
  };
}

export interface dMediaSlide {
  title?: string;
  description?: string;
  position: Number;
  image: string;
  link?: string;
}

export interface dMoreItem {
  name: string;
  position: number;
  logo?: string;
  iosLink?: string;
  androidLink?: string;
  contactEmail?: string;
  contactPhone?: string;
  otherLink?: string;
}
