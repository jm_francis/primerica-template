//@ts-check
import { IconPrimr } from "components/";
import { Config, Fonts } from "constant-deprecated/";
import { MediaPage, TeamPage, UserPage } from "pages/";
import React from "react";
import { Platform } from "react-native";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { C, ifIphoneX, KeyOf, moderateScale, scale } from "utilities/";
import ProfileImage from "./components/profile-image";
import TeamLogo from "./components/team-logo";
import { HomeStack } from "./home.navigator";

const { disableTeamsTab } = Config;

const pageOptions = {
  Home: { screen: HomeStack },
  Media: { screen: MediaPage },
  Team: { screen: TeamPage },
  You: { screen: UserPage },
};
type BOTTOM_TAB = KeyOf<typeof pageOptions>;

if (disableTeamsTab) {
  delete pageOptions.Team;
}
// if (disableScheduleTab) {
//   delete pageOptions.Tools;
// }

const plainTintColor = C.grey600;

const ipadTabBarStyle = Platform.isPad
  ? {
      height: 90,
    }
  : {};
const ipadLabelStyle = Platform.isPad
  ? {
      marginTop: -15,
      fontSize: moderateScale(15, 0.15),
    }
  : {};

export const MainNavigator = createBottomTabNavigator(
  //pages,
  pageOptions,
  {
    lazy: Platform.OS === "ios" ? false : true,
    tabBarOptions: {
      inactiveTintColor: plainTintColor,
      safeAreaInset: { bottom: 'never', top: 'never' },
      // style: { backgroundColor: C.background01},
      allowFontScaling: Platform.isPad ? true : false,
      adaptive: false,
      style: {
        // paddingBottom: getBottomSpace("safe"),
        backgroundColor: "green",
        alignSelf: "center",
        justifyContent: "center",
        ...ipadTabBarStyle,
      },
      tabStyle: {
        backgroundColor: C.background01,
        alignSelf: "center",
        height: Platform.isPad ? 56 * 2 : 56,
      },
      labelStyle: {
        fontFamily: Fonts.bold,
        ...ifIphoneX(
          {},
          {
            marginTop: -9,
            marginBottom: 9,
          }
        ),
        ...ipadLabelStyle,
      },
    },
    defaultNavigationOptions: ({
      navigation,
    }: {
      navigation: { state: { routeName: BOTTOM_TAB } };
    }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let _tintColor = tintColor;
        switch (routeName) {
          case "Home":
            return (
              <IconPrimr
                preset={`safe`}
                name={"home"}
                size={scale(20)}
                color={_tintColor}
              />
            );
            break;
          case "Media":
            return (
              <IconPrimr
                preset={`safe`}
                name={"folder"}
                size={scale(20)}
                color={_tintColor}
              />
            );
            break;
          case "Team":
            return <TeamLogo tintColor={_tintColor} />;
            break;
          case "You":
            return <ProfileImage tintColor={_tintColor} />;
            break;
        }
      },
    }),
  }
);
