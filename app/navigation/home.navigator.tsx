//@ts-check
import { PosstSchema } from "backend/";
import { IconPrimr } from "components/";
import { Fonts } from "constant-deprecated/";
import { DiscussionPage, HomePage, PosstCreatorPage, SboardPage } from "pages/";
import * as React from "react";
import { withSafeAreaInsets } from "react-native-safe-area-context";
import { NavigationRoute, NavigationRouteConfigMap } from "react-navigation";
import {
  createStackNavigator,
  NavigationStackOptions,
  NavigationStackProp,
} from "react-navigation-stack";
import { C } from "utilities/";

/**
 * ###  List of screens in this navigators,
 * -  including their custom `navigationOptions` (optional)
 */
const pageOptions: NavigationRouteConfigMap<
  NavigationStackOptions,
  NavigationStackProp<NavigationRoute, any>
> = {
  "home-scr": { screen: HomePage, navigationOptions: { headerShown: false } },
  "posst-creator-scr": {
    screen: PosstCreatorPage,
    navigationOptions: {
      header: null,
      // title: Create a Post"",
    },
  },
  "discussion-scr": {
    screen: DiscussionPage,
  },
  "scoreboard-scr": {
    screen: SboardPage,
    navigationOptions: ({ navigation }) => ({
      header: () => null,
      headerStyle: { backgroundColor: C.background01, borderBottomWidth: 0 },
    }),
  },
};

export interface dDiscussionScrParams {
  item: PosstSchema;
  section?: "comment";
}
export interface dSboardScrParams {
  sboard: ScoreboardSchema;
}

/**
 * ###  Navigator for Home's Utilities Screens
 * -  That is, we don't have "Home" here
 * (it belongs to MainNavigator's bottom tabs)
 * -  Instead, this is the stack of home-scr's related utilities
 * (create a board, create a post)
 * -  Locating in root-navigator,
 * it'll animate ABOVE MainNavigator's bottom tab,
 * making more sense for UX
 * ---
 * @version 1.1.21
 * -  *Remove home-scr*
 * -  *Add sboard-creator-scr*
 * @author nguyenkhooi
 */
export const HomeStack = createStackNavigator(
  //pages,
  pageOptions,
  {
    cardStyle: { backgroundColor: C.background01 },
    headerMode: "float",
    headerLayoutPreset: "center",
    defaultNavigationOptions: {
      headerBackTitle: " ",
      headerStyle: { backgroundColor: C.background01 },
      headerTitleStyle: { fontFamily: Fonts.bold, color: C.text01 },
      headerBackImage: () => (
        <IconPrimr preset={"circular"} name="arrow_left" color={C.text01} />
      ),
    },
  }
);
