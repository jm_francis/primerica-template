//@ts-check
import Backend, { MediaPageSchema } from "backend/";
import CustomPage from "customPage/CustomPage";
import React from "react";
import { createStackNavigator } from "react-navigation-stack";

export interface CustomPageNavigationOptions {
  /**
   * The name of the media page whose content you want to display
   */
  pageName: string;
  /**
   * Use this to alternatively display some static media page with provided data (maybe from another app/server)
   */
  staticPage?: MediaPageSchema;
  /**
   * The title of a media item you want to automatically scroll to when the page is opened
   */
  scrollToItem?: string;
}

function Display(props: { navigation: any }) {
  const { navigation } = props;
  const pageName = navigation.getParam("pageName", "");
  const staticPage = navigation.getParam("staticPage", null);
  return (
    <CustomPage
      pageName={pageName}
      navigation={navigation}
      staticPage={staticPage}
    />
  );
}

const CustomDisplay = createStackNavigator(
  {
    index: {
      screen: Display,
    },
  },
  { headerMode: "none" }
);
export default CustomDisplay;
