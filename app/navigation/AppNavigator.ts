import { Fonts } from "constant-deprecated/";
import ModalContainer from "navigation/components/ModalContainer";
import { NotificationPage, PermissionPage, ToolsPage, UserPage } from "pages/";
import Developer from "pages/developer/Developer";
import { HitEmUp } from "pages/listBuilder/components";
import MemoryJogger from "pages/listBuilder/memoryJogger/MemoryJogger";
import ListBuilderNavigator from "pages/listBuilder/navigation/ListBuilderNavigator";
import PickAList from "pages/listBuilder/navigation/PickAList";
import StandaloneContactSearch from "pages/listBuilder/navigation/StandaloneContactSearch";
import ListScreen from "pages/listBuilder/screens/ListScreen";
import ShareToList from "pages/listBuilder/ShareToList";
import More from "pages/more/More";
import NotificationComments from "pages/notifications/NotificationComments";
import WelcomePage from "pages/register/welcome-page";
import SignInPage from "pages/register/sign-in-page";
import Teammate from "pages/teammates/Teammate";
import Teammates from "pages/teammates/TeammatesPage";
import TeamAdmin from "pages/team_admin/TeamAdmin";
import { Platform } from "react-native";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import AppPassword from "root/AppPassword";
import BootingUp from "root/BootingUp";
import { moderateScale } from "utilities/";
import AdminNavigator from "./AdminNavigator";
import Browser from "./Browser";
import CustomDisplay from "./CustomDisplay";
import { HomeStack } from "./home.navigator";
import { MainNavigator } from "./main.navigator";
import { MailComapaign } from "pages/admin/MailCampaign";
import { CreateMail } from "pages/admin/mail/CreateMail";
import { MailEditor } from "pages/admin/mail/MailEditor";
import { SendMail } from "pages/admin/mail/SendMail";
import { Audience } from "pages/admin/mail/Audience";
// import HitEmUp from "pages/listBuilder/components"

const Navigator = createStackNavigator(
  {
    BootingUp: {
      screen: BootingUp,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
      }),
    },
    Register: {
      screen: WelcomePage,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
      }),
    },
    SignIn: SignInPage,
    AppPassword: {
      screen: AppPassword,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
      }),
    },
    Permissions: {
      // screen: Permissions,
      screen: PermissionPage,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
      }),
    },
    Home: {
      screen: MainNavigator,
      // screen: ExploreScreen,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
      }),
    },
    "home-utils": {
      screen: HomeStack,
    },
    Notifications: NotificationPage,
    NotificationComments,
    Settings: {
      screen: UserPage,
    },
    Teammate,
    TeamAdmin,
    // Calendar: { screen: Calendar },
    ListBuilder: ListBuilderNavigator,
    ListScreen: createStackNavigator(
      { Index: ListScreen },
      {
        headerMode: "none",
      }
    ),
    HitemUp: createStackNavigator(
      {Index: HitEmUp},
      {
        headerMode: "none",
      }
    ),
    MailComapaign: createStackNavigator(
      {Index: MailComapaign},
      {
        headerMode: "none",
      }
    ),
    CreateMail: createStackNavigator(
      {Index: CreateMail},
      {
        headerMode: "none"
      }
    ),
    MailEditor: createStackNavigator(
      {Index: MailEditor},
      {
        headerMode: "none"
      }
    ),
    SendMail: createStackNavigator(
      {Index: SendMail},
      {
        headerMode:"none"
      }
    ),
    Audience: createStackNavigator(
      {Index: Audience},
      {
        headerMode:"none"
      }
    ), 
    ShareToList: createStackNavigator(
      { Index: ShareToList },
      {
        headerMode: "none",
      }
    ),
    PickAList: createStackNavigator(
      { Index: PickAList },
      {
        headerMode: "none",
      }
    ),
    StandaloneList: createStackNavigator(
      {
        Index: {
          screen: ListScreen,
          navigationOptions: ({ navigation }) => ({
            ...standaloneListNavigationOptions,
          }),
        },
      },
      {
        headerMode: "none",
      }
    ),
    ListBuilderModal: createStackNavigator(
      { ModalContainer, OutsiderListScreen: ListScreen },
      {
        headerMode: "none",
        // defaultNavigationOptions: ({ navigation }) => ({
        //   headerTintColor: "white",
        //   headerStyle,
        //   headerTitleStyle,
        // }),
      }
    ),
    MemoryJogger: createStackNavigator(
      {
        Index: MemoryJogger,
      },
      {
        headerMode: "none",
      }
    ),
    StandaloneContactSearch: createStackNavigator(
      {
        screen: StandaloneContactSearch,
        navigationOptions: ({ navigation }) => ({ title: "Phone Contacts" }),
      },
      {
        headerMode: "none",
      }
    ),
    // Scoreboard,
    Browser,
    Admin: AdminNavigator,
    Teammates,
    More,
    Developer,
    Calendar: {
      screen: ToolsPage,
    },
    CustomDisplay1: CustomDisplay,
    CustomDisplay2: CustomDisplay,
    CustomDisplay3: CustomDisplay,
    CustomDisplay4: CustomDisplay,
    CustomDisplay5: CustomDisplay,
  },
  {
    defaultNavigationOptions: { headerForceInset: { top: "never" } },
    headerMode: "none",
  }
);

const AppNavigator = createAppContainer(Navigator);
export default AppNavigator;

const headerTitleStyle = {
  fontFamily: Fonts.medium,
  color: "white",
  fontSize: moderateScale(19.25, 0.3),
};
const _headerStyle = {
  backgroundColor: "#1239c7",
};
export const headerStyle = Platform.isPad
  ? { ..._headerStyle, height: moderateScale(90, 0.18) }
  : _headerStyle;
const standaloneListNavigationOptions = {
  headerMode: "none",
  headerStyle,
  headerTitleStyle,
  headerRight: () => null,
};
