import Admin from "pages/admin/Admin";
import Users from "pages/admin/Users";
import User from "pages/admin/User";
import NotificationBroadcast from "pages/admin/NotificationBroadcast";
import Email from "pages/admin/email/Email";
import AppSettings from "pages/admin/AppSettings";
import { createStackNavigator } from "react-navigation-stack";

const AdminNavigator = createStackNavigator(
  {
    Admin,
    Users,
    User,
    NotificationBroadcast,
    Email,
    AppSettings,
  },
  {
    headerMode: "none",
  }
);

export default AdminNavigator;
