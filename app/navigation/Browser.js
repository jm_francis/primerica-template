//@ts-check
import { Buttoon, Txt } from "components/";
import { Fonts } from "constant-deprecated/";
import React, { Component } from "react";
import { Text, View, Linking } from "react-native";
import { QuickStyle } from "react-native-shortcuts";
import { WebView } from "react-native-webview";
import { C, scale, spacing, verticalScale } from "utilities/";

export default class Browser extends Component {
  constructor() {
    super();
    this.state = {
      // copyText: "Open in Browser",
      loading: true,
      currentPageURL: "",
    };
  }

  render() {
    const { currentPageURL } = this.state;
    const source = this.props.navigation.getParam("source");

    const headerHeight = verticalScale(65);

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: C.background01,
        }}
      >
        <View
          style={{
            backgroundColor: C.background01,
            ...QuickStyle.boxShadow,
            ...QuickStyle.fillWidth,
            height: headerHeight,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "flex-end",
          }}
        >
          <Buttoon
            icon={{ name: "arrow_left", color: "white", size: scale(21) }}
            appearance="ghost"
            onPress={() => {
              this.props.navigation.goBack();
            }}
          ></Buttoon>
          <Buttoon
            onPress={() => Linking.openURL(currentPageURL)}
            icon={{ name: "arrow_right", right: true, size: scale(12) }}
            appearance="ghost"
            style={{
              marginTop: -spacing(1),
            }}
          >
            <Txt.P2
              style={{
                color: C.primary,
              }}
            >
              Open in Browser
            </Txt.P2>
          </Buttoon>
          {/* </View> */}
          {/* </TouchableOpacity> */}
        </View>

        <WebView
          onLoad={() => {
            this.setState({
              loading: false,
              currentPageURL: source,
            });
          }}
          onFileDownload={({ nativeEvent: { downloadUrl } }) => {
            Linking.openURL(downloadUrl);
          }}
          pullToRefreshEnabled={true}
          allowFileAccessFromFileURLs={true}
          allowsLinkPreview={true}
          sharedCookiesEnabled={true}
          javaScriptEnabled
          allowFileAccess
          domStorageEnabled
          source={{ uri: source }}
          onNavigationStateChange={(data) =>
            this.setState({ currentPageURL: data.url })
          }
          style={{
            ...QuickStyle.fillWidth,
          }}
        />

        {this.state.loading ? (
          <View
            style={{
              position: "absolute",
              top: "50%",
              left: "50%",
            }}
          >
            <Text
              style={{
                left: "-50%",
                fontFamily: Fonts.bold,
                fontSize: 22,
                color: "rgb(161, 157, 156)",
              }}
            >
              Loading...
            </Text>
          </View>
        ) : null}
      </View>
    );
  }
}
