//@ts-check
import { Colors, Fonts } from "constant-deprecated/";
import { X_ICON } from "constant-deprecated/Images";
import React from "react";
import { Image, Text, TouchableOpacity } from "react-native";
import { moderateScale } from "utilities/";

/**
 * @deprecated Use Buttoon or IconPrimr
 */
export default class XButton extends React.Component {
  state = { active: false };

  press() {
    if (this.props.navigation) this.props.navigation.pop();
    if (this.props.onPress) this.props.onPress(!this.state.active);
    if (this.props.useDone) this.setState({ active: !this.state.active });
  }

  render() {
    return (
      <TouchableOpacity onPress={this.press.bind(this)}>
        {this.state.active === true ? (
          <Text style={{ ...Styles.doneText, ...this.props.textStyle }}>
            Done
          </Text>
        ) : (
          <Image
            source={X_ICON}
            style={{ ...Styles.container, ...this.props.style }}
          />
        )}
      </TouchableOpacity>
    );
  }
}

const Styles = {
  container: {
    width: moderateScale(19, 0.35),
    height: moderateScale(19, 0.35),
    tintColor: Colors.black,
    marginLeft: 12
  },
  doneText: {
    color: "rgb(45, 105, 223)",
    fontFamily: Fonts.bold,
    fontSize: moderateScale(18, 0.35),
    marginRight: 16
  }
};
