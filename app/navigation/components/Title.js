import { Fonts } from "constant-deprecated";
import React, { Component } from "react";
import { Platform, Text } from "react-native";
import { moderateScale } from "utilities/";

/**
 * @deprecated Use <Txt> now instead
 */
export default class Title extends Component {
  state = {
    value: ""
  };

  UNSAFE_componentWillReceiveProps(props) {
    if (props.onValue) props.onValue(this.onValue.bind(this));
  }

  onValue(value) {
    this.setState({ value });
  }

  render() {
    const { style = {}, fixAndroid } = this.props;
    return (
      <Text
        style={{
          ...Styles.title,
          ...(fixAndroid && Platform.OS === "android"
            ? { marginLeft: 27 }
            : {}),
          ...style
        }}
      >
        {this.props.children ? this.props.children : this.state.value}
      </Text>
    );
  }
}

const Styles = {
  title: {
    color: "white",
    fontFamily: Fonts.medium,
    fontSize: moderateScale(21.4)
  }
};
