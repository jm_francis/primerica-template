import { Fonts } from "constant-deprecated";
import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { moderateScale } from "utilities/";

/**
 * @deprecated Use Buttoon instead
 */
export default class NextButton extends Component {
  state = {
    text: "Next"
  };

  setText(text) {
    this.setState({ text });
  }

  componentDidMount() {
    const { text, getText } = this.props;
    if (getText) getText(this.setText.bind(this));
    else if (text) this.setState({ text });
  }

  render() {
    // const text = this.props.text&&this.props.text !== 'undefined'? this.props.text : 'Next'
    const { textStyle } = this.props;
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={Styles.container}>
          <Text
            allowFontScaling={false}
            style={{
              ...Styles.text,
              ...textStyle
            }}
          >
            {this.state.text}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const Styles = {
  container: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 16
  },
  text: {
    fontFamily: Fonts.bold,
    color: "rgb(17, 107, 232)",
    fontSize: moderateScale(17.2, 0.35)
  }
};
