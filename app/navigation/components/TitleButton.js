import K from "constant-deprecated";
import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { moderateScale } from "utilities/";

/**
 * @deprecated Use Buttooon instead
 */
export default class TitleButton extends Component {
  render() {
    const { onPress, style = {} } = this.props;

    return (
      <TouchableOpacity onPress={onPress}>
        <View
          style={{
            ...Styles.container,
            ...style,
          }}
        >
          <Text style={Styles.title}>{this.props.children}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const Styles = {
  container: {
    marginVertical: 7,
    marginHorizontal: 20,
  },
  title: {
    color: "white",
    fontFamily: K.Fonts.medium,
    fontSize: moderateScale(19),
  },
};
