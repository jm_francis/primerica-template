import { Fonts } from "constant-deprecated";
import { BACK_BUTTON } from "constant-deprecated/Images";
import React, { Component } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { moderateScale } from "utilities/";

/**
 * @deprecated Use Buttoon instead
 */
export default class BackButton extends Component {
  press() {
    if (this.props.onPress) this.props.onPress();
    else this.props.navigation.pop();
  }
  render() {
    const { text, tintColor, style } = this.props;
    const dimension = moderateScale(20, 0.45);
    return (
      <TouchableOpacity onPress={this.press.bind(this)}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Image
            source={BACK_BUTTON}
            style={{
              width: dimension,
              height: dimension,
              marginLeft: moderateScale(6, 0.35),
              tintColor: tintColor ? tintColor : "white",
              ...this.props.style
            }}
          />
          {text ? (
            <Text
              allowFontScaling={false}
              style={{
                color: tintColor ? tintColor : "white",
                fontFamily: Fonts.medium,
                fontSize: moderateScale(17, 0.35)
              }}
            >
              {text}
            </Text>
          ) : null}
        </View>
      </TouchableOpacity>
    );
  }
}
