import React from "react";
import XButton from "navigation/components/XButton";

/**
 * @deprecated No need to use this sillyness
 */
export default class ModalContainer extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.title,
    headerLeft: (
      <XButton
        navigation={navigation}
        style={{ tintColor: "white" }}
        onPress={() => {
          navigation.goBack();
        }}
      />
    )
  });

  state = { content: null };

  componentDidMount() {
    const content = this.props.navigation.getParam("content", null);
    this.setState({ content });
  }
  render() {
    return this.state.content;
  }
}
