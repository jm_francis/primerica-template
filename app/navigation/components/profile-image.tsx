import React from "react";
import Backend from "backend/";
import { IconPrimr } from "components/";
import { scale } from "utilities/";
import { Image } from "react-native";

export default function ProfileImage(props) {
  const { tintColor } = props;
  const [_account, setAccount] = React.useState(null);

  React.useEffect(function fetchTeam() {
    Backend.firestoreHandler.account(setAccount);
  }, []);

  const profileImage = _account?.profileImage;

  return profileImage ? (
    <Image
      source={{ uri: profileImage }}
      style={{
        width: scale(24),
        height: scale(24),
        borderRadius: 100,
        borderWidth: 1,
        borderColor: tintColor,
      }}
    />
  ) : (
    <IconPrimr
      preset={`safe`}
      name={"admin"}
      size={scale(20)}
      color={tintColor}
    />
  );
}
