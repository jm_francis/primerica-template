import React from "react";
import Backend from "backend/";
import { IconPrimr } from "components/";
import { scale } from "utilities/";
import { Image } from "react-native";

export default function TeamLogo(props) {
  const { tintColor } = props;
  const [_account, setAccount] = React.useState(null);
  const [_teamPage, setTeamPage] = React.useState(null);

  React.useEffect(function fetchTeam() {
    Backend.firestoreHandler.account(setAccount);
    Backend.firebasePages.pages((pages) =>
      setTeamPage(Backend.firebasePages.getMyTeamPage())
    );
  }, []);

  const team = _account?.team;
  const teamLogo = _teamPage && team ? _teamPage.mediaItem.logo : null;

  return !teamLogo ? (
    <IconPrimr
      preset={`safe`}
      name={"flag"}
      size={scale(20)}
      color={tintColor}
    />
  ) : (
    <Image
      source={{ uri: teamLogo }}
      style={{
        width: scale(25.7),
        height: scale(25.7),
      }}
    />
  );
}
